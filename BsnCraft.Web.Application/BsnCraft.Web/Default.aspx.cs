﻿using BsnCraft.Web.Common.Classes;
using DevExpress.Web;
using System;
using System.Web.UI;

namespace BsnCraft.Web
{
    public partial class Application : Page
    {      
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {            
            Session["app"].AppLoad(this);
        }
        #endregion
    }
}