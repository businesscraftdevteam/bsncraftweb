﻿<%@ Application Language="C#" %>
<%@ Import Namespace="BsnCraft.Web.Common.Classes" %>
<%@ Import Namespace="System.Web.Routing" %>

<script RunAt="server">

    //Try to use querystring instead of routing
    void Application_Start(object sender, EventArgs e)
    {
        //RouteTable.Routes.MapPageRoute("", "app/{menu}/{action}/{mode}/{id}/{key}/", "~/Default.aspx");
        //RouteTable.Routes.MapPageRoute("", "app/{menu}/{action}/{mode}/{id}/", "~/Default.aspx");
        //RouteTable.Routes.MapPageRoute("", "app/{menu}/{action}/{id}/", "~/Default.aspx");
        //RouteTable.Routes.MapPageRoute("", "app/{menu}/{action}/", "~/Default.aspx");
        //RouteTable.Routes.MapPageRoute("", "app/{menu}/", "~/Default.aspx");
        //RouteTable.Routes.MapPageRoute("", "app", "~/Default.aspx");
    }

    void Application_Error(object sender, EventArgs e)
    {
        Exception objErr = Server.GetLastError().GetBaseException();
        Helpers.LogExc(objErr);
        Server.ClearError();
    }

</script>
