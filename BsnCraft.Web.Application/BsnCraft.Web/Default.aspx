﻿<%@ Page Language="C#" AutoEventWireup="True" EnableViewState="false" Codebehind="Default.aspx.cs" Inherits="BsnCraft.Web.Application" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>BusinessCraft</title>
    </head>
    <body runat="server">
        <form runat="server" id="form">
            <div class="alerts collapse fixed-top container mt-3"></div>
            <dx:BootstrapCallbackPanel runat="server" id="panel" ClientInstanceName="Panel">
                <SettingsLoadingPanel Delay="500" />
                <ClientSideEvents Init="App.init" BeginCallback="App.begin" EndCallback="App.update" />
                <ContentCollection>
                    <dx:ContentControl>
                        <nav runat="server" id="iconav" visible="false" class="iconav">
                            <a href="javascript:void(0);" runat="server" id="iconlogo" class="iconav-brand">
                                <span class="sr-only">BusinessCraft</span>
                            </a>
                            <div class="iconav-slider">
                                <ul runat="server" id="iconitems" class="nav nav-pills iconav-nav flex-md-column"></ul>           
                            </div>                            
                        </nav>
                        <div runat="server" id="sidenav" visible="false">
                            <nav class="sidebar-nav">
                                <div class="sidebar-header">
                                    <button class="nav-toggler nav-toggler-md sidebar-toggler" type="button" data-toggle="collapse" data-target="#nav-toggleable-md">
                                        <span class="sr-only">Toggle nav</span>
                                    </button>
                                    <button runat="server" id="sidelogo" type="button" class="sidebar-brand btn btn-link d-flex align-items-center">
                                        <span class="sr-only">BusinessCraft</span>
                                    </button>
                                </div>
                                <div id="nav-toggleable-md" class="collapse nav-toggleable-md">
                                    <ul runat="server" id="sideitems" class="nav nav-pills nav-stacked flex-column"></ul>                                
                                    <div class="hr-divider mt-3 mb-3">
                                        <h3 class="hr-divider-content hr-divider-heading">
                                            <a runat="server" id="version" href="javascript:void(0);" class="btn btn-link" data-toggle="tooltip" data-placement="bottom" data-container="body"></a>
                                        </h3>
                                    </div>
                                </div>                            
                            </nav>
                        </div>                        
                        <div runat="server" id="main">                            
                            <div runat="server" id="head" visible="false">
                                <div class="hr-divider mb-3">
                                    <div runat="server" id="usercomp" class="hr-divider-content"></div>
                                </div>                                
                                <div class="dashhead">
                                    <div class="dashhead-titles">
                                        <h6 runat="server" id="subtitle" class="dashhead-subtitle text-primary"></h6>
                                        <h3 runat="server" id="title" class="dashhead-title"></h3>
                                    </div>
                                    <div class="dashhead-toolbar">
                                        <div runat="server" id="modes" class="dashhead-toolbar-item btn-group"></div>
                                        <span runat="server" id="modediv" class="dashhead-toolbar-divider d-inline-block"></span>
                                        <div runat="server" id="toolbar" class="dashhead-toolbar-item btn-group"></div>
                                        <span runat="server" id="tooldiv" class="dashhead-toolbar-divider d-inline-block"></span>
                                        <div runat="server" id="profile" class="dashhead-toolbar-item btn-group"></div>
                                    </div>
                                </div>
                                <div runat="server" id="tabbar">
                                    <ul runat="server" id="tabs" class="nav nav-bordered clearfix"></ul>
                                </div>                                
                                <hr class="mt-0 mb-4" runat="server" id="tabdiv" />
                                <div class="hr-divider mt-2" runat="server" id="actiondiv">
                                    <ul runat="server" id="actions" class="nav nav-pills hr-divider-content hr-divider-nav"></ul>
                                </div>
                                <div runat="server" id="keybar">
                                    <ul runat="server" id="keys" class="nav nav-bordered clearfix"></ul>
                                </div>                                
                                <hr class="mt-0 mb-4" runat="server" id="keydiv" />
                                <div runat="server" id="upload" visible="false" class="dxbs-fl">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label dxbs-fl-cpt" for="panel_UploadControl">Upload:</label>
                                                <dx:BootstrapUploadControl runat="server" ID="UploadControl" UploadMode="Auto" AutoStartUpload="true" FileUploadMode="OnPageLoad"> 
                                                    <AdvancedModeSettings EnableDragAndDrop="true" EnableFileList="true" />
                                                    <ClientSideEvents FileUploadComplete="App.fileUploadComplete" FilesUploadStart="App.filesUploadStart" />
                                                </dx:BootstrapUploadControl>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="uploadFileContainer card border-primary">
                                                  <div class="card-header bg-primary text-white">Uploaded files</div>
                                                  <div class="card-body"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <dx:ASPxDashboard runat="server" ID="DashboardDesigner" AllowExportDashboardItems="true" Width="100%" Visible="false" />
                                <dx:ASPxWebDocumentViewer runat="server" ID="ReportViewer" Width="100%" Visible="false" />
                                <dx:ASPxReportDesigner runat="server" ID="ReportDesigner" Width="100%" Visible="false" />
                            </div>  
                        </div>
                        <div runat="server" id="row" visible="false"></div>
                        <div runat="server" id="container" visible="false"></div> 
                    </dx:ContentControl>
                </ContentCollection>                
            </dx:BootstrapCallbackPanel>
            <dx:BootstrapCallbackPanel runat="server" id="modal" ClientInstanceName="Modal">
                <ClientSideEvents EndCallback="App.update" />
                <SettingsLoadingPanel Enabled="false" />
                <ContentCollection>
                    <dx:ContentControl>
                    <div runat="server" id="modalDialog" class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div runat="server" id="modalHeader" class="modal-header">
                                <h5 runat="server" id="modalLabel" class="modal-title"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div runat="server" id="modalBody" class="modal-body"></div>
                            <div runat="server" id="modalFooter" class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </dx:ContentControl>
                </ContentCollection>  
            </dx:BootstrapCallbackPanel>
            <dx:BootstrapCallbackPanel runat="server" id="data" ClientInstanceName="DataOnly">
                <ClientSideEvents EndCallback="App.update" />
                <CssClasses Control="d-none" />
                <SettingsLoadingPanel Enabled="false" />
            </dx:BootstrapCallbackPanel>            
        </form>
    </body>
</html>
