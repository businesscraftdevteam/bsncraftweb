﻿function dbg(str) {
    if (true) {
        console.log(str);
    }
}

//NOTE: this is no longer being used, keeping it incase any more js needs to come to application.js

(function ($, bsn) {

    var MIDDLE_SIZE = 992;
    var dropdownMenu = null;

    $(document).on('shown.bs.modal', '.bsn-modal', function () {
        maxModal($('.bsn-modal'));
        adjustDx();
    });

    $(document).on('hidden.bs.modal', '.bsn-modal', function () {
        //Ensure removed - especially for modal-fullscreen
        $('.modal-backdrop').remove();
        $('body').removeClass('modal-open');
    });

    $(window).on('show.bs.dropdown', function (e) {
        dropdownMenu = $(e.target).find('.dropdown-menu-table');
        $('body').append(dropdownMenu.detach());
        var eOffset = $(e.target).offset();
        dropdownMenu.css({
            'display': 'block',
            'top': eOffset.top + $(e.target).outerHeight(),
            'left': eOffset.left
        });
    });
    $(window).on('hide.bs.dropdown', function (e) {
        $(e.target).append(dropdownMenu.detach());
        dropdownMenu.hide();
    });
   
    $(window).resize(function () {
        maxModal($('.bsn-modal'));
        initNotes();
        if ($(window).width() >= MIDDLE_SIZE) {
            $(".sticky_item").stick_in_parent({ offset_top: 70 });
        } else {
            $(".sticky_item").trigger("sticky_kit:detach");
        }
    });

    $(window).on('load', function () {
        NProgress.configure({ showSpinner: false });
        //initScrollSpy();
    });

    function toggleModal() {
        $('.bsn-modal').toggleClass('modal-fullscreen');
        if (!maxModal($('.bsn-modal'))) {
            var body = $('.bsn-modal').find('.modal-body');
            $(body).height($(body).height() - 80);
        }
        adjustDx();
    }
    bsn.toggleModal = toggleModal;

    function maxModal(obj) {        
        if (!$(obj).hasClass('modal-fullscreen'))
            return false;
        
        var body = $(obj).find('.modal-body');        
        var winHeight = $(window).height();
        var headerHeight = $(obj).find('.modal-header').outerHeight();
        var footerHeight = $(obj).find('.modal-footer').outerHeight();;
        $(body).css('overflow-y', 'auto');
        $(body).height(winHeight - headerHeight - footerHeight);
        return true;
    }
    
    function adjustDx(t) {
        var timeout = t == null ? 0 : t;
        setTimeout(function () { ASPxClientControl.AdjustControls(); }, timeout);
    }
    bsn.adjustDx = adjustDx;    

    function initSearch(s, e) {
        if (typeof s.cpSearchUrl !== 'undefined') {
            var obj = s.GetMainElement();
            var text = s.GetInputElement();
            var type = $(obj).attr('search');
            dbg('type=' + type);
            $(text).bsnAutocomplete({
                minChars: 2,
                serviceUrl: s.cpSearchUrl,
                type: "POST",
                noCache: true,
                params: {
                    "search" : type 
                },
                onSelect: function (suggestion) {
                    var data = JSON.parse(suggestion.data);                        
                    searchValue(type, text, data);
                    $(document).trigger("search", [text, data]);
                }
            });
        }
    }
    bsn.initSearch = initSearch;

    function searchValue(type, textbox, data) {
        switch (type.toLowerCase()) {
            case "customer":
                $(textbox).val(data.CustomerNumber);
                if (typeof CustomerSearch !== 'undefined') {
                    CustomerSearch.SetText(data.CustomerNumber);
                }
                break;
            case "lead":
                $(textbox).val(data.ContractNumber);
                break;
            case "contract":
                $(textbox).val(data.ContractNumber);
                break;
            case "item":
                $(textbox).val(data.ItemNumber);
                break;
            case "house":
                $(textbox).val(data.ItemNumber);
                break;
            case "facade":
                $(textbox).val(data.ItemNumber);
                break;
            case "suburb":
                $(textbox).val(data.Suburb);
                break;
        }        
    }        
        
    //function Login(s) {                
    //    var key = CryptoJS.MD5(Today()).toString();
    //    var k1 = key.substring(0, 16);
    //    key = key + k1;        
    //    var k = CryptoJS.enc.Hex.parse(key);
    //    var o = {
    //        mode: CryptoJS.mode.ECB,
    //        padding: CryptoJS.pad.Pkcs7
    //    };
    //    var decrypted = CryptoJS.TripleDES.decrypt({
    //        ciphertext: CryptoJS.enc.Base64.parse(s)
    //    }, k, o);        
    //    time = decrypted.toString(CryptoJS.enc.Utf8);
    //    key = CryptoJS.MD5(time).toString();
    //    k1 = key.substring(0, 16);
    //    key = key + k1;
    //    k = CryptoJS.enc.Hex.parse(key);
    //    var p = CryptoJS.enc.Utf8.parse(LoginPassword.GetText());
    //    var encrypted = CryptoJS.TripleDES.encrypt(p, k, o);
    //    var password = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
    //    if (ASPxClientEdit.ValidateGroup('Login')) {            
    //        var cb = {
    //            mode: "Login",
    //            username: LoginUsername.GetText(),
    //            password: password,
    //            time: time,
    //            company: LoginCompany.GetSelectedItem().value
    //        }
    //        callback(JSON.stringify(cb));
    //    }        
    //}
    //bsn.Login = Login;

    //function Today() {
    //    var today = new Date();
    //    var dd = today.getDate();
    //    var mm = today.getMonth() + 1;
    //    var yyyy = today.getFullYear();
    //    if (dd < 10) {
    //        dd = '0' + dd
    //    }
    //    if (mm < 10) {
    //        mm = '0' + mm
    //    }
    //    return dd + "/" + mm + "/" + yyyy;
    //}

})(jQuery, window.bsn || (window.bsn = {}));
