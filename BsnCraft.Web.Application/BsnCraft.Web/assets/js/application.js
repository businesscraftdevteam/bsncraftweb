(function ($) {
    window.App = {

        _isPageDirty: false,
        _isDelete: false,
        _isWithTooltips: false,
        _toastTimeoutID: -1,
        _filterRowClicked: false,

        init: function () {
            window.onpopstate = function (event) {
                if (event.state != null) {
                    App.callback(JSON.stringify(event.state))
                }
            }
            NProgress.configure({ showSpinner: false })
            if ($('.docs-top').length) {
                App._backToTopButton()
                $(window).on('scroll', App._backToTopButton)
            }
            App.callback('{}');
        },

        callback: function (cb) {
            console.log(cb)
            if (App._formUpdate(cb)) {
                App._tooltips(true)
                var obj = JSON.parse(cb)
                if (obj.key) {
                    var controls = ASPxClientControl.GetControlCollection()
                    var control = controls.GetByName(obj.key)
                    control.PerformCallback(cb)
                    return
                }
                DataOnly.PerformCallback(cb)
                return
            }
            if (App._checkDirty(cb)) {
                return
            }
            //if (App._confimDelete(cb)) {
            //    return
            //}

            if (App._loadModal(cb)) {
                Modal.PerformCallback(cb)
                return
            }
            if (App._dataOnly(cb)) {
                App._tooltips(true)
                DataOnly.PerformCallback(cb)
                return
            }
            $('#modal').modal('hide')
            App._location(cb)
            Panel.PerformCallback(cb)
        },

        begin: function (sender, e) {
            NProgress.start()
            App._tooltips(true)
            //$("#panel").animate({ opacity: 0.75 })
            $(".bsn-refresh").addClass("fa-spin")
        },

        update: function (sender, e) {
            if (sender != null) {
                App._checkTheme(sender)
                App._checkToast(sender)
                App._checkPanels(sender)
                App._checkModal(sender)
                App._checkFieldUpdate(sender)
                App._checkDataSave(sender)
                //if (sender instanceof ASPxClientGridView) {
                // Example code for grid-only updates                      
                //}
            }

            App._tooltips()
            $(window).on('resize', App._tooltips)

            App._readmore()
            App._popover()
            $('.bsn-lightbox').featherlight()
            //$('#panel').animate({ opacity: 1 })
            $('.bsn-refresh').removeClass('fa-spin')
            NProgress.done()
        },

        report: function (sender, e) {
            e.processOnServer = false
            var keys = sender.GetSelectedKeysOnPage()
            if (e.visibleIndex < 0 && keys.length == 0) {
                return
            }
            var obj = {
                data: {
                    "mode": "SelectReport",
                    "key": keys[0]
                }
            }
            $('.bsn-report').removeClass('d-none')
            DataOnly.PerformCallback(JSON.stringify(obj))
        },

        filterClick: function (sender, e) {
            App._filterRowClicked = true
        },

        filterSelect: function (sender, e, cb) {
            e.processOnServer = false
            //console.log(cb)
            var jsKeys = sender.GetSelectedKeysOnPage()
            var obj = JSON.parse(cb)
            if (obj.data.object) {
                if (App._arraysEqual(obj.data.object, jsKeys)) {
                    return
                }
            }
            var selectAll = jsKeys.length === sender.GetVisibleRowsOnPage()
            var selectNone = jsKeys.length === 0
            if (e.visibleIndex < 0) {
                if (App._filterRowClicked) {
                    //clicking a filter unselects all rows first
                    App._filterRowClicked = false
                    return
                }
                if (!selectAll && !selectNone) {
                    //selectionchanged event from server-side
                    return;
                }
            }
            App._filterRowClicked = false
            obj.data.object = jsKeys
            var selectCallback = JSON.stringify(obj)
            App.callback(selectCallback)
        },

        gridSelect: function (sender, e, cb) {
			e.processOnServer = false
			setTimeout(function () {
				var keys = sender.GetSelectedKeysOnPage()
				if (e.visibleIndex < 0 && keys.length == 0) {
					return
				}

				var obj = JSON.parse(cb)
				obj.data.object = keys
				console.log("gridSelect - Keys:" + JSON.stringify(obj))
				DataOnly.PerformCallback(JSON.stringify(obj))
			}, 200);
        },

        //Used by in-grid form editing
        edit: function (grid, index) {
            //console.log(grid + " " + index)
            var controls = ASPxClientControl.GetControlCollection()
            var control = controls.GetByName(grid)
            if (!control) {
                return
            }
            control.UnselectAllRowsOnPage()
            control.StartEditRow(index)
        },

        checkbox: function (sender, e, cb, yn) {
            App._isPageDirty = true
            if (cb) {
                var obj = JSON.parse(cb)
                if (yn) {
                    obj.data.value = sender.GetChecked() ? 'Y' : 'N'
                } else {
                    obj.data.value = sender.GetChecked()
                }
                App.callback(JSON.stringify(obj))
            }
        },

        combobox: function (sender, e, cb, skipDirty) {
            if (!skipDirty) {
                App._isPageDirty = true
            }
            var value = sender.GetSelectedItem() ? sender.GetSelectedItem().value : null
            console.log("combo: " + sender.name + " " + value)
            App._valueCallback(cb, value)
        },

        dateSelect: function (sender, e, cb) {
            App._isPageDirty = true
            if (cb) {
                var obj = JSON.parse(cb)
                var date = sender.GetDate()
                var day = ("0" + date.getDate()).slice(-2)
                var month = ("0" + (date.getMonth() + 1)).slice(-2)
                var year = date.getFullYear()
                obj.data.value = day + "/" + month + "/" + year
                App.callback(JSON.stringify(obj))
            }
        },

        memo: function (sender, e, cb) {
            App._isPageDirty = true
            if (cb) {
                var obj = JSON.parse(cb)
                obj.data.value = sender.GetValue()
                App.callback(JSON.stringify(obj))
            }
        },

        textbox: function (sender, e, cb, min, typ) {
            App._isPageDirty = true
            if (cb) {
                var obj = JSON.parse(cb)
                var error = { text: '' }
                obj.data.value = sender.GetValue()
                App.minimumValidation(min, obj.data.value, error)
                App.numericValidation(typ, obj.data.value, error)
                sender.SetIsValid(error.text)
                if (error.text) {
                    sender.SetErrorText(error.text)
                    return
                }
                obj.data.value = (obj.data.value || '')
                App.callback(JSON.stringify(obj))
            }
        },

        minimumValidation: function (min, value, error) {
            if (!min) {
                return
            }
            if (!value) {
                error.text = "Question must contain an answer.";
            }
        },

        numericValidation: function (typ, value, error) {
            if (!typ) {
                return
            }
            if (!$.isNumeric(value)) {
                error.text = "Answer must be a numeric value.";
            }
            if (typ === 'p' && value < 0) {
                error.text = "Answer must be a positive numeric value.";
            }
        },

        filesUploadStart: function (sender, e) {
            App.uploadedFilesContainer.hide()
        },

        fileUploadComplete: function (sender, e) {
            if (e.callbackData) {
                var fileData = e.callbackData.split('|')
                var fileName = fileData[0], fileUrl = fileData[1], fileSize = fileData[2]
                App.uploadedFilesContainer.addFile(sender, fileName, fileUrl, fileSize)
            }
        },

        uploadedFilesContainer: {
            uploadControl: null,
            addFile: function (sender, fileName, fileUrl, fileSize) {
                var self = App.uploadedFilesContainer
                if (!sender || sender != self.uploadControl) {
                    self.clear()
                    self.getMainElement().hide()
                    //TODO: Consider below to move upload control itself into BsnForm in future
                    //self.getMainElement().insertAfter(sender.GetMainElement());
                }
                if (self.getMainElement().is(":hidden")) {
                    self.getMainElement().show("slow")
                }
                self.uploadControl = sender
                var html = self.buildHtml(fileName, fileUrl, fileSize)
                self.update(html)
            },

            buildHtml: function (fileName, fileUrl, fileSize) {
                var html = ["<div><span class=\"fileName\"><a href=\""]
                html.push(fileUrl)
                html.push("\" class=\"alert-link\">")
                html.push(fileName)
                html.push("</a></span><span class=\"fileSize\">")
                html.push(fileSize)
                html.push("</span></div>")
                return html.join("")
            },

            getMainElement: function () {
                return $(".uploadFileContainer")
            },

            getInnerElement: function () {
                return $(".uploadFileContainer .panel-body, .uploadFileContainer .card-body")
            },

            update: function (html) {
                var self = App.uploadedFilesContainer
                var $element = self.getInnerElement()
                $element.html($element.html() + html)
            },

            clear: function () {
                var $element = App.uploadedFilesContainer.getInnerElement()
                $element.html("")
            },

            hide: function () {
                App.uploadedFilesContainer.clear()
                var $element = App.uploadedFilesContainer.getMainElement()
                $element.hide()
            }
        },

        dockPanels: function (s, e, cb) {
            if (typeof DockManager === 'undefined') {
                return
            }
            var panelData = []
            DockManager.GetPanels().forEach(function (panel) {
                panelData.push({
                    name: panel.panelUID,
                    zoneId: panel.GetOwnerZone().zoneUID,
                    visible: panel.GetVisible(),
                    visibleIndex: panel.GetVisibleIndex()
                })
            })
            //console.log(JSON.stringify(panelData));
            var obj = JSON.parse(cb)
            obj.data.object = panelData
            App.callback(JSON.stringify(obj))
        },

        calendar: function () {
            if (typeof Calendar === 'undefined')
                return;
            var selectedDates = Calendar.GetSelectedDates();
            var value = '';
            jQuery.each(selectedDates, function (i, date) {
                var day = ("0" + date.getDate()).slice(-2);
                var month = ("0" + (date.getMonth() + 1)).slice(-2);
                var year = date.getFullYear();
                value += day + "/" + month + "/" + year + ";";
            });
            //console.log("dates= " + value);
            var obj = {
                type: "DataOnly",
                data: {
                    mode: "SelectDates",
                    value: value
                }
            }
            //console.log("cb= " + JSON.stringify(obj));
            App.callback(JSON.stringify(obj));
        },

        _valueCallback: function (cb, value) {
            if (cb) {           
                if (App._formUpdate(cb)) {
                    App.callback(cb)
                    return;
                }
                var obj = JSON.parse(cb)
                if (value) {
                    var dataOnly = App._dataOnly(cb)                    
                    if (dataOnly) {
                        //console.log(obj.data.value)
                        if (obj.data.value) {
                            obj.data.value += value
                        } else {
                            obj.data.value = value
                        }
                    } else {
                        if (obj.value) {
                            obj.value += value
                        } else {
                            obj.value = value
                        }
                    }
                }
                App.callback(JSON.stringify(obj))
            }
        },

        _checkDirty: function (cb) {
            var control = $(".bsn-formsave")
            //console.log((control == null) + " " + control)
            //console.log((App._getMode(cb) === 'formsave') + " " + App._getMode(cb))
            var noButton = (control == null || control.length === 0)
            var allowCallback = (App._getMode(cb) === 'formsave' || App._getMode(cb) === 'formupdate')
            if (noButton || allowCallback) {
                if (App._getMode(cb) === 'formupdate') {
                    return false
                }
                App._isPageDirty = false
                return false
            }
            if (App._isPageDirty) {
                $.confirm({
                    title: 'There are unsaved Changes.',
                    titleClass: 'text-danger',
                    content: 'Would you like to Save Changes before closing?',
                    theme: 'bootstrap',
                    escapeKey: 'cancel',
                    buttons: {
                        yes: {
                            text: 'Yes',
                            btnClass: 'btn-primary',
                            action: function () {
                                control.click()
                            }
                        },
                        no: {
                            text: 'No',
                            btnClass: 'btn-danger',
                            action: function () {
                                App._isPageDirty = false
                                App.callback(cb)
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
                return true;
            }
            return false
        },

        _confimDelete: function (cb) {
            if (App._getMode(cb) !== 'delete' || App._isDelete) {
                return false
            }
            App._isDelete = true
            $.confirm({
                title: 'Confirm Delete',
                titleClass: 'text-danger',
                content: 'Are you sure you wish to delete the selected Records?',
				theme: 'supervan',
				escapeKey: 'No',
                buttons: {
                    Yes: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function () {
                            App.callback(cb)
                            App._isDelete = false
                            return true
                        }
                    },
                    No: {
                        text: 'No',
                        action: function () {
                            App._isDelete = false
                        }
                    }
                }
            });
            return true
        },


        _getMode: function (cb) {
            var obj = JSON.parse(cb)
            if (typeof obj.data === 'undefined') {
                return ''
            }
            if (typeof obj.data.mode === 'undefined') {
                return ''
            }
            //console.log(obj.data.mode.toLowerCase())
            return obj.data.mode.toLowerCase()
        },

        _dataOnly: function (cb) {
            var obj = JSON.parse(cb)
            if (!obj.type) {
                return false
            }
            var type = obj.type.toLowerCase()
            if (type === 'dataonly') {
                return true
            }
            return false
        },

        _formUpdate: function (cb) {
            var obj = JSON.parse(cb)
            if (!obj.type) {
                return false
            }
            var type = obj.type.toLowerCase()
            if (type === 'form') {
                return true
            }
            return false
        },

        _loadModal: function (cb) {
            var obj = JSON.parse(cb)
            if (!obj.type) {
                return false
            }
            var type = obj.type.toLowerCase()
            if (type === 'modal') {
                return true
            }
            return false
        },

        _location: function (cb) {
            var obj = JSON.parse(cb)
            if (obj.type && obj.type.toLowerCase() === 'windowlocation') {
                //console.log("location| cb = " + cb)
                var windowTitle = $.grep([obj.menu, obj.action, obj.id], Boolean).join(' ')
                var windowUrl = App._getUrl(obj)
                //console.log(windowUrl)
                var windowState = {
                    type: 'WindowState',
                    app: obj.app,
                    menu: obj.menu,
                    action: obj.action,
                    mode: obj.mode,
                    id: obj.id,
                    key: obj.key,
                    value: obj.value
                }
                document.title = windowTitle
                history.pushState(windowState, windowTitle, windowUrl)
            }
        },

        _getUrl: function (obj) {
            var routes = []
            if (obj.menu) {
                routes.push("m=" + obj.menu)
            }
            if (obj.action) {
                routes.push("a=" + obj.action)
            }
            if (obj.mode) {
                routes.push("o=" + obj.mode)
            }
            if (obj.id) {
                routes.push("i=" + obj.id)
            }
            if (obj.key) {
                routes.push("k=" + obj.key)
            }
            if (obj.value) {
                routes.push("v=" + obj.value)
            }
            //console.log(obj)
            var url = window.location.protocol + '//' + window.location.host + '/'
            if (obj.app) {
                url += obj.app + "/"
            }
            if (routes.length > 0) {
                url += "?" + routes.join('&')
            }
            //console.log(url)     
            return url;
        },

        _checkTheme: function (sender) {
            //console.log("pt = " + sender.cpToolkitPrev + "  t = " + sender.cpToolkit)
            //console.log("pb = " + sender.cpBootstrapPrev + "  b = " + sender.cpBootstrap)
            if (sender.cpToolkitPrev && sender.cpToolkit) {
                $('link[href="' + sender.cpToolkitPrev + '"]').attr('href', sender.cpToolkit)
            }
            if (sender.cpBootstrap) {
                if (sender.cpBootstrapPrev) {
                    $('link[href="' + sender.cpBootstrapPrev + '"]').attr('href', sender.cpBootstrap)
                } else {
                    $('head').append('<link rel="stylesheet" href="' + sender.cpBootstrap + '" type="text/css" />')
                }
            } else {
                if (sender.cpBootstrapPrev) {
                    $('link[href="' + sender.cpBootstrapPrev + '"]').remove();
                }
            }
        },

        _checkToast: function (sender) {
            if (sender.cpToast && sender.cpToast !== '[]') {
                var toastMessage = ''
                var toastAdditional = ''
                var obj = JSON.parse(sender.cpToast)
                $.each(obj, function (i, message) {
                    if (i == 0)
                        toastMessage += message
                    else
                        toastAdditional += message
                })
                App._showToast(toastMessage, toastAdditional, sender.cpToastType)
            }
        },

        _checkPanels: function (sender) {
            if (!sender.cpPanels) {
                return
            }
            if (typeof DockManager === 'undefined') {
                return
            }
            //console.log(sender.cpPanels)
            var panels = JSON.parse(sender.cpPanels)
            for (var i = 0, len = panels.length; i < len; i++) {
                //console.log("panel = " + panels[i].name)
                var panel = DockManager.GetPanelByUID(panels[i].name)
                if (panel != null) {
                    panel.SetVisibleIndex(panels[i].visibleIndex)
                    panel.Dock(DockManager.GetZoneByUID(panels[i].zoneId))
                    panel.SetVisible(panels[i].visible)
                }
            }
        },

        _checkModal: function (sender) {
            if (sender.cpModal) {
                var obj = JSON.parse(sender.cpModal)
                //console.log(obj)
                if (obj.show) {
                    $('#modal').modal('show')
                }
            }
        },

        _checkDataSave: function (sender) {
            if (sender.cpSaveSettings) {
                var obj = { type: "DataOnly", mode: "SaveSettings" }
                DataOnly.PerformCallback(JSON.stringify(obj))
            }
            if (sender.cpDataLocation) {
                //console.log('DataLocation = ' + sender.cpDataLocation)
                App.callback(sender.cpDataLocation)
            }
        },

        _checkFieldUpdate: function (sender) {
            //Not used: replaced with custom callback panel
            if (!sender.cpFields) {
                return
            }
            var fields = JSON.parse(sender.cpFields)
            var controls = ASPxClientControl.GetControlCollection()            
            fields.forEach(function (field) {
                //console.log('field = ' + field.value + " - " + field.data)
                var control = controls.GetByName(field.value)
                if (control) {
                    if (control instanceof ASPxClientTextBox) {
                        control.SetText(field.data)
                    }
                }
            })
        },

        _popover: function (refresh) {
            refresh = refresh || false
            if (refresh) {
                $('.bsn-popover').tooltip('dispose')
            } else {
                $('.bsn-popover').each(function (i, obj) {
                    $(obj).popover({
                        html: true,
                        container: 'body',
                        content: $(obj).find('.popover-content').html(),
                        trigger: 'focus'
                    })
                })
            }
        },

        _showToast: function (message, additionalMessage, type, timeout) {
            if (App._toastTimeoutID > 0) {
                clearTimeout(App._toastTimeoutID)
                App._toastTimeoutID = -1
            }
            $('.bsn-toast').remove()
            if (additionalMessage) {
                message += '<br><em>' + additionalMessage + '</em>'
            }
            type = type || 'primary'
            var alert = '<div class="alert alert-' + type + ' alert-dismissible fade show bsn-toast " role="alert">'
            alert += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            alert += '<span aria-hidden="true">&times;</span></button>' + message + '</div>'
            $(alert).appendTo('.alerts')
            $(".alerts").collapse('show')
            timeout = timeout || 5000
            App._toastTimeoutID = setTimeout(App._hideToast, timeout)
        },

        _hideToast: function () {
            $('.bsn-toast').alert('close')
            $('.alerts').collapse('hide')
            App._toastTimeoutID = -1
        },

        _backToTopButton: function () {
            if ($(window).scrollTop() > $(window).height()) {
                $('.docs-top').fadeIn()
            } else {
                $('.docs-top').fadeOut()
            }
        },

        _tooltips: function (refresh) {
            refresh = refresh || false
            if (($(window).width() > 768) && (!refresh)) {
                //if (App._isWithTooltips) return
                //App._isWithTooltips = true
                $('.bsn-tooltip, [data-toggle="tooltip"]').tooltip({ container: 'body' })
            } else {
                //if (!App._isWithTooltips) return
                //App._isWithTooltips = false
                $('.bsn-tooltip, [data-toggle="tooltip"]').tooltip('dispose')
            }
        },

        _readmore: function () {
            $('article').readmore()
        },

        _arraysEqual: function (array1, array2) {
            if (!Array.isArray(array1) || !Array.isArray(array2) || array1.length !== array2.length) {
                return false
            }
            var arr1 = array1.concat().sort()
            var arr2 = array2.concat().sort()
            for (var i = 0; i < arr1.length; i++) {
                if (arr1[i] !== arr2[i]) {
                    return false
                }
            }
            return true
        }
    }
})(jQuery); 