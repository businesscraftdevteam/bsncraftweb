﻿using BsnCraft.Web.Common.Views;
using BsnCraft.Web.Common.Classes;
using DevExpress.Web.Office;
using System;
using System.IO;
using System.Web.UI;

public partial class Viewers_RichEdit : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var App = new BsnApplication();
        Page.Header.Controls.Add(App.Utils.Styles());
        Page.Header.Controls.Add(App.Utils.Scripts);
        Page.Header.Controls.Add(App.Utils.FavIcon32);
        Page.Header.Controls.Add(App.Utils.FavIcon16);
        Page.Header.Controls.Add(App.Utils.TouchIcon);
        Page.Header.DataBind();

        string filename = Request["filename"];
        if (!string.IsNullOrEmpty(filename))
        {
            try
            {
                DocumentManager.CloseAllDocuments();
                RichEdit.Open(MapPath(filename));
                RichEdit.DocumentId = Path.GetFileNameWithoutExtension(filename);
            }
            catch(Exception exc)
            {
                Helpers.LogExc(exc);
            }
        }        
    }
}