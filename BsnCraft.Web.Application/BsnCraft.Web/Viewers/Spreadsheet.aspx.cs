﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views;
using System;
using System.Web.UI;

public partial class Viewers_Spreadsheet : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var App = new BsnApplication();
        Page.Header.Controls.Add(App.Utils.Styles());
        Page.Header.Controls.Add(App.Utils.Scripts);
        Page.Header.Controls.Add(App.Utils.FavIcon32);
        Page.Header.Controls.Add(App.Utils.FavIcon16);
        Page.Header.Controls.Add(App.Utils.TouchIcon);
        Page.Header.DataBind();

        string filename = Request["filename"];
        if (!string.IsNullOrEmpty(filename))
        {
            //filename = Utils.DocsFolder + filename;
            viewSpreadsheet.Open(MapPath(filename));            
        }        
    }
}