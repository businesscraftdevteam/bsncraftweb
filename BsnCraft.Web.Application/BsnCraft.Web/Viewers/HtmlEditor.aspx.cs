﻿using System;
using System.Web.UI;

public partial class Viewers_HtmlEditor : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string filename = Request["filename"];
        if (!string.IsNullOrEmpty(filename))
        {
            //filename = Utils.DocsFolder + filename;
            viewHtmlEditor.Import(MapPath(filename));
        }        
    }
}