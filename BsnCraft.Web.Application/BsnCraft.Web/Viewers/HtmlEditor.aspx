﻿<%@ Page Language="C#" AutoEventWireup="True"  Codebehind="HtmlEditor.aspx.cs" Inherits="Viewers_HtmlEditor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function OnInit(s, e) {
            s.ExecuteCommand(ASPxClientCommandConsts.FULLSCREEN_COMMAND);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <dx:ASPxHtmlEditor runat="server" id="viewHtmlEditor">
            <ClientSideEvents Init="OnInit" />
        </dx:ASPxHtmlEditor>
    </div>
    </form>
</body>
</html>
