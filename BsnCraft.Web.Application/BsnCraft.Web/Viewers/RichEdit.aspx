﻿<%@ Page Language="C#" AutoEventWireup="True"  Codebehind="RichEdit.aspx.cs" Inherits="Viewers_RichEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <dx:BootstrapRichEdit runat="server" id="RichEdit" ReadOnly="true">
            <ClientSideEvents Init="function(s, e) { s.SetFullscreenMode(true); }" />
        </dx:BootstrapRichEdit>
    </div>
    </form>
</body>
</html>
