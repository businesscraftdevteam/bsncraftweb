﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.UI;

public partial class Viewers_GetData : Page
{
    BsnApplication App;

    protected void Page_Load(object sender, EventArgs e)
    {
        App = (BsnApplication)Session["app"];
        if (!Page.IsPostBack && App != null)
        {
            if (Request.QueryString["callback"] != null)
            {
                try
                {
                    string callBackSignature = Request.QueryString["callback"];
                    string jsData = GetDataSource(Request.QueryString["data"].ToDataSource());
                    string jsFunction = string.Format("{0}({1});", callBackSignature, jsData);
                    Response.ContentType = "text/javascript; charset=utf-8";
                    Response.Write(jsFunction);
                    //Response.End();
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.SuppressContent = true;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
                catch (Exception ex)
                {
                    App.Utils.LogExc(ex);
                }
            }
        }        
    }

    public string GetDataSource(DataSourceTypes dataSource)
    {
        switch (dataSource)
        {
            case DataSourceTypes.LeadsBySalesCentre:
                return JsonConvert.SerializeObject(App.VM.LE.LeadsBySalesCentre);
            case DataSourceTypes.LeadsByOperatingCentre:
                return JsonConvert.SerializeObject(App.VM.LE.LeadsByOperatingCentre);
            case DataSourceTypes.LeadsBySalesConsultant:
                return JsonConvert.SerializeObject(App.VM.LE.LeadsBySalesConsultant);
            case DataSourceTypes.ContractsByOperatingCentre:
                return JsonConvert.SerializeObject(App.VM.CO.ContractsByOperatingCentre);
        }
        return string.Empty;
    }
}