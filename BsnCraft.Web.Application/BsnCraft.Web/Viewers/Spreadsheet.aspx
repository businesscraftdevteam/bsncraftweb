﻿<%@ Page Language="C#" AutoEventWireup="True"  Codebehind="Spreadsheet.aspx.cs" Inherits="Viewers_Spreadsheet" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <dx:BootstrapSpreadsheet runat="server" id="viewSpreadsheet" FullscreenMode="true" />
    </div>
    </form>
</body>
</html>
