﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views;
using System;
using System.Web;
using System.Web.UI;

public partial class Viewers_Thumbnail : Page
{
    BsnApplication App;

    protected void Page_Load(object sender, EventArgs e)
    {
        App = (BsnApplication)Session["app"];
        if (!Page.IsPostBack && App != null)
        {
            try
            {
                Response.Clear();
                var maxWidth = Request["width"].ToInt();
                var maxHeight = Request["height"].ToInt();
                var imageName = Request.QueryString["image"];
                var extension = System.IO.Path.GetExtension(Server.MapPath(imageName));

                imageName = HttpContext.Current.Server.MapPath(imageName);
                byte[] pBuffer = App.Utils.CreateThumbnail(imageName, maxWidth, maxHeight);

                //set response content type: image/jpeg, image/gif, image/png, etc...
                Response.ContentType = "image/" + extension;

                //write the image to the output stream
                Response.OutputStream.Write(pBuffer, 0, pBuffer.Length);

                //Response.End();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
    }
}