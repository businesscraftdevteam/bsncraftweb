﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Reflection;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.ViewModels;
using NLog;
using Newtonsoft.Json;
using DevExpress.DashboardWeb;
using DevExpress.XtraReports.Web;
using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using DevExpress.Web.ASPxSpreadsheet;
using DevExpress.Web.ASPxHtmlEditor;
using DevExpress.Web.ASPxRichEdit;

namespace BsnCraft.Web.Common
{

    #region Misc Enums
    public enum PageSettings
    {
        None,
        ShowLeft,
        ShowRight,
        ShowHeader,
        ShowOptions,
        [Description("Show Notes")]
        ShowNotes,        
        ShowFilter,
        [Description("Show Preview")]
        ShowPreview,
        [Description("Show Amounts")]
        ShowAmounts,
        [Description("Hide Groups")]
        HideGroups,
        [Description("Show Images")]
        ShowImages,
        [Description("Show Pages")]
        ShowPages,
        [Description("Expand Notes")]
        ExpandNotes,
        [Description("Collapse Groups")]
        CollapseGroups,
        [Description("Advanced Mode")]
        Advanced,
        GridSize,
        ShowContacts,
        SelectedTab,
        SelectedGroup,
        PhotosGallery,
        HideImportant
    }
    
    public enum PageModes
    {
        Default,
        Grid,
        Edit,
        View,
        Lookup,
        Desktop,
        Contacts,
        Customers,
        List,
        Tabs,
        Answers,
        Photos,
        Notes,
        Deal,
        Setup
    }

    public enum ModalEvents
    {
        None,
        Show,
        Hide,
        Close
    }
    
    public enum PanelTypes
    {
        Bookmarks,
        Charts,
        Activities,
        Dashboards,
        Reports
    }

    public enum PanelZones
    {
        LeftZone,
        MainZone,
        RightZone
    }

    public enum BsnControls
    {
        None,
        Contract_Estimate,
        Contract_ChecklistMaint,
        Edit_Settings,
        Edit_Customer,
        Edit_Dashboard,
        Edit_DateSelect,
        Edit_Document,
        Edit_Heading,
        Edit_Paragraph,
        Edit_Estimate,
        Edit_Event,
        Edit_Lead,
        Edit_Contract,
        Edit_Chart,
        Edit_Report,
        Edit_Variation,
        Edit_Selection,
        Edit_VariationEvent,
        Edit_VariationDocument,
        Grid_Contacts,
        Grid_Customer,
        Grid_Customers,
        Grid_Dashboards,
        Grid_Documents,
        Grid_Headings,
        Grid_Events,
        Grid_DisplayInfo,
        Grid_JobInfo,
        Grid_Leads,
        Grid_Contracts,
        Grid_Charts,
        Grid_Reports,
        Grid_SalesCentres,
        Grid_Variations,
        Grid_Checklists,
        Grid_Estimate,
        Grid_Items,
        Grid_Users,
        Grid_Selections,
        Grid_VariationEvents,
        Grid_VariationDocuments,
        Grid_ChecklistHeader,
        Grid_ChecklistLines,
        Grid_ChecklistQuestions,
        Grid_DealSubmission,
        View_Document,
        View_Chart,
        View_Event,
        Grid_Jobs,
    }

    public enum CallbackType
    {
        Normal,
        PageOnly,
        WindowLocation
    }

    public enum CallbackMode
    {
        None,
        Logout,
        DefaultPage,
        Documentation,
        RefreshCache,
        Reset,
        PageSetting,
        UserSetting,
        SetTheme,
        CloseModal,
        RefreshModal,
        Edit,
        Save,
        View,
        SaveAndView,
        Update,
        Delete,
        ConfirmDelete,
        Lookup,
        Bookmark,
        Desktop,
        Cancel,
        Open,        
        Select,
        Design,
        Tender,
        Date,
        Register,
        QuickRegister,
        UnRegister,
        Filter,
        ViewFile,
        ExportGrid,
        Copy,
        RunReport,
        ViewReport,
        SelectReport,
        PreviewSelect,
        PreviewSave,
        QuickSave,
        Document,
        Refresh,
        Recalc,
        EditField,
        Accept,
        CopySelect,
        CopyPerform,
        AltEdit,
        AltSave,
    }

    public enum ModalSizes
    {
        Small,
        Normal,
        Large,
        Fullscreen
    }

    public enum DataSources
    {
        LeadsBySalesCentre,
        LeadsByOperatingCentre,
        LeadsBySalesConsultant,
        ContractsByOperatingCentre
    }

    public enum ChartTypes
    {
        Pie,
        Doughnut,
        Bar,        
        Area,
        Line
    }

    public enum EstimateTypes
    {
        Sales,
        Production,
        Variation,
        EmptySales
    }
    #endregion

    public class Utils
    {
        public const string PageModeKey = "PageMode";

        #region Constructor
        Utils()
        {
            //Ensure publish delivers DLLs
            var dxGrid = new BootstrapGridView();
            var dxChrt = new BootstrapPieChart();
            var dxSers = new BootstrapPieChartSeries();
            var dxDash = new ASPxDashboard();
            var dxView = new ASPxDashboardViewer();
            var dxDesn = new ASPxReportDesigner();
            var dxDocv = new ASPxWebDocumentViewer();
            var dxSprd = new ASPxSpreadsheet();
            var dxHtml = new ASPxHtmlEditor();
            var dxRich = new ASPxRichEdit();
        }
        #endregion

        //Leave without region for debug attach
        public static void SetError(string value)
        {
            SetAlert(value, true, false, false);
        }

        public static void SetError(string value, bool displayError)
        {
            SetAlert(value, true, false, displayError);
        }

        #region SetSuccess
        public static void SetSuccess()
        {
            SetSuccess("Saved");
        }

        public static void SetSuccess(string value)
        {
            SetAlert(value, false, false, false);
        }
        #endregion

        #region SetAlert
        public static void SetAlert(string message, bool isError, bool stackTrace, bool displayError)
        {
            if (string.IsNullOrEmpty(message))
                return;

            if (isError)
            {
                if (stackTrace)
                {
                    Log.Debug(Environment.StackTrace);
                }                
                Log.Debug(message);
            }
            var icon = (isError) ? "fal fa-times" : "fal fa-check-circle";
            var type = (isError) ? "danger" : "success";
            var align = (isError) ? "center" : "center";
            if (isError && !Settings.Comp.ShowFullError && !displayError)
            {
                message = "An error occurred. Please try again.";
            }
            BsnAlerts.Add(new BsnAlert() { Message = message, Icon = icon, Type = type, Align = align });
        }
        #endregion

        #region BsnAlerts
        public static string GetAlerts
        {
            get
            {
                return JsonConvert.SerializeObject(BsnAlerts);
            }
        }

        public static List<BsnAlert> BsnAlerts
        {
            get
            {
                var alerts = (List<BsnAlert>)VarUtils.Get(WebVars.Alerts);
                if (alerts == null)
                {
                    alerts = new List<BsnAlert>();
                    VarUtils.Set(WebVars.Alerts, alerts);
                }
                return alerts;
            }
        }
        #endregion

        #region ActiveFilters
        public static List<PageFilters> ActiveFilters
        {
            get
            {
                var activeFilters = (List<PageFilters>)VarUtils.Get(WebVars.PageFilter);
                if (activeFilters == null)
                {
                    activeFilters = new List<PageFilters>();
                    VarUtils.Set(WebVars.PageFilter, activeFilters);
                }
                return activeFilters;
            }
        }
        #endregion

        #region BsnLogo
        public static HtmlGenericControl BsnLogo(string theme)
        {
            HttpBrowserCapabilities browser = CurrentPage.Request.Browser;
            var isMSIE = false; // (browser.Browser == "InternetExplorer");

            var bsnLogo = (HtmlGenericControl)VarUtils.Get(WebVars.BsnLogo);
            if (bsnLogo == null)
            {
                if (isMSIE)
                {
                    bsnLogo = new HtmlGenericControl("img");
                    bsnLogo.Attributes["class"] = "bc-logo";
                    bsnLogo.Attributes["src"] = ResolveUrl("~/Content/bclogo.png");
                    bsnLogo.Attributes["alt"] = "BusinessCraft";
                }
                else
                {
                    if (string.IsNullOrEmpty(theme))
                    {
                        bsnLogo = new HtmlGenericControl("img");
                        bsnLogo.Attributes["class"] = "bc-logo";
                        bsnLogo.Attributes["src"] = ResolveUrl("~/Content/bclogo.svg");
                        bsnLogo.Attributes["alt"] = "BusinessCraft";
                    }
                    else
                    {
                        bsnLogo = new HtmlGenericControl("svg");
                        bsnLogo.Attributes["xmlns"] = "http://www.w3.org/2000/svg";
                        bsnLogo.Attributes["preserveAspectRatio"] = "xMidYMid";
                        bsnLogo.Attributes["viewBox"] = "0 0 640 120";
                        bsnLogo.Attributes["class"] = "bc-logo";
                        bsnLogo.Attributes["role"] = "img";
                        //B
                        var txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M37.786,102.093 C41.266,102.573 46.786,103.053 53.986,103.053 C67.185,103.053 76.305,100.653 81.944,95.494 C86.024,91.534 88.784,86.254 88.784,79.294 C88.784,67.295 79.785,60.935 72.105,59.015 L72.105,58.775 C80.625,55.655 85.784,48.816 85.784,41.016 C85.784,34.656 83.264,29.856 79.065,26.736 C74.025,22.657 67.305,20.857 56.866,20.857 C49.546,20.857 42.346,21.577 37.786,22.537 L37.786,102.093 ZM48.226,29.616 C49.906,29.256 52.666,28.896 57.465,28.896 C68.025,28.896 75.225,32.616 75.225,42.096 C75.225,49.895 68.745,55.655 57.705,55.655 L48.226,55.655 L48.226,29.616 ZM48.226,63.575 L56.866,63.575 C68.265,63.575 77.745,68.135 77.745,79.174 C77.745,90.934 67.785,94.894 56.986,94.894 C53.266,94.894 50.266,94.774 48.226,94.414 L48.226,63.575 Z";
                        txt.Attributes["class"] = "bc-txt-1";
                        bsnLogo.Controls.Add(txt);
                        //u
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M143.008,43.888 L132.448,43.888 L132.448,79.526 C132.448,81.446 132.088,83.366 131.488,84.926 C129.568,89.606 124.649,94.526 117.569,94.526 C107.969,94.526 104.609,87.086 104.609,76.047 L104.609,43.888 L94.050,43.888 L94.050,77.847 C94.050,98.246 104.969,103.285 114.089,103.285 C124.409,103.285 130.528,97.166 133.288,92.486 L133.528,92.486 L134.128,101.966 L143.488,101.966 C143.128,97.406 143.008,92.126 143.008,86.126 L143.008,43.888 Z";
                        txt.Attributes["class"] = "bc-txt-1";
                        bsnLogo.Controls.Add(txt);
                        //s
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M148.480,98.840 C152.560,101.240 158.439,102.800 164.799,102.800 C178.599,102.800 186.518,95.600 186.518,85.401 C186.518,76.761 181.358,71.721 171.279,67.881 C163.719,65.001 160.239,62.842 160.239,58.042 C160.239,53.722 163.719,50.122 169.959,50.122 C175.359,50.122 179.559,52.042 181.838,53.482 L184.478,45.802 C181.238,43.882 176.079,42.202 170.199,42.202 C157.719,42.202 150.160,49.882 150.160,59.242 C150.160,66.201 155.080,71.961 165.519,75.681 C173.319,78.561 176.319,81.321 176.319,86.361 C176.319,91.160 172.719,95.000 165.039,95.000 C159.759,95.000 154.240,92.840 151.120,90.800 L148.480,98.840 Z";
                        txt.Attributes["class"] = "bc-txt-1";
                        bsnLogo.Controls.Add(txt);
                        //i
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M201.919,102.000 L201.919,43.922 L191.360,43.922 L191.360,102.000 L201.919,102.000 ZM196.639,21.003 C192.800,21.003 190.040,23.883 190.040,27.603 C190.040,31.203 192.680,34.083 196.399,34.083 C200.599,34.083 203.239,31.203 203.119,27.603 C203.119,23.883 200.599,21.003 196.639,21.003 Z";
                        txt.Attributes["class"] = "bc-txt-1";
                        bsnLogo.Controls.Add(txt);
                        //n
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M210.960,101.800 L221.519,101.800 L221.519,66.881 C221.519,65.081 221.759,63.282 222.239,61.962 C224.039,56.082 229.439,51.162 236.399,51.162 C246.358,51.162 249.838,58.962 249.838,68.321 L249.838,101.800 L260.398,101.800 L260.398,67.121 C260.398,47.202 247.918,42.402 239.878,42.402 C230.279,42.402 223.559,47.802 220.679,53.322 L220.439,53.322 L219.839,43.722 L210.480,43.722 C210.840,48.522 210.960,53.442 210.960,59.442 L210.960,101.800 Z";
                        txt.Attributes["class"] = "bc-txt-1";
                        bsnLogo.Controls.Add(txt);
                        //e
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M316.557,75.327 C316.677,74.247 316.917,72.567 316.917,70.407 C316.917,59.727 311.877,43.048 292.918,43.048 C275.999,43.048 265.679,56.847 265.679,74.367 C265.679,91.886 276.359,103.645 294.238,103.645 C303.478,103.645 309.837,101.726 313.557,100.046 L311.757,92.486 C307.798,94.166 303.238,95.486 295.678,95.486 C285.118,95.486 275.999,89.606 275.759,75.327 L316.557,75.327 ZM275.879,67.767 C276.719,60.447 281.399,50.608 292.078,50.608 C303.958,50.608 306.838,61.047 306.718,67.767 L275.879,67.767 Z";
                        txt.Attributes["class"] = "bc-txt-1";
                        bsnLogo.Controls.Add(txt);
                        //s1
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M316.730,99.193 C320.810,101.593 326.690,103.153 333.049,103.153 C346.849,103.153 354.768,95.953 354.768,85.753 C354.768,77.114 349.609,72.074 339.529,68.234 C331.969,65.354 328.490,63.194 328.490,58.394 C328.490,54.075 331.969,50.475 338.209,50.475 C343.609,50.475 347.809,52.395 350.089,53.835 L352.729,46.155 C349.489,44.235 344.329,42.555 338.449,42.555 C325.970,42.555 318.410,50.235 318.410,59.594 C318.410,66.554 323.330,72.314 333.769,76.034 C341.569,78.914 344.569,81.673 344.569,86.713 C344.569,91.513 340.969,95.353 333.289,95.353 C328.010,95.353 322.490,93.193 319.370,91.153 L316.730,99.193 Z";
                        txt.Attributes["class"] = "bc-txt-1";
                        bsnLogo.Controls.Add(txt);
                        //s2
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M355.730,99.193 C359.810,101.593 365.690,103.153 372.049,103.153 C385.849,103.153 393.768,95.953 393.768,85.753 C393.768,77.114 388.609,72.074 378.529,68.234 C370.969,65.354 367.490,63.194 367.490,58.394 C367.490,54.075 370.969,50.475 377.209,50.475 C382.609,50.475 386.809,52.395 389.089,53.835 L391.729,46.155 C388.489,44.235 383.329,42.555 377.449,42.555 C364.970,42.555 357.410,50.235 357.410,59.594 C357.410,66.554 362.330,72.314 372.769,76.034 C380.569,78.914 383.569,81.673 383.569,86.713 C383.569,91.513 379.969,95.353 372.289,95.353 C367.010,95.353 361.490,93.193 358.370,91.153 L355.730,99.193 Z";
                        txt.Attributes["class"] = "bc-txt-1";
                        bsnLogo.Controls.Add(txt);
                        //C
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M454.026,90.753 C449.826,92.793 443.346,94.113 436.986,94.113 C417.307,94.113 405.907,81.394 405.907,61.595 C405.907,40.355 418.507,28.236 437.466,28.236 C444.186,28.236 449.826,29.676 453.786,31.596 L456.305,23.076 C453.546,21.636 447.186,19.476 437.106,19.476 C412.027,19.476 394.868,36.636 394.868,61.955 C394.868,88.474 412.027,102.873 434.826,102.873 C444.666,102.873 452.346,100.953 456.185,99.033 L454.026,90.753 Z";
                        txt.Attributes["class"] = "bc-txt-2";
                        bsnLogo.Controls.Add(txt);
                        //r
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M460.224,102.374 L470.664,102.374 L470.664,71.416 C470.664,69.616 470.904,67.936 471.144,66.496 C472.584,58.576 477.863,52.936 485.303,52.936 C486.743,52.936 487.823,53.056 488.903,53.296 L488.903,43.337 C487.943,43.097 487.103,42.977 485.903,42.977 C478.823,42.977 472.464,47.897 469.824,55.696 L469.344,55.696 L468.984,44.297 L459.744,44.297 C460.104,49.697 460.224,55.576 460.224,62.416 L460.224,102.374 Z";
                        txt.Attributes["class"] = "bc-txt-2";
                        bsnLogo.Controls.Add(txt);
                        //a
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M532.438,101.692 C531.718,97.732 531.478,92.812 531.478,87.773 L531.478,66.053 C531.478,54.414 527.159,42.294 509.399,42.294 C502.080,42.294 495.120,44.334 490.320,47.454 L492.720,54.414 C496.800,51.774 502.440,50.094 507.839,50.094 C519.719,50.094 521.039,58.734 521.039,63.533 L521.039,64.733 C498.600,64.613 486.120,72.293 486.120,86.333 C486.120,94.732 492.120,103.012 503.880,103.012 C512.159,103.012 518.399,98.932 521.639,94.372 L521.999,94.372 L522.839,101.692 L532.438,101.692 ZM521.279,82.133 C521.279,83.213 521.039,84.413 520.679,85.493 C518.999,90.412 514.199,95.212 506.639,95.212 C501.240,95.212 496.680,91.972 496.680,85.133 C496.680,73.853 509.759,71.813 521.279,72.053 L521.279,82.133 Z";
                        txt.Attributes["class"] = "bc-txt-2";
                        bsnLogo.Controls.Add(txt);
                        //f
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M551.698,101.962 L551.698,51.924 L565.737,51.924 L565.737,43.884 L551.698,43.884 L551.698,40.765 C551.698,31.885 553.977,23.965 562.977,23.965 C565.977,23.965 568.137,24.565 569.697,25.285 L571.137,17.126 C569.097,16.286 565.857,15.446 562.137,15.446 C557.217,15.446 551.938,17.006 547.978,20.845 C543.058,25.525 541.258,32.965 541.258,41.125 L541.258,43.884 L533.098,43.884 L533.098,51.924 L541.258,51.924 L541.258,101.962 L551.698,101.962 Z";
                        txt.Attributes["class"] = "bc-txt-2";
                        bsnLogo.Controls.Add(txt);
                        //t
                        txt = new HtmlGenericControl("path");
                        txt.Attributes["d"] = "M576.335,29.704 L576.335,43.624 L567.336,43.624 L567.336,51.663 L576.335,51.663 L576.335,83.342 C576.335,90.182 577.415,95.342 580.415,98.462 C582.935,101.341 586.895,102.901 591.815,102.901 C595.894,102.901 599.134,102.301 601.174,101.461 L600.694,93.542 C599.374,93.902 597.334,94.262 594.575,94.262 C588.695,94.262 586.655,90.182 586.655,82.982 L586.655,51.663 L601.774,51.663 L601.774,43.624 L586.655,43.624 L586.655,26.944 L576.335,29.704 Z";
                        txt.Attributes["class"] = "bc-txt-2";
                        bsnLogo.Controls.Add(txt);
                    }
                }
                VarUtils.Set(WebVars.BsnLogo, bsnLogo);
            }
            return bsnLogo;
        }
        #endregion
        
        #region Folders
        public static string HomeFolder
        {
            get
            {
                string homeFolder = ResolveUrl("~/home/" + BsnUtils.BsnCompany + "/");
                try
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(homeFolder)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(homeFolder));
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return homeFolder;
            }
        }

        public static string HelpFolder
        {
            get
            {
                string helpFolder = ResolveUrl("~/home/Documentation/");
                try
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(helpFolder)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(helpFolder));
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return helpFolder;
            }
        }

        public static string DocsFolder
        {
            get
            {
                string docsFolder = HomeFolder + "documents/";
                try
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(docsFolder)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(docsFolder));
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return docsFolder;
            }
        }

        public static string UploadFolder
        {
            get
            {
                string uploadFolder = HomeFolder + "upload/";
                try
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(uploadFolder)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(uploadFolder));
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return uploadFolder;
            }
        }

        public static string DownloadFolder
        {
            get
            {
                string downloadFolder = HomeFolder + "download/";
                try
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(downloadFolder)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(downloadFolder));
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return downloadFolder;
            }
        }

        public static string ImagesFolder
        {
            get
            {
                string imagesFolder = HomeFolder + "images/";
                try
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(imagesFolder)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(imagesFolder));
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return imagesFolder;
            }
        }
        #endregion

        #region Settings        
        public static WebSettings Settings
        {
            get
            {                                
                var settings = (WebSettings)VarUtils.Get(WebVars.WebSettings);
                if (settings != null)
                {
                    return settings;                    
                }

                settings = new WebSettings
                {
                    Comp = new CompanySettings(),
                    User = new UserSettings()
                };

                if (settings.App == null)
                {
                    try
                    {
                        var settingsFile = HttpContext.Current.Server.MapPath(@"~/home/Settings.json");
                        var jsonSettings = File.ReadAllText(settingsFile);
                        var appSettings = (AppSettings)JsonConvert.DeserializeObject(jsonSettings, typeof(AppSettings));
                        settings.App = appSettings;
                    }
                    catch (Exception e)
                    {
                        LogExc(e);
                    }
                }

                if (VarUtils.Str(WebVars.Connected) != "Y")
                {
                    return settings;
                }

                try
                {
                    var jsonSettings = BsnUtils.GetSettings(SettingsLevel.Company);
                    if (string.IsNullOrEmpty(jsonSettings))
                    {
                        var settingsFile = HttpContext.Current.Server.MapPath(HomeFolder + "Settings.json");
                        if (!File.Exists(settingsFile))
                        {
                            File.WriteAllText(settingsFile, JsonConvert.SerializeObject(Settings.Comp));
                        }
                        jsonSettings = File.ReadAllText(settingsFile);
                    }
                    settings.Comp = (CompanySettings)JsonConvert.DeserializeObject(jsonSettings, typeof(CompanySettings));


                    jsonSettings = BsnUtils.GetSettings(SettingsLevel.User);
                    if (!string.IsNullOrEmpty(jsonSettings))
                    {
                        settings.User = (UserSettings)JsonConvert.DeserializeObject(jsonSettings, typeof(UserSettings));
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                
                VarUtils.Set(WebVars.WebSettings, settings);
                return settings;
            }
        }
        #endregion

        #region SaveSettings
        public static void SaveSettings(bool UserSettings = true)
        {
            if (UserSettings)
            {
                BsnUtils.SaveSettings(SettingsLevel.User, string.Empty, JsonConvert.SerializeObject(Utils.Settings.User));
            }
            else
            { 
                BsnUtils.SaveSettings(SettingsLevel.Company, string.Empty, JsonConvert.SerializeObject(Utils.Settings.Comp));
            }
        }
        #endregion

        #region PageTitle
        public static string PageTitle
        {
            get
            {
                string caption = string.Empty;
                if (BsnUtils.Connected)
                {
                    string url = CurrentPage.Request.Url.LocalPath.ToLower().Replace("/default.aspx", "/");
                    if (!url.EndsWith(".aspx") && !url.EndsWith("/"))
                    {
                        url += "/";
                    }

                    foreach (var item in MainMenu)
                    {
                        if (item.ResolveUrl.ToLower() == url.ToLower())
                        {
                            if (item.Parent != null)
                            {
                                caption += item.Parent.Caption + ": ";
                            }
                            caption += item.Caption;
                            break;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(caption))
                {
                    caption += " - ";
                }
                caption += "BusinessCraft";
                return caption;
            }
        }
        #endregion

        #region Helpers
        public static string ResolveUrl(string url)
        {
            return (HttpContext.Current.Handler as Page).ResolveUrl(url); ;
        }
        
        public static string Home
        {
            get
            {
                return GetMenu(Menus.Home).ResolveUrl;
            }
        }

        public static void RedirectToMenu(Menus Menu, bool IsCallback = false, string Extras = "")
        {
            if (!IsCallback)
            {
                CurrentPage.Response.Redirect(Utils.GetMenu(Menu).ResolveUrl + Extras);
            }
            else
            {
                ASPxWebControl.RedirectOnCallback(Utils.GetMenu(Menu).ResolveUrl + Extras);
            }
        }

        public static BsnPage CurrentPage
        {
            get { return HttpContext.Current.CurrentHandler as BsnPage; }
        }

        public static string EnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static EstimateTypes EstimateType(string type)
        {
            EstimateTypes headingType = EstimateTypes.Sales;
            try
            {
                headingType = (EstimateTypes)Enum.Parse(typeof(EstimateTypes), type);
            }
            catch { }
            return headingType;
        }

        public static PageModes PageMode(string mode)
        {
            PageModes pageMode = PageModes.Default;
            try
            {
                pageMode = (PageModes)Enum.Parse(typeof(PageModes), mode);
            }
            catch { }
            return pageMode;
        }

        public static BsnControls BsnControl(string controlId)
        {
            BsnControls bsnControl = BsnControls.None;
            try
            {
                bsnControl = (BsnControls)Enum.Parse(typeof(BsnControls), controlId);
            }
            catch { }
            return bsnControl;
        }

        public static bool IsImage(string Filename)
        {
            var extension = Path.GetExtension(Filename).ToUpper().Replace(".", "");
            switch (extension)
            {
                case "JPG":
                case "JPEG":
                case "GIF":
                case "PNG":
                case "BMP":
                    return true;
            }
            return false;
        }
        
        public static string StringDate(DateTime date)
        {
            return StringDate(date, false);
        }

        public static string StringDate(DateTime date, bool friendly)
        {
            if (date == DateTime.MinValue)
                return string.Empty;

            if (friendly)
            {
                return date.ToString("D");
            }

            return date.ToString("dd/MM/yyyy");
        }

        public static string JulianDate(DateTime date)
        {
            return date.Year.ToString() + date.DayOfYear.ToString("000");
        }

        public static string GetFilterString(string field, string[] values, int length = 2, bool where = false)
        {
            string filters = string.Empty;
            if (values.Length < length)
            {
                return filters;
            }

            filters += (where) ? " WHERE" : " AND";
            filters += " " + field + " IN (";

            foreach (var fieldvalue in values)
            {
                filters += "'" + fieldvalue.TrimStart('[').TrimEnd(']') + "',";
            }

            filters = filters.TrimEnd(',');

            filters += ")";

            return filters;
        }

        public static string RemoveDefaultFormat(string code)
        {
            return code.TrimStart('[').TrimEnd(']');
        }

        public static string GetDefault(string[] values)
        {
            foreach (var fieldvalue in values)
            {
                if (fieldvalue.StartsWith("[") && fieldvalue.EndsWith("]"))
                {
                    return fieldvalue.TrimStart('[').TrimEnd(']');
                }
            }
            return string.Empty;
        }

        //Used by BsnButton
        public static string WindowLocation(string location)
        {
            return "window.location.href = '" + location + "';";
        }

        public static string WindowLocation(Menus menu)
        {
            return WindowLocation(menu, string.Empty, string.Empty);
        }

        public static string WindowLocation(Menus menu, string extras)
        {
            return WindowLocation(menu, extras, string.Empty);
        }

        public static string WindowLocation(Menus menu, string extras, string javascript)
        {
            string url = GetMenu(menu).ResolveUrl;
            string location = "'" + url + extras + "'" + javascript;
            return "window.location.href = " + location + ";";
        }

        //Used by DevExpress
        public static string ClientRedirect(Menus menu, string extras)
        {
            return ClientRedirect(menu, extras, string.Empty);
        }

        public static string ClientRedirect(Menus menu, string extras, string javascript)
        {
            string url = "function(s,e) { window.location.href = '";
            url += GetMenu(menu).ResolveUrl;
            if (!string.IsNullOrEmpty(extras))
            {
                url += extras;
            }
            url += "'";
            if (!string.IsNullOrEmpty(javascript))
            {
                url += javascript;
            }
            url += ";}";
            return url;
        }

        public static string CheckValue(object value)
        {

            return ((bool)value) ? "checked" : string.Empty;
        }
        
        public static string ComboValue(BootstrapComboBox combo, bool uppercase = true)
        {
            string value = string.Empty;
            try
            {
                if (combo.Value != null)
                {
                    value = combo.Value.ToString();
                    if (uppercase)
                    {
                        value = value.ToUpper();
                    }
                }                
            }
            catch { }

            return value;
        }

        public static string CleanString(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9]", "");
        }

        public static string CleanSpec(string spec)
        {
            return spec.Replace(((char)254).ToString(), string.Empty)
                .Replace(((char)255).ToString(), string.Empty);
        }

        public static string CleanSpec(string spec, string linebreak)
        {
            return spec.Replace(((char)254).ToString(), string.Empty)
                .Replace(((char)255).ToString(), linebreak);
        }

        public static string GetArray(string[] values, int index)
        {
            return (values.Length > index) ? values[index] : string.Empty;
        }

        public static IDictionary<int, string> GetEnums<TEnum>() where TEnum : struct
        {
            var enumerationType = typeof(TEnum);

            if (!enumerationType.IsEnum)
                throw new ArgumentException("Enumeration type is expected.");

            var dictionary = new Dictionary<int, string>();

            foreach (int value in Enum.GetValues(enumerationType))
            {
                var name = Enum.GetName(enumerationType, value);
                dictionary.Add(value, name);
            }

            return dictionary;
        }
       
        public static string AddQuery(string url, string query)
        {
            if (string.IsNullOrEmpty(url)) {
                url = CurrentPage.Request.Url.PathAndQuery;
            }
            return url + (url.Contains("?") ? "&" : "?") + query;
        }

        public static void InitText(ASPxTextBoxBase field, string value, string validation)
        {
            field.Text = value;
            field.ValidationSettings.ValidationGroup = validation;            
        }

        public static void InitDate(BootstrapDateEdit field, DateTime value, string validation)
        {
            field.Date = value;
            field.ValidationSettings.ValidationGroup = validation;
        }

        public static void InitMemo(BootstrapMemo field, string value, string validation)
        {
            field.Text = value;
            field.ValidationSettings.ValidationGroup = validation;
        }

        public static void InitCheckBox(BootstrapCheckBox field, bool value, string validation)
        {
            field.Checked = value;
            field.ValidationSettings.ValidationGroup = validation;
        }

        public static void InitCombo(string name, BootstrapComboBox field, IDictionary<int, string> list, string value, string validation)
        {
            int index = -1;
            field.ValidationSettings.ValidationGroup = validation;

            if (!string.IsNullOrEmpty(value))
            {
                index = list.Where(p => p.Value == value).FirstOrDefault().Key;
                if (index < 0)
                {
                    SetError(name + ": " + value + " not found.");
                }
            }

            field.SelectedIndex = index;
        }

        public static void InitCombo(string name, BootstrapComboBox field, List<ComboItem> list, string value, string validation)
        {
            field.ValidationSettings.ValidationGroup = validation;            
            int index = -1;

            if (!string.IsNullOrEmpty(value))
            {
                index = list.FindIndex(p => p.Code == value);
                if (index < 0)
                {
                    SetError(name + ": " + value + " not found.");
                }
            }

            field.SelectedIndex = index;            
        }
        #endregion

        #region LogExc
        private static Logger Log = LogManager.GetCurrentClassLogger();
        public static void LogExc(Exception ex)
        {
            LogExc(ex, false);
        }

        public static void LogExc(Exception ex, bool quiet)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            if (!quiet)
            {
                SetAlert(ex.Message, true, true, false);
            }
            else
            {
                Log.Debug(message);
            }
        } 
        #endregion

        #region MainMenu
        public static List<WebMenu> MainMenu
        {
            get
            {
                var mainMenu = (List<WebMenu>)VarUtils.Get(WebVars.WebMenu);
                if (mainMenu == null)
                {
                    mainMenu = new List<WebMenu>();
                    WebMenu home = new WebMenu(Menus.Home, "Home", "~/", "home");
                    mainMenu.Add(home);
                }
                return mainMenu;
            }
        }
        #endregion

        #region GetMenu
        public static WebMenu GetMenu(Menus menu)
        {
            if (MainMenu.Count == 1)
                return MainMenu[0];

            foreach (var item in MainMenu)
            {
                if (item.Menu == menu)
                    return item;
            }
            Log.Debug("GetMenu| menu: " + menu.ToString() + " not found.");
            return CurrentMenu;
        }
        #endregion

        #region CurrentMenu
        public static WebMenu CurrentMenu
        {
            get
            {
                string url = CurrentPage.Request.Url.LocalPath.ToLower().Replace("/default.aspx", "/");
                if (!url.EndsWith(".aspx") && !url.EndsWith("/"))
                {
                    url += "/";
                }

                foreach (var item in MainMenu)
                {
                    if (item.ResolveUrl.ToLower() == url.ToLower())
                        return item;
                }
                Log.Debug("CurrentMenu| url: " + url + " not found.");
                return GetMenu(Menus.Home);
            }
        }
        #endregion

        #region Show Menu
        public static bool ShowFinance
        {
            get
            {
                return MenuVisible(Menus.Finance.ToString());
            }
        }

        public static bool ShowDocuments
        {
            get
            {
                return MenuVisible(Menus.Documents.ToString());
            }
        }

        public static bool ShowActivities
        {
            get
            {
                return MenuVisible(Menus.Activities.ToString());
            }
        }

        public static bool ShowJobs
        {
            get
            {
                return MenuVisible(Menus.Jobs.ToString());
            }
        }

        public static bool ShowLeads
        {
            get
            {
                if (!MenuVisible(Menus.Leads.ToString()))
                    return false;

                if (Settings.Comp.ActiveLeadStatusList.Length != 1)
                    return true;

                return (!string.IsNullOrEmpty(Settings.Comp.ActiveLeadStatusList[0]));
            }
        }

        public static bool ShowContracts
        {
            get
            {
                if (!MenuVisible(Menus.Contracts.ToString()))
                    return false;

                if (Settings.Comp.ActiveContractStatusList.Length != 1)
                    return true;

                return (!string.IsNullOrEmpty(Settings.Comp.ActiveContractStatusList[0]));
            }
        }

        public static bool ShowDisplays
        {
            get
            {
                return MenuVisible(Menus.DisplayCentres.ToString()) && MenuVisible("Displays"); 
            }
        }

        public static bool ShowCharts
        {
            get
            {
                return MenuVisible(Menus.Charts.ToString());
            }
        }

        public static bool ShowReports
        {
            get
            {
                return MenuVisible(Menus.Reports.ToString());
            }
        }

        public static bool ShowDashboards
        {
            get
            {
                return MenuVisible(Menus.Dashboards.ToString());
            }
        }
        #endregion

        #region MenuVisible
        public static bool MenuVisible(string menuName)
        {
            bool menuVisible = true;
            if (Settings.Comp.HideMainMenu.Contains(menuName))
            {
                menuVisible = false;
            }
            return menuVisible;
        }
        #endregion

        #region ModalContent
        public static HtmlGenericControl ModalContent(BootstrapCallbackPanel panel)
        {
            try
            {
                var content = (HtmlGenericControl)panel.FindControl("MDC");
                if (content == null)
                {
                    content = new HtmlGenericControl("div")
                    {
                        ID = "MDC"
                    };
                    content.Attributes["class"] = "modal-content";                    
                }
                         
                var dialog = (HtmlGenericControl)panel.FindControl("MD");
                if (dialog == null)
                {
                    dialog = new HtmlGenericControl("div")
                    {
                        ID = "MD"
                    };
                    dialog.Attributes["class"] = "modal-dialog";
                    dialog.Attributes["role"] = "document";
                    dialog.Controls.Add(content);
                }

                var modal = (HtmlGenericControl)panel.FindControl("MC");
                if (modal == null)
                {
                    modal = new HtmlGenericControl("div")
                    {
                        ID = "MC"
                    };
                    modal.Attributes["class"] = "modal bsn-modal fade";
                    modal.Attributes["tabindex"] = "-1";
                    modal.Attributes["role"] = "dialog";                    
                    modal.Controls.Add(dialog);
                    panel.Controls.Add(modal);
                }

                return content;
            }
            catch (Exception e)
            {
                LogExc(e);
            }
            return null;
        }
        #endregion
        
        #region ControlId
        public static string ControlId(BsnCallback Callback)
        {
            if (Callback == null || Callback.Id == BsnControls.None)
            {
                return string.Empty;
            }
            //Lookups should be handled before here
            return (Callback.Mode == CallbackMode.Lookup) ?
                    GridControl(Callback.Id) : 
                    EditControl(Callback.Id);
        }
        #endregion

        #region Load BsnControl
        public static BsnUserControl LoadBsnControl(BsnCallback Callback, BootstrapCallbackPanel panel)
        {
            string controlId = ControlId(Callback);
            if (string.IsNullOrEmpty(controlId))
                return null;

            try
            {
                var BsnControl = (BsnUserControl)panel.FindControl("BC");
                if (BsnControl == null || BsnControl.AppRelativeVirtualPath != controlId)
                {
                    if (BsnControl != null)
                    {
                        panel.Controls.Remove(BsnControl);
                        BsnControl = null;
                    }
                    BsnControl = LoadControl(controlId, "BC");
                }
                if (BsnControl != null)
                {
                    BsnControl.InitControl(Callback);
                }
                return BsnControl;
            }
            catch (Exception e)
            {
                LogExc(e);
            }
            return null;
        }
        #endregion
        
        #region JSProperties        
        public static void JSProperties(CustomJSPropertiesEventArgs e)
        {
            e.Properties["cpAlerts"] = GetAlerts;
            e.Properties["cpSearchUrl"] = ResolveUrl("~/Shared/Search.aspx");
            e.Properties["cpModalEvent"] = VarUtils.Str(WebVars.JSModalEvent);
            e.Properties["cpModalSize"] = VarUtils.Str(WebVars.JSModalSize);
            e.Properties["cpShowDelete"] = VarUtils.Str(WebVars.JSDeleteMessage);
            e.Properties["cpSetLocation"] = VarUtils.Str(WebVars.JSLocation);
            e.Properties["cpPageData"] = VarUtils.Str(WebVars.JSPageData);
            e.Properties["cpViewFile"] = VarUtils.Str(WebVars.JSViewFile);
            VarUtils.Set(WebVars.JSModalEvent, null);
            VarUtils.Set(WebVars.JSModalSize, null);
            VarUtils.Set(WebVars.JSDeleteMessage, null);
            VarUtils.Set(WebVars.JSLocation, null);
            VarUtils.Set(WebVars.JSPageData, null);
            VarUtils.Set(WebVars.JSViewFile, null);
        }
        #endregion

        #region ProcessCallback
        public static bool ProcessCallback(BsnCallback Callback, BootstrapCallbackPanel panel)
        {
            //Handled modes do not enter modal
            switch (Callback.Mode)
            {
                case CallbackMode.Select:
                    VarUtils.Set(WebVars.JSModalEvent, "hide");
                    VarUtils.Set(WebVars.ModalCallback, null);
                    return false;
                //Todo: These could be pageonly types
                case CallbackMode.Desktop:
                case CallbackMode.PreviewSelect:
                case CallbackMode.Filter:
                    break;
                case CallbackMode.RefreshCache:
                case CallbackMode.Reset:
                    VarUtils.Refresh();
                    break;
                case CallbackMode.Date:
                    //Date has been selected - could be refactored into a generic mode
                    VarUtils.Set(WebVars.JSModalEvent, "hide");
                    VarUtils.Set(WebVars.ModalCallback, null);
                    break;
                case CallbackMode.Bookmark:
                    switch (Callback.Id)
                    {
                        case BsnControls.Grid_Customers:
                            new CustomersViewModel().Bookmark(Callback.Key);
                            break;
                        case BsnControls.Grid_Contacts:
                        case BsnControls.Grid_Customer:
                            new CustomersViewModel().BookmarkContact(Callback.Key);
                            break;
                        case BsnControls.Grid_Leads:
                            new LeadsViewModel().Bookmark(Callback.Key);
                            break;
                        case BsnControls.Grid_Contracts:
                            new ContractsViewModel().Bookmark(Callback.Key);
                            break;
                        default:
                            return false;
                    }
                    break;
                case CallbackMode.ExportGrid:
                    VarUtils.Set(WebVars.ExportGrid, Callback.Key + ":" + Callback.Value);
                    break;
                case CallbackMode.ViewFile:
                    ViewFile(ResolveUrl(Callback.Value), "View", panel);
                    break;
                case CallbackMode.Documentation:
                    ViewFile(ResolveUrl(Callback.Value), "Help", panel);
                    break;
                case CallbackMode.ViewReport:
                    var pdf = ViewReport(Callback.Value);
                    ViewFile(pdf, "Report", panel);
                    break;
                case CallbackMode.RunReport:
                    var key = Callback.Key;
                    var reportParams = VarUtils.Str(WebVars.ReportParams);
                    string report = RunReport(Callback.Value, reportParams);
                    if (!string.IsNullOrEmpty(report))
                    {
                        ViewFile(report, "Report", panel);
                    }
                    else
                    {
                        VarUtils.Set(WebVars.JSModalEvent, "hide");
                    }
                    break;
                case CallbackMode.UserSetting:                    
                    //BsnUtils.SaveSettings(SettingsLevel.User, "WebSettings", JsonConvert.SerializeObject(settings.User));
                    break;
                default:
                    return false;
            }
            return true;
        }
        #endregion

        #region Process Modal        
        public static void ProcessModal(BsnUserControl BsnControl, BsnCallback Callback, BootstrapCallbackPanel panel)
        {
            switch (Callback.Mode)
            {
                case CallbackMode.CloseModal:
                    BsnControl.CloseControl();
                    return;
                case CallbackMode.RefreshModal:
                    //Not implemented 
                    //BsnControl.RefreshControl();
                    break;
                case CallbackMode.Lookup:
                    BsnControl.InitControl(Callback);
                    break;
                case CallbackMode.Edit:
                    BsnControl.Edit();
                    break;
                case CallbackMode.View:
                    BsnControl.View();
                    break;
                case CallbackMode.Save:
                    BsnControl.Save();
                    break;
                case CallbackMode.Delete:
                    string[] delete = new string[3];
                    delete[0] = BsnControl.ConfirmDelete();
                    delete[1] = BsnControl.Caption; //Above sets up caption
                    Callback.Mode = CallbackMode.ConfirmDelete;
                    delete[2] = Callback.ToString();
                    Callback.Mode = CallbackMode.Delete;
                    VarUtils.Set(WebVars.JSDeleteMessage, JsonConvert.SerializeObject(delete));
                    break;
                case CallbackMode.ConfirmDelete:
                    BsnControl.Delete();
                    break;
                default:
                    BsnControl.Process(Callback);
                    break;
            }

            if (BsnControl.ModalEvent != ModalEvents.None)
            {
                var modalEvent = BsnControl.ModalEvent.ToString().ToLower();
                VarUtils.Set(WebVars.JSModalEvent, modalEvent);
            }
            
            switch (BsnControl.ModalEvent)
            {
                case ModalEvents.Show:                    
                    var modal = ModalContent(panel);
                    if (modal != null)
                    {
                        modal.Controls.Add(BsnControl);
                        //Save the callback for grid paging, etc.
                        Log.Debug("ProcessModal| ModalCallback = " + Callback.ToString());
                        VarUtils.Set(WebVars.ModalCallback, Callback);
                    }
                    break;
                case ModalEvents.Hide:
                    VarUtils.Set(WebVars.ModalCallback, null);
                    break;
            }
            
            var modalSize = string.Empty;
            switch (BsnControl.Size)
            {
                case ModalSizes.Small:
                    modalSize = "modal-sm";
                    break;
                case ModalSizes.Large:
                    modalSize = "modal-lg";
                    break;
                case ModalSizes.Fullscreen:
                    modalSize = "modal-fullscreen";
                    break;
            }
            if (!string.IsNullOrEmpty(modalSize))
            {
                VarUtils.Set(WebVars.JSModalSize, modalSize);
            }
        }
        #endregion
        
        #region LoadControl
        public static BsnUserControl LoadControl(string virtualPath)
        {
            return LoadControl(virtualPath, string.Empty);
        }

        public static BsnUserControl LoadControl(string virtualPath, string id)
        {            
            try
            {
                var BsnControl = (BsnUserControl)CurrentPage.LoadControl(virtualPath);
                if (string.IsNullOrEmpty(id))
                {
                    id = virtualPath.Replace("~/Controls/", "");
                    id = id.Replace("/", "_").Replace(".ascx", "");
                }
                //Log.Debug("LoadControl id=" + id);
                BsnControl.ID = id;
                return BsnControl;
            }
            catch (Exception e)
            {
                LogExc(e);
            }
            return null;
        }
        #endregion

        #region FileViewer
        public static HtmlGenericControl FileViewer(string Filename, string MasterFile = "")
        {
            var fileViewer = new HtmlGenericControl("iframe");
            fileViewer.Attributes["style"] = "border:0; width:100%; height:100%;";

            var extension = Path.GetExtension(Filename).ToUpper().Replace(".", "");
            int.TryParse(extension, out int lineNumber);

            if (!string.IsNullOrEmpty(MasterFile) && lineNumber > 0)
            {        
                string oldFilename = HttpContext.Current.Server.MapPath(Filename);
                extension = Path.GetExtension(MasterFile).ToUpper().Replace(".", "");
                if (extension == "DOC")
                {
                    if (Settings.Comp.UseBsnMerge)
                    {
                        //RichEdit Expects docx from BsnMerge
                        Filename = Filename.Replace(".", "_") + ".docx";
                    }
                }
                else
                {
                    Filename = Filename.Replace(".", "_") + Path.GetExtension(MasterFile);
                }                

                string newFilename = HttpContext.Current.Server.MapPath(Filename);
                try
                {
                    if (File.Exists(newFilename))
                    {
                        File.Delete(newFilename);
                    }
                    File.Move(oldFilename, newFilename);
                }                        
                catch (Exception e)
                {
                    LogExc(e);
                }
            }

            Log.Trace("About to view: " + Filename);

            switch (extension)
            {
                case "PDF":
                    fileViewer.Attributes["src"] = ResolveUrl("~/Viewers/pdf/viewer.html?file=" + Filename);
                    if (Settings.Comp.ShowPDFTools)
                    {
                        var pdfWrapper = new HtmlGenericControl("div");
                        pdfWrapper.Attributes["style"] = "border:0; width:100%; height:100%;";
                        var pdfViewer = new HtmlGenericControl("div");
                        pdfViewer.Attributes["style"] = "border:0; width:100%; height:100%; padding-top: 30px;";

                        var pdfToolbar = new HtmlGenericControl("div");
                        pdfToolbar.Attributes["style"] = "position: absolute; text-align: right;";
                        var pdfTablet = new BsnButton("javascript:", "fal fa-tablet-alt", "no-btn-lg", ButtonTypes.Link);
                        pdfToolbar.Controls.Add(pdfTablet);
                        var pdfDesktop = new BsnButton("javascript:", "fal fa-desktop", "no-btn-lg", ButtonTypes.Link);
                        pdfToolbar.Controls.Add(pdfDesktop);

                        pdfViewer.Controls.Add(fileViewer);
                        pdfWrapper.Controls.Add(pdfToolbar);
                        pdfWrapper.Controls.Add(pdfViewer);

                        return pdfWrapper;
                    }                    
                    break;                    
                case "RTF":
                case "DOC":
                case "DOCX":
                    fileViewer.Attributes["src"] = ResolveUrl("~/Viewers/RichEdit.aspx?filename=" + Filename);
                    break;
                case "HTM":
                case "HTML":
                    fileViewer.Attributes["src"] = ResolveUrl("~/Viewers/HtmlEditor.aspx?filename=" + Filename);
                    break;
                case "CSV":
                case "XLS":
                case "XLSX":
                    fileViewer.Attributes["src"] = ResolveUrl("~/Viewers/Spreadsheet.aspx?filename=" + Filename);
                    break;
                default:                    
                    fileViewer.Attributes["src"] = Filename;
                    break;
            }
            return fileViewer;
        }
        #endregion

        #region ViewFile
        public static void ViewFile(string filename, string title, BootstrapCallbackPanel panel)
        {
            var modal = ModalContent(panel);
            if (modal != null)
            {
                modal.Controls.Add(ModalControl(FileViewer(filename), title));
                VarUtils.Set(WebVars.JSModalEvent, "show");
                VarUtils.Set(WebVars.JSModalSize, "modal-fullscreen");
            }
        }
        #endregion
        
        #region ModalControl
        public static HtmlGenericControl ModalControl(Control child, string title)
        {
            var content = new HtmlGenericControl("div");
            content.Attributes["class"] = "modal-content";

            var modalHead = new HtmlGenericControl("div");
            modalHead.Attributes["class"] = "modal-header";

            var timesButton = new HtmlGenericControl("button");
            timesButton.Attributes["type"] = "button";
            timesButton.Attributes["class"] = "close";
            timesButton.Attributes["data-dismiss"] = "modal";
            timesButton.Attributes["aria-label"] = "Close";

            var closeSpan = new HtmlGenericControl("span");
            closeSpan.Attributes["aria-hidden"] = "true";
            closeSpan.Controls.Add(new LiteralControl("&nbsp; &times;"));
            timesButton.Controls.Add(closeSpan);
            modalHead.Controls.Add(timesButton);

            var plusButton = new HtmlGenericControl("button");
            plusButton.Attributes["type"] = "button";
            plusButton.Attributes["onclick"] = "bsn.toggleModal();";
            plusButton.Attributes["class"] = "close";
            plusButton.Attributes["aria-label"] = "Maximize";

            var plusSpan = new HtmlGenericControl("span");
            plusSpan.Attributes["aria-hidden"] = "true";
            plusSpan.Controls.Add(new LiteralControl("&plus;"));
            plusButton.Controls.Add(plusSpan);
            modalHead.Controls.Add(plusButton);

            var titleHead = new HtmlGenericControl("h4");
            titleHead.Attributes["class"] = "modal-title text-primary";
            titleHead.Controls.Add(new LiteralControl(title));
            modalHead.Controls.Add(titleHead);
            content.Controls.Add(modalHead);

            var modalBody = new HtmlGenericControl("div");
            modalBody.Attributes["class"] = "modal-body";
            modalBody.Controls.Add(child);
            content.Controls.Add(modalBody);

            var modalFooter = new HtmlGenericControl("div");
            modalFooter.Attributes["class"] = "modal-footer";
            var closeButton = new HtmlGenericControl("button");
            closeButton.Attributes["type"] = "button";
            closeButton.Attributes["class"] = "btn btn-default";
            closeButton.Attributes["data-dismiss"] = "modal";
            closeButton.Controls.Add(new LiteralControl("Close"));
            modalFooter.Controls.Add(closeButton);
            content.Controls.Add(modalFooter);

            return content;
        }
        #endregion

        #region TabControl
        public static HtmlGenericControl TabControl(string id, string caption, bool active)
        {
            var tabControl = new HtmlGenericControl("li");
            tabControl.Attributes["role"] = "presentation";
            if (active)
            {
                tabControl.Attributes["class"] = "active";
            }
            var href = new HtmlGenericControl("a");
            href.Attributes["href"] = "#" + id;
            href.Attributes["aria-controls"] = id;
            href.Attributes["role"] = "tab";
            href.Attributes["data-toggle"] = "tab";

            href.Controls.Add(new LiteralControl(caption));

            tabControl.Controls.Add(href);
            return tabControl;
        }
        #endregion

        #region DropDownControl
        public static HtmlGenericControl DropDownControl(string caption, string icon, string cssclass, List<BsnButton> buttons, bool inTable, bool isRight)
        {
            var dropDownControl = new HtmlGenericControl("div");
            dropDownControl.Attributes["class"] = "button-group";
            
            var href = new HtmlGenericControl("a");
            href.Attributes["href"] = "#";
            href.Attributes["class"] = "btn ";
            href.Attributes["class"] += (!string.IsNullOrEmpty(cssclass)) ? cssclass : "btn-default";
            href.Attributes["class"] += " dropdown-toggle";
            href.Attributes["aria-expanded"] = "false";
            href.Attributes["data-toggle"] = "dropdown";

            if (!string.IsNullOrEmpty(icon))
            {
                var span = new HtmlGenericControl("span");
                span.Attributes["class"] = "fa " + icon + " fa-fw";
                span.Attributes["aria-hidden"] = "true";
                href.Controls.Add(span);
            }

            if (!string.IsNullOrEmpty(caption))
            {
                href.Controls.Add(new LiteralControl(caption));
            }

            var caret = new HtmlGenericControl("span");
            caret.Attributes["class"] = "caret";
            href.Controls.Add(caret);

            dropDownControl.Controls.Add(href);

            var dropDownMenu = new HtmlGenericControl("ul");
            dropDownMenu.Attributes["class"] = "dropdown-menu";
            if (inTable)
            {
                dropDownMenu.Attributes["class"] += " dropdown-menu-table";
            }
            if (isRight)
            {
                dropDownMenu.Attributes["class"] += " dropdown-menu-right";
            }
            foreach (var button in buttons)
            {
                var dropDownList = new HtmlGenericControl("li");
                dropDownList.Controls.Add(button);
                dropDownMenu.Controls.Add(dropDownList);
            }
            dropDownControl.Controls.Add(dropDownMenu);

            return dropDownControl;
        }
        #endregion

        #region Export Grid
        public static HtmlGenericControl ExportGrid(string gridname)
        {
            var exportGrid = new HtmlGenericControl("div");
            exportGrid.Attributes["class"] = "hidden-xs pull-right btn-inline-block btn-group";

            var callback = new BsnCallback()
            {
                Mode = CallbackMode.ExportGrid,
                Key = "PDF",
                Value = gridname,
            };
            
            var export = new BsnButton(callback, "fal fa-file-pdf", "no-btn-lg", ButtonTypes.Default, false);
            exportGrid.Controls.Add(export);

            var button = new HtmlGenericControl("a");
            button.Attributes["href"] = "#";
            button.Attributes["class"] = "btn btn-default dropdown-toggle";
            button.Attributes["data-toggle"] =  "dropdown";
            var span = new HtmlGenericControl("span");
            span.Attributes["class"] = "caret";
            button.Controls.Add(span);
            exportGrid.Controls.Add(button);

            var menu = new HtmlGenericControl("ul");
            menu.Attributes["class"] = "dropdown-menu";
            menu.Attributes["role"] = "menu";

            foreach (var btn in ExportButtons)
            {
                var list = new HtmlGenericControl("li");
                var format = btn.Attributes["onclick"];
                callback.Key = format;
                btn.Attributes["onclick"] = callback.ToCallback();
                list.Controls.Add(btn);
                menu.Controls.Add(list);
            }

            exportGrid.Controls.Add(menu);

            return exportGrid;
        }

        #region ExportButtons
        public static List<BsnButton> ExportButtons
        {
            get
            {
                var buttons = new List<BsnButton>
                {
                    new BsnButton("PDF", "fal fa-file-pdf", "PDF", ButtonTypes.Link, false),
                    new BsnButton("XLS", "fal fa-file-excel", "XLS", ButtonTypes.Link, false),
                    new BsnButton("XLSX", "fal fa-file-excel", "XLSX", ButtonTypes.Link, false),
                    new BsnButton("CSV", "fal fa-file-alt", "CSV", ButtonTypes.Link, false),
                    new BsnButton("RTF", "fal fa-file-word", "RTF", ButtonTypes.Link, false)
                };
                return buttons;
            }
        }
        #endregion
        #endregion

        #region WebSettings
        /*
        public static string GetTopicDescription(SettingBase webSetting)
        {
            if (webSetting.Type == Types.Area)
            {
                AreaSetting setting = (AreaSetting)webSetting;
                FieldInfo fi = typeof(AreaTopics).GetField(setting.Topic.ToString());
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes != null && attributes.Length > 0)
                    return attributes[0].Description;
                return setting.Topic.ToString().ToString();
            }

            if (webSetting.Type == Types.Custom)
            {
                CustomSetting setting = (CustomSetting)webSetting;
                FieldInfo fi = typeof(CustomTopics).GetField(setting.Topic.ToString());
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes != null && attributes.Length > 0)
                    return attributes[0].Description;
                return setting.Topic.ToString().ToString();
            }

            if (webSetting.Type == Types.Admin)
            {
                CustomSetting setting = (CustomSetting)webSetting;
                FieldInfo fi = typeof(CustomTopics).GetField(setting.Topic.ToString());
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes != null && attributes.Length > 0)
                    return attributes[0].Description;
                return setting.Topic.ToString().ToString();
            }
            return "Unknown Setting";
        }

        public static string CfgId(string setting)
        {
            return CfgId(setting, string.Empty);
        }

        const string ConfigPrefix = "BsnCraft.Web"; 
        public static string CfgId(string setting, string area)
        {
            string configId = ConfigPrefix;
            if (!string.IsNullOrEmpty(area))
            {
                configId += "." + area;
            }
            configId += "." + setting;
            return configId;
        }
        */
        #endregion

        #region SettingsButton
        public static List<BsnButton> SettingsButton
        {
            get
            {
                var buttons = (List<BsnButton>)VarUtils.Get(WebVars.SettingsButton);
                if (buttons == null)
                {
                    buttons = new List<BsnButton>();
                    VarUtils.Set(WebVars.SettingsButton, buttons);
                }
                return buttons;
            }
        }
        #endregion        
        
        #region GetRecords (xfServerPlus)
        public static List<T> GetRecords<T>(string BsnTable, string BsnParams, int MaxRecs = 0)
        {
            if (!BsnUtils.Connected)
            {
                return null;
            }

            string BsnResult = BsnUtils.GetData(BsnTable, BsnParams, typeof(T), MaxRecs);
            var BsnRecords = new List<T>();

            try
            {
                AddtoListFromString(BsnRecords, BsnResult);
            }
            catch (Exception e)
            {
                LogExc(e);
            }

            return BsnRecords;
        }

        public static String GetFieldList(Type elemType)
        {
            string s = string.Empty;
            var properties = elemType.GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(p => (!p.GetCustomAttributes(false).Any(a => a is XmlIgnoreAttribute)));
            foreach (var p in properties)
            {
                DescriptionAttribute[] attr = (DescriptionAttribute[])p.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attr != null && attr.Length > 0)
                {
                    s = s + attr[0].Description + ",";
                }
                else
                {
                    s = s + p.Name + ",";
                }

            }
            s = s.Remove(s.Length - 1);

            return s;
        }

        public static void AddtoListFromString<T>(List<T> list, String s)
        {
            AddtoListFromString(list, s, new string[] { "" + (Char)15 }, new string[] { "" + (Char)14 });
        }

        public static void AddtoListFromString<T>(List<T> list, String s, string[] recordDelim, string[] fieldDelim)
        {
            try
            {
                PropertyInfo[] pi = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .Where(p => (!p.GetCustomAttributes(false).Any(a => a is XmlIgnoreAttribute))).ToArray();

                String[] fields;
                String[] rows = s.Split(recordDelim, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < rows.GetLength(0); i++)
                {
                    fields = rows[i].Split(fieldDelim, StringSplitOptions.None);
                    Object obj = (T)Activator.CreateInstance(typeof(T));
                    LoadObjFromStringArray(fields, pi, ref obj);
                    list.Add((T)obj);
                }
            }
            catch (Exception)
            { }
        }

        public static void LoadObjFromStringArray<T>(String[] fields, PropertyInfo[] pi, ref T obj)
        {
            decimal d;
            Int32 tmpInt;
            Int64 tmpInt64;
            DateTime date;
            for (int f = 0; f < fields.GetLength(0); f++)
            {
                if (pi[f].PropertyType == typeof(String))
                {
                    pi[f].SetValue(obj, fields[f].TrimEnd(), null);
                    continue;
                }
                if (pi[f].PropertyType == typeof(Decimal))
                {
                    Decimal.TryParse(fields[f], out d);
                    pi[f].SetValue(obj, d, null);
                    continue;
                }
                if (pi[f].PropertyType == typeof(DateTime))
                {
                    if (!string.IsNullOrEmpty(fields[f]))
                    {
                        DateTime.TryParse(fields[f], out date);
                        pi[f].SetValue(obj, date, null);
                    }
                    continue;

                }
                if (pi[f].PropertyType == typeof(long))
                {
                    Decimal.TryParse(fields[f], out d);
                    tmpInt64 = (long)d;
                    pi[f].SetValue(obj, tmpInt64, null);
                    continue;
                }
                if (pi[f].PropertyType == typeof(DateTime?))
                {
                    DateTime.TryParse(fields[f], out date);
                    DateTime? ndate = new DateTime?(date);
                    pi[f].SetValue(obj, ndate, null);
                    continue;
                }
                if (pi[f].PropertyType == typeof(int))
                {
                    if (fields[f].GetType() == typeof(int))
                    {
                        Int32.TryParse(fields[f], out tmpInt);
                    }
                    else
                    {
                        Decimal.TryParse(fields[f], out d);
                        tmpInt = (int)d;
                    }

                    pi[f].SetValue(obj, tmpInt, null);
                    continue;
                }
            }
        }
        #endregion        
        
        #region Serialize
        public static String ObjectToString<T>(T obj)
        {
            XmlSerializer ser = XmlSerializer.FromTypes(new[] { typeof(T) })[0];
            TextWriter tw = new StringWriter();
            ser.Serialize(tw, obj);
            return tw.ToString();
        }

        public static T StringToObject<T>(String settingString) where T : new()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(settingString)) return new T();
                using (TextReader reader = new StringReader(settingString))
                {
                    XmlSerializer xmlSerial = XmlSerializer.FromTypes(new[] { typeof(T) })[0];
                    return (T)xmlSerial.Deserialize(reader);
                }
            }
            catch (Exception e)
            {

                LogExc(e);
                return default(T);

            }
        }
        #endregion

        #region RemoveFileWithDelay
        public static void RemoveFileWithDelay(string key, string fullPath, int delay)
        {
            RemoveFileWithDelayInternal(key, fullPath, delay, FileSystemRemoveAction);
        }

        const string RemoveTaskKeyPrefix = "DXRemoveTask_";
        static void RemoveFileWithDelayInternal(string fileKey, object fileData, int delay, CacheItemRemovedCallback removeAction)
        {
            string key = RemoveTaskKeyPrefix + fileKey;
            if (HttpRuntime.Cache[key] == null)
            {
                DateTime absoluteExpiration = DateTime.UtcNow.Add(new TimeSpan(0, delay, 0));
                HttpRuntime.Cache.Insert(key, fileData, null, absoluteExpiration,
                    Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, removeAction);
            }
        }

        static void FileSystemRemoveAction(string key, object value, CacheItemRemovedReason reason)
        {
            try
            {
                string fileFullPath = value.ToString();
                if (File.Exists(fileFullPath))
                    File.Delete(fileFullPath);
            }
            catch (Exception e)
            {
                LogExc(e, true);
            }
        }
        #endregion
        
        #region EditControl
        public static Dictionary<BsnControls, string> EditControls
        {
            get
            {
                var editControls = (Dictionary<BsnControls, string>)VarUtils.Get(WebVars.EditControls);
                if (editControls == null)
                {
                    editControls = new Dictionary<BsnControls, string>();
                    editControls.Add(BsnControls.Edit_Customer, "~/Customers/Edit/Customer.ascx");
                    VarUtils.Set(WebVars.EditControls, editControls);
                }
                return editControls;
            }
        }


        //Old Bootstrap3 method
        public static string EditControl(BsnControls controlId)
        {
            string virtualPath = string.Empty;
            switch (controlId)
            {
                case BsnControls.Edit_Settings:
                    virtualPath += "Settings";
                    break;
                case BsnControls.Contract_Estimate:
                case BsnControls.Edit_Paragraph:
                    virtualPath += "Paragraph";
                    break;
                case BsnControls.Edit_DateSelect:
                    virtualPath += "DateSelect";
                    break;
                case BsnControls.Edit_Customer:
                case BsnControls.Grid_Contacts:
                case BsnControls.Grid_Customer:
                case BsnControls.Grid_Customers:
                    virtualPath += "Customer";
                    break;
                case BsnControls.Edit_Chart:
                case BsnControls.Grid_Charts:
                    virtualPath += "Chart";
                    break;
                case BsnControls.Edit_Dashboard:
                case BsnControls.Grid_Dashboards:
                    virtualPath += "Dashboard";
                    break;
                case BsnControls.Edit_Report:
                case BsnControls.Grid_Reports:
                    virtualPath += "Report";
                    break;
                case BsnControls.Edit_Contract:
                case BsnControls.Grid_Contracts:
                    virtualPath += "Contract";
                    break;
                case BsnControls.Edit_Lead:
                case BsnControls.Grid_Leads:
                    virtualPath += "Lead";
                    break;
                case BsnControls.Edit_Heading:
                case BsnControls.Grid_Headings:
                    virtualPath += "Heading";
                    break;
                case BsnControls.Edit_Estimate:
                case BsnControls.Grid_Estimate:
                    virtualPath += "Estimate";
                    break;
                case BsnControls.Edit_Document:
                case BsnControls.Grid_Documents:
                case BsnControls.Grid_DealSubmission:
                    virtualPath += "Document";
                    break;
                case BsnControls.Edit_Event:
                case BsnControls.Grid_Events:
                    virtualPath += "Event";
                    break;
                case BsnControls.Edit_Variation:
                case BsnControls.Grid_Variations:
                    virtualPath += "Variation";
                    break;
                case BsnControls.Edit_Selection:
                case BsnControls.Grid_Selections:
                    virtualPath += "Selection";
                    break;
                case BsnControls.Edit_VariationEvent:
                case BsnControls.Grid_VariationEvents:
                    virtualPath += "VariationEvent";
                    break;

            }
            if (!string.IsNullOrEmpty(virtualPath))
            {
                virtualPath = "~/Controls/Edit/" + virtualPath + ".ascx";
            }            
            return virtualPath;
        }
        #endregion
        
        #region GridControl
        public static string GridControl(BsnControls controlId)
        {
            string virtualPath = string.Empty;
            switch (controlId)
            {
                case BsnControls.Grid_Customer:
                    virtualPath += "Customer";
                    break;
                case BsnControls.Grid_Customers:
                    virtualPath += "Customers";
                    break;
                case BsnControls.Grid_Contacts:
                    virtualPath += "Contacts";
                    break;
                case BsnControls.Grid_Leads:
                    virtualPath += "Leads";
                    break;
                case BsnControls.Grid_Contracts:
                    virtualPath += "Contracts";
                    break;
                case BsnControls.Grid_Estimate:
                    virtualPath += "Estimate";
                    break;
                case BsnControls.Grid_Headings:
                    virtualPath += "Headings";
                    break;
                case BsnControls.Grid_Documents:
                    virtualPath += "Documents";
                    break;
                case BsnControls.Grid_JobInfo:
                    virtualPath += "JobInfo";
                    break;
                case BsnControls.Grid_Events:
                    virtualPath += "Events";
                    break;
                case BsnControls.Grid_Variations:
                    virtualPath += "Variations";
                    break;
                case BsnControls.Grid_DisplayInfo:
                    virtualPath += "DisplayInfo";
                    break;
                case BsnControls.Grid_Items:
                    virtualPath += "Items";
                    break;
                case BsnControls.Grid_Users:
                    virtualPath += "Users";
                    break;
                case BsnControls.Grid_Selections:
                    virtualPath += "Selections";
                    break;
            }
            if (!string.IsNullOrEmpty(virtualPath))
            {
                virtualPath = "~/Controls/Grid/" + virtualPath + ".ascx";
            }
            return virtualPath;
        }
        #endregion
        
        #region CreateThumbnail
        public static byte[] CreateThumbnail(string path, int maxWidth, int maxHeight)
        {            
            byte[] buffer = null;
            Image img = null;
            try
            {
                if (maxHeight == 0 || maxWidth == 0)
                {
                    img = ScaleImage(Image.FromFile(path), maxWidth, maxHeight);
                }
                else
                {
                    img = FixedSize(Image.FromFile(path), maxWidth, maxHeight);
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    img.Save(ms, ImageFormat.Png);
                    buffer = ms.ToArray();
                }                
            }
            catch
            {
                if (VarUtils.Get(WebVars.NoImage) != null)
                {
                    buffer = (byte[])VarUtils.Get(WebVars.NoImage);
                }
                else
                {
                    if (string.IsNullOrEmpty(Settings.Comp.ThumbnailDefault))
                    {
                        Settings.Comp.ThumbnailDefault = "~/Content/images/noimage.png";
                    }
                    var noImage = ResolveUrl(Settings.Comp.ThumbnailDefault);
                    path = HttpContext.Current.Server.MapPath(noImage);
                    img = FixedSize(Image.FromFile(path), maxWidth, maxHeight);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        img.Save(ms, ImageFormat.Png);
                        buffer = ms.ToArray();
                        VarUtils.Set(WebVars.NoImage, buffer);
                    }
                }
            }
            return buffer;
        }

        private static Image ScaleImage(Image imgPhoto, int maxWidth, int maxHeight)
        {
            int newHeight = imgPhoto.Height;
            int newWidth = imgPhoto.Width;

            if (maxWidth > 0 && newWidth > maxWidth) //WidthResize
            {
                Decimal divider = Math.Abs(newWidth / (Decimal)maxWidth);
                newWidth = maxWidth;
                newHeight = (int)Math.Round(newHeight / divider);
            }
            if (maxHeight > 0 && newHeight > maxHeight) //HeightResize
            {
                Decimal divider = Math.Abs(newHeight / (Decimal)maxHeight);
                newHeight = maxHeight;
                newWidth = (int)Math.Round(newWidth / divider);
            }

            //Log.Debug("ScaleImage| NewWidth = " + newWidth.ToString() + ", NewHeight = " + newHeight.ToString());
            return FixedSize(imgPhoto, newWidth, newHeight);
        }

        private static Image FixedSize(Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = Width / (float)sourceWidth;
            nPercentH = Height / (float)sourceHeight;
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = Convert.ToInt16((Width - (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = Convert.ToInt16((Height - (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format32bppArgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Transparent);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //Log.Debug("FixedSize| DestWidth = " + destWidth.ToString() + ", DestHeight = " + destHeight.ToString());

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }
        #endregion

        #region ViewReport
        public static string ViewReport(string keystring)
        {
            string[] keyval = keystring.Split(':');
            string key = GetArray(keyval, 0);
            string basepath = GetArray(keyval, 1);
            string report = GetArray(keyval, 2);
            string path = path = HttpContext.Current.Server.MapPath(basepath + "/pdf/");
            string pdfFile = path + report + " " + key + ".pdf";
            return basepath + "/pdf/" + Path.GetFileName(pdfFile);
        }
        #endregion

        #region RunReport
        public static string RunReport(string keystring, string formulas)
        {
            try
            {
                string[] keyval = keystring.Split(':');
                string key = GetArray(keyval, 0);
                string basepath = GetArray(keyval, 1);
                string report = GetArray(keyval, 2);

                string path = HttpContext.Current.Server.MapPath(basepath + "/rpt/");
                string rptFile = path + report + ".rpt";

                path = HttpContext.Current.Server.MapPath(basepath + "/pdf/");
                string pdfFile = path + report + " " + key + ".pdf";
                string bsnerror = string.Empty;
                ReportUtils.BsnExportPDF(rptFile, RecordUtils.BsnDSN.Replace("DSN=",""), pdfFile, formulas, out bsnerror);
                if (string.IsNullOrEmpty(bsnerror))
                {
                    SetSuccess("Created: " + Path.GetFileName(pdfFile));
                    return basepath + "/pdf/" + Path.GetFileName(pdfFile);                    
                }

                SetError(bsnerror);
            }
            catch (Exception exc)
            {
                LogExc(exc);
            }
            return string.Empty;
        }
        #endregion
        
    }
}