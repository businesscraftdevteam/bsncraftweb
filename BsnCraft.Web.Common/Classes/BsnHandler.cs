﻿using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Web;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace BsnCraft.Web.Common.Classes
{
    public class BsnHandler
    {
        #region Public Properties
        //public bool Refresh { get; private set; }
        public string Modal { get; private set; } = "";
        #endregion

        #region Private Properties                        
        private Logger Log { get; set; }        
        private BsnApplication App { get; set; }
        #endregion

        #region Constructor
        public BsnHandler(BsnApplication app)
        {
            Log = LogManager.GetLogger(GetType().FullName);
            App = app;
        }
        #endregion
        
        #region Process
        public void Process(bool dataOnly = false)
        {
            var cb = dataOnly ? App.DataCallback : App.Callback;
            Log.Trace("Callback: " + cb.ToString());

            switch (cb.Type)
            {
                case CallbackType.WindowLocation:
                case CallbackType.WindowState:
                    App.LoadMenu(cb.Menu, cb.Action, cb.Id, cb.Mode, cb.Key, cb.Value);
                    break;
                case CallbackType.Form:
                    App.Forms.Get(cb.Key.ToForm()).Update = true;
                    break;
            }

            switch (cb.Mode)
            {
                case BsnModes.Logout:
                    App.Api.Logout();
                    App.Callback.Init();
                    break;
                case BsnModes.SaveSettings:
                    App.Settings.SaveUser();
                    break;                
                case BsnModes.Refresh:
                    Refresh();
                    return;                
                case BsnModes.MenuOption:
                    App.Settings.MenuOption((cb.Menu, cb.Action), cb.Key, cb.Value.DefaultBool(false));
                    break;
                case BsnModes.ColumnVisible:
                    App.Settings.ColumnVisible(cb.Key.ToGrid(), cb.Id, cb.Value.DefaultBool(true));                    
                    break;                
                case BsnModes.Bookmark:
                    App.Settings.SetBookmark(cb.Key.ToGrid(), cb.Id, cb.Value);
                    break;                
                case BsnModes.ViewFile:
                    App.Utils.ViewFile(Helpers.ResolveUrl(cb.Value), "View", App.Panel);
                    break;
                case BsnModes.Documentation:
                    App.Utils.ViewFile(Helpers.ResolveUrl(cb.Value), "Help", App.Panel);
                    break;
                case BsnModes.SelectReport:
                    var reportCallback = App.Utils.GetModeCallback(BsnModes.RunReport);
                    var reportButton = new BsnButton(reportCallback, "search", "View", true, ButtonTypes.Primary)
                    { CSS = "bsn-report d-none" };
                    var reportOptions = new ModalOptions("Select Report");
                    App.LoadModal(App.Utils.ReportSelector, reportOptions, reportButton);
                    break;
                case BsnModes.SelectDates:
                    var dateButton = new BsnButton("check", "Select", true, ButtonTypes.Primary)
                    { Javascript = "App.calendar();" };
                    var dateOptions = new ModalOptions("Select Date(s)");
                    App.LoadModal(App.Utils.Calendar(), dateOptions, dateButton);
                    break;
                case BsnModes.RunReport:
                    App.Utils.ExportCrystal();
                    break;
                default:
                    break;
            }
            
            if (cb.Data == null)
            {
                cb.Data = new CallbackData();
                return;
            }

            switch (cb.Data.Mode)
            {
                case BsnDataModes.FormSave:
                case BsnDataModes.FormCancel:
                    var afterProcess = true;
                    afterProcess = App.Forms.Get(cb.Data.Key.ToForm()).Process(cb.Data);
                    if (afterProcess)
                    {
                        App.Forms.Get(cb.Data.Key.ToForm()).AfterProcess(cb.Data);
                    }                    
                    break;
                case BsnDataModes.ShowGrouping:
                    App.Settings.GroupToggleVisibility(cb.Data.Key.ToGrid(),(bool) cb.Data.Object);
                    break;
                case BsnDataModes.ShowNotes:
                    App.Settings.NotesToggleVisibility(cb.Data.Key.ToGrid(), (bool)cb.Data.Object);
                    break;
                case BsnDataModes.GroupToggle:
                case BsnDataModes.CollapseAll:
                    App.Settings.GroupToggle(cb.Data.Key.ToGrid(), cb.Data.Object.ToString());
                    break;
                case BsnDataModes.SavePanels:
                    try
                    {
                        var jsPanels = (List<DockPanel>)JsonConvert.DeserializeObject(cb.Data.Object.ToString(), typeof(List<DockPanel>));
                        var panels = App.Settings.MenuPanels((cb.Menu, cb.Action));
                        foreach (var panel in panels)
                        {
                            var jsPanel = jsPanels.Find(p => p.Name.Equals(panel.Name));
                            if (jsPanel != null)
                            {
                                panel.Visible = jsPanel.Visible;
                                panel.VisibleIndex = jsPanel.VisibleIndex;
                                panel.ZoneId = jsPanel.ZoneId;
                            }
                        }
                        App.Settings.UpdatePanels((cb.Menu, cb.Action), panels);
                    }
                    catch (Exception ex)
                    {
                        App.Utils.LogExc(ex);
                    }
                    break;
                case BsnDataModes.ResetPanels:
                    App.Settings.UpdatePanels((cb.Menu, cb.Action), new List<DockPanel>());
                    break;
                case BsnDataModes.PanelToggle:
                    App.Settings.UpdatePanel((cb.Menu, cb.Action), cb.Data.Key, cb.Data.Value.ToBool());
                    break;
                case BsnDataModes.ExpandAll:
                    App.Settings.GroupToggle(cb.Data.Key.ToGrid(), cb.Data.Object.ToString(), true);
                    break;
                case BsnDataModes.UserSetting:
                    App.Settings.UserSetting(cb.Data.Key, cb.Data.Value);
                    App.DataLocation = new Callback();
                    break;
                case BsnDataModes.CloseTab:                    
                    App.DataLocation = App.Settings.CloseTab(App.Menu, App.Action, App.Id);
                    break;
                case BsnDataModes.CloseKey:
                    App.DataLocation = App.Settings.CloseKey(cb.Menu, cb.Action, cb.Id, cb.Data.Key);
                    break;
                case BsnDataModes.SelectedKeys:
                    var keyList = cb.Data.Object.ToString();
                    var keys = (List<string>)JsonConvert.DeserializeObject(keyList, typeof(List<string>));
                    break;
                case BsnDataModes.SelectReport:
                    App.ReportKey = cb.Data.Key;
                    break;
            }

        }
        #endregion

        #region Refresh
        void Refresh()
        {
            var menu = App.Menus.Current.UID.Item1;
            var action = App.Menus.Current.UID.Item2;
            var id = App.Menus.Current.Id;
            var mode = App.Menus.Current.Mode;
            var key = App.Menus.Current.Key;
            var value = App.Menus.Current.Value;
            App.Init();
            App.LoadMenu(menu, action, id, mode, key, value);
        }
        #endregion
        
    }
}
