﻿using NLog;
using System;
using System.Data;
using System.Data.Odbc;

namespace BsnCraft.Web.Common
{
    public class RecordUtils
    {
        private static Logger Log = LogManager.GetCurrentClassLogger();

        #region BsnDSN
        public static string BsnDSN
        {
            get
            {
                if (!string.IsNullOrEmpty(Utils.Settings.Comp.AlternateDSN))
                {
                    return "DSN=" + Utils.Settings.Comp.AlternateDSN;
                }
                return "DSN=BSN_xfODBC_" + BsnUtils.BsnCompany;
            }
        }
        #endregion

        #region Data
        public static DataSet Data(string Query)
        {
            return Data(BsnDSN, Query);
        }

        public static DataSet Data(string bsnDSN, string Query)
        {
            //Note: do not use any session vars in here.
            //Will break Report Data Serializer

            Log.Info("Table Query=" + Query);
            var Connection = new OdbcConnection(bsnDSN);
            var Data = new DataSet();
            var Adapter = new OdbcDataAdapter(Query, Connection);
            try
            {
                Connection.Open();
                Adapter.Fill(Data);
                Connection.Close();
            }
            catch (Exception e)
            {
                Utils.LogExc(e);
                return null;
            }
            return Data;
        }
        #endregion

        #region Table
        public static DataTable Table(string Query)
        {
            if (!BsnUtils.Connected)
            {
                return null;
            }

            var data = Data(Query);
            if (data == null ||
                data.Tables == null || 
                data.Tables.Count == 0)
            {
                return null;
            }

            return data.Tables[0];
        }
        #endregion

        #region Spec
        public static string Spec(string tablename, string keyfield, string key, string specfield, string linebreak)
        {
            Log.Debug("Spec: Table=" + tablename + "; Key=" + key + "; KeyField=" + keyfield + "; SpecField=" + specfield);
            string specvalue = string.Empty;
            
            var query = "SELECT " + specfield + " FROM " + tablename + " WHERE " + keyfield + " = '" + key + "'";
            var table = Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    specvalue += row[specfield].ToString();
                }
            }
            specvalue = Utils.CleanSpec(specvalue, linebreak);
            return specvalue;
        }
        #endregion
        
        #region SpecL (Like statement)
        public static string SpecL(string tablename, string keyfield, string key, string specfield, string linebreak)
        {
            Log.Debug("GetSpec: Table=" + tablename + "; Key=" + key + "; KeyField=" + keyfield + "; SpecField=" + specfield);
            string specvalue = string.Empty;
            var query = "SELECT " + specfield + " FROM " + tablename + " WHERE " + keyfield + " LIKE '" + key + "%'";
            var table = Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    specvalue += row[specfield].ToString();
                }
            }
            specvalue = Utils.CleanSpec(specvalue, linebreak);
            return specvalue;
        }
        #endregion        

        #region Count
        public static string Count(string table, string code)
        {
            return Count(table, code, "{fn LENGTH(" + code + ")} > 0", "");
        }

        public static string Count(string table, string code, string value)
        {
            return Count(table, code, code + "= '" + value + "'", "");
        }

        public static string Count(string table, string code, string where, string extras)
        {
            string count = string.Empty;
            try
            {
                string query = "SELECT COUNT(" + code + ") FROM " + table + " WHERE " + where;
                if (!string.IsNullOrEmpty(extras))
                {
                    if (!extras.ToUpper().TrimStart().StartsWith("AND"))
                    {
                        query += " AND ";
                    }
                    query += extras;
                }
                count = Table(query).Rows[0]["EXPR0"].ToString();
            }
            catch (Exception e)
            {
                Utils.LogExc(e);
            }
            return count;
        }
        #endregion
    }
}
