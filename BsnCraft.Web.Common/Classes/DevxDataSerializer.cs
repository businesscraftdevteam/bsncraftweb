using DevExpress.XtraReports.Native;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace BsnCraft.Web.Common.Classes
{
    public class DevxDataSerializer : IDataSerializer
    {
        #region Public Properties
        public const string Name = "DevxDataSerializer";
        public BsnApplication App { get; private set; }
        public string QueryString { get; set; }
        #endregion

        #region Constructor
        public DevxDataSerializer(BsnApplication app)
        {
            App = app;
        }
        #endregion

        public bool CanSerialize(object data, object extensionProvider)
        {
            var canSerialize = (data is DataSet);
            return canSerialize;
        }

        public string Serialize(object data, object extensionProvider)
        {
            var name = ((DataSet)data).DataSetName;
            return name;
        }


        public bool CanDeserialize(string value, string typeName, object extensionProvider)
        {
            var canDeserialize = (value == "Data");
            return canDeserialize;
		} 
		
        public object Deserialize(string value, string typeName, object extensionProvider)
        {
            if (value == "Data")
            {
                return DataSet;
            }
            return null;
        }

        public DataSet DataSet
        {
            get
            {
                return App.Odbc.Data(QueryString);
            }
        }
    }
}
