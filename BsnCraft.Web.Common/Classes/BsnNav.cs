﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Settings.Base;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Menu.Base;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes
{
    public class BsnNav
    {
        #region Private Properties
        private BsnApplication App { get; set; }
        private BsnButton Refresh { get; set; }
        private BsnButton CloseTab { get; set; }
        private BsnButton CloseKey { get; set; }
        private BsnButtonGroup ProfileList { get; set; }
        private BsnButtonGroup DarkThemes { get; set; }
        private BsnButtonGroup LightThemes { get; set; }
        private List<string> LogoIcon1 { get; set; }
        private List<string> LogoText1 { get; set; }
        private List<string> LogoText2 { get; set; }
        private BsnMenu Menu { get; set; }
        #endregion

        #region Constructor
        public BsnNav(BsnApplication app)
        {
            App = app;
            LogoIcon1 = new List<string>();
            LogoText1 = new List<string>();
            LogoText2 = new List<string>();

            Refresh = new BsnButton(new BsnCallback(App)
            { Mode = BsnModes.Refresh }, "sync bsn-refresh", "", true, ButtonTypes.OutlineInfo)
            { Tooltip = "Refresh Data" };

            CloseTab = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.CloseTab
                }
            }, "", "&nbsp;", false, ButtonTypes.Close)
            { Tooltip = "Close Tab" };

            CloseKey = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.CloseKey,
                }
            }, "", "&nbsp;", false, ButtonTypes.Close)
            { Tooltip = "Close Key" };

            ProfileList = new BsnButtonGroup(new BsnButton("user-circle", "", true, ButtonTypes.OutlineInfo) { Tooltip = "My Profile" }, true);
            ProfileList.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.UserSetting,
                    Key = UserSettingsTypes.Theme.ToString()
                }
            }, "moon", "Dark Theme")
            { UID = AppButtons.DarkMode.ToString() });
            ProfileList.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.UserSetting,
                    Key = UserSettingsTypes.TabletMode.ToString()
                }
            },"tablet-alt", "Touch Mode")
            { UID = AppButtons.TabletMode.ToString() });
            ProfileList.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.UserSetting,
                    Key = UserSettingsTypes.FluidLayout.ToString()
                }
            },
                "desktop", "Widescreen")
            { UID = AppButtons.FluidLayout.ToString() });
            ProfileList.Buttons.Add(new BsnButton(new BsnCallback(App) { Mode = BsnModes.Logout }, "power-off", "Sign Out"));
            
            DarkThemes = new BsnButtonGroup(new BsnButton("paint-brush", "", true, ButtonTypes.OutlineInfo) { Tooltip = "Themes" }, true);
            foreach (var theme in App.Utils.Themes.Where(p => p.Value.Item1))
            {
                var themeName = (theme.Key == "Dark") ? "Default" : theme.Key;
                var button = new BsnButton(new BsnCallback(App)
                {
                    Type = CallbackType.DataOnly,
                    Data = new CallbackData()
                    {
                        Mode = BsnDataModes.UserSetting,
                        Key = UserSettingsTypes.Theme.ToString(),
                        Value = theme.Key
                    }
                }, "fas fa-square fa-fw", themeName);
                button.IconAttributes["style"] = "color: #" + theme.Value.Item2;
                DarkThemes.Buttons.Add(button);
            }

            LightThemes = new BsnButtonGroup(new BsnButton("paint-brush", "", true, ButtonTypes.OutlineInfo) { Tooltip = "Themes" }, true);
            foreach (var theme in App.Utils.Themes.Where(p => !p.Value.Item1))
            {
                var button = new BsnButton(new BsnCallback(App)
                {
                    Type = CallbackType.DataOnly,
                    Data = new CallbackData()
                    {
                        Mode = BsnDataModes.UserSetting,
                        Key = UserSettingsTypes.Theme.ToString(),
                        Value = theme.Key
                    }
                }, "fas fa-square fa-fw", theme.Key);
                button.IconAttributes["style"] = "color: #" + theme.Value.Item2;
                LightThemes.Buttons.Add(button);
            }
        }
        #endregion

        #region Init
        public void Init()
        {            

        }
        #endregion

        #region Load
        public void Load()
        {
            Menu = App.Menus.Current;            
            Icons();
            Sidebar();
            DashHead();
            Modes();
            Options();
            Reports();
            Profile();
            MainTabs();
            Actions();
            KeyTabs();
            Upload();
        }
        #endregion

        #region Icons
        void Icons()
        {
            var iconav = (HtmlGenericControl)App.Panel.FindControl("iconav");
            if (!App.Settings.UserSetting(UserSettingsTypes.TabletMode).DefaultBool(false))
            {
                iconav.Visible = false;
                return;
            }
            var fluidLayout = App.Settings.UserSetting(UserSettingsTypes.FluidLayout).DefaultBool(false);
            var main = (HtmlGenericControl)App.Panel.FindControl("main");
            var logo = (HtmlAnchor)App.Panel.FindControl("iconlogo");
            var items = (HtmlGenericControl)App.Panel.FindControl("iconitems");
            var container = (HtmlGenericControl)App.Panel.FindControl("container");
            iconav.Visible = true;
            container.Visible = true;
            container.Controls.Add(iconav);
            container.Controls.Add(main);
            container.Attributes["class"] = "with-iconav";
            main.Attributes["class"] = fluidLayout ? "container-fluid" : "container";
            logo.Controls.Add(LogoIcon());
            var item = new HtmlGenericControl("li");    //Dummy
            items.Controls.Add(item);
            item.Attributes["class"] = "nav-item";
            NavItems(items, App.Menus.Main, true);            
        }
        #endregion

        #region Sidebar
        void Sidebar()
        {
            var sidenav = (HtmlGenericControl)App.Panel.FindControl("sidenav");
            if (App.Settings.UserSetting(UserSettingsTypes.TabletMode).DefaultBool(false))
            {
                sidenav.Visible = false;
                return;
            }
            var fluidLayout = App.Settings.UserSetting(UserSettingsTypes.FluidLayout).DefaultBool(false);
            var main = (HtmlGenericControl)App.Panel.FindControl("main");
            var logo = (HtmlButton)App.Panel.FindControl("sidelogo");
            var items = (HtmlGenericControl)App.Panel.FindControl("sideitems");
            var version = (HtmlAnchor)App.Panel.FindControl("version");
            var row = (HtmlGenericControl)App.Panel.FindControl("row");
            var container = (HtmlGenericControl)App.Panel.FindControl("container");
            sidenav.Visible = true;
            row.Visible = true;
            container.Visible = true;
            container.Controls.Add(row);
            row.Controls.Add(sidenav);
            row.Controls.Add(main);
            container.Attributes["class"] = fluidLayout ? "container-fluid" : "container";
            row.Attributes["class"] = "row";
            sidenav.Attributes["class"] = "col-md-3 sidebar";
            main.Attributes["class"] = "col-md-9 content";
            if (fluidLayout)
            {
                sidenav.Attributes["class"] += " col-xl-2";
                main.Attributes["class"] += " col-xl-10";                
            }
            logo.Controls.Add(LogoIcon());
            logo.Controls.Add(LogoText(120));            
            version.Attributes["title"] = App.Version.ToString();
            version.Controls.Add(new LiteralControl("v" + App.Version.Major + "." + App.Version.Minor));
            var header = new HtmlGenericControl("li");
            items.Controls.Add(header);
            header.Attributes["class"] = "nav-header";
            header.Controls.Add(new LiteralControl("Main"));            
            NavItems(items, App.Menus.Main);            
        }
        #endregion

        #region Nav Items
        void NavItems(HtmlGenericControl items, List<BsnMenu> menu, bool icons = false)
        {
            foreach (var m in menu.Where(p => p.Visible.Equals(true)))
            {
                var callback = new BsnCallback(App)
                {
                    Type = CallbackType.WindowLocation,
                    Menu = m.UID.Item1,
                    Action = m.UID.Item2
                };

                var item = new HtmlGenericControl("li");
                items.Controls.Add(item);
                item.Attributes["class"] = "nav-item";
                var link = new HtmlGenericControl("a");
                item.Controls.Add(link);
                link.Attributes["class"] = "nav-link";
                link.Attributes["href"] = "javascript:void(0);";
                link.Attributes["onclick"] = callback.ToCallback();
                link.Attributes["title"] = string.Empty;
                link.Attributes["data-toggle"] = string.Empty;
                link.Attributes["data-placement"] = string.Empty;
                link.Attributes["data-container"] = string.Empty;
                if (m.UID.Item1.Equals(Menu.UID.Item1))
                {
                    link.Attributes["class"] += " active";
                }
                if (!icons)
                {
                    var badge = new HtmlGenericControl("small");
                    link.Attributes["class"] += " justify-content-between align-items-center d-flex";
                    badge.Attributes["class"] = "badge badge-secondary";
                    if (m.UID.Equals(Menu.UID))
                    {
                        badge.Attributes["class"] = "badge badge-light";
                    }
                    link.Controls.Add(new LiteralControl(m.Caption));
                    if (m.GetCount() > 0)
                    {
                        link.Controls.Add(badge);
                        badge.Controls.Add(new LiteralControl(m.GetCount().ToString()));
                    }
                }
                else
                {
                    //TODO: Investigate 
                    //These are not working with the official popper.js needed for grid
                    //Only work in popper-toolkit.js

                    //link.Attributes["title"] = m.Caption;
                    //if (m.GetCount() > 0)
                    //{
                    //    link.Attributes["title"] += " (" + m.GetCount().ToString()  + ")";
                    //}
                    //link.Attributes["data-toggle"] = "tooltip";
                    //link.Attributes["data-placement"] = "right";
                    //link.Attributes["data-container"] = "body";
                    var small = new HtmlGenericControl("small");
                    small.Attributes["class"] = "d-md-none";
                    link.Controls.Add(small);
                    small.Controls.Add(new LiteralControl(m.Caption));
                    small.Attributes["class"] += " iconav-nav-label";
                    var span = new HtmlGenericControl("span");
                    span.Attributes["class"] = m.Icon;                    
                    link.Controls.Add(span);
                }
            }
        }
        #endregion

        #region DashHead
        private void DashHead()
        {
            var head = (HtmlGenericControl)App.Panel.FindControl("head");
            var usercomp = (HtmlGenericControl)App.Panel.FindControl("usercomp");
            var title = (HtmlGenericControl)App.Panel.FindControl("title");
            var subtitle = (HtmlGenericControl)App.Panel.FindControl("subtitle");
            head.Visible = true;
            usercomp.Controls.Add(new LiteralControl(App.Api.FullName + " - " + App.Api.CompanyName));
            title.Controls.Add(new LiteralControl(Menu.Title));
            subtitle.Controls.Add(new LiteralControl(SubTitle));            
        }
        #endregion

        #region SubTitle
        private string SubTitle
        {
            get
            {
                if (Menu.SubTitle != Menu.Title)
                {
                    return Menu.SubTitle;
                }
                var subtitle = "Main";
                //TODO: perform checks here for non-main menu
                return subtitle;
            }            
        }
        #endregion

        #region Modes
        void Modes()
        {
            var modes = (HtmlGenericControl)App.Panel.FindControl("modes");
            var div = (HtmlGenericControl)App.Panel.FindControl("modediv");
            modes.Visible = false;
            div.Visible = false;
            bool show = false;
            foreach (var button in Menu.Modes)
            {
                if (button.Visible)
                {
                    show = true;
                    if (button is BsnButton b)
                    {
                        b.Type = App.Utils.DarkTheme ? ButtonTypes.OutlineWarning : ButtonTypes.Outline;
                    }
                    if (button is BsnButtonGroup g)
                    {
                        g.Button.Type = App.Utils.DarkTheme ? ButtonTypes.OutlineWarning : ButtonTypes.Outline;
                    }
                    modes.Controls.Add(button.ToControl());
                }
            }
            if (show)
            {
                modes.Visible = true;
                div.Visible = true;
            }
        }
        #endregion

        #region Options
        void Options()
        {
            if (Menu.Options.Count == 0)
            {
                return;
            }
            var optionsButton = new BsnButtonGroup(new BsnButton("cog", "", true, ButtonTypes.Outline)
            {
                Tooltip = "Options"
            });
            foreach (var option in Menu.Options)
            {
                var value = Menu.Settings.Options.ContainsKey(option.Key) ?
                    Menu.Settings.Options[option.Key] : option.Value.Item2;
                var callback = new BsnCallback(App)
                {
                    Menu = Menu.UID.Item1,
                    Action = Menu.Action,
                    Mode = BsnModes.MenuOption,
                    Key = option.Key,
                    Value = (!value).ToString()
                };
                var icon = value ? "check-square" : "square";
                var caption = option.Value.Item1;
                optionsButton.Buttons.Add(new BsnButton(callback, icon, caption));
            }
            AddTool(optionsButton.ToControl());
        }
        #endregion

        #region Reports
        void Reports()
        {
            if (!Menu.ShowReports)
            {
                return;
            }
            var callback = App.Utils.GetModeCallback(BsnModes.SelectReport);
            callback.Type = CallbackType.Modal;
            var reportsButton = new BsnButton(callback, "file-alt", "", true, ButtonTypes.Outline)
            {
                Tooltip = "Reports"
            };
            AddTool(reportsButton.ToControl());
        }
        #endregion

        #region Add Tool
        public void AddTool(Control control)
        {
            var toolbar = (HtmlGenericControl)App.Panel.FindControl("toolbar");
            var div = (HtmlGenericControl)App.Panel.FindControl("tooldiv");
            if (toolbar != null)
            {
                toolbar.Visible = true;
                div.Visible = true;
                toolbar.Controls.Add(control);
            }
        }
        #endregion

        #region Profile
        void Profile()
        {
            var profile = (HtmlGenericControl)App.Panel.FindControl("profile");
            var themeButton = ProfileList.Buttons.Where(p => p.UID.Equals(AppButtons.DarkMode.ToString())).FirstOrDefault();
            themeButton.Callback.Data.Value = App.Utils.DarkTheme ? "Default" : "Dark";
            themeButton.Active = App.Utils.DarkTheme;

            var tabletButton = ProfileList.Buttons.Where(p => p.UID.Equals(AppButtons.TabletMode.ToString())).FirstOrDefault();
            var tabletMode = App.Settings.UserSetting(UserSettingsTypes.TabletMode).DefaultBool(false);
            tabletButton.Callback.Data.Value = (!tabletMode).ToString();
            tabletButton.Active = tabletMode;

            var fluidButton = ProfileList.Buttons.Where(p => p.UID.Equals(AppButtons.FluidLayout.ToString())).FirstOrDefault();
            var fluidLayout = App.Settings.UserSetting(UserSettingsTypes.FluidLayout).DefaultBool(false);
            fluidButton.Callback.Data.Value = (!fluidLayout).ToString();
            fluidButton.Active = fluidLayout;

            profile.Controls.Add(Refresh.ToControl());
            var themeList = App.Utils.DarkTheme ? DarkThemes : LightThemes;
            foreach (var button in themeList.Buttons)
            {
                var theme = (App.Utils.Theme == "Dark") ? "Default" : App.Utils.Theme;
                button.Active = button.Caption.Equals(theme);
            }
            profile.Controls.Add(themeList.ToControl());
            profile.Controls.Add(ProfileList.ToControl());
        }
        #endregion

        #region Main Tabs
        void MainTabs()
        {
            var control = (HtmlGenericControl)App.Panel.FindControl("tabs");
            var div = (HtmlGenericControl)App.Panel.FindControl("tabdiv");
            var bar = (HtmlGenericControl)App.Panel.FindControl("tabbar");            
            var display = false;
            var tabs = App.Settings.User.Tabs
                .Where(p => p.Menu.Equals(App.Menu) && p.Id.Equals(string.Empty))
                .OrderBy(p => p.TimeOpened)
                .ToList();
            AddMainTabs(ref display, control, tabs);
            tabs = App.Settings.User.Tabs
                .Where(p => p.Menu.Equals(App.Menu) && !p.Id.Equals(string.Empty))
                .OrderBy(p => p.TimeOpened)
                .ToList();
            AddMainTabs(ref display, control, tabs);
            control.Visible = display;
            div.Visible = display;
            bar.Visible = display;
        }
        #endregion

        #region Add Main Tabs
        void AddMainTabs(ref bool display, HtmlGenericControl control, List<TabSettings> tabs)
        {
            if (tabs.Count == 0)
            {
                return;
            }
            foreach (var tab in tabs)
            {
                display = true;
                var menu = App.Menus.Get(tab.Menu, tab.Action);
                var caption = tab.Id.ValueOrDefault(menu.ParentMenu.Caption);
                var button = new BsnButton(new BsnCallback(App)
                {
                    Type = CallbackType.WindowLocation,
                    Menu = tab.Menu,
                    Action = tab.Action,
                    Id = tab.Id,
                    Mode = tab.Mode,
                    Key = tab.Key
                }, "", caption, false, ButtonTypes.Link);
                if (string.IsNullOrEmpty(tab.Id))
                {                    
                    if (menu.GetCount() != 0)
                    {
                        button.Tooltip = "Count: " + menu.GetCount();
                    }                    
                }
                else
                {
                    button.Tooltip = menu.Caption;
                }
                var list = new HtmlGenericControl("li");
                control.Controls.Add(list);                
                list.Attributes["class"] = "nav-item";
                var link = (HtmlGenericControl)button.ToControl();
                list.Controls.Add(link);
                link.Attributes["class"] = "nav-link";
                if (tab.Menu == Menu.UID.Item1 && tab.Id == Menu.Id)
                {
                    if (link.Controls[0] != null && link.Controls[0] is HtmlGenericControl span)
                    {
                        span.Attributes["class"] += " mr-2";
                    }
                    link.Attributes["class"] += " active";
                    CloseTab.Callback.Menu = Menu.UID.Item1;
                    CloseTab.Callback.Action = Menu.UID.Item2;
                    CloseTab.Callback.Id = Menu.Id;
                    var close = (HtmlGenericControl)CloseTab.ToControl();
                    close.Attributes["class"] = "nav-link active";
                    list.Controls.Add(close);
                }
            }
        }
        #endregion

        #region Actions
        void Actions()
        {
            var control = (HtmlGenericControl)App.Panel.FindControl("actions");
            var div = (HtmlGenericControl)App.Panel.FindControl("actiondiv");
            var display = false;
            foreach (var action in Menu.GetActions())
            {
                if (!action.Visible)
                {
                    continue;
                }
                var visible = (!action.RequiresId && string.IsNullOrEmpty(Menu.Id)) ||
                    (action.RequiresId && !string.IsNullOrEmpty(Menu.Id));
                if (visible)
                {
                    display = true;
                    var button = new BsnButton(new BsnCallback(App)
                    {
                        Type = CallbackType.WindowLocation,
                        Menu = Menu.UID.Item1,
                        Action = action.UID.Item1,                        
                        Mode = action.UID.Item2,
                        Id = action.RequiresId ? Menu.Id : string.Empty
                    }, 
                    action.Icon, action.Caption, false, ButtonTypes.Link);
                    var list = new HtmlGenericControl("li");
                    control.Controls.Add(list);
                    list.Attributes["class"] = "nav-item";
                    var link = (HtmlGenericControl)button.ToControl();
                    list.Controls.Add(link);
                    link.Attributes["class"] = "nav-link";

                    //Below is strange code but it convers the unusual situation of contracts menu
                    //Either Important Docs or Contacts could be the default action
                    //Contract details is highlighted based on current mode
                    var currentAction = Menu.Action;
                    if (Menu.ParentAction != BsnActions.Default)
                    {
                        currentAction = Menu.ParentAction;
                    }
                    if ((currentAction == action.UID.Item1) || action.Selected)
                    {
                        var allModes = action.AllModes;
                        if (allModes && action.UID.Item2 != BsnModes.None)
                        {
                            allModes = (Menu.Mode != BsnModes.None);
                        }
                        if (Menu.Mode == action.UID.Item2 || allModes)
                        {
                            link.Attributes["class"] += " active";
                        }                        
                    }
                }
            }
            control.Visible = display;
            div.Visible = display;
        }
        #endregion

        #region Key Tabs
        void KeyTabs()
        {
            var control = (HtmlGenericControl)App.Panel.FindControl("keys");
            var div = (HtmlGenericControl)App.Panel.FindControl("keydiv");
            var bar = (HtmlGenericControl)App.Panel.FindControl("keybar");
            var display = false;
            var tab = App.Settings.User.Tabs.Find(p => p.Menu.Equals(Menu.UID.Item1) && p.Id.Equals(Menu.Id));
            if (tab != null && tab.OpenKeys.ContainsKey(tab.Action))
            {
                var openKeys = tab.OpenKeys[tab.Action];
                if (openKeys.Count > 1)
                {
                    foreach (var key in openKeys)
                    {
                        var caption = key.Key.DefaultString(Menu.Caption);
                        display = true;
                        var button = new BsnButton(new BsnCallback(App)
                        {
                            Type = CallbackType.WindowLocation,
                            Menu = tab.Menu,
                            Action = tab.Action,
                            Id = tab.Id,
                            Mode = key.Mode,
                            Key = key.Key
                        }, "", caption, false, ButtonTypes.Link);
                        var list = new HtmlGenericControl("li");
                        control.Controls.Add(list);
                        list.Attributes["class"] = "nav-item";
                        var link = (HtmlGenericControl)button.ToControl();
                        list.Controls.Add(link);
                        link.Attributes["class"] = "nav-link";
                        if (tab.Key == key.Key)
                        {
                            if (link.Controls[0] != null && link.Controls[0] is HtmlGenericControl span)
                            {
                                span.Attributes["class"] += " mr-2";
                            }
                            link.Attributes["class"] += " active";
                            CloseKey.Callback.Menu = tab.Menu;
                            CloseKey.Callback.Action = tab.Action;
                            CloseKey.Callback.Id = tab.Id;
                            CloseKey.Callback.Data.Key = key.Key;
                            var close = (HtmlGenericControl)CloseKey.ToControl();
                            close.Attributes["class"] = "nav-link active";
                            if (!string.IsNullOrEmpty(key.Key))
                            {
                                list.Controls.Add(close);
                            }                            
                        }
                    }
                }
            }
            var actions = (HtmlGenericControl)App.Panel.FindControl("actiondiv");
            actions.Attributes["class"] += display ? " mb-2" : " mb-4";
            control.Visible = display;
            div.Visible = display;
            bar.Visible = display;
        }
        #endregion

        #region LogoIcon
        public HtmlGenericControl LogoIcon(int width = 40)
        {
            if (LogoIcon1.Count == 0)
            {
                LogoIcon1.Add("m 248.69855,430.83747 c -32.3408,-56.13735 -58.6658,-102.20158 -58.5,-102.36496 0.1658,-0.16338 4.30807,0.53864 9.20505,1.56004 35.06008,7.31278 " +
                    "77.85086,5.95298 126.54093,-4.02119 36.7638,-7.53106 79.60899,-21.68403 122.39162,-40.42937 l 3.83614,-1.68082 -2.42152,4.06072 c -1.33184,2.2334 -33.6581,58.26123 " +
                    "-71.83615,124.50629 C 339.73658,478.71324 308.275,532.91186 308,532.90955 c -0.275,-0.002 -26.96065,-45.93473 -59.30145,-102.07208 z M 58.245732,100.67511 1.0083441,1.5 " +
                    "70.004172,1.2431547 c 37.947708,-0.1412648 100.067758,-0.1412648 138.044568,0 L 277.09748,1.5 266.79874,9.1855854 C 195.11374,62.681564 139.0276,131.30182 119.01174,190 " +
                    "c -1.7817,5.225 -3.30452,9.5788 -3.38404,9.67511 -0.0795,0.0963 -25.901404,-44.4537 -57.381968,-99 z");                
                LogoIcon1.Add("M 234,307.55532 C 184.28007,301.13149 153.84859,279.60216 144.99925,244.59004 140.90481,228.39053 142.89132,204.92017 150.16984,183.5 168.41009,129.82029 " +
                    "221.86672,65.554065 295.17379,9.1744287 L 305.84758,0.9653375 330.81217,1.2326688 355.77677,1.5 l -12.53162,9 C 289.09074,49.392812 240.82302,96.792718 214.42914,137 " +
                    "196.79034,163.87018 188,187.60296 188,208.35528 c 0,15.37336 3.87485,25.94458 13.29356,36.26697 C 226.69059,272.45604 287.64131,276.21 368,254.8897 " +
                    "c 35.05871,-9.30157 91.20615,-30.7718 124.21458,-47.49843 2.63198,-1.33373 4.78542,-2.2407 4.78542,-2.0155 0,0.22521 -6.77082,12.12371 -15.04626,26.44112 " +
                    "l -15.04626,26.03166 -14.54014,6.17386 C 401.14177,285.77321 349.98187,300.43397 304,306.53965 c -16.94498,2.25003 -56.05411,2.81749 -70,1.01567 z");
                LogoIcon1.Add("m 267.05405,243.87788 c -14.5669,-1.68763 -27.96761,-6.4733 -36.04889,-12.87379 -5.10784,-4.04549 -9.57261,-11.24585 -11.45533,-18.47408 -1.95501,-7.50579 " +
                    "-1.94895,-11.68204 0.0308,-21.2668 C 228.4729,148.21315 283.37319,85.017134 368,20.416946 375.975,14.3292 385.2,7.4650506 388.5,5.163281 l 6,-4.18503569 " +
                    "L 504.59384,1.2391227 614.68768,1.5 563.59384,89.987701 512.5,178.4754 l -16,7.20894 c -91.60107,41.27163 -180.62285,63.84991 -229.44595,58.19354 z");
            }
            var logoIcon = new HtmlGenericControl("svg");
            logoIcon.Attributes["xmlns"] = "http://www.w3.org/2000/svg";
            logoIcon.Attributes["preserveAspectRatio"] = "xMidYMid";
            logoIcon.Attributes["viewBox"] = "0 0 616 534";
            logoIcon.Attributes["role"] = "img";
            logoIcon.Attributes["style"] = "width: " + width.ToString() + "px; opacity: 0.75;";            
            var icon = new HtmlGenericControl("path");
            icon.Attributes["d"] = LogoIcon1[0];
            icon.Attributes["class"] = App.Utils.DarkTheme ? "logo-inverse1" : "logo-light1"; ;
            logoIcon.Controls.Add(icon);
            icon = new HtmlGenericControl("path");
            icon.Attributes["d"] = LogoIcon1[1];
            icon.Attributes["class"] = App.Utils.DarkTheme ? "logo-inverse2" : "logo-light2"; ;
            logoIcon.Controls.Add(icon);
            icon = new HtmlGenericControl("path");
            icon.Attributes["d"] = LogoIcon1[2];
            icon.Attributes["class"] = App.Utils.DarkTheme ? "logo-inverse3" : "logo-light3"; ;
            logoIcon.Controls.Add(icon);
            return logoIcon;
        }
        #endregion

        #region LogoText        
        public HtmlGenericControl LogoText(int width = 200)
        {
            if (LogoText1.Count == 0)
            {
                /* B */
                LogoText1.Add("M37.786,102.093 C41.266,102.573 46.786,103.053 53.986,103.053 C67.185,103.053 76.305,100.653 81.944,95.494 C86.024,91.534 88.784,86.254 88.784,79.294 " +
                    "C88.784,67.295 79.785,60.935 72.105,59.015 L72.105,58.775 C80.625,55.655 85.784,48.816 85.784,41.016 C85.784,34.656 83.264,29.856 79.065,26.736 " +
                    "C74.025,22.657 67.305,20.857 56.866,20.857 C49.546,20.857 42.346,21.577 37.786,22.537 L37.786,102.093 ZM48.226,29.616 C49.906,29.256 52.666,28.896 57.465,28.896 " +
                    "C68.025,28.896 75.225,32.616 75.225,42.096 C75.225,49.895 68.745,55.655 57.705,55.655 L48.226,55.655 L48.226,29.616 ZM48.226,63.575 L56.866,63.575 " +
                    "C68.265,63.575 77.745,68.135 77.745,79.174 C77.745,90.934 67.785,94.894 56.986,94.894 C53.266,94.894 50.266,94.774 48.226,94.414 L48.226,63.575 Z");
                /* u */
                LogoText1.Add("M143.008,43.888 L132.448,43.888 L132.448,79.526 C132.448,81.446 132.088,83.366 131.488,84.926 C129.568,89.606 124.649,94.526 117.569,94.526 " +
                    "C107.969,94.526 104.609,87.086 104.609,76.047 L104.609,43.888 L94.050,43.888 L94.050,77.847 C94.050,98.246 104.969,103.285 114.089,103.285 " +
                    "C124.409,103.285 130.528,97.166 133.288,92.486 L133.528,92.486 L134.128,101.966 L143.488,101.966 C143.128,97.406 143.008,92.126 143.008,86.126 L143.008,43.888 Z");
                /* s */
                LogoText1.Add("M148.480,98.840 C152.560,101.240 158.439,102.800 164.799,102.800 C178.599,102.800 186.518,95.600 186.518,85.401 C186.518,76.761 181.358,71.721 171.279,67.881 " +
                    "C163.719,65.001 160.239,62.842 160.239,58.042 C160.239,53.722 163.719,50.122 169.959,50.122 C175.359,50.122 179.559,52.042 181.838,53.482 L184.478,45.802 " +
                    "C181.238,43.882 176.079,42.202 170.199,42.202 C157.719,42.202 150.160,49.882 150.160,59.242 C150.160,66.201 155.080,71.961 165.519,75.681 " +
                    "C173.319,78.561 176.319,81.321 176.319,86.361 C176.319,91.160 172.719,95.000 165.039,95.000 C159.759,95.000 154.240,92.840 151.120,90.800 L148.480,98.840 Z");
                /* i */
                LogoText1.Add("M201.919,102.000 L201.919,43.922 L191.360,43.922 L191.360,102.000 L201.919,102.000 ZM196.639,21.003 C192.800,21.003 190.040,23.883 190.040,27.603 " +
                    "C190.040,31.203 192.680,34.083 196.399,34.083 C200.599,34.083 203.239,31.203 203.119,27.603 C203.119,23.883 200.599,21.003 196.639,21.003 Z");
                /* n */
                LogoText1.Add("M210.960,101.800 L221.519,101.800 L221.519,66.881 C221.519,65.081 221.759,63.282 222.239,61.962 C224.039,56.082 229.439,51.162 236.399,51.162 " +
                    "C246.358,51.162 249.838,58.962 249.838,68.321 L249.838,101.800 L260.398,101.800 L260.398,67.121 C260.398,47.202 247.918,42.402 239.878,42.402 " +
                    "C230.279,42.402 223.559,47.802 220.679,53.322 L220.439,53.322 L219.839,43.722 L210.480,43.722 C210.840,48.522 210.960,53.442 210.960,59.442 L210.960,101.800 Z");
                /* e */
                LogoText1.Add("M316.557,75.327 C316.677,74.247 316.917,72.567 316.917,70.407 C316.917,59.727 311.877,43.048 292.918,43.048 C275.999,43.048 265.679,56.847 265.679,74.367 " +
                    "C265.679,91.886 276.359,103.645 294.238,103.645 C303.478,103.645 309.837,101.726 313.557,100.046 L311.757,92.486 C307.798,94.166 303.238,95.486 295.678,95.486 " +
                    "C285.118,95.486 275.999,89.606 275.759,75.327 L316.557,75.327 ZM275.879,67.767 C276.719,60.447 281.399,50.608 292.078,50.608 C303.958,50.608 306.838,61.047 306.718,67.767 L275.879,67.767 Z");
                /* s1 */
                LogoText1.Add("M316.730,99.193 C320.810,101.593 326.690,103.153 333.049,103.153 C346.849,103.153 354.768,95.953 354.768,85.753 C354.768,77.114 349.609,72.074 339.529,68.234 " +
                    "C331.969,65.354 328.490,63.194 328.490,58.394 C328.490,54.075 331.969,50.475 338.209,50.475 C343.609,50.475 347.809,52.395 350.089,53.835 L352.729,46.155 " +
                    "C349.489,44.235 344.329,42.555 338.449,42.555 C325.970,42.555 318.410,50.235 318.410,59.594 C318.410,66.554 323.330,72.314 333.769,76.034 " +
                    "C341.569,78.914 344.569,81.673 344.569,86.713 C344.569,91.513 340.969,95.353 333.289,95.353 C328.010,95.353 322.490,93.193 319.370,91.153 L316.730,99.193 Z");
                /* s2 */
                LogoText1.Add("M355.730,99.193 C359.810,101.593 365.690,103.153 372.049,103.153 C385.849,103.153 393.768,95.953 393.768,85.753 C393.768,77.114 388.609,72.074 378.529,68.234 " +
                    "C370.969,65.354 367.490,63.194 367.490,58.394 C367.490,54.075 370.969,50.475 377.209,50.475 C382.609,50.475 386.809,52.395 389.089,53.835 L391.729,46.155 " +
                    "C388.489,44.235 383.329,42.555 377.449,42.555 C364.970,42.555 357.410,50.235 357.410,59.594 C357.410,66.554 362.330,72.314 372.769,76.034 " +
                    "C380.569,78.914 383.569,81.673 383.569,86.713 C383.569,91.513 379.969,95.353 372.289,95.353 C367.010,95.353 361.490,93.193 358.370,91.153 L355.730,99.193 Z");
            }

            if (LogoText2.Count == 0)
            {
                /* C */
                LogoText2.Add("M454.026,90.753 C449.826,92.793 443.346,94.113 436.986,94.113 C417.307,94.113 405.907,81.394 405.907,61.595 C405.907,40.355 418.507,28.236 437.466,28.236 " +
                    "C444.186,28.236 449.826,29.676 453.786,31.596 L456.305,23.076 C453.546,21.636 447.186,19.476 437.106,19.476 C412.027,19.476 394.868,36.636 394.868,61.955 " +
                    "C394.868,88.474 412.027,102.873 434.826,102.873 C444.666,102.873 452.346,100.953 456.185,99.033 L454.026,90.753 Z");
                /* r */
                LogoText2.Add("M460.224,102.374 L470.664,102.374 L470.664,71.416 C470.664,69.616 470.904,67.936 471.144,66.496 C472.584,58.576 477.863,52.936 485.303,52.936 " +
                    "C486.743,52.936 487.823,53.056 488.903,53.296 L488.903,43.337 C487.943,43.097 487.103,42.977 485.903,42.977 C478.823,42.977 472.464,47.897 469.824,55.696 " +
                    "L469.344,55.696 L468.984,44.297 L459.744,44.297 C460.104,49.697 460.224,55.576 460.224,62.416 L460.224,102.374 Z");
                /* a */
                LogoText2.Add("M532.438,101.692 C531.718,97.732 531.478,92.812 531.478,87.773 L531.478,66.053 C531.478,54.414 527.159,42.294 509.399,42.294 " +
                    "C502.080,42.294 495.120,44.334 490.320,47.454 L492.720,54.414 C496.800,51.774 502.440,50.094 507.839,50.094 C519.719,50.094 521.039,58.734 521.039,63.533 " +
                    "L521.039,64.733 C498.600,64.613 486.120,72.293 486.120,86.333 C486.120,94.732 492.120,103.012 503.880,103.012 C512.159,103.012 518.399,98.932 521.639,94.372 " +
                    "L521.999,94.372 L522.839,101.692 L532.438,101.692 ZM521.279,82.133 C521.279,83.213 521.039,84.413 520.679,85.493 C518.999,90.412 514.199,95.212 506.639,95.212 " +
                    "C501.240,95.212 496.680,91.972 496.680,85.133 C496.680,73.853 509.759,71.813 521.279,72.053 L521.279,82.133 Z");
                /* f */
                LogoText2.Add("M551.698,101.962 L551.698,51.924 L565.737,51.924 L565.737,43.884 L551.698,43.884 L551.698,40.765 C551.698,31.885 553.977,23.965 562.977,23.965 " +
                    "C565.977,23.965 568.137,24.565 569.697,25.285 L571.137,17.126 C569.097,16.286 565.857,15.446 562.137,15.446 C557.217,15.446 551.938,17.006 547.978,20.845 " +
                    "C543.058,25.525 541.258,32.965 541.258,41.125 L541.258,43.884 L533.098,43.884 L533.098,51.924 L541.258,51.924 L541.258,101.962 L551.698,101.962 Z");
                /* t */
                LogoText2.Add("M576.335,29.704 L576.335,43.624 L567.336,43.624 L567.336,51.663 L576.335,51.663 L576.335,83.342 C576.335,90.182 577.415,95.342 580.415,98.462 " +
                    "C582.935,101.341 586.895,102.901 591.815,102.901 C595.894,102.901 599.134,102.301 601.174,101.461 L600.694,93.542 C599.374,93.902 597.334,94.262 594.575,94.262 " +
                    "C588.695,94.262 586.655,90.182 586.655,82.982 L586.655,51.663 L601.774,51.663 L601.774,43.624 L586.655,43.624 L586.655,26.944 L576.335,29.704 Z");
            }
            var logoText = new HtmlGenericControl("svg");
            logoText.Attributes["xmlns"] = "http://www.w3.org/2000/svg";
            logoText.Attributes["preserveAspectRatio"] = "xMidYMid";
            logoText.Attributes["viewBox"] = "0 0 640 120";
            logoText.Attributes["role"] = "img";
            logoText.Attributes["style"] = "width: " + width.ToString() + "px;";
            var theme = App.Settings.UserSetting(UserSettingsTypes.Theme).DefaultString("default");            
            foreach (var letter in LogoText1)
            {
                var txt = new HtmlGenericControl("path");
                txt.Attributes["d"] = letter;
                txt.Attributes["class"] = App.Utils.DarkTheme ? "logo-inverse1" : "logo-light1"; ;
                logoText.Controls.Add(txt);
            }
            foreach (var letter in LogoText2)
            {
                var txt = new HtmlGenericControl("path");
                txt.Attributes["d"] = letter;
                txt.Attributes["class"] = App.Utils.DarkTheme ? "logo-inverse2" : "logo-light2";
                logoText.Controls.Add(txt);
            }
            return logoText;
        }
        #endregion

        #region Upload
        void Upload()
        {
            App.UploadPanel.Visible = Menu.ShowUpload;
            if (Menu.ShowUpload)
            {
                App.UploadControl.NullText = "Drop file";
                if (Menu.MultiUpload)
                {
                    App.UploadControl.NullText += "(s)";
                }
                App.UploadControl.NullText += " here, or click Browse button";
            }
            App.UploadControl.AdvancedModeSettings.EnableMultiSelect = Menu.MultiUpload;
        }
        #endregion

    }
}
