﻿using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes
{
    public enum SearchTypes
    {
        Item,
        Lead,
        Customer
    }

    //Note: Consider DevExpress Form Validation
    public class BsnSearch : HtmlGenericControl
    {
        public BsnSearch(SearchTypes type)
        {
            TagName = "div";
            Attributes["class"] = "input-group";

            var group = new HtmlGenericControl("div");
            group.Attributes["class"] = "input-group-btn disabled";

            var button = new HtmlGenericControl("span");
            button.Attributes["class"] = "dxbs-edit-btn disabled btn btn-default text-icon";

            var span = new HtmlGenericControl("span");
            button.Controls.Add(span);

            var input = new HtmlInputText();
            input.Attributes["class"] = "form-control";

            switch (type)
            {
                case SearchTypes.Item:
                    span.Attributes["class"] = "image fal fa-cubes";
                    button.Controls.Add(new LiteralControl("Item"));
                    input.ID = "ItemSearch";
                    input.Attributes["search"] = "item";
                    break;
            }
            
            group.Controls.Add(button);
            Controls.Add(group);
            Controls.Add(input);
        }

    }
}
