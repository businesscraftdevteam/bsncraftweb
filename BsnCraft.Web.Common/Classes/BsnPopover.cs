﻿using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes
{
    public class BsnPopover : HtmlGenericControl
    {
        public BsnPopover(string title, string content, bool bootstrap4 = false)
        {
            TagName = "a";
            Attributes["href"] = "javascript:void(0);";

            Attributes["class"] = "bsn-popover btn btn-link btn-lg";            

            if (!string.IsNullOrEmpty(title))
            {
                Attributes["title"] = title;
            }
            //Attributes["data-content"] = content;

            var span = new HtmlGenericControl("span");
            span.Attributes["class"] = "fal fa-file-alt fa-fw";
            Controls.Add(span);

            span = new HtmlGenericControl("span");
            span.Attributes["class"] = "popover-content";
            span.Attributes["class"] += bootstrap4 ? " d-none" : " hidden";
            span.InnerHtml = content;
            Controls.Add(span);
        }
    }
}
