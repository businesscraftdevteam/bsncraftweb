﻿using BsnCraft.Web.Common.Settings;
using NLog;
using System;
using System.Data;
using System.Data.Odbc;

namespace BsnCraft.Web.Common.Classes
{
    public class BsnOdbc
    {
        #region Private Properties
        private Logger Log { get; set; }
        private BsnApplication App { get; set; }
        #endregion

        #region Constructor
        public BsnOdbc(BsnApplication app)
        {
            Log = LogManager.GetLogger(GetType().FullName);
            App = app;
        }
        #endregion

        #region BsnDSN
        public string BsnDSN
        {
            get
            {
                var altDsn = App.Settings.Comp(CompanySettingsType.AlternateDSN).ToString();
                if (!string.IsNullOrEmpty(altDsn))
                {
                    return "DSN=" + altDsn;
                }
                return "DSN=BSN_xfODBC_" + App.Api.Company;
            }
        }
        #endregion

        #region Data        
        public DataSet Data(string query, string bsnDSN = "")
        {
            if (string.IsNullOrEmpty(bsnDSN))
            {
                bsnDSN = BsnDSN;
            }
            Log.Info("ODBC Query = " + query);
            var startTime = DateTime.Now;
            var connection = new OdbcConnection(bsnDSN);
            var data = new DataSet()
            {
                DataSetName = "Data"
            };
            var adapter = new OdbcDataAdapter(query, connection);
            try
            {
                connection.Open();
                adapter.Fill(data);
                connection.Close();
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e, true);
                return null;
            }
            Log.Info("ODBC Ms = " + (DateTime.Now - startTime).TotalMilliseconds);
            return data;
        }
        #endregion

        #region Table
        public DataTable Table(string Query)
        {
            if (!App.Api.Connected)
            {
                return null;
            }

            var data = Data(Query);
            if (data == null ||
                data.Tables == null || 
                data.Tables.Count == 0)
            {
                return null;
            }

            return data.Tables[0];
        }
        #endregion

        #region Spec
        public string Spec(string tablename, string keyfield, string key, string specfield, string linebreak = "\r\n")
        {
            Log.Info("Spec: Table=" + tablename + "; Key=" + key + "; KeyField=" + keyfield + "; SpecField=" + specfield);
            string specvalue = string.Empty;
            
            var query = "SELECT " + specfield + " FROM " + tablename + " WHERE " + keyfield + " = '" + key + "'";
            var table = Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    specvalue += row[specfield].ToString();
                }
            }
            specvalue = Helpers.CleanSpec(specvalue, linebreak);
            return specvalue;
        }
        #endregion
        
        #region SpecL (Like statement)
        public string SpecL(string tablename, string keyfield, string key, string specfield, string keytype = "", string linebreak = "\r\n")
        {
            Log.Info("GetSpec: Table=" + tablename + "; Key=" + key + "; KeyField=" + keyfield + "; SpecField=" + specfield);
            string specvalue = string.Empty;
            var query = "SELECT " + specfield + " FROM " + tablename + " WHERE " + keyfield + " LIKE '" + keytype + key + "%'";
            var table = Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    specvalue += row[specfield].ToString();
                }
            }
            specvalue = Helpers.CleanSpec(specvalue, linebreak);
            return specvalue;
        }
        #endregion        

        #region Count
        public string Count(string table, string code)
        {
            return Count(table, code, "{fn LENGTH(" + code + ")} > 0", "");
        }

        public string Count(string table, string code, string value)
        {
            return Count(table, code, code + "= '" + value + "'", "");
        }

        public string Count(string table, string code, string where, string extras)
        {
            string count = string.Empty;
            try
            {
                string query = "SELECT COUNT(" + code + ") FROM " + table + " WHERE " + where;
                if (!string.IsNullOrEmpty(extras))
                {
                    if (!extras.ToUpper().TrimStart().StartsWith("AND"))
                    {
                        query += " AND ";
                    }
                    query += extras;
                }
                count = Table(query).Rows[0]["EXPR0"].ToString();
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return count;
        }
        #endregion
    }
}
