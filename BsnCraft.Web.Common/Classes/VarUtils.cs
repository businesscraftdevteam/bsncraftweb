﻿using NLog;
using System;

namespace BsnCraft.Web.Common
{
    public enum WebVars
    {
        BsnServer,
        BsnTicket,
        BsnCompany,
        BsnFullName,
        BsnInitials,
        PasswordKey,
        AllContracts,
        Connected,
        EditControls,
        JSModalEvent,
        JSModalSize,
        JSDeleteMessage,
        JSLocation,
        JSPageData,
        JSViewFile,
        ExportGrid,
        RefreshMenu,
        BsnLogo,
        WebMenu,
        WebSettings,        
        Alerts,
        Themes,     
        Filename,
        SettingsButton,
        PageSettings,
        PageSize,
        PageFilter,
        PagePreview,
        CallbackData,
        ModalCallback,
        SaveTab,
        NoImage,
        ReportParams,
        DesktopPanels,
        Customers,
        CustomersGrid,
        CustomerContacts,
        Leads,
        Contracts,
        BookmarkedCustomers,
        BookmarkedContacts,
        BookmarkedLeads,
        BookmarkedContracts,
        DisplayCentres,
        ContractSettings,
        Headings,
        Documents,
        ImportantDocuments,
        ImportantDocumentsGrid,
        DealDocuments,
        JobDocuments,
        JobInfo,
        JobInfoGroups,
        JobInfoCategories,
        JobInfoQuestions,
        JobInfoAnswers1,
        JobInfoAnswers2,
        JobInfoAnswers3,
        Events,
        Variations,
        VariationEvents,
        VariationDocuments,
        Checklists,
        LoginUsers,
        ItemSelections,
        CustomerTitles,
        DataViews,
        Charts,
        Reports,
        Dashboards,
        States,
        Districts,
        Councils,
        Employees,
        SalesConsultants,
        SalesPromotions,
        SalesCentres,
        OpCentres,
        StatusCodes,
        UOMCodes,
        PriceDisplay,
        MarginCodes,
        SaleTypes,
        SaleGroups,
        DocumentTypes,
        DealDocumentTypes,
        HeadingDocTypes,
        JobInfoDocuments,
        DisplayDates,
        DisplayInfo,
        DisplayData,
        DisplayInfoQuestions,
        DisplayInfoAnswers,
        Templates,
        EstimateDetails,
        EstimateFilter,
        Paragraphs,
        AllItems,
        Items,
        ItemSelection,
        ItemList,
        TemplateControl,
        TemplateCouncils,
        TemplateEstates,
        //SingleContract,
        EventChecklist,
        ChecklistInfo,
        ChecklistData,
        ChecklistInfoQuestions,
        ChecklistInfoAnswers,
        InvoicingSettings,
        Houses,
        Facades,
        PostCodes,
        EventCodes,
        ChecklistHeaders,
        ChecklistLines,
        Activities,
        Activity,
        ActivityFilter,
        ActivityContracts,
        ActivityEventCodes,
        Jobs,
        PurchaseOrders
    }

    public class VarUtils
    {

        #region Str
        public static string Str(WebVars name)
        {
            return Str(name.ToString()).ToString();
        }

        public static string Str(string name)
        {
            if (Get(name) != null)
            {
                return Get(name).ToString();
            }
            return string.Empty;
        }
        #endregion

        #region Get
        public static object Get(WebVars name)
        {
            return Get(name.ToString());
        }

        public static object Get(string name)
        {
            if (Utils.CurrentPage == null || Utils.CurrentPage.Session[name] == null)
            {
                return null;
            }
            return Utils.CurrentPage.Session[name];
        }
        #endregion

        #region Set
        public static void Set(WebVars name, object value)
        {
            Set(name.ToString(), value);
        }

        public static void Set(string name, object value)
        {
            if (Utils.CurrentPage != null)
            {
                Utils.CurrentPage.Session[name] = value;
            }
        }
        #endregion

        #region Refresh
        public static void Refresh()
        {
            BsnUtils.BsnDisconnect();
            LogManager.Configuration = LogManager.Configuration.Reload();
            LogManager.ReconfigExistingLoggers();
            foreach (WebVars var in Enum.GetValues(typeof(WebVars)))
            {
                Set(var, null);
            }
        }
        #endregion

    }
}
