﻿namespace BsnCraft.Web.Common.Classes
{
    public class BsnDataView
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DataSource { get; set; }
        public string Display { get; set; }
        public bool IsNew { get; set; }
        public bool IsBookmarked { get; set; }
    }
}
