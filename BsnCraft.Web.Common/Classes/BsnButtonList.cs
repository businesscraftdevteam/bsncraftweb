﻿using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes
{
    public class BsnButtonList : HtmlGenericControl
    {
        HtmlGenericControl Menu;

        //Primary Button is clickable with side button drop down
        public BsnButtonList(HtmlGenericControl primary, ButtonTypes type = ButtonTypes.Default) 
        {
            TagName = "div";
            Attributes["class"] = "btn-inline-block btn-group";
            Controls.Add(primary);

            var button = (type == ButtonTypes.Link) ?
                new BsnButton(string.Empty, "fal fa-chevron-down", string.Empty, type, false) :
                new BsnButton(string.Empty, "caret", "no-btn-lg", type, false);

            button.AddAttr("class", "dropdown-toggle");
            button.AddAttr("data-toggle", "dropdown");
            Controls.Add(button);
            
            AddMenu();            
        }

        //Entire Button is drop down
        public BsnButtonList(string caption = "", string iconclass = "fal fa-bars fa-fw", bool right = true)
        {
            TagName = "div";
            Attributes["class"] = "dropdown";
            var anchor = new HtmlGenericControl("a");
            anchor.Attributes["href"] = "#";
            anchor.Attributes["class"] = "dropdown-toggle btn btn-default";
            anchor.Attributes["data-toggle"] = "dropdown";
            anchor.Attributes["role"] = "button";
            anchor.Attributes["aria-expanded"] = "false";
            Controls.Add(anchor);

            var icon = new HtmlGenericControl("span");
            icon.Attributes["class"] = iconclass;
            icon.Attributes["aria-hidden"] = "true";
            anchor.Controls.Add(icon);
            anchor.Controls.Add(new LiteralControl(" "));

            if (!string.IsNullOrEmpty(caption))
            {
                var text = new HtmlGenericControl("span");
                text.Attributes["class"] = "hidden-xs";
                text.Controls.Add(new LiteralControl(caption));
                anchor.Controls.Add(text);
            }

            anchor.Controls.Add(new LiteralControl(" "));
            var caret = new HtmlGenericControl("span");
            caret.Attributes["class"] = "caret";
            anchor.Controls.Add(caret);

            AddMenu(right);
        }

        void AddMenu(bool right = false)
        {
            Menu = new HtmlGenericControl("ul");
            Menu.Attributes["class"] = "dropdown-menu";
            if (right)
            {
                Menu.Attributes["class"] += " dropdown-menu-right";
            }
            Menu.Attributes["role"] = "menu";
            Controls.Add(Menu);
        }

        public void Add(HtmlGenericControl button)
        {
            var list = new HtmlGenericControl("li");
            list.Controls.Add(button);
            Menu.Controls.Add(list);
        }
    }
}
