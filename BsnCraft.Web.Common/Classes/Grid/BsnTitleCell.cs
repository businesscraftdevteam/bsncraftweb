﻿using DevExpress.Web;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes.Grid
{
    public class BsnTitleCell : ITemplate
    {
        private string Caption;

        public BsnTitleCell(string caption)
        {
            Caption = caption;
        }

        public void InstantiateIn(Control container)
        {
            var gridContainer = (GridViewTitleTemplateContainer)container;
            //var anchor = new HtmlGenericControl("a");
            //var text = new HtmlGenericControl("i");
            //text.Attributes["class"] = Icon;
            //anchor.Controls.Add(text);
            gridContainer.Controls.Add(new LiteralControl(Caption));
        }
    }
}
