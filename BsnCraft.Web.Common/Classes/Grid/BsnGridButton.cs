﻿using BsnCraft.Web.Common.Models;
using System;

namespace BsnCraft.Web.Common.Classes.Grid
{
    public class BsnGridButton
    {
        public BsnCallback Callback { get; set; }
        public string Icon { get; set; }
        public string Caption { get; set; }
        public bool IsButton { get; set; }
        public ButtonTypes Type { get; set; }

        public BsnGridButton(BsnCallback callback, string icon = "", string caption = "", bool isButton = true, ButtonTypes type = ButtonTypes.Link)
        {
            Callback = callback;
            Icon = icon;
            Caption = caption;
            IsButton = isButton;
            Type = type;
        }

        public virtual void OnCellPrepared(EventArgs e)
        {
            CellPrepared?.Invoke(this, e);
        }

        public event EventHandler CellPrepared;
    }

    public class BsnEventArgs : EventArgs
    {
        public string KeyValue { get; set; }

        public BsnEventArgs(string keyval)
        {
            KeyValue = keyval;
        }
    }
}
