﻿using DevExpress.Web;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes.Grid
{
    public class BsnHeaderCell : ITemplate
    {
        private string Icon;

        public BsnHeaderCell(string icon)
        {
            Icon = icon;
        }

        public void InstantiateIn(Control container)
        {
            var gridContainer = (GridViewHeaderTemplateContainer)container;
            var anchor = new HtmlGenericControl("a");
            var text = new HtmlGenericControl("i");
            text.Attributes["class"] = Icon;
            anchor.Controls.Add(text);
            gridContainer.Controls.Add(anchor);
        }
    }
}
