﻿
namespace BsnCraft.Web.Common.Classes.Grid
{
    //Note: value is stored in PageSettings for reading/saving
    public struct BsnGridSetting
    {
        public string Name { get; set; }
        public string Caption { get; set; }
        public int Sort { get; set; }
    }
}
