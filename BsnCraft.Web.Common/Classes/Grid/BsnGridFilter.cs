﻿
namespace BsnCraft.Web.Common.Classes.Grid
{
    public class BsnGridFilter
    {
        public string Name { get; set; }
        public string Caption { get; set; }
        public string Parent { get; set; }
        public bool RequiresParent { get; set; }
        public bool MultiSelect { get; set; }
        public bool ShowCount { get; set; }
        public bool CanEdit { get; set; }
        public string DataKey { get; set; }
        public string DataCaption { get; set; }
        public string DataCount { get; set; }
        public dynamic DataSource { get; set; }
        public CallbackType Type { get; set; } = CallbackType.PageOnly;
        public CallbackMode Mode { get; set; } = CallbackMode.Filter;
    }
}
