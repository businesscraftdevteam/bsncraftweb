﻿using DevExpress.Web;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes.Grid
{
    public class BsnGridTemplate : ITemplate
    {
        public BsnGridColumn Column { get; private set; }
        //public EventHandler LoadEvent { get; set; }

        public BsnGridTemplate(BsnGridColumn column)
        {
            Column = column;
        }

        public void InstantiateIn(Control container)
        {
            /*
            GridViewDataItemTemplateContainer gridContainer = (GridViewDataItemTemplateContainer)container;
            var div = new HtmlGenericControl("div");
            div.Load += LoadEvent;
            gridContainer.Controls.Add(div);
            */
        }
    }
}
