﻿using DevExpress.Web;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes.Grid
{
    public class BsnGridCell : ITemplate
    {
        public EventHandler LoadEvent { get; set; }

        public void InstantiateIn(Control container)
        {
            var gridContainer = (GridViewDataItemTemplateContainer)container;
            var div = new HtmlGenericControl("div");
            div.Load += LoadEvent;
            gridContainer.Controls.Add(div);
        }
    }
}
