﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Classes.Grid;
using BsnCraft.Web.Common.Models;
using DevExpress.Data;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes.Grid
{
    public class BsnGridColumn
    {
        public string Name { get; set; }
        public string FieldName { get; set; }
        public string Caption { get; set; }
        public int Sort { get; set; }
        public int Width { get; set; }
        public List<BsnGridButton> Buttons { get; set; } = new List<BsnGridButton>();
        public ITemplate DataTemplate { get; set; }
        public object Data { get; set; }

        public string GetDataString()
        {
            string value = (Data == null) ? string.Empty : Data.ToString();
            if (Data is DateTime dateValue)
            {
                if (dateValue.Equals(DateTime.MinValue))
                {
                    value = string.Empty;
                }
                else
                {
                    value = dateValue.ToShortDateString();
                }
            }
            return value;
        }

        public Control Output
        {
            get
            {
                var div = new HtmlGenericControl("div");
                if (Buttons.Count == 0)
                {
                    div.Controls.Add(new LiteralControl(this.ToString()));
                }
                else
                {
                    foreach (var button in Buttons)
                    {
                        var caption = button.Caption;
                        var buttonType = button.Type;
                        var callback = button.Callback;
                        if (buttonType == ButtonTypes.EventRegister && Data is Event contractEvent)
                        {
                            string keyval = contractEvent.ContractNumber + ":" + contractEvent.Code;
                            button.Callback.Key = keyval;
                            button.Callback.Mode = (contractEvent.IsRegistered) ? CallbackMode.UnRegister : CallbackMode.Edit;

                            caption = (contractEvent.IsRegistered) ? Utils.StringDate(contractEvent.Actual) : Utils.StringDate(contractEvent.Forecast);
                            if (contractEvent.IsRegistered)
                            {
                                buttonType = (contractEvent.IsLate) ? ButtonTypes.Danger : ButtonTypes.Success;
                            }

                            if (string.IsNullOrEmpty(caption))
                            {
                                caption = "Register";
                            }
                        }
                        else
                        {
                            if (caption.ToLower() == "cellvalue")
                            {
                                caption = this.ToString();
                            }
                            var key = callback.Key.Replace("[cellvalue]", this.ToString());
                            callback.Key = key;
                        }
                        div.Controls.Add(new BsnButton(callback, button.Icon, caption, buttonType, button.IsButton));
                    }
                }
                return div;
            }
        }
    }
}
