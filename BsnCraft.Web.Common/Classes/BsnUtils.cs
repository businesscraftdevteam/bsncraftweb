﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Reflection;
using System.Xml.Serialization;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using NLog;
using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using Op = System.Web.Optimization;
using Img = System.Drawing.Imaging;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Buttons;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using WC = System.Web.UI.WebControls;
using System.Text;

namespace BsnCraft.Web.Common.Classes
{

    #region Misc Enums    
    public enum StyleTypes
    {
        primary,
        secondary,
        success,        
        danger,
        warning,
        info,
        light,
        dark
    }

    public enum ModalEvents
    {
        None,
        Show,
        Hide,
        Close
    }
    
    public enum PanelTypes
    {
        Grid,
        Chart,
        Dashboard,
        Report
    }

    public enum PanelZones
    {
        LeftZone,
        MainZone,
        RightZone
    }

    public enum ModalSizes
    {
        Small,
        Normal,
        Large,
        Fullscreen
    }

    public enum EstimateTypes
    {
        Sales,
        Production,
        Variation,
        EmptySales
    }
    #endregion

    public class BsnUtils
    {
        public const string PageModeKey = "PageMode";

        #region Private Properties
        private Logger Log { get; set; }
        private BsnApplication App { get; set; }
        #endregion

        #region Constructor
        public BsnUtils(BsnApplication app)
        {
            Log = LogManager.GetLogger(GetType().FullName);
            App = app;
        }
        #endregion

        #region SetError
        public void SetError(string message, bool display = true)
        {
            Log.Debug(message);
            if (display)
            {
                App.AddToast(message, true, display);
            }
        }
        #endregion

        #region SetSuccess
        public void SetSuccess(string message = "")
        {
            //TODO: add save visual
            if (!string.IsNullOrEmpty(message))
            {
                App.AddToast(message);
            }
        }
        #endregion

        #region Init
        public void Init()
        {
            styles = null;
            scripts = null;
            favIcon16 = null;
            favIcon32 = null;
            touchIcon = null;
        }
        #endregion

        #region Themes
        public string Theme
        {
            get
            {
                return App.Settings.UserSetting(UserSettingsTypes.Theme).DefaultString("Default");
            }
        }

        public bool DarkTheme
        {
            get
            {
                if (!Themes.ContainsKey(Theme))
                {
                    SetError("Could not find theme: " + Theme);
                    return false;
                }

                return Themes[Theme].Item1;
            }
        }

        public bool CustomTheme
        {
            get
            {
                return (Theme != "Dark" && Theme != "Default");
            }
        }

        private Dictionary<string, (bool, string)> themes;
        public Dictionary<string, (bool, string)> Themes
        {
            get
            {
                if (themes == null)
                {
                    themes = new Dictionary<string, (bool, string)>()
                    {
                        { "Default", (false, "1997c6") },
                        { "Dark", (true, "1997c6") },
                        { "Bootstrap", (false, "007bff") },
                        { "Cerulean", (false, "2fa4e7") },
                        { "Cosmo", (false, "2780e3") },
                        { "Cyborg", (true, "060606") },
                        { "Darkly", (true, "00bc8c") },
                        { "Flatly", (false, "dce4ec") },
                        { "Journal", (false, "eb6864") },
                        { "Litera", (false, "4582ec") },
                        { "Lumen", (false, "158cba") },
                        { "Lux", (false, "1a1a1a") },
                        { "Materia", (false, "2196f3") },
                        { "Minty", (false, "78c2ad") },
                        { "Pulse", (false, "593196") },
                        { "Sandstone", (false, "93c54b") },
                        { "Simplex", (false, "d9230f") },
                        { "Sketchy", (false, "333") },
                        { "Slate", (true, "484e55") },
                        { "Solar", (true, "b58900") },
                        { "Spacelab", (false, "446e9b") },
                        { "Superhero", (true, "df691a") },
                        { "United", (false, "e95420") },
                        { "Yeti", (false, "008cba") }
                    };
                }
                return themes;
            }
        }
        #endregion

        #region Styles
        private LiteralControl styles;
        public LiteralControl Styles(bool isApplication = false)
        {
            if (styles == null)
            {
                var css = CssFolder(false);
                var fa = "~/assets/fa/";
                var toolkit = DarkTheme ? "toolkit-inverse.min.css" : "toolkit-light.min.css";
                var list = new List<string>()
                {
                    css + toolkit,                    
                    css + "animate.min.css",
                    fa + "all.min.css",
                    fa + "brands.min.css",
                    fa + "fontawesome.min.css",
                    fa + "light.min.css",
                    fa + "regular.min.css",
                    fa + "solid.min.css",
                    fa + "v4-shims.min.css",
                    css + "featherlight.min.css",
                    css + "nprogress.css",
                    css + "jquery-confirm.min.css",
                    //css + "jquery-ui.min.css"             //Not needed, Chart script error troubleshooting
                };
                if (isApplication)
                {
                    list.Add(css + "application.css");
                }
                if (CustomTheme)
                {
                    list.Add(css + "themes/" + Theme.ToLower() + "/bootstrap.min.css");
                }
                Op.BundleTable.Bundles.Add(new Op.StyleBundle(css + "styles").Include(list.ToArray()));
                styles = new LiteralControl(Op.Styles.Render(css + "styles").ToHtmlString());
            }
                return styles;
        }
        #endregion

        #region FavIcons
        private HtmlGenericControl favIcon16;
        public HtmlGenericControl FavIcon16
        {
            get
            {
                if (favIcon16 == null)
                {
                    favIcon16 = new HtmlGenericControl("link");
                    favIcon16.Attributes["rel"] = "icon";
                    favIcon16.Attributes["type"] = "image/png";
                    favIcon16.Attributes["sizes"] = "16x16";
                    favIcon16.Attributes["href"] = AssetsFolder() + "favicon-16x16.png";
                }
                return favIcon16;
            }
        }

        private HtmlGenericControl favIcon32;
        public HtmlGenericControl FavIcon32
        {
            get
            {
                if (favIcon32 == null)
                {
                    favIcon32 = new HtmlGenericControl("link");
                    favIcon32.Attributes["rel"] = "icon";
                    favIcon32.Attributes["type"] = "image/png";
                    favIcon32.Attributes["sizes"] = "32x32";
                    favIcon32.Attributes["href"] = AssetsFolder() + "favicon-32x32.png";
                }
                return favIcon32;
            }
        }

        private HtmlGenericControl touchIcon;
        public HtmlGenericControl TouchIcon
        {
            get
            {
                if (touchIcon == null)
                {
                    touchIcon = new HtmlGenericControl("link");
                    touchIcon.Attributes["rel"] = "apple-touch-icon";
                    touchIcon.Attributes["sizes"] = "180x180";
                    touchIcon.Attributes["href"] = AssetsFolder() + "apple-touch-icon.png";
                }
                return touchIcon;
            }
        }
        #endregion

        #region Scripts
        private LiteralControl scripts;
        public LiteralControl Scripts
        {
            get
            {
                if (scripts == null)
                {
                    var js = JsFolder(false);
                    var list = new List<string>()
                    {
                        js + "jquery.min.js",                        
                        js + "popper.min.js",
                        //js + "jquery-ui.min.js",              //Not needed, Chart script error troubleshooting
                        js + "jquery.autocomplete.min.js",          
                        js + "jquery.sticky-kit.min.js",
                        js + "jquery.confirm.min.js",
                        js + "bootstrap.min.js",
                        js + "featherlight.min.js",
                        js + "nprogress.js",
                        js + "readmore.min.js",                        
                        js + "application.js"
                    };
                    Op.BundleTable.Bundles.Add(new Op.ScriptBundle(js + "scripts").Include(list.ToArray()).ForceOrdered());
                    scripts = new LiteralControl(Op.Scripts.Render(js + "scripts").ToHtmlString());
                }
                return scripts;
            }
        }
        #endregion

        #region Folders
        public string AssetsFolder(bool resolveUrl = true)
        {
            var assets = "~/assets/";
            if (resolveUrl)
            {
                assets = Helpers.ResolveUrl(assets);
            }
            return assets;
        }

        public string CssFolder(bool resolveUrl = true)
        {
            return AssetsFolder(resolveUrl) + "css/";
        }

        public string JsFolder(bool resolveUrl = true)
        {
            return AssetsFolder(resolveUrl) + "js/";
        }

        public string HomeFolder
        {
            get
            {
                string homeFolder = Helpers.ResolveUrl("~/home/" + App.Api.Company + "/");
                try
                {
                    if (!Directory.Exists(homeFolder.MapPath()))
                    {
                        Directory.CreateDirectory(homeFolder.MapPath());
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return homeFolder;
            }
        }

        public string HelpFolder
        {
            get
            {
                string helpFolder = Helpers.ResolveUrl("~/home/Documentation/");
                try
                {
                    if (!Directory.Exists(helpFolder.MapPath()))
                    {
                        Directory.CreateDirectory(helpFolder.MapPath());
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return helpFolder;
            }
        }

        public string DocsFolder
        {
            get
            {
                string docsFolder = HomeFolder + "documents/";
                try
                {
                    if (!Directory.Exists(docsFolder.MapPath()))
                    {
                        Directory.CreateDirectory(docsFolder.MapPath());
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return docsFolder;
            }
        }

        public string UploadFolder
        {
            get
            {
                string uploadFolder = HomeFolder + "upload/";
                try
                {
                    if (!Directory.Exists(uploadFolder.MapPath()))
                    {
                        Directory.CreateDirectory(uploadFolder.MapPath());
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return uploadFolder;
            }
        }

        public string DownloadFolder
        {
            get
            {
                string downloadFolder = HomeFolder + "download/";
                try
                {
                    if (!Directory.Exists(downloadFolder.MapPath()))
                    {
                        Directory.CreateDirectory(downloadFolder.MapPath());
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return downloadFolder;
            }
        }

        public string ImagesFolder
        {
            get
            {
                string imagesFolder = HomeFolder + "images/";
                try
                {
                    if (!Directory.Exists(imagesFolder.MapPath()))
                    {
                        Directory.CreateDirectory(imagesFolder.MapPath());
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return imagesFolder;
            }
        }

        public string ReportsFolder
        {
            get
            {
                string reportsFolder = HomeFolder + "reports/";
                try
                {
                    if (!Directory.Exists(reportsFolder.MapPath()))
                    {
                        Directory.CreateDirectory(reportsFolder.MapPath());
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return reportsFolder;
            }
        }

        public string ExportFolder
        {
            get
            {
                string exportFolder = HomeFolder + "export/";
                try
                {
                    if (!Directory.Exists(exportFolder.MapPath()))
                    {
                        Directory.CreateDirectory(exportFolder.MapPath());
                    }
                }
                catch (Exception e)
                {
                    LogExc(e);
                }
                return exportFolder;
            }
        }
        #endregion
        
        #region Get Mode Callback
        public BsnCallback GetKeyCallback(BsnModes mode, string key = "")
        {
            return GetModeCallback(mode, BsnActions.Default, key);
        }

        public BsnCallback GetModeCallback(BsnModes mode, BsnActions action = BsnActions.Default, string key = "#")
        {
            return new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Menu = App.Menu,
                Action = (action == BsnActions.Default) ? App.Action : action,
                Id = App.Id,
                Mode = mode,
                Key = (key == "#") ? App.Key : key
            };
        }
        #endregion

        #region LogExc
        public void LogExc(Exception ex, bool showError = true, string extras = "")
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;

            Log.Error(message);

            if (showError)
            {
                var toast = ex.Message;
                if (string.IsNullOrEmpty(extras))
                {
                    extras = ex.StackTrace;
                }                
                if (!string.IsNullOrEmpty(extras))
                {
                    toast += " " + extras;
                }
                App.AddToast(toast, true);
            }
        }
        #endregion        
        
        #region ViewFile
        public void ViewFile(string filename, string title, BootstrapCallbackPanel panel)
        {
           
        }
        #endregion
                
        #region GetRecords (xfServerPlus)
        public List<T> GetRecords<T>(string BsnTable, string BsnParams, int MaxRecs = 0)
        {
            if (!App.Api.Connected)
            {
                return null;
            }

            string BsnResult = App.Api.GetData(BsnTable, BsnParams, typeof(T), MaxRecs);
            var BsnRecords = new List<T>();

            try
            {
                AddtoListFromString(BsnRecords, BsnResult);
            }
            catch (Exception e)
            {
                LogExc(e);
            }

            return BsnRecords;
        }

        public string GetFieldList(Type elemType)
        {
            var s = string.Empty;
            var properties = elemType.GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(p => (!p.GetCustomAttributes(false).Any(a => a is XmlIgnoreAttribute)));
            foreach (var p in properties)
            {
                var attr = (DescriptionAttribute[])p.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attr != null && attr.Length > 0)
                {
                    s = s + attr[0].Description + ",";
                }
                else
                {
                    s = s + p.Name + ",";
                }

            }
            s = s.Remove(s.Length - 1);
            return s;
        }

        public void AddtoListFromString<T>(List<T> list, string s)
        {
            AddtoListFromString(list, s, new string[] { "" + (char)15 }, new string[] { "" + (char)14 });
        }

        public void AddtoListFromString<T>(List<T> list, string s, string[] recordDelim, string[] fieldDelim)
        {
            try
            {
                PropertyInfo[] pi = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .Where(p => (!p.GetCustomAttributes(false).Any(a => a is XmlIgnoreAttribute))).ToArray();

                string[] fields;
                string[] rows = s.Split(recordDelim, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < rows.GetLength(0); i++)
                {
                    fields = rows[i].Split(fieldDelim, StringSplitOptions.None);
                    object obj = (T)Activator.CreateInstance(typeof(T));
                    LoadObjFromStringArray(fields, pi, ref obj);
                    list.Add((T)obj);
                }
            }
            catch (Exception)
            { }
        }

        public static void LoadObjFromStringArray<T>(string[] fields, PropertyInfo[] pi, ref T obj)
        {
            decimal d;
            int tmpInt;
            long tmpInt64;
            DateTime date;
            for (int f = 0; f < fields.GetLength(0); f++)
            {
                if (pi[f].PropertyType == typeof(string))
                {
                    pi[f].SetValue(obj, fields[f].TrimEnd(), null);
                    continue;
                }
                if (pi[f].PropertyType == typeof(decimal))
                {
                    decimal.TryParse(fields[f], out d);
                    pi[f].SetValue(obj, d, null);
                    continue;
                }
                if (pi[f].PropertyType == typeof(DateTime))
                {
                    if (!string.IsNullOrEmpty(fields[f]))
                    {
                        DateTime.TryParse(fields[f], out date);
                        pi[f].SetValue(obj, date, null);
                    }
                    continue;

                }
                if (pi[f].PropertyType == typeof(long))
                {
                    decimal.TryParse(fields[f], out d);
                    tmpInt64 = (long)d;
                    pi[f].SetValue(obj, tmpInt64, null);
                    continue;
                }
                if (pi[f].PropertyType == typeof(DateTime?))
                {
                    DateTime.TryParse(fields[f], out date);
                    DateTime? ndate = new DateTime?(date);
                    pi[f].SetValue(obj, ndate, null);
                    continue;
                }
                if (pi[f].PropertyType == typeof(int))
                {
                    if (fields[f].GetType() == typeof(int))
                    {
                        int.TryParse(fields[f], out tmpInt);
                    }
                    else
                    {
                        decimal.TryParse(fields[f], out d);
                        tmpInt = (int)d;
                    }

                    pi[f].SetValue(obj, tmpInt, null);
                    continue;
                }
            }
        }
        #endregion        
        
        #region Serialize
        public string ObjectToString<T>(T obj)
        {
            XmlSerializer ser = XmlSerializer.FromTypes(new[] { typeof(T) })[0];
            TextWriter tw = new StringWriter();
            ser.Serialize(tw, obj);
            return tw.ToString();
        }

        public T StringToObject<T>(string settingString) where T : new()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(settingString)) return new T();
                using (TextReader reader = new StringReader(settingString))
                {
                    XmlSerializer xmlSerial = XmlSerializer.FromTypes(new[] { typeof(T) })[0];
                    return (T)xmlSerial.Deserialize(reader);
                }
            }
            catch (Exception e)
            {

                LogExc(e);
                return default(T);

            }
        }
        #endregion

        #region RemoveFileWithDelay
        public void RemoveFileWithDelay(string key, string fullPath, int delay)
        {
            RemoveFileWithDelayInternal(key, fullPath, delay, FileSystemRemoveAction);
        }

        const string RemoveTaskKeyPrefix = "DXRemoveTask_";
        void RemoveFileWithDelayInternal(string fileKey, object fileData, int delay, CacheItemRemovedCallback removeAction)
        {
            string key = RemoveTaskKeyPrefix + fileKey;
            if (HttpRuntime.Cache[key] == null)
            {
                DateTime absoluteExpiration = DateTime.UtcNow.Add(new TimeSpan(0, delay, 0));
                HttpRuntime.Cache.Insert(key, fileData, null, absoluteExpiration,
                    Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, removeAction);
            }
        }

        void FileSystemRemoveAction(string key, object value, CacheItemRemovedReason reason)
        {
            try
            {
                string fileFullPath = value.ToString();
                if (File.Exists(fileFullPath))
                    File.Delete(fileFullPath);
            }
            catch (Exception e)
            {
                LogExc(e, true);
            }
        }
        #endregion
        
        #region CreateThumbnail
        private byte[] noImageBytes;
        public byte[] CreateThumbnail(string path, int maxWidth, int maxHeight)
        {            
            byte[] buffer = null;
            Image img = null;
            try
            {
                if (maxHeight == 0 || maxWidth == 0)
                {
                    img = ScaleImage(Image.FromFile(path), maxWidth, maxHeight);
                }
                else
                {
                    img = FixedSize(Image.FromFile(path), maxWidth, maxHeight);
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    img.Save(ms, Img.ImageFormat.Png);
                    buffer = ms.ToArray();
                }                
            }
            catch
            {
                if (noImageBytes != null)
                {
                    buffer = noImageBytes;
                }
                else
                {
                    var thumb = App.Settings.Comp(CompanySettingsType.ThumbnailDefault).ToString();
                    if (string.IsNullOrEmpty(thumb))
                    {
                        thumb = "~/assets/noimage.png";
                    }
                    var noImage = Helpers.ResolveUrl(thumb);
                    path = HttpContext.Current.Server.MapPath(noImage);
                    img = FixedSize(Image.FromFile(path), maxWidth, maxHeight);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        img.Save(ms, Img.ImageFormat.Png);
                        buffer = ms.ToArray();
                        noImageBytes = buffer;
                    }
                }
            }
            return buffer;
        }

        private Image ScaleImage(Image imgPhoto, int maxWidth, int maxHeight)
        {
            int newHeight = imgPhoto.Height;
            int newWidth = imgPhoto.Width;

            if (maxWidth > 0 && newWidth > maxWidth) //WidthResize
            {
                decimal divider = Math.Abs(newWidth / (decimal)maxWidth);
                newWidth = maxWidth;
                newHeight = (int)Math.Round(newHeight / divider);
            }
            if (maxHeight > 0 && newHeight > maxHeight) //HeightResize
            {
                decimal divider = Math.Abs(newHeight / (decimal)maxHeight);
                newHeight = maxHeight;
                newWidth = (int)Math.Round(newWidth / divider);
            }

            //Log.Debug("ScaleImage| NewWidth = " + newWidth.ToString() + ", NewHeight = " + newHeight.ToString());
            return FixedSize(imgPhoto, newWidth, newHeight);
        }

        private Image FixedSize(Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = Width / (float)sourceWidth;
            nPercentH = Height / (float)sourceHeight;
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = Convert.ToInt16((Width - (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = Convert.ToInt16((Height - (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format32bppArgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Transparent);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //Log.Debug("FixedSize| DestWidth = " + destWidth.ToString() + ", DestHeight = " + destHeight.ToString());

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }
        #endregion

        #region CopyProperties
        public static void CopyProperties(object dest, object src)
        {
            foreach (PropertyDescriptor item in TypeDescriptor.GetProperties(src))
            {
                if (item != null && !item.IsReadOnly)
                {
                    item.SetValue(dest, item.GetValue(src));
                }
            }
        }
        #endregion

        #region Menu Reports
        public Dictionary<string, ComboItem> MenuReports
        {
            get
            {
                var menuReports = new Dictionary<string, ComboItem>();
                var reportPath = ReportsFolder.MapPath() + App.Menu + "/" + App.Action;
                if (Directory.Exists(reportPath))
                {
                    var files = Directory.GetFiles(reportPath, "*.rpt");
                    var i = 0;
                    foreach (var filename in files)
                    {
                        var report = Path.GetFileNameWithoutExtension(filename);
                        menuReports.Add(i.ToString(), new ComboItem(report, filename));
                        i++;
                    }
                }
                return menuReports;
            }
        }
        #endregion

        #region Calendar
        public Control Calendar(bool multiSelect = true)
        {
            var uid = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
            var calendar = new BootstrapCalendar()
            {
                ID = uid,
                ClientInstanceName = "Calendar",
                EnableMultiSelect = multiSelect
            };
            return calendar;
        }
        #endregion

        #region Report Selector
        public Control ReportSelector
        {
            get
            {
                var uid = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
                var reportSelector = new BootstrapGridView()
                {
                    ID = uid,                    
                    KeyFieldName = "Key"
                };                
                reportSelector.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
                reportSelector.Settings.GridLines = WC.GridLines.None;
                reportSelector.SettingsBehavior.AllowFocusedRow = true;
                reportSelector.SettingsBehavior.AllowSelectByRowClick = true;
                reportSelector.SettingsBehavior.AllowSelectSingleRowOnly = true;
                reportSelector.Settings.ShowColumnHeaders = false;
                var select = new BootstrapGridViewCommandColumn()
                {
                    ShowSelectCheckbox = true,
                    Width = WC.Unit.Pixel(45)
                };
                select.CssClasses.Cell = "py-2";
                reportSelector.Columns.Add(select);
                reportSelector.Columns.Add(new BootstrapGridViewDataColumn()
                {
                    FieldName = "Value.Code",
                    Width = WC.Unit.Percentage(100)
                });                
                var selectJs = "function (s, e) { App.report(s, e); }";
                reportSelector.ClientSideEvents.SelectionChanged = selectJs;
                reportSelector.DataSource = MenuReports;
                reportSelector.DataBind();
                reportSelector.FocusedRowIndex = -1;
                reportSelector.Selection.UnselectAll();
                return reportSelector;
            }
        }
        #endregion

        #region ExportCrystal
        public void ExportCrystal()
        {
            try
            {
                if (!MenuReports.ContainsKey(App.ReportKey))
                {
                    SetError("Invalid Report Selected");
                    return;
                }
                var reportFile = MenuReports[App.ReportKey].Desc;
                var report = new ReportDocument();
                report.Load(reportFile);
                SetupCrystal(report);
                foreach (ReportDocument subreport in report.Subreports)
                {
                    SetupCrystal(subreport);
                }
                var uid = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
                var exportFile = ExportFolder + uid + ".pdf";
                report.ExportToDisk(ExportFormatType.PortableDocFormat, exportFile.MapPath());
                report.Close();
                report = null;
                App.Menus.Current.ViewReport = exportFile;
                var location = App.Menus.Current.GetLocation();
                location.Mode = BsnModes.ViewReport;
                App.DataLocation = location;
            }
            catch (Exception ex)
            {
                LogExc(ex);
            }
        }
        #endregion

        #region SetupCrystal
        public void SetupCrystal(ReportDocument report)
        {
            var formulaName = string.Empty;
            var formulaValue = string.Empty;            
            var dsn = App.Odbc.BsnDSN.Replace("DSN=", "");
            var reportParams = App.Menus.Current.ReportParams
                .Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            
            foreach (string formula in reportParams)
            {
                var i = formula.IndexOf(("="));
                if (i > 0)
                {
                    formulaName = formula.Substring(0, i);
                    formulaValue = formula.Substring(i + 1);
                    foreach (FormulaFieldDefinition Formula in report.DataDefinition.FormulaFields)
                    {
                        if (Formula.Name.ToUpper() == formulaName.ToUpper())
                        {
                            Formula.Text = formulaValue;
                        }
                    }
                }
            }            
            foreach (IConnectionInfo crDataSourceConnection in report.DataSourceConnections)
            {
                if (crDataSourceConnection.ServerName.ToUpper().Contains("XFODBC"))
                {
                    crDataSourceConnection.SetConnection(dsn, "", "DBA", "MANAGER");
                }
            }
        }
        #endregion

        #region ViewReport
        public string ViewReport(string keystring)
        {
            string[] keyval = keystring.Split(':');
            string key = Helpers.GetArray(keyval, 0);
            string basepath = Helpers.GetArray(keyval, 1);
            string report = Helpers.GetArray(keyval, 2);
            string path = path = HttpContext.Current.Server.MapPath(basepath + "/pdf/");
            string pdfFile = path + report + " " + key + ".pdf";
            return basepath + "/pdf/" + Path.GetFileName(pdfFile);
        }
        #endregion

        #region Custom Modal
        //public HtmlGenericControl CustomModal(Control content, ModalOptions options)
        //{
        //    var dialog = new HtmlGenericControl("div")
        //    {
        //        ID = options.Id,
        //        ClientIDMode = ClientIDMode.Static
        //    };
        //    dialog.Attributes["class"] = "modal fade";
        //    dialog.Attributes["tabindex"] = "-1";
        //    dialog.Attributes["role"] = "dialog";
        //    dialog.Attributes["aria-labelledby"] = options.Id + "Label";
        //    dialog.Attributes["aria-hidden"] = "true";

        //    var document = new HtmlGenericControl("div");
        //    document.Attributes["class"] = "modal-dialog";
        //    if (!string.IsNullOrEmpty(options.Size))
        //    {
        //        document.Attributes["class"] += " modal-" + options.Size;
        //    }
        //    document.Attributes["role"] = "document";
        //    dialog.Controls.Add(document);

        //    var container = new HtmlGenericControl("div");
        //    container.Attributes["class"] = "modal-content";
        //    document.Controls.Add(container);

        //    if (options.ShowHeader)
        //    {
        //        var header = new HtmlGenericControl("div");
        //        header.Attributes["class"] = "modal-header";
        //        container.Controls.Add(header);

        //        var label = new HtmlGenericControl("h5")
        //        {
        //            ID = options.Id + "Label",
        //            ClientIDMode = ClientIDMode.Static
        //        };
        //        label.Attributes["class"] = "modal-title";
        //        label.Controls.Add(new LiteralControl(options.Title));
        //        header.Controls.Add(label);

        //        var close = new HtmlGenericControl("button");
        //        close.Attributes["type"] = "button";
        //        close.Attributes["class"] = "close";
        //        close.Attributes["data-dismiss"] = "modal";
        //        close.Attributes["aria-label"] = "Close";
        //        header.Controls.Add(close);

        //        var icon = new HtmlGenericControl("span");
        //        icon.Attributes["aria-hidden"] = "true";
        //        icon.InnerHtml = "&times;";
        //        close.Controls.Add(icon);                
        //    }

        //    var body = new HtmlGenericControl("div");
        //    body.Attributes["class"] = "modal-body";
        //    container.Controls.Add(body);

        //    body.Controls.Add(content);

        //    //TODO: add footer            
        //    return dialog;
        //}
        #endregion

    }
}