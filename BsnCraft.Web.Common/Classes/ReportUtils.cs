﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;

namespace BsnCraft.Web.Common.Classes
{
    public class ReportUtils
    {
        #region BsnExportPDF
        public static void BsnExportPDF(string ReportName, string ReportDSN, string ExportFile, string sFormulaReplaceString, out string errmsg)
        {
            string sprogress = string.Empty;
            try
            {
                sprogress = "about to create new document";
                ReportDocument reportDocument = new ReportDocument();
                sprogress = "About to load document " + ReportName.Trim();
                reportDocument.Load(ReportName.Trim());
                sprogress = "about to export to " + ExportFile;

                string[] sFormulas = sFormulaReplaceString.Trim().Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                BSNSetupReport(reportDocument, ReportDSN, sFormulas);
                foreach (ReportDocument rptDoc in reportDocument.Subreports)
                {
                    BSNSetupReport(rptDoc, ReportDSN, sFormulas);
                }

                reportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, ExportFile.Trim());
                sprogress = "about to close";
                reportDocument.Close();
                sprogress = "about to set to null";
                reportDocument = null;
                sprogress = "end of export";
                errmsg = string.Empty;
            }
            catch (Exception ex)
            {
                errmsg = "Exception: " + ex.Message + "\r\nInner Exception: " + ex.InnerException + "\r\nProgress: " + sprogress;
            }
        }
        #endregion

        #region BSNSetupReport
        public static void BSNSetupReport(ReportDocument rptDoc, string ReportDSN, string[] sFormulas)
        {
            string sFormulaName = string.Empty;
            string sFormulaValue = string.Empty;
            int iptr;

            foreach (string sFormula in sFormulas)
            {
                iptr = sFormula.IndexOf(("="));
                if (iptr > 0)
                {
                    sFormulaName = sFormula.Substring(0, iptr);
                    sFormulaValue = sFormula.Substring(iptr + 1);
                    foreach (FormulaFieldDefinition Formula in rptDoc.DataDefinition.FormulaFields)
                    {
                        if (Formula.Name.ToUpper() == sFormulaName.ToUpper())
                        {
                            Formula.Text = sFormulaValue;
                        }
                    }
                }
            }
            if (ReportDSN.Trim().Length > 0)
            {
                foreach (IConnectionInfo crDataSourceConnection in rptDoc.DataSourceConnections)
                {
                    if (crDataSourceConnection.ServerName.ToUpper().Contains("XFODBC"))
                    {
                        crDataSourceConnection.SetConnection(ReportDSN.Trim(), "", "DBA", "MANAGER");
                    }
                }
            }
        }
        #endregion
    }
}
