﻿using BsnCraft.Web.Common.Models;
using BusinessCraft;
using NLog;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using BsnCraft.Web.Common.Settings;
using System.Collections.Generic;
using System.Collections;
using BsnCraft.Shared.DTO.Classes;
using Newtonsoft.Json;
using V = BsnCraft.Shared.DTO.View;
using M = BsnCraft.Shared.DTO.Model;

namespace BsnCraft.Web.Common.Classes
{

    public class BsnApi
    {
        #region Private Properties
        private Logger Log { get; set; }
        private BsnApplication App { get; set; }        
        #endregion

        #region Constructor
        public BsnApi(BsnApplication app)
        {
            Log = LogManager.GetLogger(GetType().FullName);
            App = app;
            Init();
        }
        #endregion

        #region Init
        public void Init()
        {
            Disconnect();
            connected = false;
            ticket = null;
            companyName = string.Empty;
        }
        #endregion

        #region Disconnect
        public void Disconnect()
        {
            if (web != null)
            {
                web.disconnect();
            }
            //Log.Debug("BsnDisconnect| Successully disconnected.");
        }
        #endregion

        #region Ticket
        private Xf_authticket ticket;
        private Xf_authticket Ticket
        {
            get
            {
                return ticket;
            }
        }        
        #endregion

        #region BsnWeb
        private BsnWeb web;
        private BsnWeb Web
        {
            get
            {
                if (web == null)
                {
                    web = new BsnWeb();                    
                }
                
                //check connection active
                object connection = web.getConnect();
                if (connection != null)
                {
                    return web;
                }

                try
                {
                    web.connect(App.Settings.Application.BsnHost, App.Settings.Application.BsnPort);
                }
                catch (Exception e)
                {
                    App.Utils.LogExc(e);
                }
                return web;
            }
        }
        #endregion

        #region Username
        public string Username
        {
            get
            {
                if (Connected)
                {
                    return Ticket.At_user_name;
                }
                return string.Empty;
            }
        }
        #endregion

        #region Initials
        private string initials = string.Empty;
        public string Initials
        {
            get
            {
                return initials;
            }
        }
        #endregion

        #region FullName
        private string fullName = string.Empty;
        public string FullName
        {
            get
            {
                if (string.IsNullOrEmpty(fullName))
                {
                    return Username;
                }
                return fullName;
            }
        }
        #endregion

        #region Company
        public string Company
        {
            get
            {
                if (Connected)
                {
                    return Ticket.At_comp_name;
                }
                return string.Empty;
            }
        }
        #endregion

        #region CompanyName
        private string companyName = string.Empty;
        public string CompanyName
        {
            get
            {
                if (string.IsNullOrEmpty(companyName))
                {
                    companyName = App.Settings.Companies.Where(p => p.Code.Equals(Company)).FirstOrDefault().Desc;
                }
                return companyName;
            }
        }
        #endregion

        #region AllContracts
        public bool AllContracts { get; set; }
        #endregion

        #region Login
        public void Login(string username, string password, string company, string companypw = "")
        {
            var computername = Dns.GetHostName();
            computername += ":" + App.Settings.Application.BsnPort.ToString();
            var computerip = GetIPAddress();
            var sessionid = 0;
            var connectid = 0;
            var bsnerror = string.Empty;
            var bsnparams = string.Empty;

            username = username.ToUpper();
            if (App.Settings.Application.UppercasePassword)
            {
                password = password.ToUpper();
            }

            try
            {
                if (ticket == null)
                {
                    ticket = new Xf_authticket();
                }

                Log.Debug("BsnLogin| username=" + username + " company=" + company);
                int success = Web.qr_login(username, password, ref fullName, ref initials, company, companypw,
                    computername, computerip, ref sessionid, ref connectid, ref ticket, ref bsnerror, ref bsnparams);
                
                if (success == 0 && string.IsNullOrEmpty(bsnerror))
                {
                    var dictionary = bsnparams.Split(new char[] { '\r' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(part => part.Split('\t'))
                        .Where(part => part.Length == 2)
                        .ToDictionary(sp => sp[0], sp => sp[1]);
                    AllContracts = (dictionary["ALLCONTR"] == "Y");
                    connected = true;
                    SetCookie();
                    App.Settings.Load();
                    App.LastTab();
                    return;
                }
                App.Utils.SetError(bsnerror, true);
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region GetIPAddress
        public string GetIPAddress()
        {
            IPAddress ip = Dns.GetHostAddresses(Dns.GetHostName()).Where(address =>
            address.AddressFamily == AddressFamily.InterNetwork).First();
            return ip.ToString();
        }
        #endregion

        #region Logout
        public void Logout()
        {
            if (web != null)
            {
                if (ticket != null)
                {
                    string bsnerror = string.Empty;
                    try
                    {
                        int success = Web.qr_logout(Ticket, ref bsnerror);
                        if (success != 0)
                        {
                            App.Utils.SetError(bsnerror);
                        }
                    }
                    catch (Exception e)
                    {
                        App.Utils.LogExc(e);
                    }
                }
            }
            FormsAuthentication.SignOut();
            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            connected = false;
        }
        #endregion

        #region Connected
        private bool connected;
        public bool Connected
        {
            get
            {
                if (connected)
                {                    
                    return true;
                }

                if (HttpContext.Current.User == null ||
                    !HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    return false;
                }

                if (ticket == null)
                {
                    //Restore cookie
                    ticket = new Xf_authticket();
                    var keyval = HttpContext.Current.User.Identity.Name.Split('|');
                    ticket.At_user_name = Helpers.GetArray(keyval, 0);
                    ticket.At_user_pwd = Decrypt(Helpers.GetArray(keyval, 1));
                    ticket.At_connect_id = int.Parse(Helpers.GetArray(keyval, 2));
                    ticket.At_comp_name = Helpers.GetArray(keyval, 3);
                    ticket.At_comp_pwd = Decrypt(Helpers.GetArray(keyval, 4));                        
                    fullName = Helpers.GetArray(keyval, 5);                        
                    initials = Helpers.GetArray(keyval, 6);
                    string allContracts = Helpers.GetArray(keyval, 7);
                    AllContracts = (allContracts == "Y");
                }

                var bsnparams = string.Empty;
                var bsnerror = string.Empty;
                int success = Web.qr_get_server_time(Ticket, ref bsnerror, ref bsnerror, ref bsnerror);
                if (success == 0)
                {
                    Log.Info("Rejoin active session");
                    connected = true;
                    App.Settings.Load();
                    App.LastTab();
                }
                else
                {
                    Log.Info("Expired sesssion");
                    FormsAuthentication.SignOut();
                    var user = new GenericIdentity(string.Empty);
                    HttpContext.Current.User = new GenericPrincipal(user, null);
                    ticket = null;
                    connected = false;                    
                }
                return connected;
            }
        }
        #endregion

        #region SetCookie
        private void SetCookie()
        {
            LogManager.Configuration.Variables["user"] = Ticket.At_user_name + "|";
            string userData = Username + "|";
            userData += Encrypt(Ticket.At_user_pwd) + "|";
            userData += Ticket.At_connect_id + "|";
            userData += Company + "|";
            userData += Encrypt(Ticket.At_user_pwd) + "|";
            userData += FullName + "|" + Initials + "|";
            userData += AllContracts ? "Y" : "N" + "|";
            var user = new GenericIdentity(userData);
            HttpContext.Current.User = new GenericPrincipal(user, null);
            FormsAuthentication.SetAuthCookie(userData, true);
            return;
        }
        #endregion

        #region Encrypt/Decrypt
        private string PasswordKey
        {
            get
            {
                /*
                var salt = VarUtils.Str(WebVars.PasswordKey);
                if (string.IsNullOrEmpty(salt))
                {
                    salt = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
                    VarUtils.Set(WebVars.PasswordKey, salt);
                }
                return salt;
                */
                return "1stFloor270TurtonRoadNewLambtonNSW2305";
            }
        }

        private string Encrypt(string strToEncrypt)
        {
            return Encrypt(strToEncrypt, PasswordKey);
        }

        private string Encrypt(string strToEncrypt, string key)
        {
            try
            {
                TripleDESCryptoServiceProvider objDESCrypto = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();
                byte[] byteHash, byteBuff;
                byteHash = objHashMD5.ComputeHash(Encoding.ASCII.GetBytes(key));
                objHashMD5 = null;
                objDESCrypto.Key = byteHash;
                objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB
                objDESCrypto.Padding = PaddingMode.PKCS7;
                byteBuff = Encoding.UTF8.GetBytes(strToEncrypt);
                return Convert.ToBase64String(objDESCrypto.CreateEncryptor().
                    TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
            return string.Empty;
        }

        private string Decrypt(string strEncrypted)
        {
            return Decrypt(strEncrypted, PasswordKey);
        }

        private string Decrypt(string strEncrypted, string key)
        {
            try
            {
                TripleDESCryptoServiceProvider objDESCrypto = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();
                byte[] byteHash, byteBuff;
                byteHash = objHashMD5.ComputeHash(Encoding.ASCII.GetBytes(key));
                objHashMD5 = null;                
                objDESCrypto.Key = byteHash;
                objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB
                objDESCrypto.Padding = PaddingMode.PKCS7;
                byteBuff = Convert.FromBase64String(strEncrypted);
                string strDecrypted = Encoding.UTF8.GetString
                (objDESCrypto.CreateDecryptor().TransformFinalBlock
                (byteBuff, 0, byteBuff.Length));
                objDESCrypto = null;
                return strDecrypted;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
                Logout();
            }
            return strEncrypted;
        }
        #endregion

        #region WebData
        public bool WebData(ref string request, ref string response)
        {
            try
            {
                var success = Web.qr_web_data(Ticket, ref request, ref response);
                if (success != 0)
                {
                    App.Utils.SetError("qr_web_data response: " + response);
                }
                return (success == 0);
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region ConfigRead
        public string ConfigRead(string level, string code, string id)
        {
            var value = string.Empty;
            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;

                //Add an extra safety around settings to make sure we're connected
                int success = Web.qr_get_server_time(Ticket, ref bsnparams, ref bsnparams, ref bsnerror);
                if (success == 0)
                {
                    success = Web.qr_cfg_read_settings(Ticket, level, code, id, "N", ref value, ref bsnparams, ref bsnerror);
                    if (success != 0)
                    {
                        App.Utils.SetError(bsnerror);
                    }
                }                
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return value;
        }
        #endregion

        #region ConfigSave
        public void ConfigSave(string level, string code, string id, string value)
        {
            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;

                //Add an extra safety around settings to make sure we're connected
                int success = Web.qr_get_server_time(Ticket, ref bsnparams, ref bsnparams, ref bsnerror);
                if (success == 0)
                {
                    success = Web.qr_cfg_savestring(Ticket, level, code, id, "N", value, ref bsnparams, ref bsnerror);
                    if (success != 0)
                    {
                        App.Utils.SetError(bsnerror);
                    }
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region SaveSpec
        public void SaveSpec(string filename, string keyval, string value)
        {
            string spec = value;

            try
            {
                string bsnerror = string.Empty;
                int action = 1; //Write

                int success = Web.qr_memo_string(Ticket, action, filename, ref spec, keyval,
                    null, null, null, null, null, null, null, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region GetSpec
        public string GetSpec(string filename, string keyval)
        {
            string spec = string.Empty;

            try
            {
                string bsnerror = string.Empty;
                int action = 0; //Read

                int success = Web.qr_memo_string(Ticket, action, filename, ref spec, keyval,
                    null, null, null, null, null, null, null, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return string.Empty;
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }

            return spec;
        }
        #endregion

        #region GetData
        public string GetData(string BsnTable, string BsnParams, Type BsnType, int MaxRecs = 0)
        {
            string bsnresult = string.Empty;

            try
            {                
                string bsnrfa = string.Empty;
                string bsnerror = string.Empty;

                int success = Web.qr_get_data(Ticket, BsnTable, App.Utils.GetFieldList(BsnType),
                    ref bsnresult, true, BsnParams, MaxRecs, ref bsnrfa, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return string.Empty;
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }

            return bsnresult;
        }
        #endregion

        #region Upload
        public bool Upload(ref byte[] bytes, string bsnparams)
        {
            var bsnerror = string.Empty;
            try
            {
                int success = Web.qr_put_file_bytes(ref ticket, ref bytes, bsnparams.Trim(), ref bsnerror);
                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Download
        public bool Download(ref byte[] data, string filename, int waitSeconds = 0)
        {
            try
            {
                int elapsedSeconds = 0;
                string bsnerror = string.Empty;                  
                int success = Web.qr_get_file_bytes(ref ticket, ref data, filename, ref bsnerror);

                while (success != 0 && waitSeconds > 0)
                {
                    Thread.Sleep(1000);
                    elapsedSeconds++;

                    success = Web.qr_get_file_bytes(ref ticket, ref data, filename, ref bsnerror);

                    if (elapsedSeconds > waitSeconds)
                    {
                        break;
                    }
                }

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Update Customer
        public string UpdateCustomer(CustomerContact contact)
        {            
            int mode = 1;       //create            

            int seqnum = 0;
            string cusno = string.Empty;
            string cusname = string.Empty;
            string email = contact.Email;
            string bsnerror = string.Empty;
            string bsnparams = string.Empty;

            if (!string.IsNullOrEmpty(contact.CustomerNumber))
            {                
                mode = 2;       //append
                cusno = contact.CustomerNumber;
            }

            if (!string.IsNullOrEmpty(contact.Sequence))
            {                
                mode = 3;       //update
                seqnum = int.Parse(contact.Sequence);
            }

            Ivctblu IVCTBLU = new Ivctblu()
            {
                Ivtu_seqnum = seqnum,
                Ivtu_title = contact.Title,
                Ivtu_firstname = contact.FirstName,
                Ivtu_surname = contact.LastName,
                Ivtu_address = contact.Address1,
                Ivtu_address2 = contact.Address2,
                Ivtu_suburb = contact.Suburb,
                Ivtu_state = contact.State,
                Ivtu_zip = contact.PostCode,
                Ivtu_phhome = contact.HomePhone,
                Ivtu_phwork = contact.WorkPhone,
                Ivtu_mobile = contact.MobilePhone,
                Ivtu_fax = contact.FaxNumber
            };

            var cuscd = App.Settings.Comp(CompanySettingsType.CustomerCode).ToString();
            if (!string.IsNullOrEmpty(cuscd))
            {
                bsnparams += "CUSCD=" + cuscd + ",";
            }

            int success = Web.qr_ar_customer(ref ticket, mode, ref cusno, ref cusname, ref IVCTBLU, ref email, ref bsnerror, ref bsnparams);
            
            if (success != 0)
            {
                App.Utils.SetError(bsnerror);                
            }
            else
            {
                App.VM.AR.CustomerContacts.Clear();
                App.VM.AR.Customers.Clear();
                App.Utils.SetSuccess("Saved:" + cusno);
            }
            return cusno;
        }
        #endregion

        #region Delete Contact
        public void DeleteContact(CustomerContact contact)
        {
            try
            {
                int mode = 4;   //Delete
                var cusno = contact.CustomerNumber;
                var cusname = string.Empty;
                var email = string.Empty;
                var bsnerror = string.Empty;

                var ivctblu = new Ivctblu()
                {
                    Ivtu_seqnum = contact.Sequence.ToInt()
                };
                
                int success = Web.qr_ar_contact(ref ticket, mode, ref cusno, ref cusname, ref ivctblu, ref email, ref bsnerror);
                
                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region Update Contract
        public string UpdateContract(Contract contract)
        {
            try
            {
                var key = contract.ContractNumber;
                var CONHDRA = new Conhdra();
                var bsnerror = string.Empty;
                int success = -1;

                int mode = 1;   //create
                if (string.IsNullOrEmpty(key))
                {
                    CONHDRA.Co_psolcode = App.Settings.Comp(CompanySettingsType.DefaultPersonalSolicitor).ToString();
                    CONHDRA.Co_cmpnysol = App.Settings.Comp(CompanySettingsType.DefaultCompanySolicitor).ToString();
                    CONHDRA.Co_lendcode = App.Settings.Comp(CompanySettingsType.DefaultLendingAuthority).ToString();
                }
                else
                {
                    mode = 4;   //find
                    success = Web.qr_get_contract(ref ticket, mode, key, ref CONHDRA, ref bsnerror);
                    if (success != 0)
                    {
                        App.Utils.SetError(bsnerror);
                        return string.Empty;
                    }
                    mode = 2;   //update
                }

                var addr1 = contract.Address;
                var addr2 = string.Empty;
                if (addr1.Length > 30)
                {
                    addr1 = addr1.Substring(0, 30);
                    addr2 = contract.Address.Substring(30);
                }

                if (!string.IsNullOrEmpty(key))
                {
                    CONHDRA.Co_contract = int.Parse(key);
                }

                if (string.IsNullOrEmpty(contract.Status))
                {
                    var sts = App.Settings.Comp(CompanySettingsType.ActiveContractStatusList).ToStrArray();
                    var contractStatus = Helpers.GetArray(sts, 0);
                    sts = App.Settings.Comp(CompanySettingsType.ActiveLeadStatusList).ToStrArray();
                    var leadStatus = Helpers.GetArray(sts, 0);
                    contract.Status = contract.IsLead ? leadStatus : contractStatus;
                }

                CONHDRA.Co_cusnum = contract.CustomerNumber;
                CONHDRA.Co_jobnum = contract.JobNumber;
                CONHDRA.Co_lotno = contract.LotNumber;
                CONHDRA.Co_streetno = contract.StreetNumber;
                CONHDRA.Co_addr1 = addr1;
                CONHDRA.Co_addr2 = addr2;
                CONHDRA.Co_city = contract.City;
                CONHDRA.Co_state = contract.State;
                CONHDRA.Co_zip = contract.PostCode;
                CONHDRA.Co_status = contract.Status;
                CONHDRA.Co_saletype = contract.SaleType;
                CONHDRA.Co_salescons = contract.SalesConsultant;
                CONHDRA.Co_csr = contract.SalesConsultant;
                CONHDRA.Co_opcentre = contract.OpCentre;
                CONHDRA.Co_district = contract.District;
                CONHDRA.Co_council = contract.Council;
                CONHDRA.Co_saleprom = contract.SalesPromotion;
                CONHDRA.Co_salecent = contract.SalesCentre;
                CONHDRA.Co_house = contract.House;
                CONHDRA.Co_elevation = contract.Facade;
                CONHDRA.Co_comm1 = contract.Comment1;
                CONHDRA.Co_comm2 = contract.Comment2;
                CONHDRA.Co_comm3 = contract.Comment3;
                CONHDRA.Co_subjsale = contract.SubjectSale;
                CONHDRA.Co_saveplan = contract.SavingsPlan;
                CONHDRA.Co_salegroup = contract.SaleGroup;
                CONHDRA.Co_dp = contract.DpNumber;
                CONHDRA.Co_volume = contract.Volume;
                CONHDRA.Co_folio = contract.Folio;

                string printer_templ = string.Empty;
                string pdf_file = string.Empty;
                string event_list = string.Empty;

                success = Web.qr_co_contract(ref ticket, mode, ref key, ref CONHDRA,
                    ref bsnerror, printer_templ, ref pdf_file, event_list);
                
                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                }
                else
                {
                    App.Utils.SetSuccess();
                }
                return key;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return string.Empty;
        }
        #endregion
        
        #region Contract Document
        public bool ContractDocument(Document document, ref string bsnparams)
        {
            var bsnerror = string.Empty;
            var crefor = Initials;       //TODO: Allow override in UI at some point
            var creby = Initials;

            try
            {
                var sequence = document.Sequence.ToInt();
                var contractNumber = document.Contract.ContractNumber.ToInt();
                int success = Web.qr_co_doc_reg(Ticket, contractNumber, ref sequence, document.Type, document.Reference,
                    document.Description, document.Filename, document.Notes, crefor, creby, ref bsnerror, ref bsnparams);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Job Document
        public bool JobDocument(Document document, ref string bsnparams)
        {
            var bsnerror = string.Empty;
            var crefor = Initials;       //TODO: Allow override in UI at some point
            var creby = Initials;

            try
            {                
                var section = Helpers.GetArray(App.Settings.Comp(CompanySettingsType.SalesEstimateSections).ToStrArray(), 0);
                var mode = "1";     //Add
                if (!string.IsNullOrEmpty(document.Sequence))
                {
                    mode = "2";     //Update
                }

                var sequence = document.Sequence;
                var jobNumber = document.Contract.JobNumber;
                int success = Web.qr_jc_doc_reg(Ticket, mode, jobNumber, section, document.XRefKey, ref sequence, 
                    document.Type, document.Reference,document.Description, document.Filename, document.Notes, 
                    crefor, creby, ref bsnerror, ref bsnparams);
                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Update Heading
        public bool UpdateHeading(int mode, Heading heading, string option)
        {
            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;
                var defaultParagraph = false;
                var defEmpty = App.Settings.Comp(CompanySettingsType.DefaultEmptyParagraph).ToString();
                if (heading.Type == EstimateTypes.EmptySales)
                {
                    bsnparams = "CATALOG=EMPTY,";
                    defaultParagraph = !string.IsNullOrEmpty(defEmpty);
                }

                var job = heading.Contract.JobNumber;
                var section = heading.Section;

                int success = Web.qr_jc_slsest_heading(Ticket, mode, job, section,
                    heading.Code, heading.Description, option, ref bsnerror, ref bsnparams);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }

                var contract = heading.Contract;

                //TODO: consider the below once enhanced for estimating
                //var heading = contract.Headings.Where(p => p.Code.Equals(estimateHeading)).FirstOrDefault();
                //if (heading != null)
                //{
                //    contract.Headings.Remove(heading);
                //}                

                contract.RecalcHeadings(string.Empty);

                if (!defaultParagraph)
                {
                    return true;
                }

                var defpara = defEmpty.Split(':');
                var code = Helpers.GetArray(defpara, 0);
                var description = Helpers.GetArray(defpara, 1);
                
                var paragraph = new Paragraph(heading) {
                    Code = code,
                    Description = description,
                    IsNew = true
                };
                
                return UpdateParagraph(mode, paragraph);
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region CopyParagraph
        public bool CopyParagraph(Paragraph Source, Paragraph Destination)
        {
            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;
               
                int success = Web.qr_jc_slsest_copy_job_paragraph(Ticket,
                    Source.Heading.Contract.JobNumber, Source.Heading.Section, Source.Heading.Code, Source.Code,
                    Destination.Heading.Contract.JobNumber, Destination.Heading.Section,Destination.Heading.Code, Destination.Code,
                    1,"C", ref bsnerror, ref bsnparams);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Update Paragraph
        public bool UpdateParagraph(int mode, Paragraph paragraph)
        {
            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;

                //TODO: enhance this logic, may need prompt
                if (App.Settings.Comp(CompanySettingsType.EstimateSubHeadings).ToBool())
                {
                    bsnparams += "FMTOPT=H,";
                }

                var job = paragraph.Heading.Contract.JobNumber;
                var section = paragraph.Heading.Section;
                var heading = paragraph.Heading.Code;

                int success = Web.qr_jc_slsest_paragraph(Ticket, mode, job, section, heading, 
                    paragraph.Code, paragraph.Description, ref bsnerror, ref bsnparams);                

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Update Margin
        public void UpdateMargin(string JobNumber, string section, string heading, string paragraph, string code, decimal percent, bool updateAll)
        {
            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;

                var marginpct = string.Empty;
                if (percent > 0)
                {
                    marginpct = percent.ToString("F2");
                }

                int success = Web.qr_jc_slsest_update_margin(Ticket, JobNumber, section, heading,
                    paragraph, code, marginpct, (updateAll) ? "N" : "Y", ref bsnerror, ref bsnparams);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror, true);
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region Copy Heading
        public bool CopyHeading(string JobNumber, string section, Heading source, Heading destination)
        {
            try
            {
                var bsnerror = string.Empty;
                int updateMargin = 1;

                int success = Web.qr_jc_slsest_copy_job_heading(Ticket, JobNumber, section, source.Code,
                    JobNumber, section, destination.Code, updateMargin, ref bsnerror);

                if (success == 0)
                {
                    success = Web.qr_jc_slsest_updheading(Ticket, 2, JobNumber, section, destination.Code, 
                        "Copy of " + source.Description, string.Empty, ref bsnerror);
                }

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Recalc Heading
        public bool RecalcHeading(Heading estimate)
        {
            try
            {
                var bsnerror = string.Empty;
                var bsnstatus = string.Empty;
                var bsnparams = string.Empty;

                int success = Web.qr_jc_slsest_integ_check(ref ticket, estimate.Key, 
                    ref bsnparams, ref bsnstatus, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Set Effective
        public bool SetEffective(string JobNumber)
        {
            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;

                var effective = App.Settings.Comp(CompanySettingsType.EffectiveDate).ToString();
                int success = Web.qr_jc_slsest_updeffective(Ticket, 2, JobNumber, effective, ref bsnerror, ref bsnparams);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Template Copy
        public bool TemplateCopy(Contract contract, string estimateHeading, Template template, string Extras)
        {
            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;

                var section = Helpers.GetArray(App.Settings.Comp(CompanySettingsType.SalesEstimateSections).ToStrArray(), 0);
                if (string.IsNullOrEmpty(section))
                {
                    return false;
                }

                int success = Web.qr_jc_slsest_copy_tmpl_job(ref ticket, template.Code.Item1, template.Heading.Item1.Substring(6, 4), 
                    contract.JobNumber, section, estimateHeading, ref bsnerror, Extras, bsnparams);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Estimate Detail
        public bool EstimateDetail(ref string key, ref int mode, EstimateDetail detail, string amount, string gstInc, string costAmt, bool dataOnly = false)
        {
            try
            {

                var type = "J"; //J = Job, T = Template
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;

                var strValue = App.Settings.Comp(CompanySettingsType.PricingArea).ToString();
                if (!string.IsNullOrEmpty(strValue))
                {
                    bsnparams += "AREA=" + strValue + ",";
                }

                strValue = App.Settings.Comp(CompanySettingsType.EffectiveDate).ToString();
                if (!string.IsNullOrEmpty(strValue))
                {
                    bsnparams += "EFFECTIVE=" + strValue + ",";
                }

                var intValue = App.Settings.Comp(CompanySettingsType.EstimateDetailIncr).ToInt();
                if (intValue != 10)
                {
                    bsnparams += "INCR=" + intValue.ToString() + ",";
                }

                if (!string.IsNullOrEmpty(detail.NewSequence))
                {
                    bsnparams += "SEQ=" + detail.NewSequence + ",";
                }

                if (!string.IsNullOrEmpty(detail.UnitOfMeasure))
                {
                    bsnparams += "UOM=" + detail.UnitOfMeasure + ",";
                }

                var boolValue = App.Settings.Comp(CompanySettingsType.ItemPriceLock).ToBool();
                if (boolValue)
                {
                    bsnparams += "LCKPRC=Y,";
                }

                if (!string.IsNullOrEmpty(gstInc))
                {
                    bsnparams += "GSTINC=" + gstInc + ",";
                }

                if (!string.IsNullOrEmpty(costAmt))
                {
                    bsnparams += "COSTAMT=" + costAmt + ",";
                }

                if (detail.MarginPct != detail.OriginalMargin)
                {
                    bsnparams += "MGNPCT=" + detail.MarginPct.ToString("F2") + ",";
                }
                else
                {
                    if (!string.IsNullOrEmpty(detail.MarginCode))
                    {
                        bsnparams += "MGNCD=" + detail.MarginCode + ",";
                    }
                }

                if (dataOnly)
                {
                    bsnparams += "GETDATA=Y,";
                }

                var contract = detail.Paragraph.Heading.Contract;
                var job = contract.JobNumber;
                var section = detail.Paragraph.Heading.Section;
                var heading = detail.Paragraph.Heading.Code;

                int success = Web.qr_jc_slsest_details(Ticket, ref key, type, job, section, heading, detail.Paragraph.Code, 
                    detail.Item, detail.Description, (detail.Included) ? "Y" : "N", detail.Quantity.ToString("F3"), detail.PriceDisplay, 
                    ref mode, ref bsnerror, amount, detail.Notes, ref bsnparams);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }

                if (dataOnly)
                {
                    //App.Utils.SetSuccess(bsnparams);
                    var request = JsonConvert.SerializeObject(new DtoRequest()
                    {
                        Command = ApiKey.ParseBuffer.ToString(),
                        Keys = new List<KeyInfo>()
                        {
                            new KeyInfo("jcspec_c")
                        },
                        Params = new Dictionary<string, object>()
                        {
                            { "jcspec_c", new List<string> { bsnparams } }
                        }
                    });
                    var response = string.Empty;
                    //TODO: grab request string and run in bc restart subr
                    if (WebData(ref request, ref response))
                    {
                        var data = (V.Models)JsonConvert.DeserializeObject(response, typeof(V.Models));
                        if (data.List.Count > 0)
                        {
                            var obj = data.List[0].ToString();
                            var jcspec_c = (M.EstimateDetailBase)JsonConvert.DeserializeObject(obj, typeof(M.EstimateDetailBase));
                            detail.Item = jcspec_c.ItemNumber.Trim();
                            detail.Description = jcspec_c.Description.Trim();
                            detail.MarginCode = jcspec_c.MarginCode.Trim();
                            detail.PriceDisplay = jcspec_c.PriceDisplay.Trim();
                            detail.UnitOfMeasure = jcspec_c.UnitOfMeasure.Trim();
                            detail.SetOriginalCost(jcspec_c.UnitCost.Trim().ToDec());
                            detail.SetOriginalMargin(jcspec_c.MarginPct.Trim().ToDec());
                            detail.UnitAmountGst = jcspec_c.UnitAmountGst.Trim().ToDec();
                            detail.SetOriginalAmountEx(jcspec_c.UnitAmountEx.Trim().ToDec());
                            detail.SetOriginalAmountInc(detail.UnitAmountGst + jcspec_c.UnitAmountInc.Trim().ToDec());
                            detail.IncludeGST = (jcspec_c.IncludeGST.Trim() == "Y");                            
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion
               
        #region EstimateDetailGetNextSortOrder
        public Int64 EstimateDetailGetNextSortOrder(List<EstimateDetail> detailLines)
        {
            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;
                Int64 sortOrder = 0;

                if (detailLines.Count == 0)
                {
                    App.Utils.SetError("No Records have been selected.");
                    return -1;
                }

                var contract = detailLines[0].Paragraph.Heading.Contract;
                var job = contract.JobNumber;
                var section = detailLines[0].Paragraph.Heading.Section;
                var heading = detailLines[0].Paragraph.Heading.Code;
                var paragraph = detailLines[0].Paragraph.Code;
                var keys = new ArrayList();
                foreach (var item in detailLines)
                {
                    keys.Add(item.Key);
                }

                var success = Web.qr_jc_slsest_sortorder(Ticket, job, section, heading, paragraph, "G","", keys, ref sortOrder, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return -1;
                }
                return sortOrder;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return -1;
        }
        #endregion

        #region InsertEstimateDetail
        public bool InsertEstimateDetail(List<EstimateDetail> Source, EstimateDetail detail)
        {
            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;
                ArrayList list = new ArrayList();
                foreach (var item in Source)
                {
                    list.Add(item.Key);
                }
                bsnparams = "KEY," + detail.Key;
                int success = Web.qr_jc_slsest_copy_lines(Ticket, "C",
                    detail.Paragraph.Heading.Contract.JobNumber, detail.Paragraph.Heading.Section, detail.Paragraph.Heading.Code, detail.Paragraph.Code, "D",detail.Key,
                    list, false, bsnparams, ref bsnerror);


                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region CopyEstimateDetail
        public bool CopyEstimateDetail(List<EstimateDetail> Source, Paragraph Destination)
        {
            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;
                ArrayList list = new ArrayList();
                foreach (var item in Source)
                {
                    list.Add(item.Key);
                }
                int success = Web.qr_jc_slsest_copy_lines(Ticket, "C",
                    Destination.Heading.Contract.JobNumber, Destination.Heading.Section, Destination.Heading.Code, Destination.Code, "D","",
                    list, false, bsnparams, ref bsnerror);
                    

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Estimate Detail Move
        public bool EstimateDetailMove(List<EstimateDetail> detailLines, string mode, out Boolean ResequencePerformed)
        {
            ResequencePerformed = false;

            try
            {
                var bsnerror = string.Empty;
                var bsnparams = string.Empty;
                long sortOrder = 0;

                if (detailLines.Count == 0)
                {
                    App.Utils.SetError("No Records have been selected.");
                    return false;
                }

                var contract = detailLines[0].Paragraph.Heading.Contract;
                var job = contract.JobNumber;
                var section = detailLines[0].Paragraph.Heading.Section;
                var heading = detailLines[0].Paragraph.Heading.Code;
                var paragraph = detailLines[0].Paragraph.Code;
                var keys = new ArrayList();
                foreach (var item in detailLines)
                {
                    keys.Add(item.Key);
                }
                
                int success = Web.qr_jc_slsest_sortorder(Ticket, job, section, heading, paragraph, mode,"", keys, ref sortOrder, ref bsnerror);

                switch (success)
                {  
                    case 0:
                         return true;
                    case 1:
                        ResequencePerformed = true;
                        return true;
                    default:
                        App.Utils.SetError(bsnerror);
                        return false;
                }
               
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion
        
        #region JobInfo Copy
        public bool JobInfoCopy(string JobNumber, string template)
        {
            try
            {
                var status = string.Empty;
                var bsnparams = string.Empty;
                var bsnerror = string.Empty;
                
                int success = Web.qr_jc_jobinfo_copy_selection(ref ticket, JobNumber, template, 
                    ref status, ref bsnparams, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region JobInfo Update
        public void JobInfoUpdate(string key, string answer)
        {
            try
            {
                var bsnerror = string.Empty;

                int success = Web.qr_jc_upd_jobinfo(Ticket, key, answer, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror.Trim(), true);
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region JobInfo Add
        public void JobInfoAdd(string JobNumber, string Group, string Category, string Code, string Answer)
        {
            try
            {
                var bsnerror = string.Empty;

                int success = Web.qr_jc_add_jobinfo(Ticket, JobNumber,
                    Group, Category, Code, Answer, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror.Trim(), true);
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region ExtraInfo Update
        public void ExtraInfoUpdate(string UpdateKey, string AnsCode, string Answer)
        {
            try
            {
                var bsnerror = string.Empty;

                int success = Web.qr_xi_upd_extrainfo(Ticket, UpdateKey, AnsCode, Answer, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError("Error updating: " + bsnerror.Trim());
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region ExtraInfo Add
        public void ExtraInfoAdd(string Reference, string Code, string AnsCode, string Answer)
        {
            try
            {
                var bsnerror = string.Empty;

                int success = Web.qr_xi_add_extrainfo(Ticket, "CSC", Reference, Code, AnsCode, Answer, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError("Error updating: " + bsnerror.Trim());
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region Event Update
        public void EventUpdate(Event contractEvent)
        {
            try
            {
                var bsnerror = string.Empty;
                string today = DateTime.Now.ToString("dd/MM/yyyy");
                string bsnparams = string.Empty;

                var contractNumber = contractEvent.Contract.ContractNumber.ToInt();
                var eventNumber = contractEvent.Code.ToInt();
                var actual = (contractEvent.IsRegistered) ? string.Empty : contractEvent.Actual.ToAus();
                
                int success = Web.qr_co_event_update(
                    ref ticket,
                    contractNumber,
                    eventNumber,
                    actual,
                    contractEvent.Forecast.ToAus(),
                    contractEvent.Registered.ToAus(),
                    contractEvent.Amount.ToInt(),
                    contractEvent.PctComplete.ToInt(), 
                    contractEvent.Reference, 
                    contractEvent.EmpCode, 
                    contractEvent.GstCode, 
                    ref bsnerror, 
                    "Y");

                if (success == 0)
                {
                    success = Web.qr_co_update_event_notes(ref ticket, contractNumber, 
                        eventNumber, contractEvent.Notes, ref bsnerror);
                }

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror, true);
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region Event Register
        public void EventRegister(Event contractEvent)
        {
            try
            {
                var bsnerror = string.Empty;
                var today = DateTime.Now.ToAus();
                var bsnparams = string.Empty;
                int clearall = 0;
                int success = -1;
                var contractNumber = contractEvent.Contract.ContractNumber.ToInt();
                var eventNumber = contractEvent.Code.ToInt();
                if (contractEvent.IsRegistered)
                {
                    success = Web.qr_co_event_unregister(ref ticket, contractNumber, eventNumber, clearall, ref bsnerror);
                }
                else
                {
                    success = Web.qr_co_event_reg(ref ticket, contractNumber, eventNumber, today, bsnparams, ref bsnerror);
                }
                if (success != 0)
                {
                    App.Utils.SetError(bsnerror, true);
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion
        
        #region Update Variation
        public int UpdateVariation(Variation variation, ref string bsnparams)
        {
            int seq = 0;
            try
            {
                if (!variation.IsNew)
                {
                    seq = int.Parse(variation.Sequence);
                }
                var bsnerror = string.Empty;
                //var bsnparams = string.Empty;
                var status = string.Empty;
                var regEvents = "N";
                
                int success = Web.qr_co_add_var(ref ticket, int.Parse(variation.ContractNumber), ref seq, 
                    variation.Description, variation.Reference, variation.IncAmount.ToString("F2"), (variation.IncludeGST) ? "Y" : "N", variation.CostAmount.ToString("F2"), 
                    variation.Type, Username, regEvents, variation.Notes, ref bsnparams, ref status, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                    seq = 0;
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
                seq = 0;
            }
            return seq;
        }
        #endregion

        #region Update ItemSelection
        public void UpdateItemSelection(string Code, string Query, ref string Extras)
        {
            try
            {
                string bsnerror = string.Empty;
                string status = string.Empty;
                
                int success = Web.qr_jc_slsest_item_selection(ref ticket, Code, Query, ref Extras, ref status, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                }                
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region Variation Event Register
        public void VariationEventRegister(string contractNumber, string variationSeq, string eventNumber,
            decimal sellAmt, decimal costAmt, decimal gstAmt)
        {
            string today = DateTime.Now.ToString("dd/MM/yyyy");
            VariationEventRegister(contractNumber, variationSeq, eventNumber, today, 
                string.Empty, string.Empty, sellAmt, costAmt, gstAmt);           
        }
        #endregion

        #region Variation Event Register
        public void VariationEventRegister(string contractNumber, string variationSeq, string eventNumber, string actualDate, 
            string employee, string reference, decimal sellAmt, decimal costAmt, decimal gstAmt)
        {
            try
            {
                var bsnerror = string.Empty;
                
                long sellAmtL = Convert.ToInt64((sellAmt*100));
                long costAmtL = Convert.ToInt64((costAmt*100));
                long gstAmtL = Convert.ToInt64((gstAmt*100));

                int success = Web.qr_co_var_event_register(ref ticket, int.Parse(contractNumber), int.Parse(variationSeq), int.Parse(eventNumber), 
                    actualDate, sellAmtL, costAmtL, gstAmtL, reference, employee, null, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region Variation Event UnRegister
        public void VariationEventUnregister(string contractNumber, string variationSeq, string eventNumber)
        {
            try
            {
                var bsnerror = string.Empty;         

                int success = Web.qr_co_var_event_unregister(ref ticket, int.Parse(contractNumber), 
                    int.Parse(variationSeq), int.Parse(eventNumber), 1, ref bsnerror);

                if (success != 0)
                {
                    App.Utils.SetError(bsnerror);
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion
              
    }
}
