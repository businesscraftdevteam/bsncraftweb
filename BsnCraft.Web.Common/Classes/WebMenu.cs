﻿using System.Collections.Generic;
using System.Web.UI.HtmlControls;

public enum Menus
{
    Home,
    Customers,
    Customers_Details,
    Activities,
    Documents,
    Finance,
    Leads,
    Leads_Details,
    Leads_Estimate,    
    Leads_Template,
    Leads_Checklist,
    Leads_Variation,
    Contracts,
    Contracts_Details,
    Contracts_Estimate,
    Contracts_Template,
    Contracts_Checklist,
    Contracts_Variation,
    Jobs,
    Jobs_Details,
    DisplayCentres,
    DisplayCentres_Details,
    Charts,
    Charts_GetData,
    Reports,
    Dashboards,
    Admin,
    Shared_Thumbnail,
    Shared_Search
}

namespace BsnCraft.Web.Common.Classes
{
    public class WebMenu
    {
        public WebMenu Parent { get; set; }

        public WebMenu(Menus menu, string caption, string url, string icon)
        {
            this.menu = menu;
            this.caption = caption;
            this.url = url;
            this.icon = icon;
        }

        #region Public Properties
        private Menus menu;
        public Menus Menu
        {
            get
            {
                return menu;
            }
        }

        private string caption;
        public string Caption
        {
            get
            {
                return caption;
            }
        }

        private string url;
        public string URL
        {
            get
            {
                return url;
            }
        }

        public string ResolveUrl
        {
            get
            {
                return Utils.ResolveUrl(url);
            }
        }

        private string icon;
        public string Icon
        {
            get
            {
                return icon + " fa-fw";
            }
        }    
        
        public bool IsActive { get; set; }
        #endregion
        
        #region Visible
        private bool visible = true;
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }
        #endregion

        #region Button
        private BsnButton button;
        public BsnButton Button
        {
            get
            {
                if (button == null)
                {
                    var onclick = Utils.WindowLocation(Menu);
                    button = new BsnButton(onclick, Icon, false);
                    button.AddText(Caption, "text");
                }
                return button;
            }
        }
        #endregion

        #region Set Count
        HtmlGenericControl countSpan;
        public void SetCount(int count)
        {
            if (count == 0)
                return;

            if (countSpan == null)
            {
                countSpan = new HtmlGenericControl("span");
                countSpan.Attributes["class"] = "badge badge-pill";
                Button.Controls.Add(countSpan);
            }
            countSpan.InnerHtml = count.ToString();
        }
        #endregion
    }
}