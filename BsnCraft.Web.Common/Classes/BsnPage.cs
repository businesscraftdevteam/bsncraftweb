﻿using BsnCraft.Web.Common.Models;
using DevExpress.Export;
using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using DevExpress.XtraPrinting;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.UI;

namespace BsnCraft.Web.Common
{
    public abstract class BsnPage : System.Web.UI.Page
    {
        public PageModes PageMode { get; set; }
        public bool InCallback { get; set; }

        #region Protected Properties        
        protected Logger Log { get; private set; }
        protected Dictionary<string, string> Settings { get; private set; }        
        protected bool IsDevExtreme = false;
        protected string SettingsKey = "Default";        
        #endregion

        protected BsnPage()
        {            
            Log = LogManager.GetLogger(GetType().FullName);
            //Log.Debug("BsnPage| constructor");
            Settings = new Dictionary<string, string>();
            PageMode = PageModes.Default;
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
        
            //Testing to stop dbs.exe always open
            //If too slow, remove for now
            BsnUtils.BsnDisconnect();
        }

        #region BsnInit
        //ScriptReferenceCollection scripts
        public void BsnInit(BootstrapCallbackPanel panel)
        {
            Utils.SettingsButton.Clear();

            if (!BsnUtils.Connected)
                return;

            if (!panel.IsCallback)
            {
                InCallback = false;                
                InitPage(string.Empty);
            }
            else
            {
                InCallback = true;
            }

            if (IsDevExtreme)
            {
                //Still a few issues with reports/dashboards - Top buttons + Field List scrolling

                //scripts.Add(new ScriptReference() { Name = "Ace" });
                //scripts.Add(new ScriptReference() { Name = "DevExtreme" });
            }
        }
        #endregion

        #region Init Page
        protected virtual void InitPage(string parameter)
        {
            //Log.Debug("InitPage| parameter=" + parameter);
            LoadPageSettings();
            if (!InCallback)
            {
                Utils.ActiveFilters.Clear();
            }
        }
        #endregion
        
        #region PerformCallback
        public virtual void PerformCallback(object sender, CallbackEventArgsBase e)
        {
            if (!BsnUtils.Connected)
                return;
            
            var callback = new BsnCallback(e.Parameter);
            if (callback.Id == BsnControls.None)
            {
                switch (callback.Mode)
                {
                    case CallbackMode.Reset:
                        BsnUtils.SaveSettings(SettingsLevel.User, SettingsKey, "{}");
                        break;
                    case CallbackMode.PageSetting:
                        SavePageSettings(callback.Key, callback.Value);
                        break;
                }
            }

            InitPage(e.Parameter);
        }
        #endregion
        
        #region GetPageSettings
        protected string GetPageSettings()
        {
            string settings = VarUtils.Str(WebVars.PageSettings);
            if (!string.IsNullOrEmpty(settings))
            {
                return settings;
            }
            settings = BsnUtils.GetSettings(SettingsLevel.User, SettingsKey);
            if (!string.IsNullOrEmpty(settings))
            {
                VarUtils.Set(WebVars.PageSettings, settings);
            }
            return settings;
        }
        #endregion

        #region LoadPageSettings        
        public virtual void LoadPageSettings()
        {
            string settings = GetPageSettings();
            if (string.IsNullOrEmpty(settings))
                return;

            try
            {
                Settings = (Dictionary<string, string>)JsonConvert.DeserializeObject(settings, typeof(Dictionary<string, string>));
            }
            catch (Exception e)
            {
                Utils.LogExc(e);
            }

            if (Settings.ContainsKey(Utils.PageModeKey))
            {
                PageMode = Utils.PageMode(Settings[Utils.PageModeKey]);
            }
        }
        #endregion

        #region BoolSetting
        public virtual bool BoolSetting(PageSettings setting)
        {
            return BoolSetting(setting.ToString());
        }

        public virtual bool BoolSetting(string setting)
        {
            bool result = false;
            if (!Settings.ContainsKey(setting))
                return result;

            bool.TryParse(Settings[setting], out result);
            return result;
        }
        #endregion

        #region StringSetting
        public virtual string StringSetting(PageSettings setting)
        {
            if (!Settings.ContainsKey(setting.ToString()))
                return string.Empty;
            
            return Settings[setting.ToString()];
        }
        #endregion

        #region IntSetting
        public virtual int IntSetting(PageSettings setting)
        {
            int result = 0;
            if (!Settings.ContainsKey(setting.ToString()))
                return result;

            int.TryParse(Settings[setting.ToString()], out result);
            return result;
        }
        #endregion

        #region SavePageSettings
        public virtual void SavePageSettings(string setting, string value)
        {
            LoadPageSettings();
            Settings[setting] = value;
            if (setting == Utils.PageModeKey)
            {
                PageMode = Utils.PageMode(value);
            }
            var settings = JsonConvert.SerializeObject(Settings);
            VarUtils.Set(WebVars.PageSettings, settings);
            BsnUtils.SaveSettings(SettingsLevel.User, SettingsKey, settings);
        }
        #endregion

        #region Export Grid
        public void ExportGrid(ASPxGridViewExporter exporter)
        {
            string export = Request.QueryString["export"];
            if (string.IsNullOrEmpty(export))
            {
                return;
            }
            switch (export.ToUpper())
            {
                case "PDF":
                    exporter.WritePdfToResponse();
                    break;
                case "XLS":
                    exporter.WriteXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });
                    break;
                case "XLSX":
                    exporter.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
                    break;
                case "CSV":
                    exporter.WriteCsvToResponse(new CsvExportOptionsEx() { ExportType = ExportType.WYSIWYG });
                    break;
                case "RTF":
                    exporter.WriteRtfToResponse();
                    break;
            }
        }
        #endregion
        
        #region CustomJSProperties
        protected virtual void CustomJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            e.Properties["cpAlerts"] = Utils.GetAlerts;
            e.Properties["cpSearchUrl"] = ResolveUrl("~/Shared/Search.aspx");
        }
        #endregion

        [WebMethod]
        public static void Disconnect()
        {
            BsnUtils.BsnDisconnect();
        }

        [WebMethod]
        public static void ClearAlerts()
        {
            Utils.BsnAlerts.Clear();
        }

        [WebMethod]
        public static void CloseModal()
        {
            VarUtils.Set(WebVars.ModalCallback, null);
        }        
    }
}