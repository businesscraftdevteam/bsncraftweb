﻿using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes
{
    public static class BsnExtensions
    {
        #region ValueTupleConverter
        internal class ValueTupleConverter<T1, T2> : TypeConverter where T1 : struct where T2 : struct
        {
            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
            }
            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                try
                {
                    var elements = ((string)value).Split(new[] { '(', ',', ')' }, StringSplitOptions.RemoveEmptyEntries);
                    T1 obj1 = (typeof(T1).IsEnum) ? (T1)Enum.Parse(typeof(T1), elements.First()) : JsonConvert.DeserializeObject<T1>(elements.First());
                    T2 obj2 = (typeof(T2).IsEnum) ? (T2)Enum.Parse(typeof(T2), elements.Last()) : JsonConvert.DeserializeObject<T2>(elements.Last());
                    return new ValueTuple<T1, T2>(obj1, obj2);
                }
                catch { }
                return new ValueTuple<T1, T2>(new T1(), new T2());
            }
        }
        #endregion

        #region Bundle ForceOrdered
        internal class AsIsBundleOrderer : IBundleOrderer
        {
            public virtual IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
            {
                return files;
            }
        }

        public static Bundle ForceOrdered(this Bundle sb)
        {
            sb.Orderer = new AsIsBundleOrderer();
            return sb;
        }
        #endregion

        #region Increment
        public static void Increment<T>(this Dictionary<T, int> dictionary, T key)
        {
            dictionary.TryGetValue(key, out int count);
            dictionary[key] = count + 1;
        }

        public static void Increment<T>(this List<ChartData> list, T key)
        {
            ChartData data = list.Where(p => p.Argument == key.ToString()).FirstOrDefault();
            if (data == null)
            {
                list.Add(new ChartData() { Argument = key.ToString(), Value = 1 });
            }
            else
            {
                data.Value++;
            }
            //dictionary.TryGetValue(key, out count);
            //dictionary[key] = count + 1;
        }
        #endregion
        
        #region AppLoad
        public static void AppLoad(this object source, Page page)
        {
            if (source == null)
            {
                source = new BsnApplication()
                {
                    Version = typeof(BsnApplication).Assembly.GetName().Version
                };
                page.Session["app"] = source;
            }
            ((BsnApplication)source).Load(page);
        }
        #endregion

        #region ToCallbackType
        public static CallbackType ToCallbackType(this string source)
        {
            var type = CallbackType.Normal;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    type = (CallbackType)Enum.Parse(typeof(CallbackType), source, true);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return type;
        }
        #endregion

        #region ToMenu
        public static BsnMenus ToMenu(this string source)
        {
            var menu = BsnMenus.Home;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    menu = (BsnMenus)Enum.Parse(typeof(BsnMenus), source, true);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return menu;
        }
        #endregion

        #region ToAction
        public static BsnActions ToAction(this string source)
        {
            var action = BsnActions.Default;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    action = (BsnActions)Enum.Parse(typeof(BsnActions), source, true);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return action;
        }
        #endregion

        #region ToMode
        public static BsnModes ToMode(this string source)
        {
            var mode = BsnModes.None;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    mode = (BsnModes)Enum.Parse(typeof(BsnModes), source, true);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return mode;
        }
        #endregion

        #region ToDataMode
        public static BsnDataModes ToDataMode(this string source)
        {
            var mode = BsnDataModes.None;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    mode = (BsnDataModes)Enum.Parse(typeof(BsnDataModes), source, true);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return mode;
        }
        #endregion

        #region ToGrid
        public static BsnGrids ToGrid(this string source)
        {
            var grid = BsnGrids.Customers;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    grid = (BsnGrids)Enum.Parse(typeof(BsnGrids), source, true);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return grid;
        }
        #endregion

        #region ToForm
        public static BsnForms ToForm(this string source)
        {
            var form = BsnForms.None;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    form = (BsnForms)Enum.Parse(typeof(BsnForms), source, true);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return form;
        }
        #endregion

        #region ToPanelType
        public static PanelTypes ToPanelType(this string source)
        {
            var type = PanelTypes.Grid;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    type = (PanelTypes)Enum.Parse(typeof(PanelTypes), source, true);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return type;
        }
        #endregion

        #region ToCompSetting
        public static CompanySettingsType ToCompSetting(this string source)
        {
            var setting = CompanySettingsType.None;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    setting = (CompanySettingsType)Enum.Parse(typeof(CompanySettingsType), source, true);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return setting;
        }
        #endregion

        #region ToEstimateType
        public static EstimateTypes ToEstimateType(this string source)
        {
            var type = EstimateTypes.Sales;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    type = (EstimateTypes)Enum.Parse(typeof(EstimateTypes), source, true);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return type;
        }
        #endregion

        #region ToHtml
        public static string ToHtml(this HtmlGenericControl ctl)
        {
            var generatedHtml = new StringBuilder();
            string html = string.Empty;
            using (var htmlStringWriter = new StringWriter(generatedHtml))
            {
                using (var htmlTextWriter = new HtmlTextWriter(htmlStringWriter))
                {
                    ctl.RenderControl(htmlTextWriter);
                    html = generatedHtml.ToString();
                }
            }
            return html;
        }
        #endregion

        #region ToChartType
        public static ChartDisplayTypes ToChartType(this string source)
        {
            var type = ChartDisplayTypes.Pie;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    type = (ChartDisplayTypes)Enum.Parse(typeof(ChartDisplayTypes), source, true);                    
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return type;
        }
        #endregion


        #region ToChartType
        public static DataSourceTypes ToDataSource(this string source)
        {
            var type = DataSourceTypes.ContractsByOperatingCentre;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    type = (DataSourceTypes)Enum.Parse(typeof(DataSourceTypes), source, true);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
            }
            return type;
        }
        #endregion

        #region Bool Estensions
        public static bool IfFalse(this bool source, bool def)
        {
            return (!source) ? def : source;
        }
        #endregion

        #region Decimal Extensions
        public static string ToCurrency(this decimal source)
        {
            if (source == 0)
            {
                return string.Empty;
            }
            return source.ToString("C");
        }
        #endregion

        #region String Extensions
        public static string ToHtml(this string source)
        {
            return source.Replace("\r\n", "<br />");
        }

        public static string MapPath(this string source)
        {
            return HttpContext.Current.Server.MapPath(source);
        }

        public static string ValueOrEmpty(this string source)
        {
            return (string.IsNullOrEmpty(source)) ? string.Empty : source;
        }

        public static string DefaultString(this string source, string def)
        {
            return (string.IsNullOrEmpty(source)) ? def : source;
        }

        public static bool DefaultBool(this string source, bool def)
        {
            if (string.IsNullOrEmpty(source))
            {
                return def;
            }
            bool.TryParse(source, out def);
            return def;
        }

        public static int DefaultInt(this string source, int def)
        {
            if (string.IsNullOrEmpty(source))
            {
                return def;
            }
            int.TryParse(source, out def);
            return def;
        }
        #endregion

        #region Date Extensions
        public static string ToJulian(this DateTime source)
        {
            return source.Year.ToString() + source.DayOfYear.ToString("000");
        }

        public static string ToAus(this DateTime source)
        {
            if (source == DateTime.MinValue)
            {
                return string.Empty;
            }
            return source.ToString("dd/MM/yyyy");
        }

        public static string ToShort(this DateTime source)
        {
            return source.ToString("dd/MM/yy");
        }
        #endregion

        #region Object Extensions
        public static bool ToBool(this object source)
        {
            if (source == null)
            {
                return false;
            }
            bool.TryParse(source.ToString(), out bool result);
            return result;
        }

        public static bool IsNull(this object source)
        {
            if (source == null)
            {
                return true;
            }
            return false;
        }

        public static int ToInt(this object source)
        {
            if (source == null)
            {
                return 0;
            }
            int.TryParse(source.ToString(), out int result);
            return result;
        }
        
        public static decimal ToDec(this object source)
        {
            if (source == null)
            {
                return 0;
            }
            if (source is decimal dec)
            {
                return dec;
            }
            decimal.TryParse(source.ToString(), out decimal result);
            return result;
        }

        public static string ToCurrency(this object source)
        {
            return source.ToDec().ToCurrency();
        }

        public static DateTime ToDate(this object source)
        {
            if (source == null)
            {
                return DateTime.MinValue;
            }
            if (source is DateTime date)
            {
                return date;
            }
            DateTime.TryParse(source.ToString(), out DateTime result);
            return result;
        }

        public static string[] ToStrArray(this object source)
        {
            if (source is string[] array)
            {
                return array;
            }
            return new string[] { "" };
        }

        public static List<string> ToQuotedList(this object source)
        {
            if (source is string[] array)
            {
                if (array.Length > 1 || !string.IsNullOrEmpty(Helpers.GetArray(array, 0)))
                {
                    return array.Select(x => string.Format("'{0}'", x)).ToList();
                }                
            }
            if (source is List<string> list)
            {
                if (list.Count > 0)
                {
                    return list.Select(x => string.Format("'{0}'", x)).ToList();
                }
            }
            return new List<string>();
        }

        public static string ValueOrDefault(this object source, string def, string format = "")
        {
            if (source == null)
            {
                return def;
            }
            string value = (string.IsNullOrEmpty(source.ToString())) ? def : source.ToString();
            if (string.IsNullOrEmpty(format))
            {
                return value;
            }
            if (source is decimal dec)
            {
                return dec.ToString(format);
            }
            if (source is DateTime date)
            {
                if (!string.IsNullOrEmpty(format))
                {
                    return date.ToString(format);
                }
                return date.ToAus();
            }
            return value;
        }
        #endregion

        #region Form Item
        public static IEnumerable<BsnFormItemBase> Find(this BsnFormItemBase source)
        {
            var nodes = new Stack<BsnFormItemBase>(new[] { source });
            while (nodes.Any())
            {
                BsnFormItemBase node = nodes.Pop();
                yield return node;
                foreach (var n in node.Items) nodes.Push(n);
            }
        }

        public static BsnFormItemBase Get(this IEnumerable<BsnFormItemBase> source, string key)
        {
            return source.Where(p => p.Key.Equals(key)).FirstOrDefault();            
        }

        public static string GetValue(this IEnumerable<BsnFormItemBase> source, string key)
        {
            if (source.Any(p => p.Key.Equals(key)))
            {
                return source.Where(p => p.Key.Equals(key)).FirstOrDefault().Value;
            }
            return string.Empty;
        }
        #endregion

    }
}
