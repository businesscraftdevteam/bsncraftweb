﻿using BsnCraft.Web.Common.Views.Menu.Base;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BsnCraft.Web.Common.Classes
{
    #region Callback
    public class Callback
    {
        [JsonProperty(PropertyName = "type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CallbackType Type { get; set; }

        [JsonProperty(PropertyName = "menu")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BsnMenus Menu { get; set; }

        [JsonProperty(PropertyName = "action")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BsnActions Action { get; set; }

        [JsonProperty(PropertyName = "mode")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BsnModes Mode { get; set; }
        
        [JsonProperty(PropertyName = "app")]
        public string App { get; set; } = "";

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; } = "";

        [JsonProperty(PropertyName = "key")]
        public string Key { get; set; } = "";

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; } = "";

        [JsonProperty(PropertyName = "data")]
        public CallbackData Data { get; set; }
    }
    #endregion

    #region CallbackData
    public class CallbackData
    {
        [JsonProperty(PropertyName = "mode")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BsnDataModes Mode { get; set; }

        [JsonProperty(PropertyName = "type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BsnDataTypes Type { get; set; }

        [JsonProperty(PropertyName = "key")]
        public string Key { get; set; } = "";

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; } = "";

        [JsonProperty(PropertyName = "object")]
        public object Object { get; set; }
    }
    #endregion

    #region ModalOptions
    public class ModalOptions
    {
        public ModalOptions(string title = "")
        {
            Title = title;
        }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; } = "";

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; } = "modal";

        [JsonProperty(PropertyName = "size")]
        public string Size { get; set; } = "";

        [JsonProperty(PropertyName = "header")]
        public bool ShowHeader { get; set; } = true;

        [JsonProperty(PropertyName = "footer")]
        public bool ShowFooter { get; set; } = true;

        [JsonProperty(PropertyName = "show")]
        public bool AutoShow { get; set; } = true;
    }
    #endregion

    #region ComboItems
    public class ComboItem
    {
        public ComboItem(string code, string desc, string extras = "")
        {
            Code = code;
            Desc = desc;
            ExtraData = extras;
        }

        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "desc")]
        public string Desc { get; set; }

        [JsonProperty(PropertyName = "isSelected")]
        public bool IsSelected { get; set; }

        public string ExtraData { get; set; }

        public string CodeAndDesc
        {
            get
            {
                return Code + " - " + Desc;
            }
        }

        public string CodeDesc
        {
            get
            {
                return Code + " <br /> " + Desc;
            }
        }

        public string CodeDescExtra
        {
            get
            {
                return Code + " - " + Desc + " - " + ExtraData;
            }
        }

        public string CodeExtra
        {
            get
            {
                return Code + ":" + ExtraData;
            }
        }
    }
    #endregion

    #region DockPanel
    public class DockPanel
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; } = "";

        [JsonProperty(PropertyName = "caption")]
        public string Caption { get; set; } = "";

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "type")]
        public PanelTypes Type { get; set; } = PanelTypes.Grid;

        [JsonProperty(PropertyName = "zoneId")]
        public string ZoneId { get; set; } = "main";

        [JsonProperty(PropertyName = "visible")]
        public bool Visible { get; set; }

        [JsonProperty(PropertyName = "visibleIndex")]
        public int VisibleIndex { get; set; }
    }
    #endregion

    #region ChartData
    public class ChartData
    {
        [JsonProperty(PropertyName = "Argument")]
        public string Argument { get; set; }

        [JsonProperty(PropertyName = "Value")]
        public int Value { get; set; }
    }
    #endregion

    #region FieldData
    public class FieldData
    {
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "data")]
        public string Data { get; set; }

        public FieldData(string value, string data)
        {
            Value = value;
            Data = data;
        }
    }
    #endregion
}
