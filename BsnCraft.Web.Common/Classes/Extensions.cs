﻿using BsnCraft.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace BsnCraft.Web.Common.Classes
{
    public static class Extensions
    {
        public static void Increment<T>(this Dictionary<T, int> dictionary, T key)
        {
            int count;
            dictionary.TryGetValue(key, out count);
            dictionary[key] = count + 1;
        }

        public static void Increment<T>(this List<ChartData> list, T key)
        {
            ChartData data = list.Where(p => p.Argument == key.ToString()).FirstOrDefault();
            if (data == null)
            {
                list.Add(new ChartData() { Argument = key.ToString(), Value = 1 });
            }
            else
            {
                data.Value++;
            }
            //dictionary.TryGetValue(key, out count);
            //dictionary[key] = count + 1;
        }
    }
}
