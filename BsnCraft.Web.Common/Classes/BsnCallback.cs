﻿using BsnCraft.Web.Common.Views.Menu.Base;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace BsnCraft.Web.Common.Classes
{
    #region Enums
    //Note: WindowLocation will update browser history
    public enum CallbackType
    {
        Normal,
        DataOnly,
        WindowLocation,
        WindowState,
        Modal,
        Form
    }
    #endregion

    public class BsnCallback
    {
        #region Public Properties
        public bool IsValid { get; set; } = true;
        #endregion

        #region Private Properties
        private BsnApplication App { get; set; }
        private Callback Base { get; set; }
        #endregion

        #region Constructors        
        public BsnCallback(BsnApplication app)
        {
            App = app;
            Base = new Callback();
            Data = new CallbackData();
        }

        public BsnCallback(BsnApplication app, object options) : this(app)
        {
            if (options is string strOptions)
            {
                Init(strOptions);
            }
            else
            {
                Init(options.ToString());
            }
        }
        #endregion

        #region Init        
        public void Init(string options = "{}", bool skipError = true)
        {
            if (string.IsNullOrEmpty(options))
            {
                IsValid = false;
                return;
            }
            try
            {
                Base = (Callback)JsonConvert.DeserializeObject(options, typeof(Callback));
                IsValid = true;
                Data = Data ?? new CallbackData();
            }
            catch (Exception e)
            {
                IsValid = false;
                Base = new Callback();
                if (!skipError)
                {
                    Helpers.LogExc(e);
                }
            }
        }
        #endregion

        #region Type
        public CallbackType Type {
            get { return Base.Type; } 
            set { Base.Type = value; }
        }
        #endregion

        #region Menu
        public BsnMenus Menu
        {
            get { return Base.Menu; }
            set { Base.Menu = value; }
        }
        #endregion

        #region Action
        public BsnActions Action
        {
            get { return Base.Action; }
            set { Base.Action = value; }
        }
        #endregion

        #region Mode
        public BsnModes Mode
        {
            get { return Base.Mode; }
            set { Base.Mode = value; }
        }
        #endregion

        #region Id
        public string Id
        {
            get { return Base.Id; }
            set { Base.Id = value; }
        }
        #endregion

        #region Key
        public string Key
        {
            get { return Base.Key; }
            set { Base.Key = value; }
        }
        #endregion

        #region Value
        public string Value
        {
            get { return Base.Value; }
            set { Base.Value = value; }
        }
        #endregion

        #region Data
        public CallbackData Data
        {
            get { return Base.Data; }
            set { Base.Data = value; }
        }
        #endregion

        #region ToBase
        public Callback ToBase()
        {
            return Base;
        }
        #endregion

        #region ToString
        public override string ToString()
        {
            if (!IsValid)
            {
                return string.Empty;
            }
            Base.App = (Base.Type == CallbackType.WindowLocation) ? App.Settings.Application.Instance : "";
            var json = JsonConvert.SerializeObject(Base);
            return Helpers.CleanCallback(json);
        }
        #endregion

        #region ValidateGroup
        public string ValidateGroup { get; set; }
        #endregion

        #region ToCallback
        public string ToCallback()
        {
            var callback = string.Empty;
            if (!string.IsNullOrEmpty(ValidateGroup))
            {
                callback += "if (ASPxClientEdit.ValidateGroup('" + ValidateGroup + "')) ";
            }
            return callback += "App.callback('" + ToString() + "');";
        }
        #endregion

        #region ToFunction
        public string ToFunction()
        {
            var validate = !string.IsNullOrEmpty(ValidateGroup);
            var function = "function(s, e) { ";
            function += "e.processOnServer = false; ";
            if (validate)
            {                
                function += "if (ASPxClientEdit.ValidateGroup('" + ValidateGroup + "')) ";
            }
            function += ToCallback();
            function += " }";
            return function;
        }
        #endregion

        #region ToCheckBox
        public string ToCheckBox(bool getYnValues = true)
        {
            var yn = getYnValues.ToString().ToLower();
            return "function(s, e) { App.checkbox(s, e, '" + ToString() + "', '" + yn + "'); }";
        }
        #endregion

        #region ToComboBox
        public string ToComboBox(bool skipDirty = false)
        {
            return "function(s, e) { App.combobox(s, e, '" + ToString() + "', " + skipDirty.ToString().ToLower() + "); }";
        }
        #endregion

        #region ToDateSelect
        public string ToDateSelect()
        {
            return "function(s, e) { App.dateSelect(s, e, '" + ToString() + "'); }";
        }
        #endregion

        #region ToMemo
        public string ToMemo()
        {
            return "function(s, e) { App.memo(s, e, '" + ToString() + "'); }";
        }
        #endregion

        #region ToTextBox
        public string ToTextBox(string min = "", string typ = "")
        {
            return "function(s, e) { App.textbox(s, e, '" + ToString() + "', '" + min + "', '" + typ + "'); }";
        }
        #endregion

    }
}
