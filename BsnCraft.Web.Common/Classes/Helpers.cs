﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes
{
    public class Helpers
    {
        #region ResolveUrl
        public static string ResolveUrl(string url)
        {
            return (HttpContext.Current.Handler as Page).ResolveUrl(url); ;
        }
        #endregion

        #region CleanString
        public static string CleanString(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9]", "");
        }
        #endregion

        #region CleanSpec        
        public static string CleanSpec(string spec, string linebreak = "\r\n")
        {
            return spec.Replace(((char)254).ToString(), string.Empty)
                .Replace(((char)255).ToString(), linebreak);
        }
        #endregion
        
        #region Clean Callback
        public static string CleanCallback(string json)
        {
            //Strip out default values to remove unnecessary html data
            json = json.Replace("\"type\":\"Normal\",", "");
            json = json.Replace("\"menu\":\"Home\",", "");
            json = json.Replace("\"action\":\"Default\",", "");
            json = json.Replace("\"mode\":\"None\",", "");
            json = json.Replace("\"app\":\"\",", "");
            json = json.Replace("\"id\":\"\",", "");
            json = json.Replace("\"key\":\"\",", "");
            json = json.Replace("\"value\":\"\",", "");
            json = json.Replace("\"data\":null}", "}");
            json = json.Replace(",}", "}");
            return json;
        }
        #endregion

        #region GetArray
        public static string GetArray(string[] values, int index)
        {
            return (values.Length > index) ? values[index] : string.Empty;
        }
        #endregion

        #region LogExc
        public static void LogExc(Exception ex, bool showError = false)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            LogManager.GetCurrentClassLogger().Debug(message);
        }
        #endregion

        #region GetEnums
        public static List<ComboItem> GetEnums<TEnum>() where TEnum : struct
        {
            var enums = new List<ComboItem>();
            if (typeof(TEnum).IsEnum)
            {
                foreach (var value in Enum.GetValues(typeof(TEnum)).Cast<TEnum>())
                {
                    var code = Enum.GetName(typeof(TEnum), value);
                    var desc = EnumDescription(value);
                    enums.Add(new ComboItem(code, desc));
                }
            }
            return enums;
        }
        #endregion

        #region EnumDescription
        public static string EnumDescription(object value)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            return value.ToString();
        }
        #endregion

        #region IsImage
        public static bool IsImage(string Filename)
        {
            var extension = Path.GetExtension(Filename).ToUpper().Replace(".", "");
            switch (extension)
            {
                case "JPG":
                case "JPEG":
                case "GIF":
                case "PNG":
                case "BMP":
                    return true;
            }
            return false;
        }
        #endregion

        #region GetFilterString
        public static string GetFilterString(string field, string[] values, int length = 2, bool where = false)
        {
            string filters = string.Empty;
            if (values.Length < length)
            {
                return filters;
            }
            filters += (where) ? " WHERE" : " AND";
            filters += " " + field + " IN (";
            foreach (var fieldvalue in values)
            {
                filters += "'" + fieldvalue.TrimStart('[').TrimEnd(']') + "',";
            }
            filters = filters.TrimEnd(',');
            filters += ")";
            return filters;
        }
        #endregion

        #region RemoveDefaultFormat
        public static string RemoveDefaultFormat(string code)
        {
            return code.TrimStart('[').TrimEnd(']');
        }
        #endregion

        #region GetDefault
        public static string GetDefault(string[] values)
        {
            foreach (var fieldvalue in values)
            {
                if (fieldvalue.StartsWith("[") && fieldvalue.EndsWith("]"))
                {
                    return fieldvalue.TrimStart('[').TrimEnd(']');
                }
            }
            return string.Empty;
        }
        #endregion

        #region CheckValue
        public static string CheckValue(object value)
        {
            return ((bool)value) ? "checked" : string.Empty;
        }
        #endregion

        #region HtmlLine
        public static HtmlGenericControl HtmlLine(string className = "my-1")
        {
            var htmlLine = new HtmlGenericControl("hr");
            htmlLine.Attributes["class"] = className;
            return htmlLine;
        }

        #endregion

    }
}
