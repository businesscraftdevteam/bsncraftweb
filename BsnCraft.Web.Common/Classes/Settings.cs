﻿using System;
using System.ComponentModel;

namespace BsnCraft.Web.Common.Classes
{
    public class WebSettings
    {
        public AppSettings App { get; set; }
        public CompanySettings Comp { get; set; }
        public UserSettings User { get; set; }
    }

    public class AppSettings
    {
        public string BsnConnect { get; set; } = "localhost:2340";
        public string[] Companies { get; set; } = { "BCHOMES|BusinessCraft Homes" };
        public bool UppercasePassword { get; set; } = true;

        public int BsnPort
        {
            get
            {
                int.TryParse(BsnConnect.Split(':')[1], out int bsnPort);
                return bsnPort;
            }
        }

        public string BsnHost
        {
            get
            {
                return BsnConnect.Split(':')[0];
            }
        }
    }

    public class CompanySettings
    {
        public string[] DocumentTypes { get; set; } = { "" };
        public string CustomerQuery { get; set; } = "ORDER BY CUSNO DESC";
        public string ContactsQuery { get; set; } = "ORDER BY IVTU_CUSTNO DESC, IVTU_SEQNUM ASC";
        public string LeadsQuery { get; set; } = "ORDER BY CO_KEY DESC";
        public string ContractsQuery { get; set; } = "ORDER BY CO_KEY DESC";
        public string ItemsQuery { get; set; } = "ORDER BY ITEMNO ASC";
        public string DisplayCentresQuery { get; set; } = "";
        public string EventsQuery { get; set; } = "";
        public string DefaultPersonalSolicitor { get; set; } = "";
        public string DefaultCompanySolicitor { get; set; } = "";
        public string DefaultLendingAuthority { get; set; } = "";
        public int PhotoWidth { get; set; } = 1600;
        public int PhotoHeight { get; set; } = 1200;
        public int ThumbnailWidth { get; set; } = 300;
        public int ThumbnailHeight { get; set; } = 200;        
        public string ThumbnailDefault { get; set; } = "~/Content/images/noimage.png";
        public string CustomerCode { get; set; } = "";
        public string DefaultState { get; set; } = "NSW";
        public string EstateCode { get; set; } = "$ESTAT";
        public string CouncilCode { get; set; } = "$COUNC";
        public string[] ActiveContractStatusList { get; set; } = { "" };
        public string[] ActiveLeadStatusList { get; set; } = { "" };
        public string[] SalesConsultantCategories { get; set; } = { "" };
        public string[] SalesEstimateSections { get; set; } = { "SE" };
        public string[] VariationEstimateSections { get; set; } = { "V" };                               
        public string[] JobInfoDocumentTypes { get; set; } = { "" };
        public string[] HeadingDocumentTypes { get; set; } = { "" };        
        public string[] JobInfoGroups { get; set; } = { "" };
        public string[] EventCategories { get; set; } = { "" };
        public string[] HideMainMenu { get; set; } = { "" };
        public string[] HideContractDetails { get; set; } = { "" };
        public string JobInfoTemplate { get; set; } = "";
        public string PricingArea { get; set; } = "";
        public string EffectiveDate { get; set; } = "";
        public int EstimateDetailIncr { get; set; } = 10;
        public int JobInfoMemoRows { get; set; } = 10;
        public int DealSubmitEvent { get; set; } = 0;
        public bool EstimateSubHeadings { get; set; } = true;
        public bool AllowEditParagraphs { get; set; } = true;
        public bool AllowLockParagraphs { get; set; } = false;
        public bool ItemPriceLock { get; set; } = false;
        public bool RestrictLeads { get; set; } = false;
        public bool RestrictContracts { get; set; } = false;
        public bool JobInfoGroupsVertical { get; set; } = false;
        public bool ShowClaimNumber { get; set; } = false;
        public bool ShowFullError { get; set; } = false;
        public bool ShowPDFTools { get; set; } = false;
        public bool UseBsnMerge { get; set; } = false;
        public bool AllowEmptyEstimates { get; set; } = true;
        public bool TouchMode { get; set; } = false;

        public string AlternateDSN { get; set; } = "";
        public string WelcomeMessage { get; set; } = "";
        public string DefaultEmptyParagraph { get; set; } = "";
        public string DealDocumentTypes { get; set; } = "[]";
        public string SaleTypeDocTypes { get; set; } = "[]";

        public string[] OpCentreFilter { get; set; } = { "" };
        public string[] DistrictFilter { get; set; } = { "" };
        public string[] CouncilFilter { get; set; } = { "" };
        public string[] PromotionFilter { get; set; } = { "" };
        public string[] SaleCentreFilter { get; set; } = { "" };
        public string[] SaleTypeFilter { get; set; } = { "" };
        public string[] ActivitiesFilter { get; set; } = { "" };
    }

    public class UserSettings
    {
        public string DefaultPage { get; set; } = "";
        public string Theme { get; set; } = "default";
        public bool JavascriptPDF { get; set; } = true;
    }

    #region Not Implemented

    /*
    public enum CustomTopics
    {
        [Description("Default Page")]
        DefaultPage,
        [Description("Grid Size")]
        GridSize,
        [Description("Customer Grid")]
        CustomerGrid,
        [Description("Display Info Grid")]
        DisplayInfoGrid,
        [Description("Leads Details View")]
        LeadsDetailsView,
        [Description("Estimate View")]
        EstimateView,
    }


    public enum AdminTopics
    {
        [Description("System Admin")]
        SystemAdmin,
        [Description("Company Admin")]
        CompanyAdmin,
    }

    public enum Areas
    {
        [Description("Default")]
        Default,
        [Description("Customers")]
        Customers,
        [Description("Contracts")]
        Contracts,
        [Description("Leads")]
        Leads,
        [Description("Charts")]
        Charts,
        [Description("Reports")]
        Reports,
        [Description("Dashboards")]
        Dashboards,
        [Description("Display Centres")]
        DisplayCentres,
        [Description("Administration")]
        Admin
    }

    public enum AreaTopics
    {
        [Description("Default Page")]
        Default,
        [Description("Add New")]
        Add,
        [Description("Bookmarks")]
        Bookmarks,
    }

    public enum Levels
    {
        System,
        Company,
        Role,
        User
    }

    public enum Types
    {
        Area,
        Custom,
        Admin
    }

    public class UserRole
    {
        public string Code { get; set; }
        public string Desc { get; set; }
    }

    public class AreaSetting : SettingBase
    {
        public AreaTopics Topic { get; set; }

        public AreaSetting()
        {
            Type = Types.Area;
        }
    }

    public class CustomSetting : SettingBase
    {
        public CustomTopics Topic { get; set; }

        public CustomSetting()
        {
            Type = Types.Custom;
        }
    }

    public class AdminSetting : SettingBase
    {
        public AdminTopics Topic { get; set; }

        public AdminSetting()
        {
            Type = Types.Admin;
        }
    }

    public abstract class SettingBase
    {
        public Types Type { get; set; }
        public Areas Area { get; set; }
        public Levels Level { get; set; }

        #region SettingKey
        public string SettingKey
        {
            get
            {
                return Utils.GetTopicDescription(this);
            }
        }
        #endregion

        #region SettingValue
        private Nullable<bool> _settingValue;
        public Nullable<bool> SettingValue
        {
            get
            {
                return _settingValue;
            }
            set
            {
                if (_settingValue != value)
                {
                    _settingValue = value;
                }
            }
        }
        #endregion        
    }
    */
    #endregion

}
