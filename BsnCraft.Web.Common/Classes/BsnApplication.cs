﻿using System;
using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Settings;
using DevExpress.Web.Bootstrap;
using DevExpress.Web;
using NLog;
using System.IO;
using DevExpress.Export;
using System.Web.UI.HtmlControls;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Settings.Base;
using Pg = System.Web.UI;
using XP = DevExpress.XtraPrinting;
using Model = BsnCraft.Web.Common.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using BsnCraft.Web.Common.Views.Viewer;
using System.Web.UI;
using BsnCraft.Web.Common.Views.Buttons;
using DevExpress.XtraReports.Native;
using DevExpress.DashboardWeb;
using DevExpress.XtraReports.Web;

namespace BsnCraft.Web.Common.Classes
{
    public class BsnApplication
    {
        #region Public Properties        
        public BsnApi Api { get; private set; }
        public BsnOdbc Odbc { get; private set; }
        public BsnUtils Utils { get; private set; }
        public SettingsManager Settings { get; private set; }
        public BsnController VM { get; private set; }
        public BsnNav Nav { get; private set; }
        public MenuManager Menus { get; private set; }
        public FormManager Forms { get; private set; }
        public GridManager Grids { get; private set; }
        public BsnViewer Viewer { get; private set; }
        public BsnCallback Callback { get; private set; }
        public BsnCallback DataCallback { get; private set; }
        public Callback DataLocation { get; set; }
        public Pg.Page Page { get; private set; }
        public BootstrapCallbackPanel Panel { get; private set; }
        public BootstrapCallbackPanel Modal { get; private set; }
        public BootstrapCallbackPanel DataPanel { get; private set; }
        public HtmlGenericControl UploadPanel { get; private set; }        
        public List<string> UploadedFiles { get; private set; }
        public BootstrapUploadControl UploadControl { get; private set; }
        public ASPxDashboard DashboardDesigner { get; private set; }
        public ASPxWebDocumentViewer ReportViewer { get; private set; }
        public ASPxReportDesigner ReportDesigner { get; private set; }
        public DevxDataSerializer DataSerializer { get; private set; }
        public HtmlGenericControl Main { get; private set; }
        public Version Version { get; set; }
        public string ReportKey { get; set; }
        //public BsnForm ModalForm { get; private set; }

        //Can be checked from Page_Load
        public bool IsCallback { get; set; }
        public bool IsDataCallback { get; set; }
        public bool IsPanelCallback
        {
            get
            {
                return IsCallback || IsDataCallback;
            }
        }

        //Set from inside event handler
        public bool InCallback { get; set; }
        public bool InDataCallback { get; set; }  
        public bool InPanelCallback
        {
            get
            {
                return InCallback || InDataCallback;
            }
        }
        #endregion

        #region Private Properties
        private Logger Log { get; set; }
        private BsnHandler Handler { get; set; }
        private ASPxDockManager DockManager { get; set; }
        private bool UploadComplete { get; set; }
        private Model.Login Login { get; set; }        
        private DateTime CallbackStart { get; set; }
        #endregion

        #region JS Properties
        private string JsToolkitPrev { get; set; }
        private string JsBootstrapPrev { get; set; }
        private string JsPanels { get; set; }
        private List<string> JsToast { get; set; }        
        private StyleTypes JsToastType { get; set; } = StyleTypes.primary;
        private ModalOptions JsModal { get; set; }
        //private List<FieldData> JsFields { get; set; }
        #endregion

        #region Current Menu Values
        public BsnMenus Menu { get { return Menus.Current.UID.Item1; } }
        public BsnActions Action { get { return Menus.Current.Action; } }
        public string Id { get { return Menus.Current.Id; } }
        public BsnModes Mode { get { return Menus.Current.Mode; } }
        public string Key { get { return Menus.Current.Key; } }
        public string Value { get { return Menus.Current.Value; } }
        #endregion        

        #region Constructor
        public BsnApplication()
        {
            Log = LogManager.GetLogger(GetType().FullName);
            Api = new BsnApi(this);
            Odbc = new BsnOdbc(this);
            Utils = new BsnUtils(this);
            Settings = new SettingsManager(this);
            VM = new BsnController(this);
            Nav = new BsnNav(this);
            Menus = new MenuManager(this);
            Forms = new FormManager(this);
            Grids = new GridManager(this);
            Viewer = new BsnViewer(this);
            Handler = new BsnHandler(this);
            Callback = new BsnCallback(this);
            DataCallback = new BsnCallback(this);
            JsToast = new List<string>();            
            Login = new Model.Login();
            UploadedFiles = new List<string>();
            DockManager = new ASPxDockManager
            {
                ClientInstanceName = "DockManager"
            };
            DataSerializer = new DevxDataSerializer(this);
            SerializationService.RegisterSerializer(DevxDataSerializer.Name, DataSerializer);
            //JsFields = new List<FieldData>();
        }
        #endregion

        #region Init
        public void Init()
        {
            Api.Init();
            Settings.Init();
            Utils.Init();
            Menus.Init();
            Forms.Init();
            Grids.Init();
            VM.Init();
            Nav.Init();
        }
        #endregion
        
        #region Load
        public void Load(Pg.Page page)
        {
            try
            {
                Page = page;
                LoadApp();
                Menus.Current.OnLoad();                
            }
            catch (Exception ex)
            {
                Utils.LogExc(ex);
            }
        }
        #endregion

        #region Load App
        private void LoadApp()
        {
            InCallback = false;
            InDataCallback = false;
            Panel = (BootstrapCallbackPanel)Page.FindControl("panel");
            IsCallback = Panel.IsCallback;
            Panel.Callback += OnCallback;
            Panel.CustomJSProperties += CustomJSProperties;
            DataCallback.Init();
            DataPanel = (BootstrapCallbackPanel)Page.FindControl("data");            
            DataPanel.Callback += OnDataCallback;
            DataPanel.CustomJSProperties += DataJSProperties;            
            Main = (HtmlGenericControl)Panel.FindControl("main");
            UploadPanel = (HtmlGenericControl)Panel.FindControl("upload");
            UploadControl = (BootstrapUploadControl)Panel.FindControl("UploadControl");
            UploadControl.FilesUploadComplete += FilesUploadComplete;
            UploadControl.FileUploadComplete += FileUploadComplete;
            UploadControl.AdvancedModeSettings.TemporaryFolder = Utils.UploadFolder;
            DashboardDesigner = (ASPxDashboard)Panel.FindControl("DashboardDesigner");
            ReportViewer = (ASPxWebDocumentViewer)Panel.FindControl("ReportViewer");
            ReportDesigner = (ASPxReportDesigner)Panel.FindControl("ReportDesigner");
            Modal = (BootstrapCallbackPanel)Page.FindControl("modal");            
            Modal.Callback += OnDataCallback;
            Modal.CustomJSProperties += ModalJSProperties;
            Modal.Attributes["class"] = "modal fade";
            Modal.Attributes["tabindex"] = "-1";
            Modal.Attributes["role"] = "dialog";
            Modal.Attributes["aria-labelledby"] = "modalLabel";
            Modal.Attributes["aria-hidden"] = "true";

            IsDataCallback = false;
            IsDataCallback |= DataPanel.IsCallback;
            IsDataCallback |= Modal.IsCallback;
            
            if (Page.IsCallback || Page.IsPostBack || UploadControl.IsCallback)
            {
                return;
            }
            Settings.Load();
            AddToast(Settings.Comp(CompanySettingsType.WelcomeMessage).ToString());
            Page.Header.Controls.Add(Utils.Styles(true));
            Page.Header.Controls.Add(Utils.Scripts);
            Page.Header.Controls.Add(Utils.FavIcon32);
            Page.Header.Controls.Add(Utils.FavIcon16);
            Page.Header.Controls.Add(Utils.TouchIcon);
            Page.Header.DataBind();            
            //Try to use querystring instead of routing
            //if (Page.RouteData.Values.Count == 0)
            if (Page.Request.QueryString.Count == 0)
            {
                if (LastTab())
                {
                    return;
                }
            }
            //Try to use querystring instead of routing
            //var menu = Page.RouteData.Values["menu"].ValueOrDefault("Home").ToMenu();
            //var action = Page.RouteData.Values["action"].ValueOrDefault("Default").ToAction();
            //var id = Page.RouteData.Values["id"].ValueOrDefault(string.Empty);
            //var mode = Page.RouteData.Values["mode"].ValueOrDefault(string.Empty).ToMode();
            //var key = Page.RouteData.Values["key"].ValueOrDefault(string.Empty);
            var menu = Page.Request.QueryString["m"].ValueOrDefault("Home").ToMenu();
            var action = Page.Request.QueryString["a"].ValueOrDefault("Default").ToAction();
            var mode = Page.Request.QueryString["o"].ValueOrDefault("None").ToMode();
            var id = Page.Request.QueryString["i"].ValueOrDefault(string.Empty);            
            var key = Page.Request.QueryString["k"].ValueOrDefault(string.Empty);
            var value = Page.Request.QueryString["v"].ValueOrDefault(string.Empty);
            LoadMenu(menu, action, id, mode, key, value);
        }
        #endregion

        #region Last Tab
        public bool LastTab()
        {
            var tab = Settings.User.Tabs.OrderByDescending(p => p.TimeActive).ToList().FirstOrDefault();
            if (tab != null)
            {
                LoadMenu(tab.Menu, tab.Action, tab.Id, tab.Mode, tab.Key);
                return true;
            }
            return false;
        }
        #endregion

        #region Load Menu
        public void LoadMenu(BsnMenus menu, BsnActions action, string id, BsnModes mode = BsnModes.None, string key = "", string value = "")
        {
            Menus.Set(menu, action);
            Menus.Current.Load(action, id, mode, key, value);                        
            Settings.MainTab(menu, action, id, mode, key);
        }
        #endregion

        #region LoginCheck
        private bool LoginCheck
        {
            get
            {
                if (!Api.Connected)
                {
                    Init();
                    Main.Attributes["class"] = "h-100 d-flex flex-column justify-content-center align-items-center";
                    Forms.Get(BsnForms.Login).Load(Login);
                    Main.Controls.Add(Forms.Get(BsnForms.Login).ToControl());
                    return false;
                }
                return true;
            }
        }
        #endregion

        #region OnCallback
        public void OnCallback(object sender, CallbackEventArgsBase e)
        {
            try
            {
                CallbackStart = DateTime.Now;
                InCallback = true;
                Callback.Init(e.Parameter);
                Handler.Process();
                if (LoginCheck)
                {
                    Menus.Current.OnLoad();
                }
            }
            catch (Exception ex)
            {
                Utils.LogExc(ex);
            }
        }
        #endregion
        
        #region OnDataCallback        
        public void OnDataCallback(object sender, CallbackEventArgsBase e)
        {
            if (!Api.Connected)
            {
                return;
            }
            try
            {
                CallbackStart = DateTime.Now;
                InDataCallback = true;
                DataCallback.Init(e.Parameter);
                Handler.Process(true);

                switch (DataCallback.Mode)
                {
                    case BsnModes.SaveSettings:
                        return;
                }

                Menus.Current.OnLoad();
            }
            catch (Exception ex)
            {
                Utils.LogExc(ex);
            }
        }
        #endregion        

        #region Add Grid
        public Control AddGrid(BsnGrid grid)
        {
            Control control = null;
            try
            {
                grid.ShowExportButton = true;
                grid.Load();
                control = grid.ToControl();

                //TODO: for below "download" option to work, need page_load

                Main.Controls.Add(control);
                if (Mode == BsnModes.ExportGrid)
                {
                    var filename = ExportGrid(grid.Grid, Value.ValueOrDefault("PDF"));
                    Main.Controls.Remove(control);

                    Viewer.Filename = filename;
                    Main.Controls.Add(Viewer.ToControl());
                }                
            }
            catch (Exception ex)
            {
                Utils.LogExc(ex);
            }
            return control;
        }
        #endregion

        #region Export Grid
        public string ExportGrid(BootstrapGridView grid, string format, bool download = false)
        {
            try
            {
                int count = 0;
                int[] indexes = null;
                if (grid != null)
                {
                    count = grid.VisibleColumns.Count;
                    indexes = new int[count];
                    for (int i = count - 1; i >= 0; i--)
                    {
                        int index = grid.VisibleColumns[i].Index;
                        indexes[i] = index;
                        //Use ExportWidth with any value to hide
                        grid.Columns[index].Visible = (grid.Columns[index].ExportWidth < 1);
                    }
                }

                var exporter = new ASPxGridViewExporter
                {
                    GridViewID = grid.ID
                };
                Main.Controls.Add(exporter);
                var filename = PerformExport(exporter, format, download);
                Main.Controls.Remove(exporter);
                

                if (grid != null)
                {
                    for (int i = count - 1; i >= 0; i--)
                    {
                        //Restore visibile
                        grid.Columns[indexes[i]].Visible = true;
                    }
                }

                return filename;
            }
            catch (Exception ex)
            {
                Utils.LogExc(ex);
            }

            return string.Empty;
        }

        public string PerformExport(ASPxGridViewExporter exporter, string format, bool download = false)
        {
            var filename = string.Empty;
            if (download)
            {
                switch (format.ToUpper())
                {
                    case "PDF":
                        exporter.WritePdfToResponse();
                        break;
                    case "XLS":
                        exporter.WriteXlsToResponse(new XP.XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });
                        break;
                    case "XLSX":
                        exporter.WriteXlsxToResponse(new XP.XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
                        break;
                    case "CSV":
                        exporter.WriteCsvToResponse(new XP.CsvExportOptionsEx() { ExportType = ExportType.WYSIWYG });
                        break;
                    case "RTF":
                        exporter.WriteRtfToResponse();
                        break;
                }
                return filename;
            }

            switch (format.ToUpper())
            {
                case "PDF":
                    filename = SaveFile(actionToCall => exporter.WritePdf(actionToCall), format.ToLower());
                    break;
                case "XLS":
                    filename = SaveFile(actionToCall => exporter.WriteXls(actionToCall), format.ToLower());
                    break;
                case "XLSX":
                    filename = SaveFile(actionToCall => exporter.WriteXlsx(actionToCall), format.ToLower());
                    break;
                case "CSV":
                    filename = SaveFile(actionToCall => exporter.WriteCsv(actionToCall), format.ToLower());
                    break;
                case "RTF":
                    filename = SaveFile(actionToCall => exporter.WriteRtf(actionToCall), format.ToLower());
                    break;
            }
            return filename;
        }

        protected string SaveFile(Action<Stream> action, string format)
        {
            var uid = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
            var filename = string.Format("{0}/{1}.{2}", Utils.DownloadFolder, uid, format);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                action(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);
                using (FileStream fileStream = new FileStream(Page.MapPath(filename), FileMode.Create, FileAccess.Write))
                {
                    memoryStream.WriteTo(fileStream);
                }
            }
            return filename;
        }
        #endregion

        #region Add Toast
        public void AddToast(string message, bool error = false, bool display = true)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (!error)
            {
                JsToastType = StyleTypes.primary;
                if (!JsToast.Contains(message))
                {
                    JsToast.Add(message);
                }                
                return;
            }

            if (!Settings.Comp(CompanySettingsType.ShowFullError).ToBool() && !display)
            {
                message = "An error occurred. Please try again.";
            }

            JsToastType = StyleTypes.danger;
            if (!JsToast.Contains(message))
            {
                JsToast.Add(message);
            }
        }
        #endregion

        #region UpdateField
        //public void UpdateField(string value, string data)
        //{
        //    if (JsFields.Any(p => p.Value.Equals(value)))
        //    {
        //        var field = JsFields.Find(p => p.Value.Equals(value));
        //        field.Value = data;
        //        return;
        //    }
        //    JsFields.Add(new FieldData(value, data));
        //}
        #endregion

        #region CustomJSProperties                
        public void CustomJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            try
            {
                Log.Info("Main CB Ms = " + (DateTime.Now - CallbackStart).TotalMilliseconds);
                e.Properties["cpToast"] = JsonConvert.SerializeObject(JsToast);
                e.Properties["cpToastType"] = JsToastType;
                e.Properties["cpPanels"] = JsPanels;
                JsPanels = string.Empty;
                JsToast.Clear();
                JsToastType = StyleTypes.primary;

                if (DataLocation != null)
                {
                    var location = JsonConvert.SerializeObject(DataLocation);
                    e.Properties["cpDataLocation"] = Helpers.CleanCallback(location);
                    DataLocation = null;
                    return;
                }

                e.Properties["cpDataLocation"] = "";
                e.Properties["cpToolkitPrev"] = "";
                e.Properties["cpToolkit"] = "";
                e.Properties["cpBootstrapPrev"] = "";
                e.Properties["cpBootstrap"] = "";

                var toolkitLight = Utils.CssFolder() + "toolkit-light.min.css";
                var toolkitDark = Utils.CssFolder() + "toolkit-inverse.min.css";
                var toolkit = Utils.DarkTheme ? toolkitDark : toolkitLight;
                var bootstrap = (Utils.CustomTheme) ?
                    Utils.CssFolder() + "themes/" + Utils.Theme.ToLower() + "/bootstrap.min.css" :
                    string.Empty;

                if (Api.Connected)
                {
                    e.Properties["cpToolkitPrev"] = JsToolkitPrev;
                    e.Properties["cpToolkit"] = toolkit;
                    JsToolkitPrev = toolkit;

                    if (Utils.CustomTheme)
                    {
                        e.Properties["cpBootstrapPrev"] = JsBootstrapPrev;
                        e.Properties["cpBootstrap"] = bootstrap;
                        JsBootstrapPrev = bootstrap;
                    }
                    else
                    {
                        e.Properties["cpBootstrapPrev"] = JsBootstrapPrev;
                        JsBootstrapPrev = "";
                    }
                }
                else
                {
                    //reset themes
                    e.Properties["cpToolkitPrev"] = JsToolkitPrev;
                    e.Properties["cpToolkit"] = toolkitLight;
                    JsToolkitPrev = toolkitLight;

                    e.Properties["cpBootstrapPrev"] = JsBootstrapPrev;
                    e.Properties["cpBootstrap"] = "";
                    JsBootstrapPrev = "";
                }   

                e.Properties["cpSaveSettings"] = Settings.Update;
                
                //Could add ping/pong with idle state
                //e.Properties["cpConnected"] = Api.Connected;
                //Api.Disconnect();
            }
            catch (Exception ex)
            {
                Utils.LogExc(ex);
            }
        }
        #endregion

        #region DataJSProperties                
        public void DataJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            try
            {
                Log.Info("Main CB Ms = " + (DateTime.Now - CallbackStart).TotalMilliseconds);
                if (DataLocation != null)
                {
                    var location = JsonConvert.SerializeObject(DataLocation);
                    e.Properties["cpDataLocation"] = Helpers.CleanCallback(location);
                    DataLocation = null;
                    return;
                }
                e.Properties["cpDataLocation"] = "";
                e.Properties["cpToastType"] = JsToastType;
                e.Properties["cpToast"] = (JsToast.Count > 0) ? JsonConvert.SerializeObject(JsToast) : "";                                
                JsToastType = StyleTypes.primary;
                JsToast.Clear();
                //e.Properties["cpFields"] = (JsFields.Count > 0) ? JsonConvert.SerializeObject(JsFields) : "";
                //JsFields.Clear();
            }
            catch (Exception ex)
            {
                Utils.LogExc(ex);
            }
        }
        #endregion

        #region ModalJSProperties                
        public void ModalJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            try
            {
                Log.Debug("Finished Modal Callback: " + DateTime.Now.ToString("HH:mm:ss.fff"));
                if (JsModal != null)
                {
                    e.Properties["cpModal"] = JsonConvert.SerializeObject(JsModal);
                }
                JsModal = null;
            }
            catch (Exception ex)
            {
                Utils.LogExc(ex);
            }
        }
        #endregion

        #region FileUploadComplete
        public void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            try
            {
                if (UploadComplete)
                {
                    //User is reuploading from same page
                    UploadedFiles.Clear();
                }
                var uploadedFile = e.UploadedFile;
                var fileName = uploadedFile.FileName;
                var resultFileName = Path.GetFileNameWithoutExtension(fileName);
                resultFileName += "_" + DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                resultFileName += "_" + UploadedFiles.Count + Path.GetExtension(fileName);
                var resultFilePath = Utils.UploadFolder + resultFileName;
                UploadedFiles.Add(resultFilePath.MapPath());
                uploadedFile.SaveAs(resultFilePath.MapPath());
                string url = "javascript:App.showFile('" + resultFilePath + " ')";
                var sizeInKilobytes = uploadedFile.ContentLength / 1024;
                string sizeText = sizeInKilobytes.ToString() + " KB";
                e.CallbackData = fileName + "|" + url + "|" + sizeText;
            }
            catch (Exception ex)
            {
                Utils.LogExc(ex);
            }
        }

        private void FilesUploadComplete(object sender, FilesUploadCompleteEventArgs e)
        {
            UploadComplete = true;
        }
        #endregion

        #region LoadModal
        public void LoadModal(object data, BsnForm form, ModalOptions options, BsnButton button = null, bool readOnly = false)
        {            
            form.Load(data, readOnly, BsnFormTypes.InModal);
            LoadModal(form.ToControl(), options, button);
        }
        
        public void LoadModal(Control content, ModalOptions options, BsnButton button = null)
        {
            try
            {
                var dialog = (HtmlGenericControl)Modal.FindControl("modalDialog");
                if (!string.IsNullOrEmpty(options.Size))
                {
                    dialog.Attributes["class"] += " modal-" + options.Size;
                }

                var header = Modal.FindControl("modalHeader");
                header.Visible = options.ShowHeader;
                if (options.ShowHeader)
                {
                    var label = Modal.FindControl("modalLabel");
                    label.ClientIDMode = ClientIDMode.Static;
                    label.Controls.Add(new LiteralControl(options.Title));
                }
                var body = Modal.FindControl("modalBody");
                body.Controls.Add(content);
                var footer = Modal.FindControl("modalFooter");
                footer.Visible = options.ShowFooter;
                if (options.ShowFooter)
                {
                    if (button != null)
                    {
                        footer.Controls.Add(button.ToControl());
                    }
                }
                JsModal = options;
            }
            catch (Exception e)
            {
                Utils.LogExc(e);
            }
        }
        #endregion

        #region LoadPanels
        public void LoadPanels(List<DockPanel> panels)
        {
            var callback = new BsnCallback(this)
            {
                Type = CallbackType.DataOnly,
                Menu = Menu,
                Action = Action,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.SavePanels
                }
            };
            JsPanels = JsonConvert.SerializeObject(panels);
            var dock = "function(s, e) { App.dockPanels(s, e, '" + callback.ToString() + "'); }";
            DockManager.ClientSideEvents.AfterDock = dock;
            Main.Controls.Add(DockManager);
        }
        #endregion
        
    }
}
