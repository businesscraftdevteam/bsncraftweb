﻿using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.ViewModels;
using DevExpress.Export;
using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using DevExpress.XtraPrinting;
using NLog;
using System;
using System.IO;
using System.Web.UI;

namespace BsnCraft.Web.Common
{
    public class BsnUserControl : UserControl
    {
        protected PageModes PageMode { get; set; } = PageModes.Default;
        protected BsnPage Pg { get { return (BsnPage)Page; } }
        protected Boolean IsVisible { get; set; }
        protected BsnCallback InitCallback { get; set; }

        #region Constructor        
        protected Logger Log { get; private set; }        
        protected BsnUserControl()
        {
            InitCallback = new BsnCallback();
            Log = LogManager.GetLogger(GetType().FullName);
        }
        #endregion

        protected virtual void Page_Load(object sender, EventArgs e)
        {
            //Stop OnLoad events from executing on callbacks
            //Unless InitControl is called first
            //E.g. Logout will cause object reference errors
            Visible = IsVisible;
        }

        #region InitControl        
        public virtual void InitControl(object option)
        {            
            IsVisible = true;
            Visible = true;
            if (option is BsnCallback callback)
            {
                InitCallback = callback;
                LoadKey(callback.Key);
            }
            if (option is string strOption)
            {
                InitCallback = new BsnCallback(strOption, true);
                if (InitCallback.IsValid)
                {
                    LoadKey(InitCallback.Key);
                }
                else
                {
                    LoadKey(strOption);
                }
            }
            LoadCombos();
        }
        #endregion

        #region LoadKey
        protected virtual void LoadKey(string key)
        {
            
        }
        #endregion

        #region LoadCombos
        protected virtual void LoadCombos()
        {

        }
        #endregion

        #region ValidationGroup
        protected string validationGroup;
        public virtual string ValidationGroup
        {
            get
            {
                if (string.IsNullOrEmpty(validationGroup))
                {
                    validationGroup = ControlId.ToString();
                }
                return validationGroup;
            }
        }
        #endregion
        
        //Todo: Simplify below with initial values

        #region ControlId
        private BsnControls controlId = BsnControls.None;
        public virtual BsnControls ControlId
        {
            get
            {
                return controlId;
            }
            set
            {
                controlId = value;
            }
        }
        #endregion

        #region CBType
        private CallbackType cbType = CallbackType.Normal;
        public virtual CallbackType CBType
        {
            get
            {
                return cbType;
            }
            set
            {
                cbType = value;
            }
        }
        #endregion

        #region Mode
        private CallbackMode mode = CallbackMode.None;
        public virtual CallbackMode Mode
        {
            get
            {
                return mode;
            }
            set
            {
                mode = value;
            }
        }
        #endregion

        #region Key
        private string key = string.Empty;
        public virtual string Key
        {
            get
            {
                return key;
            }
            set
            {
                key = value;
            }
        }
        #endregion

        #region Caption
        private string caption = string.Empty;
        public virtual string Caption
        {
            get
            {
                return caption;
            }
            set
            {
                caption = value;
            }
        }
        #endregion

        #region Size
        private ModalSizes size = ModalSizes.Large;
        public virtual ModalSizes Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
            }
        }
        #endregion
        
        #region ModalEvent
        private ModalEvents modalEvent = ModalEvents.None;
        public virtual ModalEvents ModalEvent
        {
            get
            {
                return modalEvent;
            }
            set
            {
                modalEvent = value;
            }
        }
        #endregion

        #region SaveCommand
        protected string ExtraJSCommand = string.Empty;
        protected virtual string SaveCommand
        {
            get
            {
                string javascript = "if (ASPxClientEdit.ValidateGroup('" + ValidationGroup + "')) {";
                if (!string.IsNullOrEmpty(ExtraJSCommand))
                {
                    javascript += ExtraJSCommand;
                }
                javascript += "bsn.callback('" + GetCallback().ToString() + "');";
                javascript += "}";
                return javascript;
            }
        }
        #endregion

        #region Virtual Methods        
        public virtual void Edit()
        {
            Mode = CallbackMode.Save;
            ModalEvent = ModalEvents.Show;
        }

        public virtual void Save()
        {
            ModalEvent = ModalEvents.Hide;            
        }
        
        public virtual void Delete()
        {
            
        }
        
        public virtual void View()
        {
            ModalEvent = ModalEvents.Show;
        }

        public virtual string ConfirmDelete()
        {
            return string.Empty;
        }

        public virtual void Process(BsnCallback Callback)
        {
            
        }
        #endregion

        #region GetCallback
        public BsnCallback GetCallback()
        {
            var callback = new BsnCallback
            {
                Type = CBType,
                Id = ControlId,
                Mode = Mode,
                Key = Key
            };
            return callback;
        }
        #endregion
        
        #region Locked
        public virtual bool Locked { get; set; }
        #endregion

        #region ReadOnly
        public virtual bool ReadOnly { get; set; }
        #endregion

        #region CloseControl
        public virtual void CloseControl()
        {
            VarUtils.Set(WebVars.CallbackData, null);
        }
        #endregion

        #region ExportGrid        
        protected void ExportGrid(ASPxGridViewExporter exporter, BootstrapGridView grid)
        {
            ExportGrid(exporter, grid, false);
        }

        protected void ExportGrid(ASPxGridViewExporter exporter, BootstrapGridView grid, bool download)
        {
            if (grid == null)
                return;

            var exportKey = VarUtils.Str(WebVars.ExportGrid);            
            if (string.IsNullOrEmpty(exportKey))
            {
                return;
            }            

            var keyval = exportKey.Split(':');            
            string export = Utils.GetArray(keyval, 0);
            string gridid = Utils.GetArray(keyval, 1);
            string gridname = grid.ClientInstanceName;

            if (string.IsNullOrEmpty(export) || gridid != gridname)
            {
                return;
            }

            VarUtils.Set(WebVars.ExportGrid, null);

            int count = 0;
            int[] indexes = null;
            if (grid != null)
            {
                count = grid.VisibleColumns.Count;
                indexes = new int[count];
                for (int i = count - 1; i >= 0; i--)
                {
                    int index = grid.VisibleColumns[i].Index;
                    indexes[i] = index;
                    //Use ExportWidth with any value to hide
                    grid.Columns[index].Visible = (grid.Columns[index].ExportWidth < 1);                     
                }
            }

            switch (export.ToUpper())
            {
                case "PDF":
                    if (download)
                    {
                        exporter.WritePdfToResponse(gridname);
                    }
                    else
                    {
                        SaveFile(actionToCall => exporter.WritePdf(actionToCall), export.ToLower());
                    }
                    break;
                case "XLS":
                    if (download)
                    {
                        exporter.WriteXlsToResponse(gridname, new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });
                    }
                    else
                    {
                        SaveFile(actionToCall => exporter.WriteXls(actionToCall), export.ToLower());
                    }
                    break;
                case "XLSX":
                    if (download)
                    {
                        exporter.WriteXlsxToResponse(gridname, new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
                    }
                    else
                    {
                        SaveFile(actionToCall => exporter.WriteXlsx(actionToCall), export.ToLower());
                    }
                    break;
                case "CSV":
                    if (download)
                    {
                        exporter.WriteCsvToResponse(gridname, new CsvExportOptionsEx() { ExportType = ExportType.WYSIWYG });
                    }
                    else
                    {
                        SaveFile(actionToCall => exporter.WriteCsv(actionToCall), export.ToLower());
                    }
                    break;
                case "RTF":
                    if (download)
                    {
                        exporter.WriteRtfToResponse(gridname);
                    }
                    else
                    {
                        SaveFile(actionToCall => exporter.WriteRtf(actionToCall), export.ToLower());
                    }
                    break;
            }

            if (grid != null)
            {
                for (int i = count - 1; i >= 0; i--)
                {
                    //Restore visibile
                    grid.Columns[indexes[i]].Visible = true;
                }
            }
        }
     
        protected void SaveFile(Action<Stream> action, string format)
        {
            var filename = string.Format("{0}/{1}.{2}", Utils.DownloadFolder, Path.GetRandomFileName(), format);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                action(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);
                using (FileStream fileStream = new FileStream(MapPath(filename), FileMode.Create, FileAccess.Write))
                {
                    memoryStream.WriteTo(fileStream);
                }
            }
            VarUtils.Set(WebVars.JSViewFile, filename);
        }
        #endregion

        #region CustomJSProperties
        protected virtual void CustomJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            e.Properties["cpAlerts"] = Utils.GetAlerts;
            e.Properties["cpSearchUrl"] = ResolveUrl("~/Shared/Search.aspx");
        }
        #endregion

        #region GridSize
        protected int GridSize
        {
            get
            {
                var gridSize = Pg.IntSetting(PageSettings.GridSize);
                if (gridSize == 0)
                {
                    gridSize = 10;
                }
                return gridSize;
            }
        }
        #endregion
    }
}