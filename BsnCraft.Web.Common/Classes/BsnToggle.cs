﻿using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes
{
    public enum ToggleSize
    {
        Large,
        Normal,
        Small,
        Mini
    }

    public class BsnToggle : HtmlInputCheckBox
    {
        public BsnToggle(string id, string key, string on, string off, bool isChecked)
            : this(id, key, on, off, isChecked, string.Empty) { }

        public BsnToggle(string id, string key, string on, string off, bool isChecked, string width)
            : this (id, key, on, off, isChecked, ToggleSize.Small, width) { }

        public BsnToggle(string id, string key, string on, string off, bool isChecked, ToggleSize size)
            : this(id, key, on, off, isChecked, size, string.Empty) { }

        public BsnToggle(string id, string key, string on, string off, bool isChecked, ToggleSize size, string width)
        {
            Checked = isChecked;
            Attributes["class"] = "bsn-toggle";
            Attributes["data-toggle"] = "toggle";
            Attributes["data-id"] = id;
            Attributes["data-key"] = key;
            Attributes["data-on"] = on;
            Attributes["data-off"] = off;
            SetSize(size);
            SetWidth(width);
        }

        public void SetSize(ToggleSize size)
        {
            Attributes["data-size"] = size.ToString().ToLower();
        }

        public void SetWidth(string width)
        {
            if (string.IsNullOrEmpty(width))
                return;

            Attributes["data-width"] = width;
        }
    }
}
