﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.ViewModels;
using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common
{
    public abstract class BsnMaster : MasterPage
    {
        protected BsnMaster()
        {
            Log = LogManager.GetLogger(GetType().FullName);
            //Log.Debug("BsnMaster| constructor");
        }
        
        #region Protected Properties
        protected Logger Log { get; private set; }
        protected bool IsLoggedIn { get; set; }
        protected bool Bootstrap3 { get; set; }
        #endregion

        #region BsnInit        
        protected void BsnInit(BootstrapCallbackPanel panel)
        {
            if (!IsPostBack)
            {
                VarUtils.Set(WebVars.PageSettings, null);
                VarUtils.Set(WebVars.PageFilter, null);
                VarUtils.Set(WebVars.PagePreview, null);
                VarUtils.Set(WebVars.CallbackData, null);
                VarUtils.Set(WebVars.ModalCallback, null);
                VarUtils.Set(WebVars.ExportGrid, null);
            }

            var sidebar = (BootstrapTreeView)panel.FindControl("navTreeView");
            Bootstrap3 = (sidebar == null);

            //scripts.Add(new ScriptReference() { Name = "BsnCraft" });
            panel.CustomJSProperties += CustomJSProperties;
            panel.ClientSideEvents.Init = "bsn.checkAlerts";

            if (!BsnUtils.Connected)
            {
                //scripts.Add(new ScriptReference() { Name = "CryptoJS" });
                return;
            }
            
            IsLoggedIn = true;
            InitMenu();
            
            panel.Callback += PerformCallback;            
            panel.ClientSideEvents.BeginCallback = "bsn.beginCallback";
            panel.ClientSideEvents.EndCallback = "bsn.endCallback";
            panel.ClientInstanceName = "CallbackPanel";            
            panel.SettingsLoadingPanel.Enabled = false;            
            panel.CssClasses.Control = "content";
            
            Page.Header.Title = Utils.PageTitle;            
            //LoadSiteSettings();
                        
            var Callback = new BsnCallback(VarUtils.Str(WebVars.ModalCallback));
            bool initModal = (Callback.IsValid && Callback.Id != BsnControls.None);
            if (initModal)
            {
                if (Callback.Mode == CallbackMode.Lookup && panel.IsCallback)
                {
                    initModal = false;
                }
            }
            
            if (initModal)
            {    
                Log.Debug("BsnInit| Callback=" + Callback.ToString());
                if (!ProcessCallback(Callback, panel))
                {
                    var BsnControl = Utils.LoadBsnControl(Callback, panel);
                    var modal = Utils.ModalContent(panel);
                    if (modal != null)
                    {
                        modal.Controls.Add(BsnControl);
                    }
                }
            }

            ((BsnPage)Page).BsnInit(panel);

            if (sidebar == null)
            {
                LoadHeader(panel);
                LoadMenu(panel);
            }
            else
            {
                LoadMenu(sidebar);
            }
        }
        #endregion

        #region Login Redirect
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsLoggedIn && BsnUtils.Connected)
            {
                //LoadSiteSettings();

                string currentPage = Page.Request.Url.LocalPath.ToLower().Replace("/default.aspx", "/");
                if (!currentPage.EndsWith(".aspx") && !currentPage.EndsWith("/"))
                {
                    currentPage += "/";
                }

                var defaultPage = Utils.Settings.User.DefaultPage;
                if (string.IsNullOrEmpty(defaultPage))
                    defaultPage = Utils.Home;

                var nextPage = (currentPage == Utils.Home) ? defaultPage : Page.Request.Url.PathAndQuery;
                Response.Redirect(nextPage);
            }
        }
        #endregion

        #region Load Header
        public void LoadHeader(BootstrapCallbackPanel panel)
        {
            if (!BsnUtils.Connected)
                return;

            var callback = new BsnCallback();
            var header = new HtmlGenericControl("ul");
            header.Attributes["class"] = "nav";

            var nav = new HtmlGenericControl("li");
            var button = new BsnButton("bsn.toggleMenu();", "fal fa-bars fa-fw", false);
            button.AddAttr("class", "menu-button");
            nav.Controls.Add(button);
            header.Controls.Add(nav);

            nav = new HtmlGenericControl("li");
            nav.Attributes["class"] = "bsn hidden-xs";
            nav.Controls.Add(Utils.BsnLogo(Utils.Settings.User.Theme));
            header.Controls.Add(nav);

            var defaultPage = Utils.Settings.User.DefaultPage;
            if (string.IsNullOrEmpty(defaultPage))
                defaultPage = Utils.Home;

            nav = new HtmlGenericControl("li");
            nav.Attributes["class"] = "company";
            
            var onclick = "window.location.href = '" + Utils.ResolveUrl(defaultPage) + "';";
            button = new BsnButton(onclick, string.Empty, BsnUtils.BsnCompany, ButtonTypes.Default, false);
            var buttonlist = new BsnButtonList(button);
            
            callback.Mode = CallbackMode.DefaultPage;
            buttonlist.Add(new BsnButton(callback.ToCallback(), "fal fa-home fa-fw", "Set Home Page", ButtonTypes.Link, false));
            
            callback.Mode = CallbackMode.Logout;
            buttonlist.Add(new BsnButton(callback.ToCallback(), "fal fa-power-off fa-fw", "Log Out", ButtonTypes.Link, false));
            
            nav.Controls.Add(buttonlist);
            header.Controls.Add(nav);

            if (Utils.SettingsButton.Count > 0)
            {
                nav = new HtmlGenericControl("li");
                nav.Attributes["class"] = "dropdown";
                button = new BsnButton(string.Empty, "fal fa-cog fa-fw", false);
                button.AddAttr("class", "dropdown-toggle");
                button.AddAttr("data-toggle", "dropdown");
                button.AddAttr("role", "button");
                button.AddAttr("aria-expanded", "false");
                nav.Controls.Add(button);

                var menu = new HtmlGenericControl("ul");
                menu.Attributes["class"] = "dropdown-menu";
                menu.Attributes["role"] = "menu";

                foreach (var setting in Utils.SettingsButton)
                {
                    var list = new HtmlGenericControl("li");
                    list.Controls.Add(setting);
                    menu.Controls.Add(list);
                }

                nav.Controls.Add(menu);
                header.Controls.Add(nav);
            }
            
            nav = new HtmlGenericControl("li");
            nav.Attributes["class"] = "dropdown";
            button = new BsnButton(string.Empty, "fal fa-paint-brush fa-fw", false);
            button.AddAttr("class", "dropdown-toggle");
            button.AddAttr("data-toggle", "dropdown");
            button.AddAttr("role", "button");
            button.AddAttr("aria-expanded", "false");
            nav.Controls.Add(button);
            nav.Controls.Add(LoadThemes());
            header.Controls.Add(nav);

            nav = new HtmlGenericControl("li");
            callback.Mode = CallbackMode.Documentation;
            callback.Value = Utils.HelpFolder + "Product Guide.pdf";
            button = new BsnButton(callback.ToCallback(), "fal fa-question fa-fw", false);
            nav.Controls.Add(button);
            header.Controls.Add(nav);

            nav = new HtmlGenericControl("li");
            callback.Mode = CallbackMode.RefreshCache;
            callback.Value = string.Empty;
            button = new BsnButton(callback.ToCallback(), "fal fa-sync fa-fw bsn-refresh", false);
            nav.Controls.Add(button);
            header.Controls.Add(nav);

            var navbar = new HtmlGenericControl("div");
            navbar.Attributes["class"] = "navbar navbar-default navbar-fixed-top";
            //navbar.Attributes["class"] = "navbar fixed-top";
            navbar.Controls.Add(header);

            panel.Controls.Add(navbar);
        }
        #endregion
        
        #region Load Themes
        protected HtmlGenericControl LoadThemes()
        {
            var menu = new HtmlGenericControl("ul");
            menu.Attributes["class"] = "dropdown-menu dropdown-menu-right themes-menu";
            menu.Attributes["role"] = "menu";
            
            var themes = (List<string>)VarUtils.Get(WebVars.Themes);
            if (themes == null)
            {
                themes = new List<string>
                {
                    "default"
                };

                string basepath = ResolveUrl("~/Content/css/themes/");
                string themePath = HttpContext.Current.Server.MapPath(basepath);
                string[] folders = System.IO.Directory.GetDirectories(themePath);
                foreach (var folder in folders)
                {
                    var theme = System.IO.Path.GetFileName(folder).ToLower();
                    if (theme == "fonts")
                    {
                        continue;
                    }
                    themes.Add(theme);
                }
                VarUtils.Set(WebVars.Themes, themes);
            }

            var callback = new BsnCallback()
            {
                Mode = CallbackMode.SetTheme
            };

            foreach (var theme in themes)
            {
                TextInfo textInfo = new CultureInfo("en-AU", false).TextInfo;
                var caption = textInfo.ToTitleCase(theme);
                var nav = new HtmlGenericControl("li");
                callback.Value = theme;
                var button = new BsnButton(callback, theme, caption, ButtonTypes.Link, false);
                if (theme == Utils.Settings.User.Theme)
                    nav.Attributes["class"] = "active";
                nav.Controls.Add(button);
                menu.Controls.Add(nav);
            }

            return menu;
        }
        #endregion

        #region Init Menu
        protected virtual void InitMenu()
        {
            //website specific menu
            //Todo: load from settings based on security

            var mainMenu = (List<WebMenu>)VarUtils.Get(WebVars.WebMenu);
            if (mainMenu != null)
                return;

            if (!BsnUtils.Connected)
                return;

            mainMenu = new List<WebMenu>();
            WebMenu child;
            var parent = new WebMenu(Menus.Home, "Desktop", "~/", "fal fa-desktop-alt");
            mainMenu.Add(parent);

            parent = new WebMenu(Menus.Customers, "Customers", "~/Customers/", "fal fa-user-circle");
            mainMenu.Add(parent);
            child = new WebMenu(Menus.Customers_Details, "Details", "~/Customers/Details.aspx", "fal fa-folder-open")
            {
                Parent = parent,
                Visible = false
            };
            mainMenu.Add(child);

            if (Utils.ShowActivities)
            {
                parent = new WebMenu(Menus.Activities, "Activities", "~/Activities/", "fal fal fa-th-list");
                mainMenu.Add(parent);                
            }

            if (Utils.ShowLeads)
            {
                parent = new WebMenu(Menus.Leads, "Leads", "~/Leads/", "fal fa-filter");
                mainMenu.Add(parent);
                child = new WebMenu(Menus.Leads_Details, "Details", "~/Leads/Details.aspx", "fal fa-folder-open")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
                child = new WebMenu(Menus.Leads_Checklist, "Checklist", "~/Leads/Checklist.aspx", "fal fa-folder-open")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
                child = new WebMenu(Menus.Leads_Estimate, "Estimate", "~/Leads/Estimate.aspx", "fal fa-sitemap")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
                child = new WebMenu(Menus.Leads_Template, "House Template", "~/Leads/Template.aspx", "fal fa-shopping-bag")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
                child = new WebMenu(Menus.Leads_Variation, "Variation", "~/Leads/Variation.aspx", "fal fa-exchange")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
            }

            if (Utils.ShowContracts)
            {
                parent = new WebMenu(Menus.Contracts, "Contracts", "~/Contracts/", "fal fa-handshake");
                mainMenu.Add(parent);
                child = new WebMenu(Menus.Contracts_Details, "Details", "~/Contracts/Details.aspx", "fal fa-folder-open")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
                child = new WebMenu(Menus.Contracts_Checklist, "Checklist", "~/Contracts/Checklist.aspx", "fal fa-folder-open")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
                child = new WebMenu(Menus.Contracts_Estimate, "Estimate", "~/Contracts/Estimate.aspx", "fal fa-sitemap")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
                child = new WebMenu(Menus.Contracts_Template, "House Template", "~/Contracts/Template.aspx", "fal fa-shopping-bag")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
                child = new WebMenu(Menus.Contracts_Variation, "Variation", "~/Contracts/Variation.aspx", "fal fa-exchange")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
            }

            if (Utils.ShowJobs)
            {
                parent = new WebMenu(Menus.Jobs, "Jobs", "~/Jobs/", "fal fal fa-gavel");
                mainMenu.Add(parent);
                child = new WebMenu(Menus.Jobs_Details, "Details", "~/Jobs/Details.aspx", "fal fa-folder-open")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
            }

            if (Utils.ShowDisplays)
            {
                parent = new WebMenu(Menus.DisplayCentres, "Displays", "~/DisplayCentres/", "fal fa-map-signs");
                mainMenu.Add(parent);

                child = new WebMenu(Menus.DisplayCentres_Details, "Details", "~/DisplayCentres/Details.aspx", "fal fa-folder-open")
                {
                    Parent = parent,
                    Visible = false
                };
                mainMenu.Add(child);
            }


            /*
            if (Utils.ShowFinance)
            {
                parent = new WebMenu(Menus.Finance, "Finance", "~/Finance/", "fal fa-dollar-sign");
                mainMenu.Add(parent);
            }
            */

            if (Utils.ShowDocuments)
            {
                parent = new WebMenu(Menus.Documents, "Documents", "~/Documents/", "fal fa-file-alt");
                mainMenu.Add(parent);
            }

            if (Utils.ShowCharts)
            {
                parent = new WebMenu(Menus.Charts, "Charts", "~/Charts/", "fal fa-chart-pie");
                mainMenu.Add(parent);
            }

            if (Utils.ShowReports)
            {
                parent = new WebMenu(Menus.Reports, "Reports", "~/Reports/", "fal fa-file-alt");
                mainMenu.Add(parent);
            }

            if (Utils.ShowDashboards)
            {
                parent = new WebMenu(Menus.Dashboards, "Dashboards", "~/Dashboards/", "fal fa-tachometer-alt");
                mainMenu.Add(parent);
            }

            if (BsnUtils.AllContracts)
            {
                parent = new WebMenu(Menus.Admin, "Administration", "~/Admin/", "fal fa-lock");
                mainMenu.Add(parent);
            }

            //Utils
            var thumbnail = new WebMenu(Menus.Shared_Thumbnail, "", "~/Shared/Thumbnail.aspx", "") { Visible = false };
            mainMenu.Add(thumbnail);
            var shared_search = new WebMenu(Menus.Shared_Search, "", "~/Shared/Search.aspx", "") { Visible = false };
            mainMenu.Add(shared_search);
            var charts_getdata = new WebMenu(Menus.Charts_GetData, "", "~/Charts/GetData.aspx", "") { Visible = false };
            mainMenu.Add(charts_getdata);

            VarUtils.Set(WebVars.WebMenu, mainMenu);
        }
        #endregion

        #region Load Menu
        public void LoadMenu(BootstrapTreeView sidebar)
        {
            sidebar.Nodes.Clear();
            foreach (var menu in Utils.MainMenu.Where(p => p.Visible))
            {
                bool hideMenu = false;
                int count = 0;
                switch (menu.Menu)
                {
                    case Menus.Customers:
                        count = new CustomersViewModel().Customers.Count();
                        break;
                    case Menus.Activities:
                        hideMenu = !Utils.ShowActivities;
                        if (!hideMenu)
                            count = new ActivitiesViewModel().GetActivity.Count;
                        break;
                    case Menus.Finance:
                        hideMenu = !Utils.ShowFinance;
                        break;
                    case Menus.Documents:
                        hideMenu = !Utils.ShowDocuments;
                        break;
                    case Menus.Leads:
                        hideMenu = !Utils.ShowLeads;
                        if (!hideMenu)
                            count = new LeadsViewModel().Leads.Count();
                        break;
                    case Menus.Contracts:
                        hideMenu = !Utils.ShowContracts;
                        if (!hideMenu)
                            count = new ContractsViewModel().Contracts.Count();
                        break;
                    case Menus.Jobs:
                        hideMenu = !Utils.ShowJobs;
                        if (!hideMenu)
                            count = new JobCostViewModel().Jobs.Count();
                        break;
                    case Menus.DisplayCentres:
                        hideMenu = !Utils.ShowDisplays;
                        if (!hideMenu)
                            count = new DisplayCentresViewModel().DisplayCentres.Count();
                        break;
                    case Menus.Charts:
                        hideMenu = !Utils.ShowCharts;
                        if (!hideMenu)
                            count = new ChartsViewModel().Charts.Count;
                        break;
                    case Menus.Dashboards:
                        hideMenu = !Utils.ShowDashboards;
                        if (!hideMenu)
                            count = new DashboardsViewModel().Dashboards.Count;
                        break;
                    case Menus.Reports:
                        hideMenu = !Utils.ShowReports;
                        if (!hideMenu)
                            count = new ReportsViewModel().Reports.Count;
                        break;
                }
                if (!hideMenu)
                {
                    var item = sidebar.Nodes.Add(menu.Caption, menu.Menu.ToString(), menu.Icon, menu.ResolveUrl);
                    if (count > 0)
                        item.Badge.Text = count.ToString();                    
                }
            }

            sidebar.CollapseAll();
            sidebar.SelectedNode = sidebar.Nodes.FindAllRecursive(n => ResolveUrl(n.NavigateUrl).Equals(Request.Url.LocalPath, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (sidebar.SelectedNode != null && sidebar.SelectedNode.Parent != null)
            {
                sidebar.SelectedNode.Parent.CssClass = "active-parent";
                sidebar.SelectedNode.Parent.Expanded = true;
            }
        }

        public void LoadMenu(BootstrapCallbackPanel panel)
        {
            if (!BsnUtils.Connected)
                return;
            
            var list = new HtmlGenericControl("ul");
            list.Attributes["class"] = "nav nav-pills nav-stacked nav-main";

            string url = Request.Url.LocalPath.ToLower().Replace("default.aspx", "");
            if (!url.EndsWith(".aspx") && !url.EndsWith("/"))
            {
                url += "/";
            }

            foreach (var menu in Utils.MainMenu.Where(p => p.Visible))
            {
                menu.IsActive = false;
            }

            foreach (var menu in Utils.MainMenu.Where(p => !p.Visible))
            {
                if (menu.Parent != null && (menu.ResolveUrl.ToLower() == url))
                {
                    menu.Parent.IsActive = true;
                    break;
                }
            }

            foreach (var menu in Utils.MainMenu.Where(p => p.Visible))
            {
                var nav = new HtmlGenericControl("li");
                if (menu.ResolveUrl.ToLower() == url || menu.IsActive)
                {
                    nav.Attributes["class"] = "active";
                }

                bool hideMenu = false;
                int count = 0;
                switch (menu.Menu)
                {
                    case Menus.Customers:
                        count = new CustomersViewModel().Customers.Count();
                        break;
                    case Menus.Activities:
                        hideMenu = !Utils.ShowActivities;
                        if (!hideMenu)
                            count = new ActivitiesViewModel().GetActivity.Count;                        
                        break;
                    case Menus.Leads:
                        hideMenu = !Utils.ShowLeads;
                        if (!hideMenu)
                            count = new LeadsViewModel().Leads.Count();
                        break;
                    case Menus.Contracts:
                        hideMenu = !Utils.ShowContracts;
                        if (!hideMenu)
                            count = new ContractsViewModel().Contracts.Count();                        
                        break;
                    case Menus.Jobs:
                        hideMenu = !Utils.ShowJobs;
                        if (!hideMenu)
                            count = new JobCostViewModel().Jobs.Count();                        
                        break;
                    case Menus.DisplayCentres:
                        hideMenu = !Utils.ShowDisplays;
                        if (!hideMenu)
                            count = new DisplayCentresViewModel().DisplayCentres.Count();
                        break;
                    case Menus.Charts:
                        hideMenu = !Utils.ShowCharts;
                        if (!hideMenu)
                            count = new ChartsViewModel().Charts.Count;
                        break;
                    case Menus.Dashboards:
                        hideMenu = !Utils.ShowDashboards;
                        if (!hideMenu)
                            count = new DashboardsViewModel().Dashboards.Count;
                        break;
                    case Menus.Reports:
                        hideMenu = !Utils.ShowReports;
                        if (!hideMenu)
                            count = new ReportsViewModel().Reports.Count;
                        break;
                }
                if (!hideMenu)
                {
                    menu.SetCount(count);
                    var bsnButton = menu.Button;
                    nav.Controls.Add(bsnButton);
                    list.Controls.Add(nav);
                }
            }

            var sidebar = new HtmlGenericControl("div");
            sidebar.Attributes["class"] = "sidebar";
            sidebar.Controls.Add(list);

            panel.Controls.Add(sidebar);
        }
        #endregion
                
        #region Panel Callback        
        protected virtual void PerformCallback(object sender, CallbackEventArgsBase e)
        {
            var panel = (BootstrapCallbackPanel)sender;
            var Callback = new BsnCallback(e.Parameter);

            var sidebar = (BootstrapTreeView)panel.FindControl("navTreeView");
            Bootstrap3 = (sidebar == null);

            Log.Debug("Callback: " + Callback.ToString());
            if (Callback.IsValid)
            {
                if (Callback.Mode != CallbackMode.PageSetting)
                {
                    //Experimental, used for getting PageSetting for Edit Popups
                    ((BsnPage)Page).LoadPageSettings();
                }
                if (!ProcessCallback(Callback, panel))
                {
                    var BsnControl = Utils.LoadBsnControl(Callback, panel);
                    if (BsnControl != null)
                    {
                        Utils.ProcessModal(BsnControl, Callback, panel);
                    }
                }
            }

            if (!string.IsNullOrEmpty(VarUtils.Str(WebVars.RefreshMenu)))
            {
                VarUtils.Set(WebVars.RefreshMenu, null);
                VarUtils.Set(WebVars.WebMenu, null);                
            }

            InitMenu();

            if (Callback.Mode != CallbackMode.Logout)
            {
                ((BsnPage)Page).PerformCallback(panel, e);
            }

            if (sidebar == null)
            {
                LoadHeader(panel);
                LoadMenu(panel);
            }
            else
            {
                LoadMenu(sidebar);
            }
        }
        #endregion
        
        #region CustomJSProperties        
        protected virtual void CustomJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            Utils.JSProperties(e);   
        }
        #endregion

        #region ProcessCallback
        protected virtual bool ProcessCallback(BsnCallback Callback, BootstrapCallbackPanel panel)
        {
            if (Utils.ProcessCallback(Callback, panel))
            {
                return true;
            }

            switch (Callback.Mode)
            {
                /*
                case CallbackMode.Login:
                    var username = Callback.GetAttr("username");
                    var salt = Callback.GetAttr("time");
                    var password = BsnUtils.Decrypt(Callback.GetAttr("password"), salt);
                    var company = Callback.GetAttr("company");
                    if (BsnUtils.BsnLogin(username.ToUpper(), password.ToUpper(), company, ""))
                    {
                        string currentPage = Page.Request.Url.LocalPath.ToLower().Replace("/default.aspx", "/");
                        if (!currentPage.EndsWith(".aspx") && !currentPage.EndsWith("/"))
                        {
                            currentPage += "/";
                        }

                        var defaultPage = StringSetting(UserSettings.DefaultPage);
                        if (string.IsNullOrEmpty(defaultPage))
                            defaultPage = Utils.Home;

                        var nextPage = (currentPage == Utils.Home) ? defaultPage : Page.Request.Url.PathAndQuery;
                        ASPxWebControl.RedirectOnCallback(nextPage);
                    }
                    break;
                */
                case CallbackMode.Logout:
                    BsnUtils.BsnLogout();
                    ASPxWebControl.RedirectOnCallback(Page.Request.Url.PathAndQuery);
                    break;
                case CallbackMode.SetTheme:
                    Utils.Settings.User.Theme = Callback.Value;
                    Utils.SaveSettings();
                    ASPxWebControl.RedirectOnCallback(Page.Request.Url.PathAndQuery);
                    break;
                case CallbackMode.DefaultPage:
                    string baseurl = ResolveUrl("~/");
                    string url = Page.Request.Url.PathAndQuery;                    
                    url = "~/" + url.Substring(baseurl.Length);
                    Utils.Settings.User.DefaultPage = Callback.Value;
                    Utils.SaveSettings();
                    Utils.SetSuccess("Home Page: " + url);
                    break;
                default:
                    return false;
            }
            return true;
        }
        #endregion

        #region Old Site Settings

        /*
        #region SetSiteSettings
        protected void SetSiteSettings(string settings)
        {
            if (!BsnUtils.Connected)
                return;

            VarUtils.Set(WebVars.SiteSettings, settings);
            BsnUtils.SaveSettings(SettingsLevel.User, SettingsKey, settings);            
        }
        #endregion

        #region GetSiteSettings
        protected string GetSiteSettings()
        {
            if (!BsnUtils.Connected)
                return string.Empty;

            string settings = VarUtils.Str(WebVars.SiteSettings);
            if (!string.IsNullOrEmpty(settings))
            {
                return settings;
            }
            settings = BsnUtils.GetSettings(SettingsLevel.User, SettingsKey);
            if (!string.IsNullOrEmpty(settings))
            {
                VarUtils.Set(WebVars.SiteSettings, settings);
            }
            return settings;
        }
        #endregion

        #region LoadSiteSettings        
        public virtual void LoadSiteSettings()
        {
            if (!BsnUtils.Connected)
                return;

            string settings = GetSiteSettings();
            if (string.IsNullOrEmpty(settings))
                return;

            try
            {
                Settings = (Dictionary<string, string>)JsonConvert.DeserializeObject(settings, typeof(Dictionary<string, string>));
            }
            catch (Exception e)
            {
                Utils.LogExc(e);
            }
        }
        #endregion

        #region SaveSiteSettings
        protected virtual void SaveSiteSettings(string setting, string value)
        {
            if (!BsnUtils.Connected)
                return;

            //Ensure we have the latest
            LoadSiteSettings();

            //Set new value
            Settings[setting] = value;

            //Save
            var settings = JsonConvert.SerializeObject(Settings);
            SetSiteSettings(settings);
        }
        #endregion

        #region StringSetting
        public virtual string StringSetting(UserSettings setting)
        {
            string result = string.Empty;
            if (!Settings.Comp.ContainsKey(setting.ToString()))
            {
                if (setting == UserSettings.Theme)
                {
                    result = "default";
                }
                return result;
            }                
            
            return Settings[setting.ToString()];
        }
        #endregion
        */

        #endregion
    }
}
