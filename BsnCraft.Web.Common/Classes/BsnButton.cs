﻿using BsnCraft.Web.Common.Models;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Classes
{
    public enum ButtonTypes
    {
        None,
        Default,
        Link,
        Primary,
        Info,
        Success,
        Warning,
        Danger,
        Label,
        EventRegister
    }
    
    public class BsnButton : HtmlGenericControl
    {
        #region Constructors
        public BsnButton() { }

        public BsnButton(string onclick, string icon) 
            : this (onclick, icon, true) { }

        public BsnButton(string onclick, string icon, bool isButton)
        {
            SetupButton(onclick, icon, string.Empty, ButtonTypes.Link, isButton);            
        }
        
        public BsnButton(BsnCallback Callback, string icon) 
            : this (Callback, icon, true) { }

        public BsnButton(BsnCallback Callback, string icon, bool isButton)
        {
            SetupButton(Callback.ToCallback(), icon, string.Empty, ButtonTypes.Link, isButton);
        }

        public BsnButton(string onclick, string icon, string text, ButtonTypes type)
            : this (onclick, icon, text, type, true) { }

        public BsnButton(string onclick, string icon, string text, ButtonTypes type, bool isButton)
        {
            SetupButton(onclick, icon, text, type, isButton);
        }

        public BsnButton(BsnCallback Callback, string icon, string text, ButtonTypes type)
            : this (Callback, icon, text, type, true) { }

        public BsnButton(BsnCallback Callback, string icon, string text, ButtonTypes type, bool isButton)
        {
            SetupButton(Callback.ToCallback(), icon, text, type, isButton);
        }
        #endregion

        #region Setup Button
        public void SetupButton(BsnCallback Callback, string icon)
        {
            SetupButton(Callback.ToCallback(), icon);
        }

        public void SetupButton(string onclick, string icon)
        {
            SetupButton(onclick, icon, "", ButtonTypes.Link);
        }

        public void SetupButton(string onclick, string icon, string text, ButtonTypes type)
        {
            SetupButton(onclick, icon, text, type, true);
        }

        public bool IsButton { get; set; }
        public void SetupButton(string onclick, string icon, string text, ButtonTypes type, bool isButton)
        {
            IsButton = isButton;            
            if (IsButton)
            {
                TagName = "button";                
                Attributes["type"] = "button";
            }
            else
            {
                TagName = "a";
                Attributes["href"] = "javascript:void(0);";
            }

            Attributes["class"] = "btn";

            if (!string.IsNullOrEmpty(onclick))
            {
                if (onclick.StartsWith("href:"))
                {
                    Attributes["href"] = onclick.Substring(5);
                }
                else
                {
                    Attributes["onclick"] = onclick;
                }
            }

            SetIcon(icon);
            SetText(text);

            switch (type)
            {
                case ButtonTypes.None:
                    Attributes["class"] = "";
                    break;
                case ButtonTypes.Default:
                    AddAttr("class", "btn-default");
                    break;
                case ButtonTypes.Link:
                    AddAttr("class", "btn-link");
                    break;
                case ButtonTypes.Primary:
                    AddAttr("class", "btn-primary");
                    break;
                case ButtonTypes.Danger:
                    AddAttr("class", "btn-danger");
                    break;
                case ButtonTypes.Info:
                    AddAttr("class", "btn-info");
                    break;
                case ButtonTypes.Warning:
                    AddAttr("class", "btn-warning");
                    break;
                case ButtonTypes.Success:
                    AddAttr("class", "btn-success");
                    break;
                case ButtonTypes.Label:
                    AddAttr("class", "btn-link btn-label");
                    break;
            }
        }
        #endregion

        #region Set Icon
        HtmlGenericControl icon;
        public void SetIcon(string classname)
        {
            if (string.IsNullOrEmpty(classname))
                return;

            icon = new HtmlGenericControl("span");
            icon.Attributes["class"] = "image " + classname;
            icon.Attributes["aria-hidden"] = "true";
            Controls.Add(icon);
        }
        #endregion

        #region Set Text
        public void SetText(string text)
        {
            if (text.ToLower() == "no-btn-lg")
            {
                return;
            }

            if (string.IsNullOrEmpty(text))
            {
                Attributes["class"] += " btn-lg";
                return;
            }

            if (icon != null)
            {
                var span = new HtmlGenericControl("span");
                span.Attributes["style"] = "padding-left: 0.4em";
                if (IsButton)
                {
                    span.Attributes["class"] = "hidden-xs";
                }
                span.InnerText = text;
                Controls.Add(span);
                return;
            }
            
            Controls.Add(new LiteralControl(text));
        }
        #endregion

        #region Add Text
        public void AddText(string text, string className)
        {
            var span = new HtmlGenericControl("span");
            if (!string.IsNullOrEmpty(className))
            {
                span.Attributes["class"] = className;
            }
            span.Controls.Add(new LiteralControl(text));
            Controls.Add(span);
        }
        #endregion

        public void SetCallback(BsnCallback Callback)
        {
            Attributes["onclick"] = Callback.ToCallback();
        }

        public void AddAttr(string attr, string value)
        {
            if (!string.IsNullOrEmpty(Attributes[attr]))
            {
                value = " " + value;
            }
            Attributes[attr] += value;
        }

        public void RemoveAttr(string attr, string value)
        {
            var oldval = Attributes[attr];
            var newval = oldval.Replace(value, "");
            Attributes[attr] = newval;
        }

    }
}
