﻿using System.ComponentModel;

namespace BsnCraft.Web.Common.Models
{
    public class InvoicingSettings
    {
        [Description("IVC_INTCO")]
        public bool InterfaceContracts { get; set; }
    }
}
