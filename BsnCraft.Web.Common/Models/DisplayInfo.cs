﻿using BsnCraft.Web.Common.Classes;
using System;
using System.Collections.Generic;

namespace BsnCraft.Web.Common.Models
{
    public class DisplayInfo
    {        
        public string Key { get; set; }
        public string UpdateKey { get; set; }
        public DateTime Day { get; set; }
        public string SalesCentre { get; set; }
        public string Code { get; set; }
        public string Prompt { get; set; }

        /*
            A = Alphanumeric Text (Uppercase Only)
            L = Alphanumeric Text (Lowercase Allowed)
            D = Date
            T = Time
            N = Numeric (Positive or Negative Allowed)
            P = Numeric (Positive only)
            Q = Question (Yes/No only)
            * = Comment Field Only 
        */

        public string Type { get; set; }
        public string Min { get; set; }
        public string Max { get; set; }
        public string Dec { get; set; }
        public string Default { get; set; }
        public List<ComboItem> Answers { get; set; }
        public bool HasAnswers
        {
            get
            {
                return (Answers != null && Answers.Count > 0);
            }
        }
        public string AnsCode { get; set; }
        public string Answer { get; set; }
        public bool Modified { get; set; }
        public string JulianDay
        {
            get
            {
                return Day.Year.ToString() + Day.DayOfYear.ToString("000");
            }
        }

        public string StringDay
        {
            get
            {
                return Day.ToString("D");
            }
        }
    }
}
