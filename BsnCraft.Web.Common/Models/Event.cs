﻿using BsnCraft.Web.Common.Classes;
using System;

namespace BsnCraft.Web.Common.Models
{
    public class Event
    {
        #region Public Properties
        public Contract Contract { get; private set; }
        public string Key { get; set; } = "";
        public string Code { get; set; } = "";
        public string Description { get; set; } = "";
        public string CatgCode { get; set; } = "";
        public string EmpCode { get; set; } = "";
        public string Employee { get; set; } = "";
        public string Reference { get; set; } = "";
        public string Amount { get; set; } = "";
        public string PctComplete { get; set; } = "";
        public string GstCode { get; set; } = "";
        public DateTime Actual { get; set; }
        public DateTime Forecast { get; set; }
        public DateTime Due { get; set; }
        public DateTime Registered { get; set; }
        public string Parent { get; set; } = "";
        public string Notes { get; set; } = "";
        #endregion

        #region Constructor
        public Event(Contract contract)
        {
            Contract = contract;
        }
        #endregion
        
        #region Extra Properties
        public bool IsRegistered
        {
            get
            {
                return (Registered != DateTime.MinValue);
            }
        }

        public bool IsLate
        {
            get
            {
                if (IsRegistered)
                {
                    return (Due != DateTime.MinValue && Actual > Due);
                }
                else
                {
                    return (Due != DateTime.MinValue && Forecast > Due);
                }
            }
        }

        public string CodeAndDescription
        {
            get
            {
                return Code + " " + Description;
            }
        }
        #endregion
    }
}
