﻿using BsnCraft.Web.Common.Classes;
using System.Collections.Generic;

namespace BsnCraft.Web.Common.Models
{
    public class JobInfo
    {
        #region JobInfo
        public JobInfo(Contract contract)
        {
            Contract = contract;
        }
        #endregion

        #region Public Properties
        public Contract Contract { get; set; }
        public string Key { get; set; } = "";
        public string Group { get; set; } = "";
        public string Category { get; set; } = "";
        public string CategoryDesc { get; set; } = "";
        public string CategorySort { get; set; } = "";
        public string QuestionSort { get; set; } = "";
        public string UpdateKey { get; set; } = "";
        public string Code { get; set; } = "";
        public string Prompt { get; set; } = "";
        public string Type { get; set; } = "";
        public string Min { get; set; } = "";
        public string Max { get; set; } = "";
        public string Dec { get; set; } = "";
        public string Default { get; set; } = "";
        public string LinkPrefix { get; set; } = "";
        public string LinkTable { get; set; } = "";
        public List<ComboItem> Answers { get; set; }
        public string Answer { get; set; } = "";
        public string AnsDesc { get; set; } = "";
        public bool Modified { get; set; }
        #endregion

        #region Extra Properties
        public bool HasAnswers
        {
            get
            {
                return (Answers != null && Answers.Count > 0);
            }
        }

        public string CategoryKey
        {
            get
            {
                return CategorySort + ":" + Category + ":" + CategoryDesc;
            }
        }
        #endregion
    }
}
