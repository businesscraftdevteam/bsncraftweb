﻿using BsnCraft.Web.Common.Classes;
using System;
using System.IO;

namespace BsnCraft.Web.Common.Models
{    
    public class Document
    {
        public Document(Contract contract)
        {
            Contract = contract;
        }

        public Contract Contract { get; private set; }
        public string Key { get; set; } = "";        
        public string Module { get; set; } = "CO";
        public string Type { get; set; } = "";
        public string Sequence { get; set; } = "";
        public string Reference { get; set; } = "";
        public string Description { get; set; } = "";
        public string Date { get; set; } = "";
        public string Filename { get; set; } = "";
        public string UploadFile { get; set; } = "";
        public string ReportParameters { get; set; } = "";
        public string XRefSource { get; set; } = "";
        public string XRefKey { get; set; } = "";
        public string Notes { get; set; } = "";
        public bool Locked { get; set; }
        public int SubmitStatus { get; set; } = 0;
        public int Rating { get; set; } = -1;

        #region FileOnly
        public string FileOnly
        {
            get
            {
                try
                {
                    return Path.GetFileName(Filename);
                }
                catch (Exception e)
                {
                    Helpers.LogExc(e);
                }
                                
                //TODO: Handle Variables?
                return string.Empty;
            }
        }
        #endregion

        #region Deal Key
        public string DealKey
        {
            get
            {
                return Type + ":" + Reference;
            }
        }
        #endregion
    }
}
