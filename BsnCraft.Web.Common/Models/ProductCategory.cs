﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsnCraft.Web.Common.Models
{
    public class ProductCategory
    {
        public string Code { get; set; } = "";
        public string Description { get; set; } = "";
        public string ProductGroup { get; set; } = "";
    }
}
