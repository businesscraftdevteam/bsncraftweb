﻿using System.ComponentModel;

namespace BsnCraft.Web.Common.Models
{
    public class PostCode
    {
        [Description("PC_KEY")]
        public string Key { get; set; }

        [Description("PC_STATE")]
        public string State { get; set; }

        [Description("PC_SUBURB")]
        public string Suburb { get; set; }
    }
}