﻿using System;
using System.ComponentModel;

namespace BsnCraft.Web.Common.Models
{
    public class AreaPriceSelection
    {
        public string Area { get; set; }
        public DateTime EffectiveDate { get; set; }
        public Boolean IncludeMissing { get; set; }
        public Decimal Progress { get; set; }
    }
}