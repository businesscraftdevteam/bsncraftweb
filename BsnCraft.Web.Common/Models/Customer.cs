﻿
using System;
using System.Globalization;
using System.Linq;

namespace BsnCraft.Web.Common.Models
{

    public class Customer
    {
        public string CustomerNumber { get; set; } = "";
        public string Name { get; set; } = "";
        public string Address1 { get; set; } = "";
        public string Address2 { get; set; } = "";
        public string Address3 { get; set; } = "";
        public string Address4 { get; set; } = "";
        public string Suburb { get; set; } = "";
        public string State { get; set; } = "";
        public string PostCode { get; set; } = "";
        public string Phone { get; set; } = "";
        public string Email { get; set; } = "";

        #region FriendlyName        
        public string FriendlyName
        {
            get
            {
                string name = Name.ToLower();
                string[] names = name.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                string[] unique = names.Distinct().ToArray();
                name = string.Join(", ", unique);
                TextInfo textInfo = new CultureInfo("en-AU", false).TextInfo;
                name = textInfo.ToTitleCase(name);
                return name;
            }
        }
        #endregion
        
        #region Full Address
        public string FullAddress
        {
            get
            {
                string address = Address1 + " " + Address2 + " " + Address3 + " " + Address4;
                address += " " + Suburb + " " + State + " " + PostCode;
                return address;
            }
        }
        #endregion
    }
}