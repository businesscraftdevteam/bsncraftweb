﻿using BsnCraft.Web.Common.Classes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace BsnCraft.Web.Common.Models
{

    public class Contract
    {
        #region Constructor
        public readonly BsnApplication App;
        public Contract(BsnApplication app)
        {
            App = app;
        }
        #endregion

        #region Public Properties
        public string ContractNumber { get; set; } = "";
        public string Key { get { return ContractNumber; } }
        public string CustomerNumber { get; set; } = "";
        public string Name { get; set; } = "";
        public string LotNumber { get; set; } = "";
        public string StreetNumber { get; set; } = "";
        public string Address { get; set; } = "";
        public string City { get; set; } = "";
        public string State { get; set; } = "";
        public string PostCode { get; set; } = "";
        public string Status { get; set; } = "";
        public string SaleType { get; set; } = "";
        public string SalesConsultant { get; set; } = "";
        public string CustomerService { get; set; } = "";
        public string BuildingSupervisor { get; set; } = "";
        public string District { get; set; } = "";
        public string Council { get; set; } = "";
        public string SalesPromotion { get; set; } = "";
        public string SalesCentre { get; set; } = "";
        public string Comment1 { get; set; } = "";
        public string Comment2 { get; set; } = "";
        public string Comment3 { get; set; } = "";
        public string House { get; set; } = "";
        public string Facade { get; set; } = "";
        public string SubjectSale { get; set; } = "";
        public string SavingsPlan { get; set; } = "";
        public string SaleGroup { get; set; } = "";
        public string OpCentre { get; set; } = "";
        public string DpNumber { get; set; } = "";
        public string Volume { get; set; } = "";
        public string Folio { get; set; } = "";
        public string JobNumber { get; set; } = "";
        public bool IsLead { get; set; }
        #endregion

        #region RemoveDetail
        public void RemoveDetail(EstimateDetail detail)
        {
            var p = detail.Paragraph;
            var h = detail.Paragraph.Heading;
            EstimateDetails(h).Remove(detail);
            App.VM.JC.ReloadParagraph(p);
            App.VM.JC.ReloadHeading(h);
            detail = null;
        }
        #endregion
        
        #region RecalcHeadings
        public void RecalcHeadings(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                headings.Clear();
                if (paragraphs != null)
                {
                    paragraphs.Clear();
                }                
            }
            var heading = headings.Where(p => p.Key.Equals(key)).FirstOrDefault();
            if (heading == null)
            {
                return;
            }
            if (paragraphs != null && paragraphs.Any(p => p.Key.Equals(heading)))
            {
                paragraphs.Remove(heading);
            }
            if (estimateDetails != null && estimateDetails.Any(p => p.Key.Equals(heading)))
            {
                estimateDetails.Remove(heading);
            }            
        }
        #endregion

        #region JobInfo
        private List<JobInfo> jobInformation;
        public List<JobInfo> JobInformation
        {
            get
            {
                if (jobInformation == null || jobInformation.Count == 0)
                {
                    jobInformation = App.VM.JI.GetJobInfo(this);
                }
                return jobInformation;
            }
        }
        #endregion

        #region JobInfoGroups
        private Dictionary<string, string[]> jobInfoGroups;
        public Dictionary<string, string[]> JobInfoGroups
        {
            get
            {
                if (jobInfoGroups == null || jobInfoGroups.Count == 0)
                {
                    jobInfoGroups = App.VM.JI.GetJobInfoGroups(this);
                }
                return jobInfoGroups;
            }
        }
        #endregion

        #region Find JobInfo
        public JobInfo FindJobInfo(string key)
        {
            return JobInformation.Where(p => p.Key.Equals(key)).FirstOrDefault();
        }
        #endregion

        #region Important Documents
        private List<Document> importantDocuments;
        public List<Document> ImportantDocuments
        {
            get
            {
                if (importantDocuments == null)
                {
                    importantDocuments = App.VM.CO.GetImportant(this);
                }
                return importantDocuments;
            }
        }
        #endregion

        #region Documents
        private List<Document> documents;
        public List<Document> Documents
        {
            get
            {
                if (documents == null || documents.Count == 0)
                {
                    documents = App.VM.CO.GetDocuments(this);
                    foreach (var doc in App.VM.JC.GetDocuments(this))
                    {
                        documents.Add(doc);
                    }
                }
                return documents;
            }
        }
        #endregion

        #region DealDocuments
        private List<Document> dealDocuments;
        public List<Document> DealDocuments
        {
            get
            {
                if (dealDocuments == null)
                {
                    dealDocuments = App.VM.CO.GetDealDocuments(this);
                }
                return dealDocuments;
            }
        }
        #endregion

        #region Find Document        
        public Document FindDocument(string key, bool isSequence = false)
        {
            if (isSequence)
            {
                return Documents.Where(p => p.Sequence.Equals(key)).FirstOrDefault();
            }
            return Documents.Where(p => p.Key.Equals(key)).FirstOrDefault();
        }
        #endregion

        #region Headings
        private List<Heading> headings;
        public List<Heading> Headings {
            get
            {
                if (headings == null || headings.Count == 0)
                {
                    headings = App.VM.JC.GetHeadings(this, EstimateTypes.Sales);
                }
                return headings;
            }
        }
        #endregion

        //TODO: this will need to be a dictionary of variation seq keys
        #region VariationHeadings
        public List<Heading> variationHeadings;
        public List<Heading> VariationHeadings
        {
            get
            {
                if (variationHeadings == null || variationHeadings.Count == 0)
                {
                    variationHeadings = App.VM.JC.GetHeadings(this, EstimateTypes.Variation);
                }
                return variationHeadings;
            }
        }
        #endregion

        #region FindHeading
        public Heading FindHeading(string key, bool isCode = false)
        {
            if (isCode)
            {
                return Headings.Where(p => p.Code.Equals(key)).FirstOrDefault();
            }
            return Headings.Where(p => p.Key.Equals(key)).FirstOrDefault();
        }
        #endregion

        #region FindVariationHeading
        public Heading FindVariationHeading(string key, bool isCode = false)
        {
            if (isCode)
            {
                return VariationHeadings.Where(p => p.Code.Equals(key)).FirstOrDefault();
            }
            return VariationHeadings.Where(p => p.Key.Equals(key)).FirstOrDefault();
        }
        #endregion

        #region Events
        private List<Event> events;
        public List<Event> Events
        {
            get
            {
                if (events == null || events.Count == 0)
                {
                    events = App.VM.CO.GetEvents(this);
                }
                return events;
            }
        }
        #endregion

        #region FindEvent
        public Event FindEvent(string key, bool isCode = false)
        {
            if (isCode)
            {
                return Events.Where(p => p.Code.Equals(key)).FirstOrDefault();
            }
            return Events.Where(p => p.Key.Equals(key)).FirstOrDefault();
        }
        #endregion

        #region Checklists
        public List<Checklist> checklists;
        public List<Checklist> Checklists
        {
            get
            {
                if (checklists == null || checklists.Count == 0)
                {
                    checklists = App.VM.CO.GetChecklists(this);
                }
                return checklists;
            }
        }
        #endregion

        #region Variations
        public List<Variation> variations;
        public List<Variation> Variations
        {
            get
            {
                if (variations == null || variations.Count == 0)
                {
                    variations = App.VM.VA.GetVariations(this);
                }
                return variations;
            }
        }
        #endregion

        #region Find Variation
        public Variation FindVariation(string key)
        {
            return Variations.Where(p => p.Key.Equals(key)).FirstOrDefault();
        }
        #endregion

        #region CostCentres
        private Dictionary<Heading, List<ComboItem>> costCentres;
        public List<ComboItem> CostCentres(Heading heading)
        {
            if (costCentres == null)
            {
                costCentres = new Dictionary<Heading, List<ComboItem>>();
            }
            if (!costCentres.ContainsKey(heading))
            {
                List<ComboItem> list = new List<ComboItem>();
                foreach (var item in EstimateDetails(heading))
                {
                    if (list.Find(p => p.Code == item.CostCentre) == null)
                    {
                        ComboItem cc;
                        if (string.IsNullOrWhiteSpace(item.CostCentre))
                        {
                            cc = new ComboItem("", "No Cost Centre");
                        }
                        else
                        {
                            cc = App.VM.JC.CostCentres.Find(p => p.Code == item.CostCentre);
                            if (cc == null)
                            {
                                cc.Code = item.CostCentre;
                                cc.Desc = "Unknown Cost Centre";
                            }
                        }
                        list.Add(cc);
                    }
                }
                costCentres.Add(heading, list);
            }
            return costCentres[heading];
        }
        #endregion

        #region CostCentres
        private Dictionary<Heading, List<ComboItem>> priceDisplays;
        public List<ComboItem> PriceDisplays(Heading heading)
        {
            if (priceDisplays == null)
            {
                priceDisplays = new Dictionary<Heading, List<ComboItem>>();
            }
            if (!priceDisplays.ContainsKey(heading))
            {
                List<ComboItem> list = new List<ComboItem>();
                foreach (var item in EstimateDetails(heading))
                {
                    if (list.Find(p => p.Code == item.PriceDisplay) == null)
                    {
                        ComboItem pd;
                        if (string.IsNullOrWhiteSpace(item.PriceDisplay))
                        {
                            pd = new ComboItem("", "No Price Display");
                        }
                        else
                        {
                            pd = App.VM.JC.PDCodes.Find(p => p.Code == item.PriceDisplay);
                            if (pd == null)
                            {
                                pd.Code = item.PriceDisplay;
                                pd.Desc = "Unknown Price Display";
                            }
                        }
                        list.Add(pd);
                    }
                }
                priceDisplays.Add(heading, list);
            }
            return priceDisplays[heading];
        }
        #endregion

        #region Paragraphs
        private Dictionary<Heading, List<Paragraph>> paragraphs;
        public List<Paragraph> Paragraphs(Heading heading)
        {
            if (paragraphs == null)
            {
                paragraphs = new Dictionary<Heading, List<Paragraph>>();
            }
            if (!paragraphs.ContainsKey(heading))
            {
                paragraphs.Add(heading, App.VM.JC.GetParagraphs(heading));
            }
            return paragraphs[heading];
        }
        #endregion

        #region FindParagraph
        public Paragraph FindParagraph(Heading heading, string code)
        {
            return Paragraphs(heading).Where(p => p.Code.Equals(code)).FirstOrDefault();
        }
        #endregion

        #region EstimateDetails
        private Dictionary<Heading, List<EstimateDetail>> estimateDetails;
        public List<EstimateDetail> EstimateDetails(Heading heading)
        {
            if (estimateDetails == null)
            {
                estimateDetails = new Dictionary<Heading, List<EstimateDetail>>();
            }
            if (!estimateDetails.ContainsKey(heading))
            {
                estimateDetails.Add(heading, App.VM.JC.GetEstimateDetails(heading));
            }
            var items = estimateDetails[heading];
            App.VM.JC.LoadParagraphAddItems(heading, items);
            return estimateDetails[heading];
        }
        #endregion

        #region SelectedEstimateDetailKeys
        private Dictionary<Heading, List<string>> selectedEstimateDetailKeys;
        public List<string> SelectedEstimateDetailKeys(Heading heading)
        {
            if (selectedEstimateDetailKeys == null)
            {
                selectedEstimateDetailKeys = new Dictionary<Heading, List<string>>();
            }
            if (!selectedEstimateDetailKeys.ContainsKey(heading))
            {
                selectedEstimateDetailKeys.Add(heading, new List<string>());
            }
            return selectedEstimateDetailKeys[heading];
        }
        #endregion

        #region CopiedEstimateDetails
        private List<EstimateDetail> copiedEstimateDetails;
        public List<EstimateDetail> CopiedEstimateDetails
        {
            get
            {
                if (copiedEstimateDetails == null)
                {
                    copiedEstimateDetails = new List<EstimateDetail>();
                }
                return copiedEstimateDetails;
            }
        }
        #endregion

        #region FindEstimateDetail
        public EstimateDetail FindEstimateDetail(Heading heading, string key)
        {
            return EstimateDetails(heading).Where(p => p.Key.Equals(key)).FirstOrDefault();
        }
        #endregion

        #region GetParagraphDetails
        public List<EstimateDetail> GetParagraphDetails(Paragraph paragraph)
        {
            return EstimateDetails(paragraph.Heading).Where(p => p.Paragraph == paragraph).ToList();
        }
        #endregion

        #region CopyEstimateDetails
        public void CopyEstimateDetails(List<EstimateDetail> details)
        {
            CopiedEstimateDetails.Clear();
            CopiedEstimateDetails.AddRange(details);
        }
        #endregion
               
        #region ReloadParagraphDetails
        public void ReloadParagraphDetails(Paragraph paragraph)
        {
            var headingDetails = EstimateDetails(paragraph.Heading);
            headingDetails.RemoveAll(p => p.Paragraph == paragraph);
            headingDetails.AddRange(App.VM.JC.GetEstimateDetails(paragraph.Heading, paragraph));
        }
        #endregion
               
        #region PurchaseOrders
        public List<PurchaseOrder> purchaseOrders;
        public List<PurchaseOrder> PurchaseOrders
        {
            get
            {
                if (purchaseOrders == null || purchaseOrders.Count == 0)
                {
                    purchaseOrders = App.VM.PO.GetPurchaseOrders(this);
                }
                return purchaseOrders;
            }
        }
        #endregion

        #region FindPurchaseOrder
        public PurchaseOrder GetPurchaseOrder(string PONumber)
        {
            return PurchaseOrders.Where(p => p.PONumber.Equals(PONumber)).FirstOrDefault();
        }
        #endregion

        #region Formatted Names
        public string FriendlyName
        {
            get
            {
                string name = Name.ToLower();
                string[] names = name.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                string[] unique = names.Distinct().ToArray();
                name = string.Join(", ", unique);
                TextInfo textInfo = new CultureInfo("en-AU", false).TextInfo;
                name = textInfo.ToTitleCase(name);
                return name;
            }
        }
        
        public string Customer
        {
            get
            {
                return CustomerNumber + " - " + Name;
            }
            set
            {
                CustomerNumber = value.Substring(0, 6);
            }
        }
        public string FullAddress
        {
            get
            {
                string address = string.Empty;
                if (!string.IsNullOrEmpty(LotNumber))
                {
                    address += "Lot " + LotNumber + " ";
                }
                address += StreetNumber + " " + Address + " " + City + " " + State + " " + PostCode;
                return address;
            }
        }
        public string NameAddress
        {
            get
            {
                return Name + " - " + FullAddress;
            }
        }
        #endregion
    }
}