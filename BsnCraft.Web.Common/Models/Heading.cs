﻿using BsnCraft.Web.Common.Classes;

namespace BsnCraft.Web.Common.Models
{
    public class Heading
    {
        public Contract Contract { get; private set; }
        public EstimateTypes Type { get; private set; }

        public Heading(Contract contract, EstimateTypes type)
        {
            Contract = contract;
            Type = type;
        }

        public string Key { get; set; } = "";
        public string Section { get; set; } = "";
        public string Code { get; set; } = "";
        public string Description { get; set; } = "";
        public string Catalog { get; set; } = "";
        public decimal SellAmtEx { get; set; } = 0;
        public decimal SellAmtGST { get; set; } = 0;
        public decimal CostAmt { get; set; } = 0;
        public string GSTInc { get; set; } = "";
        public string MarginCode { get; set; } = "";
        public decimal DefMarginPct { get; set; } = 0;
        public decimal OriginalMargin { get; set; } = 0;
        public bool MarginUpdAll { get; set; } 

        #region Property Amount
        public decimal Amount
        {
            get {
                if (GSTInc == "Y")
                {
                    return SellAmtEx + SellAmtGST;
                }
                else
                {
                    return SellAmtEx;
                }
            }
        }
        #endregion

        #region Property SellAmtInc
        public decimal SellAmtInc
        {
            get { return SellAmtEx + SellAmtGST; }
        }
        #endregion

        #region Property MarginAmt
        public decimal MarginAmt
        {
            get { return SellAmtEx - CostAmt; }
        }
        #endregion

        #region Property MarginPct
        public decimal MarginPct
        {
            get
            {
                if (SellAmtEx > 0)
                {
                    return MarginAmt / SellAmtEx;
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion

        

        public bool Locked { get; set; }
        public bool IsNew { get; set; }
        public bool IsEstimate { get; set; }

        //#region Amount
        //public string Amount {
        //    get
        //    {
        //        var amount = SellAmtEx + SellAmtGST;
        //        if (amount <= 0)
        //        {
        //            return "N/A";
        //        }
        //        return amount.ToString("C");
        //    }
        //}
        //#endregion

        #region Status
        public string Status { get; set; }
        public string StatusDesc {
            get
            {
                switch (Status)
                {
                    case "A":
                        return "Accepted";
                    case "X":
                        return "Cancelled";
                    case "C":
                    case " ":
                    case "":
                        return "Open";
                }
                return "Unknown Status: " + Status;                
            }
        }
        #endregion       
        
        public bool CanEdit
        {
            get
            {
                return (!Locked && StatusDesc == "Open");
            }
        }
    }
}
