﻿using BsnCraft.Web.Common.Classes;
using System;

namespace BsnCraft.Web.Common.Models
{
    public class EstimateDetail
    {
        #region Constructor
        public EstimateDetail(Paragraph paragraph)
        {
            SetParagraph(paragraph);
        }
        #endregion

        #region Paragraph
        public Paragraph Paragraph { get; private set; }
        public Heading Heading { get; private set; }
        public Contract Contract { get; private set; }

        public void SetParagraph(Paragraph paragraph)
        {            
            if (paragraph != null)
            {
                Paragraph = paragraph;
                Heading = paragraph.Heading;
                if (Heading != null)
                {
                    Contract = Heading.Contract;
                }
            }            
        }

        public void SetParagraph(string paragraphCode)
        {
            if (string.IsNullOrEmpty(paragraphCode))
            {
                return;
            }            
            if (Contract != null && Heading != null)
            {
                var paragraph = Contract.FindParagraph(Heading, paragraphCode);
                SetParagraph(paragraph);
            }
        }

        private string paragraphCode;
        public string ParagraphCode
        {
            get
            {
                if (string.IsNullOrEmpty(paragraphCode))
                {
                    return Paragraph.Code;
                }
                return paragraphCode;
            }
            set
            {
                paragraphCode = value;
            }
        }
        #endregion

        #region Public Properties                
        public string Key { get; set; } = "";
        public string ScrKey { get; set; } = "";
        public string Sequence { get; set; } = "";
        public string Item { get; set; } = "";
        public string Description { get; set; }  = "";
        public string CostCentre { get; set; } = "";
        public decimal Quantity { get; set; }
        public decimal UnitCost { get; set; }        
        public decimal UnitAmountEx { get; set; }
        public decimal UnitAmountGst { get; set; }
        public decimal UnitAmountInc { get; set; }
        public decimal MarginPct { get; set; }
        public string MarginCode { get; set; } = "";
        public string PriceDisplay { get; set; } = "";
        public string UnitOfMeasure { get; set; } = "";        
        public bool IncludeGST { get; set; }
        public bool Included { get; set; }
        public bool ShowCheckbox { get; set; }
        public bool ShowQuantity { get; set; }
        public bool ShowSelect { get; set; }
        public bool IsNew { get; set; }
        public string NewSequence { get; set; } = "";
        public string Notes { get; set; } = "";
        public string SortOrder { get; set; }
        #endregion

        #region Calculated Fields
        public decimal ExtendedCost { get { return UnitCost * Quantity; } }
        public decimal ExtendedAmountEx { get { return UnitAmountEx * Quantity; } }
        public decimal ExtendedAmountGst { get { return UnitAmountGst * Quantity; } }
        public decimal ExtendedAmountInc { get { return UnitAmountInc * Quantity; } }
        public decimal MarginAmount { get { return (UnitCost + (UnitCost * MarginPct / 100)) * Quantity; } }

        public decimal OriginalCost { get; private set; }
        public decimal OriginalMargin { get; private set; }
        public decimal OriginalAmountEx { get; private set; }
        public decimal OriginalAmountInc { get; private set; }

        //Loaded from ViewModel
        public void SetOriginalCost(decimal cost)
        {
            UnitCost = cost;
            OriginalCost = cost;
        }

        public void SetOriginalMargin(decimal margin)
        {
            MarginPct = margin;
            OriginalMargin = margin;
        }

        public void SetOriginalAmountEx(decimal exValue)
        {
            UnitAmountEx = exValue;
            OriginalAmountEx = exValue;
        }
        public void SetOriginalAmountInc(decimal incValue)
        {
            UnitAmountInc = incValue;
            OriginalAmountInc = incValue;
        }
        #endregion

        #region Options
        private string options = string.Empty;
        public string Options
        {
            get
            {
                var parsed = options.ToLower().Replace("select=", "").Replace("'", "");
                return parsed;
            }
        }

        public void SetOptions(string value)
        {
            options = value;
        }
        private bool optionsLoaded = false;
        public void LoadOptions(string value)
        {
            if (!optionsLoaded)
            {
                options = value;
                optionsLoaded = true;
            }
        }
        #endregion

        #region LoadNotes
        public void LoadNotes(string value)
        {
            Notes = value;

            //JAC 19-Sep-2018 Try to get Description + Notes working with MS Word templates
            
            //Need to revisit this
            
            //var description = Description + " " + @"\r\n\";
            //Notes = Notes.TrimStart(description.ToCharArray());
        }
        #endregion
        
        #region Seq
        public int Seq
        {
            get
            {
                int.TryParse(Sequence, out int seq);
                return seq;
            }
        }
        #endregion
    }
}
