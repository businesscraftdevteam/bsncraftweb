﻿using System.ComponentModel;

namespace BsnCraft.Web.Common.Models
{
    public class ItemSelection
    {
        [Description("SEIH_CODE")]
        public string Code { get; set; }

        [Description("SEIH_DESC")]
        public string Description { get; set; }

        [Description("SEIH_OPTION")]
        public string Option { get; set; }

        [Description("BMSPEC_U_QUERY")]
        public string Query { get; set; }
    }
}