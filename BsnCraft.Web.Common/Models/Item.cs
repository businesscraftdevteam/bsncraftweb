﻿using BsnCraft.Web.Common.Classes;
using System;

namespace BsnCraft.Web.Common.Models
{
    public class Item
    {
        public string ItemNumber { get; set; } = "";
        public string Description { get; set; } = "";
        public string ExtraDescription { get; set; } = "";
        public string ProductCategory { get; set; } = "";
        public string ProductGroup { get; set; } = "";
        public string SortCode { get; set; } = "";
        public string Image { get; set; } = "";
        public string Notes { get; set; } = "";
    }
}
