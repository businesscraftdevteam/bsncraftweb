﻿using BsnCraft.Web.Common.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsnCraft.Web.Common.Models
{
    public class Activity
    {
        public string Code { get; set; } = "";
        public string Desc { get; set; } = "";
        public string UserSource { get; set; } = "";
        public string UserEvent { get; set; } = "";
        public int Count { get; set; } = 0;
        public bool IsSelected { get; set; } = false;
        public List<ComboItem> Events { get; set; }
    }

    public class ActivityField
    {
        public string Code { get; set; } = "";
        public string Desc { get; set; } = "";
    }
}
