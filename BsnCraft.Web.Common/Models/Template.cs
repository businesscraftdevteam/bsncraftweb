﻿
namespace BsnCraft.Web.Common.Models
{
    public class Template
    {        
        public (string, string) Code { get; set; }
        public (string, string) Group1 { get; set; }
        public (string, string) Group2 { get; set; }
        public (string, string) Group3 { get; set; }
        public (string, string) Heading { get; set; }
    }
}
