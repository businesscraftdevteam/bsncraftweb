﻿namespace BsnCraft.Web.Common.Models
{
    public class ChecklistHeader
    {
        public string Template { get; set; }
        public string Description { get; set; }
        public string Trigger { get; set; }
        public string Complete { get; set; }
    }
}
