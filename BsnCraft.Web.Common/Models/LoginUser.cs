﻿using System.ComponentModel;

namespace BsnCraft.Web.Common.Models
{
    public class LoginUser
    {
        [Description("QSA_USERNAME")]
        public string Username { get; set; }

        [Description("QSA_FULLNAME")]
        public string FullName { get; set; }

        [Description("QSA_INITIALS")]
        public string Initials { get; set; }
    }
}