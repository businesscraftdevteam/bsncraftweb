﻿
using System;
using System.Globalization;
using System.Linq;

namespace BsnCraft.Web.Common.Models
{

    public class CustomerContact
    {        
        public string Key { get; set; } = "";
        public string CustomerNumber { get; set; } = "";
        public string Name { get; set; } = "";
        public string Sequence { get; set; } = "";
        public string Title { get; set; } = "";
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public string Address1 { get; set; } = "";
        public string Address2 { get; set; } = "";
        public string Address3 { get; set; } = "";
        public string Address4 { get; set; } = "";
        public string Suburb { get; set; } = "";
        public string State { get; set; } = "";
        public string PostCode { get; set; } = "";
        public string HomePhone { get; set; } = "";
        public string WorkPhone { get; set; } = "";
        public string MobilePhone { get; set; } = "";
        public string FaxNumber { get; set; } = "";
        public string Email { get; set; } = "";

        #region EditKey
        public string EditKey
        {
            get
            {
                return CustomerNumber + ":" + Sequence;
            }
        }
        #endregion

        #region FriendlyName        
        public string FriendlyName
        {
            get
            {
                string name = Name.ToLower();
                string[] names = name.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                string[] unique = names.Distinct().ToArray();
                name = string.Join(", ", unique);
                TextInfo textInfo = new CultureInfo("en-AU", false).TextInfo;
                name = textInfo.ToTitleCase(name);
                return name;
            }
        }
        #endregion

        #region Phone Number
        public string PhoneNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(MobilePhone))
                    return "M: " + MobilePhone;

                if (!string.IsNullOrEmpty(HomePhone))
                    return "H: " + HomePhone;

                if (!string.IsNullOrEmpty(WorkPhone))
                    return "W: " + WorkPhone;

                if (!string.IsNullOrEmpty(FaxNumber))
                    return "F:" + FaxNumber;

                return string.Empty;
            }
        }
        #endregion

        #region Full Name
        public string FullName
        {
            get
            {
                if (!string.IsNullOrEmpty(Title + FirstName + LastName))
                {
                    return Title + " " + FirstName + " " + LastName;
                }
                return string.Empty;
            }
        }
        #endregion

        #region Full Address
        public string FullAddress
        {
            get
            {
                string address = Address1 + " " + Address2 + " " + Address3 + " " + Address4;
                address += " " + Suburb + " " + State + " " + PostCode;
                return address;
            }
        }
        #endregion
    }
}