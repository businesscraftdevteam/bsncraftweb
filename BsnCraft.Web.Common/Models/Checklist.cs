﻿namespace BsnCraft.Web.Common.Models
{
    public class Checklist
    {
        public string Key { get; set; }
        public string ContractNumber { get; set; }
        public string Sequence { get; set; }
        public string Template { get; set; }
        public string Description { get; set; }
    }
}
