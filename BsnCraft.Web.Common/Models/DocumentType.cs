﻿using BsnCraft.Web.Common.Classes;

namespace BsnCraft.Web.Common.Models
{
    public class DocumentType : ComboItem
    {
        public DocumentType(string code, string desc) : base(code, desc)
        {

        }

        public string MasterDocument { get; set; }
        public bool Locked { get; set; }
    }
}
