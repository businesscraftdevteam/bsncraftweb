﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace BsnCraft.Web.Common.Models
{
    #region Public Enums
    public enum DataViewTypes
    {
        Chart,
        Dashboard,
        Report
    }

    public enum DataSourceTypes
    {
        [Description("Leads By Sales Centre")]
        LeadsBySalesCentre,
        [Description("Leads By Operating Centre")]
        LeadsByOperatingCentre,
        [Description("Leads By Sales Consultant")]
        LeadsBySalesConsultant,
        [Description("Contracts By Operating Centre")]
        ContractsByOperatingCentre
    }

    public enum ChartDisplayTypes
    {
        [Description("Pie Chart")]
        Pie,
        [Description("Doughnut")]
        Doughnut,
        [Description("Bar Chart")]
        Bar,
        [Description("Area Chart")]
        Area,
        [Description("Line Chart")]
        Line
    }
    #endregion

    public class DataViewItem
    {
        [JsonProperty(PropertyName = "type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DataViewTypes Type { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; } = "";

        [JsonProperty(PropertyName = "desc")]
        public string Description { get; set; } = "";

        [JsonProperty(PropertyName = "data")]
        public string DataSource { get; set; } = "";

        [JsonProperty(PropertyName = "display")]
        public string Display { get; set; } = "";

        [JsonProperty(PropertyName = "bookmarked")]
        public bool Bookmarked { get; set; }

        [JsonIgnore]
        public bool IsNew { get; set; }

        [JsonIgnore]
        public string Key {
            get
            {
                return Type.ToString() + Name.ToString();
            }
        }
    }
}
