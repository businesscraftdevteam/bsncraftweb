﻿using System.Globalization;

namespace BsnCraft.Web.Common.Models
{
    public class Paragraph
    {
        public Heading Heading { get; private set; }

        public Paragraph(Heading heading)
        {
            Heading = heading;
        }

        public string Key { get; set; } = "";
        public string Code { get; set; } = "";
        public string Description { get; set; } = "";
        public string FormatOption { get; set; } = "";
        public string MarginCode { get; set; } = "";
        public decimal DefMarginPct { get; set; } = 0;
        public decimal OriginalMargin { get; set; } = 0;
        public bool Locked { get; set; }
        public bool IsNew { get; set; }
        public bool MarginUpdAll { get; set; }
        public bool IsSelected { get; set; }
        public bool IsParent { get; set; }

        public decimal SellAmtEx { get; set; } = 0;
        public decimal SellAmtGST { get; set; } = 0;
        public decimal CostAmt { get; set; } = 0;
        public bool IncludeGST { get; set; } = true;

        #region Property Amount
        public decimal Amount
        {
            get
            {
                return IncludeGST ? SellAmtEx + SellAmtGST : SellAmtEx;                
            }
        }
        #endregion

        #region Property SellAmtInc
        public decimal SellAmtInc
        {
            get { return SellAmtEx + SellAmtGST; }
        }
        #endregion

        #region Property MarginAmt
        public decimal MarginAmt
        {
            get { return SellAmtEx - CostAmt; }
        }
        #endregion

        #region Property MarginPct
        public decimal MarginPct
        {
            get
            {
                if (SellAmtEx > 0)
                {
                    return MarginAmt / SellAmtEx;
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion


        //#region Amount
        //public decimal AmountEx { get; set; }
        //public decimal AmountGst { get; set; }
        //public decimal Amount {
        //    get
        //    {
        //        return AmountEx + AmountGst;
        //    }
        //}
        //#endregion

        #region SumAmount
        public decimal SumAmountEx { get; set; }
        public decimal SumAmountGst { get; set; }
        public decimal SumAmount
        {
            get
            {
                if (IsParent)
                {
                    return SumAmountEx + SumAmountGst;
                }
                else
                {
                    return SellAmtInc;
                }
            }
        }
        #endregion

        #region SubHeading
        public bool SubHeading
        {
            get
            {
                return (FormatOption != "H");
            }
        }
        #endregion

        #region Options
        private string options = string.Empty;
        public string Options
        {
            get
            {
                var parsed = options.ToUpper().Replace("SELECT=", "").Replace("'", "").Replace("\r\n","");
                return parsed;
            }
        }

        public void SetOptions(string value)
        {
            options = value;
        }
        private bool optionsLoaded = false;
        public void LoadOptions(string value)
        {
            if (!optionsLoaded)
            {
                options = value;
                optionsLoaded = true;
            }
        }
        #endregion

        public string FriendlyDesc
        {
            get
            {
                string txt = Description.ToLower();
                TextInfo textInfo = new CultureInfo("en-AU", false).TextInfo;
                txt = textInfo.ToTitleCase(txt);
                return txt;
            }
        }
    }
}
