﻿using Newtonsoft.Json;

namespace BsnCraft.Web.Common.Models
{
    public class DealDocument
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; } = "";

        [JsonProperty(PropertyName = "reference")]
        public string Reference { get; set; } = "";

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; } = "";

        [JsonProperty(PropertyName = "required")]
        public bool Required { get; set; } = true;

        public string Key
        {
            get
            {
                return Type + Reference;
            }
        }
    }
}
