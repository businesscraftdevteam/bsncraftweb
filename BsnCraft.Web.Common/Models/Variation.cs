﻿using BsnCraft.Web.Common.Classes;
using System;

namespace BsnCraft.Web.Common.Models
{
    public class Variation
    {
        public string Key { get; set; } = "";
        public string ContractNumber { get; set; } = "";
        public string Sequence { get; set; } = "";
        public string Date { get; set; } = "";
        public string Reference { get; set; } = "";
        public string Description { get; set; } = "";
        public decimal IncAmount { get; set; }
        public decimal Amount { get; set; }
        public decimal GSTAmount { get; set; }
        public decimal CostAmount { get; set; }
        public decimal IssuedAmount { get; set; }
        public string ProgBillStage { get; set; } = "";
        public string GSTCode { get; set; } = "";
        public string Type { get; set; } = "";
        public string Notes { get; set; } = "";
        public bool Issued { get; set; }
        public bool IssuedRegistered { get; set; }
        public bool Invoiced { get; set; }
        public bool IsNew { get; set; }
        public bool IncludeGST { get; set; }

        #region Type Label
        public string TypeLabel
        {
            get
            {
                string lbl = "";
                switch (Type)
                {
                    case "P":
                        lbl = "Pre";
                        break;
                    case "B":
                        lbl = "Build";
                        break;
                    case "V":
                        lbl = "Post";
                        break;
                    case "X":
                        lbl = "Cancelled";
                        break;
                    default:
                        lbl = "";
                        break;
                }
                return lbl;
            }
        }
        #endregion

    }
}
