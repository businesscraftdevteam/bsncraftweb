﻿
namespace BsnCraft.Web.Common.Models
{
    public class Job
    {
        public string JobNumber { get; set; }
        public string Key { get { return JobNumber; } }
        public string CustomerNumber { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public string OpCentre { get; set; }
        public string District { get; set; }
        public bool IsBookmarked { get; set; }
    }
}
