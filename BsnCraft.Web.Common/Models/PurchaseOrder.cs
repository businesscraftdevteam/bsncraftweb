﻿using System;

namespace BsnCraft.Web.Common.Models
{
    public class PurchaseOrder
    {
        public string PONumber { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public string JobNumber { get; set; }
        public string Vendor { get; set; }
        public string VendorName { get; set; }
        public string Address { get; set; }
        public string CostCentre { get; set; }
        public string Issued { get; set; }
    }
}
