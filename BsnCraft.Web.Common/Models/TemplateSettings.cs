﻿using System.ComponentModel;

namespace BsnCraft.Web.Common.Models
{
    public class TemplateSettings
    {
        [Description("JP02_SETMPL_DESC1")]
        public string Group1Desc { get; set; }

        [Description("JP02_SETMPL_DESC2")]
        public string Group2Desc { get; set; }

        [Description("JP02_SETMPL_DESC3")]
        public string Group3Desc { get; set; }

        [Description("JP02_SETMPL_LEN1")]
        public string Group1Length { get; set; }

        [Description("JP02_SETMPL_LEN2")]
        public string Group2Length { get; set; }

        [Description("JP02_SETMPL_LEN3")]
        public string Group3Length { get; set; }
    }
}
