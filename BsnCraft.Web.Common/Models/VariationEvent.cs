﻿using System;

namespace BsnCraft.Web.Common.Models
{
    public class VariationEvent
    {
        public string Key { get; set; }
        public string ContractNumber { get; set; }
        public string VariationSequence { get; set; }
        public string EventNumber { get; set; }
        public string EventNumberA { get; set; }
        public DateTime ActualDate { get; set; }
        public DateTime RegisterDate { get; set; }
        public DateTime ForecastDate { get; set; }
        public string Reference { get; set; }
        public string Description { get; set; }
        public string Employee { get; set; }
        public string ForecastFlag { get; set; }
        public string ChangeEvent { get; set; }

        #region Extra Properties
        public bool IsRegistered
        {
            get
            {
                return (ActualDate != DateTime.MinValue);
            }
        }

        public bool IsLate
        {
            get
            {
                if (IsRegistered)
                {
                    return (ForecastDate != DateTime.MinValue && ActualDate > ForecastDate);
                }
                else
                {
                    return (ForecastDate != DateTime.MinValue && DateTime.Today > ForecastDate);
                }
            }
        }

        public string CodeAndDescription
        {
            get
            {
                return EventNumber + " " + Description;
            }
        }

        public string EmployeeAndReference
        {
            get
            {
                return Employee + " " + Reference;
            }
        }
        #endregion
    }
}
