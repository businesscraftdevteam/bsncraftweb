﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsnCraft.Web.Common.Models
{
    public class Login
    {
        public string Username { get; set; } = "";
        public string Password { get; set; } = "";
        public string Company { get; set; } = "";
    }
}
