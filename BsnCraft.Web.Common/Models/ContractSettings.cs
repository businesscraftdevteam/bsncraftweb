﻿using System.ComponentModel;

namespace BsnCraft.Web.Common.Models
{

    public class ContractSettings
    {
        [Description("COC_OPCENTRE")]
        public string DefaultOpCentre { get; set; } = "";

        [Description("COC_DISTRICT")]
        public string DefaultDistrict { get; set; } = "";

        [Description("COC_HOUSE_FILTER")]
        public string HouseFilterType { get; set; } = "C";

        [Description("COC_HOUSECAT")]
        public string HouseFilterCode { get; set; } = "";

        [Description("COC_ENQ_HOUSE")]
        public string DefaultHouse { get; set; } = "ENQUIRY";


        [Description("COC_ELE_FILTER")]
        public string FacadeFilterType { get; set; } = "C";

        [Description("COC_ELEVATCAT")]
        public string FacadeFilterCode { get; set; } = "";

        [Description("COC_ELEVATMASK")]
        public string FacadeMask { get; set; } = "";

        [Description("COC_ENQ_ELEV")]
        public string DefaultFacade { get; set; } = "ENQUIRY";
        

        [Description("COC_VAREVGRP")]
        public string VariationSalesGroup { get; set; }

        [Description("COC_SITEPHO_DOCTYP")]
        public string SitePhotoDocType { get; set; }

        [Description("COC_SITENOT_DOCTYP")]
        public string SiteNotesDocType { get; set; }
    }
}