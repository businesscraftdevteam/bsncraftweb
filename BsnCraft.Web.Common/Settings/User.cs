﻿using System;
using System.Collections.Generic;
using BsnCraft.Web.Common.Settings.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;

namespace BsnCraft.Web.Common.Settings
{
    public enum UserSettingsTypes
    {
        Theme,
        FluidLayout,
        TabletMode,
        PageSize
    }

    public class User
    {
        public Dictionary<string, string> Settings { get; set; } 
            = new Dictionary<string, string>();

        public Dictionary<(BsnMenus, BsnActions), MenuSettings> Menus { get; set; } 
            = new Dictionary<(BsnMenus, BsnActions), MenuSettings>();

        public Dictionary<BsnGrids, GridSettings> Grids { get; set; } 
            = new Dictionary<BsnGrids, GridSettings>();
     
        public List<FormSettings> Form { get; set; } = new List<FormSettings>();
        public List<TabSettings> Tabs { get; set; } = new List<TabSettings>();
    }
}
