﻿
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings.Base;
using System.Collections.Generic;
using System.ComponentModel;

namespace BsnCraft.Web.Common.Settings
{
    #region Public Enum
    public enum CompanySettingsType
    {
        None,
        [Description("Document Types")]
        DocumentTypes,
        [Description("Customer Query")]
        CustomerQuery,
        [Description("Contacts Query")]
        ContactsQuery,
        [Description("Leads Query")]
        LeadsQuery,
        [Description("Contracts Query")]
        ContractsQuery,
        [Description("Items Query")]
        ItemsQuery,
        [Description("Display Centres Query")]
        DisplayCentresQuery,
        [Description("Events Query")]
        EventsQuery,
        [Description("Customer Code")]
        CustomerCode,
        [Description("Default State")]
        DefaultState,
        [Description("Estate Code")]
        EstateCode,
        [Description("Council Code")]
        CouncilCode,
        [Description("Active Contract Status List")]
        ActiveContractStatusList,
        [Description("Active Lead Status List")]
        ActiveLeadStatusList,
        [Description("Sales Consultant Categories")]
        SalesConsultantCategories,
        [Description("Sales Estimate Sections")]
        SalesEstimateSections,
        [Description("Variation Estimate Sections")]
        VariationEstimateSections,
        [Description("JobInfo Document Types")]
        JobInfoDocumentTypes,
        [Description("Heading Document Types")]
        HeadingDocumentTypes,
        [Description("Job Info Groups")]
        JobInfoGroups,
        [Description("Event Categories")]
        EventCategories,
        [Description("Hide Main Menus")]
        HideMainMenu,
        [Description("Hide Contract Actions")]
        HideContractDetails,
        [Description("Job Info Template")]
        JobInfoTemplate,
        [Description("Pricing Area")]
        PricingArea,
        [Description("Effective Date")]
        EffectiveDate,
        [Description("Estimate Detail Increment")]
        EstimateDetailIncr,
        [Description("Job Info Memo Rows")]
        JobInfoMemoRows,
        [Description("Deal Submit Event")]
        DealSubmitEvent,
        [Description("Estimate SubHeadings")]
        EstimateSubHeadings,
        [Description("Allow Edit Paragraphs")]
        AllowEditParagraphs,
        [Description("Allow Lock Paragraphs")]
        AllowLockParagraphs,
        [Description("Item Price Lock")]
        ItemPriceLock,
        [Description("Restrict Leads")]
        RestrictLeads,
        [Description("Restrict Contracts")]
        RestrictContracts,
        [Description("Show Claim Number")]
        ShowClaimNumber,
        [Description("Show Full Error")]
        ShowFullError,
        [Description("Show PDF Tools")]
        ShowPDFTools,
        [Description("Use BsnMerge")]
        UseBsnMerge,
        [Description("Allow Empty Estimates")]
        AllowEmptyEstimates,        
        [Description("Alternate DSN")]
        AlternateDSN,
        [Description("Welcome Message")]
        WelcomeMessage,
        [Description("Default Empty Paragraph")]
        DefaultEmptyParagraph,
        [Description("Op Centre Filter")]
        OpCentreFilter,
        [Description("District Filter")]
        DistrictFilter,
        [Description("Council Filter")]
        CouncilFilter,
        [Description("Promotion Filter")]
        PromotionFilter,
        [Description("Sale Centre Filter")]
        SaleCentreFilter,
        [Description("Sale Type Filter")]
        SaleTypeFilter,
        [Description("Activities Filter")]
        ActivitiesFilter,
        [Description("Default Personal Solicitor")]
        DefaultPersonalSolicitor,
        [Description("Default Company Solicitor")]
        DefaultCompanySolicitor,
        [Description("Default Lending Authority")]
        DefaultLendingAuthority,
        [Description("Photo Width")]
        PhotoWidth,
        [Description("Photo Height")]
        PhotoHeight,
        [Description("Thumbnail Width")]
        ThumbnailWidth,
        [Description("Thumbnail Height")]
        ThumbnailHeight,
        [Description("Thumbnail Default")]
        ThumbnailDefault,
        [Description("Documents Directory")]
        DocumentsDirectory,
        [Description("Documents Edit")]
        DocumentsEdit
    }
    #endregion

    public class Company
    {
        public Dictionary<CompanySettingsType, object> Settings { get; set; } = new Dictionary<CompanySettingsType, object>();
        public List<DataViewItem> DataViews { get; set; } = new List<DataViewItem>();
        public List<DealDocument> DealDocuments { get; set; } = new List<DealDocument>();
    }
}
