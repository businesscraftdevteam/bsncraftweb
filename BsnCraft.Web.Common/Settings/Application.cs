﻿
namespace BsnCraft.Web.Common.Settings
{
    public class Application
    {
        public string BsnConnect { get; set; } = "localhost:2340";
        public string[] Companies { get; set; } = { "BCHOMES|BusinessCraft Homes" };
        public string Instance { get; set; } = "";

        public bool UppercasePassword { get; set; } = false;

        public int BsnPort
        {
            get
            {
                int.TryParse(BsnConnect.Split(':')[1], out int bsnPort);
                return bsnPort;
            }
        }

        public string BsnHost
        {
            get
            {
                return BsnConnect.Split(':')[0];
            }
        }
    }
}
