﻿using BsnCraft.Web.Common.Views.Form.Base;
using System.Collections.Generic;

namespace BsnCraft.Web.Common.Settings.Base
{
    public class FormSettings
    {
        public BsnForms Form { get; set; } = BsnForms.Login;
        public Dictionary<string, string> Settings { get; set; } = new Dictionary<string, string>();
    }
}
