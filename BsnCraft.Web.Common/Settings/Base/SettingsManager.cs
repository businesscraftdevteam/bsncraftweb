﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;

namespace BsnCraft.Web.Common.Settings.Base
{
    #region Enums
    public enum SettingsLevel
    {
        System,
        Company,
        Profile,
        User
    }
    #endregion

    public class SettingsManager
    {
        #region Public Properties
        public Application Application { get; private set; }
        public Company Company { get; private set; }
        public User User { get; private set; }        
        public bool Update { get; set; }
        public int GridUID { get; set; }
        #endregion

        #region Private Properties        
        private Logger Log { get; set; }
        private BsnApplication App { get; set; }        
        private bool Loaded { get; set; }

        private const string ConfigPrefix = "Web.Application";
        #endregion

        #region Constructor
        public SettingsManager(BsnApplication app)
        {
            Log = LogManager.GetLogger(GetType().FullName);
            App = app;
            User = new User();
            Company = new Company();
            Init();
        }
        #endregion

        #region Init
        public void Init()
        {
            GridUID = 0;
            companies = null;
            Loaded = false;

            var appSettings = HttpContext.Current.Server.MapPath(@"~/home/Settings.json");
            var jsonSettings = File.ReadAllText(appSettings);
            Application = (Application)JsonConvert.DeserializeObject(jsonSettings, typeof(Application));

            if (Application == null)
            {
                //TODO: could take to setup page instead of default bchomes login
                Application = new Application();
            }
        }
        #endregion

        #region Load
        public void Load()
        {
            try
            {
                if (Loaded || !App.Api.Connected)
                    return;

                Loaded = true;

                TypeDescriptor.AddAttributes(typeof((BsnMenus, BsnActions)), 
                    new TypeConverterAttribute(typeof(BsnExtensions.ValueTupleConverter<BsnMenus, BsnActions>)));

                var jsonSettings = Read(SettingsLevel.Company);
                if (!string.IsNullOrEmpty(jsonSettings))
                {
                    Company = (Company)JsonConvert.DeserializeObject(jsonSettings, typeof(Company));
                }

                jsonSettings = Read(SettingsLevel.Company, "DataViews");
                if (!string.IsNullOrEmpty(jsonSettings))
                {
                    Company.DataViews = (List<DataViewItem>)JsonConvert.DeserializeObject(jsonSettings, typeof(List<DataViewItem>));
                }

                jsonSettings = Read(SettingsLevel.User);
                if (!string.IsNullOrEmpty(jsonSettings))
                {
                    User = (User)JsonConvert.DeserializeObject(jsonSettings, typeof(User));
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region Companies
        private List<ComboItem> companies;
        public List<ComboItem> Companies
        {
            get
            {
                if (companies == null)
                {
                    companies = new List<ComboItem>();
                    foreach (string comp in Application.Companies)
                    {
                        if (comp.Contains("|"))
                        {
                            string code = comp.Split('|')[0];
                            string desc = comp.Split('|')[1];
                            companies.Add(new ComboItem(code, desc));
                        }
                        else
                        {
                            companies.Add(new ComboItem(comp, comp));
                        }
                    }
                }
                return companies;
            }
        }
        #endregion

        #region SaveUser
        public void SaveUser()
        {
            try
            {      
                Write(SettingsLevel.User, JsonConvert.SerializeObject(User));
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
                return;
            }
            App.Utils.SetSuccess();
        }
        #endregion

        #region SaveComp
        public void SaveComp()
        {
            try
            {
                Write(SettingsLevel.Company, JsonConvert.SerializeObject(Company));                
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
                return;
            }
            App.Utils.SetSuccess();
        }
        #endregion

        #region ConfigValues
        void ConfigValues(SettingsLevel lvl, ref string level, ref string code, ref string id)
        {
            level = "S";
            code = string.Empty;
            var prefix = ConfigPrefix;
            if (!string.IsNullOrEmpty(Application.Instance))
            {
                //TODO: bring back when saving settings
                //prefix += "." + Application.Instance;
            }
            if (!string.IsNullOrEmpty(id))
            {
                prefix += ".";
            }

            switch (lvl)
            {
                case SettingsLevel.Company:
                    level = "C";
                    code = App.Api.Company;
                    break;
                case SettingsLevel.Profile:
                    level = "P";
                    break;
                case SettingsLevel.User:
                    level = "U";
                    code = App.Api.Username;
                    prefix += "." + App.Api.Company;
                    break;
            }

            id = prefix + id;
        }
        #endregion

        #region Read
        public string Read(SettingsLevel lvl, string id = "")
        {
            var level = string.Empty;
            var code = string.Empty;
            ConfigValues(lvl, ref level, ref code, ref id);
            return App.Api.ConfigRead(level, code, id);
        }
        #endregion

        #region Write
        public void Write(SettingsLevel lvl, string settings, string id = "")
        {
            var level = string.Empty;
            var code = string.Empty;
            ConfigValues(lvl, ref level, ref code, ref id);
            Log.Trace("Saving settings: " + settings);
            App.Api.ConfigSave(level, code, id, settings);
            Update = false;
        }
        #endregion

        #region Comp
        public object Comp(CompanySettingsType type)
        {
            try
            {
                if (type == CompanySettingsType.None)
                {
                    return string.Empty;
                }
                var setting = CompanySettings.Where(p => p.Type.Equals(type)).FirstOrDefault();
                if (setting == null)
                {
                    App.Utils.SetError("Unable to find setting");
                    return string.Empty;
                }
                var dataType = "System.String";
                if (setting.Value != null)
                {
                    dataType = setting.Value.GetType().FullName;
                }
                else
                {
                    App.Utils.SetError("No default for setting: " + type.ToString());
                }
                if (Company.Settings.ContainsKey(setting.Type))
                {
                    switch (dataType)
                    {
                        case "System.Int32":
                            return (int)Company.Settings[setting.Type];
                        case "System.Int64":
                            return (long)Company.Settings[setting.Type];
                        case "System.String[]":
                            return (string[])Company.Settings[setting.Type];
                        case "Newtonsoft.Json.Linq.JArray":
                            var array = (JArray)Company.Settings[setting.Type];
                            return array.ToObject<string[]>();
                        case "System.Boolean":
                            return (bool)Company.Settings[setting.Type];
                    }
                    return Company.Settings[setting.Type].ToString();
                }
                return setting.Value;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
            return string.Empty;
        }

        public void Comp(CompanySettingsType type, string value)
        {
            try
            {
                if (type == CompanySettingsType.None)
                {
                    return;
                }
                //Programmer to dictate the dataType from CompanyDefaults
                var setting = CompanyDefaults.Where(p => p.Type.Equals(type)).FirstOrDefault();
                if (setting == null)
                {
                    App.Utils.SetError("Unable to find setting: " + type.ToString());
                    return;
                }
                var dataType = "System.String";
                if (setting.Value != null)
                {
                    dataType = setting.Value.GetType().FullName;
                }
                else
                {
                    App.Utils.SetError("No default for setting: " + type.ToString());
                }
                switch (dataType)
                {
                    case "System.Int32":
                    case "System.Int64":
                        Company.Settings[setting.Type] = int.Parse(value);
                        break;
                    case "System.String[]":
                        Company.Settings[setting.Type] = value.Split(',').ToArray();
                        break;
                    case "System.Boolean":
                        Company.Settings[setting.Type] = (value == "Y");
                        break;
                    default:
                        Company.Settings[setting.Type] = value;
                        break;
                }
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
                return;
            }
            SaveComp();
        }
        #endregion

        #region Main Tab
        public void MainTab(BsnMenus menu, BsnActions action, string id, BsnModes mode, string key)
        {
            var tab = new TabSettings()
            {
                Menu = menu,
                Id = id,
                TimeOpened = DateTime.Now
            };
            if (User.Tabs.Any(p => p.Menu.Equals(menu) && p.Id.Equals(id)))
            {
                tab = User.Tabs.Where(p => p.Menu.Equals(menu) && p.Id.Equals(id)).FirstOrDefault();                                
            }
            else
            {
                User.Tabs.Add(tab);
            }
            tab.Action = action;
            tab.Mode = mode;
            tab.TimeActive = DateTime.Now;            
            tab.Key = key;
            var openKeys = new List<KeySettings>();
            if (tab.OpenKeys.ContainsKey(action))
            {
                openKeys = tab.OpenKeys[action];
            }
            else
            {
                tab.OpenKeys.Add(action, openKeys);
            }
            if (!string.IsNullOrEmpty(key) && !openKeys.Any(p => string.IsNullOrEmpty(p.Key)))
            {
                openKeys.Add(new KeySettings() { TimeOpened = DateTime.Now });
            }            
            var openKey = new KeySettings()
            {
                Key = key,
                TimeOpened = DateTime.Now
            };
            if (openKeys.Any(p => p.Key.Equals(key)))
            {
                openKey = openKeys.Where(p => p.Key.Equals(key)).FirstOrDefault();
            }
            else
            {
                openKeys.Add(openKey);
            }
            openKey.Mode = mode;
            openKey.TimeActive = DateTime.Now;
            Update = true;

            //Added this for switching between desktop and main grids
            GridUID++;
        }
        #endregion

        #region CloseTab
        public Callback CloseTab(BsnMenus menu, BsnActions action, string id)
        {
            var callback = new Callback();
            if (User.Tabs.Any(p => p.Menu.Equals(menu) && p.Id.Equals(id)))
            {
                var tab = User.Tabs.Where(p => p.Menu.Equals(menu) && p.Id.Equals(id)).FirstOrDefault();
                User.Tabs.Remove(tab);
                var prevTab = User.Tabs.OrderByDescending(p => p.TimeActive).ToList().FirstOrDefault();
                if (prevTab != null)
                {
                    callback.Type = CallbackType.WindowLocation;
                    callback.App = Application.Instance;
                    callback.Menu = prevTab.Menu;
                    callback.Action = prevTab.Action;
                    callback.Id = prevTab.Id;
                    callback.Mode = prevTab.Mode;
                    callback.Key = prevTab.Key;
                }
                Update = true;
            }
            return callback;
        }
        #endregion

        #region CloseKey        
        public Callback CloseKey(BsnMenus menu, BsnActions action, string id, string key)
        {
            var callback = new Callback();
            if (!User.Tabs.Any(p => p.Menu.Equals(menu) && p.Id.Equals(id)))
            {
                return callback;
            }
            var tab = User.Tabs.Where(p => p.Menu.Equals(menu) && p.Id.Equals(id)).FirstOrDefault();
            if (!tab.OpenKeys.ContainsKey(action))
            {
                return callback;
            }
            var openKeys = tab.OpenKeys[action];
            if (!openKeys.Any(p => p.Key.Equals(key)))
            {
                return callback;
            }
            var openKey = openKeys.Where(p => p.Key.Equals(key)).FirstOrDefault();
            openKeys.Remove(openKey);
            var prevKey = openKeys.OrderByDescending(p => p.TimeActive).ToList().FirstOrDefault();
            if (prevKey != null)
            {
                callback.Type = CallbackType.WindowLocation;
                callback.App = Application.Instance;
                callback.Menu = tab.Menu;
                callback.Action = tab.Action;
                callback.Id = tab.Id;
                callback.Mode = prevKey.Mode;
                callback.Key = prevKey.Key;
            }
            Update = true;
            return callback;
        }
        #endregion

        #region User Setting
        public string UserSetting(UserSettingsTypes type)
        {
            return UserSetting(type.ToString());
        }

        public string UserSetting(string key)
        {
            if (!App.Api.Connected)
                return string.Empty;

            if (User.Settings.ContainsKey(key))
            {
                return User.Settings[key];
            }
            return string.Empty;
        }

        public void UserSetting(UserSettingsTypes type, string value)
        {
            UserSetting(type.ToString(), value);
            if (type == UserSettingsTypes.PageSize)
            {
                GridUID++;
            }
        }

        public void UserSetting(string key, string value)
        {
            User.Settings[key] = value;
            Update = true;
        }
        #endregion

        #region Menu Option
        public void MenuOption((BsnMenus, BsnActions) uid, string key, bool value)
        {
            if (!User.Menus.ContainsKey(uid))
            {
                User.Menus.Add(uid, new MenuSettings());
            }
            User.Menus[uid].Options[key] = value;
            Update = true;

            //TODO: could make GridUID increment optional based on extra param
            GridUID++;            
        }
        #endregion

        #region Menu Panels
        public List<DockPanel> MenuPanels((BsnMenus, BsnActions) uid)
        {
            if (!User.Menus.ContainsKey(uid))
            {
                User.Menus.Add(uid, new MenuSettings());
            }
            return User.Menus[uid].Panels.OrderBy(p => p.VisibleIndex).ToList();
        }
        #endregion

        #region Update Panels
        public void UpdatePanels((BsnMenus, BsnActions) uid, List<DockPanel> panels)
        {
            if (!User.Menus.ContainsKey(uid))
            {
                User.Menus.Add(uid, new MenuSettings());
            }
            User.Menus[uid].Panels = panels;
            Update = true;
        }
        #endregion

        #region Update Panel
        public void UpdatePanel((BsnMenus, BsnActions) uid, string name, bool visible)
        {
            if (!User.Menus.ContainsKey(uid))
            {
                User.Menus.Add(uid, new MenuSettings());
            }
            var panel = User.Menus[uid].Panels.Where(p => p.Name.Equals(name)).FirstOrDefault();
            if (panel != null)
            {
                panel.Visible = visible;
                Update = true;
            }            
        }
        #endregion

        #region SetBookmark
        public void SetBookmark(BsnGrids grid, string field, string recordKey)
        {            
            if (!User.Grids.ContainsKey(grid))
            {
                User.Grids.Add(grid, new GridSettings());
            }
            if (!User.Grids[grid].Bookmarks.ContainsKey(field))
            {
                User.Grids[grid].Bookmarks.Add(field, new List<BookmarkSettings>());
            }
            var bookmarks = User.Grids[grid].Bookmarks[field];
            var isBookmark = bookmarks.Any(p => p.RecordKey.Equals(recordKey));
            if (isBookmark)
            {
                var bookmark = bookmarks.Where(p => p.RecordKey.Equals(recordKey)).FirstOrDefault();
                bookmarks.Remove(bookmark);
            }
            else
            {
                bookmarks.Add(new BookmarkSettings() { RecordKey = recordKey, DateSaved = DateTime.Now });
            }
            Update = true;
        }
        #endregion

        #region ColumnVisible
        public void ColumnVisible(BsnGrids grid, string column, bool visible)
        {
            if (!User.Grids.ContainsKey(grid))
            {
                User.Grids.Add(grid, new GridSettings());
            }
            if (!User.Grids[grid].Columns.ContainsKey(column))
            {
                User.Grids[grid].Columns.Add(column, new ColumnSettings());
            }
            if (User.Grids[grid].Columns.ContainsKey(column))
            {
                User.Grids[grid].Columns[column].Visible = visible;
            }
            else
            {
                User.Grids[grid].Columns.Add(column, new ColumnSettings() { Visible = visible });
            }            
            Update = true;
            GridUID++;
        }
        #endregion

        #region GroupToggleVisibility
        public void GroupToggleVisibility(BsnGrids grid, bool value)
        {
            if (!User.Grids.ContainsKey(grid))
            {
                User.Grids.Add(grid, new GridSettings());
            }
            User.Grids[grid].ShowGroups = value;
            App.Settings.GridUID++;
        }
        #endregion

        #region GroupToggle
        public void GroupToggle(BsnGrids grid, string groupList, bool expanded = false)
        {
            if (!User.Grids.ContainsKey(grid))
            {
                User.Grids.Add(grid, new GridSettings());
            }

            var groups = (List<string>)JsonConvert.DeserializeObject(groupList, typeof(List<string>));
            foreach (var group in groups)
            {
                if (!User.Grids[grid].Groups.ContainsKey(group))
                {
                    User.Grids[grid].Groups.Add(group, expanded);
                }
                else
                {
                    if (groups.Count == 1)
                    {
                        User.Grids[grid].Groups[group] = !User.Grids[grid].Groups[group];
                    }
                    else
                    {
                        User.Grids[grid].Groups[group] = expanded;
                    }
                }
            }
            Update = true;
        }
        #endregion

        #region NotesToggleVisibility
        public void NotesToggleVisibility(BsnGrids grid, bool value)
        {
            if (!User.Grids.ContainsKey(grid))
            {
                User.Grids.Add(grid, new GridSettings());
            }
            User.Grids[grid].ShowNotes = value;
            App.Settings.GridUID++;
        }
        #endregion

        #region CompanySettings
        public List<CompanySettings> CompanySettings
        {
            //This is used to bind the grid for new settings not yet saved
            get
            {
                var companySettings = new List<CompanySettings>();
                foreach (var defaultSetting in CompanyDefaults)
                {
                    var value = defaultSetting.Value;
                    if (Company.Settings.ContainsKey(defaultSetting.Type))
                    {
                        value = Company.Settings[defaultSetting.Type];
                    }
                    companySettings.Add(new CompanySettings(defaultSetting.Type,
                        defaultSetting.Group, defaultSetting.Category, value));
                }
                return companySettings;
            }
        }
        #endregion

        #region CompanyDefaults
        private List<CompanySettings> companyDefaults;
        public List<CompanySettings> CompanyDefaults
        {
            get
            {
                if (companyDefaults == null)
                {
                    companyDefaults = new List<CompanySettings>
                    {
                        { new CompanySettings(CompanySettingsType.AlternateDSN, CompanySettingsGroup.Options, CompanySettingsCategory.System, "") },
                        { new CompanySettings(CompanySettingsType.ShowFullError, CompanySettingsGroup.Options, CompanySettingsCategory.System, false) },
                        { new CompanySettings(CompanySettingsType.ShowPDFTools, CompanySettingsGroup.Options, CompanySettingsCategory.System, false) },
                        { new CompanySettings(CompanySettingsType.UseBsnMerge, CompanySettingsGroup.Options, CompanySettingsCategory.System, false) },
                        { new CompanySettings(CompanySettingsType.RestrictLeads, CompanySettingsGroup.Security, CompanySettingsCategory.Contracts, false) },
                        { new CompanySettings(CompanySettingsType.RestrictContracts, CompanySettingsGroup.Security, CompanySettingsCategory.Contracts, false) },
                        { new CompanySettings(CompanySettingsType.DocumentsEdit, CompanySettingsGroup.Security, CompanySettingsCategory.Documents, false) },
                        { new CompanySettings(CompanySettingsType.CustomerQuery, CompanySettingsGroup.Query, CompanySettingsCategory.Customers, "ORDER BY CUSNO DESC") },
                        { new CompanySettings(CompanySettingsType.ContactsQuery, CompanySettingsGroup.Query, CompanySettingsCategory.Customers, "ORDER BY IVTU_CUSTNO DESC, IVTU_SEQNUM ASC") },
                        { new CompanySettings(CompanySettingsType.LeadsQuery, CompanySettingsGroup.Query, CompanySettingsCategory.Contracts, "ORDER BY CO_KEY DESC") },
                        { new CompanySettings(CompanySettingsType.ContractsQuery, CompanySettingsGroup.Query, CompanySettingsCategory.Contracts, "ORDER BY CO_KEY DESC") },                        
                        { new CompanySettings(CompanySettingsType.DisplayCentresQuery, CompanySettingsGroup.Query, CompanySettingsCategory.Contracts, "") },
                        { new CompanySettings(CompanySettingsType.EventsQuery, CompanySettingsGroup.Query, CompanySettingsCategory.Contracts, "") },
                        { new CompanySettings(CompanySettingsType.ItemsQuery, CompanySettingsGroup.Query, CompanySettingsCategory.Jobs, "ORDER BY ITEMNO ASC") },
                        { new CompanySettings(CompanySettingsType.DocumentsDirectory, CompanySettingsGroup.Defaults, CompanySettingsCategory.Documents, "~/Docs/") },
                        { new CompanySettings(CompanySettingsType.CustomerCode, CompanySettingsGroup.Defaults, CompanySettingsCategory.Customers, "") },
                        { new CompanySettings(CompanySettingsType.DefaultState, CompanySettingsGroup.Defaults, CompanySettingsCategory.Contracts, "NSW") },
                        { new CompanySettings(CompanySettingsType.DealSubmitEvent, CompanySettingsGroup.Defaults, CompanySettingsCategory.Contracts, 0) },
                        { new CompanySettings(CompanySettingsType.DefaultPersonalSolicitor, CompanySettingsGroup.Defaults, CompanySettingsCategory.Contracts, "") },
                        { new CompanySettings(CompanySettingsType.DefaultCompanySolicitor, CompanySettingsGroup.Defaults, CompanySettingsCategory.Contracts, "") },
                        { new CompanySettings(CompanySettingsType.DefaultLendingAuthority, CompanySettingsGroup.Defaults, CompanySettingsCategory.Contracts, "") },
                        { new CompanySettings(CompanySettingsType.PricingArea, CompanySettingsGroup.Defaults, CompanySettingsCategory.Jobs, "") },
                        { new CompanySettings(CompanySettingsType.EffectiveDate, CompanySettingsGroup.Defaults, CompanySettingsCategory.Jobs, "") },
                        { new CompanySettings(CompanySettingsType.EstimateDetailIncr, CompanySettingsGroup.Defaults, CompanySettingsCategory.Jobs, 10) },
                        { new CompanySettings(CompanySettingsType.EstateCode, CompanySettingsGroup.Defaults, CompanySettingsCategory.Jobs, "$ESTAT") },
                        { new CompanySettings(CompanySettingsType.CouncilCode, CompanySettingsGroup.Defaults, CompanySettingsCategory.Jobs, "$COUNC") },
                        { new CompanySettings(CompanySettingsType.JobInfoTemplate, CompanySettingsGroup.Defaults, CompanySettingsCategory.Jobs, "$MASTER") },
                        { new CompanySettings(CompanySettingsType.DefaultEmptyParagraph, CompanySettingsGroup.Defaults, CompanySettingsCategory.Jobs, "") },
                        { new CompanySettings(CompanySettingsType.ActiveContractStatusList, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "CUR" }) },
                        { new CompanySettings(CompanySettingsType.ActiveLeadStatusList, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "ENQ" }) },
                        { new CompanySettings(CompanySettingsType.SalesConsultantCategories, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.VariationEstimateSections, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "V" }) },
                        { new CompanySettings(CompanySettingsType.EventCategories, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.DocumentTypes, CompanySettingsGroup.Filters, CompanySettingsCategory.Documents, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.JobInfoDocumentTypes, CompanySettingsGroup.Filters, CompanySettingsCategory.Documents, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.HeadingDocumentTypes, CompanySettingsGroup.Filters, CompanySettingsCategory.Documents, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.ShowClaimNumber, CompanySettingsGroup.Options, CompanySettingsCategory.Contracts, false) },
                        { new CompanySettings(CompanySettingsType.SalesEstimateSections, CompanySettingsGroup.Filters, CompanySettingsCategory.Jobs, new string[] { "SE" }) },                        
                        { new CompanySettings(CompanySettingsType.JobInfoGroups, CompanySettingsGroup.Filters, CompanySettingsCategory.Jobs, new string[] { "" }) },                                                                                                                                      
                        { new CompanySettings(CompanySettingsType.EstimateSubHeadings, CompanySettingsGroup.Options, CompanySettingsCategory.Jobs, true) },
                        { new CompanySettings(CompanySettingsType.AllowEditParagraphs, CompanySettingsGroup.Options, CompanySettingsCategory.Jobs, true) },
                        { new CompanySettings(CompanySettingsType.AllowLockParagraphs, CompanySettingsGroup.Options, CompanySettingsCategory.Jobs, false) },
                        { new CompanySettings(CompanySettingsType.ItemPriceLock, CompanySettingsGroup.Options, CompanySettingsCategory.Jobs, false) },                                                
                        { new CompanySettings(CompanySettingsType.AllowEmptyEstimates, CompanySettingsGroup.Options, CompanySettingsCategory.Jobs, true) },                        
                        { new CompanySettings(CompanySettingsType.OpCentreFilter, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.DistrictFilter, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.CouncilFilter, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.PromotionFilter, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.SaleCentreFilter, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.SaleTypeFilter, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.ActivitiesFilter, CompanySettingsGroup.Filters, CompanySettingsCategory.Contracts, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.HideContractDetails, CompanySettingsGroup.UI, CompanySettingsCategory.Contracts, new string[] { "" }) },
                        { new CompanySettings(CompanySettingsType.JobInfoMemoRows, CompanySettingsGroup.UI, CompanySettingsCategory.Jobs, 10) },
                        { new CompanySettings(CompanySettingsType.WelcomeMessage, CompanySettingsGroup.UI, CompanySettingsCategory.System, "") },
                        { new CompanySettings(CompanySettingsType.HideMainMenu, CompanySettingsGroup.UI, CompanySettingsCategory.System, new string[] { "" }) },                        
                        { new CompanySettings(CompanySettingsType.PhotoWidth, CompanySettingsGroup.UI, CompanySettingsCategory.System, 1600) },
                        { new CompanySettings(CompanySettingsType.PhotoHeight, CompanySettingsGroup.UI, CompanySettingsCategory.System, 1200) },
                        { new CompanySettings(CompanySettingsType.ThumbnailWidth, CompanySettingsGroup.UI, CompanySettingsCategory.System, 300) },
                        { new CompanySettings(CompanySettingsType.ThumbnailHeight, CompanySettingsGroup.UI, CompanySettingsCategory.System, 200) },
                        { new CompanySettings(CompanySettingsType.ThumbnailDefault, CompanySettingsGroup.UI, CompanySettingsCategory.System, "~/assets/noimage.png") }
                    };
                }
                return companyDefaults;
            }
        }
        #endregion

    }

}
