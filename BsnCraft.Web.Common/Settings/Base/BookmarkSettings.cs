﻿using System;

namespace BsnCraft.Web.Common.Settings.Base
{
    public class BookmarkSettings
    {
        public string RecordKey { get; set; }

        //Could use this to clean up old keys in future
        public DateTime DateSaved { get; set; } = DateTime.MinValue;
    }
}
