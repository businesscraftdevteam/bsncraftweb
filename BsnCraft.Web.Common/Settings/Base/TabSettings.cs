﻿using BsnCraft.Web.Common.Views.Menu.Base;
using System;
using System.Collections.Generic;

namespace BsnCraft.Web.Common.Settings.Base
{
    public class TabSettings
    {
        public BsnMenus Menu { get; set; } = BsnMenus.Home;
        public string Id { get; set; } = "";

        public BsnActions Action { get; set; } = BsnActions.Default;
        public BsnModes Mode { get; set; } = BsnModes.None;
        public string Key { get; set; } = "";

        public DateTime TimeOpened { get; set; } = DateTime.MinValue;
        public DateTime TimeActive { get; set; } = DateTime.MinValue;

        public Dictionary<BsnActions, List<KeySettings>> OpenKeys { get; set; } 
            = new Dictionary<BsnActions, List<KeySettings>>();
    }
}
