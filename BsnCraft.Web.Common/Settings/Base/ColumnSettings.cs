﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsnCraft.Web.Common.Settings.Base
{
    public class ColumnSettings
    {
        public bool Visible { get; set; } = true;
        public int Sort { get; set; } = -1;
    }
}
