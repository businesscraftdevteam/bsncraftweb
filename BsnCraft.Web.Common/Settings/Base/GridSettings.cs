﻿using System.Collections.Generic;

namespace BsnCraft.Web.Common.Settings.Base
{
    public class GridSettings
    {
        public Dictionary<string, ColumnSettings> Columns { get; set; } = new Dictionary<string, ColumnSettings>();
        public Dictionary<string, bool> Groups { get; set; } = new Dictionary<string, bool>();
        public Dictionary<string, string> Settings { get; set; } = new Dictionary<string, string>();
        public Dictionary<string, List<BookmarkSettings>> Bookmarks { get; set; } = new Dictionary<string, List<BookmarkSettings>>();
        public bool ShowGroups { get; set; } = true;
        public bool ShowNotes { get; set; } = true;
    }
}
