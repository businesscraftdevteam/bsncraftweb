﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsnCraft.Web.Common.Settings.Base
{
    #region Public Enums
    public enum CompanySettingsGroup
    {
        [Description("Options")]
        Options,
        [Description("Security")]
        Security,
        [Description("User Interface")]
        UI,
        [Description("Defaults")]
        Defaults,
        [Description("ODBC Query Extras")]
        Query,
        [Description("Data Filters")]
        Filters
    }

    public enum CompanySettingsCategory
    {
        [Description("System")]
        System,
        [Description("Customers")]
        Customers,
        [Description("Contracts")]
        Contracts,
        [Description("Jobs")]
        Jobs,
        [Description("Documents")]
        Documents
    }
    #endregion

    public class CompanySettings
    {
        //Note: This structure is only used for display purposes
        //      Did not want to bloat serialization with groups/cagtegories

        public CompanySettingsType Type { get; private set; }
        public CompanySettingsGroup Group { get; private set; }
        public CompanySettingsCategory Category { get; private set; }
        public object Value { get; private set; }

        public CompanySettings(CompanySettingsType type, CompanySettingsGroup group, 
            CompanySettingsCategory category, object value)
        {
            Type = type;
            Group = group;
            Category = category;
            Value = value;
        }
    }
}
