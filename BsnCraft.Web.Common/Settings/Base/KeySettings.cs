﻿using BsnCraft.Web.Common.Views.Menu.Base;
using System;
using System.Collections.Generic;

namespace BsnCraft.Web.Common.Settings.Base
{
    public class KeySettings
    {
        public string Key { get; set; } = "";
        public BsnModes Mode { get; set; } = BsnModes.None;        

        public DateTime TimeOpened { get; set; } = DateTime.MinValue;
        public DateTime TimeActive { get; set; } = DateTime.MinValue;
    }
}
