﻿using BsnCraft.Web.Common.Classes;
using System.Collections.Generic;

namespace BsnCraft.Web.Common.Settings.Base
{
    public class MenuSettings
    {
        public Dictionary<string, bool> Options { get; set; } = new Dictionary<string, bool>();
        public List<DockPanel> Panels { get; set; } = new List<DockPanel>();
    }
}
