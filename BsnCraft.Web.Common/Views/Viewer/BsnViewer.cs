﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Base;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Views.Viewer
{
    public class BsnViewer : BsnView
    {
        #region Public Properties
        public BsnApplication App { get; private set; }
        public string Filename { get; set; } = "";
        public string MasterFile { get; set; } = "";
        #endregion

        #region Private Properties
        private Logger Log { get; set; }
        #endregion

        #region Constructor
        public BsnViewer(BsnApplication app)
        {
            Log = LogManager.GetLogger(GetType().FullName);
            App = app;
        }
        #endregion

        #region FileViewer
        public override Control ToControl()
        {
            base.ToControl();

            var fileViewer = new HtmlGenericControl("iframe");
            fileViewer.Attributes["style"] = "border: 0; width: 100%; height: 100%; min-height: 1000px;";

            var extension = Path.GetExtension(Filename).ToUpper().Replace(".", "");
            int.TryParse(extension, out int lineNumber);

            if (!string.IsNullOrEmpty(MasterFile) && lineNumber > 0)
            {
                string oldFilename = HttpContext.Current.Server.MapPath(Filename);
                extension = Path.GetExtension(MasterFile).ToUpper().Replace(".", "");
                if (extension == "DOC")
                {
                    if (App.Settings.Comp(CompanySettingsType.UseBsnMerge).ToBool())
                    {
                        //RichEdit Expects docx from BsnMerge
                        Filename = Filename.Replace(".", "_") + ".docx";
                    }
                }
                else
                {
                    Filename = Filename.Replace(".", "_") + Path.GetExtension(MasterFile);
                }

                string newFilename = HttpContext.Current.Server.MapPath(Filename);
                try
                {
                    if (File.Exists(newFilename))
                    {
                        File.Delete(newFilename);
                    }
                    File.Move(oldFilename, newFilename);
                }
                catch (Exception e)
                {
                    App.Utils.LogExc(e);
                }
            }

            Log.Trace("About to view: " + Filename);

            switch (extension)
            {
                case "PDF":
                    fileViewer.Attributes["src"] = Helpers.ResolveUrl("~/Viewers/pdf/viewer.html?file=" + Filename);
                    if (App.Settings.Comp(CompanySettingsType.ShowPDFTools).ToBool())
                    {
                        var pdfWrapper = new HtmlGenericControl("div");
                        pdfWrapper.Attributes["style"] = "border:0; width:100%; height:100%;";
                        var pdfViewer = new HtmlGenericControl("div");
                        pdfViewer.Attributes["style"] = "border:0; width:100%; height:100%; padding-top: 30px;";

                        //var pdfToolbar = new HtmlGenericControl("div");
                        //pdfToolbar.Attributes["style"] = "position: absolute; text-align: right;";
                        //var pdfTablet = new BsnHtmlButton("javascript:", "fal fa-tablet-alt", "no-btn-lg", ButtonTypes.Link);
                        //pdfToolbar.Controls.Add(pdfTablet);
                        //var pdfDesktop = new BsnHtmlButton("javascript:", "fal fa-desktop", "no-btn-lg", ButtonTypes.Link);
                        //pdfToolbar.Controls.Add(pdfDesktop);

                        //pdfViewer.Controls.Add(fileViewer);
                        //pdfWrapper.Controls.Add(pdfToolbar);
                        //pdfWrapper.Controls.Add(pdfViewer);

                        return pdfWrapper;
                    }
                    break;
                case "RTF":
                case "DOC":
                case "DOCX":
                    fileViewer.Attributes["src"] = Helpers.ResolveUrl("~/Viewers/RichEdit.aspx?filename=" + Filename);
                    break;
                case "HTM":
                case "HTML":
                    fileViewer.Attributes["src"] = Helpers.ResolveUrl("~/Viewers/HtmlEditor.aspx?filename=" + Filename);
                    break;
                case "CSV":
                case "XLS":
                case "XLSX":
                    fileViewer.Attributes["src"] = Helpers.ResolveUrl("~/Viewers/Spreadsheet.aspx?filename=" + Filename);
                    break;
                default:
                    fileViewer.Attributes["src"] = Filename;
                    break;
            }
            return fileViewer;
        }
        #endregion
    }
}
