﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System;
using System.Linq;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Grid.Data
{
    public class DataViews : BsnGrid
    {        
        #region Private Properties
        public BsnActions Action { get; set; }
        #endregion

        #region Constructor
        public DataViews(BsnApplication app) : base(app)
        {
            Init(BsnGrids.DataViews, "Key");
            EditForm = BsnForms.DataView;
            OptionsColumn.Field = "Key";
            KeyColumn.Field = "Name";

            OptionsColumn.Buttons.Add(new BsnButton("pencil") { GridEdit = true });
            OptionsColumn.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.Delete,
                    Key = "[cellvalue]"
                }
            }, "trash", "", true, ButtonTypes.LinkDanger)
            { Tooltip = "Delete" });

            var callback = new BsnCallback(App) {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.Bookmark
                }                
            };
            var bookmark = new BsnButton(callback, "fal fa-bookmark fa-fw");
            bookmark.CellPrepared += Bookmark_CellPrepared;
            OptionsColumn.Buttons.Add(bookmark);

            var keyButton = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Menu = BsnMenus.DataViews,
                Action = Action,
                Mode = BsnModes.Open,
                Id = "[cellvalue]"
            }, "fal fa-external-link-square", "[cellvalue]", false, ButtonTypes.Link);
            keyButton.CellPrepared += KeyButton_CellPrepared;
            KeyColumn.Buttons.Add(keyButton);

            Columns.Add(new BsnGridColumn() { Field = "Description" });
            Columns.Add(new BsnGridColumn() { Field = "DataSource", Caption = "Data" });
            var displayColumn = new BsnGridColumn() { Field = "Display", CustomTemplate = true };
            displayColumn.CellPrepared += DisplayColumn_CellPrepared;
            Columns.Add(displayColumn);
        }
        private void DisplayColumn_CellPrepared(object sender, EventArgs e)
        {
            var args = ((BsnGridEventArgs)e);
            switch (Action)
            {
                case BsnActions.Charts:
                case BsnActions.Default:
                    args.GridCell.Controls.Add(new LiteralControl(args.Column.Data.ToString()));
                    break;
                case BsnActions.Dashboards:                    
                case BsnActions.Reports:
                    args.GridCell.Controls.Add(new LiteralControl("[ XML Layout ]"));
                    break;
            }
        }
        #endregion

        #region Button Events        
        private void Bookmark_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                button.Active = App.VM.DV.GetDataView(key).Bookmarked;
                button.Icon = (button.Active) ? "fas fa-bookmark fa-fw" : "bookmark";
                button.Callback.Data.Key = key;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }

        private void KeyButton_CellPrepared(object sender, EventArgs e)
        {
            var args = ((BsnGridEventArgs)e);
            var key = args.KeyValue;
            var button = (BsnButton)sender;
            button.Callback.Action = Action;
        }

        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            switch (Action)
            {
                case BsnActions.Charts:
                case BsnActions.Default:
                    DataSource = App.VM.DV.Charts;
                    break;
                case BsnActions.Dashboards:
                    DataSource = App.VM.DV.Dashboards;
                    break;
                case BsnActions.Reports:
                    DataSource = App.VM.DV.Reports;
                    break;
            }
        }
        #endregion

        #region AddTools
        protected override void AddTools()
        {
            base.AddTools();

            var callback = App.Utils.GetKeyCallback(BsnModes.Add);
            var addButton = new BsnButton(callback, "plus-circle", "Add New", true, ButtonTypes.Primary);
            switch (Action)
            {
                case BsnActions.Default:
                case BsnActions.Charts:
                    addButton.Tooltip = "New Chart";
                    break;
                case BsnActions.Dashboards:
                    addButton.Tooltip = "New Dashboard";
                    break;
                case BsnActions.Reports:
                    addButton.Tooltip = "New Report";
                    break;
            }            
            Toolbar.Add(addButton);
        }
        #endregion

    }
}
