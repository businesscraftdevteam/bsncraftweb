﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Data;
using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace BsnCraft.Web.Common.Views.Grid.Base
{
    public class BsnGridFilter
    {
        #region Public Properties
        public string Name { get; set; } = "";
        public string Caption { get; set; } = "";
        public string Parent { get; set; } = "";
        public bool RequiresParent { get; set; }
        public bool MultiSelect { get; set; }
        public bool ShowCount { get; set; }
        public bool CanEdit { get; set; }
        public string KeyField { get; set; } = "";
        public string SelectedField { get; set; } = "";
        public string CaptionField { get; set; } = "";
        public string CountField { get; set; } = "";
        public string CountCaption { get; set; } = "";
        public string CountDisplayFormat { get; set; } = "";
        public object DataSource { get; set; }
        public bool AllowSort { get; set; } = true;

        public BsnCallback Callback { get; set; }
        #endregion

        #region Private Properties
        private BootstrapGridView Grid { get; set; }
        private BsnApplication App { get; set; }
        #endregion

        #region Constructor
        public BsnGridFilter(BsnApplication app, string name)
        {
            App = app;
            Name = name;
            Grid = new BootstrapGridView();
            Callback = new BsnCallback(App)
            {
                Data = new CallbackData
                {
                    Mode = BsnDataModes.Filter,
                    Key = Name
                }
            };
        }
        #endregion

        #region Load
        public void Load()
        {
            Grid.ID = Name;
            Grid.KeyFieldName = KeyField;
            Grid.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
            Grid.Settings.GridLines = GridLines.Horizontal;
            Grid.SettingsBehavior.AllowSelectByRowClick = true;
            Grid.SettingsBehavior.AllowSelectSingleRowOnly = !MultiSelect;

            Grid.SettingsBehavior.AllowEllipsisInText = true;
            Grid.SettingsResizing.ColumnResizeMode = ColumnResizeMode.NextColumn;                       

            if (MultiSelect && Grid.Columns["Select"] == null)
            {
                Grid.Columns.Add(new BootstrapGridViewCommandColumn()
                {
                    Name = "Select",
                    ShowSelectCheckbox = true,
                    SelectAllCheckboxMode = GridViewSelectAllCheckBoxMode.Page,
                    Width = Unit.Pixel(45)
                });
            }
            
            if (Grid.Columns[CaptionField] == null)
            {
                BootstrapGridViewDataColumn  CaptionCol = new BootstrapGridViewDataColumn()
                {
                    FieldName = CaptionField,
                    Caption = Caption,
                    Width = Unit.Percentage(70)
                };
                Grid.Columns.Add(CaptionCol);
                if (AllowSort) Grid.SortBy(CaptionCol, ColumnSortOrder.Ascending);
            }

            if (!string.IsNullOrEmpty(CountField))
            {
                if (Grid.Columns[CountField] == null)
                {
                    var column = new BootstrapGridViewTextColumn()
                    {
                        FieldName = CountField,
                        Caption = CountCaption,
                        Width = Unit.Percentage(30)
                    };
                    column.CssClasses.HeaderCell = "text-right";
                    column.CssClasses.DataCell = "small text-primary";
                    ((BootstrapTextBoxProperties)column.PropertiesEdit)
                        .DisplayFormatString = CountDisplayFormat;
                    Grid.Columns.Add(column);
                }
                Grid.Columns[CountField].Visible = ShowCount;
            }

            Grid.SettingsBehavior.AllowSort = AllowSort;
            Grid.DataSource = DataSource;
            Grid.DataBind();

            if (Grid.IsCallback || string.IsNullOrEmpty(SelectedField))
            {
                return;
            }

            var selectedKeys = new List<string>();
            Grid.Selection.BeginSelection();
            for (int i = Grid.VisibleStartIndex; i < Grid.VisibleRowCount; i++)
            {
                var selected = (bool)Grid.GetRowValues(i, SelectedField);
                if (selected)
                {
                    selectedKeys.Add((string)Grid.GetRowValues(i, KeyField));
                }
                Grid.Selection.SetSelection(i, selected);
            }

            Callback.Data.Object = selectedKeys;
            var selectJs = "function (s, e) { App.filterSelect(s, e, '" + Callback.ToString() + "'); }";
            Grid.ClientSideEvents.SelectionChanged = selectJs;
            Grid.ClientSideEvents.RowClick = "App.filterClick";
        }

        #endregion

        #region ToControl
        public BootstrapGridView ToControl()
        {
            return Grid;
        }
        #endregion
    }
}
