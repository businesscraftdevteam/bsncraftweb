﻿using DevExpress.Web;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Views.Grid.Base
{
    public class BsnGridTemplate : ITemplate
    {
        public BsnGridColumn Column { get; private set; }

        public BsnGridTemplate(BsnGridColumn column = null)
        {
            Column = column;
        }

        public void InstantiateIn(Control container)
        {
            /*
            var parent = (GridViewDataItemTemplateContainer)container;
            */
        }
    }
}
