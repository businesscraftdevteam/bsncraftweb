﻿using DevExpress.Web.Bootstrap;
using DevExpress.Web.Bootstrap.Internal;
using System;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Grid.Base
{
    public class BsnGridEventArgs : EventArgs
    {
        public BsnGridColumn Column { get; set; }
        public string KeyValue { get; set; }
        public BootstrapGridView GridView { get; set; }
        public BSGridViewTableDataCell GridCell { get; set; }
        public Control Control { get; set; }
        public string Result { get; set; }
        public bool IsVisible { get; set; }
        public bool Handled { get; set; }

        public BsnGridEventArgs(BsnGridColumn column, string keyval, object sender = null, object extra = null)
        {
            Column = column;
            KeyValue = keyval;
            if (sender is BootstrapGridView grid)
            {
                GridView = grid;
            }            
            if (extra is BSGridViewTableDataCell gridCell)
            {
                GridCell = gridCell;
            }
        }
    }
}
