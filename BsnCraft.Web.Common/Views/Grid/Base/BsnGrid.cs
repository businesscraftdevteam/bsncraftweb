﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Settings.Base;
using BsnCraft.Web.Common.Views.Base;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Data;
using DevExpress.Export;
using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using DevExpress.Web.Bootstrap.Internal;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace BsnCraft.Web.Common.Views.Grid.Base
{
    #region Public Enums
    public enum BsnGrids
    {
        Activities,
        Contracts,
        Leads,
        Customers,
        Contacts,
        JobInfo,
        DisplayInfo,
        Documents,
        Headings,
        Estimate,
        DealDocs,
        ImportantDocs,
        Events,
        Variations,
        Jobs,
        PurchaseOrders,        
        DataViews,
        CompSettings,
        AreaPrices,
        DealSubmit,
        ItemSelections
    }
    #endregion

    public abstract class BsnGrid : BsnView
    {
        #region Private Properties
        public BsnApplication App { get; private set; }
        private List<string> CurrentGroups { get; set; }
        private BsnButton ToggleGrouping { get; set; }
        private BsnButton ToggleNotes { get; set; }
        private BsnButton ExpandAll { get; set; }
        private BsnButton CollapseAll { get; set; }
        private Control EditControl { get; set; }
        #endregion

        #region Public Properties        
        //TODO: Consider moving some optional settings to user settings
        public BsnGrids Key { get; private set; }
        public string Title { get; protected set; } = "";
        public string KeyField { get; protected set; } = "";
        public string GroupField { get; set; } = "";
        public string NotesField { get; set; } = "";
        public object DataSource { get; set; }
        public int EditRowIndex { get; set; } = -1;
        public BsnForms EditForm { get; set; } = BsnForms.None;
        public bool InlineEdit { get; set; } = false;
        public bool ShowSearch { get; set; } = true;
        public bool ShowPager { get; set; } = false;
        public bool ShowStripes { get; set; } = false;
        public bool ShowLines { get; set; } = false;
        public bool ShowColumnsButton { get; set; } = true;
        public bool ShowExportButton { get; set; } = false;
        public bool ShowDetails { get; set; } = false;
        public bool ShowGroupFooter { get; set; } = false;
        public bool SolidGroups { get; set; } = false;
        public bool AllowSelect { get; set; } = false;        
        public bool ClientSideSelect { get; set; } = true;
        public bool KeyOptionChild { get; set; } = false;
        public bool StartEdit { get; set; } = false;
        public bool AllowSort { get; set; } = true;
        public bool OnlyBookmarks { get; set; } = false;
        public bool ShowOptions { get { return (!string.IsNullOrEmpty(OptionsColumn.Field)); } }
        public bool ShowKey { get { return (!string.IsNullOrEmpty(KeyColumn.Field)); } }
        public List<BsnView> Toolbar { get; private set; }
        public List<BsnGridColumn> Columns { get; private set; }
        public List<GridViewFormatConditionHighlight> FormatConditions { get; private set; }
        public List<string> SelectedKeys { get; set; }
        #endregion

        #region Options
        private BsnGridColumn optionsColumn;
        public BsnGridColumn OptionsColumn
        {
            get
            {
                if (optionsColumn == null)
                {
                    optionsColumn = new BsnGridColumn()
                    {
                        Name = "BsnGridOptions",
                        Type = ColumnTypes.Data,
                        Width = 1,
                        ExportWidth = 1
                    };
                    optionsColumn.HeaderTemplate = new BsnGridTemplate(optionsColumn);
                }
                return optionsColumn;
            }
        }
        #endregion

        #region Key
        //Key field will not be able to be hidden in column selection
        private BsnGridColumn keyColumn;
        public BsnGridColumn KeyColumn
        {
            get
            {
                if (keyColumn == null)
                {
                    keyColumn = new BsnGridColumn()
                    {
                        Name = "BsnGridKey",
                        Type = ColumnTypes.Data                        
                    };
                }
                return keyColumn;
            }
        }
        #endregion

        #region UID
        public override string UID
        {
            get
            {
                return Key.ToString() + App.Settings.GridUID;
            }
        }
        #endregion

        #region Settings
        private GridSettings settings;
        public GridSettings Settings
        {
            get
            {
                if (settings != null)
                    return settings;

                if (App.Settings.User.Grids.ContainsKey(Key))
                {
                    settings = App.Settings.User.Grids[Key];
                }
                else
                {
                    settings = new GridSettings();
                }
                return settings;
            }
        }
        #endregion

        #region Constructior
        public BsnGrid(BsnApplication app)
        {
            App = app;
            Toolbar = new List<BsnView>();
            Columns = new List<BsnGridColumn>();
            SelectedKeys = new List<string>();
            FormatConditions = new List<GridViewFormatConditionHighlight>();
            CurrentGroups = new List<string>();

            ExpandAll = new BsnButton(new BsnCallback(App)
            {
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.ExpandAll
                }
            },
            "plus", "", true, ButtonTypes.Outline)
            { Tooltip = "Expand All" };

            CollapseAll = new BsnButton(new BsnCallback(App)
            {
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.CollapseAll
                }
            },
            "minus", "", true, ButtonTypes.Outline)
            { Tooltip = "Collapse All" };

            ToggleGrouping = new BsnButton(new BsnCallback(App)
            {
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.ShowGrouping
                }
            },
            "layer-group", "", true, ButtonTypes.Outline)
            { Tooltip = "Show/Hide Groups" };

            ToggleNotes = new BsnButton(new BsnCallback(App)
            {
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.ShowNotes
                }
            },
            "comment-alt", "", true, ButtonTypes.Outline)
            { Tooltip = "Show/Hide Notess" };
        }
        #endregion

        #region Init
        public virtual void Init(BsnGrids key, string keyField, string title = "", string notesField = "")
        {
            Key = key;            
            KeyField = keyField;
            Title = (!string.IsNullOrEmpty(title)) ? title : key.ToString();
            NotesField = notesField;

            ExpandAll.Callback.Data.Key = Key.ToString();
            CollapseAll.Callback.Data.Key = Key.ToString();
            ToggleGrouping.Callback.Data.Key = Key.ToString();
            ToggleNotes.Callback.Data.Key = Key.ToString();
        }
        #endregion

        #region Reset
        public virtual void Reset()
        {
            OnlyBookmarks = false;
            ShowExportButton = false;
        }
        #endregion
               
        #region ColumnVisible
        public bool ColumnVisible(string column)
        {
            if (Settings.Columns.ContainsKey(column))
            {
                return Settings.Columns[column].Visible;
            }
            return true;
        }
        #endregion

        #region GroupExpanded
        public bool GroupExpanded(string group)
        {
            if (Settings.Groups.ContainsKey(group))
            {
                return Settings.Groups[group];
            }
            return true;
        }
        #endregion

        #region IsBookmark
        public bool IsBookmark(string recordKey, string field = "")
        {
            if (string.IsNullOrEmpty(field))
            {
                field = KeyField;
            }
            if (Settings.Bookmarks.ContainsKey(field))
            {
                var bookmarks = Settings.Bookmarks[field];
                return bookmarks.Any(p => p.RecordKey.Equals(recordKey));
            }
            return false;
        }
        #endregion

        #region Bookmarks
        public List<string> Bookmarks(string field = "")
        {
            if (string.IsNullOrEmpty(field))
            {
                field = KeyField;
            }
            if (Settings.Bookmarks.ContainsKey(field))
            {
                var bookmarks = Settings.Bookmarks[field];
                return bookmarks.Select(p => p.RecordKey).ToList();
            }
            return new List<string>();
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            return Grid;
        }
        #endregion

        #region Grid
        private BootstrapGridView grid;
        public BootstrapGridView Grid
        {
            get
            {
                if (grid == null)
                {
                    grid = new BootstrapGridView
                    {
                        ClientInstanceName = Key.ToString()
                    };
                    grid.CssClasses.Control = "mb-3";
                    grid.ClientSideEvents.Init = "App.update";
                    grid.ClientSideEvents.EndCallback = "App.update";
                    grid.SettingsSearchPanel.Visible = true;
                    grid.SettingsSearchPanel.ShowApplyButton = false;
                    grid.EnableRowsCache = false;
                    grid.EnableViewState = false;
                    grid.SettingsPager.ShowDisabledButtons = false;
                    grid.SettingsPager.EnableAdaptivity = true;
                    grid.SettingsDataSecurity.AllowEdit = true;
                    grid.SettingsLoadingPanel.Mode = GridViewLoadingPanelMode.Disabled;
                    grid.Settings.ShowGroupPanel = false;
                    grid.Settings.ShowTitlePanel = false;
                    grid.Templates.PreviewRow = new BsnGridTemplate();
                    grid.Templates.DetailRow = new BsnGridTemplate();
                    grid.Templates.GroupFooterRow = new BsnGridTemplate();
                    grid.Templates.EditForm = new BsnGridTemplate();
                    grid.SettingsSearchPanel.CustomEditorID = "BsnCraft";

                    grid.PreviewFieldName = NotesField;
                }
                return grid;
            }
        }
        #endregion

        #region Load
        public virtual void Load()
        {
            Grid.SearchPanelEditorInitialize += SearchPanelEditorInitialize;
            Grid.PageSizeChanged += PageSizeChanged;
            Grid.CustomUnboundColumnData += CustomUnboundColumnData;
            Grid.HtmlDataCellPrepared += HtmlDataCellPrepared;
            Grid.HtmlRowPrepared += HtmlRowPrepared;
            Grid.HtmlRowCreated += HtmlRowCreated;
            Grid.DetailRowGetButtonVisibility += DetailButton;
            
            //TODO enable context mnenu
            //grid.FillContextMenuItems += Grid_FillContextMenuItems;
            //grid.ContextMenuItemClick += Grid_ContextMenuItemClick;
            //grid.ClientSideEvents.ContextMenuItemClick = "function(s, e) { OnContextMenuItemClick(s, e);}";

            Setup();
            DataBind();
            AddTools(); 
            
            //Note: Search panel gets created in Grid.DataBind()
            Grid.DataSource = DataSource;
            Grid.DataBind();
            CheckGrouping();
            CheckEditing();
            CheckSelection();
        }        
        #endregion

        #region Setup
        public virtual void Setup()
        {
            //Refetch
            settings = null;
            var cbData = App.Callback.Data;
            StartEdit = (cbData != null && cbData.Mode == BsnDataModes.StartEdit);
            if (StartEdit)
            {
                EditRowIndex = App.Callback.Data.Key.ToInt();
                Grid.FocusedRowIndex = EditRowIndex;
            }

            if (Grid.ID == UID)
            {
                return;
            }
            
            //TODO: consider making some settings optional in user/profile settings
            //TODO: consider saving column widths, etc. in future

            Grid.ID = UID;
            Grid.KeyFieldName = KeyField;
            Grid.SettingsBootstrap.Striped = ShowStripes;
            Grid.Settings.GridLines = ShowLines ? GridLines.Both : GridLines.Horizontal;
            Grid.SettingsDetail.ShowDetailRow = ShowDetails;
            if (ShowPager)
            {
                Grid.SettingsPager.AlwaysShowPager = !OnlyBookmarks;
                Grid.SettingsPager.PageSizeItemSettings.Visible = !OnlyBookmarks;
                Grid.SettingsPager.Visible = !OnlyBookmarks;
                Grid.SettingsPager.PageSize = OnlyBookmarks ? 10 :
                    App.Settings.UserSetting(UserSettingsTypes.PageSize).DefaultInt(10);
            }
            else
            {
                Grid.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
            }

            grid.SettingsBehavior.AllowSort = AllowSort;
            
            if (Columns.Any(p => !p.Child.IsNull()))
            {
                Grid.Columns.Clear();
            }
            
            if (!string.IsNullOrEmpty(GroupField))
            {
                if (!Columns.Any(p => p.Field.Equals(GroupField)))
                {
                    var group = new BootstrapGridViewDataColumn()
                    {
                        FieldName = GroupField,
                        ExportWidth = 1,
                        Visible = false
                    };
                    Grid.Columns.Add(group);
                }
                Grid.Settings.ShowGroupFooter = ShowGroupFooter ?
                    GridViewGroupFooterMode.VisibleIfExpanded :
                    GridViewGroupFooterMode.Hidden;
            }

            if (!string.IsNullOrEmpty(GroupField) && Settings.ShowGroups)
            {
                Grid.Settings.ShowGroupFooter = ShowGroupFooter ?
                    GridViewGroupFooterMode.VisibleIfExpanded :
                    GridViewGroupFooterMode.Hidden;
            }
            
            if (!string.IsNullOrEmpty(NotesField))
            {                
                if (Settings.ShowNotes)
                {
                    Grid.Settings.ShowPreview = true;
                }
                else
                {
                    Grid.Settings.ShowPreview = false;
                    Grid.PreviewFieldName = string.Empty;
                }
            }

            if (InlineEdit)
            {
                Grid.SettingsEditing.Mode = GridViewEditingMode.Inline;
            }

            if (AllowSelect)
            {
                AddSelectColumnToGrid();
            }

            if (ShowOptions && Grid.Columns[OptionsColumn.Name] == null)
            {
                AddColumnToGrid(OptionsColumn);
            }
            if (ShowKey && Grid.Columns[KeyColumn.Name] == null)
            {
                if (KeyOptionChild)
                {
                    AddColumnToGrid(KeyColumn, OptionsColumn);
                }
                else
                {
                    AddColumnToGrid(KeyColumn);
                }
            }
            foreach (var col in Columns.OrderBy(p => p.Sort))
            {
                AddColumnToGrid(col);
                AddColumnToGrid(col.Child, col);
            }
            foreach (var f in FormatConditions)
            {
                Grid.FormatConditions.Add(f);
            }
        }
        #endregion

        #region AddColumnToGrid
        void AddSelectColumnToGrid()
        {
            var column = (BootstrapGridViewCommandColumn)Grid.Columns["Select"];
            if (column == null)
            {
                column = new BootstrapGridViewCommandColumn
                {
                    Name = "Select",
                    Width = 0,
                    ShowSelectCheckbox = true,
                    SelectAllCheckboxMode = GridViewSelectAllCheckBoxMode.AllPages
                };
                Grid.Columns.Add(column);
            }
        }
        #endregion

        #region AddColumnToGrid
        void AddColumnToGrid(BsnGridColumn col, BsnGridColumn parent = null)
        {
            if (col == null)
            {
                return;
            }
            
            var column = (BootstrapGridViewDataColumn)Grid.Columns[col.Name];
            if (column != null && column.Name != col.Name)
            {
                //Allow column used by key/options to be added manually
                column = null;
            }
            if (column == null)
            {
                switch (col.Type)
                {
                    case ColumnTypes.Data:
                        column = new BootstrapGridViewDataColumn();
                        break;
                    case ColumnTypes.Check:
                        column = new BootstrapGridViewCheckColumn();
                        break;
                    case ColumnTypes.Text:
                        column = new BootstrapGridViewTextColumn();
                        ((BootstrapTextBoxProperties)column.PropertiesEdit).EncodeHtml = false;
                        ((BootstrapTextBoxProperties)column.PropertiesEdit)
                            .DisplayFormatString = col.DisplayFormat;
                        break;
                }
                switch (col.Justify)
                {
                    case ColumnJustify.Center:
                        column.CssClasses.HeaderCell += " text-center";
                        column.CssClasses.DataCell += " text-center";
                        column.CssClasses.EditCell += " text-center";
                        break;
                    case ColumnJustify.Right:
                        column.CssClasses.HeaderCell += " text-right";
                        column.CssClasses.DataCell += " text-right";
                        column.CssClasses.EditCell += " text-right";
                        break;
                }
                switch (col.Align)
                {
                    case ColumnAlign.Top:
                        column.CssClasses.HeaderCell += " align-top";
                        column.CssClasses.DataCell += " align-top";
                        column.CssClasses.EditCell += " align-top";
                        break;
                    case ColumnAlign.Bottom:
                        column.CssClasses.HeaderCell += " align-bottom";
                        column.CssClasses.DataCell += " align-bottom";
                        column.CssClasses.EditCell += " align-bottom";
                        break;
                }
                if (col.NoWrap)
                {
                    column.CssClasses.HeaderCell += " text-nowrap";
                    column.CssClasses.DataCell += " text-nowrap";
                    column.CssClasses.EditCell += " text-nowrap";
                }
                switch (col.Collapse)
                {
                    case ColumnCollapse.Small:
                        column.CssClasses.HeaderCell += " d-none d-md-table-cell";
                        column.CssClasses.DataCell += " d-none d-md-table-cell";
                        column.CssClasses.EditCell += " d-none d-md-table-cell";
                        break;
                }
                if (col.HeaderTemplate != null)
                {
                    column.HeaderTemplate = col.HeaderTemplate;
                }
                column.Name = col.Name;
                column.FieldName = col.Field;
                column.Caption = col.Caption;
                column.SortIndex = col.SortIndex;

                switch (col.SortOrder)
                {
                    case ColumnSortOrder.Ascending:
                        column.SortOrder = ColumnSortOrder.Ascending;
                        break;
                    case ColumnSortOrder.Descending:
                        column.SortOrder = ColumnSortOrder.Descending;
                        break;
                    default:
                        column.SortOrder = ColumnSortOrder.None;
                        break;
                }

                column.Settings.SortMode = col.SortMode;

                if (column.Width != 0)
                {
                    column.Width = col.Width;
                }

                if (column.ExportWidth != 0)
                {
                    column.ExportWidth = col.ExportWidth;
                }

                Grid.Columns.Add(column);

                if (col.CustomTemplate || col.Buttons.Count > 0)
                {
                    column.FieldName = "Unbound." + col.Name;
                    column.UnboundType = UnboundColumnType.String;
                    column.DataItemTemplate = new BsnGridTemplate(col);
                }
            }

            column.Visible = col.Visible;
            if (col.Visible)
            {
                column.Visible = ColumnVisible(column.Name);
            }

            if (column.Visible && parent != null && parent.Visible && parent.ShowChild)
            {
                var parentColumn = Grid.Columns[parent.Name];
                parentColumn.Columns.Add(column);
            }
        }
        #endregion

        #region Check Selecttion
        private void CheckSelection()
        {
            if (!AllowSelect) return;
            
            Grid.Selection.BeginSelection();
            Grid.Selection.UnselectAll();
            foreach (var item in SelectedKeys)
            {
                Grid.Selection.SetSelectionByKey(item, true);
            }
            grid.Selection.EndSelection();


            Grid.SettingsBehavior.AllowSelectByRowClick = true;
            Grid.SettingsBehavior.AllowSelectSingleRowOnly = false;
            if (!ClientSideSelect) return;

            var callback = new BsnCallback(App)
            {
                Key = Key.ToString(),
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.SelectedKeys
                }
            };
            var selectJs = "function (s, e) { App.gridSelect(s, e, '" + callback.ToString() + "'); }";
            Grid.ClientSideEvents.SelectionChanged = selectJs;
            
        }
        #endregion

        #region Check Grouping
        private void CheckGrouping()
        {
            if (string.IsNullOrEmpty(GroupField) || !Settings.ShowGroups)
            {
                return;
            }
            Grid.GroupBy(Grid.Columns[GroupField]);
            CurrentGroups.Clear();

            for (int i = Grid.VisibleStartIndex; i < Grid.VisibleRowCount; i++)
            {
                if (Grid.IsGroupRow(i))
                {
                    var groupVal = Grid.GetRowValues(i, GroupField);
                    var group = groupVal.ToString();
                    CurrentGroups.Add(group);
                    if (GroupExpanded(group))
                    {
                        Grid.ExpandRow(i);
                    }
                    else
                    {
                        Grid.CollapseRow(i);
                    }
                }
            }
        }
        #endregion

        #region Check Editing
        private void CheckEditing()
        {
            if (EditForm == BsnForms.None)
            {
                return;
            }
            EditControl = null;
            if ((App.InCallback && !StartEdit) || Grid.IsCallback)
            {
                EditRowIndex = -1;
                Grid.CancelEdit();
            }
            if (EditRowIndex < 0)
            {
                return;
            }
            var row = Grid.GetRow(EditRowIndex);
            if (row == null)
            {
                return;
            }
            Grid.StartEdit(EditRowIndex);
            var editForm = App.Forms.Get(EditForm);
            if (editForm != null)
            {
                PrepareEditForm(editForm, row);
                EditControl = editForm.ToControl();
                Grid.Controls.Add(EditControl);
            }            
        }
        #endregion

        #region PrepareEditForm
        protected virtual void PrepareEditForm(BsnForm editForm, object row, bool readOnly = false)
        {
            editForm.Load(row, readOnly, BsnFormTypes.InGrid, Grid);
        }
        #endregion
        
        #region AddColumnToggle
        void AddColumnToggle(BsnGridColumn column, BsnButtonGroup columnsButton)
        {
            if (column == null || !column.Visible)
            {
                return;
            }

            var visible = ColumnVisible(column.Field);
            var callback = new BsnCallback(App)
            {
                Mode = BsnModes.ColumnVisible,
                Key = Key.ToString(),
                Id = column.Field,
                Value = (!visible).ToString()
            };
            var icon = visible ? "check-square" : "square";
            var caption = column.Caption.Replace("<br />", "");
            columnsButton.Buttons.Add(new BsnButton(callback, icon, caption));
        }
        #endregion

        #region SearchPanel
        private void SearchPanelEditorInitialize(object sender, ASPxGridViewSearchPanelEditorEventArgs e)
        {
            try
            {
                if (!(e.Editor is BootstrapButtonEdit))
                {
                    return;
                }

                var searchBox = (BootstrapButtonEdit)e.Editor;
                if (!(searchBox.Parent is BSGridViewSearchPanel))
                {
                    return;
                }

                //var searchControl = (InternalWebControl)searchBox.Parent;
                var searchPanel = (BSGridViewSearchPanel)searchBox.Parent;
                searchPanel.Controls.Clear();
                var flexPanel = new HtmlGenericControl("div");
                flexPanel.Attributes["class"] = "d-flex";
                searchPanel.Controls.Add(flexPanel);

                if (Toolbar.Count > 0)
                {
                    var toolbar = new HtmlGenericControl("div");
                    toolbar.Attributes["class"] = "btn-group";
                    flexPanel.Controls.Add(toolbar);
                    foreach (var tool in Toolbar)
                    {
                        toolbar.Controls.Add(tool.ToControl());
                    }
                    var divider = new HtmlGenericControl("span");
                    divider.Attributes["class"] = "dashhead-toolbar-divider d-inline-block";
                    flexPanel.Controls.Add(divider);
                }

                var searchContainer = new HtmlGenericControl("div");
                searchContainer.Attributes["class"] = "w-100";
                flexPanel.Controls.Add(searchContainer);

                if (ShowSearch)
                {
                    searchContainer.Controls.Add(searchBox);

                    var divider = new HtmlGenericControl("span");
                    divider.Attributes["class"] = "dashhead-toolbar-divider d-inline-block";
                    flexPanel.Controls.Add(divider);
                }

                var settingsBar = new HtmlGenericControl("div");
                settingsBar.Attributes["class"] = "float-right btn-group";
                if (!string.IsNullOrEmpty(GroupField) && Settings.ShowGroups)
                {
                    ExpandAll.Callback.Data.Object = CurrentGroups;
                    settingsBar.Controls.Add(ExpandAll.ToControl());

                    CollapseAll.Callback.Data.Object = CurrentGroups;
                    settingsBar.Controls.Add(CollapseAll.ToControl());
                }

                settingsBar.Attributes["class"] = "float-right btn-group";
                if (!string.IsNullOrEmpty(GroupField))
                {
                    ToggleGrouping.Active = Settings.ShowGroups;
                    ToggleGrouping.Tooltip = Settings.ShowGroups ? "Hide Groups" : "Show Groups";
                    ToggleGrouping.Callback.Data.Object = !Settings.ShowGroups;
                    settingsBar.Controls.Add(ToggleGrouping.ToControl());
                }
                
                if (!string.IsNullOrEmpty(NotesField))
                {
                    ToggleNotes.Active = Settings.ShowNotes;
                    ToggleNotes.Tooltip = Settings.ShowNotes ? "Hide Notes" : "Show Notes";
                    ToggleNotes.Callback.Data.Object = !Settings.ShowNotes;
                    settingsBar.Controls.Add(ToggleNotes.ToControl());
                }

                if (ShowExportButton)
                {
                    settingsBar.Controls.Add(ExportButtons.ToControl());                    
                }

                if (ShowColumnsButton)
                {
                    var columnsButton = new BsnButtonGroup(new BsnButton("columns", "", true, ButtonTypes.Outline)
                    {
                        Tooltip = "Toggle Columns"
                    }, true);
                    foreach (var column in Columns.OrderBy(p => p.Sort))
                    {
                        AddColumnToggle(column, columnsButton);
                        AddColumnToggle(column.Child, columnsButton);
                    }
                    settingsBar.Controls.Add(columnsButton.ToControl());
                }
                flexPanel.Controls.Add(settingsBar);
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region Row Prepared
        protected virtual void HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            try
            { 
                if (e.KeyValue == null)
                {
                    return;
                }            
                var key = e.KeyValue.ToString();
                var hasGroups = (!string.IsNullOrEmpty(GroupField) && Settings.ShowGroups);
                var i = hasGroups ? 1 : 0;
                switch (e.RowType)
                {
                    case GridViewRowType.Preview:
                    case GridViewRowType.EditForm:
                    case GridViewRowType.Detail:
                        if (ShowDetails)
                        {
                            i++;
                        }
                        break;
                }
                var cell = e.Row.Cells[i];
                switch (e.RowType)
                {
                    case GridViewRowType.Group:
                        var collapseCell = e.Row.Cells[0];                        
                        var group = e.GetValue(GroupField).ToString();
                        var expanded = GroupExpanded(group);
                        var icon = expanded ? "fas fa-angle-down fa-fw" : "fas fa-angle-right fa-fw";
                        var groupList = new List<string>() { group };
                        var button = (HtmlGenericControl)new BsnButton(new BsnCallback(App)
                        {
                            Data = new CallbackData()
                            {
                                Mode = BsnDataModes.GroupToggle,
                                Key = Key.ToString(),
                                Value = (!expanded).ToString(),
                                Object = groupList,
                            }
                        },
                        icon, "", false, ButtonTypes.Link).ToControl();
                        button.Attributes["class"] += SolidGroups ? " text-white btn-lg" : " btn-lg";
                        collapseCell.CssClass = SolidGroups ? "bg-primary text-white" : "text-primary";
                        collapseCell.Controls.Clear();
                        collapseCell.Controls.Add(button);
                        cell.Controls.Clear();
                        GroupPanel(cell, group, key, e);
                        break;
                    case GridViewRowType.GroupFooter:
                        GroupFooterPanel(cell, e.GetValue(GroupField).ToString(), key, e);
                        break;
                    case GridViewRowType.Detail:
                        DetailPanel(cell, key);
                        break;
                    case GridViewRowType.Preview:
                        var notes = e.GetValue(NotesField).ValueOrDefault(string.Empty);
                        PreviewPanel(cell, e.KeyValue.ToString(), notes);
                        break;
                    case GridViewRowType.EditForm:
                        if (EditControl != null && cell != null)
                        {
                            cell.Controls.Add(EditControl);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region Row Created
        protected virtual void HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                if (e.RowType == GridViewRowType.Preview)
                {
                    if (!string.IsNullOrEmpty(NotesField))
                    {
                        e.Row.Visible = !string.IsNullOrEmpty(e.GetValue(NotesField).ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
}
        #endregion

        #region Custom Columns
        private void CustomUnboundColumnData(object sender, BootstrapGridViewColumnDataEventArgs e)
        {
            try
            {
                if (e.Column.DataItemTemplate is BsnGridTemplate template)
                {
                    var column = template.Column;
                    column.Data = e.GetListSourceFieldValue(column.Field);
                    e.Value = column;
                }
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region Cell Prepared
        protected virtual void HtmlDataCellPrepared(object sender, BootstrapGridViewTableDataCellEventArgs e)
        {
            try
            {
                if (e.CellValue == null || e.KeyValue == null)
                {
                    return;
                }

                if (e.CellValue is BsnGridColumn column && e.Cell is BSGridViewTableDataCell cell)
                {
                    cell.Controls.Clear();
                    if (column.CustomTemplate)
                    {
                        var args = new BsnGridEventArgs(column, e.KeyValue.ToString(), sender, cell);
                        column.OnCellPrepared(args);
                    }
                    else
                    {
                        RenderButtons(column, e.KeyValue.ToString(), sender, cell);
                    }
                }
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region Page Size
        protected void PageSizeChanged(object sender, EventArgs e)
        {
            try
            {
                App.Settings.UserSetting(UserSettingsTypes.PageSize, Grid.SettingsPager.PageSize.ToString());
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region Render Buttons
        private void RenderButtons(BsnGridColumn column, string key, object sender, BSGridViewTableDataCell cell)
        {
            var group = new HtmlGenericControl("div");
            if (column.Buttons.Count > 1)
            {
                group.Attributes["class"] = "btn-group";
            }
            cell.Controls.Add(group);
            foreach (var view in column.Buttons)
            {
                if (view is BsnButton button)
                {
                    if (button.GridEdit)
                    {
                        button.SetCallback(new BsnCallback(App)
                        {
                            Data = new CallbackData()
                            {
                                Mode = BsnDataModes.StartEdit,
                                Key = cell.VisibleIndex.ToString()
                            }
                        });
                        button.Tooltip = "Quick Edit";
                        group.Controls.Add(button.ToControl());
                        continue;
                    }

                    button.Data = column.Data;
                    var args = new BsnGridEventArgs(column, key, sender, cell);
                    button.OnCellPrepared(args);
                    if (!button.Visible)
                    {
                        continue;
                    }
                    if (!args.Handled)
                    {
                        button.DataBind(App, column.Data.ToString());
                    }
                    group.Controls.Add(button.ToControl());
                    button.Refresh();
                }
                if (view is BsnButtonGroup btnGroup)
                {
                    foreach (var groupButton in btnGroup.Buttons)
                    {
                        if (groupButton.GridEdit)
                        {
                            groupButton.SetCallback(new BsnCallback(App)
                            {
                                Data = new CallbackData()
                                {
                                    Mode = BsnDataModes.StartEdit,
                                    Key = cell.VisibleIndex.ToString()
                                }
                            });
                            groupButton.Tooltip = "Quick Edit";
                            continue;
                        }

                        groupButton.Data = column.Data;
                        var args = new BsnGridEventArgs(column, key, sender, cell);
                        groupButton.OnCellPrepared(args);
                        if (!args.Handled)
                        {
                            groupButton.DataBind(App, column.Data.ToString());
                        }
                    }
                    group.Controls.Add(btnGroup.ToControl());
                    foreach (var groupButton in btnGroup.Buttons)
                    {
                        groupButton.Refresh();
                    }
                }
            }
        }
        #endregion
        
        #region DetailButton
        protected virtual void DetailButton(object sender, ASPxGridViewDetailRowButtonEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion        

        #region DataBind
        public virtual void DataBind()
        {
        }
        #endregion

        #region AddTools
        protected virtual void AddTools()
        {
            Toolbar.Clear();
        }
        #endregion

        #region PreviewPanel
        protected virtual void PreviewPanel(TableCell container, string key, string notes)
        {
            container.Controls.Clear();
            if (string.IsNullOrEmpty(notes))
            {
                return;
            }
            var article = new HtmlGenericControl("article")
            {
                InnerHtml = notes.ToHtml()
            };
            var wrap = new HtmlGenericControl("div");
            wrap.Attributes["class"] = "row";
            var notesIcon = new HtmlGenericControl("span");
            notesIcon.Attributes["class"] = "text-secondary fal fa-comment-alt fa-fw p-2";
            var notesIconContainer = new HtmlGenericControl("div");
            notesIconContainer.Attributes["class"] = "col-1 text-right";
            notesIconContainer.Controls.Add(notesIcon);
            wrap.Controls.Add(notesIconContainer);
            var articleContainer = new HtmlGenericControl("div");
            articleContainer.Attributes["class"] = "col-11 pt-1 pb-3";
            articleContainer.Controls.Add(article);
            wrap.Controls.Add(articleContainer);
            container.Controls.Add(wrap);
        }
        #endregion

        #region GroupPanel
        protected virtual void GroupPanel(TableCell container, string group, string key, ASPxGridViewTableRowEventArgs e)
        {
            //var tableRow = (BSGridViewTableGroupRow)e.Row;            
            container.CssClass = SolidGroups ? "h6 bg-primary text-white" : "h6 text-primary";            
            container.Controls.Add(new LiteralControl(group));
        }
        #endregion

        #region GroupFooterPanel
        protected virtual void GroupFooterPanel(TableCell container, string group, string key, ASPxGridViewTableRowEventArgs e)
        {
            container.Controls.Clear();
        }
        #endregion

        #region DetailPanel
        protected virtual void DetailPanel(TableCell container, string key)
        {
        }
        #endregion
        
        #region ExportButtons
        public BsnButtonGroup ExportButtons
        {
            get
            {
                var exportButtons = new BsnButtonGroup(new BsnButton("file-export", "", true, ButtonTypes.Outline) { Tooltip = "Export Grid" }, true);
                exportButtons.Buttons.Add(new BsnButton(ExportCallback("PDF"), "file-pdf", "PDF", false));
                exportButtons.Buttons.Add(new BsnButton(ExportCallback("XLS"), "file-excel", "XLS", false));
                exportButtons.Buttons.Add(new BsnButton(ExportCallback("XLSX"), "file-excel", "XLSX", false));
                exportButtons.Buttons.Add(new BsnButton(ExportCallback("CSV"), "file-alt", "CSV", false));
                exportButtons.Buttons.Add(new BsnButton(ExportCallback("RTF"), "file-word", "RTF", false));
                return exportButtons;
            }
        }

        private BsnCallback ExportCallback(string value)
        {
            var callback = App.Utils.GetModeCallback(BsnModes.ExportGrid);
            callback.Value = value.ToLower();   //Looks prettier
            return callback;
        }

        #endregion

        #region Context Menu
        //private void Grid_ContextMenuItemClick(object sender, BootstrapGridViewContextMenuItemClickEventArgs e)
        //{
        //    var gridView = (ASPxGridView)sender;
        //    switch (e.Item.Name)
        //    {
        //        case "CustomExportToXLS":
        //            gridView.ExportXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });
        //            break;
        //    }
        //}

        //private void Grid_FillContextMenuItems(object sender, BootstrapGridViewContextMenuEventArgs e)
        //{
        //    if (e.MenuType == GridViewContextMenuType.Rows)
        //    {
        //        var item = e.CreateItem("Export", "Export");
        //        item.BeginGroup = true;
        //        e.Items.Insert(e.Items.IndexOfCommand(GridViewContextMenuCommand.Refresh), item);
        //        item.Items.Add(e.CreateItem(GridViewContextMenuCommand.ExportToPdf));
        //        item.Items.Add(e.CreateItem(GridViewContextMenuCommand.ExportToRtf));
        //        item.Items.Add(e.CreateItem(GridViewContextMenuCommand.ExportToXls));
        //        item.Items.Add("Custom Export to XLS(WYSIWYG)", "CustomExportToXLS");
        //    }
        //}
        #endregion

    }
}
