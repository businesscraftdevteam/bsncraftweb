﻿using BsnCraft.Web.Common.Classes;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsnCraft.Web.Common.Views.Grid.Base
{
    public class GridManager
    {
        #region Grids - Add here
        private List<BsnGrid> grids;
        private List<BsnGrid> Grids
        {
            get
            {
                if (grids == null || grids.Count == 0)
                {
                    grids = new List<BsnGrid>()
                    {
                        new Customers.Customers(App),
                        new Customers.Contacts(App),
                        new Contracts.Activities(App),
                        new Contracts.Leads(App),
                        new Contracts.Contracts(App),                        
                        new Contracts.Documents(App),
                        new Contracts.Events(App),
                        new Contracts.ImportantDocs(App),
                        new Contracts.Variations(App),
                        new Contracts.DealDocs(App),
                        new Contracts.DisplayInfo(App),
                        new Jobs.Jobs(App),
                        new Jobs.JobInfo(App),
                        new Jobs.Headings(App),
                        new Jobs.Estimate(App),
                        new Jobs.AreaPrices(App),
                        new Jobs.PurchaseOrders(App),
                        new Data.DataViews(App),
                        new Admin.CompSettings(App),
                        new Admin.DealSubmit(App),
                        new Admin.ItemSelections(App)
                    };
                }
                return grids;
            }
        }
        #endregion

        #region Private Properties
        //private Logger Log { get; set; }
        private BsnApplication App { get; set; }        
        #endregion

        #region Constructor
        public GridManager(BsnApplication app)
        {
            //Log = LogManager.GetLogger(GetType().FullName);
            App = app;
        }
        #endregion

        #region Init
        public void Init()
        {
            Grids.Clear();
        }
        #endregion

        #region Get
        public BsnGrid Get(BsnGrids view)
        {
            return Grids.Where(p => p.Key.Equals(view)).FirstOrDefault();
        }
        #endregion
    }
}
