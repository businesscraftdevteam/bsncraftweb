﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Base;
using BsnCraft.Web.Common.Views.Buttons;
using DevExpress.Data;
using DevExpress.XtraGrid;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Grid.Base
{
    #region Column Enums
    public enum ColumnTypes
    {
        Data,
        Text,
        Check,
    }

    public enum ColumnJustify
    {
        Left,
        Center,
        Right
    }

    public enum ColumnAlign
    {
        Top,
        Middle,
        Bottom
    }

    public enum ColumnCollapse
    {
        None,
        XSmall,
        Small,
        Medium,
        Large,
        XLarge
    }
    #endregion

    public class BsnGridCurrencyColumn : BsnGridColumn
    {
        public BsnGridCurrencyColumn()
        {
            Type = ColumnTypes.Text;
            Justify = ColumnJustify.Right;
            DisplayFormat = "C";
        }

    }
    public class BsnGridColumn
    {
        #region Public Properties        
        public string Field { get; set; } = "";
        public string DisplayFormat { get; set; } = "";
        public int Sort { get; set; }
        public int Width { get; set; } = 0;
        public int ExportWidth { get; set; } = 0;
        public bool Visible { get; set; } = true;
        public bool ShowChild { get; set; } = true;
        public bool CustomTemplate { get; set; }
        public BsnGridColumn Child { get; set; }
        public BsnGridTemplate HeaderTemplate { get; set; }
        public int SortIndex { get; set; } = -1;
        public bool NoWrap { get; set; }

        public ColumnSortMode SortMode { get; set; } = ColumnSortMode.Default;
        public ColumnSortOrder SortOrder { get; set; } 

        #region Name
        private string name = string.Empty;
        public string Name
        {
            get
            {
                return name.ValueOrDefault(Field);

            }
            set
            {
                name = value;
            }
        }
        #endregion

        #region Caption
        private string caption = string.Empty;
        public string Caption
        {
            get
            {
                return caption.ValueOrDefault(Field);

            }
            set
            {
                caption = value;
            }
        }
        #endregion

        public ITemplate DataTemplate { get; set; }
        public object Data { get; set; }

        public List<BsnView> Buttons { get; set; } = new List<BsnView>();
        public ColumnTypes Type { get; set; } = ColumnTypes.Data;
        public ColumnJustify Justify { get; set; } = ColumnJustify.Left;
        public ColumnAlign Align { get; set; } = ColumnAlign.Middle;
        public ColumnCollapse Collapse { get; set; } = ColumnCollapse.None;
        #endregion

        #region CellPrepared
        public virtual void OnCellPrepared(BsnGridEventArgs e)
        {
            CellPrepared?.Invoke(this, e);
        }
        public event EventHandler CellPrepared;
        #endregion

        #region ToString
        //This is mostly for exporting of columns with buttons
        public override string ToString()
        {
            if (Data is string strData)
            {
                return strData;
            }
            return Data.ToString();
        }
        #endregion
    }
}
