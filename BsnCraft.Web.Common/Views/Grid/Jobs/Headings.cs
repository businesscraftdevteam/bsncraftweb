﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Web;
using System;
using System.Collections.Generic;

namespace BsnCraft.Web.Common.Views.Grid.Jobs
{
    public class Headings : BsnGrid
    {
        #region Public Properties
        public Contract Contract { get; set; }
        #endregion

        #region Constructor
        public Headings(BsnApplication app) : base(app)
        {
            Init(BsnGrids.Headings, "Key", "Estimates");

            EditForm = BsnForms.Heading;

            OptionsColumn.Field = "Key";
            OptionsColumn.Buttons.Add(new BsnButton("pencil") { GridEdit = true });

            var optionsButton = new BsnButtonGroup(new BsnButton("bars"));
            OptionsColumn.Buttons.Add(optionsButton);
            var openClose = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Key = "[cellvalue]",
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateHeadings,
                    Mode = BsnDataModes.UpdateHeading                    
                }
            }, "thumbs-up", "Close", false, ButtonTypes.Link);

            openClose.CellPrepared += OpenClose_CellPrepared;
            optionsButton.Buttons.Add(openClose);

            var accept = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Key = "[cellvalue]",
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateHeadings,
                    Mode = BsnDataModes.UpdateHeading,
                    Value = "A"
                }
            }, "thumbs-up", "Accept", false, ButtonTypes.Link);
            
            accept.CellPrepared += Accept_CellPrepared;
            optionsButton.Buttons.Add(accept);

            var duplicate = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.Modal,
                Key = BsnForms.Heading.ToString(),
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateHeadings,
                    Mode = BsnDataModes.Duplicate,
                    Value = "[cellvalue]"
                }
            }, "clone", "Duplicate", false, ButtonTypes.Link);
            optionsButton.Buttons.Add(duplicate);

            var margin = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.Modal,
                Key = BsnForms.Heading.ToString(),
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateHeadings,
                    Mode = BsnDataModes.SetMargin,
                    Value = "[cellvalue]"
                }
            }, "percentage", "Set Margin", false, ButtonTypes.Link);
            optionsButton.Buttons.Add(margin);

            KeyColumn.Field = "Code";
            var edit = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Action = BsnActions.Quotes,
                Id = "[id]",
                Key = "[cellvalue]"
            }
            , "edit", "[cellvalue]", false, ButtonTypes.Link);
            edit.CellPrepared += Edit_CellPrepared;
            KeyColumn.Buttons.Add(edit);

            var sections = App.Settings.Comp(CompanySettingsType.SalesEstimateSections).ToQuotedList();
            if (sections.Count != 1)
            {
                Columns.Add(new BsnGridColumn()
                {
                    Field = "Section",
                    Caption = "Se"
                });
            }            

            Columns.Add(new BsnGridColumn()
            {
                Field = "Description"
            });
            Columns.Add(new BsnGridColumn()
            {
                Field = "StatusDesc",
                Caption = "Status",
                Justify = ColumnJustify.Center
            });
            Columns.Add(new BsnGridColumn()
            {
                Field = "Amount",
                Caption = "Sell Price",
                Type = ColumnTypes.Text,
                DisplayFormat = "C",
                Justify = ColumnJustify.Right
            });

            var cancelledStatus = new GridViewFormatConditionHighlight()
            {
                FieldName = "Status",
                Expression = "[Status] == 'X'",
                ApplyToRow = true,
                Format = GridConditionHighlightFormat.Custom,
            };
            cancelledStatus.RowStyle.CssClass = "text-danger";
            
            var acceptedStatus = new GridViewFormatConditionHighlight()
            {
                FieldName = "Status",
                Expression = "[Status] == 'A'",
                ApplyToRow = true,
                Format = GridConditionHighlightFormat.Custom,
            };
            acceptedStatus.RowStyle.CssClass = "text-success";

            FormatConditions.Add(cancelledStatus);
            FormatConditions.Add(acceptedStatus);
        }
        #endregion

        #region Button Events
        private void Edit_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                var heading = Contract.FindHeading(key);
                if (heading == null)
                {
                    return;
                }
                button.Icon = (heading.IsEstimate) ? "edit" : "mouse-pointer";
                button.Callback.Menu = App.Menu;
                button.Tooltip = (heading.IsEstimate) ? "Estimate" : "Template Select";
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }

        private void OpenClose_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                var heading = Contract.FindHeading(key);
                if (heading == null)
                {
                    return;
                }
                button.Type = (heading.Status.Equals("X")) ? ButtonTypes.LinkSuccess : ButtonTypes.LinkDanger;
                button.Icon = (heading.Status.Equals("X")) ? "lock-open-alt" : "times";
                button.Callback.Data.Value = (heading.Status.Equals("X")) ? "R" : "X";
                button.Caption = (heading.Status.Equals("X")) ? "Re-open" : "Cancel";
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }


        private void Accept_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                var heading = Contract.FindHeading(key);
                if (heading == null)
                {
                    return;
                }
                button.Visible = heading.CanEdit;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            if (Contract == null)
            {
                DataSource = new List<Document>();
                return;
            }                
            DataSource = Contract.Headings;
        }
        #endregion

        #region Add Tools
        protected override void AddTools()
        {
            base.AddTools();
            var addNew = new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Key = "#TEMPLATE#",
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateHeadings,
                    Mode = BsnDataModes.UpdateHeading
                }
            };
            Toolbar.Add(new BsnButton(addNew, "plus-circle", "Template", true, ButtonTypes.Primary) { Tooltip = "Add Heading" });
            addNew.Key = "#EMPTY#";
            Toolbar.Add(new BsnButton(addNew, "plus-circle", "Empty", true, ButtonTypes.Primary) { Tooltip = "Add Heading" });
        }
        #endregion

        #region PrepareEditForm
        protected override void PrepareEditForm(BsnForm editForm, object row, bool readOnly = false)
        {
            var heading = (Heading)row;
            readOnly = !heading.CanEdit;
            base.PrepareEditForm(editForm, row, readOnly);
        }
        #endregion
    }
}
