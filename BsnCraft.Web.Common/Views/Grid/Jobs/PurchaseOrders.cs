﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;

namespace BsnCraft.Web.Common.Views.Grid.Jobs
{
    //TODO: Move to PO namespace when mature

    public class PurchaseOrders : BsnGrid
    {
        #region Public Properties
        public string JobNumber { get; set; }
        #endregion

        #region Constructor
        public PurchaseOrders(BsnApplication app) : base(app)
        {
            Init(BsnGrids.PurchaseOrders, "PONumber", "Purchase Orders");            
        }
        #endregion

        #region Setup
        public override void Setup()
        {
            base.Setup();

            //DataSource = App.VM.PO.PurchaseOrders(JobNumber);

            Columns.Add(new BsnGridColumn() { Field = "Vendor" });
            Columns.Add(new BsnGridColumn() { Field = "VendorName" });
            Columns.Add(new BsnGridColumn() { Field = "Address" });
            Columns.Add(new BsnGridColumn() { Field = "Date" });
            Columns.Add(new BsnGridColumn() { Field = "CostCentre", Caption = "C/C" });
            Columns.Add(new BsnGridColumn() { Field = "Status", Caption = "Sts" });
            Columns.Add(new BsnGridColumn() { Field = "Issued", Caption = "Amount" });
        }
        #endregion

    }
}
