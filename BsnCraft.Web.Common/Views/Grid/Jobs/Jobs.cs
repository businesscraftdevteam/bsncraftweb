﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;

namespace BsnCraft.Web.Common.Views.Grid.Jobs
{
    public class Jobs : BsnGrid
    {
        #region Constructor
        public Jobs(BsnApplication app) : base(app)
        {
            Init(BsnGrids.Jobs, "JobNumber");

            ShowSearch = true;
            ShowPager = true;

            KeyColumn.Field = "JobNumber";
            KeyColumn.Caption = "Job";
            
            KeyColumn.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Menu = BsnMenus.Jobs,
                Id = "[cellvalue]"
            }
            , "fal fa-external-link-square", "[cellvalue]", true, ButtonTypes.Link));

            Columns.Add(new BsnGridColumn() { Field = "CustomerNumber", Caption = "Customer" });
            Columns.Add(new BsnGridColumn() { Field = "Name", Caption = "Name" });
            Columns.Add(new BsnGridColumn() { Field = "OpCentre", Caption = "Opc" });
            Columns.Add(new BsnGridColumn() { Field = "District", Caption = "Dist" });
            Columns.Add(new BsnGridColumn() { Field = "Address", Caption = "Address" });
            Columns.Add(new BsnGridColumn() { Field = "Status", Caption = "Sts" });
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            DataSource = App.VM.JC.Jobs;            
        }
        #endregion
    }
}
