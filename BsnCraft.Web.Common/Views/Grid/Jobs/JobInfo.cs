﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace BsnCraft.Web.Common.Views.Grid.Jobs
{
    public class JobInfo : BsnGrid
    {
        #region Public Properties
        public Contract Contract { get; set; }
        public string Group { get; set; }
        #endregion

        #region Constructor
        public JobInfo(BsnApplication app) : base(app)
        {
            Init(BsnGrids.JobInfo, "Key", "Job Information");

            ShowColumnsButton = false;
            GroupField = "Category";
            Columns.Add(new BsnGridColumn() { Field = "Prompt", Caption = "Question" });
            var answerColumn = new BsnGridColumn() { Field = "Answer", CustomTemplate = true };
            answerColumn.CellPrepared += AnswerColumn_CellPrepared;
            Columns.Add(answerColumn);
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            var jobInfo = new List<Models.JobInfo>();
            if (Contract != null)
            {
                if (Contract.JobInfoGroups.Count > 1 && !string.IsNullOrEmpty(Group))
                {
                    jobInfo = Contract.JobInformation.Where(p => p.Group.Equals(Group)).ToList();
                }
                else
                {
                    jobInfo = Contract.JobInformation;
                }
            }
            App.VM.JI.LoadAnswers(jobInfo);
            DataSource = jobInfo;
        }
        #endregion

        #region GroupPanel
        protected override void GroupPanel(TableCell container, string group, string key, ASPxGridViewTableRowEventArgs e)
        {
            var span = new HtmlGenericControl("span");
            span.Attributes["class"] = "text-primary h6";
            if (!App.VM.JI.JobInfoCategories.ContainsKey(group))
            {
                span.Controls.Add(new LiteralControl("Unknown Category"));                
            }
            else
            {
                var category = App.VM.JI.JobInfoCategories[group];
                span.Controls.Add(new LiteralControl(Helpers.GetArray(category, 0)));
            }            
            container.Controls.Add(span);
        }
        #endregion

        #region Answer Column
        private void AnswerColumn_CellPrepared(object sender, EventArgs e)
        {
            try
            { 
                var args = (BsnGridEventArgs)e;            
                var question = Contract.FindJobInfo(args.KeyValue);
                if (question == null)
                {
                    return;
                }
                var cb = new BsnCallback(App)
                {
                    Type = CallbackType.DataOnly,
                    Data = new CallbackData()
                    {
                        Mode = BsnDataModes.GridSave,
                        Key = args.KeyValue
                    }                    
                };
                var answer = question.Answer.ValueOrDefault(question.Default);
                if (question.HasAnswers)
                {
                    var combo = new BootstrapComboBox()
                    {
                        DataSource = question.Answers,
                        TextField = "Desc",
                        ValueField = "Code",
                        ValueType = typeof(string),
                        Value = answer
                    };
                    combo.ClientSideEvents.ValueChanged = cb.ToComboBox(true);
                    combo.DataBind();
                    args.GridCell.Controls.Add(combo);
                }
                else
                {
                    switch (question.Type)
                    {
                        case "D":                            
                            var date = new BootstrapDateEdit()
                            {
                                DisplayFormatString = "dd/MM/yyyy",
                                EditFormatString = "dd/MM/yyyy"
                            };
                            if (!string.IsNullOrEmpty(answer))
                            {
                                int year = int.Parse(answer.Substring(0, 4));
                                int day = int.Parse(answer.Substring(4, 3));
                                date.Date = new DateTime(year, 1, 1).AddDays(day - 1);
                            }
                            date.ClientSideEvents.DateChanged = cb.ToDateSelect();
                            args.GridCell.Controls.Add(date);
                            break;
                        case "Q":
                            var chk = new BootstrapCheckBox()
                            {
                                AllowGrayed = true,
                                AllowGrayedByClick = false
                            };
                            switch (answer)
                            {
                                case "Y":
                                    chk.CheckState = CheckState.Checked;
                                    break;
                                case "N":
                                    chk.CheckState = CheckState.Unchecked;
                                    break;
                                default:
                                    chk.CheckState = CheckState.Indeterminate;
                                    break;
                            }
                            chk.ClientSideEvents.ValueChanged = cb.ToCheckBox();
                            args.GridCell.Controls.Add(chk);
                            break;
                        case "*":
                            //Display prompt only
                            break;
                        case "M":
                            var memo = new BootstrapMemo
                            {
                                ID = "memo" + args.KeyValue,
                                ClientIDMode = ClientIDMode.Static,
                                Text = answer,
                                Rows = App.Settings.Comp(CompanySettingsType.JobInfoMemoRows).ToInt()
                            };
                            memo.ClientSideEvents.LostFocus = cb.ToMemo();
                            args.GridCell.Controls.Add(memo);
                            break;
                        default:
                            var txt = new BootstrapTextBox()
                            {
                                ID = "txt" + args.KeyValue,
                                ClientIDMode = ClientIDMode.Static,
                                MaxLength = question.Max.ToInt(),
                                Text = answer
                            };                           
                            txt.ClientSideEvents.LostFocus = cb.ToTextBox(question.Min, question.Type);
                            txt.ValidationSettings.EnableCustomValidation = true;
                            args.GridCell.Controls.Add(txt);
                            break;
                    }
                }
            }
            catch (Exception exc)
            {
                App.Utils.LogExc(exc);
            }
        }
        #endregion
    }
}
