using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Data;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using JC = BsnCraft.Web.Common.Views.Form.Jobs;

namespace BsnCraft.Web.Common.Views.Grid.Jobs
{
    public class Estimate : BsnGrid
    {
        #region Public Properties
        public Contract Contract { get; set; }
        public Heading Heading { get; set; }
        public BsnDataModes GridDataMode { get; set; }
        public bool SubHeadings { get; set; }
        public bool ShowAmounts { get; set; }
        public bool ShowMargin { get; set; }
        public bool ShowEmpty { get; set; }
        public bool AdvancedMode { get; set; }
        #endregion

        #region Private Properties
        private BsnButton AddItem { get; set; }
        private BsnButton Duplicate { get; set; }
        private BsnButton BtnEdit { get; set; }
        private BsnButton BtnDelete { get; set; }
        private BsnButton BtnPaste { get; set; }
        private BsnButton BtnSetMargin { get; set; }
        private bool FilterParagraphs { get; set; }
        private List<ComboItem> FilterCostCentres { get; set; }
        private List<ComboItem> FilterPriceDisplays { get; set; }
        private BsnGridColumn SequenceColumn { get; set; }
        private BsnGridColumn SortColumn { get; set; }
        private BsnGridColumn ParagraphColumn { get; set; }
        private BsnGridColumn ItemColumn { get; set; }
        private BsnGridColumn CostCentreColumn { get; set; }
        private BsnGridColumn DescriptionColumn { get; set; }
        private BsnGridColumn IncludeColumn { get; set; }
        private BsnGridColumn QuantityColumn { get; set; }
        private BsnGridColumn ExtdCostColumn { get; set; }
        private BsnGridColumn UnitCostColumn { get; set; }
        private BsnGridColumn MarginCodeColumn { get; set; }
        private BsnGridColumn MarginPctColumn { get; set; }
        private BsnGridColumn UomColumn { get; set; }
        private BsnGridColumn PriceDisplayColumn { get; set; }
        private BsnGridColumn UnitGstColumn { get; set; }
        private BsnGridColumn UnitExColumn { get; set; }
        private BsnGridColumn ExtdIncColumn { get; set; }
        private BsnGridColumn UnitIncColumn { get; set; }
        #endregion

        #region Constructor
        public Estimate(BsnApplication app) : base(app)
        {
            Init(BsnGrids.Estimate, "Key", "Estimate", "Notes");
            ShowGroupFooter = true;
            AllowSelect = true;
            AllowSort = false;
            //InlineEdit = true;
            EditForm = BsnForms.EstimateDetail;
            OptionsColumn.Field = "Key";
            OptionsColumn.SortOrder = ColumnSortOrder.Ascending;
            OptionsColumn.Buttons.Add(new BsnButton("pencil") { GridEdit = true });

            var optionsButton = new BsnButtonGroup(new BsnButton("bars"));
            optionsButton.Buttons.Add(new BsnButton("pencil", "Edit") { GridEdit = true });
            optionsButton.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Id = "EstimateDetail",
                Type = CallbackType.DataOnly,
                Key = "[id]",
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateDetails,
                    Mode = BsnDataModes.Delete,
                    Key = "[cellvalue]"
                }
            }, "trash", "Delete", true, ButtonTypes.LinkDanger));


            optionsButton.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Key = "[id]",
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateDetails,
                    Mode = BsnDataModes.Copy,
                    Key = "[cellvalue]"
                }
            }, "copy", "Copy", true, ButtonTypes.Default));


            BsnButton insertButton = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Key = "[id]",
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateDetails,
                    Mode = BsnDataModes.Paste,
                    Key = "[cellvalue]",
                }
            }, "plus", "Paste After", true, ButtonTypes.Default);

            optionsButton.Buttons.Add(insertButton);


            optionsButton.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Key = "[id]",
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateDetails,
                    Mode = BsnDataModes.MoveTop,
                    Key = "[cellvalue]"
                }
            }, "arrow-to-top", "Move Top", true, ButtonTypes.Default));

            optionsButton.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Key = "[id]",
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateDetails,
                    Mode = BsnDataModes.MoveUp,
                    Key = "[cellvalue]"
                }
            }, "arrow-up", "Move Up", true, ButtonTypes.Default));

            optionsButton.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Key = "[id]",
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateDetails,
                    Mode = BsnDataModes.MoveDown,
                    Key = "[cellvalue]"
                }
            }, "arrow-down", "Move Down", true, ButtonTypes.Default));

            optionsButton.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Key = "[id]",
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateDetails,
                    Mode = BsnDataModes.MoveBottom,
                    Key = "[cellvalue]"
                }
            }, "arrow-to-bottom", "Move Bottom", true, ButtonTypes.Default));


            OptionsColumn.Buttons.Add(optionsButton);

            SequenceColumn = new BsnGridColumn()
            {
                Field = "Sequence",
                Caption = "",
                Justify = ColumnJustify.Left,
                SortIndex = 0,
                SortOrder = ColumnSortOrder.Ascending,
                Visible = false
            };
            SortColumn = new BsnGridColumn()
            {
                Field = "SortOrder",
                Caption = "",
                Justify = ColumnJustify.Left,
                SortIndex = 0,
                SortOrder = ColumnSortOrder.Ascending,
                Visible = false
            };
            ParagraphColumn = new BsnGridColumn()
            {
                Field = "ParagraphCode",
                Caption = "Par"
            };
            CostCentreColumn = new BsnGridColumn()
            {
                Field = "CostCentre",
                Caption = "C/C"
            };
            ItemColumn = new BsnGridColumn()
            {
                Field = "Item"
            };
            DescriptionColumn = new BsnGridColumn()
            {
                Field = "Description"
            };
            IncludeColumn = new BsnGridColumn()
            {
                Field = "Included",
                Caption = "Inc",
                Justify = ColumnJustify.Center
            };
            var includeButton = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateDetails,
                    Mode = BsnDataModes.Include
                }
            });
            includeButton.CellPrepared += IncludeButton_CellPrepared;
            IncludeColumn.Buttons.Add(includeButton);

            QuantityColumn = new BsnGridColumn()
            {
                Field = "Quantity",
                Caption = "Qty",
                Type = ColumnTypes.Text,
                Justify = ColumnJustify.Center,
                DisplayFormat = "F3"
            };
            ExtdCostColumn = new BsnGridColumn()
            {
                Field = "ExtendedCost",
                Caption = "Extd Cost",
                Type = ColumnTypes.Text,
                Justify = ColumnJustify.Right,
                DisplayFormat = "C"
            };
            UnitCostColumn = new BsnGridColumn()
            {
                Field = "UnitCost",
                Caption = "Unit Cost",
                Type = ColumnTypes.Text,
                Justify = ColumnJustify.Right,
                DisplayFormat = "C"
            };
            MarginCodeColumn = new BsnGridColumn()
            {
                Field = "MarginCode",
                Caption = "Mgn Cd",
                Justify = ColumnJustify.Center
            };
            MarginPctColumn = new BsnGridColumn()
            {
                Field = "MarginPct",
                Caption = "Mgn Pct",
                Type = ColumnTypes.Text,
                Justify = ColumnJustify.Center,
                DisplayFormat = "F2"
            };
            UomColumn = new BsnGridColumn()
            {
                Field = "UnitOfMeasure",
                Caption = "UOM",
                Justify = ColumnJustify.Center
            };
            PriceDisplayColumn = new BsnGridColumn()
            {
                Field = "PriceDisplay",
                Caption = "PD",
                Justify = ColumnJustify.Center
            };
            UnitExColumn = new BsnGridColumn()
            {
                Field = "UnitAmountEx",
                Caption = "Unit Ex",
                Type = ColumnTypes.Text,
                Justify = ColumnJustify.Right,
                DisplayFormat = "C"
            };
            UnitGstColumn = new BsnGridColumn()
            {
                Field = "UnitAmountGst",
                Caption = "Unit GST",
                Type = ColumnTypes.Text,
                Justify = ColumnJustify.Right,
                DisplayFormat = "C"
            };
            UnitIncColumn = new BsnGridColumn()
            {
                Field = "UnitAmountInc",
                Caption = "Unit Inc",
                Type = ColumnTypes.Text,
                Justify = ColumnJustify.Right,
                DisplayFormat = "C"
            };
            ExtdIncColumn = new BsnGridColumn()
            {
                Field = "ExtendedAmountInc",
                Caption = "Extd Inc",
                Type = ColumnTypes.Text,
                Justify = ColumnJustify.Right,
                DisplayFormat = "C"
            };

            Columns.Add(SequenceColumn);
            SequenceColumn.Child = SortColumn;
            ParagraphColumn.Child = CostCentreColumn;
            Columns.Add(ParagraphColumn);
            ItemColumn.Child = DescriptionColumn;
            Columns.Add(ItemColumn);
            QuantityColumn.Child = IncludeColumn;
            Columns.Add(QuantityColumn);
            UnitCostColumn.Child = ExtdCostColumn;
            Columns.Add(UnitCostColumn);
            MarginPctColumn.Child = MarginCodeColumn;
            Columns.Add(MarginPctColumn);
            PriceDisplayColumn.Child = UomColumn;
            Columns.Add(PriceDisplayColumn);
            UnitExColumn.Child = UnitGstColumn;
            Columns.Add(UnitExColumn);
            UnitIncColumn.Child = ExtdIncColumn;
            Columns.Add(UnitIncColumn);
            AddItem = new BsnButton(new BsnCallback(App), "plus", "Add Item");
            BtnEdit = new BsnButton(new BsnCallback(App), "pencil");
            Duplicate = new BsnButton(new BsnCallback(App), "clone", "Duplicate");
            BtnDelete = new BsnButton(new BsnCallback(App), "trash-alt", "Delete");
            BtnPaste = new BsnButton(new BsnCallback(App), "paste", "Paste Item(s)");
            BtnSetMargin = new BsnButton(new BsnCallback(App), "percentage", "Set Margin");
        }
        #endregion

        #region Include Button
        private void IncludeButton_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                var detail = Contract.FindEstimateDetail(Heading, key);
                if (detail == null)
                {
                    return;
                }
                button.Visible = detail.ShowCheckbox;
                button.Callback.Key = detail.Paragraph.Heading.Key;
                button.Icon = (detail.Included) ? "fas fa-check-square fa-fw" : "square";
                button.Callback.Data.Key = key;
                button.Callback.Data.Value = (!detail.Included).ToString();
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region Insert Button
        private void InsertButton_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var button = (BsnButton)sender;
                button.Callback.Data.Object = args.GridCell.VisibleIndex.ToString();
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region Setup Columns
        void SetupColumns()
        {
            ParagraphColumn.Visible = !Settings.ShowGroups;
            CostCentreColumn.Visible = !Settings.ShowGroups;

            if (!Settings.ShowGroups)
            {
                ParagraphColumn.Visible = (GridDataMode == BsnDataModes.Paragraphs || AdvancedMode);
                CostCentreColumn.Visible = (GridDataMode == BsnDataModes.CostCentres || AdvancedMode);
            }

            UnitCostColumn.Visible = ShowAmounts && AdvancedMode;
            ExtdCostColumn.Visible = ShowAmounts && AdvancedMode;
            MarginCodeColumn.Visible = ShowAmounts && AdvancedMode;
            MarginPctColumn.Visible = ShowAmounts && AdvancedMode;
            UnitExColumn.Visible = ShowAmounts && AdvancedMode;
            UnitGstColumn.Visible = ShowAmounts && AdvancedMode;
            UnitIncColumn.Visible = ShowAmounts;
            ExtdIncColumn.Visible = ShowAmounts;

            KeyOptionChild = AdvancedMode;
            ItemColumn.ShowChild = AdvancedMode;
            QuantityColumn.ShowChild = AdvancedMode;
            UnitCostColumn.ShowChild = AdvancedMode;
            MarginPctColumn.ShowChild = AdvancedMode;
            PriceDisplayColumn.ShowChild = AdvancedMode;
            UnitExColumn.ShowChild = AdvancedMode;
            UnitIncColumn.ShowChild = AdvancedMode;

            //Experimental settings for estimate grid...

            //Grid.SettingsBehavior.AllowFocusedRow = true;
            //Grid.SettingsBehavior.EnableRowHotTrack = true;
            //Grid.SettingsContextMenu.Enabled = true;
            //Grid.SettingsExport.EnableClientSideExportAPI = true;
            //Grid.SettingsExport.ExcelExportMode = DevExpress.Export.ExportType.DataAware;            
            //Grid.SettingsContextMenu.EnableRowMenu = DevExpress.Utils.DefaultBoolean.True;
            //Grid.SettingsContextMenu.EnableGroupPanelMenu = DevExpress.Utils.DefaultBoolean.False;
        }
        #endregion

        #region Load
        public override void Load()
        {
            SetupColumns();
            base.Load();
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            if (Contract == null || Heading == null)
            {
                DataSource = new List<EstimateDetail>();
                return;
            }
            try
            {
                var list = Contract.EstimateDetails(Heading);
                if (GridDataMode == BsnDataModes.CostCentres)
                {
                    FilterCostCentres = new List<ComboItem>(Contract.CostCentres(Heading).Where(p => p.IsSelected));
                    if (FilterCostCentres.Count > 0)
                    {
                        list = list.Where(p => FilterCostCentres.Find(i => i.Code == p.CostCentre) != null).ToList();
                    }
                }
                else
                {
                    FilterParagraphs = Contract.Paragraphs(Heading).Any(p => p.IsSelected);
                    if (FilterParagraphs)
                    {
                        if (!SubHeadings)
                        {
                            list = list.Where(p => p.Paragraph.IsSelected).ToList();
                        }
                        else
                        {
                            var paragraphs = new List<string>();
                            bool included = false;
                            foreach (var paragraph in Contract.Paragraphs(Heading).OrderBy(p => p.Code))
                            {
                                if (!paragraph.SubHeading)
                                {
                                    included = paragraph.IsSelected;
                                    if (!included)
                                        continue;
                                }
                                if (included)
                                {
                                    paragraphs.Add(paragraph.Code);
                                }
                            }
                            list = list.Where(p => paragraphs.Contains(p.Paragraph.Code)).ToList();
                        }
                    }
                }
                FilterPriceDisplays = new List<ComboItem>(Contract.PriceDisplays(Heading).Where(p => p.IsSelected));
                if (FilterPriceDisplays.Count > 0)
                {
                    list = list.Where(p => FilterPriceDisplays.Find(i => i.Code == p.PriceDisplay) != null).ToList();
                }
                DataSource = list.OrderBy(p => p.Key);
                SelectedKeys = Contract.SelectedEstimateDetailKeys(Heading);
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region PrepareEditForm
        protected override void PrepareEditForm(BsnForm editForm, object row, bool readOnly = false)
        {
            var form = (JC.EstimateDetail)editForm;
            form.ShowAmounts = ShowAmounts;
            form.ShowMargin = ShowMargin;
            form.AdvancedMode = AdvancedMode;
            base.PrepareEditForm(editForm, row, readOnly);
        }
        #endregion

        #region GroupPanel
        protected override void GroupPanel(TableCell container, string group, string key, ASPxGridViewTableRowEventArgs e)
        {
            var flex = new HtmlGenericControl("div");
            flex.Attributes["class"] = "flextable";

            var primary = new HtmlGenericControl("span");
            primary.Attributes["class"] = "flextable-item flextable-primary";
            flex.Controls.Add(primary);

            if (GridDataMode == BsnDataModes.CostCentres)
            {
                container.CssClass = "h6 text-primary";
                var costCentre = Contract.CostCentres(Heading).Find(p => p.Code == group);
                var description = (costCentre == null) ? "Unknown Cost Centre" : costCentre.Desc;
                primary.Controls.Add(new LiteralControl(description));
            }

            if (GridDataMode == BsnDataModes.Paragraphs)
            {
                var paragraph = Contract.FindParagraph(Heading, group);
                var isEmpty = Contract.EstimateDetails(Heading)
                    .Where(p => p.Paragraph.Equals(paragraph) && !p.Key.StartsWith("#")).Count() == 0;

                container.CssClass = isEmpty ? "h6 text-secondary" : "h6 text-primary";
                //e.Row.CssClass = isEmpty ? "text-secondary" : "text-primary";

                if (paragraph == null)
                {
                    primary.Controls.Add(new LiteralControl("Unknown Paragraph"));
                }
                else
                {
                    primary.Controls.Add(new LiteralControl(paragraph.Code + " - " + paragraph.Description));

                    if (ShowAmounts)
                    {
                        primary.Attributes["class"] += " col-3";

                        var item = new HtmlGenericControl("span");
                        if (ShowMargin)
                        {
                            item.Attributes["class"] = "flextable-item col-2";
                            flex.Controls.Add(item);
                            item.Controls.Add(new LiteralControl("Sell: " + paragraph.SellAmtEx.ToCurrency()));

                            item = new HtmlGenericControl("span");
                            item.Attributes["class"] = "flextable-item col-2";
                            flex.Controls.Add(item);
                            item.Controls.Add(new LiteralControl("Gst: " + paragraph.SellAmtGST.ToCurrency()));

                            item = new HtmlGenericControl("span");
                            item.Attributes["class"] = "flextable-item col-2";
                            flex.Controls.Add(item);
                            item.Controls.Add(new LiteralControl("Cost: " + paragraph.CostAmt.ToCurrency()));

                            item = new HtmlGenericControl("span");
                            item.Attributes["class"] = "flextable-item col-2";
                            flex.Controls.Add(item);
                            item.Controls.Add(new LiteralControl("Mgn:" + paragraph.MarginAmt.ToCurrency()));

                            item = new HtmlGenericControl("span");
                            item.Attributes["class"] = "flextable-item col-1";
                            flex.Controls.Add(item);
                            item.Controls.Add(new LiteralControl(paragraph.MarginPct.ToString("P2")));
                        }
                        else
                        {
                            item = new HtmlGenericControl("span");
                            item.Attributes["class"] = "flextable-item col-2";
                            item.Attributes["style"] = "padding-right: 0rem;";
                            flex.Controls.Add(item);
                            item.Controls.Add(new LiteralControl(paragraph.SellAmtInc.ToCurrency()));

                        }
                    }
                }
            }
            container.Controls.Add(flex);
        }
        #endregion

        #region GroupFooterPanel
        protected override void GroupFooterPanel(TableCell container, string group, string key, ASPxGridViewTableRowEventArgs e)
        {
            base.GroupFooterPanel(container, group, key, e);


            if (GridDataMode == BsnDataModes.Paragraphs)
            {
                var addItemIndex = Grid.FindVisibleIndexByKeyValue("#" + group);
                AddItem.SetCallback(new BsnCallback(App)
                {
                    Data = new CallbackData()
                    {
                        Mode = BsnDataModes.StartEdit,
                        Key = addItemIndex.ToString()
                    }
                });

                BtnEdit.SetCallback(new BsnCallback(App)
                {
                    Type = CallbackType.Modal,
                    Key = BsnForms.EstimateParagraph.ToString(),
                    Data = new CallbackData()
                    {
                        Type = BsnDataTypes.EstimateParagraphs,
                        Mode = BsnDataModes.Edit,
                        Value = group
                    }
                });

                Duplicate.SetCallback(new BsnCallback(App)
                {
                    Type = CallbackType.DataOnly,
                    Data = new CallbackData()
                    {
                        Type = BsnDataTypes.EstimateParagraphs,
                        Mode = BsnDataModes.Duplicate,
                        Key = group
                    }
                });

                BtnDelete.SetCallback(new BsnCallback(App)
                {
                    Type = CallbackType.DataOnly,
                    Data = new CallbackData()
                    {
                        Type = BsnDataTypes.EstimateParagraphs,
                        Mode = BsnDataModes.Delete,
                        Key = group
                    }
                });

                BtnPaste.SetCallback(new BsnCallback(App)
                {
                    Type = CallbackType.DataOnly,
                    Data = new CallbackData()
                    {
                        Type = BsnDataTypes.EstimateParagraphs,
                        Mode = BsnDataModes.Paste,
                        Key = group
                    }
                });
                BtnPaste.Visible = Contract.CopiedEstimateDetails.Count > 0;
                BtnPaste.Caption = "Paste " + Contract.CopiedEstimateDetails.Count + " Item(s)";

                BtnSetMargin.SetCallback(new BsnCallback(App)
                {
                    Type = CallbackType.Modal,
                    Key = BsnForms.EstimateParagraphMargin.ToString(),
                    Data = new CallbackData()
                    {
                        Type = BsnDataTypes.EstimateParagraphs,
                        Mode = BsnDataModes.SetMargin,
                        Value = group
                    }
                });

                var optionsButton = new BsnButtonGroup(new BsnButton("bars"));

                container.Controls.Add(AddItem.ToControl());
                container.Controls.Add(BtnEdit.ToControl());

                optionsButton.Buttons.Add(BtnPaste);
                optionsButton.Buttons.Add(BtnSetMargin);
                optionsButton.Buttons.Add(Duplicate);
                optionsButton.Buttons.Add(BtnDelete);
                container.Controls.Add(optionsButton.ToControl());
            }
            else
            {
                container.Controls.Add(AddItem.ToControl());
            }
        }
        #endregion

        #region AddTools
        protected override void AddTools()
        {
            base.AddTools();
            
            var addLine = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.Modal,
                Key = BsnForms.EstimateDetail.ToString()
            }
            , "plus-circle", "Item", true, ButtonTypes.Primary) { Tooltip = "Add new Line" };

            var addGroup = new BsnButtonGroup(addLine, true, true);
            addGroup.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.Modal,
                Key = BsnForms.EstimateParagraph.ToString(),
                Data = new CallbackData()
                {
                    Type = BsnDataTypes.EstimateParagraphs,
                    Mode = BsnDataModes.Add,
                }
            }
            , "plus-circle", "Paragraph", true, ButtonTypes.Primary) { Tooltip = "Add new Paragraph" });            
            Toolbar.Add(addGroup);

            var mode = (App.Mode == BsnModes.CostCentres) ? BsnModes.None : BsnModes.CostCentres;
            Toolbar.Add(new BsnButton(App.Utils.GetModeCallback(mode), "box-usd", "", true, ButtonTypes.Outline)
            {
                Tooltip = (App.Mode == BsnModes.CostCentres) ? "By Paragraphs" : "By Cost Centre",
                Active = (App.Mode == BsnModes.CostCentres)
            });

            //New drop down button for context menu functions
            //Toolbar.Add(new BsnButton(new BsnCallback(App)
            //{
            //    Type = CallbackType.DataOnly,
            //    Data = new CallbackData()
            //    {
            //        Mode = BsnDataModes.Include
            //    }
            //}));

        }
        #endregion

        #region HtmlRowCreated
        protected override void HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                base.HtmlRowCreated(sender, e);
                if (e.KeyValue == null)
                {
                    return;
                }

                switch (e.RowType)
                {
                    case GridViewRowType.Data:
                    case GridViewRowType.Group:
                    case GridViewRowType.GroupFooter:
                        var detail = Contract.FindEstimateDetail(Heading, e.KeyValue.ToString());
                        if (detail == null)
                        {
                            return;
                        }

                        if (!ShowEmpty)
                        {
                            var isEmpty = Contract.EstimateDetails(Heading)
                            .Where(p => p.Paragraph.Equals(detail.Paragraph) && !p.Key.StartsWith("#"))
                            .Count() == 0;

                            e.Row.Visible = !isEmpty;
                        }

                        if (e.RowType == GridViewRowType.Data && e.Row.Visible)
                        {
                            if (e.KeyValue.ToString().StartsWith("#"))
                            {
                                e.Row.CssClass = "d-none";
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion
    }
}
