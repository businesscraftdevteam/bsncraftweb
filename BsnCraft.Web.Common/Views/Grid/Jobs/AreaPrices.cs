﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Grid.Base;
using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using System;

namespace BsnCraft.Web.Common.Views.Grid.Jobs
{
    public class AreaPrices : BsnGrid
    {
        #region Public Properties
        public bool AdvancedMode { get; set; }
        public bool ShowSpecifications { get; set; }
        BsnGridColumn ItemNumberColumn { get; set; }
        BsnGridColumn SourceAreaColumn { get; set; }
        BsnGridColumn SourceEffectiveDateColumn { get; set; }
        BsnGridColumn Description1Column { get; set; }
        BsnGridColumn Description2Column { get; set; }
        BsnGridColumn DescriptionColumn { get; set; }
        BsnGridColumn NotesColumn { get; set; }
        BsnGridColumn CostCentreColumn { get; set; }
        BsnGridColumn ProductCategoryColumn { get; set; }
        BsnGridColumn ProductGroupColumn { get; set; }
        BsnGridColumn SortCodeColumn { get; set; }
        BsnGridColumn PriceDisplayColumn { get; set; }
        BsnGridColumn UOMColumn { get; set; }
        BsnGridColumn TotalCostColumn { get; set; }
        BsnGridColumn MarginPctColumn { get; set; }
        BsnGridColumn MarginAmountColumn { get; set; }
        BsnGridColumn SellColumn { get; set; }
        BsnGridColumn GSTAmountColumn { get; set; }
        BsnGridColumn GSTPctColumn { get; set; }
        BsnGridColumn GSTCodeColumn { get; set; }
        BsnGridColumn IncGSTSellColumn { get; set; }
        BsnGridColumn CommentColumn { get; set; }
        BsnGridColumn CalculatedRateColumn { get; set; }
        BsnGridColumn SheetIDColumn { get; set; }
        BsnGridColumn SellDisplayColumn { get; set; }
        BsnGridColumn ImageColumn { get; set; }
        #endregion

        #region Constructor
        public AreaPrices(BsnApplication app) : base(app)
        {
            Init(BsnGrids.AreaPrices, "ItemNumber","Area Prices");
            ShowPager = true;
            //ShowStripes = true;
            SolidGroups = true;

            Columns.Add(ItemNumberColumn = new BsnGridColumn()
            {
                Field = "ItemNumber",
                Caption = "Item Number",
                NoWrap = true,
                Align = ColumnAlign.Top
            });
            Columns.Add(SourceAreaColumn = new BsnGridColumn()
            {
                Field = "SourceArea",
                Caption = "Src Area",
                Child = SourceEffectiveDateColumn = new BsnGridColumn() { Field = "SourceEffectiveDate", Caption = "Eff. Date" }
            });
            Columns.Add(Description1Column = new BsnGridColumn()
            {
                Field = "Description1",
                Caption = "Description 1",
                Child = Description2Column = new BsnGridColumn() { Field = "Description2", Caption = "Description 2" }
            });
            Columns.Add(DescriptionColumn = new BsnGridColumn()
            {
                Type = ColumnTypes.Text,
                Field = "FullHTMLDescription",
                Caption = "Description",
                Align = ColumnAlign.Top
                //Child = CommentColumn = new BsnGridColumn() { Field = "Comment", Caption = "Comment" }
            });
            //NotesColumn = new BsnGridColumn() { Field = "ItemSpecifications", Caption = "Specifications" };            
            Columns.Add(ProductGroupColumn = new BsnGridColumn()
            {
                Field = "ProductGroup.Description",
                Caption = "Product Group",
                NoWrap = true,
                Child = ProductCategoryColumn = new BsnGridColumn()
                {
                    Field = "ProductCategory.Description",
                    Caption = "Product Category",
                    NoWrap = true
                }
            });
            Columns.Add(CostCentreColumn = new BsnGridColumn()
            {
                Field = "CostCentre",
                Caption = "Cost Centre",
                Child = SortCodeColumn = new BsnGridColumn() { Field = "SortCode.Description", Caption = "Sort Code" }
            });
            Columns.Add(UOMColumn = new BsnGridColumn()
            {
                Field = "UOM.Code",
                Caption = "UOM",
                Align = ColumnAlign.Top,
                Child = PriceDisplayColumn = new BsnGridColumn() { Field = "PriceDisplay.Code", Caption = "PD" }
            });
            Columns.Add(TotalCostColumn = new BsnGridCurrencyColumn()
            {
                Field = "TotalCost",
                Caption = "Cost",
                Child = SellColumn = new BsnGridCurrencyColumn() { Field = "Sell", Caption = "Sell" }
            });
            Columns.Add(MarginPctColumn = new BsnGridColumn()
            {
                Field = "MarginPct",
                Caption = "Margin %",
                Child = MarginAmountColumn = new BsnGridCurrencyColumn() { Field = "MarginAmount", Caption = "Margin" }
            });            
            Columns.Add(GSTPctColumn = new BsnGridColumn()
            {
                Field = "GSTPct",
                Caption = "GST %",
                Child = GSTCodeColumn = new BsnGridColumn() { Field = "GSTCode", Caption = "GST Code" }
            });            
            Columns.Add(GSTAmountColumn = new BsnGridCurrencyColumn()
            {
                Field = "GSTAmount",
                Caption = "GST Amount",
                Child = IncGSTSellColumn = new BsnGridCurrencyColumn() { Field = "IncGSTSell", Caption = "Sell (Inc GST)" }
            });
            Columns.Add(CalculatedRateColumn = new BsnGridColumn()
            {
                Field = "CalculatedRate",
                Caption = "Calc?",
                Child = SheetIDColumn = new BsnGridColumn() { Field = "SheetID", Caption = "Sheet ID" }
            });
            Columns.Add(SellDisplayColumn = new BsnGridColumn()
            {
                Field = "SellDisplay",
                Caption = "Sell Price",
                Justify = ColumnJustify.Right ,
                SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom,
                Align = ColumnAlign.Top
            });


            //DescriptionColumn.Child = NotesColumn;
            GroupField = "SortCode";          

            var isheading= new GridViewFormatConditionHighlight()
            {
                FieldName = "IsHeading",
                Expression = "[IsHeading] = true",
                ApplyToRow = true,
                Format = GridConditionHighlightFormat.Custom,
            };
            
            isheading.RowStyle.CssClass = "bg-secondary text-white";
            FormatConditions.Add(isheading);

        }

        //private void ImageColumn_CellPrepared(object sender, EventArgs e)
        //{
        //    var args = (BsnGridEventArgs)e;
        //    var image = new BootstrapImage()
        //    {
        //        DataSource = App.VM.JC.ItemImage(args.KeyValue, 50)
        //    };
        //    args.GridCell.Controls.Add(image);
        //}
        #endregion

        #region HtmlDataCellPrepared
        protected override void HtmlDataCellPrepared(object sender, BootstrapGridViewTableDataCellEventArgs e)
        {
            base.HtmlDataCellPrepared(sender, e);
            var isHead = Convert.ToBoolean(e.GetValue("IsHeading"));
            if (!isHead)
            {
                return;
            }

            if (e.DataColumn.FieldName.Equals("ItemNumber") || e.DataColumn.FieldName.Equals("UOM.Code"))
            {
                e.Cell.ForeColor = System.Drawing.Color.Transparent;
            }
        }
        #endregion

        #region Load
        public override void Load()
        {
            
            SetupColumns();
            Grid.CustomColumnSort += Grid_CustomColumnSort;
            base.Load();
        }

        private void Grid_CustomColumnSort(object sender, DevExpress.Web.Bootstrap.BootstrapGridViewCustomColumnSortEventArgs e)
        {
            if (e.Column.Name != "SellDisplay") return;
            e.Handled = true;
            Decimal amount1 = Decimal.Parse(e.GetRow1Value("IncGSTSell").ToString());
            Decimal amount2 = Decimal.Parse(e.GetRow2Value("IncGSTSell").ToString());

            e.Result = amount1.CompareTo(amount2);
        }
        #endregion

        #region Setup Columns
        void SetupColumns()
        {
            SourceAreaColumn.Visible = AdvancedMode;
            SourceEffectiveDateColumn.Visible = AdvancedMode;
            CostCentreColumn.Visible = AdvancedMode;
            Description2Column.Visible = AdvancedMode;
            Description1Column.Visible = AdvancedMode;
            ProductCategoryColumn.Visible = AdvancedMode;
            ProductGroupColumn.Visible = AdvancedMode;
            SortCodeColumn.Visible = AdvancedMode;
            PriceDisplayColumn.Visible = AdvancedMode;
            TotalCostColumn.Visible = AdvancedMode;
            MarginPctColumn.Visible = AdvancedMode;
            MarginAmountColumn.Visible = AdvancedMode;
            SellColumn.Visible = AdvancedMode;
            GSTAmountColumn.Visible = AdvancedMode;
            GSTPctColumn.Visible = AdvancedMode;
            GSTCodeColumn.Visible = AdvancedMode;
            //CommentColumn.Visible = AdvancedMode;
            CalculatedRateColumn.Visible = AdvancedMode;
            SheetIDColumn.Visible = AdvancedMode;
            IncGSTSellColumn.Visible = AdvancedMode;
            
        }
        #endregion
        
        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            DataSource = App.VM.APR.SelectedAreaPrices;
        }
        #endregion

    }
}
