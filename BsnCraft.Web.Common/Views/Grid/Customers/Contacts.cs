﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Views.Grid.Customers
{
    public class Contacts : BsnGrid
    {
        #region Private Properties
        public string CustomerNumber { get; set; } = "";
        #endregion

        #region Constructor
        public Contacts(BsnApplication app) : base(app)
        {
            Init(BsnGrids.Contacts, "Key", "Contacts");

            EditForm = BsnForms.Customer;
            ShowPager = true;

            OptionsColumn.Field = "Sequence";
            OptionsColumn.Buttons.Add(new BsnButton("pencil") { GridEdit = true });            
            var bookmark = new BsnButton(new BsnCallback(App)
            {
                Mode = BsnModes.Bookmark,
                Key = Key.ToString()
            }, "bookmark");
            bookmark.CellPrepared += Bookmark_CellPrepared;
            OptionsColumn.Buttons.Add(bookmark);
            var delete = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.Delete
                }
            }, "trash", "", true, ButtonTypes.LinkDanger)
            { Tooltip = "Delete" };
            delete.CellPrepared += Delete_CellPrepared;
            OptionsColumn.Buttons.Add(delete);
            var details = new BsnButton("", "Details");
            details.CellPrepared += Details_CellPrepared;
            OptionsColumn.Buttons.Add(details);

            KeyColumn.Caption = "Contact";
            KeyColumn.Field = "Sequence";

            var customerButton = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Menu = BsnMenus.Customers
            }, "fal fa-external-link-square", "[cellvalue]", true, ButtonTypes.Link);
            customerButton.CellPrepared += CustomerButton_CellPrepared;

            var contactButton = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Action = BsnActions.Contacts,
                Mode = BsnModes.Open,
                Key = "[cellvalue]"
            }
            , "folder-open", "[cellvalue]", true, ButtonTypes.Link);
            contactButton.CellPrepared += ContactButton_CellPrepared;

            KeyColumn.Buttons.Add(customerButton);
            KeyColumn.Buttons.Add(contactButton);
            
            Columns.Add(new BsnGridColumn() { Field = "FullName", Caption = "Name" });
            Columns.Add(new BsnGridColumn() { Field = "FullAddress", Caption = "Address", Collapse = ColumnCollapse.Small });
            Columns.Add(new BsnGridColumn() { Field = "MobilePhone", Caption = "Mobile", Collapse = ColumnCollapse.Small });
            Columns.Add(new BsnGridColumn() { Field = "Email", Collapse = ColumnCollapse.Small });
        }
        #endregion

        #region Reset
        public override void Reset()
        {
            base.Reset();
            CustomerNumber = string.Empty;
        }
        #endregion

        #region Setup
        public override void Setup()
        {    
            //TODO: Below is not working, many BootstrapGridView settings may not work on callback            
            //ShowPager = (string.IsNullOrEmpty(CustomerNumber));
            base.Setup();            
        }
        #endregion

        #region Button Events
        private void CustomerButton_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                var contact = App.VM.AR.FindContact(key);
                if (contact == null)
                {
                    return;
                }            
                button.Caption = contact.CustomerNumber;
                button.Visible = (App.Menu != BsnMenus.Customers || string.IsNullOrEmpty(App.Id));
                button.Callback.Id = contact.CustomerNumber;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }

        private void ContactButton_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                var contact = App.VM.AR.FindContact(key);
                if (contact == null)
                {
                    return;
                }
                button.Callback.Menu = App.Menu;
                var isContracts = (App.Menu == BsnMenus.Contracts || App.Menu == BsnMenus.Leads);
                button.Callback.Id = isContracts ? App.Id : contact.CustomerNumber;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }

        private void Bookmark_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                button.Active = IsBookmark(key);
                button.Icon = (button.Active) ? "fas fa-bookmark fa-fw" : "bookmark";
                button.Callback.Value = key;
                button.Callback.Id = args.GridView.KeyFieldName;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }

        private void Delete_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;

                if (string.IsNullOrEmpty(CustomerNumber))
                {
                    button.Visible = false;
                    return;
                }
                button.Callback.Data.Key = key;
                var firstContact = App.VM.AR.GetContacts(CustomerNumber).OrderBy(p => p.Sequence).ToList().FirstOrDefault();
                var contact = App.VM.AR.FindContact(key);
                button.Visible = (contact != firstContact);
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }

        private void Details_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                var contact = App.VM.AR.FindContact(key);
                if (contact == null)
                {
                    return;
                }
                var details = new List<string[]>()
                {
                    new string []{ contact.Name, "" },
                    new string []{ "", contact.FullAddress },
                    new string []{ "M", contact.MobilePhone },
                    new string []{ "H", contact.HomePhone },
                    new string []{ "W", contact.WorkPhone },
                    new string []{ "F", contact.FaxNumber },
                    new string []{ "E", contact.Email }
                };
                var pop = new HtmlGenericControl("span");
                foreach (var detail in details)
                {
                    var s = Helpers.GetArray(detail, 0);
                    var t = Helpers.GetArray(detail, 1);
                    if (s.Length == 1)
                    {
                        if (string.IsNullOrEmpty(t))
                            continue;
                        s += ": ";
                    }
                    if (!string.IsNullOrEmpty(s))
                    {
                        var strong = new HtmlGenericControl("strong");
                        strong.Controls.Add(new LiteralControl(s));
                        pop.Controls.Add(strong);
                    }
                    if (!string.IsNullOrEmpty(t))
                    {
                        pop.Controls.Add(new LiteralControl(t));
                    }
                    pop.Controls.Add(new LiteralControl("<br />"));
                }
                button.Popover = pop;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();            
            DataSource = (string.IsNullOrEmpty(CustomerNumber)) ?
                App.VM.AR.CustomerContacts :
                App.VM.AR.GetContacts(CustomerNumber);
        }
        #endregion

        #region AddTools
        protected override void AddTools()
        {
            base.AddTools();
            if (OnlyBookmarks)
            {
                return;
            }
            if (string.IsNullOrEmpty(CustomerNumber))
            {
                Toolbar.Add(new BsnButton(App.Utils.GetModeCallback(BsnModes.Add, BsnActions.Contacts, ""),
                    "plus-circle", "Add New", true, ButtonTypes.Primary)
                {
                    Tooltip = "New Customer"
                });

                var mode = (App.Mode == BsnModes.Contacts) ? BsnModes.Customers : BsnModes.Contacts;
                Toolbar.Add(new BsnButton(App.Utils.GetModeCallback(mode), 
                    "users", "", true, ButtonTypes.Outline)
                {
                    Tooltip = "Show Contacts",
                    Active = (App.Mode == BsnModes.Contacts)
                });
            }
            else
            {
                Toolbar.Add(new BsnButton(App.Utils.GetKeyCallback(BsnModes.Add),
                    "plus-circle", "Add New", true, ButtonTypes.Primary)
                { Tooltip = "New Contact" });
            }            
        }
        #endregion

    }
}
