﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Views.Grid.Customers
{
    public class Customers : BsnGrid
    {
        #region Private Properties
        public string CustomerNumber { get; set; }
        #endregion
        
        #region Constructor
        public Customers(BsnApplication app) : base(app)
        {
            Init(BsnGrids.Customers, "CustomerNumber");
            ShowPager = true;
            KeyColumn.Caption = "Customer";
            KeyField = "CustomerNumber";
            Title = "Customers";
            OptionsColumn.Field = "CustomerNumber";
            KeyColumn.Field = "CustomerNumber";

            if (OnlyBookmarks)
            {
                Title = "My " + Title;
            }
            
            var callback = new BsnCallback(App) { Mode = BsnModes.Bookmark, Key = Key.ToString() };
            var bookmark = new BsnButton(callback, "fal fa-bookmark fa-fw");
            bookmark.CellPrepared += Bookmark_CellPrepared;
            OptionsColumn.Buttons.Add(bookmark);

            var details = new BsnButton("", "Details");
            details.CellPrepared += Details_CellPrepared;
            OptionsColumn.Buttons.Add(details);

            var keyLink = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Menu = BsnMenus.Customers,
                Action = BsnActions.Contacts,
                Id = "[cellvalue]"
            }
            , "fal fa-external-link-square", "[cellvalue]", true, ButtonTypes.Link)
            { IsButton = false };
            KeyColumn.Buttons.Add(keyLink);

            CreateColumns();
        }
        #endregion

        #region Reset
        public override void Reset()
        {
            base.Reset();
            CustomerNumber = string.Empty;
        }
        #endregion

        #region Create Columns
        private void CreateColumns()
        {
            Columns.Add(new BsnGridColumn() { Field = "FriendlyName", Caption = "Name" });
            Columns.Add(new BsnGridColumn() { Field = "FullAddress", Caption = "Address", Collapse = ColumnCollapse.Small });
            Columns.Add(new BsnGridColumn() { Field = "Phone", Collapse = ColumnCollapse.Small, });
            Columns.Add(new BsnGridColumn() { Field = "Email", Collapse = ColumnCollapse.Small });
        }
        #endregion

        #region Button Events        
        private void Bookmark_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                button.Active = IsBookmark(key);
                button.Icon = (button.Active) ? "fas fa-bookmark fa-fw" : "fal fa-bookmark fa-fw";
                button.Callback.Value = key;
                button.Callback.Id = args.GridView.KeyFieldName;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }

        private void Details_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;

                if (args.GridView.KeyFieldName == "Key")
                {
                    var contact = App.VM.AR.FindContact(key);
                    if (contact == null)
                    {
                        return;
                    }
                    var details = new List<string[]>()
                    {
                        new string []{ contact.Name, "" },
                        new string []{ "", contact.FullAddress },
                        new string []{ "M", contact.MobilePhone },
                        new string []{ "H", contact.HomePhone },
                        new string []{ "W", contact.WorkPhone },
                        new string []{ "F", contact.FaxNumber },
                        new string []{ "E", contact.Email }
                    };
                    var pop = new HtmlGenericControl("span");
                    foreach (var detail in details)
                    {
                        var s = Helpers.GetArray(detail, 0);
                        var t = Helpers.GetArray(detail, 1);
                        if (s.Length == 1)
                        {
                            if (string.IsNullOrEmpty(t))
                                continue;
                            s += ": ";
                        }
                        if (!string.IsNullOrEmpty(s))
                        {
                            var strong = new HtmlGenericControl("strong");
                            strong.Controls.Add(new LiteralControl(s));
                            pop.Controls.Add(strong);
                        }
                        if (!string.IsNullOrEmpty(t))
                        {
                            pop.Controls.Add(new LiteralControl(t));
                        }
                        pop.Controls.Add(new LiteralControl("<br />"));
                    }
                    button.Popover = pop;
                }
                else
                {
                    button.Visible = false;
                }
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion
        
        #region DataBind
        public override void DataBind()
        {
            base.DataBind();            
            var data = App.VM.AR.Customers;
            if (OnlyBookmarks)
            {
                data = data.Where(p => Bookmarks().Contains(p.CustomerNumber)).ToList();
            }
            DataSource = data;
        }
        #endregion

        #region AddTools
        protected override void AddTools()
        {
            base.AddTools();
            if (OnlyBookmarks)
            {
                return;
            }
            Toolbar.Add(new BsnButton(App.Utils.GetKeyCallback(BsnModes.Add),
                "plus-circle", "Add New", true, ButtonTypes.Primary)
            { Tooltip = "New Customer" });

            var mode = (App.Mode == BsnModes.Contacts) ? BsnModes.Customers : BsnModes.Contacts;
            Toolbar.Add(new BsnButton(App.Utils.GetModeCallback(mode),"users", "", true, ButtonTypes.Outline)
            {
                Tooltip = "Show Contacts",
                Active = (App.Mode == BsnModes.Contacts)
            });
        }
        #endregion

    }
}
