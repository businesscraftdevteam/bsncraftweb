﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Web;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI.WebControls;

namespace BsnCraft.Web.Common.Views.Grid.Contracts
{
    public class Documents : BsnGrid
    {
        #region Public Properties
        public Contract Contract { get; set; }
        #endregion

        #region Constructor
        public Documents(BsnApplication app) : base(app)
        {
            Init(BsnGrids.Documents, "Key", "Documents", "Notes");

            EditForm = BsnForms.Document;

            OptionsColumn.Field = "Sequence";
            OptionsColumn.Buttons.Add(new BsnButton("pencil") { GridEdit = true });

            KeyColumn.Field = "Sequence";
            KeyColumn.Caption = "Seq";
            KeyColumn.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Menu = BsnMenus.Contracts,
                Action = BsnActions.Documents,
                Mode = BsnModes.Open,
                Id = "[id]",
                Key = "[cellvalue]"
            }
            , "folder-open", "[cellvalue]", true, ButtonTypes.Link));

            Columns.Add(new BsnGridColumn() { Field = "Module", Justify = ColumnJustify.Center });
            Columns.Add(new BsnGridColumn() { Field = "Type" });
            Columns.Add(new BsnGridColumn() { Field = "Date", Justify = ColumnJustify.Center });
            Columns.Add(new BsnGridColumn() { Field = "Reference", Caption = "Ref" });
            Columns.Add(new BsnGridColumn() { Field = "Description" });

            var highRating = new GridViewFormatConditionHighlight()
            {
                Index = 0,
                FieldName = "Rating",
                Expression = "[Rating] == 1",
                ApplyToRow = true,
                Format = GridConditionHighlightFormat.LightRedFillWithDarkRedText
            };
            //highRating.RowStyle.CssClass = "bg-danger text-light";

            var mediumRating = new GridViewFormatConditionHighlight()
            {
                Index = 1,
                FieldName = "Rating",
                Expression = "[Rating] == 2",
                ApplyToRow = true,
                Format = GridConditionHighlightFormat.YellowFillWithDarkYellowText
            };
            //mediumRating.RowStyle.CssClass = "bg-warning text-dark";

            var lowRating = new GridViewFormatConditionHighlight()
            {
                Index = 2,
                FieldName = "Rating",
                Expression = "[Rating] == 3",
                ApplyToRow = true,
                Format = GridConditionHighlightFormat.GreenFillWithDarkGreenText
            };
            //lowRating.RowStyle.CssClass = "bg-success text-dark";

            FormatConditions.Add(highRating);
            FormatConditions.Add(mediumRating);
            FormatConditions.Add(lowRating);            
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            if (Contract == null)
            {
                DataSource = new List<Document>();
                return;
            }                
            DataSource = Contract.Documents;
        }
        #endregion

        #region AddTools
        protected override void AddTools()
        {
            base.AddTools();
            Toolbar.Add(new BsnButton(App.Utils.GetKeyCallback(BsnModes.Add), 
                "plus-circle", "Add New", true, ButtonTypes.Primary) { Tooltip = "New Document" });
        }
        #endregion

        #region PreviewPanel
        protected override void PreviewPanel(TableCell container, string key, string notes)
        {
            base.PreviewPanel(container, key, notes);
            var document = Contract.FindDocument(key);
            if (document == null)
            {
                return;
            }
            switch (document.Rating)
            {
                case 1:
                    container.ForeColor = Color.FromArgb(156, 0, 6);
                    container.BackColor = Color.FromArgb(255, 199, 206);
                    break;
                case 2:
                    container.ForeColor = Color.FromArgb(156, 101, 0);
                    container.BackColor = Color.FromArgb(255, 235, 156);
                    break;
                case 3:
                    container.ForeColor = Color.FromArgb(0, 97, 0);
                    container.BackColor = Color.FromArgb(198, 239, 206);
                    break;
            }
        }
        #endregion
    }
}
