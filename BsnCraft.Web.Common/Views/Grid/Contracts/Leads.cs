﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Views.Grid.Contracts
{
    public class Leads : BsnGrid
    {        
        #region Public Properties
        public string CustomerNumber { get; set; }
        #endregion

        #region Constructor
        public Leads(BsnApplication app) : base(app)
        {
            Init(BsnGrids.Leads, "ContractNumber");

            ShowPager = true;
            
            OptionsColumn.Field = "ContractNumber";
            var editCallback = new BsnCallback(App) { Mode = BsnModes.Edit, Key = "[cellvalue]" };
            var editButton = new BsnButton(editCallback, "fal fa-pencil");
            OptionsColumn.Buttons.Add(editButton);

            var callback = new BsnCallback(App) { Mode = BsnModes.Bookmark, Key = Key.ToString() };
            var bookmark = new BsnButton(callback, "fal fa-bookmark fa-fw");
            bookmark.CellPrepared += Bookmark_CellPrepared;
            OptionsColumn.Buttons.Add(bookmark);

            var details = new BsnButton("", "Details");
            details.CellPrepared += Details_CellPrepared;
            OptionsColumn.Buttons.Add(details);

            KeyColumn.Caption = "Lead";
            KeyColumn.Field = "ContractNumber";
            
            KeyColumn.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Menu = BsnMenus.Leads,
                Id = "[cellvalue]"
            }, "fal fa-external-link-square", "[cellvalue]", false, ButtonTypes.Link));

            var customer = new BsnGridColumn() { Field = "CustomerNumber", Caption = "Customer" };
            customer.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Menu = BsnMenus.Customers,
                Id = "[cellvalue]"
            }, "fal fa-external-link-square", "[cellvalue]", false, ButtonTypes.Link));
            Columns.Add(customer);

            Columns.Add(new BsnGridColumn() { Field = "Comment1", Caption = "Claim #" });
            Columns.Add(new BsnGridColumn() { Field = "FriendlyName", Caption = "Name", Collapse = ColumnCollapse.Small });
            Columns.Add(new BsnGridColumn() { Field = "FullAddress", Caption = "Address", Collapse = ColumnCollapse.Small });
            Columns.Add(new BsnGridColumn() { Field = "Status", Caption = "Sts" });
            Columns.Add(new BsnGridColumn() { Field = "SalesConsultant", Caption = "SC" });
        }
        #endregion

        #region Reset
        public override void Reset()
        {
            base.Reset();
            CustomerNumber = string.Empty;
        }
        #endregion

        #region Button Events        
        private void Bookmark_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                button.Active = IsBookmark(key);
                button.Icon = (button.Active) ? "fas fa-bookmark fa-fw" : "fal fa-bookmark fa-fw";
                button.Callback.Value = key;
                button.Callback.Id = args.GridView.KeyFieldName;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }

        private void Details_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                var contract = App.VM.CO.GetContract(key);
                if (contract == null)
                {
                    return;
                }
                var details = new List<string[]>()
            {
                new string []{ contract.FriendlyName, "" },
                new string []{ "", contract.FullAddress },
                new string []{ "St", contract.Status },
                new string []{ "SC", contract.SalesConsultant },
            };
                var pop = new HtmlGenericControl("span");
                foreach (var detail in details)
                {
                    var s = Helpers.GetArray(detail, 0);
                    var t = Helpers.GetArray(detail, 1);
                    if (!string.IsNullOrEmpty(s))
                    {
                        var strong = new HtmlGenericControl("strong");
                        if (!string.IsNullOrEmpty(t))
                        {
                            s += ": ";
                        }
                        strong.Controls.Add(new LiteralControl(s));
                        pop.Controls.Add(strong);
                    }
                    if (!string.IsNullOrEmpty(t))
                    {
                        pop.Controls.Add(new LiteralControl(t));
                    }
                    pop.Controls.Add(new LiteralControl("<br />"));
                }
                button.Popover = pop;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }        
        #endregion

        #region Setup
        public override void Setup()
        {
            var showClaim = App.Settings.Comp(CompanySettingsType.ShowClaimNumber).ToBool();
            Columns.Where(p => p.Field.Equals("Comment1")).FirstOrDefault().Visible = showClaim;
            base.Setup();
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            var data = (string.IsNullOrEmpty(CustomerNumber)) ? App.VM.LE.Leads : App.VM.LE.CustomerLeads(CustomerNumber);
            if (OnlyBookmarks)
            {
                data = data.Where(p => Bookmarks().Contains(p.ContractNumber)).ToList();
            }
            DataSource = data;
        }
        #endregion

        #region AddTools
        protected override void AddTools()
        {
            base.AddTools();
            if (OnlyBookmarks)
            {
                return;
            }
            Toolbar.Add(new BsnButton(App.Utils.GetKeyCallback(BsnModes.Add),
                "plus-circle", "Add New", true, ButtonTypes.Primary)
            { Tooltip = "New Lead" });
        }
        #endregion

    }
}
