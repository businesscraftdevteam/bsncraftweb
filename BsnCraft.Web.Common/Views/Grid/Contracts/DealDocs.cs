﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views.Grid.Base;
using DevExpress.Web;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI.WebControls;

namespace BsnCraft.Web.Common.Views.Grid.Contracts
{
    public class DealDocs : BsnGrid
    {
        #region Public Properties
        public Contract Contract { get; set; }
        #endregion

        #region Constructor
        public DealDocs(BsnApplication app) : base(app)
        {
            Init(BsnGrids.DealDocs, "Key", "Deal Documents", "Notes");
            ShowSearch = false;

            Columns.Add(new BsnGridColumn() { Field = "Sequence", Caption = "Seq" });
            Columns.Add(new BsnGridColumn() { Field = "Type" });
            Columns.Add(new BsnGridColumn() { Field = "Reference", Caption = "Ref" });
            Columns.Add(new BsnGridColumn() { Field = "Description" });

            FormatConditions.Add(new GridViewFormatConditionHighlight()
            {
                FieldName = "SubmitStatus",
                Expression = "[SubmitStatus] < 1",
                ApplyToRow = true,
                Format = GridConditionHighlightFormat.YellowFillWithDarkYellowText
            });

            FormatConditions.Add(new GridViewFormatConditionHighlight()
            {
                FieldName = "SubmitStatus",
                Expression = "[SubmitStatus] > 1",
                ApplyToRow = true,
                Format = GridConditionHighlightFormat.GreenFillWithDarkGreenText
            });
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            if (Contract == null)
            {
                DataSource = new List<Document>();
                return;
            }                
            DataSource = Contract.DealDocuments;
        }
        #endregion

        #region PreviewPanel
        protected override void PreviewPanel(TableCell container, string key, string notes)
        {
            base.PreviewPanel(container, key, notes);
            var document = Contract.FindDocument(key);
            if (document == null)
            {
                return;
            }
            switch (document.Rating)
            {
                case 1:
                    container.ForeColor = Color.FromArgb(156, 0, 6);
                    container.BackColor = Color.FromArgb(255, 199, 206);                    
                    break;
                case 2:
                    container.ForeColor = Color.FromArgb(156, 101, 0);
                    container.BackColor = Color.FromArgb(255, 235, 156);                    
                    break;
                case 3:
                    container.ForeColor = Color.FromArgb(0, 97, 0);
                    container.BackColor = Color.FromArgb(198, 239, 206);                    
                    break;
            }
        }
        #endregion
    }
}
