﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System;

namespace BsnCraft.Web.Common.Views.Grid.Contracts
{
    public class Activities : BsnGrid
    {
        #region Constructor
        public Activities(BsnApplication app) : base(app)
        {
            Init(BsnGrids.Activities, "ContractNumber");

            ShowPager = true;
            KeyColumn.Field = "ContractNumber";
            KeyColumn.Caption = "Key";

            var showLeads = App.Menus.Get(BsnMenus.Leads).Visible;
            var showContracts = App.Menus.Get(BsnMenus.Contracts).Visible;
            
            var keyLink = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Id = "[cellvalue]"
            }
            , "fal fa-external-link-square", "[cellvalue]", true, ButtonTypes.Link);
            keyLink.CellPrepared += KeyLink_CellPrepared;
            KeyColumn.Buttons.Add(keyLink);
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            try
            {
                DataSource = App.VM.AC.ActivityContracts;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region Setup
        public override void Setup()
        {
            var activity = App.VM.AC.Activity;
            if (activity != null && Grid.ID != UID)
            {
                Columns.Clear();
                Grid.Columns.Clear();

                Columns.Add(new BsnGridColumn() { Field = "CustomerNumber", Caption = "Customer" });
                Columns.Add(new BsnGridColumn() { Field = "FriendlyName", Caption = "Name" });
                Columns.Add(new BsnGridColumn() { Field = "FullAddress", Caption = "Address" });

                if (activity.Events == null)
                {
                    activity.Events = App.VM.AC.ActivityEventCodes(activity.Code);
                }

                foreach (var evt in activity.Events)
                {
                    var column = new BsnGridColumn()
                    {
                        Name = evt.Code,
                        Field = "ContractNumber",
                        Caption = evt.CodeDesc
                    };
                    var eventButton = new BsnButton(new BsnCallback(App)
                    {
                        Type = CallbackType.DataOnly,
                        Data = new CallbackData()
                        {
                            Mode = BsnDataModes.QuickRegister
                        }
                    }
                    , "check", "", true, ButtonTypes.Outline);
                    eventButton.CellPrepared += EventButton_CellPrepared;
                    column.Buttons.Add(eventButton);
                    Columns.Add(column);
                }
            }
            base.Setup();
        }
        #endregion

        #region Button Events        
        private void KeyLink_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = ((BsnGridEventArgs)e);
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                var contract = App.VM.CO.GetContract(key);
                button.Callback.Menu = (contract.IsLead) ? BsnMenus.Leads : BsnMenus.Contracts;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }

        private void EventButton_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = (BsnGridEventArgs)e;
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                var contract = App.VM.CO.GetContract(key);
                var contractEvent = contract.FindEvent(args.Column.Name, true);
                if (contractEvent == null)
                {
                    return;
                }
                button.Callback.Data.Key = contractEvent.Key;
                button.Callback.Data.Value = contract.ContractNumber;
                button.Caption = (contractEvent.IsRegistered) ? contractEvent.Actual.ToShort() : contractEvent.Forecast.ToShort();
                button.Icon = (contractEvent.IsRegistered) ? "times" : "check";

                if (contractEvent.IsRegistered)
                {
                    button.Type = (contractEvent.IsLate) ? ButtonTypes.Danger : ButtonTypes.Success;
                }
                else
                {
                    button.Type = (contractEvent.IsLate) ? ButtonTypes.OutlineDanger : ButtonTypes.OutlineSuccess;
                }

                if (string.IsNullOrEmpty(button.Caption))
                {
                    button.Caption = "Register";
                }
                args.Handled = true;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion
    }
}
