﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace BsnCraft.Web.Common.Views.Grid.Contracts
{
    public class Events : BsnGrid
    {
        #region Public Properties
        public Contract Contract { get; set; }
        public Event Parent { get; set; }
        #endregion

        #region Constructor
        public Events(BsnApplication app) : base(app)
        {
            Init(BsnGrids.Events, "Key", "Events", "Notes");
            ShowDetails = (App.VM.CO.EventChecklist.Count > 0);
            EditForm = BsnForms.Event;            

            OptionsColumn.Field = "Key";
            OptionsColumn.Buttons.Add(new BsnButton("pencil") { GridEdit = true });

            Columns.Add(new BsnGridColumn() { Field = "Code", Caption = "Event" });
            Columns.Add(new BsnGridColumn() { Field = "Description" });
            var column = new BsnGridColumn()
            {
                Field = "Actual",
                Caption = "Date"
            };
            var eventButton = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.QuickRegister
                }
            }
            , "check", "", true, ButtonTypes.Outline);
            eventButton.CellPrepared += EventButton_CellPrepared;
            column.Buttons.Add(eventButton);
            Columns.Add(column);
            Columns.Add(new BsnGridColumn() { Field = "Employee" });
            Columns.Add(new BsnGridColumn() { Field = "Amount" });
            Columns.Add(new BsnGridColumn() { Field = "Reference" });            
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            if (Contract == null)
            {
                DataSource = new List<Event>();
                return;
            }                        
            if (Parent != null)
            {
                DataSource = Contract.Events.Where(p => p.Parent.Equals(Parent.Code)).ToList();
                return;
            }
            DataSource = Contract.Events.Where(p => string.IsNullOrEmpty(p.Parent)).ToList();

        }
        #endregion

        #region EventButton
        private void EventButton_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = (BsnGridEventArgs)e;
                var key = args.KeyValue;
                var button = (BsnButton)sender;
                var contractEvent = Contract.FindEvent(key);
                if (contractEvent == null)
                {
                    return;
                }
                button.Callback.Id = contractEvent.Contract.ContractNumber;
                button.Callback.Data.Key = key;
                button.Caption = (contractEvent.IsRegistered) ? contractEvent.Actual.ToShort() : contractEvent.Forecast.ToShort();
                button.Icon = (contractEvent.IsRegistered) ? "times" : "check";
                if (contractEvent.IsRegistered)
                {
                    button.Type = (contractEvent.IsLate) ? ButtonTypes.Danger : ButtonTypes.Success;
                }
                else
                {
                    button.Type = (contractEvent.IsLate) ? ButtonTypes.OutlineDanger : ButtonTypes.OutlineSuccess;
                }

                if (string.IsNullOrEmpty(button.Caption))
                {
                    button.Caption = "Register";
                }
                args.Handled = true;
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region DetailPanel
        protected override void DetailPanel(TableCell container, string key)
        {
            base.DetailPanel(container, key);

            var contractEvent = Contract.FindEvent(key);
            if (contractEvent == null)
                return;

            var checklist = new Events(App)
            {
                ShowSearch = false,
                Contract = Contract,
                Parent = contractEvent
            };
            checklist.Load();

            container.Controls.Add(checklist.ToControl());
        }
        #endregion

        #region DetailButton
        protected override void DetailButton(object sender, ASPxGridViewDetailRowButtonEventArgs e)
        {
            try
            {
                base.DetailButton(sender, e);

                var key = e.KeyValue.ToString();
                var contractEvent = Contract.FindEvent(key);
                if (contractEvent == null)
                {
                    return;
                }
                e.ButtonState = GridViewDetailRowButtonState.Hidden;
                if (Contract.Events.Any(p => p.Parent.Equals(contractEvent.Code)))
                {
                    e.ButtonState = GridViewDetailRowButtonState.Visible;
                }
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion
    }
}
