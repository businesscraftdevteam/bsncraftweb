﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System;

namespace BsnCraft.Web.Common.Views.Grid.Contracts
{
    public class Variations : BsnGrid
    {
        #region Public Properties
        public bool ReadOnly { get; set; }
        public bool Compact { get; set; }
        public Contract Contract { get; set; }
        #endregion

        #region Constructor
        public Variations(BsnApplication app) : base(app)
        {
            Init(BsnGrids.Variations, "Key", "", "Notes");

            EditForm = BsnForms.Variation;
            //AllowSelect = true;

            OptionsColumn.Field = "Sequence";
            OptionsColumn.Buttons.Add(new BsnButton("pencil") { GridEdit = true });

            KeyColumn.Field = "Sequence";
            KeyColumn.Caption = "Seq";
            
            KeyColumn.Buttons.Add(new BsnButton(new BsnCallback(App) { Key = "[cellvalue]" },
                "external-link-square", "[cellvalue]", true));

            SetupColumns(Compact);            
        }
        #endregion

        #region Setup Columns
        private void SetupColumns(bool compact)
        {
            Columns.Add(new BsnGridColumn() { Field = "Date" });
            Columns.Add(new BsnGridColumn() { Field = "Reference" });
            Columns.Add(new BsnGridColumn() { Field = "Description" });

            if (!compact)
            {
                Columns.Add(new BsnGridColumn()
                {
                    Field = "Amount",
                    Caption = "Ex $",
                    Type = ColumnTypes.Text,
                    Justify = ColumnJustify.Right,
                    DisplayFormat = "C"
                });

                Columns.Add(new BsnGridColumn()
                {
                    Field = "GSTAmount",
                    Caption = "Gst $",
                    Type = ColumnTypes.Text,
                    Justify = ColumnJustify.Right,
                    DisplayFormat = "C"
                });

                Columns.Add(new BsnGridColumn()
                {
                    Field = "IncAmount",
                    Caption = "Inc $",
                    Type = ColumnTypes.Text,
                    Justify = ColumnJustify.Right,
                    DisplayFormat = "C"
                });

                Columns.Add(new BsnGridColumn()
                {
                    Field = "CostAmount",
                    Caption = "Cost $",
                    Type = ColumnTypes.Text,
                    Justify = ColumnJustify.Right,
                    DisplayFormat = "C"
                });

                //TODO: consider TypeLabel as popover
                Columns.Add(new BsnGridColumn()
                {
                    Field = "Type",
                    Justify = ColumnJustify.Center
                });

                Columns.Add(new BsnGridColumn()
                {
                    Field = "Issued",
                    Caption = "Iss",
                    Type = ColumnTypes.Check,
                    Justify = ColumnJustify.Center
                });

                Columns.Add(new BsnGridColumn()
                {
                    Field = "Invoiced",
                    Caption = "Inv",
                    Type = ColumnTypes.Check,
                    Justify = ColumnJustify.Center
                });
            }
        }
        #endregion
        
        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            if (Contract == null)
            {
                return;
            }
            DataSource = Contract.Variations;
        }
        #endregion

    }
}
