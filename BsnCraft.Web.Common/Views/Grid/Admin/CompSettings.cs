﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Web.Bootstrap;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Grid.Admin
{
    public class CompSettings : BsnGrid
    {
        #region Constructor
        public CompSettings(BsnApplication app) : base(app)
        {
            Init(BsnGrids.CompSettings, "Type");
            GroupField = "Category";

            KeyColumn.Field = "Type";
            KeyColumn.Caption = "Setting";

            var valueColumn = new BsnGridColumn() { Field = "Value", CustomTemplate = true };
            valueColumn.CellPrepared += ValueColumn_CellPrepared;
            Columns.Add(valueColumn);
        }
        #endregion

        #region Value Column
        private void ValueColumn_CellPrepared(object sender, EventArgs e)
        {
            try
            {
                var args = (BsnGridEventArgs)e;
                var column = (BsnGridColumn)sender;

                var cb = new BsnCallback(App)
                {
                    Type = CallbackType.DataOnly,
                    Data = new CallbackData()
                    {
                        Mode = BsnDataModes.GridSave,
                        Key = args.KeyValue
                    }
                };

                object data = (column.Data is Newtonsoft.Json.Linq.JArray array) ? array.ToObject<string[]>() : column.Data;
                var type = data.GetType().FullName;
                switch (type)
                {
                    case "System.String":
                    case "System.Int32":
                    case "System.Int64":
                    case "System.String[]":
                        var textBox = new BootstrapTextBox()
                        {
                            ID = "txt" + args.KeyValue,
                            Text = type.Equals("System.String[]") ? string.Join(",", data.ToStrArray()) : data.ToString(),
                            ClientIDMode = ClientIDMode.Static,
                        };
                        textBox.ClientSideEvents.LostFocus = cb.ToTextBox();
                        args.GridCell.Controls.Add(textBox);
                        break;
                    case "System.Boolean":
                        var checkBox = new BootstrapCheckBox()
                        {
                            ID = "chk" + args.KeyValue,
                            Checked = data.ToBool(),
                            ClientIDMode = ClientIDMode.Static
                        };
                        checkBox.ClientSideEvents.CheckedChanged = cb.ToCheckBox();
                        args.GridCell.Controls.Add(checkBox);
                        break;
                    default:
                        var debug = type;
                        break;
                }
            }
            catch (Exception exc)
            {
                App.Utils.LogExc(exc);
            }
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            List<CompanySettings> list = App.VM.SY.GetSelectedSettings();
            DataSource = list;
        }
        #endregion
    }
}