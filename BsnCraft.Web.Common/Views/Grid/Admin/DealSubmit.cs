﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Web;

namespace BsnCraft.Web.Common.Views.Grid.Admin
{
    public class DealSubmit : BsnGrid
    {
        #region Constructor
        public DealSubmit(BsnApplication app) : base(app)
        {
            Init(BsnGrids.DealSubmit, "Key", "Deal Submit");
            EditForm = BsnForms.DealSubmit;

            OptionsColumn.Field = "Key";
            OptionsColumn.Buttons.Add(new BsnButton("pencil") { GridEdit = true });
            OptionsColumn.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.Delete,
                    Key = "[cellvalue]"
                }
            }, "trash", "", true, ButtonTypes.LinkDanger)
            { Tooltip = "Delete" });

            KeyColumn.Field = "Type";

            Columns.Add(new BsnGridColumn() { Field = "Reference", Caption = "Ref" });
            Columns.Add(new BsnGridColumn() { Field = "Description" });
            Columns.Add(new BsnGridColumn() { Field = "Required", Caption = "Req?", Justify = ColumnJustify.Center });
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();            
            DataSource = App.Settings.Company.DealDocuments;
        }
        #endregion
        
        #region AddTools
        protected override void AddTools()
        {
            base.AddTools();
            Toolbar.Add(new BsnButton(App.Utils.GetKeyCallback(BsnModes.Add),
                "plus-circle", "Add New", true, ButtonTypes.Primary)
            { Tooltip = "New Document" });
        }
        #endregion
    }
}
