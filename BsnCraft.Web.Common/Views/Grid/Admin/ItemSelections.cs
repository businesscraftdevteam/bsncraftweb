﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Web;

namespace BsnCraft.Web.Common.Views.Grid.Admin
{
    public class ItemSelections : BsnGrid
    {
        #region Constructor
        public ItemSelections(BsnApplication app) : base(app)
        {
            Init(BsnGrids.ItemSelections, "Code", "Item Selections", "Query");
            EditForm = BsnForms.ItemSelection;

            OptionsColumn.Field = "Code";
            OptionsColumn.Buttons.Add(new BsnButton("pencil") { GridEdit = true });
            OptionsColumn.Buttons.Add(new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.Delete,
                    Key = "[cellvalue]"
                }
            }, "trash", "", true, ButtonTypes.LinkDanger)
            { Tooltip = "Delete" });

            KeyColumn.Field = "Code";

            Columns.Add(new BsnGridColumn() { Field = "Description" });
            Columns.Add(new BsnGridColumn() { Field = "Option" });
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();            
            DataSource = App.VM.IM.ItemSelections;
        }
        #endregion
        
        #region AddTools
        protected override void AddTools()
        {
            base.AddTools();
            Toolbar.Add(new BsnButton(App.Utils.GetKeyCallback(BsnModes.Add),
                "plus-circle", "Add New", true, ButtonTypes.Primary)
            { Tooltip = "New Selection" });
        }
        #endregion
    }
}
