﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using JC = BsnCraft.Web.Common.Views.Form.Jobs;

namespace BsnCraft.Web.Common.Views.Grid
{
    public class Estimate : BsnGrid
    {
        #region Public Properties
        public Contract Contract { get; set; }
        public Heading Heading { get; set; }
        public bool SubHeadings { get; set; }
        public bool ShowAmounts { get; set; }
        public bool AdvancedMode { get; set; }        
        #endregion

        #region Private Properties
        private BsnButton AddItem { get; set; }
        private bool FilterParagraphs { get; set; }
        private bool FilterCostCentres { get; set; }
        private BsnGridColumn ItemColumn { get; set; }
        private BsnGridColumn DescriptionColumn { get; set; }
        private BsnGridColumn IncludeColumn { get; set; }
        private BsnGridColumn QuantityColumn { get; set; }
        private BsnGridColumn ExtdCostColumn { get; set; }
        private BsnGridColumn UnitCostColumn { get; set; }
        private BsnGridColumn MarginCodeColumn { get; set; }
        private BsnGridColumn MarginPctColumn { get; set; }
        private BsnGridColumn UomColumn { get; set; }
        private BsnGridColumn PriceDisplayColumn { get; set; }
        private BsnGridColumn UnitGstColumn { get; set; }
        private BsnGridColumn UnitExColumn { get; set; }
        private BsnGridColumn ExtdIncColumn { get; set; }
        private BsnGridColumn UnitIncColumn { get; set; }
        #endregion

        #region Constructor
        public Estimate(BsnApplication app) : base(app)
        {
            Init(BsnGrids.Estimate, "Key", "Estimate", "Notes");
            ShowGroupFooter = true;            
            EditForm = BsnForms.EstimateDetail;
            GroupField = "ParagraphCode";
            OptionsColumn.Field = "ScrKey";
            OptionsColumn.Buttons.Add(new BsnButton("pencil") { GridEdit = true });
            KeyColumn.Field = "Seq";
            KeyColumn.Caption = "#";
            KeyColumn.Align = ColumnAlign.Center;

            ItemColumn = new BsnGridColumn() { Field = "Item" };
            DescriptionColumn = new BsnGridColumn() { Field = "Description" };
            IncludeColumn = new BsnGridColumn()
            {
                Field = "Included",
                Caption = "Inc",
                Align = ColumnAlign.Center
            };
            var includeButton = new BsnButton(new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.Include
                }
            });
            includeButton.CellPrepared += IncludeButton_CellPrepared;
            IncludeColumn.Buttons.Add(includeButton);
            QuantityColumn = new BsnGridColumn()
            {
                Field = "Quantity",
                Caption = "Qty",
                Type = ColumnTypes.Text,
                Align = ColumnAlign.Center,
                DisplayFormat = "F3"
            };
            ExtdCostColumn = new BsnGridColumn()
            {
                Field = "ExtendedCost",
                Caption = "Extd Cost",
                Type = ColumnTypes.Text,
                Align = ColumnAlign.Right,
                DisplayFormat = "C"
            };
            UnitCostColumn = new BsnGridColumn()
            {
                Field = "UnitCost",
                Caption = "Unit Cost",
                Type = ColumnTypes.Text,
                Align = ColumnAlign.Right,
                DisplayFormat = "C"
            };
            MarginCodeColumn = new BsnGridColumn()
            {
                Field = "MarginCode",
                Caption = "Mgn Cd",
                Align = ColumnAlign.Center
            };
            MarginPctColumn = new BsnGridColumn()
            {
                Field = "MarginPct",
                Caption = "Mgn Pct",
                Type = ColumnTypes.Text,
                Align = ColumnAlign.Center,
                DisplayFormat = "F2"
            };
            UomColumn = new BsnGridColumn()
            {
                Field = "UnitOfMeasure",
                Caption = "UOM",
                Align = ColumnAlign.Center
            };
            PriceDisplayColumn = new BsnGridColumn()
            {
                Field = "PriceDisplay",
                Caption = "PD",
                Align = ColumnAlign.Center
            };
            UnitExColumn = new BsnGridColumn()
            {
                Field = "UnitAmountEx",
                Caption = "Unit Ex",
                Type = ColumnTypes.Text,
                Align = ColumnAlign.Right,
                DisplayFormat = "C"
            };
            UnitGstColumn = new BsnGridColumn()
            {
                Field = "UnitAmountGst",
                Caption = "Unit GST",
                Type = ColumnTypes.Text,
                Align = ColumnAlign.Right,
                DisplayFormat = "C"
            };
            UnitIncColumn = new BsnGridColumn()
            {
                Field = "UnitAmountInc",
                Caption = "Unit Inc",
                Type = ColumnTypes.Text,
                Align = ColumnAlign.Right,
                DisplayFormat = "C"
            };
            ExtdIncColumn = new BsnGridColumn()
            {
                Field = "ExtendedAmountInc",
                Caption = "Extd Inc",
                Type = ColumnTypes.Text,
                Align = ColumnAlign.Right,
                DisplayFormat = "C"
            };
            ItemColumn.Child = DescriptionColumn;
            Columns.Add(ItemColumn);
            QuantityColumn.Child = IncludeColumn;
            Columns.Add(QuantityColumn);
            UnitCostColumn.Child = ExtdCostColumn;
            Columns.Add(UnitCostColumn);
            MarginPctColumn.Child = MarginCodeColumn;
            Columns.Add(MarginPctColumn);
            PriceDisplayColumn.Child = UomColumn;
            Columns.Add(PriceDisplayColumn);
            UnitExColumn.Child = UnitGstColumn;
            Columns.Add(UnitExColumn);
            UnitIncColumn.Child = ExtdIncColumn;
            Columns.Add(UnitIncColumn);
            AddItem = new BsnButton(new BsnCallback(App), "plus", "Add Item");
        }
        #endregion

        #region Button Events
        private void IncludeButton_CellPrepared(object sender, EventArgs e)
        {
            var args = ((BsnGridEventArgs)e);
            var key = args.KeyValue;
            var button = (BsnButton)sender;

            var detail = Contract.FindEstimateDetail(Heading, key);
            if (detail == null)
                return;

            button.Callback.Key = detail.Paragraph.Heading.Key;
            button.Icon = (detail.Included) ? "fas fa-check-square fa-fw" : "square";
            button.Callback.Data.Key = key;
            button.Callback.Data.Value = (!detail.Included).ToString();
        }
        #endregion

        #region Setup Columns
        void SetupColumns()
        {            
            UnitCostColumn.Visible = ShowAmounts && AdvancedMode;
            ExtdCostColumn.Visible = ShowAmounts && AdvancedMode;
            MarginCodeColumn.Visible = ShowAmounts && AdvancedMode;
            MarginPctColumn.Visible = ShowAmounts && AdvancedMode;
            UnitExColumn.Visible = ShowAmounts && AdvancedMode;
            UnitGstColumn.Visible = ShowAmounts && AdvancedMode;
            UnitIncColumn.Visible = ShowAmounts;
            ExtdIncColumn.Visible = ShowAmounts;

            KeyInOption = AdvancedMode;
            ItemColumn.ShowChild = AdvancedMode;
            QuantityColumn.ShowChild = AdvancedMode;
            UnitCostColumn.ShowChild = AdvancedMode;
            MarginPctColumn.ShowChild = AdvancedMode;
            PriceDisplayColumn.ShowChild = AdvancedMode;
            UnitExColumn.ShowChild = AdvancedMode;
            UnitIncColumn.ShowChild = AdvancedMode;
        }
        #endregion

        #region Load
        public override void Load()
        {
            SetupColumns();
            base.Load();
        }
        #endregion
        
        #region DataBind
        public override void DataBind()
        {            
            base.DataBind();
            if (Contract == null || Heading == null)
            {
                DataSource = new List<EstimateDetail>();
                return;
            }

            try
            {
                

                FilterParagraphs = Contract.Paragraphs(Heading).Any(p => p.IsSelected);
                if (FilterParagraphs)
                {
                    if (!SubHeadings)
                    {
                        DataSource = Contract.EstimateDetails(Heading)
                            .Where(p => p.Paragraph.IsSelected).ToList();
                    }
                    else
                    {
                        var paragraphs = new List<string>();
                        bool included = false;
                        foreach (var paragraph in Contract.Paragraphs(Heading).OrderBy(p => p.Code))
                        {
                            if (!paragraph.SubHeading)
                            {
                                included = paragraph.IsSelected;
                                if (!included)
                                    continue;
                            }
                            if (included)
                            {
                                paragraphs.Add(paragraph.Code);
                            }
                        }
                        DataSource = Contract.EstimateDetails(Heading)
                            .Where(p => paragraphs.Contains(p.Paragraph.Code)).ToList();
                    }
                }
                else
                {
                    DataSource = Contract.EstimateDetails(Heading);
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region PrepareEditForm
        protected override void PrepareEditForm(BsnForm editForm, object row)
        {
            base.PrepareEditForm(editForm, row);
            var form = (JC.EstimateDetail)editForm;
            form.ShowAmounts = ShowAmounts;
            form.AdvancedMode = AdvancedMode;
        }
        #endregion

        #region GroupPanel
        protected override void GroupPanel(TableCell container, string group, string key)
        {
            var flex = new HtmlGenericControl("div");
            flex.Attributes["class"] = "flextable";

            var primary = new HtmlGenericControl("div");
            primary.Attributes["class"] = "flextable-item flextable-primary";
            flex.Controls.Add(primary);

            var span = new HtmlGenericControl("span");
            span.Attributes["class"] = "text-primary h6";
            primary.Controls.Add(span);

            var paragraph = Contract.FindParagraph(Heading, group);

            if (paragraph == null)
            {
                span.Controls.Add(new LiteralControl("Unknown Paragraph"));
            }
            else
            {
                span.Controls.Add(new LiteralControl(paragraph.Description));

                if (ShowAmounts)
                {
                    var item = new HtmlGenericControl("div");
                    item.Attributes["class"] = "flextable-item";
                    flex.Controls.Add(item);

                    var amount = new HtmlGenericControl("span");
                    amount.Attributes["class"] = "text-primary h6";
                    item.Controls.Add(amount);

                    amount.Controls.Add(new LiteralControl(paragraph.Amount.ToString("C")));
                }

            }
            container.Controls.Add(flex);
        }
        #endregion

        #region GroupFooterPanel
        protected override void GroupFooterPanel(TableCell container, string group, string key)
        {
            base.GroupFooterPanel(container, group, key);
            
            container.Controls.Add(AddItem.ToControl());
        }
        #endregion

    }
}
