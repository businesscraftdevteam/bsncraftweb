﻿using BsnCraft.Web.Common.Views.Base;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Views.Buttons
{
    public class BsnButtonGroup : BsnView
    {
        #region Public Properties
        public List<BsnButton> Buttons { get; private set; }
        public BsnButton Button { get; private set; }
        public bool Right { get; private set; }
        public bool Split { get; private set; }
        #endregion

        //TODO: this could just be replaced with enhanced BsnButton

        #region Constructor
        public BsnButtonGroup(BsnButton button = null, bool right = false, bool split = false)
        {
            Buttons = new List<BsnButton>();
            Button = button;
            Right = right;
            Split = split;
        }
        #endregion
        
        #region ToControl
        private HtmlGenericControl group;
        public HtmlGenericControl ToGroup()
        {
            if (group == null)
            {
                group = new HtmlGenericControl("div");
                group.Attributes["class"] = "btn-group";
            }

            group.Controls.Clear();
            foreach (var button in Buttons.ToList())
            {
                button.Type = ButtonTypes.Outline;
                group.Controls.Add(button.ToControl());
            }

            return group;
        }
        #endregion

        #region ToDropDown
        private HtmlGenericControl dropdown;
        public override Control ToControl()
        {
            if (dropdown == null)
            {
                dropdown = new HtmlGenericControl("div");
                dropdown.Attributes["class"] = "btn-group";
            }
            if (Button == null)
            {
                //Shouldn't happen
                return dropdown;
            }
            dropdown.Controls.Clear();
            var mainButton = (HtmlGenericControl)Button.ToControl();
            dropdown.Controls.Add(mainButton);
            var span = new HtmlGenericControl("span");
            span.Attributes["class"] = "sr-only";
            span.Controls.Add(new LiteralControl("Toggle Dropdown"));
            if (Split)
            {
                var splitButton = new HtmlGenericControl("button");
                dropdown.Controls.Add(splitButton);
                splitButton.Controls.Add(span);
                splitButton.Attributes["type"] = "button";
                var css = mainButton.Attributes["class"];
                css += " dropdown-toggle dropdown-toggle-split";
                splitButton.Attributes["class"] = css;
                splitButton.Attributes["data-toggle"] = "dropdown";
            }
            else
            {
                mainButton.Controls.Add(span);
                mainButton.Attributes["class"] += " dropdown-toggle";
                mainButton.Attributes["data-toggle"] = "dropdown";
                if (!string.IsNullOrEmpty(Button.Tooltip))
                {
                    mainButton.Attributes["class"] += " bsn-tooltip";
                }
            }
            var menu = new HtmlGenericControl("div");
            dropdown.Controls.Add(menu);
            menu.Attributes["class"] = "dropdown-menu";
            if (Right)
            {
                menu.Attributes["class"] += " dropdown-menu-right";
            }
            foreach (var item in Buttons.ToList())
            {
                var link = (HtmlGenericControl)item.ToControl();
                link.Attributes["class"] = "dropdown-item";
                if (item.Active)
                {
                    link.Attributes["class"] += " active";
                }
                menu.Controls.Add(link);
            }
            return dropdown;
        }
        #endregion

    }
}
