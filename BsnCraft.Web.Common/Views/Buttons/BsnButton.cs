﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views.Base;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Views.Buttons
{
    #region Public Enums
    public enum ButtonTypes
    {
        None,
        Outline,
        OutlineDanger,
        OutlineSuccess,
        OutlineInfo,
        OutlineWarning,
        OutlineSecondary,
        Default,
        Link,
        LinkDanger,
        LinkSuccess,
        LinkSecondary,
        Primary,
        Secondary,
        Info,
        Success,
        Warning,
        Danger,
        Close        
    }

    public enum AppButtons
    {
        DarkMode,
        FluidLayout,
        TabletMode
    }
    #endregion

    public class BsnButton : BsnView
    {
        #region Public Properties        
        public string Caption { get; set; } = "";
        public string Javascript { get; set; } = "";
        public object Data { get; set; }
        public BsnCallback Callback { get; private set; }
        public int Sort { get; set; }        
        public bool IsButton { get; set; }
        public bool Enabled { get; set; } = true;
        public ButtonTypes Type { get; set; }        
        public HtmlGenericControl Popover { get; set; }        
        public string Notes { get; set; }
        public string Tooltip { get; set; }
        public string TooltipPos { get; set; }
        public bool GridEdit { get; set; }
        public Dictionary<string, string> HtmlAttributes { get; private set; }
        public Dictionary<string, string> IconAttributes { get; private set; }
        #endregion

        #region Icon
        private string icon = string.Empty;
        public string Icon
        {
            get
            {
                return icon;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    return;
                }
                var fa = value;
                if (fa.StartsWith("fas ") || fa.StartsWith("fal "))
                {
                    icon = value;
                    return;
                }
                icon = "fal fa-" + value + " fa-fw";
            }
        }
        #endregion

        #region Private Properties
        private string OriginalCaption;
        private string OriginalCallback;
        #endregion

        #region Refresh
        public void Refresh()
        {
            Caption = OriginalCaption;
            if (Callback != null)
            {
                Callback.Init(OriginalCallback);
            }
        }
        #endregion

        #region Constructor
        public BsnButton(string icon = "", string caption = "", bool isButton = true, ButtonTypes type = ButtonTypes.Link)
        {
            Setup(icon, caption, isButton, type);
        }

        public BsnButton(BsnCallback callback, string icon = "", string caption = "", bool isButton = true, ButtonTypes type = ButtonTypes.Link)
        {  
            Callback = callback;            
            OriginalCallback = callback.ToString();
            Setup(icon, caption, isButton, type);
        }
        #endregion

        #region Setup
        private void Setup(string icon = "", string caption = "", bool isButton = true, ButtonTypes type = ButtonTypes.Primary)
        {
            HtmlAttributes = new Dictionary<string, string>();
            IconAttributes = new Dictionary<string, string>();
            Icon = icon;
            Caption = caption;
            IsButton = isButton;
            Type = type;
            OriginalCaption = caption;
        }
        #endregion

        #region SetCallback
        public void SetCallback(BsnCallback callback)
        {
            if (callback == null)
            {
                return;
            }
            Callback = callback;
            OriginalCallback = callback.ToString();
        }
        #endregion

        #region CellPrepared
        public virtual void OnCellPrepared(EventArgs e)
        {
            CellPrepared?.Invoke(this, e);
        }
        public event EventHandler CellPrepared;
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            base.ToControl();
            if (Popover != null || !string.IsNullOrEmpty(Notes))
            {
                return GetPopover();
            }
            return GetButton();
        }
        #endregion

        #region DataBind
        public void DataBind(BsnApplication app, string cellvalue)
        {
            Caption = Caption.Replace("[cellvalue]", cellvalue);
            if (Callback == null)
            {
                return;
            }

            Callback.Id = Callback.Id.Replace("[cellvalue]", cellvalue);
            Callback.Key = Callback.Key.Replace("[cellvalue]", cellvalue);
            Callback.Value = Callback.Value.Replace("[cellvalue]", cellvalue);
            
            var id = app.Menus.Current.Id;
            Callback.Id = Callback.Id.Replace("[id]", id);
            Callback.Key = Callback.Key.Replace("[id]", id);
            Callback.Value = Callback.Value.Replace("[id]", id);


            if (Callback.Data != null)
            {
                Callback.Data.Key = Callback.Data.Key.Replace("[cellvalue]", cellvalue);
                Callback.Data.Value = Callback.Data.Value.Replace("[cellvalue]", cellvalue);
                Callback.Data.Key = Callback.Data.Key.Replace("[id]", id);
                Callback.Data.Value = Callback.Data.Value.Replace("[id]", id);
            }
        }
        #endregion

        #region Get Button
        public HtmlGenericControl GetButton()
        {
            var button = new HtmlGenericControl(IsButton ? "button" : "a");
            if (IsButton)
            {
                button.Attributes["type"] = "button";
            }
            else
            {
                button.Attributes["href"] = "javascript:void(0);";
            }
            button.ID = UID;
            button.Attributes["class"] = "btn";
            if (!string.IsNullOrEmpty(Javascript))
            {
                if (Javascript.StartsWith("href:"))
                {
                    button.Attributes["href"] = Javascript.Substring(5);
                }
                else
                {
                    button.Attributes["onclick"] = Javascript;
                }
            }
            if (Callback != null)
            {
                button.Attributes["onclick"] = Callback.ToCallback();
            }
            if (!string.IsNullOrEmpty(Icon))
            {
                var icon = new HtmlGenericControl("span");
                icon.Attributes["class"] = "image " + Icon;
                icon.Attributes["aria-hidden"] = "true";
                button.Controls.Add(icon);
            }
            SetText(button);
            ApplyType(button);            
            if (!string.IsNullOrEmpty(Tooltip))
            {
                button.Attributes["title"] = Tooltip;
                button.Attributes["data-toggle"] += "tooltip";
                if (!string.IsNullOrEmpty(TooltipPos))
                {
                    button.Attributes["data-placement"] = TooltipPos;
                }
                button.Attributes["data-container"] = "body";
            }
            foreach (var attribute in HtmlAttributes)
            {
                button.Attributes[attribute.Key] = attribute.Value;
            }
            if (!string.IsNullOrEmpty(Icon))
            {
                if (button.Controls[0] is HtmlGenericControl span)
                {
                    foreach (var attribute in IconAttributes)
                    {
                        span.Attributes[attribute.Key] = attribute.Value;
                    }
                }
            }
            if (Active)
            {
                button.Attributes["class"] += " active";
            }
            if (!Enabled)
            {
                button.Attributes["class"] += " disabled";
            }

            if (!string.IsNullOrEmpty(CSS))
            {
                button.Attributes["class"] += " " + CSS.Trim();
            }
            button.Visible = Visible;
            return button;
        }
        #endregion

        #region Set Text
        public void SetText(HtmlGenericControl button)
        {
            if (string.IsNullOrEmpty(Caption))
            {
                return;
            }

            var span = new HtmlGenericControl("span");
            if (!string.IsNullOrEmpty(Icon))
            {
                span.Attributes["style"] = "padding-left: 0.4em";
                if (IsButton)
                {
                    //span.Attributes["class"] = "d-sm-none d-md-block";
                }
            }
            if (Type == ButtonTypes.Close)
            {
                span.Attributes["class"] = "btn-danger fal fa-times";
                span.Attributes["style"] = "padding-left: 0.22rem;";
                span.Controls.Add(new LiteralControl("&nbsp;"));
            }
            else
            {
                span.InnerText = Caption;
            }
            button.Controls.Add(span);
        }
        #endregion

        #region Apply Type
        void ApplyType(HtmlGenericControl button)
        {
            switch (Type)
            {
                case ButtonTypes.None:
                case ButtonTypes.Close:
                    button.Attributes["class"] = "";
                    break;
                case ButtonTypes.Outline:
                    button.Attributes["class"] += " btn-outline-primary";
                    break;
                case ButtonTypes.OutlineSuccess:
                    button.Attributes["class"] += " btn-outline-success";
                    break;
                case ButtonTypes.OutlineDanger:
                    button.Attributes["class"] += " btn-outline-danger";
                    break;
                case ButtonTypes.OutlineInfo:
                    button.Attributes["class"] += " btn-outline-info";
                    break;
                case ButtonTypes.OutlineWarning:
                    button.Attributes["class"] += " btn-outline-warning";
                    break;
                case ButtonTypes.OutlineSecondary:
                    button.Attributes["class"] += " btn-outline-secondary";
                    break;
                case ButtonTypes.Default:
                    button.Attributes["class"] += " btn-default";
                    break;
                case ButtonTypes.Link:
                    button.Attributes["class"] += " btn-link";
                    break;
                case ButtonTypes.LinkSuccess:
                    button.Attributes["class"] += " btn-link text-success";
                    break;
                case ButtonTypes.LinkDanger:
                    button.Attributes["class"] += " btn-link text-danger";
                    break;
                case ButtonTypes.LinkSecondary:
                    button.Attributes["class"] += " btn-link text-secondary";
                    break;
                case ButtonTypes.Primary:
                    button.Attributes["class"] += " btn-primary";
                    break;
                case ButtonTypes.Secondary:
                    button.Attributes["class"] += " btn-secondary";
                    break;
                case ButtonTypes.Danger:
                    button.Attributes["class"] += " btn-danger";
                    break;
                case ButtonTypes.Info:
                    button.Attributes["class"] += " btn-info";
                    break;
                case ButtonTypes.Warning:
                    button.Attributes["class"] += " btn-warning";
                    break;
                case ButtonTypes.Success:
                    button.Attributes["class"] += " btn-success";
                    break;
            }
        }
        #endregion

        #region Get Popover
        HtmlGenericControl GetPopover()
        {
            var button = new HtmlGenericControl("a")
            {
                ID = UID
            };
            button.Attributes["href"] = "javascript:void(0);";
            button.Attributes["class"] = "btn";
            ApplyType(button);
            button.Attributes["class"] += " bsn-popover";

            if (!string.IsNullOrEmpty(Caption))
            {
                button.Attributes["title"] = Caption;
            }

            var span = new HtmlGenericControl("span");

            if (string.IsNullOrEmpty(Icon))
            {
                Icon = "file-alt";
            }

            var icon = new HtmlGenericControl("span");
            icon.Attributes["class"] = "image " + Icon;
            icon.Attributes["aria-hidden"] = "true";
            button.Controls.Add(icon);

            if (Popover != null)
            {
                Popover.Attributes["class"] += " popover-content d-none";
                button.Controls.Add(Popover);
            }
            else
            {
                var content = new HtmlGenericControl("span");
                content.Attributes["class"] = "popover-content d-none";
                content.InnerHtml = Notes;
                button.Controls.Add(span);
            }

            if (!string.IsNullOrEmpty(CSS))
            {
                button.Attributes["class"] += " " + CSS.Trim();
            }

            return button;
        }
        #endregion

    }
}
