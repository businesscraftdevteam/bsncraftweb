﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;

namespace BsnCraft.Web.Common.Views.Form.Customers
{
    public class Customer : BsnForm
    {
        #region Constructor
        public Customer(BsnApplication app) : base(app)
        {
            Init(BsnForms.Customer);
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            base.AddItems();
         
            var left = new BsnFormItemLayout(this, "Left");
            Root.Add(left, 7);
            var right = new BsnFormItemLayout(this, "Right");
            Root.Add(right, 5);

            left.Add(new BsnFormItemComboBox(this, "Title") { Data = App.VM.AR.CustomerTitles, Required = true });
            left.Add(new BsnFormItemTextBox(this, "FirstName") { Caption = "First Name", Required = true });
            left.Add(new BsnFormItemTextBox(this, "Surname") { Field = "LastName", Required = true });
            left.Add(new BsnFormItemTextBox(this, "Address1") { Caption = "Address 1", Required = true });
            left.Add(new BsnFormItemTextBox(this, "Address2") { Caption = "Address 2" });
            left.Add(new BsnFormItemTextBox(this, "Suburb") { Required = true });
            left.Add(new BsnFormItemComboBox(this, "State") { Data = App.VM.AR.States, Required = true }, 8);
            left.Add(new BsnFormItemTextBox(this, "PostCode") { Caption = "P/Code", Required = true }, 4);            
            right.Add(new BsnFormItemTextBox(this, "MobilePhone") { Caption = "Mobile Phone" });
            right.Add(new BsnFormItemTextBox(this, "HomePhone") { Caption = "Home Phone" });
            right.Add(new BsnFormItemTextBox(this, "WorkPhone") { Caption = "Work Phone" });
            right.Add(new BsnFormItemTextBox(this, "FaxNumber") { Caption = "Fax Number" });
            right.Add(new BsnFormItemButtonEdit(this, "Email") { Icon = "envelope" });                        
        }
        #endregion
        
        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {                
                App.VM.AR.UpdateCustomer((CustomerContact)Data);
            }
            return true;
        }
        #endregion
    }
}
