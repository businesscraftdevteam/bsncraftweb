﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Web;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Generic
{
    public class Login : BsnForm
    {
        #region Constructor
        public Login(BsnApplication app) : base(app)
        {
            Init(BsnForms.Login);
            HideCaption = true;
            ShowCaptions = false;
            Styles["width"] = "400px";
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            base.AddItems();
            
            Root.Add(new BsnFormItemGeneric(this, "Logo") { Align = FormLayoutHorizontalAlign.Center });
            Root.Add(new BsnFormItemButtonEdit(this, "Username") { Icon = "user-alt", Required = true });
            Root.Add(new BsnFormItemButtonEdit(this, "Password") { Icon = "lock", Required = true, Password = true });
            Root.Add(new BsnFormItemComboBox(this, "Company")
            {
                Data = App.Settings.Companies,
                SelectedIndex = 0,
                Required = true
            });
        }
        #endregion

        #region Before Render
        protected override void BeforeRender()
        {
            base.BeforeRender();            
            Root.Find().Get("Logo").Child = App.Nav.LogoText();
            SetItemVisibility("Cancel", false);                
            var save = Root.Find().Get("Save");
            if (save != null)
            {
                save.SetVisible(true);
                save.Caption = "Login";
                save.Css = "form-control";
                save.Icon = "sign-in-alt";
                save.Callback.Type = CallbackType.Normal;
            }
        }
        #endregion

        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {
                var login = (Model.Login)Data;
                App.Api.Login(login.Username, login.Password, login.Company);
            }
            return false;
        }
        #endregion
    }
}
