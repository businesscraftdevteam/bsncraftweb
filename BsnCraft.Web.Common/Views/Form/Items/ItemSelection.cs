﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System.Linq;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Items
{
    public class ItemSelection : BsnForm
    {
        #region Constructor
        public ItemSelection(BsnApplication app) : base(app)
        {
            Init(BsnForms.ItemSelection);
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            Root.Add(new BsnFormItemTextBox(this, "Code") { Required = true, Caption = "Selection Code", MaxLength = 20 });
            Root.Add(new BsnFormItemTextBox(this, "Description") { Required = true, MaxLength = 50 });
            Root.Add(new BsnFormItemComboBox(this, "Option") { Required = true, Data = App.VM.IM.SelectionOptions });            
            Root.Add(new BsnFormItemMemo(this, "Query"));
        }
        #endregion

        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {
                var selection = (Model.ItemSelection)Data;
                App.VM.IM.UpdateSelection(selection);
            }
            return true;
        }
        #endregion
    }
}
