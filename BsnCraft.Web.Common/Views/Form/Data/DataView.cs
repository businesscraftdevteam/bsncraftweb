﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Data
{
    public class DataView : BsnForm
    {
        #region Constructor
        public DataView(BsnApplication app) : base(app)
        {
            Init(BsnForms.DataView);
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            Root.Add(new BsnFormItemTextBox(this, "Name") { Required = true });
            Root.Add(new BsnFormItemTextBox(this, "Description") { Required = true });
            Root.Add(new BsnFormItemComboBox(this, "DSSelect")
            {
                Caption = "Data Source",
                Field = "DataSource",
                Required = true,
                ComboText = "Desc",
                Data = App.VM.DV.DataSources
            });
            Root.Add(new BsnFormItemComboBox(this, "Chart")
            {
                Caption = "Chart Display",
                Field = "Display",
                Required = true,
                ComboText = "Desc",
                Data = App.VM.DV.ChartDisplays
            });
            Root.Add(new BsnFormItemMemo(this, "Query") { Caption = "Query String", Field = "DataSource", Required = true });
            Root.Add(new BsnFormItemMemo(this, "Layout") { Caption = "XML Layout", Field = "Display" });
            Root.Add(new BsnFormItemCheckBox(this, "Bookmarked") { Caption = "Bookmark?" });
        }
        #endregion

        #region Before Render
        protected override void BeforeRender()
        {
            base.BeforeRender();
            Root.Vertical = Type != BsnFormTypes.InGrid;            
            var dataView = (Model.DataViewItem)Data;
            var name = Root.Find().Get("NamePanel");
            if (name != null)
            {
                name.ReadOnly = !dataView.IsNew;
            }
            SetItemVisibility("DSSelectPanel", dataView.Type == Model.DataViewTypes.Chart);
            SetItemVisibility("ChartPanel", dataView.Type == Model.DataViewTypes.Chart);
            SetItemVisibility("QueryPanel", dataView.Type != Model.DataViewTypes.Chart);
            SetItemVisibility("LayoutPanel", dataView.Type != Model.DataViewTypes.Chart);
        }
        #endregion

        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {
                var dataView = (Model.DataViewItem)Data;
                App.VM.DV.Save(dataView);
            }
            return true;
        }
        #endregion
    }
}
