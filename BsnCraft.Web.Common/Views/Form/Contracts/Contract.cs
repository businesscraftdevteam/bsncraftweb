﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Contracts
{
    public class Contract : BsnForm
    {
        #region Constructor
        public Contract(BsnApplication app) : base(app)
        {
            Init(BsnForms.Contract);
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            base.AddItems();

            Root.Add(new BsnFormItemComboBox(this, "CustomerNumber")
            {
                Caption = "Customer",
                Data = App.VM.AR.CustomerCombo,
                Required = true
            });

            var left = new BsnFormItemLayout(this, "Left") { Span = 7 };
            Root.Add(left);
            var right = new BsnFormItemLayout(this, "Right") { Span = 5 };
            Root.Add(right);

            left.Add(new BsnFormItemTextBox(this, "LotNumber") { Caption = "Lot No", Required = true }, 3);
            left.Add(new BsnFormItemTextBox(this, "StreetNumber") { Caption = "Street No" }, 3);
            left.Add(new BsnFormItemTextBox(this, "Address") { Caption = "Street", Required = true }, 6);
            left.Add(new BsnFormItemComboBox(this, "State") { Data = App.VM.CO.States, Required = true }, 8);
            left.Add(new BsnFormItemTextBox(this, "PostCode") { Caption = "P/Code", Required = true }, 4);
            left.Add(new BsnFormItemTextBox(this, "Comment1") { Caption = "Comment 1" });
            left.Add(new BsnFormItemTextBox(this, "Comment2") { Caption = "Comment 2" });
            left.Add(new BsnFormItemTextBox(this, "Comment3") { Caption = "Comment 3" });
            left.Add(new BsnFormItemButtonEdit(this, "House") { Icon = "search" });
            left.Add(new BsnFormItemButtonEdit(this, "Facade") { Icon = "search" });
            left.Add(new BsnFormItemTextBox(this, "DpNumber") { Caption = "DP Number" }, 4);
            left.Add(new BsnFormItemTextBox(this, "Volume"), 4);
            left.Add(new BsnFormItemTextBox(this, "Folio"), 4);

            right.Add(new BsnFormItemComboBox(this, "OpCentre")
            {
                Caption = "Op Centre",
                Data = App.VM.CO.OpCentres,
                Required = true
            });
            right.Add(new BsnFormItemComboBox(this, "District")
            {
                Data = App.VM.CO.Districts,
                Required = true
            });
            right.Add(new BsnFormItemComboBox(this, "Council")
            {
                Data = App.VM.CO.Councils,
                Required = true
            });
            right.Add(new BsnFormItemComboBox(this, "SalesConsultant")
            {
                Caption = "Sales Consultant",
                Data = App.VM.CO.SalesConsultants,
                Required = true
            });
            right.Add(new BsnFormItemComboBox(this, "SalesPromotion")
            {
                Caption = "Sales Promotion",
                Data = App.VM.CO.SalesPromotions,
                Required = true
            });
            right.Add(new BsnFormItemComboBox(this, "SalesCentre")
            {
                Caption = "Sales Centre",
                Data = App.VM.CO.SalesCentres,
                Required = true
            });
            right.Add(new BsnFormItemComboBox(this, "SaleType")
            {
                Caption = "Sales Type",
                Data = App.VM.CO.SaleTypes,
                Required = true
            });
            right.Add(new BsnFormItemComboBox(this, "Status")
            {
                Required = true
            });
            right.Add(new BsnFormItemCheckBox(this, "SubjectSale") { Caption = "Subject Sale?" }, 6);
            right.Add(new BsnFormItemCheckBox(this, "SavingsPlan") { Caption = "Saving Plan?" }, 6);
        }
        #endregion
        
        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {
                App.VM.CO.UpdateContract((Model.Contract)Data);
            }
            return true;
        }
        #endregion

        #region Before Render
        protected override void BeforeRender()
        {
            base.BeforeRender();
            if (Data is Model.Contract contract)
            {
                var status = Root.Find().Get("Status");
                if (status != null)
                {
                    var visible = (contract.IsLead) ? App.VM.LE.MultiStatusCodes : App.VM.CO.MultiStatusCodes;
                    SetItemVisibility("StatusPanel", visible);
                    if (visible)
                    {
                        status.Data = (contract.IsLead) ? App.VM.CO.LeadStatusCodes : App.VM.CO.ContractStatusCodes;
                    }
                }
            }
        }
        #endregion
    }
}
