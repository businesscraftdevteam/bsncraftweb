﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System.Linq;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Contracts
{
    public class DealSubmit : BsnForm
    {
        #region Constructor
        public DealSubmit(BsnApplication app) : base(app)
        {
            Init(BsnForms.DealSubmit);
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            Root.Add(new BsnFormItemComboBox(this, "Type")
            {
                Required = true,
                Caption = "Document Type",
                Data = App.VM.CO.DocumentTypes
            });
            Root.Add(new BsnFormItemTextBox(this, "Reference") { Required = true });
            Root.Add(new BsnFormItemTextBox(this, "Description") { Required = true });
            Root.Add(new BsnFormItemCheckBox(this, "Required") { Caption = "Required?" });
        }
        #endregion

        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {
                var dealDocument = (Model.DealDocument)Data;
                if (App.Settings.Company.DealDocuments.Any(p => p.Key.Equals(dealDocument)))
                {
                    var previous = App.Settings.Company.DealDocuments
                        .Where(p => p.Key.Equals(dealDocument)).FirstOrDefault();
                    App.Settings.Company.DealDocuments.Remove(previous);
                }
                App.Settings.Company.DealDocuments.Add(dealDocument);
                App.Settings.SaveComp();
            }
            return true;
        }
        #endregion
    }
}
