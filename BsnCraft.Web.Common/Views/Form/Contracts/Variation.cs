﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Contracts
{
    public class Variation : BsnForm
    {
        #region Constructor
        public Variation(BsnApplication app) : base(app)
        {
            Init(BsnForms.Variation);
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            Root.Add(new BsnFormItemTextBox(this, "Date") { ReadOnly = true }, 3);
            Root.Add(new BsnFormItemTextBox(this, "Reference") { Required = true }, 3);
            Root.Add(new BsnFormItemTextBox(this, "Description") { Required = true }, 6);

            var left = new BsnFormItemLayout(this, "Left") { Caption = "Amount" };
            Root.Add(left, 6);
            
            left.Add(new BsnFormItemTextBox(this, "Amount") { Caption = " ", ReadOnly = true, DisplayFormat = "C" });
            left.Add(new BsnFormItemTextBox(this, "GSTAmount") { ReadOnly = true, DisplayFormat = "C" });
            left.Add(new BsnFormItemTextBox(this, "IncAmount") { Required = true, DisplayFormat = "C" });

            var right = new BsnFormItemLayout(this, "Right") { Caption = " " };
            Root.Add(right, 6);
        }
        #endregion

        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {
                App.VM.VA.UpdateVariation((Model.Variation)Data);
            }
            return true;
        }
        #endregion
    }
}
