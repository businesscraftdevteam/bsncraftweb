﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Contracts
{
    public class Event : BsnForm
    {
        #region Constructor
        public Event(BsnApplication app) : base(app)
        {
            Init(BsnForms.Event);
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            Root.Add(new BsnFormItemDateEdit(this, "Actual"), 4);
            Root.Add(new BsnFormItemPanel(this, "EmptyPanel"), 8);
            Root.Add(new BsnFormItemDateEdit(this, "Forecast"), 4);
            Root.Add(new BsnFormItemDateEdit(this, "Due"), 4);
            Root.Add(new BsnFormItemDateEdit(this, "Registered"), 4);
            Root.Add(new BsnFormItemComboBox(this, "Employee") { Data = App.VM.CO.Employees }, 4);
            Root.Add(new BsnFormItemTextBox(this, "Reference"), 4);
            Root.Add(new BsnFormItemTextBox(this, "Amount"), 4);
            Root.Add(new BsnFormItemMemo(this, "Notes"));
        }
        #endregion

        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {
                App.VM.CO.UpdateEvent((Model.Event)Data);
            }
            return true;
        }
        #endregion
    }
}
