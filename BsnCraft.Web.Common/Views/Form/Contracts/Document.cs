﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Contracts
{
    public class Document : BsnForm
    {
        #region Constructor
        public Document(BsnApplication app) : base(app)
        {
            Init(BsnForms.Document);
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            Root.Add(new BsnFormItemComboBox(this, "Type")
            {
                Caption = "Document Type",
                Data = App.VM.CO.DocumentTypes,
                Required = true
            }, 6);
            Root.Add(new BsnFormItemTextBox(this, "Reference") { MaxLength = 8 }, 3);
            Root.Add(new BsnFormItemTextBox(this, "XRefKey") { Caption = "Key Link", MaxLength = 8 }, 3);
            Root.Add(new BsnFormItemTextBox(this, "Description") { MaxLength = 50 }, 6);
            Root.Add(new BsnFormItemTextBox(this, "Filename") { ReadOnly = true }, 6);
            Root.Add(new BsnFormItemMemo(this, "Notes"));
        }
        #endregion
        
        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {
                var document = (Model.Document)Data;
                if (string.IsNullOrEmpty(document.Type))
                {
                    App.Utils.SetError("Cannot add document. Document type is blank");
                    return false;
                }
                if (App.UploadedFiles.Count == 0)
                {
                    App.VM.CO.UpdateDocument(document);
                }
                else
                {
                    foreach (var uploadFile in App.UploadedFiles)
                    {
                        document.UploadFile = uploadFile;
                        App.VM.CO.UpdateDocument(document);
                    }
                }
                App.UploadedFiles.Clear();
            }
            return true;
        }
        #endregion
    }
}
