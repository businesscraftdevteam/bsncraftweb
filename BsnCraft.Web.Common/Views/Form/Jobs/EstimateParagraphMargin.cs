using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System.Collections.Generic;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Jobs
{

    public class EstimateParagraphMargin : BsnForm
    {
        
        #region Private Properties        
        private string PrevMarginCode { get; set; }
        private decimal PrevMarginPct { get; set; }
        #endregion

        #region Constructor
        public EstimateParagraphMargin(BsnApplication app) : base(app)
        {
            Init(BsnForms.EstimateParagraphMargin);
            UpdateCallback.Data.Mode = BsnDataModes.SetMargin;
        }
        #endregion

        #region Create
        public override void Create()
        {
            var model = (Model.Paragraph)Data;
            PrevMarginCode = (model != null) ? model.MarginCode : "";
            PrevMarginPct = (model != null) ? model.DefMarginPct : 0;
            
            base.Create();
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            base.AddItems();
            Root.Add(new BsnFormItemComboBox(this, "MarginCode")
            {
                Caption = "Margin Code",
                Data = App.VM.JC.MarginCodes,
                UpdateForm = true
            });
            Root.Add(new BsnFormItemTextBox(this, "DefMarginPct")
            {
                Caption ="Margin %",
                DisplayFormat = "F2",
                UpdateForm = true
            });

        }
        #endregion

        #region Form Update
        protected override void UpdateCheck()
        {
            base.UpdateCheck();
            var model = (Model.Paragraph)Data;
            if (model.MarginCode != PrevMarginCode && !string.IsNullOrEmpty(model.MarginCode))
            {
                model.DefMarginPct = 0;
            }
            if (model.DefMarginPct != PrevMarginPct)
            {
                model.MarginCode = string.Empty;
            }
        }
        #endregion

        #region Before Render
        protected override void BeforeRender()
        {
            base.BeforeRender();
            var model = (Model.Paragraph)Data;
            UpdateCallback.Data.Value = model.Code;
        }
        #endregion

        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {
                var mode = 4;
                return App.Api.UpdateParagraph(mode, (Model.Paragraph)Data);
            }
            return true;
        }
        #endregion

        #region AfterProcess
        public override void AfterProcess(CallbackData data)
        {
            base.AfterProcess(data);
            App.DataLocation.Key = App.Key;
        }
        #endregion

    }

}
