﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System.Collections.Generic;
using System.Linq;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Jobs
{
    public class EstimateDetail : BsnForm
    {
        #region Public Properties                
        public bool ShowAmounts { get; set; }
        public bool AdvancedMode { get; set; }
        public bool ShowMargin { get; set; }
        public bool NotesOnly { get; set; }
        #endregion

        #region Private Properties
        private string PreviousParagraph { get; set; }
        private string PreviousItem { get; set; }
        #endregion

        #region Constructor
        public EstimateDetail(BsnApplication app) : base(app)
        {            
            Init(BsnForms.EstimateDetail);
        }
        #endregion

        #region Create
        public override void Create()
        {
            var model = (Model.EstimateDetail)Data;
            PreviousParagraph = (model != null) ? model.ParagraphCode : "";
            PreviousItem = (model != null) ? model.Item : "";
            base.Create();
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            base.AddItems();
            Fields["Paragraph"] = new BsnFormItemComboBox(this, "ParagraphCode") { Caption = "Paragraph", Required = true, UpdateForm = true };
            Fields["Item"] = new BsnFormItemComboBox(this, "Item") { Data = App.VM.IM.ItemList, UpdateForm = true };
            Fields["Description"] = new BsnFormItemTextBox(this, "Description") { Required = true };
            Fields["Included"] = new BsnFormItemCheckBox(this, "Included") { Caption = "Included" };
            Fields["Quantity"] = new BsnFormItemTextBox(this, "Quantity") { DisplayFormat = "F3" };
            Fields["UnitCost"] = new BsnFormItemTextBox(this, "UnitCost") { Caption = "Cost: Unit", DisplayFormat = "C" };
            Fields["ExtdCost"] = new BsnFormItemTextBox(this, "ExtendedCost") { Caption = "Extd", ReadOnly = true, DisplayFormat = "C" };
            Fields["MarginPct"] = new BsnFormItemTextBox(this, "MarginPct") { Caption = "Margin: Pct", DisplayFormat = "F2" };
            Fields["MarginCode"] = new BsnFormItemComboBox(this, "MarginCode") { Caption = "Cd", Data = App.VM.JC.MarginCodes };
            Fields["UnitEx"] = new BsnFormItemTextBox(this, "UnitEx") { Field = "UnitAmountEx", Caption = "Sell: Unit Ex", DisplayFormat = "C" };
            Fields["GST"] = new BsnFormItemTextBox(this, "GST") { ReadOnly = true, Field = "UnitAmountGst", Caption = "GST", DisplayFormat = "C" };
            Fields["UnitInc"] = new BsnFormItemTextBox(this, "UnitInc") { Field = "UnitAmountInc", Caption = "Sell: Unit Inc", DisplayFormat = "C" };
            Fields["Extd"] = new BsnFormItemTextBox(this, "Extd") { ReadOnly = true, Field = "ExtendedAmountInc", Caption = "Extd", DisplayFormat = "C" };
            Fields["UOM"] = new BsnFormItemComboBox(this, "UOM") { Field = "UnitOfMeasure", Caption = "UOM", Data = App.VM.JC.UOMCodes };
            Fields["PriceDisplay"] = new BsnFormItemComboBox(this, "PriceDisplay") { Field = "PriceDisplay", Caption = "PD", Data = App.VM.JC.PDCodes };
            Fields["Seq"] = new BsnFormItemTextBox(this, "Sequence") { Field = "Sequence", Caption = "Seq" };
            Fields["Sort"] = new BsnFormItemTextBox(this, "Sort") { Field = "SortOrder", Caption = "Sort" };
            Fields["Notes"] = new BsnFormItemMemo(this, "Notes") { Rows = 10 };
            if (Type == BsnFormTypes.InModal)
            {
                ModalLayout();
            }            
            else
            {
                GridLayout();
            }
        }
        #endregion

        #region Layouts

        #region GridLayout
        void GridLayout()
        {
            Root.Add(Fields["Paragraph"]);
            Root.Add(Fields["Item"]);
            Root.Add(Fields["Quantity"], 7);
            Root.Add(Fields["Included"], 5);
            Root.Add(Fields["Description"]);
            Root.Add(Fields["UnitCost"], 7);
            Root.Add(Fields["ExtdCost"], 5);
            Root.Add(Fields["MarginPct"], 7);
            Root.Add(Fields["MarginCode"], 5);
            Root.Add(Fields["UnitEx"], 7);
            Root.Add(Fields["GST"], 5);
            Root.Add(Fields["UnitInc"], 7);
            Root.Add(Fields["Extd"], 5);
            Root.Add(Fields["UOM"], 7);
            Root.Add(Fields["PriceDisplay"], 5);
            Root.Add(Fields["Seq"], 7);
            Root.Add(Fields["Sort"], 5);
            Root.Add(Fields["Notes"]);
        }
        #endregion

        #region Modal Layout
        void ModalLayout()
        {
            Root.Add(Fields["Paragraph"]);
            Root.Add(Fields["Item"], 6);
            Root.Add(Fields["Included"], 6);
            Root.Add(Fields["Description"]);

            var unitCostLayout = new BsnFormItemLayout(this, "UnitCostLayout") { Css = "row" };
            Root.Add(unitCostLayout, 6);
            unitCostLayout.Add(Fields["UnitCost"]);
            unitCostLayout.Add(Fields["ExtdCost"]);

            var marginLayout = new BsnFormItemLayout(this, "MarginLayout") { Css = "row" };
            Root.Add(marginLayout, 6);
            marginLayout.Add(Fields["MarginPct"]);
            marginLayout.Add(Fields["MarginCode"]);
            
            Root.Add(Fields["Quantity"], 6);
            Root.Add(Fields["UnitEx"], 6);

            var unitGstLayout = new BsnFormItemLayout(this, "GSTLayout") { Css = "row" };
            Root.Add(unitGstLayout);
            unitGstLayout.Add(Fields["GST"]);
            unitGstLayout.Add(Fields["UOM"]);

            Fields["Notes"].Rows = 16;
            Root.Add(Fields["Notes"]);
        }
        #endregion

        #endregion

        #region Form Update
        protected override void UpdateCheck()
        {
            base.UpdateCheck();
            var model = (Model.EstimateDetail)Data;            
            if (model.ParagraphCode != PreviousParagraph)
            {
                model.SetParagraph(model.ParagraphCode);
            }
            if (model.Item != PreviousItem)
            {
                App.VM.QT.UpdateEstimateDetail(model, false, true);
                model.Notes = App.Odbc.Spec("ITMLIND", "ILD_ITEM", model.Item, "ILD_DESC");
            }
        }
        #endregion
        
        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            var model = (Model.EstimateDetail)Data;
            var contract = model.Paragraph.Heading.Contract;
            if (data.Mode == BsnDataModes.FormSave)
            {
                App.VM.QT.UpdateEstimateDetail(model);
                if (Type != BsnFormTypes.InGrid)                
                {
                    contract.ReloadParagraphDetails(model.Paragraph);
                }
                App.VM.QT.ReloadEstimateDetail(model);
            }
            return true;
        }
        #endregion

        #region AfterProcess
        public override void AfterProcess(CallbackData data)
        {
            base.AfterProcess(data);
            App.DataLocation = App.Menus.Current.GetLocation();
            App.DataLocation.Type = CallbackType.Normal;
        }
        #endregion

        #region Before Render
        protected override void BeforeRender()
        {
            base.BeforeRender();

            var paragraph = Root.Find().Get("ParagraphCode");
            var itemCombo = Root.Find().Get("Item");
            var model = (Model.EstimateDetail)Data;

            if (paragraph != null)
            {
                paragraph.Data = App.VM.JC.ParagraphCombo(model);
                paragraph.ReadOnly = (!model.IsNew);
            }

            if (itemCombo != null)
            {
                if (!string.IsNullOrEmpty(model.Options))
                {
                    itemCombo.Data = App.VM.IM.GetSelectionItemList(model.Options);
                }
                else
                {
                    if (model.Paragraph != null)
                    {
                        itemCombo.Data = App.VM.IM.GetSelectionItemList(model.Paragraph.Options);
                    }
                    else
                    {
                        itemCombo.Data = App.VM.IM.ItemList;
                    }
                }
            }

            foreach (var item in Root.Find().Where(p => p.Type.Equals(FormTypes.Layout)))
            {
                item.SetVisible(!NotesOnly);
            }

            if (NotesOnly)
            {
                SetItemVisibility("NotesPanel", true);
                return;
            }

            if (model.ShowCheckbox && !model.ShowQuantity)
            {
                Fields["Included"].ShowCaption = true;
            }

            SetItemVisibility("UnitCostWrap", Type == BsnFormTypes.InModal && AdvancedMode && ShowAmounts);
            SetItemVisibility("MarginWrap", Type == BsnFormTypes.InModal && AdvancedMode && ShowAmounts && ShowMargin);
            SetItemVisibility("ItemPanel", model.ShowSelect);
            SetItemVisibility("IncludedPanel", model.ShowCheckbox);
            SetItemVisibility("QuantityPanel", model.ShowQuantity);
            SetItemVisibility("UnitExPanel", ShowAmounts);
            SetItemVisibility("GSTPanel", ShowAmounts);
            SetItemVisibility("UnitIncPanel", ShowAmounts);
            SetItemVisibility("ExtdPanel", ShowAmounts);
            SetItemVisibility("ExtendedCostPanel", AdvancedMode && ShowAmounts);
            SetItemVisibility("UnitCostPanel", AdvancedMode && ShowAmounts);
            SetItemVisibility("ExtendedCostPanel", AdvancedMode && ShowAmounts);
            SetItemVisibility("MarginPctPanel", AdvancedMode && ShowAmounts && ShowMargin);
            SetItemVisibility("MarginCodePanel", AdvancedMode && ShowAmounts && ShowMargin);
            SetItemVisibility("SequencePanel", AdvancedMode);
            SetItemVisibility("SortPanel", AdvancedMode);
        }
        #endregion
    }
}
