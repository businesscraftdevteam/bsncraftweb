﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Web;
using System.Linq;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Jobs
{
    public class AreaPriceSelection : BsnForm
    {
        #region Constructor
        public AreaPriceSelection(BsnApplication app) : base(app)
        {
            Init(BsnForms.AreaPriceSelection);
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            Root.Add(new BsnFormItemComboBox(this, "Area")
            {
                Field = "Area",
                Required = true,
                Data = App.VM.APR.Areas
            }, 4);
            Root.Add(new BsnFormItemDateEdit(this, "Date")
            {
                Field = "EffectiveDate",
                Caption = "Date",
                Required = true,
            }, 3);
            Root.Add(new BsnFormItemCheckBox(this, "IncMissing")
            {
                Field = "IncludeMissing",
                Caption = "Missing Prices",
                ShowCaption = true,
                PanelCaption = "Include?"
            }, 3);

            Root.Add(new BsnFormItemProgress(this, "Progress")
            {
                Field = "Progress",
                Caption = "Progress",
                ShowCaption = true,
                PanelCaption = "Progress"
            });
        }
        #endregion

        #region Before Render
        protected override void BeforeRender()
        {
            base.BeforeRender();
            Root.Vertical = false;
            var buttons = Root.Find().Get("Buttons");
            if (buttons != null)
            {
                buttons.Span = 2;
                buttons.Css = "d-flex align-items-end";
            }
            SetItemVisibility("Cancel", false);
            var save = Root.Find().Get("Save");
            if (save != null)
            {
                save.SetVisible(true);
                save.Caption = "Load";
            }

            var progress = Root.Find().Get("ProgressPanel");
            if (progress != null)
            {
                progress.SetVisible(false);
            }

        }
        #endregion

        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {
                var selection = (Model.AreaPriceSelection)Data;
                App.VM.APR.UpdateSelection(selection);
            }
            return true;
        }
        #endregion
    }
}
