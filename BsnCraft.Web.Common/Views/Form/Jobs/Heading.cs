﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Jobs
{
    public class Heading : BsnForm
    {
        #region Public Properties        
        public bool IsModal { get; set; }
        public bool IsSetMarginForm{ get; set; }
        public Model.Heading CopyFrom { get; set; }
        public string PrevMarginCode { get; set; }
        public decimal PrevMarginPct { get; set; }
        #endregion

        #region Constructor
        public Heading(BsnApplication app) : base(app)
        {
            Init(BsnForms.Heading);
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            Root.Add(new BsnFormItemTextBox(this, "Code") { Required = true, MaxLength = 4, ReadOnly = Mode == BsnModes.Edit });
            Root.Add(new BsnFormItemTextBox(this, "Description") { Required = true, MaxLength = 30 , ReadOnly = IsSetMarginForm});
            Root.Add(new BsnFormItemComboBox(this, "MarginCode")
            {
                Caption = "Margin Code",
                Data = App.VM.JC.MarginCodes,
                Callback = UpdateCallback
            });
            Root.Add(new BsnFormItemTextBox(this, "DefMarginPct") { Caption = "Margin %", DisplayFormat = "F2"});
            Root.Find().Get("DefMarginPctPanel").SetVisible(IsSetMarginForm);
            Root.Find().Get("MarginCodePanel").SetVisible(IsSetMarginForm);
            
        }
        #endregion

        #region Form Update
        protected override void UpdateCheck()
        {
            base.UpdateCheck();
            var model = (Model.Heading)Data;
            if (model.MarginCode != PrevMarginCode && !string.IsNullOrEmpty(model.MarginCode))
            {
                model.DefMarginPct = 0;
            }
            if (model.DefMarginPct != PrevMarginPct)
            {
                model.MarginCode = string.Empty;
            }
        }
        #endregion


        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            var mode = 1;
            if (data.Mode == BsnDataModes.FormSave)
            {
                if (IsSetMarginForm)
                {
                    mode = 4;
                }
                App.VM.JC.UpdateHeading((Model.Heading)Data, data.Value ,mode);
            }
            return true;
        }
        #endregion
    }
}
