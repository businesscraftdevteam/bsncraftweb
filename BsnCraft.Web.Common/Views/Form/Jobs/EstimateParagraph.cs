using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using System.Collections.Generic;
using Model = BsnCraft.Web.Common.Models;

namespace BsnCraft.Web.Common.Views.Form.Jobs
{

    public class EstimateParagraph : BsnForm
    {
        #region Public Properties        
        public Model.Paragraph CopyFrom { get; set; }
        #endregion

        #region Constructor
        public EstimateParagraph(BsnApplication app) : base(app)
        {
            Init(BsnForms.EstimateParagraph);
        }
        #endregion

        #region Create
        public override void Create()
        {
            var model = (Model.Paragraph)Data;
            base.Create();
        }
        #endregion

        #region AddItems
        protected override void AddItems()
        {
            base.AddItems();
            Root.Add(new BsnFormItemTextBox(this, "Code")
            {
                Caption = "Paragraph Code",
                Required = true,
                MaxLength = 4
            });
            Root.Add(new BsnFormItemTextBox(this, "Description")
            {
                MaxLength = 30,
            });
        }
        #endregion

        #region Process
        public override bool Process(CallbackData data)
        {
            base.Process(data);
            if (data.Mode == BsnDataModes.FormSave)
            {
                var mode = 2;
                if (((Model.Paragraph)Data).IsNew) mode = 1;
                App.VM.JC.UpdateParagraph((Model.Paragraph)Data, mode);
            }
            return true;
        }
        #endregion

        #region AfterProcess
        public override void AfterProcess(CallbackData data)
        {
            base.AfterProcess(data);
            App.DataLocation.Key = App.Key;
        }
        #endregion

    }

}
