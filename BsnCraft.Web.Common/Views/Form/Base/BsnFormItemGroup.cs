﻿using DevExpress.Utils;
using DevExpress.Web.Bootstrap;
using System.Linq;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemGroup : BsnFormItemBase
    {
        #region IsContainer
        public override bool IsContainer
        {
            get
            {               
                return true;
            }
        }
        #endregion
        
        #region Constructor
        public BsnFormItemGroup(BsnForm form, string key) 
            : base(form, key, FormTypes.Group)
        {
        }
        #endregion
        
        #region Reset
        public override void Reset()
        {
            base.Reset();
            group = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            Group.Caption = Caption;
            Group.ColSpanMd = Span;
            Group.HorizontalAlign = Align;
            Group.Visible = Visible;
            Group.CssClasses.Control = Css;
            Group.CssClasses.GroupHeader = "text-secondary";
            if (string.IsNullOrEmpty(Caption))
            {
                if (Items.Count == 1 && Items[0].ShowCaption)
                {
                    Group.Caption = Items[0].Caption;
                }
                else
                {
                    Group.Caption = " ";
                }
            }
            if (Group.Caption == " ")
            {
                Group.ShowCaption = DefaultBoolean.False;
            }
            foreach (var item in Items.Where(p => p.IsContainer && p.Type.Equals(FormTypes.Panel)))
            {
                var panel = (BsnFormItemPanel)item;
                Group.Items.Add(panel.Panel);
            }
        }
        #endregion

        #region Group
        BootstrapLayoutGroup group;
        public BootstrapLayoutGroup Group
        {
            get
            {
                if (group == null)
                {
                    group = new BootstrapLayoutGroup();                    
                }                                
                return group;
            }
        }
        #endregion

    }
}
