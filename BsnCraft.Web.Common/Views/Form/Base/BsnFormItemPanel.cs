﻿using DevExpress.Utils;
using DevExpress.Web.Bootstrap;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemPanel : BsnFormItemBase
    {
        #region IsContainer
        public override bool IsContainer
        {
            get
            {               
                return true;
            }
        }
        #endregion
        
        #region Constructor
        public BsnFormItemPanel(BsnForm form, string key) 
            : base(form, key, FormTypes.Panel)
        {
            ShowCaption = false;
        }
        #endregion
        
        #region Reset
        public override void Reset()
        {
            base.Reset();
            panel = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            Panel.Caption = Caption;
            Panel.ColSpanMd = Span;
            Panel.HorizontalAlign = Align;
            Panel.Visible = Visible;
            Panel.CssClasses.Control = Css;
            Panel.CssClasses.Caption = "text-secondary";
            if (string.IsNullOrEmpty(Caption))
            {
                if (Items.Count == 1 && Items[0].ShowCaption)
                {
                    Panel.Caption = Items[0].Caption;
                }
                else
                {
                    Panel.Caption = " ";
                }
            }
            if (Panel.Caption == " ")
            {
                Panel.ShowCaption = DefaultBoolean.False;
            }
            Panel.Controls.Clear();
            foreach (var item in Items)
            {
                Panel.Controls.Add(item.ToControl());
            }
        }
        #endregion

        #region Panel
        private BootstrapLayoutItem panel;
        public BootstrapLayoutItem Panel
        {
            get
            {
                if (panel == null)
                {
                    panel = new BootstrapLayoutItem();                    
                }                
                return panel;
            }
        }
        #endregion

    }
}
