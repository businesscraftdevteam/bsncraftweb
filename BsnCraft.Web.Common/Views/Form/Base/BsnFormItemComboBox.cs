﻿using BsnCraft.Web.Common.Classes;
using DevExpress.Web.Bootstrap;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemComboBox : BsnFormItemBase
    {
        #region Constructor
        public BsnFormItemComboBox(BsnForm form, string key) 
            : base(form, key, FormTypes.ComboBox)
        {
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            base.ToControl();
            return ComboBox;
        }
        #endregion
        
        #region Value
        public override string Value
        {
            get
            {
                return ComboBox.Value.ValueOrDefault(string.Empty);
            }
        }
        #endregion        

        #region Reset
        public override void Reset()
        {
            base.Reset();
            comboBox = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            ComboBox.ID = Key + Form.FormUID;
            ComboBox.ClientInstanceName = ClientInstanceName;
            ComboBox.ReadOnly = ReadOnly.IfFalse(Form.ReadOnly);
            ComboBox.Visible = Visible;
            ComboBox.Caption = Caption;
            ComboBox.ValueType = typeof(string);
            ComboBox.ValueField = ComboValue;
            ComboBox.TextField = ComboText;
            ComboBox.ClientSideEvents.ValueChanged = Callback == null ? "App.combobox" :
                "function(s,e) { App.combobox(s, e, '" + Callback.ToString() + "'); } ";
            ComboBox.ValidationSettings.ValidationGroup = Form.Key + "Validation";
            ComboBox.ValidationSettings.RequiredField.IsRequired = Required;
            ComboBox.ValidationSettings.RequiredField.ErrorText = Error;
            ComboBox.CaptionSettings.Hidden = Form.ShowCaptions;
            if (SelectedIndex != -1)
            {
                ComboBox.SelectedIndex = SelectedIndex;
            }
            else
            {
                ComboBox.Value = GetValue;
            }
            ComboBox.DataSource = Data;
            ComboBox.DataBind();
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            SetValue(ComboBox.Value.ValueOrDefault(string.Empty));            
        }
        #endregion

        #region ComboBox
        private BootstrapComboBox comboBox;
        public BootstrapComboBox ComboBox
        {
            get
            {
                if (comboBox == null)
                {
                    comboBox = new BootstrapComboBox();
                }
                return comboBox;
            }
        }
        #endregion

    }
}
