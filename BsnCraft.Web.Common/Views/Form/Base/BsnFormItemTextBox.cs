﻿using BsnCraft.Web.Common.Classes;
using DevExpress.Web.Bootstrap;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemTextBox : BsnFormItemBase
    {
        #region Constructor
        public BsnFormItemTextBox(BsnForm form, string key) 
            : base(form, key, FormTypes.TextBox)
        {
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            base.ToControl();
            return TextBox;
        }
        #endregion
        
        #region Value
        public override string Value
        {
            get
            {
                return TextBox.Text;
            }
        }
        #endregion        

        #region Reset
        public override void Reset()
        {
            base.Reset();
            textBox = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            TextBox.ID = Key + Form.FormUID;
            TextBox.ClientInstanceName = ClientInstanceName;
            TextBox.Visible = Visible;
            TextBox.Caption = Caption;
            TextBox.Password = Password;
            TextBox.ReadOnly = ReadOnly.IfFalse(Form.ReadOnly);
            TextBox.MaxLength = MaxLength;
            TextBox.ClientSideEvents.ValueChanged = "App.textbox";
            TextBox.ValidationSettings.ValidationGroup = Form.Key + "Validation";
            TextBox.ValidationSettings.RequiredField.IsRequired = Required;
            TextBox.ValidationSettings.RequiredField.ErrorText = Error;
            TextBox.CaptionSettings.Hidden = Form.ShowCaptions;
            TextBox.CssClasses.Control = Css;
            TextBox.Text = GetValue;
            TextBox.DataBind();
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            SetValue(TextBox.Text);            
        }
        #endregion

        #region TextBox
        private BootstrapTextBox textBox;
        public BootstrapTextBox TextBox
        {
            get
            {
                if (textBox == null)
                {
                    textBox = new BootstrapTextBox();
                }
                return textBox;
            }

        }
        #endregion

    }
}
