﻿using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using System.Linq;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemLayout : BsnFormItemBase
    {
        #region IsContainer
        public override bool IsContainer
        {
            get
            {               
                return true;
            }
        }
        #endregion
        
        #region Constructor
        public BsnFormItemLayout(BsnForm form, string key) 
            : base(form, key, FormTypes.Layout)
        {
            ShowCaption = false;
        }
        #endregion
        
        #region Reset
        public override void Reset()
        {
            base.Reset();
            layout = null;
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            base.ToControl();
            Load();
            foreach (var item in Items.Where(p => p.IsContainer))
            {
                switch (item.Type)
                {
                    case FormTypes.Panel:
                        var panel = (BsnFormItemPanel)item;
                        Layout.Items.Add(panel.Panel);
                        break;
                    case FormTypes.Group:
                        var group = (BsnFormItemGroup)item;
                        Layout.Items.Add(group.Group);
                        break;
                    case FormTypes.Tab:
                        var tab = (BsnFormItemTab)item;
                        Layout.Items.Add(tab.Tab);
                        break;
                }
            }
            Layout.DataBind();
            return Layout;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            Layout.ID = Key + Form.FormUID;
            Layout.OptionalMark = string.Empty;
            Layout.RequiredMark = "*";
            Vertical |= Form.Root.Vertical;
            Layout.LayoutType = (Vertical) ? FormLayoutType.Vertical : FormLayoutType.Horizontal;            
            Layout.SettingsItems.ShowCaption = (Form.ShowCaptions) ? DefaultBoolean.True : DefaultBoolean.False;
            Layout.RequiredMarkDisplayMode = (Form.ShowCaptions) ? RequiredMarkMode.Auto : RequiredMarkMode.None;
            Layout.CssClasses.Control = Css;
            Layout.Style.Clear();
            foreach (var style in Form.Styles)
            {
                Layout.Style.Add(style.Key, style.Value);
            }
        }
        #endregion

        #region Layout
        private BootstrapFormLayout layout;
        public BootstrapFormLayout Layout
        {
            get
            {
                if (layout == null)
                {
                    layout = new BootstrapFormLayout();                    
                }               
                return layout;
            }
        }
        #endregion
    }
}
