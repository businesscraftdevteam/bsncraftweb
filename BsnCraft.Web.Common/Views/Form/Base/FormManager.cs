﻿using BsnCraft.Web.Common.Classes;
using NLog;
using System.Collections.Generic;
using System.Linq;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class FormManager
    {
        #region Forms - Add here        
        private List<BsnForm> forms;
        private List<BsnForm> Forms
        {
            get
            {
                if (forms == null || forms.Count == 0)
                {
                    forms = new List<BsnForm>()
                    {
                        new Generic.Login(App),                        
                        new Customers.Customer(App),
                        new Contracts.Contract(App),                        
                        new Contracts.Document(App),
                        new Contracts.Event(App),
                        new Contracts.Variation(App),
                        new Contracts.DealSubmit(App),
                        new Jobs.Heading(App),
                        new Jobs.EstimateDetail(App),
                        new Jobs.EstimateParagraph(App),
                        new Jobs.EstimateParagraphMargin(App),
                        new Jobs.AreaPriceSelection(App),
                        new Data.DataView(App),
                        new Items.ItemSelection(App)
                        
                    };
                }
                return forms;
            }
        }
        #endregion

        #region Private Properties
        //private Logger Log { get; set; }
        private BsnApplication App { get; set; }
        private List<BsnForm> CreatedForms { get; set; }
        #endregion

        #region Constructor
        public FormManager(BsnApplication app)
        {
            //Add back if needed
            //Log = LogManager.GetLogger(GetType().FullName);
            App = app;
            CreatedForms = new List<BsnForm>();
        }
        #endregion

        #region Init
        public void Init()
        {
            Forms.Clear();
        }
        #endregion

        #region Get
        public BsnForm Get(BsnForms view)
        {
            return Forms.Find(p => p.Key.Equals(view));
        }
        #endregion

        #region Create
        public void Create()
        {
            if (!App.Page.IsCallback)
            {
                return;
            }
            if (App.IsDataCallback && App.Settings.Update)
            {
                return;
            }
            CreatedForms.Clear();
            foreach (var form in Forms.Where(p => p.IsLoaded))
            {
                CreatedForms.Add(form);
                form.Create();
            }
        }
        #endregion

        #region Remove
        public void Remove()
        {
            foreach (var form in CreatedForms.Where(p => !p.IsLoaded))
            {
                form.RemoveFromMain();
            }
        }
        #endregion

        #region IsFormCallback
        public bool IsFormCallback
        {
            get
            {
                var isFormCallback = false;
                foreach (var form in Forms.Where(p => p.IsLoaded))
                {
                    isFormCallback |= form.Panel.IsCallback;
                    if (isFormCallback)
                    {
                        break;
                    }
                }
                return isFormCallback;
            }            
        }
        #endregion
    }
}
