using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Buttons;
using DevExpress.Web.Bootstrap;
using NLog;
using System;
using System.Collections.Generic;
using System.Web.UI;
using BsnCraft.Web.Common.Views.Base;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    #region Public Enums
    public enum BsnForms
    {
        None,
        Login,
        DataView,
        Customer,
        Contract,
        Document,
        Heading,
        Event,
        Variation,
        EstimateDetail,
        DealSubmit,
        ItemSelection,
        AreaPriceSelection,
        EstimateParagraph,
        EstimateParagraphMargin
    }

    public enum BsnFormTypes
    {
        InPanel,
        InModal,
        InGrid
    }
    #endregion

    public abstract class BsnForm : BsnView
    {
        #region Public Properties
        public BsnModes Mode { get; set; } = BsnModes.Edit;
        public BsnForms Key { get; private set; }
        public BsnApplication App { get; private set; }
        public BootstrapCallbackPanel Panel { get; private set; }
        public BsnFormItemLayout Root { get; private set; }
        public Dictionary<string, string> Styles { get; private set; }
        public BsnFormTypes Type { get; private set; }
        public object Data { get; set; }
        public string Css { get; set; } = "";
        public bool ShowCaptions { get; set; } = true;
        public bool HideCaption { get; set; }
        public bool ShowSave { get; set; }
        public bool Update { get; set; } = false;
        public bool ReadOnly { get; private set; }
        public bool IsLoaded { get; private set; }
        public BsnCallback UpdateCallback { get; private set; }
        public int FormUID { get; private set; }
        #endregion

        #region Protected Properties
        //protected Logger Log { get; set; }        
        protected Control Parent { get; private set; }
        protected Dictionary<string, BsnFormItemBase> Fields { get; set; }
        #endregion

        #region Constructor
        public BsnForm(BsnApplication app)
        {
            //Add back if needed
            //Log = LogManager.GetLogger(GetType().FullName);
            Styles = new Dictionary<string, string>();
            Root = new BsnFormItemLayout(this, "root");
            Fields = new Dictionary<string, BsnFormItemBase>();
            App = app;            
        }
        #endregion
        
        #region Init
        protected virtual void Init(BsnForms key)
        {
            Key = key;
            UID = "Fm" + Key.ToString();
            UpdateCallback = new BsnCallback(App)
            {                
                Type = CallbackType.Form,
                Key = Key.ToString()
            };
            Panel = new BootstrapCallbackPanel()
            {
                ClientInstanceName = Key.ToString(),
                ID = Key.ToString()
            };
        }
        #endregion

        #region Load
        public void Load(object data, bool readOnly = false, BsnFormTypes type = BsnFormTypes.InPanel, Control parent = null)
        {
            Data = data;
            ReadOnly = readOnly;
            Parent = parent;
            Type = type;
            if (Update)
            {
                Root.DataBind();
                UpdateCheck();
                Update = false;
            }
            Create();
        }
        #endregion

        #region Create
        public virtual void Create()
        {
            if (App.InPanelCallback)
            {                
                FormUID++;
                Root.Reset();
                AddItems();                
            }
            else
            {
                AddToMain();
                IsLoaded = false;
            }            
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            base.ToControl();
            BeforeRender();
            AddToMain();
            IsLoaded = true;
            return Panel;
        }
        #endregion

        #region Add To Main
        public virtual void AddToMain()
        {
            try
            {
                RemoveFromMain();                                
                var isModal = Type == BsnFormTypes.InModal;
                var main = isModal ? App.Modal : App.Panel;
                var container = Parent ?? main;
                Panel.Callback += App.OnDataCallback;
                Panel.Controls.Add(Root.ToControl());
                container.Controls.Add(Panel);
                if (container != main)
                {
                    main.Controls.Add(container);
                }                
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region Remove From Main
        public virtual void RemoveFromMain()
        {
            try
            {
                var main = Type == BsnFormTypes.InModal ? App.Modal : App.Panel;                
                var container = Parent ?? main;                
                if (container.Controls.Contains(Panel))
                {
                    container.Controls.Remove(Panel);
                }
                else
                {
                    var previous = container.FindControl(Key.ToString());
                    if (previous != null)
                    {
                        container.Controls.Remove(previous);
                    }
                }
                if (container != main && main.Controls.Contains(container))
                {
                    main.Controls.Remove(container);
                }
                Panel.Controls.Clear();
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region Update Check
        protected virtual void UpdateCheck()
        {
        }
        #endregion

        #region AddItems
        protected virtual void AddItems()
        {
            Fields.Clear();
        }
        #endregion
        
        #region Before Render
        protected virtual void BeforeRender()
        {
            AddButtons();
            Root.Vertical = Type == BsnFormTypes.InPanel;
            UpdateCallback.Data.Type = Type == BsnFormTypes.InModal ?
                BsnDataTypes.ModalForm : BsnDataTypes.Default;
        }
        #endregion

        #region Process
        public virtual bool Process(CallbackData data)
        {
            if (data.Mode == BsnDataModes.FormSave)
            {
                Root.DataBind();
                return true;
            }
            return false;
        }
        #endregion

        #region AfterProcess
        public virtual void AfterProcess(CallbackData data)
        {
            var location = App.Menus.Current.GetLocation();
            
            //jph stop cancel from changing location
            if (data.Mode == BsnDataModes.FormSave)
            {
                location.Mode = BsnModes.None;
                location.Key = string.Empty;
            }
            App.DataLocation = location;
        }
        #endregion
        
        #region Add Buttons
        private void AddButtons()
        {
            var buttons = new BsnFormItemPanel(this, "Buttons") { Css = "text-center" };
            Root.Add(buttons);            
            var saveButton = new BsnFormItemButton(this, "Save")
            {
                Caption = "Save",
                ClientInstanceName = "FormSave",
                Icon = "check-circle",
                ButtonType = ButtonTypes.Primary,
                Validate = true,
                Css = "bsn-formsave mx-2"
            };
            saveButton.Callback = new BsnCallback(App)
            {
                Type = CallbackType.DataOnly,
                Mode = App.Mode,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.FormSave,
                    Key = Key.ToString()
                }
            };
            saveButton.SetVisible(!ReadOnly);
            buttons.Add(saveButton);

            var cancelButton = new BsnFormItemButton(this, "Cancel")
            {
                Caption = "Cancel",
                Icon = "times",
                ButtonType = ButtonTypes.Default,
                Css = "mx-2"
            };

            switch (Type)
            {
                case BsnFormTypes.InGrid:
                    cancelButton.HtmlAttributes["data-toggle"] = "gridview-cancelchanges";
                    break;
                case BsnFormTypes.InModal:
                    cancelButton.Callback = new BsnCallback(App)
                    {
                        Type = CallbackType.DataOnly,
                        Data = new CallbackData()
                        {
                            Mode = BsnDataModes.FormCancel,
                            Key = Key.ToString()
                        }
                    };
                    break;
                case BsnFormTypes.InPanel:
                    //TODO: this may need significant improvement
                    cancelButton.Callback = App.Utils.GetModeCallback(BsnModes.None);
                    break;
            }            
            buttons.Add(cancelButton);
        }
        #endregion

        #region SetItemVisibility
        public void SetItemVisibility(string key, bool visible)
        {
            var item = Root.Find().Get(key);
            if (item != null)
            {
                item.SetVisible(visible);
            }
        }
        #endregion

    }
}
