﻿using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemGeneric : BsnFormItemBase
    {
        #region Constructor
        public BsnFormItemGeneric(BsnForm form, string key) 
            : base(form, key, FormTypes.Generic)
        {
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            return Generic;
        }
        #endregion
        
        #region Value
        public override string Value
        {
            get
            {
                return Generic.InnerHtml;
            }
        }
        #endregion        

        #region Reset
        public override void Reset()
        {
            base.Reset();
            generic = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            Generic.ID = Key + Form.FormUID;
            Generic.Visible = Visible;
            if (Child != null)
            {
                Generic.Controls.Clear();
                Generic.Controls.Add(Child);
            }
            else
            {
                if (!string.IsNullOrEmpty(Caption))
                {
                    Generic.Controls.Add(new LiteralControl(Caption));
                }
            }
        }
        #endregion

        #region Generic
        private HtmlGenericControl generic;
        public HtmlGenericControl Generic
        {
            get
            {
                if (generic == null)
                {
                    generic = new HtmlGenericControl(Tag);                        
                }           
                return generic;
            }
        }
        #endregion        
    }
}
