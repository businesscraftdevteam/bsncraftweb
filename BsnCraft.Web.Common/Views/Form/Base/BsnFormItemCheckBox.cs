﻿using BsnCraft.Web.Common.Classes;
using DevExpress.Web.Bootstrap;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemCheckBox : BsnFormItemBase
    {
        #region Constructor
        public BsnFormItemCheckBox(BsnForm form, string key) 
            : base(form, key, FormTypes.CheckBox)
        {
            ShowCaption = false;
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            base.ToControl();
            return CheckBox;
        }
        #endregion
        
        #region Value
        public override string Value
        {
            get
            {
                return CheckBox.Checked.ToString();
            }
        }
        #endregion        

        #region Reset
        public override void Reset()
        {
            base.Reset();
            checkBox = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            CheckBox.ID = Key + Form.FormUID;
            CheckBox.ClientInstanceName = ClientInstanceName;
            CheckBox.ReadOnly = Form.ReadOnly;
            CheckBox.Visible = Visible;
            CheckBox.Text = Caption;
            CheckBox.ClientSideEvents.ValueChanged = "App.checkbox";
            CheckBox.ValidationSettings.ValidationGroup = Form.Key + "Validation";
            CheckBox.ValidationSettings.RequiredField.IsRequired = Required;
            CheckBox.ValidationSettings.RequiredField.ErrorText = Error;
            CheckBox.Checked = GetValue.DefaultBool(false);
            CheckBox.DataBind();
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            SetValue(CheckBox.Checked.ToString());            
        }
        #endregion

        #region CheckBox
        private BootstrapCheckBox checkBox;
        public BootstrapCheckBox CheckBox
        {
            get
            {
                if (checkBox == null)
                {
                    checkBox = new BootstrapCheckBox();
                }
                return checkBox;
            }
        }
        #endregion

    }
}
