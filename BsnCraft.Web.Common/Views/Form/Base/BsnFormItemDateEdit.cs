﻿using BsnCraft.Web.Common.Classes;
using DevExpress.Web.Bootstrap;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemDateEdit : BsnFormItemBase
    {
        #region Constructor
        public BsnFormItemDateEdit(BsnForm form, string key) 
            : base(form, key, FormTypes.DateEdit)
        {
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            base.ToControl();
            return DateEdit;
        }
        #endregion
        
        #region Value
        public override string Value
        {
            get
            {
                return DateEdit.Date.ToString();
            }
        }
        #endregion        

        #region Reset
        public override void Reset()
        {
            base.Reset();
            dateEdit = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            DateEdit.ID = Key + Form.FormUID;
            DateEdit.ClientInstanceName = ClientInstanceName;
            DateEdit.ReadOnly = Form.ReadOnly;
            DateEdit.Visible = Visible;
            DateEdit.Caption = Caption;
            //TODO: check this
            DateEdit.ClientSideEvents.ValueChanged = "App.dateSelect";
            DateEdit.ValidationSettings.ValidationGroup = Form.Key + "Validation";
            DateEdit.ValidationSettings.RequiredField.IsRequired = Required;
            DateEdit.ValidationSettings.RequiredField.ErrorText = Error;
            DateEdit.CaptionSettings.Hidden = Form.ShowCaptions;
            DateEdit.Date = GetValue.ToDate();
            DateEdit.DataBind();
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            SetValue(DateEdit.Date.ToString());            
        }
        #endregion

        #region  DateEdit
        private BootstrapDateEdit dateEdit;
        public BootstrapDateEdit DateEdit
        {
            get
            {
                if (dateEdit == null)
                {
                    dateEdit = new BootstrapDateEdit();
                }
                return dateEdit;
            }
        }
        #endregion

    }
}
