﻿using BsnCraft.Web.Common.Views.Buttons;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemButton : BsnFormItemBase
    {
        #region Constructor
        public BsnFormItemButton(BsnForm form, string key) 
            : base(form, key, FormTypes.Button)
        {
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            base.ToControl();
            return Button.ToControl();
        }
        #endregion
        
        #region Value
        public override string Value
        {
            get
            {
                return Button.Caption;
            }
        }
        #endregion        

        #region Reset
        public override void Reset()
        {
            base.Reset();
            button = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            Button.Type = ButtonType;
            Button.Visible = Visible;
            Button.Icon = Icon;
            Button.Caption = Caption;
            Button.Tooltip = Tooltip;
            Button.CSS = Css;
            Button.UID = Key + Form.FormUID;            
            if (Validate && Callback != null)
            {
                Callback.ValidateGroup = Form.Key + "Validation";
            }
            Button.SetCallback(Callback);
            foreach (var attr in HtmlAttributes)
            {
                Button.HtmlAttributes[attr.Key] = attr.Value;
            }
        }
        #endregion

        #region Button
        private BsnButton button;
        public BsnButton Button
        {
            get
            {
                if (button == null)
                {
                    button = new BsnButton();
                }
                return button;
            }
        }
        #endregion

    }
}
