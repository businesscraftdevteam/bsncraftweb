﻿using DevExpress.Web.Bootstrap;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemMemo : BsnFormItemBase
    {
        #region Constructor
        public BsnFormItemMemo(BsnForm form, string key) 
            : base(form, key, FormTypes.Memo)
        {
            ShowCaption = false;
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            base.ToControl();
            return Memo;
        }
        #endregion
        
        #region Value
        public override string Value
        {
            get
            {
                return Memo.Text;
            }
        }
        #endregion        

        #region Reset
        public override void Reset()
        {
            base.Reset();
            memo = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            Memo.ID = Key + Form.FormUID;
            Memo.ClientInstanceName = ClientInstanceName;
            Memo.Visible = Visible;
            Memo.Caption = Caption;
            Memo.ReadOnly = Form.ReadOnly;
            Memo.Rows = Rows;
            Memo.EncodeHtml = false;
            Memo.ClientSideEvents.ValueChanged = "App.memo";
            Memo.CssClasses.Control = Css;
            Memo.ValidationSettings.ValidationGroup = Form.Key + "Validation";
            Memo.ValidationSettings.RequiredField.IsRequired = Required;
            Memo.ValidationSettings.RequiredField.ErrorText = Error;
            Memo.CaptionSettings.Hidden = Form.ShowCaptions;
            Memo.Text = GetValue;
            Memo.DataBind();
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            SetValue(Memo.Text);            
        }
        #endregion

        #region Memo
        private BootstrapMemo memo;
        public BootstrapMemo Memo
        {
            get
            {
                if (memo == null)
                {
                    memo = new BootstrapMemo();                    
                }
                return memo;
            }

        }
        #endregion

    }
}
