﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Buttons;
using DevExpress.Web;
using System.Collections.Generic;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    #region Public Enums
    public enum FormTypes
    {
        Generic,
        Panel,
        Group,
        Tab,
        TextBox,
        ButtonEdit,
        ComboBox,
        CheckBox,
        Memo,
        DateEdit,        
        Button,  
        Layout,
        Progress
    }
    #endregion
    
    public abstract class BsnFormItemBase
    {
        #region Public Properties
        public BsnForm Form { get; private set; }        
        public string Key { get; private set; }
        public FormTypes Type { get; private set; }
        public List<BsnFormItemBase> Items { get; private set; }        
        public bool Visible { get; private set; } = true;
        public bool Vertical { get; set; } = false;
        public bool ReadOnly { get; set; } = false;
        public bool ShowCaption { get; set; } = true;
        public string DisplayFormat { get; set; } = "";
        public string ClientInstanceName { get; set; } = "";
        public string Css { get; set; } = "";
        public string Tooltip { get; set; } = "";
        public object Data { get; set; }
        public int MaxLength { get; set; } = 0;
        public int Span { get; set; } = 12;
        public int Rows { get; set; } = 10;
        public FormLayoutHorizontalAlign Align { get; set; } = FormLayoutHorizontalAlign.Left;
        public bool Password { get; set; }
        public bool Required { get; set; }
        public string ComboValue { get; set; } = "Code";
        public string ComboText { get; set; } = "CodeAndDesc";
        public int SelectedIndex { get; set; } = -1;
        public BsnCallback Callback { get; set; }
        public bool UpdateForm { get; set; } = false;
        public bool Validate { get; set; }
        public ButtonTypes ButtonType { get; set; }
        public string Tag { get; set; } = "div";        
        public Dictionary<string, string> HtmlAttributes { get; set; }
        public Control Child { get; set; }
        public string PanelCaption { get; set; } = "";        
        #endregion
        
        #region Caption
        private string caption = string.Empty;
        public string Caption
        {
            get
            {
                if (!Form.HideCaption && ShowCaption)
                {
                    return caption.ValueOrDefault(Key);
                }
                return caption;
            }
            set
            {
                caption = value;
            }
        }
        #endregion

        #region Field
        private string field = string.Empty;
        public string Field
        {
            get
            {
                return field.ValueOrDefault(Key);
            }
            set
            {
                field = value;
            }
        }
        #endregion

        #region Icon
        private string icon = string.Empty;
        public string Icon
        {
            get
            {
                return icon;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    return;
                var fa = value;
                if (fa.StartsWith("fas ") || fa.StartsWith("fal "))
                {
                    icon = value;
                }
                icon = "fal fa-" + value + " fa-fw";
            }
        }
        #endregion

        #region Error
        private string error = string.Empty;
        public string Error {
            get { return (string.IsNullOrEmpty(error)) ? Key + " is required." : error; }
            set { error = value; }
        }
        #endregion
        
        #region Constructor
        public BsnFormItemBase(BsnForm form, string key, FormTypes type = FormTypes.Generic)
        {
            Items = new List<BsnFormItemBase>();
            HtmlAttributes = new Dictionary<string, string>();            
            Form = form;
            Key = key;
            Type = type;
        }
        #endregion

        #region Reset
        public virtual void Reset()
        {
            foreach (var item in Items)
            {
                item.Reset();
            }
            Items.Clear();
        }
        #endregion

        #region Add
        public virtual void Add(BsnFormItemBase item, int span = -1)
        {
            //Panel cannot be inside a panel
            if (Type == FormTypes.Panel || item.Type == FormTypes.Panel)
            {
                Items.Add(item);
                return;
            }
            if (span >= 0)
            {
                //Just for convenience
                item.Span = span;
            }
            Items.Add(item.Container);
        }
        #endregion

        #region Container
        public BsnFormItemBase container;
        public virtual BsnFormItemBase Container
        {
            get
            {
                if (container == null)
                {
                    container = new BsnFormItemPanel(Form, Key + "Panel")
                    {
                        Span = Span,
                        Align = Align,
                        Caption = PanelCaption
                    };
                    container.Items.Add(this);
                }
                return container;
            }
        }
        #endregion

        #region SetVisible
        public void SetVisible(bool visible)
        {
            foreach (var item in Items)
            {
                item.Visible = visible;
            }
            Visible = visible;
        }
        #endregion
               
        #region ToControl
        public virtual Control ToControl()
        {
            return null;
        }
        #endregion

        #region Value
        public virtual string Value
        {
            get
            {                
                return string.Empty;
            }
        }
        #endregion
               
        #region IsContainer
        public virtual bool IsContainer
        {
            get
            {
                return false;
            }
        }
        #endregion
        
        #region Load
        public virtual void Load()
        {
            if (UpdateForm && Callback == null)
            {
                Callback = Form.UpdateCallback;
            }
            foreach (var item in Items)
            {
                item.Load();
            }            
        }
        #endregion

        #region DataBind
        public virtual void DataBind()
        {
            foreach (var item in Items)
            {
                item.DataBind();
            }
        }
        #endregion

        #region GetValue
        protected string GetValue
        {
            get
            {
                var obj = Form.Data.GetType().GetProperty(Field).GetValue(Form.Data, null);
                return obj.ValueOrDefault(string.Empty, DisplayFormat);
            }            
        }
        #endregion

        #region SetValue
        protected void SetValue(string value)
        {
            if (ReadOnly)
            {
                return;
            }
            var field = Form.Data.GetType().GetProperty(Field);
            switch (field.PropertyType.FullName.ToLower())
            {
                case "system.boolean":
                    field.SetValue(Form.Data, value.DefaultBool(false));
                    break;
                case "system.decimal":
                    if (DisplayFormat.Equals("C"))
                    {
                        value = value.Replace("$", "");
                    }
                    field.SetValue(Form.Data, value.ToDec());
                    break;
                case "system.datetime":
                    field.SetValue(Form.Data, value.ToDate());
                    break;
                default:
                    field.SetValue(Form.Data, value);
                    break;
            }
        }
        #endregion        

    }
}
