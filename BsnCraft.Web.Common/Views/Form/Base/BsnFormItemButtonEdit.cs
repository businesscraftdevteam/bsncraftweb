﻿using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemButtonEdit : BsnFormItemBase
    {
        
        #region Constructor
        public BsnFormItemButtonEdit(BsnForm form, string key) 
            : base(form, key, FormTypes.ButtonEdit)
        {
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            base.ToControl();
            return ButtonEdit;
        }
        #endregion
        
        #region Value
        public override string Value
        {
            get
            {
                return ButtonEdit.Text;
            }
        }
        #endregion        

        #region Reset
        public override void Reset()
        {
            base.Reset();
            buttonEdit = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            ButtonEdit.ID = Key + Form.FormUID;
            ButtonEdit.ClientInstanceName = ClientInstanceName;
            ButtonEdit.Visible = Visible;
            ButtonEdit.Caption = Caption;
            ButtonEdit.Password = Password;
            ButtonEdit.ReadOnly = Form.ReadOnly;
            ButtonEdit.MaxLength = MaxLength;
            ButtonEdit.ClientSideEvents.ValueChanged = "App.textbox";
            ButtonEdit.Buttons.Clear();
            ButtonEdit.Buttons.Add(new BootstrapEditButton()
            {
                IconCssClass = Icon,
                Position = ButtonsPosition.Left,
                Enabled = false
            });
            ButtonEdit.ValidationSettings.ValidationGroup = Form.Key + "Validation";
            ButtonEdit.ValidationSettings.RequiredField.IsRequired = Required;
            ButtonEdit.ValidationSettings.RequiredField.ErrorText = Error;
            ButtonEdit.CaptionSettings.Hidden = Form.ShowCaptions;
            ButtonEdit.CssClasses.Control = Css;                    
            ButtonEdit.Text = GetValue;
            ButtonEdit.DataBind();
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            SetValue(ButtonEdit.Text);            
        }
        #endregion

        #region ButtonEdit
        private BootstrapButtonEdit buttonEdit;
        public BootstrapButtonEdit ButtonEdit
        {
            get
            {
                if (buttonEdit == null)
                {
                    buttonEdit = new BootstrapButtonEdit();
                }
                return buttonEdit;
            }
        }
        #endregion

    }
}
