﻿using DevExpress.Utils;
using DevExpress.Web.Bootstrap;
using System.Linq;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemTab : BsnFormItemBase
    {
        #region IsContainer
        public override bool IsContainer
        {
            get
            {               
                return true;
            }
        }
        #endregion
        
        #region Constructor
        public BsnFormItemTab(BsnForm form, string key) 
            : base(form, key, FormTypes.Tab)
        {
        }
        #endregion
        
        #region Reset
        public override void Reset()
        {
            base.Reset();
            tab = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            Tab.Caption = Caption;
            Tab.ColSpanMd = Span;
            Tab.HorizontalAlign = Align;
            Tab.Visible = Visible;
            Tab.CssClasses.Control = Css;
            if (string.IsNullOrEmpty(Caption))
            {
                if (Items.Count == 1 && Items[0].ShowCaption)
                {
                    Tab.Caption = Items[0].Caption;
                }
                else
                {
                    Tab.Caption = " ";
                }
            }
            if (Tab.Caption == " ")
            {
                Tab.ShowCaption = DefaultBoolean.False;
            }
            foreach (var item in Items.Where(p => p.IsContainer))
            {
                switch (item.Type)
                {
                    case FormTypes.Panel:
                        var panel = (BsnFormItemPanel)item;
                        Tab.Items.Add(panel.Panel);
                        break;
                    case FormTypes.Group:
                        var group = (BsnFormItemGroup)item;
                        Tab.Items.Add(group.Group);
                        break;
                }
            }
        }
        #endregion

        #region Tab
        private BootstrapTabbedLayoutGroup tab;
        public BootstrapTabbedLayoutGroup Tab
        {
            get
            {
                if (tab == null)
                {
                    tab = new BootstrapTabbedLayoutGroup();                    
                }                
                return tab;
            }
        }
        #endregion

    }
}
