﻿using DevExpress.Web.Bootstrap;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Form.Base
{
    public class BsnFormItemProgress : BsnFormItemBase
    {
        #region Constructor
        public BsnFormItemProgress(BsnForm form, string key) 
            : base(form, key, FormTypes.Progress)
        {
            ShowCaption = false;
        }
        #endregion

        #region ToControl
        public override Control ToControl()
        {
            base.ToControl();
            return Progress;
        }
        #endregion
        
        #region Value
        public override string Value
        {
            get
            {
                return Progress.Value.ToString();
            }
        }
        #endregion        

        #region Reset
        public override void Reset()
        {
            base.Reset();
            progress = null;
        }
        #endregion

        #region Load
        public override void Load()
        {
            base.Load();
            Progress.ID = Key + Form.FormUID;
            Progress.ClientInstanceName = ClientInstanceName;
            Progress.Visible = Visible;
            Progress.Caption = Caption;
            Progress.CssClasses.Control = Css;
            Progress.CaptionSettings.Hidden = Form.ShowCaptions;
            Progress.Value = GetValue;
            Progress.DataBind();
        }
        #endregion

        #region Progress
        private BootstrapProgressBar progress;
        public BootstrapProgressBar Progress
        {
            get
            {
                if (progress == null)
                {
                    progress = new BootstrapProgressBar();                    
                }
                return progress;
            }

        }
        #endregion

    }
}
