﻿using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Base
{
    public abstract class BsnView
    {
        //TODO: This is just an idea

        #region Public Properties
        public virtual string UID { get; set; } = "";
        public string CSS { get; set; } = "";
        public bool Active { get; set; } = false;
        public bool Visible { get; set; } = true;        
        #endregion

        public virtual Control ToControl()
        {
            return null;
        }
    }
}
