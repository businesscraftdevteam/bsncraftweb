﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Menu.Contracts.Base;

namespace BsnCraft.Web.Common.Views.Menu.Leads
{
    public class Events : EventsBase
    {
        #region Constructor
        public Events(BsnApplication app) : base(app, BsnMenus.Leads)
        {
        }
        #endregion        
    }
}
