﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Menu.Contracts.Base;

namespace BsnCraft.Web.Common.Views.Menu.Leads
{
    public class Contacts : ContactsBase
    {
        #region Constructor
        public Contacts(BsnApplication app) : base(app, BsnMenus.Leads)
        {
        }
        #endregion        
    }
}