using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Menu.Contracts.Base;

namespace BsnCraft.Web.Common.Views.Menu.Leads
{
    public class Quotes : QuotesBase
    {
        #region Constructor
        public Quotes(BsnApplication app) : base(app, BsnMenus.Leads)
        {
        }
        #endregion
    }
}
