﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Menu.Contracts.Base;

namespace BsnCraft.Web.Common.Views.Menu.Leads
{
    public class Variations : VariationsBase
    {
        #region Constructor
        public Variations(BsnApplication app) : base(app, BsnMenus.Leads)
        {
        }
        #endregion
    }
}
