using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using GD = BsnCraft.Web.Common.Views.Grid.Jobs;

namespace BsnCraft.Web.Common.Views.Menu.AreaPrices
{
    public class AreaPriceList : BsnMenu
    {
        #region Private Properties        
        //private BsnGridFilter CostCentreFilters { get; set; }        
        private BsnGridFilter ProductGroupFilters { get; set; }        
        private BsnGridFilter ProductCatFilters { get; set; }        
        private BsnGridFilter SortCodeFilters { get; set; }
        private AreaPriceSelection Selection { get; set; }
        //private List<string> CostCentreCodes { get; set; }
        private List<string> ProductGroupCodes { get; set; }
        private List<string> ProductCatCodes { get; set; }
        private List<string> SortCodes { get; set; }

        const string ShowFilter = "ShowFilter";
        const string AdvancedMode = "AdvancedMode";
        #endregion
        
        #region Constructor
        public AreaPriceList(BsnApplication app) : base(app, true)
        {
            Init(BsnMenus.AreaPrices);
            Icon = "usd-square";
            Caption = "Area Prices";

            Selection = new AreaPriceSelection();

            //CostCentreCodes = new List<string>();
            ProductGroupCodes = new List<string>();
            ProductCatCodes = new List<string>();
            SortCodes = new List<string>();

            //CostCentreFilters = new BsnGridFilter(App, "CostCentres")
            //{
            //    Caption = "Cost Centres",
            //    KeyField = "Code",
            //    SelectedField = "IsSelected",
            //    CaptionField = "Desc",
            //    ShowCount = false,
            //    MultiSelect = true
            //};

            ProductGroupFilters = new BsnGridFilter(App, "ProductGroups")
            {
                Caption = "Product Groups",
                KeyField = "Code",
                SelectedField = "IsSelected",
                CaptionField = "Desc",
                CountField = "Count",
                CountCaption = "Count",
                CountDisplayFormat = "N0",
                MultiSelect = true
            };

            ProductCatFilters = new BsnGridFilter(App, "ProductCategories")
            {
                Caption = "Product Categories",
                KeyField = "Code",
                SelectedField = "IsSelected",
                CaptionField = "Desc",
                CountField = "Count",
                CountCaption = "Count",
                CountDisplayFormat = "N0",
                MultiSelect = true
            };

            SortCodeFilters = new BsnGridFilter(App, "SortCodes")
            {
                Caption = "Sort Codes",
                KeyField = "Code",
                SelectedField = "IsSelected",
                CaptionField = "Desc",
                CountField = "Count",
                CountCaption = "Count",
                CountDisplayFormat = "N0",
                MultiSelect = true
            };
        }
        #endregion

        #region GetCount
        public override int GetCount()
        {
            if (!IsLoaded)
            {
                return 0;
            }
            return App.VM.APR.CurrentPricing.Count;
        }
        #endregion

        #region Load
        protected override void Load()
        {
            try
            {
                base.Load();

                var selectForm = App.Forms.Get(BsnForms.AreaPriceSelection);
                Selection.Area = App.VM.APR.CurrentArea;
                Selection.EffectiveDate = App.VM.APR.CurrentEffective;
                selectForm.Load(Selection);
                App.Main.Controls.Add(selectForm.ToControl());                
                App.Main.Controls.Add(Helpers.HtmlLine());

                var grid = (GD.AreaPrices)App.Grids.Get(BsnGrids.AreaPrices);
                grid.AdvancedMode = GetOption(AdvancedMode);
                if (GetOption(ShowFilter))
                {
                    CheckProductGroup(grid);
                    CheckProductCategory(grid);
                    CheckSortCode(grid);
                }

                var gridControl = App.AddGrid(grid);
                if (Mode == BsnModes.ExportGrid)
                {
                    return;
                }

                if (!GetOption(ShowFilter))
                {
                    App.Main.Controls.Add(gridControl);
                    return;
                }
          
                var container = new HtmlGenericControl("div");
                container.Attributes["class"] = "row";
                
                var filterColumn = new HtmlGenericControl("div");
                filterColumn.Attributes["class"] = "col-md-3 mb-3";
                filterColumn.Attributes["style"] = "overflow-x: auto";
                container.Controls.Add(filterColumn);
                    
                filterColumn.Controls.Add(ProductGroupFilters.ToControl());
                filterColumn.Controls.Add(new HtmlGenericControl("hr"));
                filterColumn.Controls.Add(ProductCatFilters.ToControl());
                filterColumn.Controls.Add(new HtmlGenericControl("hr"));
                filterColumn.Controls.Add(SortCodeFilters.ToControl());

                var gridColumn = new HtmlGenericControl("div");
                gridColumn.Attributes["class"] = "col-md-9";
                gridColumn.Controls.Add(gridControl);
                container.Controls.Add(gridColumn);

                App.Main.Controls.Add(container);
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion

        #region Check Product Group
        void CheckProductGroup(GD.AreaPrices grid)
        {
            var data = App.Callback.Data;
            if (data != null && data.Mode == BsnDataModes.Filter && data.Key == "ProductGroups")
            {
                App.VM.APR.ClearProductCatSelection();
                App.VM.APR.ClearSortCodeSelection();
                try
                {
                    var codes = data.Object.ToString();
                    ProductGroupCodes = (List<string>)JsonConvert.DeserializeObject(codes, typeof(List<string>));
                }
                catch
                {
                    ProductGroupCodes = new List<string>();
                }
                foreach (var ProductGroup in App.VM.APR.ProductGroups)
                {
                    ProductGroup.IsSelected = ProductGroupCodes.Contains(ProductGroup.Code);
                }
            }
            var prices = App.VM.APR.CurrentPricing;
            var list = App.VM.APR.ProductGroups.Where(p => prices.Any(i => i.ProductGroup.Code.Equals(p.Code))).ToList();
            ProductGroupFilters.DataSource = list;
            ProductGroupFilters.ShowCount = false;
            ProductGroupFilters.Load();            
        }
        #endregion

        #region Check Product Category
        void CheckProductCategory(GD.AreaPrices grid)
        {
            var data = App.Callback.Data;
            if (data != null && data.Mode == BsnDataModes.Filter && data.Key == "ProductCategories")
            {
                App.VM.APR.ClearSortCodeSelection();
                try
                {
                    var codes = data.Object.ToString();
                    ProductCatCodes = (List<string>)JsonConvert.DeserializeObject(codes, typeof(List<string>));
                }
                catch
                {
                    ProductCatCodes = new List<string>();
                }
                foreach (var prodCatg in App.VM.APR.ProductCats)
                {
                    prodCatg.IsSelected = ProductCatCodes.Contains(prodCatg.Code);
                }
            }            
            var prices = App.VM.APR.CurrentPricing;
            var filterProductGroups = new List<ComboItem>(App.VM.APR.ProductGroups.Where(p => p.IsSelected));            
            if (filterProductGroups.Count > 0)
            {
                prices = prices.Where(p => filterProductGroups.Any(i => i.Code.Equals(p.ProductGroup.Code))).ToList();
            }
            var list = App.VM.APR.ProductCats.Where(p => prices.Any(i => i.ProductCategory.Code.Equals(p.Code))).ToList();
            ProductCatFilters.DataSource = list;
            ProductCatFilters.ShowCount = false;
            ProductCatFilters.Load();            
        }
        #endregion

        #region Check Sort Code
        void CheckSortCode(GD.AreaPrices grid)
        {
            var data = App.Callback.Data;
            if (data != null && data.Mode == BsnDataModes.Filter && data.Key == "SortCodes")
            {
                try
                {
                    var codes = data.Object.ToString();
                    SortCodes = (List<string>)JsonConvert.DeserializeObject(codes, typeof(List<string>));
                }
                catch
                {
                    SortCodes = new List<string>();
                }
                foreach (var sortCode in App.VM.APR.SortCodes)
                {
                    sortCode.IsSelected = SortCodes.Contains(sortCode.Code);
                }
            }            
            var prices = App.VM.APR.CurrentPricing;
            var filterProductGroups = new List<ComboItem>(App.VM.APR.ProductGroups.Where(p => p.IsSelected));
            var filterProductCats = new List<ComboItem>(App.VM.APR.ProductCats.Where(p => p.IsSelected));
            if (filterProductGroups.Count > 0)
            {
                prices = prices.Where(p => filterProductGroups.Any(i => i.Code.Equals(p.ProductGroup.Code))).ToList();
            }
            if (filterProductCats.Count > 0)
            {
                prices = prices.Where(p => filterProductCats.Any(i => i.Code.Equals(p.ProductCategory.Code))).ToList();
            }
            var list = App.VM.APR.ProductCats.Where(p => prices.Any(i => i.ProductCategory.Code.Equals(p.Code))).ToList();
            SortCodeFilters.DataSource = App.VM.APR.SortCodes.Where(p => prices.Any(i => i.SortCode.Code.Equals(p.Code))).ToList();
            SortCodeFilters.ShowCount = false;
            SortCodeFilters.Load();
            
        }        
        #endregion

        #region CostCentreFilter
        //Not sure if we'll bring this back
        //void AddCostCentreFilter(HtmlGenericControl group, GD.AreaPrices grid)
        //{
        //    var CostCentres = App.VM.APR.GetSelectedCostCentres(Area, EffectiveDate);
        //    CheckCostCentreFilter(CostCentres);
        //    CostCentreFilters.DataSource = CostCentres;
        //    CostCentreFilters.ShowCount = false;
        //    CostCentreFilters.Load();
        //    group.Controls.Add(new HtmlGenericControl("hr"));
        //    group.Controls.Add(CostCentreFilters.ToControl());
        //}

        //void CheckCostCentreFilter(List<ComboItem> CostCentres)
        //{
        //    var data = App.Callback.Data;
        //    if (data == null || data.Mode != BsnDataModes.Filter || data.Key != "CostCentre")
        //    {
        //        return;
        //    }
        //    try
        //    {
        //        var codes = data.Object.ToString();
        //        CostCentreCodes = (List<string>)JsonConvert.DeserializeObject(codes, typeof(List<string>));
        //    }
        //    catch
        //    {
        //        CostCentreCodes = new List<string>();
        //    }            
        //    foreach (var CostCentre in CostCentres)
        //    {
        //        CostCentre.IsSelected = CostCentreCodes.Contains(CostCentre.Code);
        //    }
        //}
        #endregion

        #region Load Options
        protected override void LoadOptions()
        {
            base.LoadOptions();
            Options.Add(ShowFilter, ("Show Filter", true));
            Options.Add(AdvancedMode, ("Advanced Mode", false));
        }
        #endregion
    }
}

