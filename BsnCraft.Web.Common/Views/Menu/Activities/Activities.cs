﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Classes;
using System.Linq;
using System.Web.UI.HtmlControls;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using GD = BsnCraft.Web.Common.Views.Grid.Contracts;

namespace BsnCraft.Web.Common.Views.Menu.Activities
{
    public class Activities : BsnMenu
    {
        #region Private Properties
        const string ShowFilter = "ShowFilter";
        private BsnGridFilter ActivityFilter { get; set; }
        //private BsnGridFilter UsersFilter { get; set; }
        //private List<string> Users { get; set; }
        #endregion

        #region Constructor
        public Activities(BsnApplication app) : base(app, true)
        {
            Init(BsnMenus.Activities);
            Icon = "th-list";

            ActivityFilter = new BsnGridFilter(App, "Activities")
            {
                Caption = "Activities",
                KeyField = "Code",
                CaptionField = "Desc",
                CountField = "Count",
                CountCaption = "#",
                SelectedField = "IsSelected",
                ShowCount = true,
                MultiSelect = false
            };

            //UsersFilter = new BsnGridFilter(App, "Users")
            //{
            //    Caption = "Users",
            //    Parent = "Activities",
            //    RequiresParent = true,
            //    KeyField = "Code",
            //    CaptionField = "Desc",
            //    CountField = "Count",
            //    MultiSelect = true,
            //    ShowCount = true
            //};
        }
        #endregion

        #region GetCount
        public override int GetCount()
        {
            return App.VM.AC.ActivityContracts.Count;
        }
        #endregion

        #region Load
        protected override void Load()
        {
            base.Load();

            var grid = (GD.Activities)App.Grids.Get(BsnGrids.Activities);
            if (GetOption(ShowFilter))
            {
                CheckFilter();
                ActivityFilter.DataSource = App.VM.AC.Activities;
                ActivityFilter.Load();
            }
            var control = App.AddGrid(grid);

            if (GetOption(ShowFilter) && Mode != BsnModes.ExportGrid)
            {
                var row = new HtmlGenericControl("div");
                row.Attributes["class"] = "row";
                App.Main.Controls.Add(row);

                var group = new HtmlGenericControl("div");
                group.Attributes["class"] = "col-md-3 mb-3";
                group.Attributes["style"] = "overflow-x: auto";
                row.Controls.Add(group);

                group.Controls.Add(ActivityFilter.ToControl());

                var gridControl = new HtmlGenericControl("div");
                gridControl.Attributes["class"] = "col-md-9";
                gridControl.Controls.Add(control);
                row.Controls.Add(gridControl);
            }
            
        }
        #endregion
        
        #region Load Options
        protected override void LoadOptions()
        {
            base.LoadOptions();
            Options.Add(ShowFilter, ("Show Filter", true));
        }
        #endregion

        #region PerformUpdate
        protected override bool PerformUpdate(CallbackData data)
        {
            base.PerformUpdate(data);
            bool updated = false;
            var contract = App.VM.CO.GetContract(data.Value);
            if (string.IsNullOrEmpty(contract.ContractNumber))
            {
                return updated;
            }
            var contractEvent = contract.FindEvent(data.Key);
            switch (data.Mode)
            {
                case BsnDataModes.QuickRegister:
                    App.VM.CO.EventRegister(contractEvent);
                    updated = true;
                    break;
                default:
                    return updated;
            }
            App.DataLocation = App.Menus.Current.GetLocation();
            return updated;
        }
        #endregion

        #region CheckFilter
        void CheckFilter()
        {
            //TODO: enhance to include users, etc.

            var data = App.Callback.Data;
            if (data != null && data.Mode != BsnDataModes.Filter)
            {
                return;                
            }

            App.Settings.GridUID++;

            try
            {
                var activities = (List<string>)JsonConvert.DeserializeObject(data.Object.ToString(), typeof(List<string>));
                foreach (var activity in App.VM.AC.Activities)
                {
                    activity.IsSelected = activities.Contains(activity.Code);
                }
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion
    }
}
