﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Classes;
using System.Web.UI.HtmlControls;
using DevExpress.Web;
using Newtonsoft.Json;
using System.Web.UI.WebControls;
using BsnCraft.Web.Common.Views.Buttons;
using BsnCraft.Web.Common.Views.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web.UI;

namespace BsnCraft.Web.Common.Views.Menu.Home
{
    public class Desktop : BsnMenu
    {
        #region Constants
        const string Customers = "Customers";
        const string Contacts = "Contacts";
        const string Leads = "Leads";
        const string Contracts = "Contracts";
        const string Activities = "Activities";        
        const string LeftMode = "LeftMode";
        const string RightMode = "RightMode";
        const string EditMode = "EditMode";
        #endregion

        #region Private Properties
        private BsnButtonGroup PanelsButton { get; set; }
        private HtmlGenericControl LeftColumn { get; set; }
        private HtmlGenericControl MainColumn { get; set; }
        private HtmlGenericControl RightColumn { get; set; }
        private bool ShowLeft { get; set; }
        private bool ShowRight { get; set; }
        private bool ShowEdit { get; set; }
        private int VisibleIndex { get; set; }
        #endregion

        #region Contstructor
        public Desktop(BsnApplication app) : base(app, true)
        {
            Init(BsnMenus.Home);
            Icon = "home";
            PanelsButton = new BsnButtonGroup(new BsnButton("layer-group"));
        }
        #endregion

        #region PageTitle
        public override string PageTitle
        {
            get
            {
                return "BusinessCraft Web";
            }
        }
        #endregion
        
        #region Load
        protected override void Load()
        {
            base.Load();
            if (!App.InCallback)
            {
                return;
            }
            SetupLayout();
            SetupPanels();
        }
        #endregion

        #region Setup Layout
        void SetupLayout()
        {
            var container = new HtmlGenericControl("div");
            container.Attributes["class"] = "row";
            App.Main.Controls.Add(container);

            ShowLeft = SettingsOpt(LeftMode);
            ShowRight = SettingsOpt(RightMode);
            ShowEdit = SettingsOpt(EditMode);
            var showBoth = ShowLeft && ShowRight;
            var colClass = showBoth ? "col-md-4" : ShowLeft || ShowRight ? "col-md-6" : "col-md-12";

            LeftColumn = new HtmlGenericControl("div")
            {
                Visible = ShowLeft
            };
            LeftColumn.Attributes["class"] = colClass;
            container.Controls.Add(LeftColumn);

            MainColumn = new HtmlGenericControl("div");
            MainColumn.Attributes["class"] = colClass;
            container.Controls.Add(MainColumn);

            RightColumn = new HtmlGenericControl("div")
            {
                Visible = ShowRight
            };
            RightColumn.Attributes["class"] = colClass;
            container.Controls.Add(RightColumn);            
        }
        #endregion

        #region Setup Panels
        void SetupPanels()
        {
            var panels = App.Settings.MenuPanels(UID);
            var update = false;
            CheckDefaults(panels, ref update);
            BookmarkedCharts(panels, ref update);
            if (update)
            {
                App.Settings.UpdatePanels(UID, panels);
            }
            AddDockManager(panels);
            PanelsButton.Buttons.Clear();
            foreach (var panel in panels)
            {
                AddPanel(panel);
            }
            var button = new BsnButton(new BsnCallback(App)
            {
                Menu = UID.Item1,
                Action = UID.Item2,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.ResetPanels
                }
            }, "trash", "Reset All", false);
            PanelsButton.Buttons.Add(button);
        }
        #endregion

        #region Add DockManager
        void AddDockManager(List<DockPanel> panels)
        {
            if (!ShowEdit)
            {
                return;
            }
            App.LoadPanels(panels);
            MainColumn.Controls.Add(new ASPxDockZone() { ZoneUID = "main", Width = Unit.Percentage(100) });
            LeftColumn.Controls.Add(new ASPxDockZone() { ZoneUID = "left", Width = Unit.Percentage(100) });
            RightColumn.Controls.Add(new ASPxDockZone() { ZoneUID = "right", Width = Unit.Percentage(100) });
        }
        #endregion

        #region Add Panel
        protected void AddPanel(DockPanel panel)
        {
            var icon = (panel.Visible) ? "check-square" : "square";
            var button = new BsnButton(new BsnCallback(App)
            {
                Menu = UID.Item1,
                Action = UID.Item2,
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.PanelToggle,
                    Key = panel.Name,
                    Value = (!panel.Visible).ToString()
                }
            }, icon, panel.Caption, false);
            PanelsButton.Buttons.Add(button);

            if (!panel.Visible || 
                (panel.ZoneId.ToLower() == "left" && !ShowLeft) ||
                (panel.ZoneId.ToLower() == "right" && !ShowRight))
            {
                return;
            }
            
            Control control = null;
            switch (panel.Type)
            {
                case PanelTypes.Grid:
                    var grid = AddGrid(panel);
                    if (grid == null)
                    {
                        return;
                    }
                    grid.Reset();
                    grid.OnlyBookmarks = true;
                    grid.Visible = panel.Visible;
                    grid.Load();
                    control = grid.ToControl();
                    break;
                case PanelTypes.Chart:
                    var chart = App.VM.DV.Charts.Find(p => p.Key.Equals(panel.Name));
                    if (chart == null)
                    {
                        return;
                    }
                    control = App.VM.DV.GenerateChart(chart.Name);
                    break;
            }

            if (ShowEdit)
            {
                var dockPanel = new ASPxDockPanel()
                {
                    PanelUID = panel.Name,
                    AllowedDockState = AllowedDockState.DockedOnly,
                    DragElement = DragElement.Window,
                    Cursor = "Default",
                    ShowHeader = false,
                    ShowShadow = false,
                    Theme = "iOS"
                };                
                dockPanel.Controls.Add(control);
                App.Main.Controls.Add(dockPanel);
            }
            else
            {
                switch (panel.ZoneId.ToLower())
                {
                    case "main":
                        MainColumn.Controls.Add(control);
                        break;
                    case "left":
                        LeftColumn.Controls.Add(control);
                        break;
                    case "right":
                        RightColumn.Controls.Add(control);
                        break;
                }
            }
        }
        #endregion

        #region Add Grid
        protected BsnGrid AddGrid(DockPanel panel)
        {
            switch (panel.Name.ToLower())
            {
                case "customers":
                    return App.Grids.Get(BsnGrids.Customers);
                case "contacts":
                    return App.Grids.Get(BsnGrids.Contacts);
                case "activities":
                    return App.Grids.Get(BsnGrids.Activities);
                case "leads":
                    return App.Grids.Get(BsnGrids.Leads);
                case "contracts":
                    return App.Grids.Get(BsnGrids.Contracts);
            }
            return null;
        }
        #endregion

        #region Load Modes
        protected override void LoadModes()
        {
            base.LoadModes();
            Modes.Add(new BsnButton(new BsnCallback(App)
            {
                Menu = UID.Item1,
                Action = Action,
                Mode = BsnModes.MenuOption,
                Key = LeftMode,
                Value = (!ShowLeft).ToString()
            }, "arrow-to-left")
            {
                Active = ShowLeft,
                Tooltip = "Left Column"
            });
            Modes.Add(new BsnButton(new BsnCallback(App)
            {
                Menu = UID.Item1,
                Action = Action,
                Mode = BsnModes.MenuOption,
                Key = RightMode,
                Value = (!ShowRight).ToString()
            }, "arrow-to-right")
            {
                Active = ShowRight,
                Tooltip = "Right Column"
            });
            Modes.Add(new BsnButton(new BsnCallback(App)
            {
                Menu = UID.Item1,
                Action = Action,
                Mode = BsnModes.MenuOption,
                Key = EditMode,
                Value = (!ShowEdit).ToString()
            }, "object-ungroup")
            {
                Active = ShowEdit,
                Tooltip = "Edit Layout"
            });
            Modes.Add(PanelsButton);
        }
        #endregion

        #region CheckDefaults        
        protected void CheckDefaults(List<DockPanel> panels, ref bool update)
        {
            if (!panels.Any(p => p.Name.Equals(Customers)))
            {                
                panels.Add(new DockPanel()
                {
                    Name = Customers,
                    Caption = "My Customers",
                    VisibleIndex = VisibleIndex++
                });
                update = true;
            };

            if (!panels.Any(p => p.Name.Equals(Contacts)))
            {
                panels.Add(new DockPanel()
                {
                    Name = Contacts,
                    Caption = "My Contacts",
                    VisibleIndex = VisibleIndex++
                });
                update = true;
            }

            if (!panels.Any(p => p.Name.Equals(Leads)))
            {
                if (App.Menus.Get(BsnMenus.Leads).Visible)
                {
                    panels.Add(new DockPanel()
                    {
                        Name = Leads,
                        Caption = "My Leads",
                        VisibleIndex = VisibleIndex++
                    });
                    update = true;
                }
            }

            if (!panels.Any(p => p.Name.Equals(Contracts)))
            {
                if (App.Menus.Get(BsnMenus.Contracts).Visible)
                {
                    panels.Add(new DockPanel()
                    {
                        Name = Contracts,
                        Caption = "My Contracts",
                        VisibleIndex = VisibleIndex++
                    });
                    update = true;
                }
            }

            if (!panels.Any(p => p.Name.Equals(Activities)))
            {
                if (App.Menus.Get(BsnMenus.Activities).Visible)
                {
                    panels.Add(new DockPanel()
                    {
                        Name = Activities,
                        Caption = "My Activities",
                        VisibleIndex = VisibleIndex++
                    });
                    update = true;
                }
            }
        }
        #endregion

        #region BookmarkedCharts
        private void BookmarkedCharts(List<DockPanel> panels, ref bool update)
        {
            foreach (var chart in App.VM.DV.Charts)
            {
                if (chart.Bookmarked)
                {
                    if (!panels.Any(p => p.Name.Equals(chart.Key)))
                    {
                        panels.Add(new DockPanel()
                        {
                            Name = chart.Key,
                            Caption = "Chart: " + chart.Name,
                            Type = PanelTypes.Chart,
                            VisibleIndex = VisibleIndex++
                        });
                        update = true;
                    }
                }
                else
                {
                    if (panels.Any(p => p.Name.Equals(chart.Key)))
                    {
                        panels.Remove(panels.Find(p => p.Name.Equals(chart.Key)));
                        update = true;
                    }
                }
            }
        }
        #endregion
    }
}
