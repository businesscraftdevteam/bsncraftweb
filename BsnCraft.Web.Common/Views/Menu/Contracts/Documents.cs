﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Menu.Contracts.Base;

namespace BsnCraft.Web.Common.Views.Menu.Contracts
{
    public class Documents : DocumentsBase
    {
        #region Constructor
        public Documents(BsnApplication app) : base(app, BsnMenus.Contracts)
        {
        }
        #endregion      
    }
}
