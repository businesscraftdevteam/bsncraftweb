using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System.Web.UI;
using System.Linq;
using BsnCraft.Web.Common.Settings;
using GD = BsnCraft.Web.Common.Views.Grid.Jobs;
using FM = BsnCraft.Web.Common.Views.Form.Jobs;
using System.Web.UI.HtmlControls;
using BsnCraft.Web.Common.Views.Buttons;
using DevExpress.Web.Bootstrap;
using System.Collections.Generic;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using System;
using BsnCraft.Web.Common.Views.Form.Base;

namespace BsnCraft.Web.Common.Views.Menu.Contracts.Base
{
    public abstract class QuotesBase : BsnMenu
    {
        #region Contract
        private Contract contract;
        public Contract Contract
        {
            get
            {
                if (contract == null)
                {
                    contract = new Contract(App);
                }
                return contract;
            }
        }
        #endregion

        #region Protected Properties
        protected Heading Heading { get; set; }
        protected bool InQuote { get; set; }
        protected bool IsEstimate { get; set; }
        protected int FilterDepth { get; set; }
        protected string Filter1 { get; set; }
        protected string Filter2 { get; set; }
        protected string Filter3 { get; set; }
        protected string FilterHeading { get; set; }
        protected string FilterCouncil { get; set; }
        protected string FilterEstate { get; set; }
        protected bool ShowCouncil { get; set; }
        protected bool ShowEstate { get; set; }
        protected bool AllowCopy { get; set; }
        protected BsnCallback FilterCallback { get; set; }
        protected BsnCallback ParagraphComboCallback { get; set; }
        protected HtmlGenericControl Container { get; set; }
        const string ShowFilter = "ShowFilter";
        const string ShowPriceDisplayFilter = "ShowPriceDisplayFilter";
        const string ShowAmounts = "ShowAmounts";
        const string ShowMargin = "ShowMargin";
        const string ShowEmpty = "ShowEmpty";
        const string AdvancedMode = "AdvancedMode";
        protected BsnGridFilter ParagraphFilters { get; set; }
        protected List<string> Paragraphs { get; set; }
        protected List<ComboItem> ParagraphList { get; set; }
        protected BsnGridFilter CostCentreFilters { get; set; }
        protected List<string> CostCentreCodes { get; set; }
        protected BsnGridFilter PriceDisplayFilters { get; set; }
        protected List<string> PriceDisplayCodes { get; set; }
        #endregion

        #region Constructor
        public QuotesBase(BsnApplication app, BsnMenus menu) : base(app)
        {
            Init(menu, BsnActions.Quotes);
        }
        #endregion
        
        #region DataBind
        public override void DataBind()
        {
            base.DataBind();

            if (string.IsNullOrEmpty(Id))
            {
                contract = new Contract(App);
                return;
            }
            contract = App.VM.CO.GetContract(Id);
            if (contract == null)
            {
                contract = new Contract(App);
                App.Main.Controls.Add(new LiteralControl(Id + " not found."));
                return;
            }
            if (GetActions().Any(p => p.UID.Equals((BsnActions.Default, BsnModes.None))))
            {
                var ImportantAction = GetActions().Where(p => p.UID.Equals((BsnActions.Default, BsnModes.None))).FirstOrDefault();
                ImportantAction.Visible = (Contract.ImportantDocuments.Count != 0);
            }
            Title = Contract.FriendlyName;
            Heading = Contract.FindHeading(App.Key, true);
            InQuote = Heading != null;
            IsEstimate = (Heading != null && Heading.IsEstimate);
            ShowReports = IsEstimate;
            
            if (InQuote)
            {
                if (IsEstimate)
                {
                    InitEstimate();
                }
                else
                {
                    InitTemplate();
                }
            }
        }
        #endregion

        #region Init Estimate
        protected virtual void InitEstimate()
        {
            Paragraphs = new List<string>();
            ParagraphList = new List<ComboItem>();
            ParagraphFilters = new BsnGridFilter(App, "Paragraphs")
            {
                Caption = "Paragraphs",
                KeyField = "Code",
                SelectedField = "IsSelected",
                CaptionField = "Description",
                CountField = "SumAmount",
                CountCaption = "Amount",
                CountDisplayFormat = "C0",
                MultiSelect = true
            };
            CostCentreCodes = new List<string>();
            CostCentreFilters = new BsnGridFilter(App, "CostCentres")
            {
                Caption = "Cost Centres",
                KeyField = "Code",
                SelectedField = "IsSelected",
                CaptionField = "Desc",
                MultiSelect = true
            };
            PriceDisplayCodes = new List<string>();
            PriceDisplayFilters = new BsnGridFilter(App, "PriceDisplay")
            {
                Caption = "Price Display",
                KeyField = "Code",
                SelectedField = "IsSelected",
                CaptionField = "Desc",
                MultiSelect = true
            };

            string houseCatg = "ZA";
            string elevationCatg = "ZB";
            string imageDrvLetter = "C";
            string imagePath = "images";

            var paragraphs = Contract.Paragraphs(Heading).Where(p => p.IsSelected).Select(x => x.Code).Distinct().ToList();
            if (paragraphs.Count == 0)
            {
                paragraphs = Contract.Paragraphs(Heading).Select(x => x.Code).Distinct().ToList();
            }

            ReportParams = "Job=\"" + Contract.JobNumber + "\";";
            ReportParams += "Contract=\"" + Contract.ContractNumber + "\";";
            ReportParams += "Section=\"" + Heading.Section + "\";";
            ReportParams += "Heading=\"" + Heading.Code + "\";";
            ReportParams += "Paragraphs=\"" + string.Join(",", paragraphs) + "\";";
            ReportParams += "HouseCatg=\"" + houseCatg + "\";";
            ReportParams += "ElevationCatg=\"" + elevationCatg + "\";";
            ReportParams += "User=\"" + App.Api.Username + "\";";
            ReportParams += "ImageDrvLetter=\"" + imageDrvLetter + "\";";
            ReportParams += "ImagePath=\"" + imagePath + "\";";
            
            ParagraphComboCallback = new BsnCallback(App)
            {
                Data = new CallbackData
                {
                    Mode = BsnDataModes.Filter,
                    Key = "Paragraphs"
                }
            };
            //ReportParams += "JobTotal=\"" + Heading.Amount.ToString("F2") + "\";";
        }
        #endregion

        #region Init Template
        protected virtual void InitTemplate()
        {
            FilterDepth = 0;
            Filter1 = string.Empty;
            Filter2 = string.Empty;
            Filter3 = string.Empty;
            FilterHeading = string.Empty;
            FilterCouncil = string.Empty;
            FilterEstate = string.Empty;
            ShowCouncil = (App.VM.JC.TemplateCouncils.Count != 0);
            ShowEstate = (App.VM.JC.TemplateEstates.Count != 0);
            AllowCopy = false;
            FilterCallback = new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation,
                Menu = App.Menu,
                Action = App.Action,
                Id = App.Id,
                Mode = BsnModes.Filter,
                Key = App.Key
            };
        }
        #endregion

        #region PerformUpdate
        protected override bool PerformUpdate(CallbackData data)
        {
            base.PerformUpdate(data);
            bool updated = false;
            if (data.Mode == BsnDataModes.SelectedKeys)
            {
                var keyList = data.Object.ToString();
                Contract.SelectedEstimateDetailKeys(Heading).Clear();
                Contract.SelectedEstimateDetailKeys(Heading).AddRange((List<string>)JsonConvert.DeserializeObject(keyList, typeof(List<string>)));
                return updated;
            }
            base.PerformUpdate(data);
            if (string.IsNullOrEmpty(Contract.ContractNumber))
            {
                return updated;
            }

            App.VM.QT.SetContract(Contract);
            App.VM.QT.SetHeading(Heading);
            updated = App.VM.QT.PerformUpdate(data, InQuote, IsEstimate);

            if (!InQuote && !IsEstimate && !updated)
            {
                updated = UpdateTemplate(data);
            }
            return updated;
        }
        #endregion
        
        #region Load
        protected override void Load()
        {
            base.Load();

            if (string.IsNullOrEmpty(Contract.ContractNumber))
            {
                return;
            }            

            if (!InQuote)
            {
                MainGrid();
                return;
            }          

            if (IsEstimate)
            {
                LoadEstimate();
                return;
            }
            
            switch (App.Mode)
            {
                case BsnModes.Filter:
                    LoadFilter(App.Value);
                    break;
            }

            TemplateDropDowns();
            AdditionalDropDowns();
            FilterTemplates();
            SelectTemplate();
            NavButtons();

        }
        #endregion

        #region Load Modal
        protected override bool LoadModal()
        {
            var isModal = base.LoadModal();
            var callback = App.DataCallback;            
            if (string.IsNullOrEmpty(callback.Key))
            {
                return isModal;
            }            
            var form = callback.Key.ToForm();
            if (form == BsnForms.None || App.Forms.Get(form) == null)
            {
                return isModal;
            }
            var data = callback.Data;
            var modalOpts = new ModalOptions()
            {
                Size = "lg",
                ShowFooter = false
            };
            switch (form)
            {
                case BsnForms.Heading:
                    var headingForm = (FM.Heading)App.Forms.Get(form);
                    headingForm.IsSetMarginForm = false;
                    var headingCode = data.Value;
                    Heading heading;
                    switch (data.Mode)
                    {
                        case BsnDataModes.SetMargin:
                            headingForm.Mode = BsnModes.Edit;
                            headingForm.IsSetMarginForm = true;
                            heading = Contract.FindHeading(headingCode);
                            modalOpts.Title = "Set Margin: " + heading.Description;
                            break;
                        case BsnDataModes.Duplicate:                            
                            headingForm.Mode = BsnModes.Add;
                            headingForm.IsModal = true;
                            var fromHeading = Contract.FindHeading(headingCode);
                            headingForm.CopyFrom = fromHeading;
                            heading = new Heading(Contract, EstimateTypes.Production)
                            {
                                IsNew = true,
                                Description = fromHeading.Description + " - Copy"
                            };
                            modalOpts.Title = "Duplicate Heading: " + fromHeading.Code + " - " + fromHeading.Description;                            
                            break;
                        default:
                            return isModal;
                    }
                    App.LoadModal(heading, headingForm, modalOpts);
                    isModal = true;
                    break;
                case BsnForms.EstimateParagraphMargin:
                    Paragraph marginparagraph;
                    var marginParagraphCode = data.Value;
                    var paragrapMarginForm = (FM.EstimateParagraphMargin)App.Forms.Get(form);
                    paragrapMarginForm.Mode = BsnModes.Edit;
                    marginparagraph = Contract.FindParagraph(Heading, marginParagraphCode);
                    modalOpts.Title = "Set Margin: " + marginparagraph.Description;
                    App.LoadModal(marginparagraph, paragrapMarginForm, modalOpts);
                    isModal = true;
                    break;
                case BsnForms.EstimateParagraph:
                    Paragraph paragraph;
                    var paragraphCode = data.Value;
                    var paragraphForm = (FM.EstimateParagraph)App.Forms.Get(form);
                    switch (data.Mode)
                    {
                        case BsnDataModes.Add:
                            paragraphForm.Mode = BsnModes.Add;
                            modalOpts.Title = "New Paragraph";
                            paragraph = new Paragraph(Heading)
                            {
                                IsNew = true,
                            };
                            App.LoadModal(paragraph, paragraphForm, modalOpts);
                            isModal = true;
                            break;
                        case BsnDataModes.Edit:
                            paragraphForm.Mode = BsnModes.Edit;
                            modalOpts.Title = "Edit Paragraph";
                            paragraph = Contract.FindParagraph(Heading, paragraphCode);
                            App.LoadModal(paragraph, paragraphForm, modalOpts);
                            isModal = true;
                            break;
                        case BsnDataModes.Duplicate:
                            var fromParagraph = contract.FindParagraph(Heading, paragraphCode);
                            modalOpts.Title = "Duplicate Paragraph: " + fromParagraph.Code + " - " + fromParagraph.Description;
                            paragraph = new Paragraph(Heading)
                            {
                                IsNew = true,
                                Description = fromParagraph.Description + " - Copy"
                            };
                            paragraphForm.CopyFrom = fromParagraph;
                            App.LoadModal(paragraph, paragraphForm, modalOpts);
                            isModal = true;
                            break;
                        default:
                            return isModal;
                    }                    
                    break;
                case BsnForms.EstimateDetail:
                    var detailForm = (FM.EstimateDetail)App.Forms.Get(form);
                    detailForm.ShowAmounts = GetOption(ShowAmounts);
                    detailForm.ShowMargin = GetOption(ShowMargin);
                    detailForm.AdvancedMode = GetOption(AdvancedMode);
                    var detailsParagraph = Contract.Paragraphs(Heading).FirstOrDefault();
                    var newDetails = new EstimateDetail(detailsParagraph)
                    {
                        Key = "#" + detailsParagraph.Code,
                        SortOrder = detailsParagraph.Code + "999999",
                        IsNew = true,
                        Included = true,
                        Quantity = 1,
                        IncludeGST = detailsParagraph.IncludeGST,
                        ShowCheckbox = true,
                        ShowQuantity = true,
                        ShowSelect = true
                    };
                    modalOpts.Title = "New Estimate Detail Line";
                    App.LoadModal(newDetails, detailForm, modalOpts);
                    isModal = true;
                    break;
            }
            return isModal;
        }
        #endregion

        #region Load Options
        protected override void LoadOptions()
        {
            base.LoadOptions();
            if (InQuote && IsEstimate)
            {
                Options.Add(ShowFilter, ("Main Filter", true));
                Options.Add(ShowPriceDisplayFilter, ("Price Display Filter", false));
                Options.Add(ShowAmounts, ("Show Amounts", true));
                Options.Add(ShowMargin, ("Show Margin", false));
                Options.Add(ShowEmpty, ("Show Empty", false));
                Options.Add(AdvancedMode, ("Advanced Mode", false));
            }
        }
        #endregion

        #region Load Modes
        protected override void LoadModes()
        {
            base.LoadModes();
            if (Mode == BsnModes.ViewReport && !string.IsNullOrEmpty(ViewReport))
            {
                Modes.Add(new BsnButton(App.Utils.GetModeCallback(BsnModes.None), "times") { Active = true, Tooltip = "Close Report" });
            }
        }
        #endregion

        #region Load Estimate
        protected virtual void LoadEstimate()
        {
            if (Mode == BsnModes.ViewReport && !string.IsNullOrEmpty(ViewReport))
            {
                App.Viewer.Filename = ViewReport;
                App.Main.Controls.Add(App.Viewer.ToControl());
                return;
            }

            if (ModeChanged)
            {
                App.Settings.GridUID++;
            }

            var grid = (GD.Estimate)App.Grids.Get(BsnGrids.Estimate);
            grid.Contract = Contract;
            grid.Heading = Contract.FindHeading(Key, true);
            grid.ShowAmounts = GetOption(ShowAmounts);
            grid.ShowMargin = GetOption(ShowMargin);
            grid.ShowEmpty = GetOption(ShowEmpty);
            grid.AdvancedMode = GetOption(AdvancedMode);

            if (Mode == BsnModes.CostCentres)
            {
                CostCentreGrid(grid);
            }
            else
            {
                ParagraphGrid(grid);
            }
        }
        #endregion

        #region MainGrid
        protected virtual void MainGrid()
        {
            var grid = (GD.Headings)App.Grids.Get(BsnGrids.Headings);
            grid.Contract = Contract;
            App.AddGrid(grid);
        }
        #endregion

        #region CostCentreGrid
        protected virtual void CostCentreGrid(GD.Estimate grid)
        {
            grid.GroupField = "CostCentre";
            grid.GridDataMode = BsnDataModes.CostCentres;

            List<ComboItem> pricedisplays = new List<ComboItem>();

            var CostCentres = Contract.CostCentres(grid.Heading);
            CheckCostCentreFilter(CostCentres);

            if (GetOption(ShowPriceDisplayFilter))
            {
                pricedisplays = Contract.PriceDisplays(grid.Heading);
                CheckPriceDisplayFilter(pricedisplays);
            }

            
            var control = App.AddGrid(grid);

            if (Mode == BsnModes.ExportGrid)
            {
                return;
            }

            var estimate = new HtmlGenericControl("div");
            estimate.Controls.Add(EstimateSummary(null, -1));
            estimate.Controls.Add(control);

            if (GetOption(ShowFilter) || GetOption(ShowPriceDisplayFilter))
            {
                var row = new HtmlGenericControl("div");
                row.Attributes["class"] = "row";
                App.Main.Controls.Add(row);

                var group = new HtmlGenericControl("div");
                group.Attributes["class"] = "col-md-3 mb-3";
                group.Attributes["style"] = "overflow-x: auto";
                row.Controls.Add(group);

                CostCentreFilters.DataSource = CostCentres;
                CostCentreFilters.ShowCount = GetOption(ShowAmounts);
                CostCentreFilters.Load();
                group.Controls.Add(CostCentreFilters.ToControl());

                if (GetOption(ShowPriceDisplayFilter))
                {
                    PriceDisplayFilters.DataSource = pricedisplays;
                    PriceDisplayFilters.ShowCount = false;
                    PriceDisplayFilters.Load();
                    group.Controls.Add(new HtmlGenericControl("hr"));
                    group.Controls.Add(PriceDisplayFilters.ToControl());
                }

                var gridContainer = new HtmlGenericControl("div");
                gridContainer.Attributes["class"] = "col-md-9";
                gridContainer.Controls.Add(estimate);
                row.Controls.Add(gridContainer);
            }            
        }
        #endregion

        #region ParagraphGrid
        protected virtual void ParagraphGrid(GD.Estimate grid)
        {
            grid.GroupField = "ParagraphCode";
            grid.GridDataMode = BsnDataModes.Paragraphs;

            var paragraphs = Contract.Paragraphs(grid.Heading);
            var subheadings = paragraphs.Any(p => p.FormatOption.Equals("H"));
            grid.SubHeadings = subheadings;
            if (subheadings)
            {
                paragraphs = paragraphs.Where(p => p.FormatOption.Equals("H")).ToList();
            }

            ParagraphList.Clear();
            ParagraphList.Add(new ComboItem("*ALL", "All Paragraphs"));

            CheckParagraphFilter(paragraphs);

            var jason = grid.Grid.Selection.Count;
            int currentIndex = 0;
            foreach (var item in paragraphs)
            {
                if (!GetOption(ShowEmpty))
                {
                    var isEmpty = Contract.EstimateDetails(Heading)
                    .Where(p => p.Paragraph.Equals(item) && !p.Key.StartsWith("#")).Count() == 0;
                    if (isEmpty) continue;
                }
                ParagraphList.Add(new ComboItem(item.Code, item.Description));
                if (item.IsSelected) currentIndex = ParagraphList.Count - 1;
            }

            
            List<ComboItem> pricedisplays = new List<ComboItem>();
            if (GetOption(ShowPriceDisplayFilter))
            {
                pricedisplays = Contract.PriceDisplays(grid.Heading);
                CheckPriceDisplayFilter(pricedisplays);
            }
            
            var control = App.AddGrid(grid);

            if (Mode == BsnModes.ExportGrid)
            {
                return;
            }

            var estimate = new HtmlGenericControl("div");
            estimate.Controls.Add(EstimateSummary(ParagraphList, currentIndex));
            estimate.Controls.Add(Helpers.HtmlLine());
            estimate.Controls.Add(control);
            estimate.Controls.Add(EstimateSummary(ParagraphList, currentIndex));

            if (GetOption(ShowFilter))
            {
                var row = new HtmlGenericControl("div");
                row.Attributes["class"] = "row";
                App.Main.Controls.Add(row);

                var group = new HtmlGenericControl("div");
                group.Attributes["class"] = "col-md-3 mb-3";
                group.Attributes["style"] = "overflow-x: auto";
                row.Controls.Add(group);

                ParagraphFilters.DataSource = paragraphs;
                ParagraphFilters.ShowCount = GetOption(ShowAmounts);
                ParagraphFilters.AllowSort = false;
                ParagraphFilters.Load();
                group.Controls.Add(ParagraphFilters.ToControl());

                if (GetOption(ShowPriceDisplayFilter))
                {
                    PriceDisplayFilters.DataSource = pricedisplays;
                    PriceDisplayFilters.ShowCount = false;
                    PriceDisplayFilters.Load();
                    group.Controls.Add(new HtmlGenericControl("hr"));
                    group.Controls.Add(PriceDisplayFilters.ToControl());
                }
                var gridContainer = new HtmlGenericControl("div");
                gridContainer.Attributes["class"] = "col-md-9";
                gridContainer.Controls.Add(estimate);
                row.Controls.Add(gridContainer);
            }            
            else
            {
                App.Main.Controls.Add(estimate);
            }


        }
        #endregion

        #region CheckParagraphFilter
        protected virtual void CheckParagraphFilter(List<Paragraph> paragraphs)
        {
            var data = App.Callback.Data;
            Paragraphs.Clear();
            if (data != null && data.Mode == BsnDataModes.Filter && data.Key == "Paragraphs")
            {
                try
                {
                    if (data.Object != null)
                    {
                    var codes = data.Object.ToString();
                    Paragraphs = (List<string>)JsonConvert.DeserializeObject(codes, typeof(List<string>));
                }
                    else
                    {
                        Paragraphs = new List<string>();
                        var code = App.Callback.Value.ToString();
                        Paragraphs.Add(code);
                    }
                }
                catch
                {
                    Paragraphs = new List<string>();
                }
            }
            if (Paragraphs.Count == 0)
            {
                return;
            }
            foreach (var paragraph in paragraphs)
            {
                if (Paragraphs[0] == "*ALL")
                {
                    paragraph.IsSelected = false;
                }
                else
                {
                paragraph.IsSelected = Paragraphs.Contains(paragraph.Code);
            }
        }
        }
        #endregion

        #region CheckCostCentreFilter
        protected virtual void CheckCostCentreFilter(List<ComboItem> costCentres)
        {
            var data = App.Callback.Data;
            CostCentreCodes.Clear();
            if (data != null && data.Mode == BsnDataModes.Filter && data.Key == "CostCentres")
            {
                try
                {
                    var codes = data.Object.ToString();
                    CostCentreCodes = (List<string>)JsonConvert.DeserializeObject(codes, typeof(List<string>));
                }
                catch
                {
                    CostCentreCodes = new List<string>();
                }
            }
            if (CostCentreCodes.Count == 0)
            {
                return;
            }
            foreach (var CostCentre in costCentres)
            {
                CostCentre.IsSelected = CostCentreCodes.Contains(CostCentre.Code);
            }
        }
        #endregion

        #region CheckPriceDisplayFilter
        protected virtual void CheckPriceDisplayFilter(List<ComboItem> priceDisplay)
        {
            var data = App.Callback.Data;
            PriceDisplayCodes.Clear();
            if (data != null && data.Mode == BsnDataModes.Filter && data.Key == "PriceDisplay")
            {
                try
                {
                    var codes = data.Object.ToString();
                    PriceDisplayCodes = (List<string>)JsonConvert.DeserializeObject(codes, typeof(List<string>));
                }
                catch
                {
                    PriceDisplayCodes = new List<string>();
                }
            }
            if (PriceDisplayCodes.Count == 0)
            {
                return;
            }
            foreach (var PriceDisplay in priceDisplay)
            {
                PriceDisplay.IsSelected = PriceDisplayCodes.Contains(PriceDisplay.Code);
            }
        }
        #endregion

        #region EstimateSummary
        protected virtual HtmlGenericControl EstimateSummary(List<ComboItem> paragraphs, int currentIndex)
        {
            var estimateSummary = new HtmlGenericControl("div");
            estimateSummary.Attributes["class"] = "row";
            var heading = Contract.FindHeading(Key, true);
            if (heading != null)
            {
                if (GetOption(ShowAmounts))
                {
                    var col = new HtmlGenericControl("div");
                    if (GetOption(ShowMargin))
                    {
                        estimateSummary.Controls.Add(col);
                        col.Attributes["class"] = "col-sm";
                        var SaleAmount = new HtmlGenericControl("div");
                        col.Controls.Add(SaleAmount);
                        SaleAmount.Attributes["class"] = "h4 text-primary";
                        SaleAmount.InnerHtml = "Sell Amount: <br>" + heading.SellAmtEx.ToCurrency();

                        col = new HtmlGenericControl("div");
                        estimateSummary.Controls.Add(col);
                        col.Attributes["class"] = "col-sm";
                        var GSTAmount = new HtmlGenericControl("div");
                        col.Controls.Add(GSTAmount);
                        GSTAmount.Attributes["class"] = "h4 text-primary";
                        GSTAmount.InnerHtml = "GST Amount: <br>" + heading.SellAmtGST.ToCurrency();

                        col = new HtmlGenericControl("div");
                        estimateSummary.Controls.Add(col);
                        col.Attributes["class"] = "col-sm";
                        var CostAmount = new HtmlGenericControl("div");
                        col.Controls.Add(CostAmount);
                        CostAmount.Attributes["class"] = "h4 text-primary";
                        CostAmount.InnerHtml = "Cost Amount: <br>" + heading.CostAmt.ToCurrency();

                        col = new HtmlGenericControl("div");
                        estimateSummary.Controls.Add(col);
                        col.Attributes["class"] = "col-sm";
                        var MarginAmount = new HtmlGenericControl("div");
                        col.Controls.Add(MarginAmount);
                        MarginAmount.Attributes["class"] = "h4 text-primary";
                        MarginAmount.InnerHtml = "Margin Amount: <br>" + heading.MarginAmt.ToCurrency();

                        col = new HtmlGenericControl("div");
                        estimateSummary.Controls.Add(col);
                        col.Attributes["class"] = "col-sm";
                        var MarginPct = new HtmlGenericControl("div");
                        col.Controls.Add(MarginPct);
                        MarginPct.Attributes["class"] = "h4 text-primary";
                        MarginPct.InnerHtml = "Mgn %<br>" + heading.MarginPct.ToString("P2");
                    }
                    else
                    {
                        estimateSummary.Controls.Add(col);
                        col.Attributes["class"] = "col-sm";
                        var description = new HtmlGenericControl("div");
                        col.Controls.Add(description);
                        description.Attributes["class"] = "h4 text-primary";
                        description.InnerHtml = heading.Code + " - " + heading.Description;

                        if (!GetOption(ShowFilter))
                        {
                            estimateSummary.Controls.Add(ParagraphFilterNav(paragraphs, currentIndex));                         
                        }

                        col = new HtmlGenericControl("div");
                        estimateSummary.Controls.Add(col);
                        col.Attributes["class"] = "col-sm";
                        var SaleAmount = new HtmlGenericControl("div");
                        col.Controls.Add(SaleAmount);
                        SaleAmount.Attributes["class"] = "h4 text-primary float-md-right";
                        SaleAmount.InnerHtml = heading.SellAmtInc.ToCurrency();
                    }
                }
                else
                {
                    var col = new HtmlGenericControl("div");
                    estimateSummary.Controls.Add(col);
                    col.Attributes["class"] = "col-sm";
                    var description = new HtmlGenericControl("div");
                    col.Controls.Add(description);
                    description.Attributes["class"] = "h4 text-primary";
                    description.InnerHtml = heading.Code + " - " + heading.Description;

                    if (!GetOption(ShowFilter))
                    {
                        estimateSummary.Controls.Add(ParagraphFilterNav(paragraphs, currentIndex));                     
                    }
                }
            }
            return estimateSummary;
        }
        #endregion

        #region ParagraphFilterNav
        protected virtual HtmlGenericControl ParagraphFilterNav(List<ComboItem> paragraphs, int currentIndex)
        {
            var col = new HtmlGenericControl("div");
            col.Attributes["class"] = "col-sm btn-group";
            BsnButton first = new BsnButton(new BsnCallback(App)
            {
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.Filter,
                    Key = "Paragraphs",
                },
                Value = paragraphs.First().Code
            }, "chevron-double-left", "", true, ButtonTypes.Outline)
            {
                Tooltip = "First Paragraph",
                Enabled = currentIndex != 0,                
            };
            col.Controls.Add(first.ToControl());

            BsnButton prev = new BsnButton(new BsnCallback(App)
            {
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.Filter,
                    Key = "Paragraphs",
                },
                Value = currentIndex > 0 ? paragraphs[currentIndex - 1].Code : string.Empty
            }, "chevron-left", "", true, ButtonTypes.Outline)
            {
                Tooltip = "Previous Paragraph",
                Enabled = currentIndex != 0                
            };
            col.Controls.Add(prev.ToControl());

            var combo = new BootstrapComboBox()
            {
                DataSource = paragraphs,
                TextField = "Desc",
                ValueField = "Code",
                ValueType = typeof(string),
                Value = paragraphs[currentIndex].Code,                
            };
            combo.CssClasses.Control = "mx-2";
            combo.ClientSideEvents.ValueChanged = ParagraphComboCallback.ToComboBox(true);
            combo.DataBind();
            col.Controls.Add(combo);

            BsnButton next = new BsnButton(new BsnCallback(App)
            {
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.Filter,
                    Key = "Paragraphs",
                },
                Value = currentIndex < paragraphs.Count - 1 ? paragraphs[currentIndex + 1].Code : string.Empty
            }, "chevron-right", "", true, ButtonTypes.Outline)
            {
                Tooltip = "Next",
                Enabled = currentIndex < paragraphs.Count - 1
            };
            col.Controls.Add(next.ToControl());

            BsnButton last = new BsnButton(new BsnCallback(App)
            {
                Data = new CallbackData()
                {
                    Mode = BsnDataModes.Filter,
                    Key = "Paragraphs",
                },
                Value = paragraphs.Last().Code
            }, "chevron-double-right", "", true, ButtonTypes.Outline)
            {
                Tooltip = "Last Paragraph",
                Enabled = currentIndex < paragraphs.Count - 1,
            };
            col.Controls.Add(last.ToControl());
            return col;
        }
        #endregion

        #region Update Template
        protected virtual bool UpdateTemplate(CallbackData data)
        {
            var key = App.DataCallback.Key;
            if (string.IsNullOrEmpty(key))
            {
                return false;
            }
            if (data.Mode != BsnDataModes.SelectTemplate)
            {
                return false;
            }
            LoadFilter(data.Value);
            var template = App.VM.JC.Templates.Where(p => p.Heading.Item1.Equals(FilterHeading)).FirstOrDefault();
            if (template == null)
            {
                App.Utils.SetError("Template not found: " + FilterHeading);
                return false;
            }
            var extras = string.Empty;
            if (ShowCouncil)
            {
                var council = App.Settings.Comp(CompanySettingsType.CouncilCode).ToString();
                extras += council + "=" + FilterCouncil + ",";
            }
            if (ShowEstate)
            {
                var estate = App.Settings.Comp(CompanySettingsType.EstateCode).ToString();
                extras += estate + "=" + FilterEstate + ",";
            }
            var success = App.VM.JC.CopyTemplate(Contract, key, template, extras);
            if (success)
            {
                var location = App.Menus.Current.GetLocation();
                location.Mode = BsnModes.None;
                location.Value = string.Empty;
                App.DataLocation = location;
            }
            return success;
        }
        #endregion

        #region Get Filter
        protected virtual string GetFilter(int depth = 9, bool set = false)
        {
            var filter = string.Empty;
            var count = 0;
            if (count >= depth && !set)
            {
                return filter;
            }
            if (!string.IsNullOrEmpty(Filter1))
            {
                filter += Filter1 + ":";
                count++;
                if (set)
                {
                    FilterDepth++;
                }
            }
            if (count >= depth && !set)
            {
                return filter;
            }
            if (!string.IsNullOrEmpty(Filter2))
            {
                filter += Filter2 + ":";
                count++;
                if (set)
                {
                    FilterDepth++;
                }
            }
            if (count >= depth && !set)
            {
                return filter;
            }
            if (!string.IsNullOrEmpty(Filter3))
            {
                filter += Filter3 + ":";
                count++;
                if (set)
                {
                    FilterDepth++;
                }
            }
            if (count >= depth && !set)
            {
                return filter;
            }
            if (!string.IsNullOrEmpty(FilterHeading))
            {
                filter += FilterHeading + ":";
                count++;
                if (set)
                {
                    FilterDepth++;
                }
            }
            if (count >= depth && !set)
            {
                return filter;
            }
            if (!string.IsNullOrEmpty(FilterCouncil))
            {
                filter += FilterCouncil + ":";
                count++;
                if (set)
                {
                    FilterDepth++;
                }
            }
            if (count >= depth && !set)
            {
                return filter;
            }
            if (!string.IsNullOrEmpty(FilterEstate))
            {
                filter += FilterEstate + ":";
                if (set)
                {
                    FilterDepth++;
                }
            }
            return filter;
        }
        #endregion

        #region Load Filter
        protected virtual void LoadFilter(string value)
        {
            var filters = value.Split(':');
            Filter1 = Helpers.GetArray(filters, 0);
            Filter2 = Helpers.GetArray(filters, 1);
            Filter3 = Helpers.GetArray(filters, 2);
            FilterHeading = Helpers.GetArray(filters, 3);
            FilterCouncil = Helpers.GetArray(filters, 4);
            FilterEstate = Helpers.GetArray(filters, 5);
            _ = GetFilter(0, true);
        }
        #endregion

        #region Template DropDowns
        protected virtual void TemplateDropDowns()
        {
            var panel = new HtmlGenericControl("div");
            panel.Attributes["class"] = "row mb-3";
            panel.Controls.Add(Group1Combo);
            if (!string.IsNullOrEmpty(Filter1))
            {
                panel.Controls.Add(Group2Combo);
            }
            if (!string.IsNullOrEmpty(Filter2))
            {
                panel.Controls.Add(Group3Combo);
            }
            App.Main.Controls.Add(panel);
        }
        #endregion

        #region Additional DropDowns
        protected virtual void AdditionalDropDowns()
        {
            if (string.IsNullOrEmpty(FilterHeading))
            {
                return;
            }

            if (!ShowCouncil && !ShowEstate)
            {
                return;
            }

            var panel = new HtmlGenericControl("div");
            panel.Attributes["class"] = "row mb-3";
            if (ShowCouncil)
            {
                panel.Controls.Add(CouncilCombo);
            }
            if (ShowEstate)
            {
                if (!string.IsNullOrEmpty(FilterCouncil) || !ShowCouncil)
                {
                    panel.Controls.Add(EstateCombo);
                }
            }
            App.Main.Controls.Add(panel);
        }
        #endregion

        #region Council Combo
        protected virtual BootstrapComboBox CouncilCombo
        {
            get
            {
                var council = new BootstrapComboBox()
                {
                    Caption = "Council",
                    Value = FilterCouncil,
                    DataSource = App.VM.JC.TemplateCouncils,
                    TextField = "Desc",
                    ValueField = "Code",
                    ValueType = typeof(string)
                };
                FilterCallback.Value = GetFilter(4);
                council.ClientSideEvents.ValueChanged = FilterCallback.ToComboBox(true);
                council.CssClasses.Control = "col-4";
                council.DataBind();
                return council;
            }
        }
        #endregion

        #region Estate Combo
        protected virtual BootstrapComboBox EstateCombo
        {
            get
            {
                var estate = new BootstrapComboBox()
                {
                    Caption = "Estate",
                    Value = FilterEstate,
                    DataSource = App.VM.JC.TemplateEstates,
                    TextField = "Desc",
                    ValueField = "Code",
                    ValueType = typeof(string)
                };
                FilterCallback.Value = GetFilter(5);
                estate.ClientSideEvents.ValueChanged = FilterCallback.ToComboBox(true);
                estate.CssClasses.Control = "col-4";
                estate.DataBind();
                return estate;
            }
        }
        #endregion   

        #region Group1 Data
        List<ComboItem> Group1Data
        {
            get
            {
                return App.VM.JC.Templates.GroupBy(m => m.Group1)
                    .Select(n => new ComboItem(n.Key.Item1, n.Key.Item2)).ToList();
            }
        }
        #endregion

        #region Group1 Combo
        protected virtual BootstrapComboBox Group1Combo
        {
            get
            {
                var group1 = new BootstrapComboBox()
                {
                    Caption = App.VM.JC.TemplateControl.Group1Desc,
                    Value = Filter1,
                    DataSource = Group1Data,
                    TextField = "Desc",
                    ValueField = "Code",
                    ValueType = typeof(string)
                };
                FilterCallback.Value = string.Empty;
                group1.ClearButton.DisplayMode = DevExpress.Web.ClearButtonDisplayMode.Always;
                group1.ClientSideEvents.ValueChanged = FilterCallback.ToComboBox(true);
                group1.CssClasses.Control = "col-4";
                group1.DataBind();
                return group1;
            }
        }
        #endregion   

        #region Group2 Data
        protected virtual List<ComboItem> Group2Data
        {
            get
            {
                return App.VM.JC.Templates.Where(p => p.Group1.Item1.Equals(Filter1)).GroupBy(m => m.Group2)
                    .Select(n => new ComboItem(n.Key.Item1, n.Key.Item2)).ToList();
            }
        }
        #endregion

        #region Group2 Combo
        protected virtual BootstrapComboBox Group2Combo
        {
            get
            {
                var group2 = new BootstrapComboBox()
                {
                    Caption = App.VM.JC.TemplateControl.Group2Desc,
                    Value = Filter2,
                    DataSource = Group2Data,
                    TextField = "Desc",
                    ValueField = "Code",
                    ValueType = typeof(string)
                };
                FilterCallback.Value = GetFilter(1);
                group2.ClearButton.DisplayMode = DevExpress.Web.ClearButtonDisplayMode.Always;
                group2.ClientSideEvents.ValueChanged = FilterCallback.ToComboBox(true);
                group2.CssClasses.Control = "col-4";
                group2.DataBind();
                return group2;
            }
        }
        #endregion   

        #region Group3 Data
        protected virtual List<ComboItem> Group3Data
        {
            get
            {
                return App.VM.JC.Templates
                    .Where(p => p.Group1.Item1.Equals(Filter1) && p.Group2.Item1.Equals(Filter2))
                    .GroupBy(m => m.Group3)
                    .Select(n => new ComboItem(n.Key.Item1, n.Key.Item2)).ToList();
            }
        }
        #endregion

        #region Group3 Combo
        protected virtual BootstrapComboBox Group3Combo
        {
            get
            {
                var group3 = new BootstrapComboBox()
                {
                    Caption = App.VM.JC.TemplateControl.Group3Desc,
                    Value = Filter3,
                    DataSource = Group3Data,
                    TextField = "Desc",
                    ValueField = "Code",
                    ValueType = typeof(string),
                };
                FilterCallback.Value = GetFilter(2);
                group3.ClearButton.DisplayMode = DevExpress.Web.ClearButtonDisplayMode.Always;
                group3.ClientSideEvents.ValueChanged = FilterCallback.ToComboBox(true);
                group3.CssClasses.Control = "col-4";
                group3.DataBind();
                return group3;
            }
        }
        #endregion

        #region Filter Templates
        protected virtual void FilterTemplates()
        {
            if (!string.IsNullOrEmpty(FilterHeading))
            {
                return;
            }

            var previousGroup1 = string.Empty;
            var previousGroup2 = string.Empty;
            var previousGroup3 = string.Empty;

            if (!string.IsNullOrEmpty(Filter1))
            {
                Container = new HtmlGenericControl("div");
                Container.Attributes["class"] = "row";
                App.Main.Controls.Add(Container);
            }

            foreach (var template in App.VM.JC.Templates)
            {
                string dbg = "ShowTemplates| Group1=" + template.Group1.Item1 + " Group2=" + template.Group2.Item1;
                //Log.Debug(dbg);

                bool show = (string.IsNullOrEmpty(Filter1) || template.Group1.Item1 == Filter1);
                if (show && template.Group1.Item1 != previousGroup1)
                {
                    if (string.IsNullOrEmpty(Filter1))
                    {
                        previousGroup2 = string.Empty;
                        AddGroup1Panel(template.Group1);
                    }
                }
                if (show)
                {
                    show = (string.IsNullOrEmpty(Filter2) || template.Group2.Item1 == Filter2);
                }
                if (show)
                {
                    if (string.IsNullOrEmpty(Filter2))
                    {
                        if (template.Group2.Item1 != previousGroup2)
                        {
                            AddTemplate(template, 2);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Filter3))
                        {
                            if (template.Group3.Item1 != previousGroup3)
                            {
                                AddTemplate(template, 3);
                            }
                        }
                        else
                        {
                            if (template.Group3.Item1 == Filter3)
                            {
                                AddTemplate(template, 4);
                            }
                        }
                    }
                }
                if (show)
                {
                    dbg = "Showing filter1=" + Filter1 + " filter2=" + Filter2 + " filter3=" + Filter3 + " show=" + show;
                    //Log.Debug(dbg);
                    previousGroup3 = template.Group3.Item1;
                    previousGroup2 = template.Group2.Item1;
                    previousGroup1 = template.Group1.Item1;
                }
                else
                {
                    previousGroup3 = string.Empty;
                    previousGroup2 = string.Empty;
                    previousGroup1 = string.Empty;
                }
            }
        }
        #endregion

        #region Add Template        
        protected virtual void AddTemplate(Template template, int level)
        {
            var titleText = string.Empty;
            var extraText = string.Empty;
            var keyval = template.Group1.Item1 + ":" + template.Group2.Item1;
            var filtermode = false;

            var group3Count = App.VM.JC.Templates.Where(p => p.Group3.Item1.Equals(template.Group3.Item1)).Count();
            var templateCount = App.VM.JC.Templates.Where(p => p.Code.Item1.Equals(template.Code.Item1)).Count();

            switch (level)
            {
                case 2:
                    titleText = template.Group2.Item2;
                    filtermode = true;
                    if (group3Count == 1)
                    {
                        keyval += ":" + template.Group3.Item1;
                        if (templateCount == 1)
                        {
                            keyval += ":" + template.Heading.Item1;
                        }
                    }
                    break;
                case 3:
                    titleText = template.Group2.Item2;
                    extraText = template.Group3.Item2;
                    keyval += ":" + template.Group3.Item1;
                    if (templateCount == 1)
                    {
                        keyval += ":" + template.Heading.Item1;
                    }
                    break;
                case 4:
                    titleText = template.Heading.Item2;
                    extraText = template.Code.Item2;
                    keyval += ":" + template.Group3.Item1;
                    keyval += ":" + template.Heading.Item1;
                    break;

            }
            FilterCallback.Value = keyval;

            var thumb = Helpers.ResolveUrl("~/Viewers/Thumbnail.aspx?");
            thumb += "image=" + ImageFile(template, filtermode);
            thumb += "&width=" + App.Settings.Comp(CompanySettingsType.ThumbnailWidth).ToInt();
            thumb += "&height=" + App.Settings.Comp(CompanySettingsType.ThumbnailHeight).ToInt();

            var card = new HtmlGenericControl("div");
            card.Attributes["class"] = "card m-2";

            var img = new HtmlGenericControl("img");
            img.Attributes["src"] = thumb;
            img.Attributes["class"] = "card-img-top";
            card.Controls.Add(img);

            var body = new HtmlGenericControl("div");
            body.Attributes["class"] = "card-body text-center";
            card.Controls.Add(body);

            var title = new HtmlGenericControl("h5");
            title.Attributes["class"] = "card-title text-center";
            title.Controls.Add(new LiteralControl(titleText));
            body.Controls.Add(title);

            if (!string.IsNullOrEmpty(extraText))
            {
                var caption = new HtmlGenericControl("p");
                caption.Attributes["class"] = "card-text text-center";
                caption.Controls.Add(new LiteralControl(extraText));
                body.Controls.Add(caption);
            }

            body.Controls.Add(new BsnButton(FilterCallback,
                "check-circle", "Select", false, ButtonTypes.Primary).ToControl());

            Container.Controls.Add(card);
        }
        #endregion

        #region ImageFile
        protected virtual string ImageFile(Template template, bool filtermode)
        {
            var ImageBase = App.Utils.ImagesFolder + "templates/";
            var TemplateBase = template.Group1.Item2 + "/" + template.Group2.Item2;
            var imageFile = Helpers.ResolveUrl(ImageBase + TemplateBase + template.Group3.Item2 + ".jpg");
            if (filtermode || !File.Exists(HttpContext.Current.Server.MapPath(imageFile)))
            {
                imageFile = Helpers.ResolveUrl(ImageBase + TemplateBase + ".jpg");
                if (!File.Exists(HttpContext.Current.Server.MapPath(imageFile)))
                {
                    TemplateBase = template.Group1.Item2 + "/" + template.Group2.Item1;
                    imageFile = Helpers.ResolveUrl(ImageBase + TemplateBase + ".jpg");
                }
                if (!File.Exists(HttpContext.Current.Server.MapPath(imageFile)))
                {
                    imageFile = Helpers.ResolveUrl(ImageBase + template.Group1.Item2 + ".jpg");
                }
            }
            return imageFile;
        }
        #endregion

        #region Add Group1 Panel        
        protected virtual void AddGroup1Panel((string, string) group1)
        {
            //string objName = "template" + code.ToLower();
            var panel = new HtmlGenericControl("div");
            panel.Attributes["class"] = "card mb-3";
            var heading = new HtmlGenericControl("div");
            heading.Attributes["class"] = "card-header";
            var span = new HtmlGenericControl("span");
            span.Attributes["class"] = "h2 text-primary";
            span.Controls.Add(new LiteralControl(group1.Item2));
            heading.Controls.Add(span);
            panel.Controls.Add(heading);
            App.Main.Controls.Add(panel);

            Container = new HtmlGenericControl("div");
            Container.Attributes["class"] = "card-body row";
            panel.Controls.Add(Container);
        }
        #endregion

        #region Select Template
        protected virtual void SelectTemplate()
        {
            if (string.IsNullOrEmpty(FilterHeading))
            {
                return;
            }

            var panel = new HtmlGenericControl("div");
            panel.Attributes["class"] = "my-3 text-primary text-center";
            App.Main.Controls.Add(panel);

            if (ShowCouncil && string.IsNullOrEmpty(FilterCouncil))
            {
                panel.Controls.Add(new LiteralControl("Please select a council."));
                return;
            }

            if (ShowEstate && string.IsNullOrEmpty(FilterEstate))
            {
                panel.Controls.Add(new LiteralControl("Please select an estate."));
                return;
            }

            var template = App.VM.JC.Templates.Where(p => p.Heading.Item1.Equals(FilterHeading)).FirstOrDefault();
            if (template == null)
            {
                panel.Controls.Add(new LiteralControl("Selected template not found."));
                return;
            }

            AllowCopy = true;

            var imgPanel = new HtmlGenericControl("div");
            imgPanel.Attributes["class"] = "my-3 text-center";
            panel.Controls.Add(imgPanel);

            var thumb = Helpers.ResolveUrl("~/Viewers/Thumbnail.aspx?image=") + ImageFile(template, false) + "&width=380&height=285";
            var img = new HtmlGenericControl("img");
            img.Attributes["src"] = thumb;
            imgPanel.Controls.Add(img);


        }
        #endregion

        #region Nav Buttons
        protected virtual void NavButtons()
        {
            if (string.IsNullOrEmpty(Filter1))
            {
                return;
            }
            var panel = new HtmlGenericControl("div");
            panel.Attributes["class"] = "my-3 text-center";
            FilterCallback.Value = GetFilter(FilterDepth - 1);
            var backButton = new BsnButton(FilterCallback, "arrow-circle-left", "Back", true, ButtonTypes.Outline)
            { CSS = "mx-2" };
            panel.Controls.Add(backButton.ToControl());

            if (AllowCopy)
            {
                var copyButton = new BsnButton(new BsnCallback(App)
                {
                    Type = CallbackType.DataOnly,
                    Key = App.Key,
                    Data = new CallbackData()
                    {
                        Mode = BsnDataModes.SelectTemplate,
                        Value = GetFilter()
                    }
                }, "check-circle", "Select", true, ButtonTypes.Primary)
                { CSS = "mx-2" };
                panel.Controls.Add(copyButton.ToControl());
            }

            App.Main.Controls.Add(panel);
        }
        #endregion

    }
}
