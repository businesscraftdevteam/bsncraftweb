﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System.Web.UI;
using BsnCraft.Web.Common.Views.Form.Base;
using System.Linq;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Buttons;
using GD = BsnCraft.Web.Common.Views.Grid.Contracts;
using AR = BsnCraft.Web.Common.Views.Grid.Customers;

namespace BsnCraft.Web.Common.Views.Menu.Contracts.Base
{
    public abstract class DefaultBase : BsnMenu
    {
        #region Contract
        private Contract contract;
        public Contract Contract
        {
            get
            {
                if (contract == null)
                {
                    contract = new Contract(App);
                }
                return contract;
            }
        }
        #endregion

        #region Private Properties
        private bool HasImportant { get; set; }
        private bool IsLead { get; set; }
        #endregion

        #region Constructor
        public DefaultBase(BsnApplication app, BsnMenus menu, bool isLead) : base(app, true)
        {
            Init(menu);
            IsLead = isLead;
            Icon = IsLead ? "filter" : "handshake";
            MenuActions.Add(new BsnMenuAction(BsnActions.Default) { Icon = "exclamation-triangle", AllModes = false });
            MenuActions.Add(new BsnMenuAction(BsnActions.Default, BsnModes.Open) { Caption = "Details" });
            MenuActions.Add(new BsnMenuAction(BsnActions.Contacts));
            MenuActions.Add(new BsnMenuAction(BsnActions.JobInfo) { Caption = "Job Info" });
            MenuActions.Add(new BsnMenuAction(BsnActions.Quotes) { Caption = "Quotes" });
            MenuActions.Add(new BsnMenuAction(BsnActions.Documents));
            MenuActions.Add(new BsnMenuAction(BsnActions.Events));            
            if (IsLead)
            {
                MenuActions.Add(new BsnMenuAction(BsnActions.DealSubmit) { Caption = "Deal" });
            }
            MenuActions.Add(new BsnMenuAction(BsnActions.Variations));
        }
        #endregion

        #region GetCount
        public override int GetCount()
        {
            return IsLead ? App.VM.LE.Leads.Count : App.VM.CO.Contracts.Count;
        }
        #endregion

        #region Visible
        public override bool Visible
        {
            get
            {
                var visible = base.Visible;
                if (visible)
                {
                    var setting = IsLead ? CompanySettingsType.ActiveLeadStatusList :
                        CompanySettingsType.ActiveContractStatusList;
                    var sts = App.Settings.Comp(CompanySettingsType.ActiveContractStatusList).ToStrArray();
                    visible = (sts.Length > 1 || !string.IsNullOrEmpty(Helpers.GetArray(sts, 0)));
                }
                return visible;
            }
        }
        #endregion

        #region Set Title
        protected virtual void SetTitle()
        {
            var title = string.Empty;
            switch (Mode)
            {
                case BsnModes.Add:
                case BsnModes.Edit:
                    title = Mode.ToString();
                    break;
            }
            if (!string.IsNullOrEmpty(Id))
            {
                title += " " + Contract.FriendlyName;
            }
            Title = title;
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            if (string.IsNullOrEmpty(Id))
            {
                contract = new Contract(App);
                return;
            }
            contract = App.VM.CO.GetContract(Id);
            if (contract == null)
            {
                contract = new Contract(App);
                App.Main.Controls.Add(new LiteralControl(Id + " not found."));
                return;
            }
            SetTitle();
            HasImportant = (Contract.ImportantDocuments.Count != 0);
        }
        #endregion

        #region Load
        protected override void Load()
        {
            if (App.InDataCallback)
            {
                return;
            }

            base.Load();
            CheckActions();

            switch (Mode)
            {
                case BsnModes.Add:
                    Add();
                    return;
                case BsnModes.Edit:
                case BsnModes.Open:
                    if (Edit())
                    {
                        InGridMode = false;
                        return;
                    }
                    break;
            }

            if (string.IsNullOrEmpty(Id))
            {
                MainGrid();
                return;
            }

            switch (Action)
            {
                case BsnActions.Default:                
                    if (HasImportant)
                    {
                        ImportantDocuments();
                        return;
                    }
                    Contacts();
                    break;
                case BsnActions.DealSubmit:
                    DealSubmit();
                    break;
            }            
        }
        #endregion

        #region Check Actions
        protected virtual void CheckActions()
        {
            var ImportantAction = GetActions().Where(p => p.UID.Equals((BsnActions.Default, BsnModes.None))).FirstOrDefault();
            ImportantAction.Visible = HasImportant;

            var hideActions = App.Settings.Comp(CompanySettingsType.HideContractDetails).ToStrArray();
            foreach (var action in MenuActions.Where(p => p.Visible))
            {
                var visible = !hideActions.Contains(action.UID.Item1.ToString());
                action.Visible = visible;
            }
        }
        #endregion

        #region Load Modes
        protected override void LoadModes()
        {
            base.LoadModes();
            if (InGridMode)
            {
                return;
            }
            if (Mode == BsnModes.Add)
            {
                return;
            }
            var editMode = Mode == BsnModes.Edit;
            var callback = editMode ? App.Utils.GetModeCallback(BsnModes.Open) : App.Utils.GetModeCallback(BsnModes.Edit);
            var tooltip = IsLead ? "Edit Lead" : "Edit Contract";
            Modes.Add(new BsnButton(callback, "pencil") { Active = editMode, Tooltip = tooltip });
        }
        #endregion

        #region Add
        protected virtual void Add()
        {
            InGridMode = false;
            var form = App.Forms.Get(BsnForms.Contract);
 
            form.Load(App.VM.CO.NewContract(IsLead));
            App.Main.Controls.Add(form.ToControl());
        }
        #endregion
        
        #region Edit
        protected virtual bool Edit()
        {
            if (Contract == null || string.IsNullOrEmpty(Contract.ContractNumber))
            {
                return false;
            }
            var form = App.Forms.Get(BsnForms.Contract);
            var readOnly = (Mode == BsnModes.Open);
            form.Load(Contract, readOnly);
            App.Main.Controls.Add(form.ToControl());            
            return true;
        }
        #endregion

        #region MainGrid
        protected virtual void MainGrid()
        { 
            var grid = App.Grids.Get(IsLead ? BsnGrids.Leads : BsnGrids.Contracts);
            grid.Reset();
            App.AddGrid(grid);
        }
        #endregion

        #region Important Documents
        protected virtual void ImportantDocuments()
        {
            var grid = (GD.ImportantDocs)App.Grids.Get(BsnGrids.ImportantDocs);
            grid.Contract = Contract;
            App.AddGrid(grid);
        }
        #endregion

        #region Contacts
        protected virtual void Contacts()
        {
            GetActions().Where(p => p.UID.Equals((BsnActions.Contacts, BsnModes.None))).FirstOrDefault().Selected = true;
            var grid = (AR.Contacts)App.Grids.Get(BsnGrids.Contacts);
            grid.Reset();
            grid.CustomerNumber = Contract.CustomerNumber;
            App.AddGrid(grid);
        }
        #endregion

        #region Deal Submit
        protected virtual void DealSubmit()
        {
            var grid = (GD.DealDocs)App.Grids.Get(BsnGrids.DealDocs);
            grid.Contract = Contract;
            App.AddGrid(grid);
        }
        #endregion

    }
}
