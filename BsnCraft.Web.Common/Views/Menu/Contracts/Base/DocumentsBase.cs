﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System.Web.UI;
using FM = BsnCraft.Web.Common.Views.Form.Contracts;
using GD = BsnCraft.Web.Common.Views.Grid.Contracts;
using BsnCraft.Web.Common.Views.Form.Base;
using System.Linq;
using BsnCraft.Web.Common.Views.Buttons;

namespace BsnCraft.Web.Common.Views.Menu.Contracts.Base
{
    public abstract class DocumentsBase : BsnMenu
    {
        #region Contract
        private Contract contract;
        public Contract Contract
        {
            get
            {
                if (contract == null)
                {
                    contract = new Contract(App);
                }
                return contract;
            }
        }
        #endregion

        #region Constructor
        public DocumentsBase(BsnApplication app, BsnMenus menu) : base(app)
        {
            Init(menu, BsnActions.Documents);
        }
        #endregion
      
        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            if (string.IsNullOrEmpty(Id))
            {
                return;
            }
            contract = App.VM.CO.GetContract(Id);
            if (contract == null)
            {
                App.Main.Controls.Add(new LiteralControl(Id + " not found."));
                return;
            }
            if (GetActions().Any(p => p.UID.Equals((BsnActions.Default, BsnModes.None))))
            {
                var ImportantAction = GetActions().Where(p => p.UID.Equals((BsnActions.Default, BsnModes.None))).FirstOrDefault();
                ImportantAction.Visible = (Contract.ImportantDocuments.Count != 0);
            }
            Title = Contract.FriendlyName;
        }
        #endregion

        #region Load
        protected override void Load()
        {
            base.Load();
            if (string.IsNullOrEmpty(Contract.ContractNumber))
            {
                return;
            }
            switch (Mode)
            {
                case BsnModes.Add:
                    Add();
                    return;
                case BsnModes.Edit:
                case BsnModes.Open:
                    if (Edit())
                    {
                        return;
                    }
                    break;
                case BsnModes.View:
                    if (View())
                    {
                        return;
                    }
                    break;
            }
            MainGrid();
        }
        #endregion

        #region After Nav
        protected override void AfterNav()
        {
            base.AfterNav();
            if (Mode != BsnModes.View)
            {
                return;
            }
            //TODO: add javascript fullscreen function
            //This doesn't work as all parent containers need h-100
            //App.Main.Attributes["class"] += " h-100";
        }
        #endregion

        #region Load Modes
        protected override void LoadModes()
        {
            base.LoadModes();
            if (InGridMode)
            {
                return;
            }
            var document = Contract.FindDocument(Key, true);
            if (document == null)
            {
                return;
            }
            var editMode = Mode == BsnModes.Edit;
            var callback = editMode ? App.Utils.GetModeCallback(BsnModes.Open) : App.Utils.GetModeCallback(BsnModes.Edit);
            Modes.Add(new BsnButton(callback, "pencil") { Active = editMode, Tooltip = "Edit Document" });
            if (!string.IsNullOrEmpty(document.Filename))
            {
                callback = App.Utils.GetModeCallback(BsnModes.View);
                Modes.Add(new BsnButton(callback, "search") { Active = (Mode == BsnModes.View), Tooltip = "View Document" });
            }            
        }
        #endregion

        #region Add
        protected virtual void Add()
        {
            InGridMode = false;
            var form = (FM.Document)App.Forms.Get(BsnForms.Document);
            var document = new Document(Contract);            
            ShowUpload = true;
            MultiUpload = true;
            form.Load(document);
            App.Main.Controls.Add(form.ToControl());
        }
        #endregion

        #region Edit
        protected virtual bool Edit()
        {
            var document = Contract.FindDocument(Key, true);
            if (document == null)
            {
                return false;
            }
            InGridMode = false;
            ShowUpload = string.IsNullOrEmpty(document.Filename) && (Mode == BsnModes.Edit);
            MultiUpload = false;
            var form = (FM.Document)App.Forms.Get(BsnForms.Document);
            var readOnly = (Mode == BsnModes.Open);
            form.Load(document, readOnly);            
            App.Main.Controls.Add(form.ToControl());
            return true;
        }
        #endregion

        #region View
        protected virtual bool View()
        {
            var document = Contract.FindDocument(Key, true);
            if (document == null)
            {
                return false;
            }
            InGridMode = false;
            var docType = App.VM.SY.FindDocumentType(document.Type, document.Module);
            if (docType == null)
            {
                App.Utils.SetError("Document type not found: " + document.Type);
                return false;
            }            
            int waitSeconds = (string.IsNullOrEmpty(docType.MasterDocument)) ? 0 : 10;
            if (!App.VM.CO.DownloadDocument(document, false, waitSeconds))
            {
                App.Utils.SetError("Error downloading document: " + document.Filename);
                return false;
            }
            App.Viewer.Filename = App.Utils.DocsFolder + document.FileOnly;
            App.Viewer.MasterFile = docType.MasterDocument;
            App.Main.Controls.Add(App.Viewer.ToControl());
            return true;
        }
        #endregion

        #region MainGrid
        protected virtual void MainGrid()
        {
            var grid = (GD.Documents)App.Grids.Get(BsnGrids.Documents);
            grid.Contract = Contract;
            App.AddGrid(grid);
        }
        #endregion        

    }
}
