﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System.Web.UI;
using FM = BsnCraft.Web.Common.Views.Form.Contracts;
using GD = BsnCraft.Web.Common.Views.Grid.Contracts;
using BsnCraft.Web.Common.Views.Form.Base;
using System.Linq;

namespace BsnCraft.Web.Common.Views.Menu.Contracts.Base
{
    public abstract class EventsBase : BsnMenu
    {
        #region Contract
        private Contract contract;
        public Contract Contract
        {
            get
            {
                if (contract == null)
                {
                    contract = new Contract(App);
                }
                return contract;
            }
        }
        #endregion
        
        #region Constructor
        public EventsBase(BsnApplication app, BsnMenus menu) : base(app)
        {
            Init(menu, BsnActions.Events);
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            if (string.IsNullOrEmpty(Id))
            {
                contract = new Contract(App);
                return;
            }
            contract = App.VM.CO.GetContract(Id);
            if (contract == null)
            {
                contract = new Contract(App);
                App.Main.Controls.Add(new LiteralControl(Id + " not found."));
                return;
            }
            Title = Contract.FriendlyName;
            if (GetActions().Any(p => p.UID.Equals((BsnActions.Default, BsnModes.None))))
            {
                var ImportantAction = GetActions().Where(p => p.UID.Equals((BsnActions.Default, BsnModes.None))).FirstOrDefault();
                ImportantAction.Visible = (Contract.ImportantDocuments.Count != 0);
            }
        }
        #endregion

        #region PerformUpdate
        protected override bool PerformUpdate(CallbackData data)
        {
            base.PerformUpdate(data);
            bool updated = false;
            if (string.IsNullOrEmpty(Contract.ContractNumber))
            {
                return updated;
            }
            var contractEvent = Contract.FindEvent(data.Key);            
            switch (data.Mode)
            {
                case BsnDataModes.QuickRegister:
                    App.VM.CO.EventRegister(contractEvent);
                    updated = true;
                    break;
                default:
                    return updated;
            }
            App.DataLocation = App.Menus.Current.GetLocation();
            return updated;
        }
        #endregion

        #region Load
        protected override void Load()
        {
            base.Load();

            if (string.IsNullOrEmpty(Contract.ContractNumber))
            {
                return;
            }

            if (string.IsNullOrEmpty(Key))
            {
                MainGrid();
                return;
            }
            if (Mode == BsnModes.Edit)
            {
                Edit();
                return;
            }
        }
        #endregion

        #region Edit
        protected virtual void Edit()
        {
            var form = (FM.Contract)App.Forms.Get(BsnForms.Contract);
            form.Load(Contract);
            App.Main.Controls.Add(form.ToControl());
        }
        #endregion

        #region MainGrid
        protected virtual void MainGrid()
        {
            var grid = (GD.Events)App.Grids.Get(BsnGrids.Events);
            grid.Contract = Contract;
            App.AddGrid(grid);
        }
        #endregion
        
    }
}
