﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System.Web.UI;
using System.Linq;
using System.Web.UI.HtmlControls;
using System;
using System.Globalization;
using GD = BsnCraft.Web.Common.Views.Grid.Jobs;

namespace BsnCraft.Web.Common.Views.Menu.Contracts.Base
{
    public class JobInfoBase : BsnMenu
    {
        #region Contract
        private Contract contract;
        public Contract Contract
        {
            get
            {
                if (contract == null)
                {
                    contract = new Contract(App);
                }
                return contract;
            }
        }
        #endregion

        #region Constructor
        public JobInfoBase(BsnApplication app, BsnMenus menu) : base(app)
        {
            Init(menu, BsnActions.JobInfo);
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            if (string.IsNullOrEmpty(Id))
            {
                contract = new Contract(App);
                return;
            }
            contract = App.VM.CO.GetContract(Id);
            if (contract == null)
            {
                contract = new Contract(App);
            }
            Title = Contract.FriendlyName;
        }
        #endregion

        #region Load
        protected override void Load()
        {
            base.Load();
            if (!App.InCallback && !App.InDataCallback)
            {
                return;
            }
            if (string.IsNullOrEmpty(Contract.ContractNumber))
            {
                App.Main.Controls.Add(new LiteralControl("Contract: " + Id + " not found."));
                return;                
            }
            if (GetActions().Any(p => p.UID.Equals((BsnActions.Default, BsnModes.None))))
            {
                var ImportantAction = GetActions().Where(p => p.UID.Equals((BsnActions.Default, BsnModes.None))).FirstOrDefault();
                ImportantAction.Visible = (Contract.ImportantDocuments.Count != 0);
            }
            MainGrid();            
        }
        #endregion

        #region PerformUpdate
        protected override bool PerformUpdate(CallbackData data)
        {
            base.PerformUpdate(data);
            bool updated = false;
            var question = Contract.FindJobInfo(data.Key);
            var answer = data.Value;
            if (question == null)
            {
                App.Utils.SetError("Error saving Job Information. Question not found.");
                return updated;
            }
            if (question.HasAnswers)
            {
                if (question.Answers.Any(p => p.Code.Equals(answer)))
                {
                    var lookup = question.Answers.Where(p => p.Code.Equals(answer)).FirstOrDefault();
                    question.Answer = lookup.Code;
                    question.AnsDesc = lookup.Desc;
                    question.Modified = true;
                }
                else
                {
                    App.Utils.SetError("Error saving Job Information. Answer not found.");
                }
            }
            else
            {
                if (question.Type == "D")
                {
                    var day = DateTime.ParseExact(answer, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    answer = day.Year.ToString() + day.DayOfYear.ToString("000");
                }
                question.Answer = answer;
                question.Modified = true;
            }
            updated = question.Modified;
            App.VM.JI.UpdateJobInfo(Contract);
            return updated;
        }
        #endregion

        #region MainGrid
        protected virtual void MainGrid()
        {
            var grid = (GD.JobInfo)App.Grids.Get(BsnGrids.JobInfo);
            grid.Contract = Contract;
            if (Contract.JobInfoGroups.Count > 1)
            {
                var firstGroup = Contract.JobInfoGroups.FirstOrDefault().Key;
                grid.Group = App.Value.ValueOrDefault(firstGroup);
            }
            var control = App.AddGrid(grid);
            
            if (Contract.JobInfoGroups.Count > 1 && Mode != BsnModes.ExportGrid)
            {
                var cb = new BsnCallback(App, Callback.ToString());
                
                var row = new HtmlGenericControl("div");
                row.Attributes["class"] = "row";
                App.Main.Controls.Add(row);

                var groupContainer = new HtmlGenericControl("div");
                groupContainer.Attributes["class"] = "col-md-3 mb-3";
                row.Controls.Add(groupContainer);

                var groups = new HtmlGenericControl("div");
                groups.Attributes["class"] = "list-group";
                groupContainer.Controls.Add(groups);

                var header = new HtmlGenericControl("h6");
                header.Attributes["class"] = "list-group-header";
                header.Controls.Add(new LiteralControl("Groups"));
                groups.Controls.Add(header);

                foreach (var group in Contract.JobInfoGroups)
                {
                    var description = Helpers.GetArray(group.Value, 1);
                    var link = new HtmlGenericControl("a");
                    link.Attributes["href"] = "javascript:void(0);";
                    link.Attributes["class"] = "list-group-item list-group-item-action justify-content-between d-flex";
                    if (grid.Group == group.Key)
                    {
                        link.Attributes["class"] += " active";
                    }
                    //TODO: consider making an option on BsnMenu to disable tabs
                    //Use Value instead of Key for bypassing opening new tabs
                    cb.Value = group.Key;
                    link.Attributes["onclick"] = cb.ToCallback();
                    link.Controls.Add(new LiteralControl(description));
                    groups.Controls.Add(link);
                }

                var gridContainer = new HtmlGenericControl("div");
                gridContainer.Attributes["class"] = "col-md-9";
                gridContainer.Controls.Add(control);
                row.Controls.Add(gridContainer);
            }
            
        }
        #endregion

    }
}
