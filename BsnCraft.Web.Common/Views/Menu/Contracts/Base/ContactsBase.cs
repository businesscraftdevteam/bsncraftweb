﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System.Web.UI;
using BsnCraft.Web.Common.Views.Form.Base;
using System.Linq;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Buttons;
using GD = BsnCraft.Web.Common.Views.Grid.Customers;

namespace BsnCraft.Web.Common.Views.Menu.Contracts.Base
{
    public abstract class ContactsBase : BsnMenu
    {
        #region Contract
        private Contract contract;
        public Contract Contract
        {
            get
            {
                if (contract == null)
                {
                    contract = new Contract(App);
                }
                return contract;
            }
        }
        #endregion

        #region Constructor
        public ContactsBase(BsnApplication app, BsnMenus menu) : base(app)
        {
            Init(menu, BsnActions.Contacts);
        }
        #endregion

        #region Set Title
        protected virtual void SetTitle()
        {
            var title = string.Empty;                
            if (!string.IsNullOrEmpty(Id))
            {
                switch (Mode)
                {
                    case BsnModes.Add:
                    case BsnModes.Edit:
                        title = Mode.ToString();
                        break;
                }

                if (Mode != BsnModes.None)
                {
                    var contact = App.VM.AR.FindContact(Contract.CustomerNumber, Key);
                    title += (contact != null) ? " " + contact.FullName : " Contact";                        
                }                    
            }
            Title = title;
        }
        #endregion

        #region Visible
        public override bool Visible
        {
            get
            {
                var visible = base.Visible;
                if (visible)
                {
                    var sts = App.Settings.Comp(CompanySettingsType.ActiveContractStatusList).ToStrArray();
                    visible = (sts.Length > 1 || !string.IsNullOrEmpty(Helpers.GetArray(sts, 0)));
                }
                return visible;
            }
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.DataBind();
            if (string.IsNullOrEmpty(Id))
            {
                contract = new Contract(App);
                return;
            }
            contract = App.VM.CO.GetContract(Id);
            if (contract == null)
            {
                contract = new Contract(App);
                App.Main.Controls.Add(new LiteralControl(Id + " not found."));
                return;
            }
            if (GetActions().Any(p => p.UID.Equals((BsnActions.Default, BsnModes.None))))
            {
                var ImportantAction = GetActions().Where(p => p.UID.Equals((BsnActions.Default, BsnModes.None))).FirstOrDefault();
                ImportantAction.Visible = (Contract.ImportantDocuments.Count != 0);
            }
            SetTitle();
        }
        #endregion

        #region PerformUpdate
        protected override bool PerformUpdate(CallbackData data)
        {
            base.PerformUpdate(data);
            bool updated = false;
            var contact = App.VM.AR.FindContact(data.Key);
            if (contact == null)
            {
                return updated;
            }
            switch (data.Mode)
            {
                case BsnDataModes.Delete:
                    App.VM.AR.DeleteContact(contact);
                    updated = true;
                    break;
                default:
                    return updated;
            }
            App.DataLocation = App.Menus.Current.GetLocation();
            return updated;
        }
        #endregion

        #region Load
        protected override void Load()
        {
            if (App.InDataCallback)
            {
                return;
            }

            if (string.IsNullOrEmpty(Contract.ContractNumber))
            {
                return;
            }

            base.Load();

            switch (Mode)
            {
                case BsnModes.Add:
                    Add();
                    return;
                case BsnModes.Edit:
                case BsnModes.Open:
                    if (Edit())
                    {
                        InGridMode = false;
                        return;
                    }
                    break;
            }

            MainGrid();
        }
        #endregion

        #region Load Modes
        protected override void LoadModes()
        {
            base.LoadModes();
            if (InGridMode)
            {
                return;
            }
            if (Mode == BsnModes.Add)
            {
                return;
            }
            var editMode = Mode == BsnModes.Edit;
            var callback = editMode ? App.Utils.GetModeCallback(BsnModes.Open) : App.Utils.GetModeCallback(BsnModes.Edit);
            Modes.Add(new BsnButton(callback, "pencil") { Active = editMode, Tooltip = "Edit Contact" });
        }
        #endregion

        #region Add
        protected virtual void Add()
        {
            InGridMode = false;         
            var form = App.Forms.Get(BsnForms.Customer);
            var contact = new CustomerContact
            {
                CustomerNumber = Contract.CustomerNumber
            };
            form.Load(contact);
            App.Main.Controls.Add(form.ToControl());
        }
        #endregion

        #region Edit
        protected virtual bool Edit()
        {
            var form = App.Forms.Get(BsnForms.Customer);
            var contact = App.VM.AR.FindContact(Contract.CustomerNumber, Key);
            if (contact == null)
            {
                App.Utils.SetError("Contact not found.");
                return false;
            }
            var readOnly = (Mode == BsnModes.Open);
            form.Load(contact, readOnly);
            App.Main.Controls.Add(form.ToControl());
            return true;
        }
        #endregion

        #region MainGrid
        protected virtual void MainGrid()
        {
            GetActions().Where(p => p.UID.Equals((BsnActions.Contacts, BsnModes.None))).FirstOrDefault().Selected = true;
            var grid = (GD.Contacts)App.Grids.Get(BsnGrids.Contacts);
            grid.Reset();
            grid.CustomerNumber = Contract.CustomerNumber;
            App.AddGrid(grid);
        }
        #endregion

    }
}
