﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System.Web.UI;
using FM = BsnCraft.Web.Common.Views.Form.Contracts;
using GD = BsnCraft.Web.Common.Views.Grid.Contracts;
using BsnCraft.Web.Common.Views.Form.Base;
using System.Linq;

namespace BsnCraft.Web.Common.Views.Menu.Contracts.Base
{
    public class VariationsBase : BsnMenu
    {
        #region Contract
        private Contract contract;
        public Contract Contract
        {
            get
            {
                if (contract == null)
                {
                    contract = new Contract(App);
                }
                return contract;
            }
        }
        #endregion

        #region Constructor
        public VariationsBase(BsnApplication app, BsnMenus menu) : base(app)
        {
            Init(menu, BsnActions.Variations);
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            contract = App.VM.CO.GetContract(Id);
            if (contract == null)
            {
                contract = new Contract(App);
                App.Main.Controls.Add(new LiteralControl(Id + " not found."));
            }

            if (GetActions().Any(p => p.UID.Equals((BsnActions.Default, BsnModes.None))))
            {
                var ImportantAction = GetActions().Where(p => p.UID.Equals((BsnActions.Default, BsnModes.None))).FirstOrDefault();
                ImportantAction.Visible = (Contract.ImportantDocuments.Count != 0);
            }
            Title = Contract.FriendlyName;
        }
        #endregion

        #region Load
        protected override void Load()
        {
            base.Load();

            if (string.IsNullOrEmpty(Contract.ContractNumber))
                return;

            if (string.IsNullOrEmpty(Key))
            {
                MainGrid();
                return;
            }
            if (Mode == BsnModes.Edit)
            {
                Edit();
                return;
            }
        }
        #endregion

        #region Edit
        protected virtual void Edit()
        {
            var view = (FM.Contract)App.Forms.Get(BsnForms.Contract);
            view.Load(Contract);
            App.Main.Controls.Add(view.ToControl());
        }
        #endregion

        #region MainGrid
        protected virtual void MainGrid()
        {
            var grid = (GD.Variations)App.Grids.Get(BsnGrids.Variations);
            grid.Contract = Contract;
            App.AddGrid(grid);
        }
        #endregion
        
    }
}
