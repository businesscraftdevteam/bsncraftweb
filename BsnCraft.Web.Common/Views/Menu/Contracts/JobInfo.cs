﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Menu.Contracts.Base;

namespace BsnCraft.Web.Common.Views.Menu.Contracts
{
    public class JobInfo : JobInfoBase
    {
        #region Constructor
        public JobInfo(BsnApplication app) : base(app, BsnMenus.Contracts)
        {
        }
        #endregion        
    }
}
