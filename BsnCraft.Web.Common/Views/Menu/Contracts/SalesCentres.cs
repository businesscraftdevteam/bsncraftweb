﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using GD = BsnCraft.Web.Common.Views.Grid.Contracts;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using BsnCraft.Web.Common.Views.Buttons;
using System;
using System.Globalization;

namespace BsnCraft.Web.Common.Views.Menu.Contracts
{
    public class SalesCentres : BsnMenu
    {
        #region Private Properties                       
        private ComboItem SalesCentre { get; set; }
        public List<DateTime> Dates { get; set; }        
        private BsnGridFilter SalesCentreFilters { get; set; }
        const string ShowFilter = "ShowFilter";
        #endregion

        #region Constructor
        public SalesCentres(BsnApplication app) : base(app, true)
        {
            Init(BsnMenus.SalesCentres, BsnActions.Default);
            Caption = "Display Centres";
            Icon = "flag";

            SalesCentreFilters = new BsnGridFilter(App, "SalesCentres")
            {
                Caption = "Sales Centres",
                KeyField = "Code",
                SelectedField = "IsSelected",
                CaptionField = "Desc",
                MultiSelect = false
            };

            Dates = new List<DateTime>() { DateTime.Now };
        }
        #endregion

        #region Visible
        public override bool Visible
        {
            get
            {
                var visible = base.Visible;
                if (visible)
                {
                    visible = (App.VM.SC.SalesCentres.Count > 0 && App.VM.SC.DisplayInfoQuestions.Count > 0);
                }
                return visible;
            }
        }
        #endregion

        #region PerformUpdate
        protected override bool PerformUpdate(CallbackData data)
        {
            base.PerformUpdate(data);
            bool updated = false;
            if (data.Mode == BsnDataModes.SelectDates)
            {
                updated = true;
                var selectedDates = data.Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                Dates.Clear();
                foreach (var day in selectedDates)
                {
                    try
                    {
                        Dates.Add(DateTime.ParseExact(day, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                    }
                    catch (Exception ex)
                    {
                        App.Utils.LogExc(ex, true, " Date: " + day);
                    }
                }
                App.DataLocation = new Callback();
                return updated;
            }

            if (App.InPanelCallback)
            {
                return updated;
            }

            var question = App.VM.SC.FindDisplayInfo(SalesCentre.Code, data.Key);
            var answer = data.Value;
            if (question == null)
            {
                App.Utils.SetError("Error saving Job Information. Question not found.");
                return updated;
            }
            if (question.HasAnswers)
            {
                if (question.Answers.Any(p => p.Code.Equals(answer)))
                {
                    var lookup = question.Answers.Where(p => p.Code.Equals(answer)).FirstOrDefault();
                    question.Answer = lookup.Desc;
                    question.AnsCode = lookup.Code;
                    question.Modified = true;
                }
                else
                {
                    App.Utils.SetError("Error saving Job Information. Answer not found.");
                }
            }
            else
            {
                if (question.Type == "D")
                {
                    var day = DateTime.ParseExact(answer, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    answer = day.Year.ToString() + day.DayOfYear.ToString("000");
                }
                question.Answer = answer;
                question.Modified = true;
            }
            updated = question.Modified;
            App.VM.SC.UpdateDisplayInfo(SalesCentre.Code);
            return updated;
        }
        #endregion

        #region Load
        protected override void Load()
        {
            if (!App.InCallback && !App.InDataCallback)
            {
                return;
            }

            CheckSalesCentreFilter();
            SalesCentre = App.VM.SC.SalesCentres.Where(p => p.IsSelected).FirstOrDefault();
            if (SalesCentre == null)
            {
                SalesCentre = App.VM.SC.SalesCentres.FirstOrDefault();
                SalesCentre.IsSelected = true;
            }
            
            var grid = (GD.DisplayInfo)App.Grids.Get(BsnGrids.DisplayInfo);
            grid.SalesCentre = SalesCentre;
            grid.Dates = Dates;
            var control = App.AddGrid(grid);

            var container = new HtmlGenericControl("div");
            container.Controls.Add(GridHeader);
            container.Controls.Add(control);

            if (GetOption(ShowFilter) && Mode != BsnModes.ExportGrid)
            {

                var row = new HtmlGenericControl("div");
                row.Attributes["class"] = "row";
                App.Main.Controls.Add(row);

                var leftColumn = new HtmlGenericControl("div");
                leftColumn.Attributes["class"] = "col-md-3 mb-3";
                leftColumn.Attributes["style"] = "overflow-x: auto";
                row.Controls.Add(leftColumn);

                leftColumn.Controls.Add(SalesCentreFilters.ToControl());

                var gridContainer = new HtmlGenericControl("div");
                gridContainer.Attributes["class"] = "col-md-9";
                gridContainer.Controls.Add(container);
                row.Controls.Add(gridContainer);
            }
            else
            {
                App.Main.Controls.Add(container);
            }
        }
        #endregion

        #region Check SalesCentre Filter
        void CheckSalesCentreFilter()
        {
            var data = App.Callback.Data;
            var Keys = new List<string>();
            if (data != null && data.Mode == BsnDataModes.Filter && data.Key == "SalesCentres")
            {
                try
                {
                    var keyString = data.Object.ToString();
                    Keys = (List<string>)JsonConvert.DeserializeObject(keyString, typeof(List<string>));
                }
                catch
                {
                    Keys = new List<string>();
                }
                if (Keys.Count > 0)
                {
                    foreach (var salesCentre in App.VM.SC.SalesCentres)
                    {
                        salesCentre.IsSelected = Keys.Contains(salesCentre.Code);
                    }
                }
            }
            SalesCentreFilters.DataSource = App.VM.SC.SalesCentres;
            SalesCentreFilters.ShowCount = false;
            SalesCentreFilters.Load();
        }
        #endregion

        #region Load Options
        protected override void LoadOptions()
        {
            base.LoadOptions();
            Options.Add(ShowFilter, ("Show Filter", true));
        }
        #endregion

        #region Grid Header
        HtmlGenericControl GridHeader
        {
            get
            {
                var gridHeader = new HtmlGenericControl("div");
                gridHeader.Attributes["class"] = "mb-3";

                var row = new HtmlGenericControl("div");
                gridHeader.Controls.Add(row);
                row.Attributes["class"] = "row";

                var col = new HtmlGenericControl("div");
                row.Controls.Add(col);
                col.Attributes["class"] = "col-sm";
                var description = new HtmlGenericControl("div");
                col.Controls.Add(description);
                description.Attributes["class"] = "h5 text-primary";
                description.InnerHtml = SalesCentre.CodeAndDesc;

                col = new HtmlGenericControl("div");
                row.Controls.Add(col);
                col.Attributes["class"] = "col-sm text-right";

                var callback = App.Utils.GetModeCallback(BsnModes.SelectDates);
                callback.Type = CallbackType.Modal;
                
                var button = new BsnButton(callback, "calendar", "Select Dates", true, ButtonTypes.Primary);
                col.Controls.Add(button.ToControl());

                return gridHeader;
            }
        }
        #endregion
    }
}
