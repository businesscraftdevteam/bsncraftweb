﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Menu.Contracts.Base;

namespace BsnCraft.Web.Common.Views.Menu.Contracts
{
    public class Contracts : DefaultBase
    {
        #region Constructor
        public Contracts(BsnApplication app) : base(app, BsnMenus.Contracts, false)
        {
        }
        #endregion
    }
}
