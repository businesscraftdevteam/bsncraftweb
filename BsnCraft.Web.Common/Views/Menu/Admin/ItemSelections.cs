﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using GD = BsnCraft.Web.Common.Views.Grid.Admin;
using FM = BsnCraft.Web.Common.Views.Form.Items;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Models;
using System.Linq;

namespace BsnCraft.Web.Common.Views.Menu.Admin
{
    public class ItemSelections : BsnMenu
    {
        #region Constructor
        public ItemSelections(BsnApplication app) : base(app)
        {
            Init(BsnMenus.Admin, BsnActions.ItemSelections);
        }
        #endregion

        #region Visible
        public override bool Visible
        {
            get
            {
                var visible = base.Visible;
                if (visible)
                {
                    visible = App.Api.AllContracts;
                }

                return visible;
            }
        }
        #endregion

        #region Load
        protected override void Load()
        {
            Title = "Item Selections";
            if (!App.Api.AllContracts)
            {
                return;
            }
            switch (Mode)
            {
                case BsnModes.Add:
                    Add();
                    return;
                case BsnModes.Edit:
                    if (Edit())
                    {
                        return;
                    }
                    break;
            }
            var grid = (GD.ItemSelections)App.Grids.Get(BsnGrids.ItemSelections);
            App.AddGrid(grid);
        }
        #endregion

        #region Add
        void Add()
        {
            var form = (FM.ItemSelection)App.Forms.Get(BsnForms.ItemSelection);
            var selection = new ItemSelection();
            form.Load(selection);
            App.Main.Controls.Add(form.ToControl());
        }
        #endregion

        #region Edit
        bool Edit()
        {
            var selection = new ItemSelection();
            if (selection == null)
            {
                return false;
            }
            var form = (FM.ItemSelection)App.Forms.Get(BsnForms.ItemSelection);
            form.Load(selection);
            App.Main.Controls.Add(form.ToControl());
            return true;
        }
        #endregion

        #region PerformUpdate
        protected override bool PerformUpdate(CallbackData data)
        {
            base.PerformUpdate(data);
            bool updated = false;
            if (!App.Api.AllContracts)
            {
                return updated;
            }
            var selection = App.VM.IM.ItemSelections.Where(p => p.Code.Equals(data.Key)).FirstOrDefault();
            if (data.Mode == BsnDataModes.Delete && selection != null)
            {
                App.VM.IM.UpdateSelection(selection, true);
                updated = true;
            }
            App.DataLocation = App.Menus.Current.GetLocation();
            return updated;
        }
        #endregion
    }
}
