﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using GD = BsnCraft.Web.Common.Views.Grid.Admin;
using FM = BsnCraft.Web.Common.Views.Form.Contracts;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Models;
using System.Linq;

namespace BsnCraft.Web.Common.Views.Menu.Admin
{
    public class DealSubmit : BsnMenu
    {
        #region Constructor
        public DealSubmit(BsnApplication app) : base(app)
        {
            Init(BsnMenus.Admin, BsnActions.DealSubmit);            
        }
        #endregion

        #region Visible
        public override bool Visible
        {
            get
            {
                var visible = base.Visible;
                if (visible)
                {
                    visible = App.Api.AllContracts;
                }

                return visible;
            }
        }
        #endregion

        #region Load
        protected override void Load()
        {
            Title = "Deal Submission";
            if (!App.Api.AllContracts)
            {
                return;
            }
            switch (Mode)
            {
                case BsnModes.Add:
                    Add();
                    return;
                case BsnModes.Edit:
                    if (Edit())
                    {
                        return;
                    }
                    break;
            }
            var grid = (GD.DealSubmit)App.Grids.Get(BsnGrids.DealSubmit);
            App.AddGrid(grid);
        }
        #endregion

        #region Add
        void Add()
        {
            var form = (FM.DealSubmit)App.Forms.Get(BsnForms.DealSubmit);
            var document = new DealDocument();
            form.Load(document);
            App.Main.Controls.Add(form.ToControl());
        }
        #endregion

        #region Edit
        bool Edit()
        {
            var document = App.Settings.Company.DealDocuments
                .Where(p => p.Key.Equals(Key)).FirstOrDefault();
            if (document == null)
            {
                return false;
            }
            var form = (FM.DealSubmit)App.Forms.Get(BsnForms.DealSubmit);
            form.Load(document);
            App.Main.Controls.Add(form.ToControl());
            return true;
        }
        #endregion

        #region PerformUpdate
        protected override bool PerformUpdate(CallbackData data)
        {
            base.PerformUpdate(data);

            bool updated = false;
            if (!App.Api.AllContracts)
            {
                return updated;
            }

            var document = App.Settings.Company.DealDocuments
                    .Where(p => p.Key.Equals(data.Key)).FirstOrDefault();

            if (data.Mode == BsnDataModes.Delete && document != null)
            {
                App.Settings.Company.DealDocuments.Remove(document);
                App.Settings.SaveComp();
                updated = true;
            }

            App.DataLocation = App.Menus.Current.GetLocation();
            return updated;
        }
        #endregion
    }
}
