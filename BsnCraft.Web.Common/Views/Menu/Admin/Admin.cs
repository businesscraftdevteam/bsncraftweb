﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Views.Menu.Base;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Views.Menu.Admin
{
    public class Admin : BsnMenu
    {        
        #region Private Properties       
        private BsnGridFilter SettingsGroupFilters { get; set; }
        const string ShowSettingsFilter = "ShowSettingsFilter";
        #endregion

        #region Constructor
        public Admin(BsnApplication app) : base(app, true)
        {
            Init(BsnMenus.Admin);
            Icon = "cog";
            MenuActions.Add(new BsnMenuAction(BsnActions.Default) { Caption = "Settings", RequiresId = false });
            MenuActions.Add(new BsnMenuAction(BsnActions.DealSubmit) { Caption = "Deal", RequiresId = false });
            MenuActions.Add(new BsnMenuAction(BsnActions.ItemSelections) { Caption = "Item", RequiresId = false });
            
            SettingsGroupFilters = new BsnGridFilter(App, "Groups")
            {
                Caption = "Groups",
                KeyField = "Code",
                SelectedField = "IsSelected",
                CaptionField = "Desc",
                MultiSelect = true
            };

        }
        #endregion

        #region Visible
        public override bool Visible
        {
            get
            {
                var visible = base.Visible;
                if (visible)
                {
                    visible = App.Api.AllContracts;
                }

                return visible;
            }
        }
        #endregion

        #region Load
        protected override void Load()
        {
            if (!App.Api.AllContracts)
            {
                return;
            }
            
            var grid = App.Grids.Get(BsnGrids.CompSettings);
            CheckSettingsGroupFilter();
            var gridControl = App.AddGrid(grid);

            if (Mode == BsnModes.ExportGrid)
            {
                return;
            }

            if (!GetOption(ShowSettingsFilter))
            {
                App.Main.Controls.Add(gridControl);
                return;
            }
            
            if (GetOption(ShowSettingsFilter))
            {
                var container = new HtmlGenericControl("div");
                container.Attributes["class"] = "row";
                App.Main.Controls.Add(container);

                var leftColumn = new HtmlGenericControl("div");
                leftColumn.Attributes["class"] = "col-md-3 mb-3";
                leftColumn.Attributes["style"] = "overflow-x: auto";
                container.Controls.Add(leftColumn);
                                
                leftColumn.Controls.Add(SettingsGroupFilters.ToControl());

                var gridColumn = new HtmlGenericControl("div");
                gridColumn.Attributes["class"] = "col-md-9";
                gridColumn.Controls.Add(gridControl);
                container.Controls.Add(gridColumn);
            }
            
        }
        #endregion

        #region Check Settings GroupFilter
        void CheckSettingsGroupFilter()
        {
            var data = App.Callback.Data;
            var Keys = new List<string>();
            if (data != null && data.Mode == BsnDataModes.Filter && data.Key == "Groups")
            {
                try
                {
                    var keyString = data.Object.ToString();
                    Keys = (List<string>)JsonConvert.DeserializeObject(keyString, typeof(List<string>));
                }
                catch
                {
                    Keys = new List<string>();
                }
                if (Keys.Count > 0)
                {
                    foreach (var Group in App.VM.SY.SettingGroups)
                    {
                        Group.IsSelected = Keys.Contains(Group.Code);
                    }
                }
            }            
            
            SettingsGroupFilters.DataSource = App.VM.SY.SettingGroups;
            SettingsGroupFilters.ShowCount = false;
            SettingsGroupFilters.Load();            
        }        
        #endregion
               
        #region PerformUpdate
        protected override bool PerformUpdate(CallbackData data)
        {
            base.PerformUpdate(data);
            if (!App.Api.AllContracts)
            {
                return false;
            }
            App.Settings.Comp(data.Key.ToCompSetting(), data.Value);
            return true;
        }
        #endregion

        #region Load Options
        protected override void LoadOptions()
        {
            base.LoadOptions();
            if (Action == BsnActions.Default)
            {
                Options.Add(ShowSettingsFilter, ("Show Filter", true));
            }            
        }
        #endregion
    }
}
