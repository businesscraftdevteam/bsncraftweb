﻿using BsnCraft.Web.Common.Classes;
using GD = BsnCraft.Web.Common.Views.Grid.Data;
using FM = BsnCraft.Web.Common.Views.Form.Data;
using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using System.Linq;
using BsnCraft.Web.Common.Views.Form.Base;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Views.Buttons;
using DevExpress.DashboardWeb;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using DevExpress.XtraReports.Web;
using System.Text;
using System;

namespace BsnCraft.Web.Common.Views.Menu.Data
{
    public class DataViews : BsnMenu
    {
        
        #region Constructor
        public DataViews(BsnApplication app) : base(app, true)
        {
            Init(BsnMenus.DataViews);
            Caption = "Data Views";
            Icon = "chart-pie";
            MenuActions.Add(new BsnMenuAction(BsnActions.Charts) { RequiresId = false });
            MenuActions.Add(new BsnMenuAction(BsnActions.Dashboards) { RequiresId = false });
            MenuActions.Add(new BsnMenuAction(BsnActions.Reports) { RequiresId = false });
        }
        #endregion

        #region PerformUpdate
        protected override bool PerformUpdate(CallbackData data)
        {
            base.PerformUpdate(data);
            bool updated = false;
            var dataView = App.VM.DV.GetDataView(data.Key);
            if (dataView == null)
            {
                return updated;
            }
            switch (data.Mode)
            {
                case BsnDataModes.Bookmark:
                    dataView.Bookmarked = !dataView.Bookmarked;
                    App.VM.DV.Save(dataView);
                    updated = true;
                    break;
                case BsnDataModes.Delete:
                    App.VM.DV.Delete(dataView);
                    updated = true;
                    break;
                default:
                    return false;
            }
            App.DataLocation = App.Menus.Current.GetLocation();
            return updated;
        }
        #endregion

        #region Load
        protected override void Load()
        {
            base.Load();
            
            switch (Action)
            {
                case BsnActions.Charts:
                case BsnActions.Default:
                    Chart();
                    break;
                case BsnActions.Dashboards:
                    Dashboard();
                    break;
                case BsnActions.Reports:
                    Report();
                    break;
            }

            if (Mode == BsnModes.Add)
            {
                var form = (FM.DataView)App.Forms.Get(BsnForms.DataView);
                var dataView = new DataViewItem() { IsNew = true };
                switch (Action)
                {
                    case BsnActions.Default:
                    case BsnActions.Charts:
                        dataView.Type = DataViewTypes.Chart;
                        break;
                    case BsnActions.Dashboards:
                        dataView.Type = DataViewTypes.Dashboard;
                        var dataSource = "<Dashboard><DataSources><ObjectDataSource ComponentName=\"Data\"><Name>Data</Name>";
                        dataSource += "<DataMember>Table</DataMember></ObjectDataSource></DataSources></Dashboard>";
                        dataView.Display =  dataSource;
                        break;
                    case BsnActions.Reports:
                        dataView.Type = DataViewTypes.Report;
                        break;
                }
                form.Load(dataView);
                App.Main.Controls.Add(form.ToControl());
                return;
            }

            if (!string.IsNullOrEmpty(Id))
            {
                return;
            }

            var grid = (GD.DataViews)App.Grids.Get(BsnGrids.DataViews);
            grid.Action = Action;
            App.AddGrid(grid);
        }
        #endregion

        #region Chart
        void Chart()
        {
            GetActions().Where(p => p.UID.Equals((BsnActions.Charts, BsnModes.None))).FirstOrDefault().Selected = true;
            if (string.IsNullOrEmpty(Id) || Mode == BsnModes.Add)
            {
                return;
            }

            var chart = App.VM.DV.GetChart(Id);
            if (chart == null)
            {
                App.Utils.SetError("Error loading chart: " + Id);
                return;
            }

            switch (Mode)
            {
                case BsnModes.None:
                case BsnModes.Open:
                    App.Main.Controls.Add(App.VM.DV.GenerateChart(Id));
                    break;
                case BsnModes.Edit:
                    var form = (FM.DataView)App.Forms.Get(BsnForms.DataView);
                    form.Load(chart);
                    App.Main.Controls.Add(form.ToControl());
                    break;
            }            
        }
        #endregion

        #region Dashboard
        void Dashboard()
        {
            if (string.IsNullOrEmpty(Id) || Mode == BsnModes.Add)
            {
                return;
            }

            var dashboard = App.VM.DV.GetDashboard(Id);
            if (dashboard == null)
            {
                App.Utils.SetError("Error loading dashboard: " + Id);
                return;
            }
            
            switch (Mode)
            {
                case BsnModes.None:
                case BsnModes.Open:
                case BsnModes.Design:
                    var db = XDocument.Load(XmlReader.Create(new StringReader(dashboard.Display)));
                    App.DashboardDesigner.WorkingMode = (Mode == BsnModes.Open) ? WorkingMode.ViewerOnly : WorkingMode.Designer;
                    App.DashboardDesigner.OpenDashboard(db);
                    //App.Main.Controls.Add(DashboardDesigner);
                    App.DashboardDesigner.DataLoading += DashboardDesigner_DataLoading;
                    App.DashboardDesigner.DashboardSaving += DashboardDesigner_DashboardSaving;
                    App.DashboardDesigner.Visible = true;
                    break;
                case BsnModes.Edit:
                    var form = (FM.DataView)App.Forms.Get(BsnForms.DataView);
                    form.Load(dashboard);
                    App.Main.Controls.Add(form.ToControl());
                    break;
            }
        }
        #endregion

        #region Report
        void Report()
        {
            if (string.IsNullOrEmpty(Id) || Mode == BsnModes.Add)
            {
                return;
            }

            var report = App.VM.DV.GetReport(Id);
            if (report == null)
            {
                App.Utils.SetError("Error loading report: " + Id);
                return;
            }

            switch (Mode)
            {
                case BsnModes.Add:
                    return;
                case BsnModes.None:
                case BsnModes.Open:
                    App.ReportViewer.OpenReport(App.VM.DV.GetDevxReport(Id));
                    App.ReportViewer.Visible = true;
                    //App.Main.Controls.Add(ReportViewer);
                    break;
                case BsnModes.Design:                    
                    App.ReportDesigner.OpenReport(App.VM.DV.GetDevxReport(Id));
                    //App.Main.Controls.Add(ReportDesigner);
                    App.ReportDesigner.SaveReportLayout += ReportDesigner_SaveReportLayout;
                    App.ReportDesigner.Visible = true;
                    break;
                case BsnModes.Edit:
                    var form = (FM.DataView)App.Forms.Get(BsnForms.DataView);
                    form.Load(report);
                    App.Main.Controls.Add(form.ToControl());
                    break;
            }
        }
        #endregion

        #region Load Modes
        protected override void LoadModes()
        {
            base.LoadModes();
            if (string.IsNullOrEmpty(Id))
            {
                return;
            }

            var editMode = Mode == BsnModes.Edit;
            var callback = editMode ? App.Utils.GetModeCallback(BsnModes.Open) : App.Utils.GetModeCallback(BsnModes.Edit);
            var editButton = new BsnButton(callback, "pencil") { Active = editMode };
            switch (Action)
            {
                case BsnActions.Default:
                case BsnActions.Charts:
                    editButton.Tooltip = "Edit Chart";
                    break;
                case BsnActions.Dashboards:
                    editButton.Tooltip = "Edit Dashboard";
                    break;
                case BsnActions.Reports:
                    editButton.Tooltip = "Edit Report";
                    break;
            }
            Modes.Add(editButton);

            if (Action == BsnActions.Charts)
            {
                return;
            }

            var designMode = Mode == BsnModes.Design;

            switch (Action)
            {
                case BsnActions.Default:
                case BsnActions.Charts:                    
                    return;
                case BsnActions.Dashboards:                    
                    callback = designMode ? App.Utils.GetModeCallback(BsnModes.Open) : App.Utils.GetModeCallback(BsnModes.Design);
                    var designButton = new BsnButton(callback, "paint-brush") { Active = designMode };
                    Modes.Add(designButton);
                    break;
                case BsnActions.Reports:
                    callback = designMode ? App.Utils.GetModeCallback(BsnModes.Open) : App.Utils.GetModeCallback(BsnModes.Design);
                    var reportButton = new BsnButton(callback, "paint-brush") { Active = designMode };
                    Modes.Add(reportButton);
                    break;
            }            
        }
        #endregion

        #region DashboardViewer
        //ASPxDashboard dashboardDesigner;
        //ASPxDashboard DashboardDesigner
        //{
        //    get
        //    {
        //        if (dashboardDesigner == null)
        //        {
        //            dashboardDesigner = new ASPxDashboard()
        //            {
        //                ID = "DashboardDesigner",
        //                ClientInstanceName = "DashboardDesigner",
        //                AllowExportDashboardItems = true                        
        //            };
        //        }
        //        return dashboardDesigner;
        //    }
        //}
        #endregion

        #region ReportViewer
        //ASPxWebDocumentViewer reportViewer;
        //ASPxWebDocumentViewer ReportViewer
        //{
        //    get
        //    {
        //        if (reportViewer == null)
        //        {
        //            reportViewer = new ASPxWebDocumentViewer();
        //        }
        //        return reportViewer;
        //    }
        //}
        #endregion

        #region ReportDesigner
        //ASPxReportDesigner reportDesigner;
        //ASPxReportDesigner ReportDesigner
        //{
        //    get
        //    {
        //        if (reportDesigner == null)
        //        {
        //            reportDesigner = new ASPxReportDesigner()
        //            {
        //                ID = "ReportDesigner",
        //                ClientInstanceName = "ReportDesigner"
        //            };
        //        }                         
        //        return reportDesigner;
        //    }
        //}
        #endregion
        
        #region SaveReportLayout
        public void ReportDesigner_SaveReportLayout(object sender, SaveReportLayoutEventArgs e)
        {
            var report = App.VM.DV.GetReport(Id);
            if (report == null)
            {
                App.Utils.SetError("Error saving report: " + Id);
                return;
            }

            string layout;

            if (e.ReportLayout.Length >= 3 && e.ReportLayout[0] == 0xEF && e.ReportLayout[1] == 0xBB && e.ReportLayout[2] == 0xBF)
            {
                layout = Encoding.UTF8.GetString(e.ReportLayout, 3, e.ReportLayout.Length - 3);
            }
            else
            {
                layout = Encoding.UTF8.GetString(e.ReportLayout);
            }

            report.Display = layout;
            App.VM.DV.Save(report);
        }
        #endregion

        #region Dashboard Saving
        public void DashboardDesigner_DashboardSaving(object sender, DashboardSavingWebEventArgs e)
        {
            var dashboard = App.VM.DV.GetDashboard(Id);
            if (dashboard == null)
            {
                App.Utils.SetError("Error loading dashboard: " + Id);
                return;
            }
            dashboard.Display = e.DashboardXml.ToString();
            App.VM.DV.Save(dashboard);
            e.Handled = true;
        }
        #endregion

        #region Dashboard DataLoading
        public void DashboardDesigner_DataLoading(object sender, DataLoadingWebEventArgs e)
        {
            var dashboard = App.VM.DV.GetDashboard(Id);
            if (dashboard == null)
            {
                App.Utils.SetError("Error loading dashboard: " + Id);
                return;
            }
            e.Data = App.Odbc.Table(dashboard.DataSource);
        }
        #endregion


    }
}
