﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Menu.Base;
using DevExpress.Web;
using DevExpress.Web.Bootstrap;
using System;
using System.Web.UI.WebControls;

namespace BsnCraft.Web.Common.Views.Menu.Data
{
    public class Documents : BsnMenu
    {
        #region Constructor
        public Documents(BsnApplication app) : base(app, true)
        {
            Init(BsnMenus.Documents);
            Icon = "file-alt";
        }
        #endregion

        #region FileManager
        private BootstrapFileManager fileManager;
        private BootstrapFileManager FileManager
        {
            get
            {
                if (fileManager != null)
                {
                    return fileManager;
                }
                fileManager = new BootstrapFileManager()
                {
                    ID = "fileManager",
                    Height = Unit.Percentage(100)
                };
                try
                {                    
                    var root = App.Settings.Comp(CompanySettingsType.DocumentsDirectory).ToString();
                    var edit = App.Settings.Comp(CompanySettingsType.DocumentsEdit).ToBool();
                    fileManager.Settings.RootFolder = root;
                    fileManager.SettingsEditing.AllowCopy = edit;
                    fileManager.SettingsEditing.AllowCreate = edit;
                    fileManager.SettingsEditing.AllowDelete = edit;
                    fileManager.SettingsEditing.AllowMove = edit;
                    fileManager.SettingsEditing.AllowRename = edit;
                    fileManager.SettingsUpload.Enabled = edit;
                    fileManager.SettingsFileList.View = FileListView.Details;
                    fileManager.SettingsFileList.DetailsViewSettings.AllowColumnDragDrop = true;
                    fileManager.SettingsFileList.DetailsViewSettings.AllowColumnResize = true;
                    fileManager.SettingsFileList.DetailsViewSettings.AllowColumnSort = true;
                    fileManager.SettingsFileList.DetailsViewSettings.ShowHeaderFilterButton = false;
                    fileManager.SettingsAdaptivity.Enabled = true;
                }
                catch(Exception ex)
                {
                    App.Utils.LogExc(ex);
                }

                return fileManager;
            }
        }
        #endregion

        #region Load
        protected override void Load()
        {
            base.Load();
            App.Main.Controls.Add(FileManager);
        }
        #endregion
    }
}
