﻿using BsnCraft.Web.Common.Views.Menu.Base;
using BsnCraft.Web.Common.Views.Grid.Base;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using AR = BsnCraft.Web.Common.Views.Form.Customers;
using BsnCraft.Web.Common.Views.Form.Base;
using CO = BsnCraft.Web.Common.Views.Grid.Contracts;
using GD = BsnCraft.Web.Common.Views.Grid.Customers;
using System.Linq;
using BsnCraft.Web.Common.Views.Buttons;

namespace BsnCraft.Web.Common.Views.Menu.Customers
{
    public class Customers : BsnMenu
    {
        #region Constructor
        public Customers(BsnApplication app) : base(app, true)
        {
            Init(BsnMenus.Customers);
            Icon = "user";
            Callback.Mode = BsnModes.Customers;
            MenuActions.Add(new BsnMenuAction(BsnActions.Contacts));
            MenuActions.Add(new BsnMenuAction(BsnActions.Leads));
            MenuActions.Add(new BsnMenuAction(BsnActions.Contracts));
        }
        #endregion

        #region GetCount
        public override int GetCount()
        {
            return App.VM.AR.Customers.Count;
        }
        #endregion

        #region Set Title
        void SetTitle()
        {
            var title = string.Empty;
            switch (Mode)
            {
                case BsnModes.Add:
                case BsnModes.Edit:
                    title = Mode.ToString();
                    break;
            }
            var contact = App.VM.AR.FindContact(Id, Key);
            if (contact != null && Mode != BsnModes.Add)
            {
                title += " " + contact.FullName;
            }
            else
            {
                var customer = App.VM.AR.FindCustomer(Id);
                if (customer != null)
                {
                    title += " " + customer.FriendlyName;
                };
            }
            Title = title;
        }
        #endregion

        #region Load
        protected override void Load()
        {
            base.Load();
            SetTitle();
            if (ModeChanged)
            {
                App.Settings.GridUID++;
                ModeChanged = false;
            }
            switch (Mode)
            {
                case BsnModes.Add:
                case BsnModes.Edit:
                case BsnModes.Open:
                    Edit();
                    return;
            }
            if (string.IsNullOrEmpty(Id))
            {
                MainGrid();
                return;
            }
            switch (Action)
            {
                case BsnActions.Default:
                case BsnActions.Contacts:
                    Contacts();
                    return;
                case BsnActions.Leads:
                    Leads();
                    return;
                case BsnActions.Contracts:
                    Contracts();
                    return;
            }
        }
        #endregion

        #region Load Modes
        protected override void LoadModes()
        {
            base.LoadModes();
            if (InGridMode)
            {
                return;
            }
            if (Mode == BsnModes.Add)
            {
                return;
            }
            var editMode = Mode == BsnModes.Edit;
            var callback = editMode ? App.Utils.GetModeCallback(BsnModes.Open) : App.Utils.GetModeCallback(BsnModes.Edit);
            Modes.Add(new BsnButton(callback, "pencil") { Active = editMode, Tooltip = "Edit Contact" });
        }
        #endregion

        #region Edit
        void Edit()
        {
            InGridMode = false;
            var form = (AR.Customer)App.Forms.Get(BsnForms.Customer);
            var readOnly = (Mode == BsnModes.Open);
            var contact = App.VM.AR.FindContact(Id, Key);
            if (contact == null || Mode == BsnModes.Add)
            {
                contact = new CustomerContact() { CustomerNumber = Id };
            }
            form.Load(contact, readOnly);
            App.Main.Controls.Add(form.ToControl());            
        }
        #endregion

        #region PerformUpdate
        protected override bool PerformUpdate(CallbackData data)
        {
            base.PerformUpdate(data);
            bool updated = false;
            switch (Action)
            {
                case BsnActions.Default:
                case BsnActions.Contacts:
                    break;
                default:
                    return updated;
            }
            switch (data.Mode)
            {
                case BsnDataModes.Delete:
                    break;
                default:
                    return updated;
            }

            var contact = App.VM.AR.FindContact(data.Key);
            if (contact == null)
            {
                return updated;
            }
            switch (data.Mode)
            {
                case BsnDataModes.Delete:
                    App.VM.AR.DeleteContact(contact);
                    updated = true;
                    break;
                default:
                    return updated;
            }
            App.DataLocation = App.Menus.Current.GetLocation();
            return updated;
        }
        #endregion

        #region Main Grid
        void MainGrid()
        {
            if (Mode == BsnModes.Contacts)
            {
                Contacts();
                return;

            }
            var grid = (GD.Customers)App.Grids.Get(BsnGrids.Customers);
            grid.Reset();
            grid.CustomerNumber = Id;            
            App.AddGrid(grid);
        }
        #endregion

        #region Contacts
        void Contacts()
        {
            GetActions().Where(p => p.UID.Equals((BsnActions.Contacts, BsnModes.None))).FirstOrDefault().Selected = true;
            var grid = (GD.Contacts)App.Grids.Get(BsnGrids.Contacts);
            grid.Reset();
            grid.CustomerNumber = Id;
            App.AddGrid(grid);
        }
        #endregion

        #region Leads
        void Leads()
        {
            var grid = (CO.Leads)App.Grids.Get(BsnGrids.Leads);
            grid.Reset();
            grid.CustomerNumber = Id;
            App.AddGrid(grid);
        }
        #endregion

        #region Contracts
        void Contracts()
        {
            var grid = (CO.Contracts)App.Grids.Get(BsnGrids.Contracts);
            grid.Reset();
            grid.CustomerNumber = Id;
            App.AddGrid(grid);
        }
        #endregion
    }
}
