﻿using BsnCraft.Web.Common.Classes;
using NLog;
using System.Collections.Generic;
using System.Linq;

namespace BsnCraft.Web.Common.Views.Menu.Base
{
    public class MenuManager
    {
        #region Menu - Add here
        private List<BsnMenu> menus;
        private List<BsnMenu> Menus
        {
            get
            {
                if (menus == null)
                {
                    menus = new List<BsnMenu>()
                    {
                        new Home.Desktop(App),
                        new Customers.Customers(App),
                        new Activities.Activities(App),
                        new Leads.Leads(App),
                        new Leads.Contacts(App),
                        new Leads.JobInfo(App),
                        new Leads.Documents(App),
                        new Leads.Quotes(App),
                        new Leads.Events(App),
                        new Leads.Variations(App),
                        new Contracts.Contracts(App),
                        new Contracts.Contacts(App),
                        new Contracts.JobInfo(App),
                        new Contracts.Documents(App),
                        new Contracts.Quotes(App),
                        new Contracts.Events(App),
                        new Contracts.Variations(App),
                        new Contracts.SalesCentres(App),
                        new Data.Documents(App),
                        new Data.DataViews(App),                        
                        new AreaPrices.AreaPriceList(App),
                        new Admin.Admin(App),
                        new Admin.DealSubmit(App),
                        new Admin.ItemSelections(App)
                    };
                }
                return menus;
            }
        }
        #endregion

        #region Public Properties
        public BsnMenu Current { get; private set; }
        #endregion

        #region Private Properties
        //private Logger Log { get; set; }
        private BsnApplication App { get; set; }
        #endregion
        
        #region Constructor
        public MenuManager(BsnApplication app)
        {
            //Log = LogManager.GetLogger(GetType().FullName);
            App = app;
            Current = Get(BsnMenus.Home, BsnActions.Default);
        }
        #endregion

        #region Init
        public void Init()
        {
            menus = null;
        }
        #endregion

        #region Main
        public List<BsnMenu> Main
        {
            get
            {
                return Menus.Where(p => p.IsMainMenu).ToList();
            }
        }
        #endregion

        #region Get        
        public BsnMenu Get(BsnMenus menu, BsnActions action = BsnActions.Default)
        {
            if (Menus.Any(p => p.UID.Item1.Equals(menu) && p.UID.Item2.Equals(action)))
            {
                return Menus.Where(p => p.UID.Item1.Equals(menu) && p.UID.Item2.Equals(action)).FirstOrDefault();
            }
            if (Menus.Any(p => p.UID.Item1.Equals(menu) && p.UID.Item2.Equals(BsnActions.Default)))
            {
                return Menus.Where(p => p.UID.Item1.Equals(menu) && p.UID.Item2.Equals(BsnActions.Default)).FirstOrDefault();
            }
            if (Menus.Any(p => p.UID.Item1.Equals(menu)))
            {
                return Menus.Where(p => p.UID.Item1.Equals(menu)).FirstOrDefault();
            }
            return Menus.Where(p => p.UID.Item1.Equals(BsnMenus.Home)).FirstOrDefault();
        }

        public BsnMenu Get(string strMenu, string strAction = "Default")
        {
            var menu = strMenu.ToMenu();
            var action = strAction.ToAction();
            return Get(menu, action);
        }
        #endregion

        #region Set
        public void Set(BsnMenus menu, BsnActions action = BsnActions.Default)
        {
            Current = Get(menu, action);
        }

        public void Set(string menu, string action)
        {
            Current = Get(menu, action);
        }
        #endregion
    }
}
