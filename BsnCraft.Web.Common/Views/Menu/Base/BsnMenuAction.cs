﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsnCraft.Web.Common.Views.Menu.Base
{
    public class BsnMenuAction
    {
        public (BsnActions, BsnModes) UID { get; private set; }        
        public bool Visible { get; set; } = true;
        public bool AllModes { get; set; } = true;
        public bool RequiresId { get; set; } = true;
        public bool Selected { get; set; }        
        public string Icon { get; set; } = "";

        #region Caption
        string caption = string.Empty;
        public virtual string Caption
        {
            get
            {
                if (string.IsNullOrEmpty(caption) && 
                    string.IsNullOrEmpty(Icon))
                {
                    return UID.Item1.ToString();
                }
                return caption;
            }
            set
            {
                caption = value;
            }
        }
        #endregion

        //TODO: consider removing this and using List<BsnButton> like Toolbars/Modes/Options

        public BsnMenuAction(BsnActions action, BsnModes mode = BsnModes.None)
        {
            UID = (action, mode);
        }
    }
}
