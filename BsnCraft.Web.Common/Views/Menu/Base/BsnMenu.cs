using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Settings.Base;
using BsnCraft.Web.Common.Views.Base;
using BsnCraft.Web.Common.Views.Buttons;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;

namespace BsnCraft.Web.Common.Views.Menu.Base
{
    #region Public Enums
    public enum BsnMenus
    {
        Home,
        Customers,
        Activities,
        Documents,
        Finance,
        Leads,
        DataViews,
        Contracts,
        Jobs,
        SalesCentres,
        Admin,
        AreaPrices
    }

    public enum BsnActions
    {
        Default,
        Details,
        Contacts,
        Leads,
        Contracts,
        Charts,
        Dashboards,
        Reports,
        JobInfo,
        Documents,
        Quotes,
        Events,
        Variations,
        DealSubmit,
        ItemSelections
    }
    public enum BsnDataTypes
    {
        Default,
        ModalForm,
        EstimateParagraphs,
        EstimateHeadings,
        EstimateDetails
    }

    public enum BsnDataModes
    {
        None,
        CloseTab,
        CloseKey,
        UserSetting,
        GroupToggle,
        PanelToggle,
        SavePanels,
        ResetPanels,
        ExpandAll,
        CollapseAll,
        Include,       
        Filter,
        SelectTemplate,
        SelectDates,
        GridSave,
        FormSave,
        FormCancel,
        StartEdit,
        UpdateHeading,
        Register,
        QuickRegister,
        Delete,
        Paragraphs,
        CostCentres,
        ShowGrouping,
        ShowNotes,
        SelectedKeys,
        UpdateDetail,
        SelectReport,
        Bookmark,
		MoveDown,
        MoveUp,
        MoveTop,
        MoveBottom,
        Duplicate,
        GetNextSortOrder,
        SetMargin,
        Copy,
        Paste,
        Edit,
        Add
    }

    public enum BsnModes
    {
        None,
        Add,
        Edit,
        Open,
        View,
        Lookup,
        Design,
        Desktop,
        Contacts,
        Customers,
        Login,
        Logout,
        Documentation,
        MenuOption,
        SaveSettings,
        Bookmark,
        ColumnVisible,
        Update,
        Delete,
        Cancel,
        Select,
        ViewFile,
        ExportGrid,
        Copy,
        RunReport,
        ViewReport,
        SelectReport,
        SelectDates,
        QuickSave,
        Document,
        Refresh,
        Recalc,
        Accept,
        CopySelect,
        CopyPerform,
        CostCentres,
        Filter
    }
    #endregion

    public abstract class BsnMenu
    {
        #region Public Properties
        public BsnApplication App { get; private set; }
        public (BsnMenus, BsnActions) UID { get; private set; }
        public BsnCallback Callback { get; private set; }
        public BsnActions Action { get { return Callback.Action; } }
        public BsnModes Mode { get { return Callback.Mode; } }
        public bool IsMainMenu { get; set; }
        public string Id { get { return Callback.Id; } }
        public string Key { get { return Callback.Key; } }
        public string Value { get { return Callback.Value; } }        
        public BsnMenu ParentMenu { get { return App.Menus.Get(UID.Item1); } }
        public BsnActions ParentAction { get; protected set; } = BsnActions.Default;
        public List<BsnView> Modes { get; private set; }
        public Dictionary<string, (string, bool)> Options { get; private set; }
        public bool IsLoaded { get; set; }
        public bool InGridMode { get; set; }
        public bool ModeChanged { get; set; }
        public bool ShowUpload { get; set; }
        public bool MultiUpload { get; set; }
        public bool ShowReports { get; set; }
        public string ViewReport { get; set; }
        public string ReportParams { get; set; }
        #endregion

        #region Protected Properties
        protected Logger Log { get; private set; }
        protected bool IsFormCallback { get; set; }
        #endregion

        #region Icon
        private string icon = string.Empty;
        public string Icon
        {
            get
            {
                return icon;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    return;
                }                    
                var fa = value;
                if (fa.StartsWith("fas ") || fa.StartsWith("fal "))
                {
                    icon = value;
                }
                icon = "fal fa-" + value + " fa-fw";
            }
        }
        #endregion

        #region Settings
        private MenuSettings settings;
        public MenuSettings Settings
        {
            get
            {
                if (settings != null)
                {
                    return settings;
                }                
                if (App.Settings.User.Menus.ContainsKey(UID))
                {
                    settings = App.Settings.User.Menus[UID];
                }
                else
                {
                    settings = new MenuSettings();
                }
                return settings;
            }
        }
        #endregion

        #region MenuActions
        private List<BsnMenuAction> menuActions;
        protected List<BsnMenuAction> MenuActions
        {
            get
            {
                if (menuActions == null)
                {
                    menuActions = new List<BsnMenuAction>();
                }
                return menuActions;
            }
        }
        #endregion        

        #region Constructor
        public BsnMenu(BsnApplication app, bool isMainMenu = false)
        {
            App = app;
            IsMainMenu = isMainMenu;            
            Log = LogManager.GetLogger(GetType().FullName);
            Options = new Dictionary<string, (string, bool)>();
            Modes = new List<BsnView>();
            Callback = new BsnCallback(App)
            {
                Type = CallbackType.WindowLocation                
            };
        }
        #endregion

        #region Visible
        public virtual bool Visible
        {
            get
            {
                var hide = App.Settings.Comp(CompanySettingsType.HideMainMenu).ToStrArray();
                return !hide.Contains(UID.Item1.ToString());                
            }
        }
        #endregion

        #region Caption
        string caption = string.Empty;
        public string Caption
        {
            get
            {
                if (string.IsNullOrEmpty(caption))
                {
                    return (UID.Item2 != BsnActions.Default) ? UID.Item2.ToString() : UID.Item1.ToString();
                }
                return caption;
            }
            set
            {
                caption = value;
            }
        }
        #endregion

        #region Title
        string title = string.Empty;
        public string Title
        {
            get
            {
                if (string.IsNullOrEmpty(title))
                {
                    return Caption;
                }
                return title;
            }
            set
            {
                title = value;
            }
        }
        #endregion
        
        #region SubTitle
        public virtual string SubTitle
        {
            get
            {
                return ParentMenu.Caption;
            }
        }
        #endregion

        #region PageTitle
        public virtual string PageTitle
        {
            get
            {
                var pageTitle = UID.Item1.ToString();
                if (Action != BsnActions.Default)
                {
                    pageTitle += " - " + Action.ToString();
                }
                return pageTitle;
            }
        }
        #endregion

        #region GetCount
        public virtual int GetCount()
        {
            return 0;
        }
        #endregion

        #region Init
        public void Init(BsnMenus menu, BsnActions action = BsnActions.Default)
        {
            UID = (menu, action);
            Callback.Menu = menu;
            Callback.Action = action;
        }
        #endregion
        
        #region Load
        public void Load(BsnActions action, string id, BsnModes mode, string key, string value)
        {
            ModeChanged = (Mode != mode);            
            Callback.Action = action;
            Callback.Id = id;
            Callback.Mode = mode;
            Callback.Key = key;
            Callback.Value = value;
        }
        #endregion

        #region OnLoad
        public void OnLoad()
        {
            try
            {
                CheckForms();
                if (SkipOnLoad)
                {
                    return;
                }
                DataBind();
                if (CheckUpdate())
                {
                    return;
                }
                if (CheckModal())
                {
                    return;
                }                
                LoadOptions();
                Load();
                if (!App.InCallback)
                {
                    // Note: Do not early return before Load() in DataOnly callbacks.
                    // DataOnly still needs event handlers for in-grid BsnForms
                    return;
                }
                LoadModes();
                App.Nav.Load();
                AfterNav();
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
            }
        }
        #endregion

        #region Skip OnLoad
        public bool SkipOnLoad
        {
            get
            {
                //Need to call OnLoad for devx controls which are not manually handled
                var skipOnLoad = false;
                if (App.InPanelCallback)
                {
                    return skipOnLoad;
                }
                skipOnLoad |= !App.Api.Connected;
                skipOnLoad |= !App.Page.IsCallback;
                skipOnLoad |= App.IsPanelCallback;
                skipOnLoad |= App.UploadControl.IsCallback;
                skipOnLoad |= IsFormCallback;

                return skipOnLoad;
            }
        }
        #endregion

        #region SavingSettings
        public bool SavingSettings
        {
            get
            {
                return (App.IsDataCallback && App.Settings.Update);
            }
        }
        #endregion

        #region DataBind
        public virtual void DataBind()
        {
            title = string.Empty;
            ReportParams = string.Empty;
        }
        #endregion

        #region CheckForms
        protected virtual void CheckForms()
        {
            if (!App.Page.IsCallback)
            {
                return;
            }
            IsFormCallback = App.Forms.IsFormCallback;
            if (!App.IsPanelCallback && !IsFormCallback)
            {
                return;
            }
            if (App.InPanelCallback)
            {
                App.Forms.Remove();
            }
            else
            {
                if (!SavingSettings)
                {
                    App.Forms.Create();
                }
            }
        }
        #endregion

        #region CheckUpdate
        protected virtual bool CheckUpdate()
        {
            if (App.DataCallback.Data.Mode == BsnDataModes.None)
            {
                return false;
            }
            return PerformUpdate(App.DataCallback.Data);
        }
        #endregion

        #region PerformUpdate
        protected virtual bool PerformUpdate(CallbackData data)
        {
            return false;
        }
        #endregion

        #region CheckModal
        protected virtual bool CheckModal()
        {
            var loadModal = false;
            loadModal |= App.DataCallback.Type == CallbackType.Modal;
            loadModal |= App.DataCallback.Data.Type == BsnDataTypes.ModalForm;
            if (!loadModal)
            {
                return false;
            }
            return LoadModal();
        }
        #endregion

        #region LoadModal
        protected virtual bool LoadModal()
        {
            return false;
        }
        #endregion

        #region LoadOptions
        protected virtual void LoadOptions()
        {
            Options.Clear();
        }
        #endregion

        #region Load
        protected virtual void Load()
        {
            if (!App.InCallback)
            {
                return;
            }
            //Initialise menu
            settings = null;
            ShowUpload = false;
            MultiUpload = false;
            InGridMode = true;
            foreach (var action in GetActions().Where(p => p.Selected))
            {
                action.Selected = false;
            }
            var toolbar = (HtmlGenericControl)App.Panel.FindControl("toolbar");
            var div = (HtmlGenericControl)App.Panel.FindControl("tooldiv");
            toolbar.Visible = false;
            div.Visible = false;
        }
        #endregion

        #region LoadModes
        protected virtual void LoadModes()
        {
            Modes.Clear();
            if (Mode == BsnModes.ExportGrid)
            {
                Modes.Add(new BsnButton(App.Utils.GetModeCallback(BsnModes.None), "times") { Active = true, Tooltip = "Close Report" });
            }
        }
        #endregion

        #region AfterNav
        protected virtual void AfterNav()
        {
            
        }
        #endregion
        
        #region Get Option
        public virtual bool GetOption(string key)
        {
            if (!Options.ContainsKey(key))
            {
                return false;
            }

            if (!Settings.Options.ContainsKey(key))
            {
                return Options[key].Item2;
            }

            return SettingsOpt(key);
        }
        #endregion

        #region SettingsOpt
        protected bool SettingsOpt(string key)
        {
            if (!Settings.Options.ContainsKey(key))
            {
                return false;
            }
            return Settings.Options[key];
        }
        #endregion

        #region GetActions
        public List<BsnMenuAction> GetActions()
        {
            if (ParentMenu != null && ParentMenu != this && MenuActions.Count == 0)
            {
                return ParentMenu.MenuActions;
            }
            return MenuActions;
        }
        #endregion

        #region GetLocation
        public Callback GetLocation(bool window = true)
        {
            return new Callback()
            {
                Type = window ? CallbackType.WindowLocation : CallbackType.Normal,
                App = App.Settings.Application.Instance,
                Menu = UID.Item1,
                Action = Action,
                Mode = Mode,
                Id = Id,
                Key = Key,
                Value = Value
            };
        }
        #endregion

    }
}
