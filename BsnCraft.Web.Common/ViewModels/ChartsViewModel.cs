﻿using BsnCraft.Web.Common.Classes;
using DevExpress.Web.Bootstrap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BsnCraft.Web.Common.ViewModels
{
    public class ChartsViewModel : DataViewViewModel
    {

        #region Charts
        public List<BsnDataView> Charts
        {
            get
            {
                var dataViews = DataViews.Where(p => p.Type == "Chart");
                if (dataViews == null)
                {
                    return new List<BsnDataView>();
                }
                return dataViews.ToList();
            }
        }
        #endregion

        #region GetChart
        public BsnDataView GetChart(string name)
        {
            return Charts.Where(p => p.Name == name).FirstOrDefault();
        }
        #endregion

        #region GenerateChart
        public BootstrapChartBase GenerateChart(string chartName)
        {
            BootstrapChartBase cht = null;
            var chart = GetChart(chartName);
            if (chart == null)
            {
                Utils.SetError("Error loading chart: " + chartName);
                return cht;
            }
            
            var type = (ChartTypes)Enum.Parse(typeof(ChartTypes), chart.Display);
            var dataSource = (DataSources)Enum.Parse(typeof(DataSources), chart.DataSource);

            switch (type)
            {
                case ChartTypes.Pie:
                case ChartTypes.Doughnut:
                    cht = new BootstrapPieChart();
                    if (type == ChartTypes.Doughnut)
                    {
                        ((BootstrapPieChart)cht).Type = PieChartType.Doughnut;
                    }
                    break;
                case ChartTypes.Bar:
                    cht = new BootstrapChart();
                    break;
                case ChartTypes.Area:
                    cht = new BootstrapChart();
                    break;
                case ChartTypes.Line:
                    cht = new BootstrapChart();
                    break;
            }            
            //cht.TitleText = chart.Description;
            cht.DataSourceUrl = "~/Charts/GetData.aspx?data=" + chart.DataSource + "&callback=?";
            var series = GetSeries(type, dataSource);
            cht.SeriesCollection.Add(series);
            return cht;
        }
        #endregion

        #region GetSeries
        public BootstrapChartCommonSeriesBase GetSeries(ChartTypes type, DataSources dataSource)
        {
            BootstrapChartCommonSeriesBase series = null;
            switch (type)
            {
                case ChartTypes.Pie:
                case ChartTypes.Doughnut:
                    series = new BootstrapPieChartSeries() { ArgumentField = "Argument", ValueField = "Value" };
                    break;
                case ChartTypes.Bar:
                    series = new BootstrapChartBarSeries() { ArgumentField = "Argument", ValueField = "Value" };
                    break;
                case ChartTypes.Area:
                    series = new BootstrapChartAreaSeries() { ArgumentField = "Argument", ValueField = "Value" };
                    break;
                case ChartTypes.Line:
                    series = new BootstrapChartLineSeries() { ArgumentField = "Argument", ValueField = "Value" };
                    break;
            }
            series.Name = GetSeriesName(dataSource);
            return series;
        }

        public string GetSeriesName(DataSources dataSource)
        {
            switch (dataSource)
            {
                case DataSources.LeadsBySalesCentre:
                case DataSources.LeadsByOperatingCentre:
                case DataSources.LeadsBySalesConsultant:
                    return "Leads";
            }
            return string.Empty;
        }
        #endregion
        
    }
}
