﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BsnCraft.Web.Common.Classes.Grid;

namespace BsnCraft.Web.Common.ViewModels
{
    public class PurchaseOrdersViewModel : ViewModelBase
    {
        #region PurchaseOrders
        public List<dynamic> PurchaseOrders(string JobNumber)
        {
            var purchaseOrders = (List<dynamic>)VarUtils.Get(WebVars.PurchaseOrders);
            if (purchaseOrders == null)
            {
                purchaseOrders = new List<dynamic>();

                var fields = "PO_PONUM,PO_JOBNUM,PO_VENNO,PO_VENNAME,PO_ADD1,PO_ADD2,PO_CITY,PO_STATE,PO_PCODE,PO_CSTCTR,PO_COMPLETE,PO_DATE,PO_ISSU_VAL";

                var query = "SELECT " + fields + " FROM POHEADA";
                query += " WHERE PO_JOBNUM = '" + JobNumber + "'";
                
                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    foreach (DataRow row in Table.Rows)
                    {
                        var date = (row["PO_DATE"] is DateTime) ? ((DateTime)row["PO_DATE"]) : DateTime.MinValue;
                        string address = string.Concat(row["PO_ADD1"].ToString(), row["PO_ADD2"].ToString());
                        address = string.Concat(address, row["PO_ADD1"].ToString());
                        address = string.Concat(address, row["PO_STATE"].ToString());
                        address = string.Concat(address, row["PO_PCODE"].ToString());
                        string issued = (row["PO_ISSU_VAL"] is Decimal) ?
                            ((((Decimal)row["PO_ISSU_VAL"]) != 0) ?
                            ((Decimal)row["PO_ISSU_VAL"]).ToString("C") :
                            string.Empty) :
                            string.Empty;
                        purchaseOrders.Add(new PurchaseOrder()
                        {
                            PONumber = row["PO_PONUM"].ToString(),
                            Date = date,
                            Status = row["PO_COMPLETE"].ToString(),
                            JobNumber = row["PO_JOBNUM"].ToString(),
                            Vendor = row["PO_VENNO"].ToString(),
                            VendorName = row["PO_VENNAME"].ToString(),
                            Address = address,
                            CostCentre = row["PO_CSTCTR"].ToString(),
                            Issued = issued
                        });
                    }
                }
                VarUtils.Set(WebVars.PurchaseOrders, purchaseOrders);
            }
            return purchaseOrders;
        }
        #endregion

        #region GetPurchaseOrder
        public PurchaseOrder GetPurchaseOrder(string JobNumber, string PONumber)
        {
            return PurchaseOrders(JobNumber).Where(p => p.PONumber == PONumber).FirstOrDefault();
        }
        #endregion

        #region GridView
        public BsnGridViewModel GridView(string JobNumber)
        {
            var grid = new BsnGridViewModel("PurchaseOrders", "PONumber", "Purchase Orders")
            {
                DataSource = PurchaseOrders(JobNumber)                
            };

            int sort = 0;
            grid.Columns.Add(new BsnGridColumn() { Name = "Vendor", FieldName = "Vendor", Caption = "Vendor", Sort = sort++ });
            grid.Columns.Add(new BsnGridColumn() { Name = "Name", FieldName = "VendorName", Caption = "Name", Sort = sort++ });
            grid.Columns.Add(new BsnGridColumn() { Name = "Address", FieldName = "Address", Caption = "Address", Sort = sort++ });
            grid.Columns.Add(new BsnGridColumn() { Name = "Date", FieldName = "Date", Caption = "Date", Sort = sort++ });
            grid.Columns.Add(new BsnGridColumn() { Name = "CostCentre", FieldName = "CostCentre", Caption = "C/C", Sort = sort++ });
            grid.Columns.Add(new BsnGridColumn() { Name = "Status", FieldName = "Status", Caption = "Sts", Sort = sort++ });
            grid.Columns.Add(new BsnGridColumn() { Name = "Issued", FieldName = "Issued", Caption = "Amount", Sort = sort++ });

            return grid;
        }
        #endregion
        
        #region KeyButtons
        List<BsnGridButton> KeyButtons
        {
            get
            {
                var buttonType = Utils.Settings.Comp.TouchMode ? ButtonTypes.Primary : ButtonTypes.Link;
                var edit = new BsnGridButton(new BsnCallback()
                {
                    Id = BsnControls.Edit_Contract,
                    Mode = CallbackMode.Edit
                }
                , "fal fa-external-link-square", "cellvalue", true, buttonType);
                return new List<BsnGridButton>() { edit };
            }
        }
        #endregion

    }
}
