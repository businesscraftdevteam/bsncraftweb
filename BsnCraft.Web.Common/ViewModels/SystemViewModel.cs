﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BsnCraft.Web.Common.ViewModels
{
    public class SystemViewModel : ViewModelBase
    {
        
        #region LoginUsers
        public List<LoginUser> LoginUsers
        {
            get
            {
                var table = (List<LoginUser>)VarUtils.Get(WebVars.LoginUsers);
                if (table == null)
                {
                    table = Utils.GetRecords<LoginUser>("QSHSYSA", "");
                    if (table.Count > 0)
                    {
                        VarUtils.Set(WebVars.LoginUsers, table);
                    }
                }
                return table;
            }
        }
        #endregion

        #region Get User
        public LoginUser GetUser(string username)
        {
            return LoginUsers.Where(p => p.Username == username).FirstOrDefault();
        }
        #endregion

        #region Lookup Suburbs
        public List<SearchData> LookupSuburbs(string query)
        {
            string length = query.Length.ToString();
            string search = "CONSTANT=PC_SUBURB\t" + query.ToUpper() + "\t" + length + ";KEYREF=1;";

            var postcodes = Utils.GetRecords<PostCode>("POSTCDA", search, 10);
            
            var results = new List<SearchData>();
            foreach (var item in postcodes)
            {
                var value = item.Suburb + " | " + item.Key.Substring(0,4);
                results.Add(new SearchData() { Value = value, Data = JsonConvert.SerializeObject(item) });
            }
            return results;
        }
        #endregion

    }
}