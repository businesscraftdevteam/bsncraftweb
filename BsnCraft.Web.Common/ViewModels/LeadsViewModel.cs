﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BsnCraft.Web.Common.ViewModels
{
    public class LeadsViewModel : ViewModelBase
    {
        const string BookmarksKey = "Leads.Bookmarks";
        
        #region Leads
        public List<Contract> Leads
        {
            get
            {
                var leads = (List<Contract>)VarUtils.Get(WebVars.Leads);
                if (leads == null)
                {
                    leads = new List<Contract>();

                    var fields = "CO_KEY,CO_CUSNUM,CO_NAME,CO_LOTNO,CO_STREETNO,CO_ADDR,CO_CITY,CO_STATE,CO_ZIP,CO_STATUS,CO_SALETYPE";
                    fields += ",CO_DISTRICT,CO_COUNCIL,CO_SALEPROM,CO_SALECENT,CO_COMM1,CO_COMM2,CO_COMM3,CO_HOUSE,CO_ELEVATION";
                    fields += ",CO_SUBJSALE,CO_SAVEPLAN,CO_JOBNUM,CO_SALEGROUP,CO_OPCENTRE,CO_DP,CO_VOLUME,CO_FOLIO"; //,JSM_AREA,JSM_EFFECTIVE";
                    fields += ",CO_SALESCONS,CO_CSR,CO_BLDGSUPV,CO_SERVSUPV";

                    //string join = " JOIN JCSPECM ON JSM_JOBNUM = CO_JOBNUM";
                    var query = "SELECT " + fields + " FROM CONHDRA"; // + join;
                    query += " WHERE { fn LENGTH(CO_KEY)} > 0";
                    query += Utils.GetFilterString("CO_STATUS", Utils.Settings.Comp.ActiveLeadStatusList, 1);

                    if (Utils.Settings.Comp.RestrictLeads && !BsnUtils.AllContracts)
                    {
                        query += " AND CO_SALESCONS = '" + BsnUtils.BsnInitials + "'";
                    }

                    if (!string.IsNullOrEmpty(Utils.Settings.Comp.LeadsQuery))
                        query += " " + Utils.Settings.Comp.LeadsQuery;

                    DataTable Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            //Do not want to cache effective date, read on load estimate
                            //var effective = (row["JSM_EFFECTIVE"] is DateTime) ? ((DateTime)row["JSM_EFFECTIVE"]) : DateTime.MinValue;
                            leads.Add(new Contract()
                            {
                                ContractNumber = row["CO_KEY"].ToString(),
                                CustomerNumber = row["CO_CUSNUM"].ToString(),
                                Name = row["CO_NAME"].ToString(),
                                LotNumber = row["CO_LOTNO"].ToString(),
                                StreetNumber = row["CO_STREETNO"].ToString(),
                                Address = row["CO_ADDR"].ToString(),
                                City = row["CO_CITY"].ToString(),
                                State = row["CO_STATE"].ToString(),
                                PostCode = row["CO_ZIP"].ToString(),
                                Status = row["CO_STATUS"].ToString(),
                                SaleType = row["CO_SALETYPE"].ToString(),
                                SalesConsultant = row["CO_SALESCONS"].ToString(),
                                CustomerService = row["CO_CSR"].ToString(),
                                BuildingSupervisor = row["CO_BLDGSUPV"].ToString(),
                                District = row["CO_DISTRICT"].ToString(),
                                Council = row["CO_COUNCIL"].ToString(),
                                SalesPromotion = row["CO_SALEPROM"].ToString(),
                                SalesCentre = row["CO_SALECENT"].ToString(),
                                House = row["CO_HOUSE"].ToString(),
                                Facade = row["CO_ELEVATION"].ToString(),
                                Comment1 = row["CO_COMM1"].ToString(),
                                Comment2 = row["CO_COMM2"].ToString(),
                                Comment3 = row["CO_COMM3"].ToString(),
                                SubjectSale = row["CO_SUBJSALE"].ToString(),
                                SavingsPlan = row["CO_SAVEPLAN"].ToString(),                                
                                SaleGroup = row["CO_SALEGROUP"].ToString(),
                                OpCentre = row["CO_OPCENTRE"].ToString(),
                                DpNumber = row["CO_DP"].ToString(),
                                Volume = row["CO_VOLUME"].ToString(),
                                Folio = row["CO_FOLIO"].ToString(),
                                JobNumber = row["CO_JOBNUM"].ToString(),
                                //PricingArea = row["JSM_AREA"].ToString(),
                                //Effective = effective,
                                IsBookmarked = Bookmarks.Contains(row["CO_KEY"].ToString()),
                                IsLead = true
                            });
                        }
                    }
                    VarUtils.Set(WebVars.Leads, leads);
                }
                return leads;
            }
        }
        #endregion

        #region GetLead
        public Contract GetLead(string contractNumber)
        {
            return Leads.Where(p => p.ContractNumber == contractNumber).FirstOrDefault();
        }
        #endregion

        #region CustomerLeads
        public List<Contract> CustomerLeads(string customerNumber)
        {
            return Leads.Where(p => p.CustomerNumber == customerNumber).ToList();
        }
        #endregion

        #region LookupLeads
        public List<SearchData> LookupLeads(string query)
        {
            var match = Leads.Where(p => p.ContractNumber.Contains(query) ||
                p.CustomerNumber.Contains(query) ||
                p.FriendlyName.ToLower().Contains(query) ||
                p.FullAddress.ToLower().Contains(query)).Take(10);

            var results = new List<SearchData>();
            foreach (var item in match)
            {
                var value = item.ContractNumber + " | " + item.FriendlyName + " | " + item.FullAddress;
                results.Add(new SearchData() { Value = value, Data = JsonConvert.SerializeObject(item) });
            }
            return results;
        }
        #endregion

        #region Bookmarks
        public List<string> Bookmarks
        {
            get
            {
                var bookmarks = (List<string>)VarUtils.Get(WebVars.BookmarkedLeads);
                if (bookmarks == null)
                {                    
                    string keyvals = string.Empty;
                    try
                    {
                        keyvals = BsnUtils.GetSettings(SettingsLevel.User, BookmarksKey);
                        bookmarks = (List<string>)JsonConvert.DeserializeObject(keyvals, typeof(List<string>));
                        if (bookmarks == null)
                        {
                            bookmarks = new List<string>();
                        }                        
                    }
                    catch (Exception e)
                    {
                        Utils.LogExc(e);
                        bookmarks = new List<string>();
                    }
                    VarUtils.Set(WebVars.BookmarkedLeads, bookmarks);
                }
                return bookmarks;
            }
        }
        #endregion

        #region Bookmark
        public void Bookmark(string contractNumber)
        {
            var Contract = GetLead(contractNumber);
            if (Bookmarks.Contains(contractNumber))
            {
                Bookmarks.Remove(contractNumber);
                Contract.IsBookmarked = false;
            }
            else
            {
                Bookmarks.Add(contractNumber);
                Contract.IsBookmarked = true;
            }
            
            string keyvals = JsonConvert.SerializeObject(Bookmarks);
            BsnUtils.SaveSettings(SettingsLevel.User, BookmarksKey, keyvals);

            string message = (Contract.IsBookmarked) ? "Added" : "Removed";
            message += " lead bookmark: " + contractNumber;
            Utils.SetSuccess(message);            
        }
        #endregion
        
        #region LeadsBySalesCentre
        private List<ChartData> leadsBySalesCentre;
        public List<ChartData> LeadsBySalesCentre
        {
            get
            {
                if (leadsBySalesCentre == null)
                {
                    leadsBySalesCentre = new List<ChartData>();
                    foreach (var contract in Leads)
                    {
                        string salesCentre = string.Empty;
                        if (string.IsNullOrEmpty(contract.SalesCentre))
                        {
                            salesCentre = "[ Blank ]";
                        }
                        else
                        {
                            try
                            {
                                var salesCentres = new ContractsViewModel().SalesCentres;
                                salesCentre = salesCentres.Find(p => p.Code == contract.SalesCentre).Desc;
                            }
                            catch
                            {
                                salesCentre = "Unknown: " + contract.SalesCentre;
                            }
                        }
                        leadsBySalesCentre.Increment(salesCentre);
                    }
                }
                return leadsBySalesCentre;
            }
        }
        #endregion

        #region LeadsByOperatingCentre
        private List<ChartData> leadsByOperatingCentre;
        public List<ChartData> LeadsByOperatingCentre
        {
            get
            {
                if (leadsByOperatingCentre == null)
                {
                    leadsByOperatingCentre = new List<ChartData>();
                    foreach (var contract in Leads)
                    {
                        string operatingCentre = string.Empty;
                        if (string.IsNullOrEmpty(contract.OpCentre))
                        {
                            operatingCentre = "[ Blank ]";
                        }
                        else
                        {
                            try
                            {
                                var opCentres = new ContractsViewModel().OpCentres;
                                operatingCentre = opCentres.Find(p => p.Code == contract.OpCentre).Desc;
                            }
                            catch
                            {
                                operatingCentre = "Unknown: " + contract.OpCentre;
                            }
                        }
                        leadsByOperatingCentre.Increment(operatingCentre);
                    }
                }
                return leadsByOperatingCentre;
            }
        }
        #endregion

        #region LeadsBySalesConsultant
        private List<ChartData> leadsBySalesConsultant;
        public List<ChartData> LeadsBySalesConsultant
        {
            get
            {
                if (leadsBySalesConsultant == null)
                {
                    leadsBySalesConsultant = new List<ChartData>();
                    foreach (var contract in Leads)
                    {
                        string salesConsultant = string.Empty;
                        if (string.IsNullOrEmpty(contract.SalesConsultant))
                        {
                            salesConsultant = "[ Blank ]";
                        }
                        else
                        {
                            try
                            {
                                var salesConsultants = new ContractsViewModel().SalesConsultants;
                                salesConsultant = salesConsultants.Find(p => p.Code == contract.SalesConsultant).Desc;
                            }
                            catch
                            {
                                salesConsultant = "Unknown: " + contract.SalesConsultant;
                            }
                        }
                        leadsBySalesConsultant.Increment(salesConsultant);
                    }
                }
                return leadsBySalesConsultant;
            }
        }
        #endregion
    }
}