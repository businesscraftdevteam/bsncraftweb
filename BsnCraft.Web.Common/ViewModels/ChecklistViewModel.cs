﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BsnCraft.Web.Common.ViewModels
{
    public class ChecklistViewModel : ViewModelBase
    {
        #region FindChecklistInfo
        public ChecklistInfo FindChecklistInfo(string key)
        {
            var ChecklistInfo = (List<ChecklistInfo>)VarUtils.Get(WebVars.ChecklistInfo);

            if (ChecklistInfo.Count < 1)
                return null;

            int index = ChecklistInfo.FindIndex(p => p.Key == key);

            if (index < 0)
                return null;

            return ChecklistInfo[index];
        }
        #endregion

        #region GetChecklistInfo
        public List<ChecklistInfo> GetChecklistInfo(string contractNumber, string checklistSequence)
        {
            var questions = ChecklistInfoQuestions;
            if (questions == null)
            {
                return questions;
            }

            var ChecklistInfo = new List<ChecklistInfo>();

            int key = 0;
            string julian = string.Empty;
            string updateKey = string.Empty;
            string updateSeq = string.Empty;
            string answer = string.Empty;
            string ansCode = string.Empty;            

            var answers = ChecklistInfoAnswers(contractNumber + checklistSequence);

            foreach (ChecklistInfo question in questions)
            {
                updateKey = string.Empty;
                updateSeq = string.Empty;
                answer = string.Empty;
                ansCode = string.Empty;                

                if (answers.ContainsKey(question.Code))
                {
                    var data = answers[question.Code];
                    updateKey = Utils.GetArray(data, 0);
                    updateSeq = Utils.GetArray(data, 1);
                    answer = Utils.GetArray(data, 2);
                    ansCode = Utils.GetArray(data, 3);
                }

                //Format Numeric Values
                if (question.Type == "N" || question.Type == "P")
                {
                    if (!string.IsNullOrEmpty(answer) && !string.IsNullOrEmpty(question.Dec))
                    {
                        answer = answer.Replace(",", "").Replace(".", "");
                        var dp = int.Parse(question.Dec);
                        var numeric = int.Parse(answer);
                        if (dp == 0)
                        {
                            answer = numeric.ToString();
                        }
                        else
                        {
                            var dpstring = "1000000000";
                            var denominator = int.Parse(dpstring.Substring(0, dp + 1));
                            var dec = Convert.ToDouble(numeric) / denominator;
                            answer = dec.ToString("F" + dp);
                        }
                    }
                }

                ChecklistInfo.Add(new ChecklistInfo()
                {
                    Key = key++.ToString(),
                    ContractNumber = contractNumber,
                    Sequence = checklistSequence,
                    SortOrder = question.SortOrder,
                    Code = question.Code,
                    Prompt = question.Prompt,
                    Type = question.Type,
                    Min = question.Min,
                    Max = question.Max,
                    Dec = question.Dec,
                    Default = question.Default,
                    Answers = question.Answers,
                    Answer = answer,
                    AnsCode = ansCode,
                    UpdateKey = updateKey
                });
            }

            VarUtils.Set(WebVars.ChecklistInfo, ChecklistInfo);
            return ChecklistInfo;
        }
        #endregion

        #region GetChecklistData
        public List<ChecklistInfo> GetChecklistData(string contractNumber, string checklistSequence)
        {
            int key = 0;
            string fields = "XIL_SCRKEY,XIL_CODE,XIL_PROMPT,XIL_REFCODE,XIL_ANSWER,XIL_ANSCODE,XIL_ANSTYPE";
            string query = string.Empty;
            string julian = string.Empty;

            var ChecklistInfo = new List<ChecklistInfo>();
            var where = "XIL_MODULE = 'CSC' AND XIL_REFCODE = '" + contractNumber + checklistSequence + "'";
            query = "SELECT " + fields + " FROM INFLINA WHERE " + where + " ORDER BY XIL_SEQ";
            var Table = RecordUtils.Table(query);
            if (Table != null)
            {
                foreach (DataRow row in Table.Rows)
                {
                    ChecklistInfo.Add(new ChecklistInfo()
                    {
                        Key = key++.ToString(),
                        ContractNumber = contractNumber,
                        Sequence = checklistSequence,
                        Code = row["XIL_CODE"].ToString(),
                        Prompt = row["XIL_PROMPT"].ToString(),
                        AnsCode = row["XIL_ANSCODE"].ToString(),
                        Answer = row["XIL_ANSWER"].ToString(),
                        Type = row["XIL_ANSTYPE"].ToString()
                    });
                }
            }

            VarUtils.Set(WebVars.ChecklistData, ChecklistInfo);
            return ChecklistInfo;
        }
        #endregion

        #region ChecklistInfo Combo
        private List<ComboItem> ChecklistInfoCombo(string category)
        {
            var answers = (Dictionary<String, List<ComboItem>>)VarUtils.Get(WebVars.ChecklistInfoAnswers);
            if (answers == null)
            {
                answers = new Dictionary<string, List<ComboItem>>();
                var fields = "XIB_CAT,XIB_CODE,XIB_ANSWER";
                var where = "WHERE XIB_MODULE = 'COB'";
                var query = "SELECT " + fields + " FROM INFTBLB " + where + " ORDER BY XIB_CAT ASC";
                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    var combo = new List<ComboItem>();
                    var cat = string.Empty;
                    int i = 0;
                    foreach (DataRow row in Table.Rows)
                    {
                        i++;
                        if (row["XIB_CAT"].ToString() == "**")
                            continue;

                        if (row["XIB_CAT"].ToString() != cat && !string.IsNullOrEmpty(cat))
                        {
                            answers.Add(cat, combo);
                            combo = new List<ComboItem>();
                        }
                        cat = row["XIB_CAT"].ToString();
                        combo.Add(new ComboItem(row["XIB_CODE"].ToString(), row["XIB_ANSWER"].ToString()));
                        if (i == Table.Rows.Count)
                        {
                            answers.Add(cat, combo);
                        }
                    }
                }
                VarUtils.Set(WebVars.ChecklistInfoAnswers, answers);
            }
            if (answers.ContainsKey(category))
            {
                return answers[category];
            }
            return new List<ComboItem>();
        }
        #endregion        

        #region ChecklistInfo Questions
        public List<ChecklistInfo> ChecklistInfoQuestions
        {
            get
            {
                var questions = (List<ChecklistInfo>)VarUtils.Get(WebVars.ChecklistInfoQuestions);
                if (questions == null)
                {
                    questions = new List<ChecklistInfo>();
                    string fields = "XIA_MODULE,XIA_CODE,XIA_PROMPT,XIA_ANSTYPE,XIA_MIN,XIA_MAX,XIA_DEC,XIA_DEF,XIA_LNK_PRF,XIA_SORT_ORDER";
                    string where = "WHERE XIA_MODULE = 'COB'";
                    string query = "SELECT " + fields + " FROM INFTBLA " + where;
                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            var cat = row["XIA_LNK_PRF"].ToString();
                            var answers = (!string.IsNullOrEmpty(cat)) ?
                                ChecklistInfoCombo(cat) : new List<ComboItem>();

                            questions.Add(new ChecklistInfo()
                            {
                                Code = row["XIA_CODE"].ToString(),
                                Prompt = row["XIA_PROMPT"].ToString(),
                                Type = row["XIA_ANSTYPE"].ToString(),
                                Min = row["XIA_MIN"].ToString(),
                                Max = row["XIA_MAX"].ToString(),
                                Dec = row["XIA_DEC"].ToString(),
                                Default = row["XIA_DEF"].ToString(),
                                SortOrder = row["XIA_SORT_ORDER"].ToString(),
                                Answers = answers
                            });
                        }
                    }
                    if (questions.Count == 0)
                    {
                        Utils.SetError("No Checklist Questions have been configured.");
                        return null;
                    }
                    VarUtils.Set(WebVars.ChecklistInfoQuestions, questions);
                }
                return questions;
            }
        }
        #endregion

        #region ChecklistInfo Answers
        private Dictionary<string, string[]> ChecklistInfoAnswers(string RefCode)
        {
            var answers = new Dictionary<string, string[]>();

            var fields = "XIL_SCRKEY,XIL_SEQ,XIL_CODE,XIL_ANSWER,XIL_ANSCODE";
            var where = "XIL_MODULE = 'COB' AND XIL_REFCODE = '" + RefCode + "'";
            var query = "SELECT " + fields + " FROM INFLINA WHERE " + where;

            var Table = RecordUtils.Table(query);
            if (Table != null)
            {
                foreach (DataRow row in Table.Rows)
                {
                    var data = new string[4];
                    data[0] = row["XIL_SCRKEY"].ToString();
                    data[1] = row["XIL_SEQ"].ToString();
                    data[2] = row["XIL_ANSWER"].ToString();
                    data[3] = row["XIL_ANSCODE"].ToString();                    
                    answers.Add(row["XIL_CODE"].ToString(), data);
                }
            }

            return answers;
        }
        #endregion

        #region UpdateChecklistInfo
        public void UpdateChecklistInfo(string contractNumber, string checklistSequence)
        {
            var ChecklistInfo = (List<ChecklistInfo>)VarUtils.Get(WebVars.ChecklistInfo);
            if (ChecklistInfo == null)
                return;

            bool updated = false;
            foreach (ChecklistInfo info in ChecklistInfo)
            {
                if (info.Modified)
                {
                    updated = true;
                    if (!string.IsNullOrEmpty(info.UpdateKey))
                    {
                        BsnUtils.ExtraInfoUpdate(info.UpdateKey, info.AnsCode, info.Answer);
                    }
                    else
                    {
                        BsnUtils.ExtraInfoAdd(info.ContractNumber + info.Sequence, info.Code, info.AnsCode, info.Answer);
                    }
                    info.Modified = false;
                }
            }

            if (updated)
            {
                Utils.SetSuccess();
                GetChecklistData(contractNumber, checklistSequence);
            }
        }
        #endregion
    }
}
