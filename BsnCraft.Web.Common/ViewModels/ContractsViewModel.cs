﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using DevExpress.Web;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using BsnCraft.Web.Common.Classes.Grid;

namespace BsnCraft.Web.Common.ViewModels
{
    public class ContractsViewModel : ViewModelBase
    {
        const string BookmarksKey = "Contracts.Bookmarks";

        #region ClearCache
        public void ClearCache()
        {
            VarUtils.Set(WebVars.Headings, null);
            VarUtils.Set(WebVars.Documents, null);
            VarUtils.Set(WebVars.ImportantDocuments, null);
            VarUtils.Set(WebVars.ImportantDocumentsGrid, null);
            VarUtils.Set(WebVars.DealDocuments, null);
            VarUtils.Set(WebVars.JobDocuments, null);
            VarUtils.Set(WebVars.JobInfo, null);
            VarUtils.Set(WebVars.JobInfoQuestions, null);
            VarUtils.Set(WebVars.JobInfoGroups, null);
            VarUtils.Set(WebVars.JobInfoCategories, null);
            VarUtils.Set(WebVars.Events, null);
            VarUtils.Set(WebVars.Variations, null);
            VarUtils.Set(WebVars.VariationEvents, null);
            VarUtils.Set(WebVars.VariationDocuments, null);
            VarUtils.Set(WebVars.Checklists, null);
            VarUtils.Set(WebVars.EstimateDetails, null);
            VarUtils.Set(WebVars.EstimateFilter, null);
            VarUtils.Set(WebVars.Paragraphs, null);
            VarUtils.Set(WebVars.DocumentTypes, null);
            VarUtils.Set(WebVars.DealDocumentTypes, null);
            VarUtils.Set(WebVars.HeadingDocTypes, null);
        }
        #endregion
                
        #region Contracts
        public List<Contract> Contracts
        {
            get
            {
                var contracts = (List<Contract>)VarUtils.Get(WebVars.Contracts);
                if (contracts == null)
                {
                    contracts = new List<Contract>();

                    var fields = "CO_KEY,CO_CUSNUM,CO_NAME,CO_LOTNO,CO_STREETNO,CO_ADDR,CO_CITY,CO_STATE,CO_ZIP,CO_STATUS,CO_SALETYPE";
                    fields += ",CO_DISTRICT,CO_COUNCIL,CO_SALEPROM,CO_SALECENT,CO_COMM1,CO_COMM2,CO_COMM3,CO_HOUSE,CO_ELEVATION";
                    fields += ",CO_SUBJSALE,CO_SAVEPLAN,CO_JOBNUM,CO_SALEGROUP,CO_OPCENTRE,CO_DP,CO_VOLUME,CO_FOLIO";
                    fields += ",CO_SALESCONS,CO_CSR,CO_BLDGSUPV,CO_SERVSUPV";

                    var query = "SELECT " + fields + " FROM CONHDRA";
                    query += " WHERE { fn LENGTH(CO_KEY)} > 0";
                    query += Utils.GetFilterString("CO_STATUS", Utils.Settings.Comp.ActiveContractStatusList, 1);

                    if (Utils.Settings.Comp.RestrictContracts && !BsnUtils.AllContracts)
                    {
                        query += " AND CO_SALESCONS = '" + BsnUtils.BsnInitials + "'";
                    }

                    if (!string.IsNullOrEmpty(Utils.Settings.Comp.ContractsQuery))
                        query += " " + Utils.Settings.Comp.ContractsQuery;
                    
                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            contracts.Add(new Contract()
                            {
                                ContractNumber = row["CO_KEY"].ToString(),
                                CustomerNumber = row["CO_CUSNUM"].ToString(),
                                Name = row["CO_NAME"].ToString(),
                                LotNumber = row["CO_LOTNO"].ToString(),
                                StreetNumber = row["CO_STREETNO"].ToString(),
                                Address = row["CO_ADDR"].ToString(),
                                City = row["CO_CITY"].ToString(),
                                State = row["CO_STATE"].ToString(),
                                PostCode = row["CO_ZIP"].ToString(),
                                Status = row["CO_STATUS"].ToString(),
                                SaleType = row["CO_SALETYPE"].ToString(),
                                SalesConsultant = row["CO_SALESCONS"].ToString(),
                                CustomerService = row["CO_CSR"].ToString(),
                                BuildingSupervisor = row["CO_BLDGSUPV"].ToString(),
                                District = row["CO_DISTRICT"].ToString(),
                                Council = row["CO_COUNCIL"].ToString(),
                                SalesPromotion = row["CO_SALEPROM"].ToString(),
                                SalesCentre = row["CO_SALECENT"].ToString(),
                                Comment1 = row["CO_COMM1"].ToString(),
                                Comment2 = row["CO_COMM2"].ToString(),
                                Comment3 = row["CO_COMM3"].ToString(),
                                House = row["CO_HOUSE"].ToString(),
                                Facade = row["CO_ELEVATION"].ToString(),
                                SubjectSale = row["CO_SUBJSALE"].ToString(),
                                SavingsPlan = row["CO_SAVEPLAN"].ToString(),                                
                                SaleGroup = row["CO_SALEGROUP"].ToString(),
                                OpCentre = row["CO_OPCENTRE"].ToString(),
                                DpNumber = row["CO_DP"].ToString(),
                                Volume = row["CO_VOLUME"].ToString(),
                                Folio = row["CO_FOLIO"].ToString(),
                                JobNumber = row["CO_JOBNUM"].ToString(),
                                IsBookmarked = Bookmarks.Contains(row["CO_KEY"].ToString())
                            });
                        }
                    }
                    VarUtils.Set(WebVars.Contracts, contracts);
                }
                return contracts;
            }
        }
        #endregion
        
        #region LookupContracts
        public List<SearchData> LookupContracts(string query)
        {  
            var match = Contracts.Where(p => p.ContractNumber.Contains(query) || 
                p.CustomerNumber.Contains(query) ||
                p.FriendlyName.ToLower().Contains(query) ||
                p.FullAddress.ToLower().Contains(query)).Take(10);

            var results = new List<SearchData>();
            foreach (var item in match)
            {
                var value = item.ContractNumber + " | " + item.FriendlyName + " | " + item.FullAddress;
                results.Add(new SearchData() { Value = value, Data = JsonConvert.SerializeObject(item) });
            }
            return results;
        }
        #endregion
        
        #region CustomerContracts
        public List<Contract> CustomerContracts(string customerNumber)
        {
            return Contracts.Where(p => p.CustomerNumber == customerNumber).ToList();
        }
        #endregion

        #region GetContract
        public Contract GetContract(string contractNumber)
        {
            var contract = Contracts.Where(p => p.ContractNumber == contractNumber).FirstOrDefault();
            if (contract == null)
            {
                //Since a Lead is a contract object, should be safe. Saves too much duplication in Leads module
                contract = new LeadsViewModel().Leads.Where(p => p.ContractNumber == contractNumber).FirstOrDefault();
            }
            return contract;
        }
        #endregion

        #region CheckEffective
        public void CheckEffective(string contractNumber)
        {
            if (string.IsNullOrEmpty(Utils.Settings.Comp.EffectiveDate))
                return;

            var fields = "CO_KEY,CO_JOBNUM,JSM_EFFECTIVE";
            string join = " JOIN JCSPECM ON JSM_JOBNUM = CO_JOBNUM";
            var query = "SELECT " + fields + " FROM CONHDRA" + join;
            query += " WHERE CO_KEY = '" + contractNumber + "'";

            var Table = RecordUtils.Table(query);
            if (Table != null && Table.Rows.Count > 0)
            {
                DataRow row = Table.Rows[0];
                var effective = (row["JSM_EFFECTIVE"] is DateTime) ? ((DateTime)row["JSM_EFFECTIVE"]) : DateTime.MinValue;
                var effectiveDate = Utils.JulianDate(effective);
                if (Utils.Settings.Comp.EffectiveDate != effectiveDate)
                {
                    var jobNumber = row["CO_JOBNUM"].ToString();
                    Log.Info("Updating " + jobNumber + " Effective: " + effectiveDate + " to " + Utils.Settings.Comp.EffectiveDate);
                    BsnUtils.SetEffective(jobNumber);
                }
            }
        }
        #endregion

        #region SingleContract
        public Contract SingleContract(string contractNumber)
        {
            /*
            var contract = (Contract)VarUtils.Get(WebVars.SingleContract);
            if (contract != null)
            {
                return contract;
            }
            */

            Contract contract = null;

            var fields = "CO_KEY,CO_CUSNUM,CO_NAME,CO_LOTNO,CO_STREETNO,CO_ADDR,CO_CITY,CO_STATE,CO_ZIP,CO_STATUS,CO_SALETYPE";
            fields += ",CO_DISTRICT,CO_COUNCIL,CO_SALEPROM,CO_SALECENT,CO_COMM1,CO_COMM2,CO_COMM3,CO_SUBJSALE,CO_SAVEPLAN";
            fields += ",CO_JOBNUM,CO_SALEGROUP,CO_OPCENTRE,CO_SALESCONS,CO_CSR,CO_BLDGSUPV,CO_SERVSUPV";
            var query = "SELECT " + fields + " FROM CONHDRA WHERE CO_KEY = '" + contractNumber + "'";
            
            var Table = RecordUtils.Table(query);
            if (Table != null && Table.Rows.Count > 0)
            {
                DataRow row = Table.Rows[0];
                contract = new Contract()
                {
                    ContractNumber = row["CO_KEY"].ToString(),
                    CustomerNumber = row["CO_CUSNUM"].ToString(),
                    Name = row["CO_NAME"].ToString(),
                    LotNumber = row["CO_LOTNO"].ToString(),
                    StreetNumber = row["CO_STREETNO"].ToString(),
                    Address = row["CO_ADDR"].ToString(),
                    City = row["CO_CITY"].ToString(),
                    State = row["CO_STATE"].ToString(),
                    PostCode = row["CO_ZIP"].ToString(),
                    Status = row["CO_STATUS"].ToString(),
                    SaleType = row["CO_SALETYPE"].ToString(),
                    SalesConsultant = row["CO_SALESCONS"].ToString(),
                    CustomerService = row["CO_CSR"].ToString(),
                    BuildingSupervisor = row["CO_BLDGSUPV"].ToString(),
                    District = row["CO_DISTRICT"].ToString(),
                    Council = row["CO_COUNCIL"].ToString(),
                    SalesPromotion = row["CO_SALEPROM"].ToString(),
                    SalesCentre = row["CO_SALECENT"].ToString(),
                    Comment1 = row["CO_COMM1"].ToString(),
                    Comment2 = row["CO_COMM2"].ToString(),
                    Comment3 = row["CO_COMM3"].ToString(),
                    SubjectSale = row["CO_SUBJSALE"].ToString(),
                    SavingsPlan = row["CO_SAVEPLAN"].ToString(),
                    JobNumber = row["CO_JOBNUM"].ToString(),
                    SaleGroup = row["CO_SALEGROUP"].ToString(),
                    OpCentre = row["CO_OPCENTRE"].ToString()
                };
                //VarUtils.Set(WebVars.SingleContract, contract);
            }

            return contract;
        }
        #endregion

        #region Bookmarks
        public List<string> Bookmarks
        {
            get
            {
                var bookmarks = (List<string>)VarUtils.Get(WebVars.BookmarkedContracts);
                if (bookmarks == null)
                {
                    string keyvals = string.Empty;
                    try
                    {
                        keyvals = BsnUtils.GetSettings(SettingsLevel.User, BookmarksKey);
                        bookmarks = (List<string>)JsonConvert.DeserializeObject(keyvals, typeof(List<string>));
                        if (bookmarks == null)
                        {
                            bookmarks = new List<string>();
                        }                        
                    }
                    catch (Exception e)
                    {
                        Utils.LogExc(e);
                        bookmarks = new List<string>();
                    }
                    VarUtils.Set(WebVars.BookmarkedContracts, bookmarks);
                }
                return bookmarks;
            }
        }
        #endregion

        #region Bookmark
        public void Bookmark(string contractNumber)
        {
            var Contract = GetContract(contractNumber);
            if (Bookmarks.Contains(contractNumber))
            {
                Bookmarks.Remove(contractNumber);
                Contract.IsBookmarked = false;
            }
            else
            {
                Bookmarks.Add(contractNumber);
                Contract.IsBookmarked = true;
            }

            string keyvals = JsonConvert.SerializeObject(Bookmarks);
            BsnUtils.SaveSettings(SettingsLevel.User, BookmarksKey, keyvals);

            string message = (Contract.IsBookmarked) ? "Added" : "Removed";
            message += " contract bookmark: " + contractNumber;
            Utils.SetSuccess(message);
        }
        #endregion

        #region UpdateContract
        public void UpdateContract(Contract contract)
        {
            bool isNew = (string.IsNullOrEmpty(contract.ContractNumber));
            string key = BsnUtils.UpdateContract(contract);
            if (isNew && !string.IsNullOrEmpty(key))
            {
                var newContract = GetContract(key);
                if (newContract == null)
                {
                    Utils.SetError("Error finding new key: " + key);
                }
                else
                {
                    var menu = (newContract.IsLead) ? Menus.Leads_Details : Menus.Contracts_Details;                    
                    string urlextras = "?key=" + newContract.ContractNumber;
                    var location = Utils.GetMenu(menu).ResolveUrl + urlextras;
                    VarUtils.Set(WebVars.JSLocation, location);
                }
            }
        }
        #endregion

        #region GetContractSettings
        public ContractSettings Settings
        {

            get
            {
                var contractSettings = VarUtils.Get(WebVars.ContractSettings) as List<ContractSettings>;

                if (contractSettings == null)
                {
                    contractSettings = Utils.GetRecords<ContractSettings>("CONHDRB", "");
                    VarUtils.Set(WebVars.ContractSettings, contractSettings);
                }
                if (contractSettings == null)
                {
                    Utils.SetError("Error getting Contract Settings.");
                    contractSettings = new List<ContractSettings>
                    {
                        new ContractSettings()
                    };
                }
                return contractSettings.FirstOrDefault();
            }
        }
        #endregion

        #region GetInvoicingSettings
        public InvoicingSettings InvoicingSettings
        {
            get
            {
                var invoicingSettings = VarUtils.Get(WebVars.InvoicingSettings) as List<InvoicingSettings>;
                if (invoicingSettings == null || invoicingSettings.Count <= 0)
                {
                    invoicingSettings = new List<InvoicingSettings>();
                    string fields = "IVC_INTCO";
                    string query = "SELECT " + fields + " FROM IVCMSTB WHERE IVC_KEY =' '";

                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {                                                       
                            bool interfaceContracts = row["IVC_INTCO"].ToString().Equals("Y") ? true : false;
                            invoicingSettings.Add(new InvoicingSettings()
                            {
                               InterfaceContracts = interfaceContracts,                                
                            });
                        }
                    }                    

                    if (invoicingSettings.Count > 0)
                    {
                        VarUtils.Set(WebVars.InvoicingSettings, invoicingSettings);
                    }
                    else
                    {
                        Utils.SetError("Error getting Contract Settings.");                        
                    }
                }                
                return invoicingSettings.FirstOrDefault();
            }
        }
        #endregion

        #region Documents        
        public List<Document> Documents(string contractNumber, string xRefSource = "", string xRefKey = "")
        {
            var documents = (List<Document>)VarUtils.Get(WebVars.Documents);
            if (documents == null && !string.IsNullOrEmpty(contractNumber))
            {
                documents = new List<Document>();
                string fields = "CLW_DOCTYP,CLW_SCRKEY,CLW_CONTRACT,CLW_SEQNUM,CLW_DOCREF,CLW_DESCR,CLW_CRE_DATE,CLW_ALT_DOCS,CLW_XREF_KEY,CLW_XREF_SRC,IVWP_NOCHNG";
                var join = "JOIN IVCTBLP ON IVWP_CODE = CLW_DOCTYP AND IVWP_MODULE = 'CO'";
                string query = "SELECT " + fields + " FROM CONLINW " + join + " WHERE CLW_CONTRACT = '" + contractNumber + "'";
                if (!string.IsNullOrEmpty(xRefSource) && !string.IsNullOrEmpty(xRefKey))
                {
                    query += " AND CLW_XREF_SRC='" + xRefSource + "' AND CLW_XREF_KEY='" + xRefKey + "'";
                }

                string typelist = string.Empty;
                foreach (string doctype in Utils.Settings.Comp.DocumentTypes)
                {
                    typelist += "'" + doctype + "',";
                }
                if (!string.IsNullOrEmpty(typelist))
                {
                    query += " AND CLW_DOCTYP IN (" + typelist.TrimEnd(',') + ")";
                }
                query += " ORDER BY CLW_SCRKEY ASC";

                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    foreach (DataRow row in Table.Rows)
                    {
                        string date = (row["CLW_CRE_DATE"] is DateTime) ? ((DateTime)row["CLW_CRE_DATE"]).ToString("dd/MM/yyyy") : string.Empty;
                        int sequence = int.Parse(row["CLW_SEQNUM"].ToString());
                        string xref_key = string.Empty;
                        string xref_src = string.Empty;
                        if (!string.IsNullOrEmpty(xRefSource) && !string.IsNullOrEmpty(xRefKey))
                        {
                            xref_src = row["CLW_XREF_SRC"].ToString();
                            xref_key = row["CLW_XREF_KEY"].ToString();
                        }

                        documents.Add(new Document()
                        {
                            Key = row["CLW_SCRKEY"].ToString(),
                            ContractNumber = row["CLW_CONTRACT"].ToString(),
                            Type = row["CLW_DOCTYP"].ToString(),
                            Sequence = sequence.ToString("000"),
                            Reference = row["CLW_DOCREF"].ToString(),
                            Description = row["CLW_DESCR"].ToString(),                            
                            Filename = row["CLW_ALT_DOCS"].ToString(),
                            Locked = (row["IVWP_NOCHNG"].ToString() == "Y"),
                            Date = date,
                            XRefSource = xref_src,
                            XRefKey = xref_key
                        });
                    }
                }
                if (documents.Count > 0)
                {
                    var notes = DocumentNotes(contractNumber);
                    var spec = string.Empty;
                    foreach (var document in documents)
                    {
                        spec = string.Empty;
                        if (notes.ContainsKey(document.Key))
                        {
                            spec = notes[document.Key];
                        }
                        document.LoadNotes(spec);
                    }
                    VarUtils.Set(WebVars.Documents, documents);
                }                
            }
            return documents;
        }
        #endregion

        #region Document Notes
        private Dictionary<string, string> DocumentNotes(string contractNumber)
        {
            var notes = new Dictionary<string, string>();

            var fields = "CLX_CONTRACT,CLX_CONST,CLX_DESC";
            var where = "CLX_CONTRACT ='" + contractNumber + "'";
            var query = "SELECT " + fields + " FROM CONLINX WHERE " + where;

            var Table = RecordUtils.Table(query);
            if (Table != null)
            {
                var spec = string.Empty;
                var key = string.Empty;
                int i = 0;
                foreach (DataRow row in Table.Rows)
                {
                    i++;
                    if (row["CLX_CONST"].ToString() != key && !string.IsNullOrEmpty(key))
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                        spec = string.Empty;
                    }
                    spec += row["CLX_DESC"].ToString();
                    key = row["CLX_CONST"].ToString();
                    if (i == Table.Rows.Count)
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                    }
                }
            }

            return notes;
        }
        #endregion

        #region ImportantDocuments
        public List<dynamic> ImportantDocuments(string contractNumber)
        {
            var documents = (List<dynamic>)VarUtils.Get(WebVars.ImportantDocuments);
            if (documents == null && !string.IsNullOrEmpty(contractNumber))
            {
                documents = new List<dynamic>();
                string fields = "CLW_DOCTYP,CLW_SCRKEY,CLW_CONTRACT,CLW_SEQNUM,CLW_DOCREF,CLW_DESCR,CLW_CRE_DATE,CLW_ALT_DOCS,CLW_RATING,";
                fields += "CASE CLW_RATING WHEN 'H' THEN 0 WHEN 'M' THEN 1 WHEN 'L' THEN 2 END AS RATING";
                string where = "WHERE CLW_CONTRACT = '" + contractNumber + "' AND CLW_RATING <> '' ORDER BY RATING";
                string query = "SELECT " + fields + " FROM CONLINW " + where;

                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    foreach (DataRow row in Table.Rows)
                    {
                        string date = (row["CLW_CRE_DATE"] is DateTime) ? ((DateTime)row["CLW_CRE_DATE"]).ToString("dd/MM/yyyy") : string.Empty;
                        int.TryParse(row["CLW_SEQNUM"].ToString(), out int sequence);
                        int.TryParse(row["RATING"].ToString(), out int rating);

                        documents.Add(new Document()
                        {
                            Key = row["CLW_SCRKEY"].ToString(),
                            ContractNumber = row["CLW_CONTRACT"].ToString(),
                            Type = row["CLW_DOCTYP"].ToString(),
                            Sequence = sequence.ToString("000"),
                            Reference = row["CLW_DOCREF"].ToString(),
                            Description = row["CLW_DESCR"].ToString(),
                            Filename = row["CLW_ALT_DOCS"].ToString(),
                            Date = date,
                            Rating = rating
                        });
                    }
                }
                if (documents.Count > 0)
                {
                    var notes = DocumentNotes(contractNumber);
                    var spec = string.Empty;
                    foreach (var document in documents)
                    {
                        spec = string.Empty;
                        if (notes.ContainsKey(document.Key))
                        {
                            spec = notes[document.Key];
                        }
                        document.LoadNotes(spec);
                    }
                    VarUtils.Set(WebVars.ImportantDocuments, documents);
                }
            }
            return documents;
        }

        public BsnGridViewModel ImpDocsGridView(string contractNumber)
        {
            var impDocsGridView = (BsnGridViewModel)VarUtils.Get(WebVars.ImportantDocumentsGrid);

            if (impDocsGridView == null)
            {

                impDocsGridView = new BsnGridViewModel("ImportantDocuments", "Key", "Important Documents")
                {
                    DataSource = ImportantDocuments(contractNumber),
                    ShowSearch = false
                };

                int sort = 0;
                impDocsGridView.Columns.Add(new BsnGridColumn() { Name = "Sequence", FieldName = "Sequence", Caption = "Seq", Sort = sort++ });
                impDocsGridView.Columns.Add(new BsnGridColumn() { Name = "Type", FieldName = "Type", Caption = "Type", Sort = sort++ });
                impDocsGridView.Columns.Add(new BsnGridColumn() { Name = "Reference", FieldName = "Reference", Caption = "Ref", Sort = sort++ });
                impDocsGridView.Columns.Add(new BsnGridColumn() { Name = "Description", FieldName = "Description", Caption = "Description", Sort = sort++ });
                impDocsGridView.Columns.Add(new BsnGridColumn() { Name = "Notes", FieldName = "Notes", Caption = "Notes", Sort = sort++ });

                impDocsGridView.FormatConditions.Add(new GridViewFormatConditionHighlight()
                {
                    FieldName = "Rating",
                    Expression = "[Rating] < 1",
                    ApplyToRow = true,
                    Format = GridConditionHighlightFormat.LightRedFillWithDarkRedText
                });

                impDocsGridView.FormatConditions.Add(new GridViewFormatConditionHighlight()
                {
                    FieldName = "Rating",
                    Expression = "[Rating] == 1",
                    ApplyToRow = true,
                    Format = GridConditionHighlightFormat.YellowFillWithDarkYellowText
                });

                impDocsGridView.FormatConditions.Add(new GridViewFormatConditionHighlight()
                {
                    FieldName = "Rating",
                    Expression = "[Rating] > 1",
                    ApplyToRow = true,
                    Format = GridConditionHighlightFormat.GreenFillWithDarkGreenText
                });

                VarUtils.Set(WebVars.ImportantDocumentsGrid, impDocsGridView);
            }

            return impDocsGridView;
        }
        #endregion

        #region DealDocuments
        public List<Document> DealDocuments(string contractNumber)
        {
            var dealDocuments = (List<Document>)VarUtils.Get(WebVars.DealDocuments);
            if (dealDocuments == null)
            {
                dealDocuments = new List<Document>();
                foreach (var docType in DealDocumentTypes)
                {
                    var doc = Documents(contractNumber).Where(p => p.DealKey == docType.Key).FirstOrDefault();
                    if (doc == null)
                    {
                        doc = new Document()
                        {
                            Key = docType.Key,
                            ContractNumber = contractNumber,
                            Type = docType.Type,
                            Reference = docType.Reference,
                            Description = docType.Description,
                            IsNew = true
                        };
                    }
                    bool uploaded = !(string.IsNullOrEmpty(doc.Filename));
                    doc.SubmitStatus = uploaded ? 1 : 0;
                    if (!uploaded && docType.Required)
                    {
                        doc.SubmitStatus = -1;
                    }
                    dealDocuments.Add(doc);
                }
                if (dealDocuments.Count > 0)
                {
                    VarUtils.Set(WebVars.DealDocuments, dealDocuments);
                }
            }
            return dealDocuments;
        }
        #endregion

        #region DocumentImage
        public string DocumentImage(Document document, int thumbSize)
        {
            string filename = Utils.DocsFolder + Path.GetFileName(document.Filename);
            
            if (!File.Exists(HttpContext.Current.Server.MapPath(filename)))
                return string.Empty;
                    
            if (thumbSize != 0)
            {
                filename = "~/Shared/Thumbnail.aspx?image=" + filename;
                if (thumbSize > 0)
                {
                    filename += "&width=" + thumbSize.ToString();
                    filename += "&height=" + thumbSize.ToString();
                }
            }
            return Utils.ResolveUrl(filename);
        }
        #endregion

        #region GetDocument        
        public Document GetDocument(string key)
        {
            if (string.IsNullOrEmpty(key))
                return null;

            var contractNumber = key.Substring(0, 6);
            if (GetContract(contractNumber) == null)
            {
                return new JobCostViewModel().GetDocument(key);
            }

            return Documents(contractNumber).Where(p => p.Key == key).FirstOrDefault();
        }
        #endregion
        
        #region DocumentTypes
        public List<DocumentType> DocumentTypes
        {
            get
            {
                var documentTypes = (List<DocumentType>)VarUtils.Get(WebVars.DocumentTypes);
                if (documentTypes == null || documentTypes.Count == 0)
                {
                    documentTypes = new List<DocumentType>();
                    string fields = "IVWP_CODE,IVWP_MODULE,IVWP_MERGEFMT,IVWP_DESCR,IVWP_FILE";
                    string query = "SELECT " + fields + " FROM IVCTBLP WHERE IVWP_MODULE='CO'";

                    string typelist = string.Empty;
                    foreach (string doctype in Utils.Settings.Comp.DocumentTypes)
                    {
                        if (!string.IsNullOrEmpty(doctype))
                        {
                            typelist += "'" + doctype + "',";
                        }                        
                    }
                    if (!string.IsNullOrEmpty(typelist))
                    {
                        query += " AND IVWP_CODE IN (" + typelist.TrimEnd(',') + ")";
                    }

                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            var code = row["IVWP_CODE"].ToString();
                            var descr = row["IVWP_DESCR"].ToString();
                            var module = row["IVWP_MODULE"].ToString();

                            var docType = new DocumentType(code, descr)
                            {
                                ExtraData = module,
                                MasterDocument = row["IVWP_FILE"].ToString()
                            };
                            documentTypes.Add(docType);
                        }
                    }
                    if (documentTypes != null && documentTypes.Count > 0)
                    {
                        VarUtils.Set(WebVars.DocumentTypes, documentTypes);
                    }
                }
                return documentTypes;
            }
        }

        public string DocTypeDesc(string code)
        {
            var docTypes = DocumentTypes.Where(p => p.Code == code);
            if (docTypes == null || docTypes.Count() == 0)
            {
                return string.Empty;
            }
            return docTypes.FirstOrDefault().Desc;
        }
        #endregion

        #region DealDocumentTypes
        public List<DealDocument> DealDocumentTypes
        {
            get
            {
                var documentTypes = (List<DealDocument>)VarUtils.Get(WebVars.DealDocumentTypes);
                if (documentTypes == null)
                {
                    var docTypes = Utils.Settings.Comp.DealDocumentTypes;
                    documentTypes = (List<DealDocument>)JsonConvert.DeserializeObject(docTypes, typeof(List<DealDocument>));
                    if (documentTypes != null && documentTypes.Count > 0)
                    {
                        VarUtils.Set(WebVars.DealDocumentTypes, documentTypes);
                    }
                }
                return documentTypes;
            }
        }
        #endregion

        #region JobInfoDocuments
        public List<ComboItem> JobInfoDocuments
        {
            get
            {
                var documentTypes = VarUtils.Get(WebVars.JobInfoDocuments) as List<ComboItem>;
                if (documentTypes == null || documentTypes.Count == 0)
                {
                    documentTypes = new List<ComboItem>();
                    string fields = "IVWP_CODE,IVWP_MODULE,IVWP_MERGEFMT,IVWP_DESCR";
                    string query = "SELECT " + fields + " FROM IVCTBLP WHERE IVCTBLP.IVWP_MODULE='CO'";

                    string typelist = string.Empty;
                    foreach (string doctype in Utils.Settings.Comp.JobInfoDocumentTypes)
                    {
                        typelist += "'" + doctype + "',";
                    }
                    if (!string.IsNullOrEmpty(typelist))
                    {
                        query += " AND IVWP_CODE IN (" + typelist.TrimEnd(',') + ")";
                    }

                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            documentTypes.Add(new ComboItem(row["IVWP_CODE"].ToString(), row["IVWP_DESCR"].ToString()));
                        }
                    }
                    if (documentTypes != null && documentTypes.Count > 0)
                    {
                        VarUtils.Set(WebVars.JobInfoDocuments, documentTypes);
                    }
                }
                return documentTypes;
            }
        }
        #endregion

        #region UpdateDocument
        public string UpdateDocument(Document document)
        {            
            string bsnparams = string.Empty;            
            var contract = GetContract(document.ContractNumber);
            if (contract == null)
            {
                Utils.SetError("Could not find key: " + document.ContractNumber);
                return string.Empty;
            }
            
            int contractNumber = int.Parse(document.ContractNumber);
            if (!string.IsNullOrEmpty(document.UploadFile))
            {
                string ext = Path.GetExtension(document.UploadFile).ToUpper().Replace(".","");
                bsnparams += "TYPE=UPLOAD,EXT=" + ext + ",";
            }

            bool success = false;
            bool isSitePhoto = false;            
            string newkey = string.Empty;
            
            if (document.Module == "CO")
            {
                if (!string.IsNullOrEmpty(document.ReportParameters))
                {
                    bsnparams += "RPTPARAMS=" + document.ReportParameters + ",";
                }

                if (!string.IsNullOrEmpty(document.XRefSource) && !string.IsNullOrEmpty(document.XRefKey))
                {
                    bsnparams += "XREF_SRC=" + document.XRefSource + ",";
                    bsnparams += "XREF_KEY=" + document.XRefKey + ",";
                }

                isSitePhoto = (document.Type == Settings.SitePhotoDocType);

                /*
                if (string.IsNullOrEmpty(document.Reference))
                {
                    document.Reference = (isSitePhoto) ? PageModes.Photos.ToString() : DocTypeDesc(document.Type);                
                }
                */

                if (string.IsNullOrEmpty(document.Description))
                {
                    document.Description = DocTypeDesc(document.Type);
                }
                int.TryParse(document.Sequence, out int doc);
                success = BsnUtils.ContractDocument(contractNumber, ref doc, document, ref bsnparams);
                newkey = document.ContractNumber + doc.ToString("000");
            }
            else
            {
                var doc = document.Sequence;
                success = BsnUtils.JobDocument(contract.JobNumber, ref doc, document, ref bsnparams);
                int.TryParse(doc, out int seq);
                newkey = contract.JobNumber + seq.ToString("000");
            }


            if (success && !string.IsNullOrEmpty(document.UploadFile))
            {
                byte[] bytes = File.ReadAllBytes(document.UploadFile);
                if (isSitePhoto)
                {
                    var maxHeight = Utils.Settings.Comp.PhotoHeight;
                    var maxWidth = Utils.Settings.Comp.PhotoWidth;
                    Log.Debug("MaxHeight = " + maxHeight.ToString() + ", MaxWidth = " + maxWidth.ToString());
                    if (maxHeight > 0 || maxWidth > 0)
                    {
                        bytes = Utils.CreateThumbnail(document.UploadFile, maxWidth, maxHeight);
                    }
                }
                success = BsnUtils.Upload(ref bytes, bsnparams);
            }            

            if (success)
            {
                Utils.SetSuccess();
            }
            
            return newkey;
        }
        #endregion

        #region DownloadDocument
        public bool DownloadDocument(Document document)
        {
            return DownloadDocument(document, false);
        }

        public bool DownloadDocument(Document document, bool useExisting, int waitSeconds = 0)
        {
            if (document == null || string.IsNullOrEmpty(document.Filename))
            {
                return false;
            }

            string filename = Path.GetFileName(document.Filename);
            filename = HttpContext.Current.Server.MapPath(Utils.DocsFolder) + filename;
            if (File.Exists(filename) && useExisting)
            {
                return true;
            }

            byte[] bytes = new byte[0];
            if (!BsnUtils.Download(ref bytes, document.Filename, waitSeconds))
            {
                return false;
            }

            try
            {
                Log.Debug("Downloading document: " + filename);
                using (var fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
            catch (Exception ex)
            {
                Utils.LogExc(ex);
                return false;
            }

            return true;
        }
        #endregion       

        #region EventChecklist
        public Dictionary<string, string> EventChecklist
        {
            get
            {
                var eventChecklist = (Dictionary<string, string>)VarUtils.Get(WebVars.EventChecklist);
                if (eventChecklist == null)
                {
                    eventChecklist = new Dictionary<string, string>();
                    string query = "SELECT COTW_CODE,COTW_PARENT FROM COTABLW WHERE COTW_PARENT > 0";
                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            eventChecklist.Add(row["COTW_CODE"].ToString(), row["COTW_PARENT"].ToString());
                        }
                    }
                    VarUtils.Set(WebVars.EventChecklist, eventChecklist);
                }
                return eventChecklist;
            }
        }
        #endregion

        #region Events
        public List<Event> Events(string contractNumber)
        {
            var events = (List<Event>)VarUtils.Get(WebVars.Events);
            if (events == null && !string.IsNullOrEmpty(contractNumber))
            {
                events = new List<Event>();
                string fields = "CLE_KEY,CLE_SCRKEY,CLE_CONTRACT,CLE_EVENT,CLE_DESC,CLE_REGDATE,CLE_FOREDATE,CLE_DUEDATE,CLE_ACTDATE,";
                fields += "CLE_PCTCMPL,CLE_EMPLOYEE,CLE_AMOUNT,CLE_REF,CO_CONTRACT,CO_SALEGROUP,COTG_CODE,COTG_NAME,";
                fields += "COTW_CATG,COTW_USERCAT,COTX_EVENT,COTX_GROUP,COTX_SORT_ORDER";
                string join = "JOIN CONHDRA ON CO_CONTRACT = CLE_CONTRACT ";
                join += "LEFT OUTER JOIN COTABLG ON CLE_EMPLOYEE = COTG_CODE ";
                join += "JOIN COTABLX ON CLE_EVENT = COTX_EVENT AND CO_SALEGROUP = COTX_GROUP ";
                join += "JOIN COTABLW ON CLE_EVENT = COTW_CODE";
                string mainQuery = "SELECT " + fields + " FROM CONLINE " + join + " WHERE CLE_CONTRACT='" + contractNumber + "'";

                string catgQuery = mainQuery;

                bool hasCheckList = (EventChecklist.Count > 0);

                string catglist = string.Empty;
                foreach (string evtcatg in Utils.Settings.Comp.EventCategories)
                {
                    if (!string.IsNullOrEmpty(evtcatg))
                    {
                        catglist += "'" + evtcatg + "',";
                    }                    
                }
                if (!string.IsNullOrEmpty(catglist))
                {
                    catgQuery += " AND COTW_CATG IN (" + catglist.TrimEnd(',') + ")";
                }

                if (!string.IsNullOrEmpty(Utils.Settings.Comp.EventsQuery))
                    catgQuery += " " + Utils.Settings.Comp.EventsQuery;

                string order = "ORDER BY COTX_SORT_ORDER, CLE_EVENT";
                catgQuery += " " + order;

                var Table = RecordUtils.Table(catgQuery);
                if (Table != null)
                {
                    foreach (DataRow row in Table.Rows)
                    {
                        AddEvent(row, events, hasCheckList);
                    }
                }

                //Add any custom events outside of main categories below

                if (Utils.Settings.Comp.DealSubmitEvent != 0)
                {
                    string dealEvent = mainQuery;
                    dealEvent += " AND CLE_EVENT='" + Utils.Settings.Comp.DealSubmitEvent.ToString("00000") + "'";
                    dealEvent += " " + order;

                    Table = RecordUtils.Table(dealEvent);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            AddEvent(row, events, hasCheckList);
                        }
                    }
                }
                

                if (events.Count > 0)
                {
                    var notes = EventNotes(contractNumber);
                    var spec = string.Empty;
                    foreach (var contractEvent in events)
                    {
                        spec = string.Empty;
                        if (notes.ContainsKey(contractEvent.Key))
                        {
                            spec = notes[contractEvent.Key];
                        }
                        contractEvent.LoadNotes(spec);
                    }
                    VarUtils.Set(WebVars.Events, events);
                }
            }
            return events;
        }

        public void AddEvent(DataRow row, List<Event> events, bool hasCheckList = false)
        {
            var forecast = (row["CLE_FOREDATE"] is DateTime) ? ((DateTime)row["CLE_FOREDATE"]) : DateTime.MinValue;
            var due = (row["CLE_DUEDATE"] is DateTime) ? ((DateTime)row["CLE_DUEDATE"]) : DateTime.MinValue;
            var actual = (row["CLE_ACTDATE"] is DateTime) ? ((DateTime)row["CLE_ACTDATE"]) : DateTime.MinValue;
            var registered = (row["CLE_REGDATE"] is DateTime) ? ((DateTime)row["CLE_REGDATE"]) : DateTime.MinValue;

            string amount = (row["CLE_AMOUNT"] is Decimal) ?
                ((((Decimal)row["CLE_AMOUNT"]) != 0) ?
                ((Decimal)row["CLE_AMOUNT"]).ToString("C") :
                string.Empty) :
                string.Empty;

            string eventCode = row["CLE_EVENT"].ToString();
            string parent = string.Empty;

            if (hasCheckList)
            {
                if (EventChecklist.ContainsKey(eventCode))
                {
                    parent = EventChecklist[eventCode];
                }
            }

            var empCode = row["CLE_EMPLOYEE"].ToString();
            var employee = string.Empty;
            if (!string.IsNullOrEmpty(empCode))
            {
                employee = row["COTG_NAME"].ToString();
            }

            events.Add(new Event()
            {
                Key = row["CLE_SCRKEY"].ToString(),
                ContractNumber = row["CLE_CONTRACT"].ToString(),
                Code = row["CLE_EVENT"].ToString(),
                Description = row["CLE_DESC"].ToString(),
                CatgCode = row["COTW_USERCAT"].ToString(),
                EmpCode = empCode,
                Employee = employee,
                Reference = row["CLE_REF"].ToString(),
                Amount = amount,
                PctComplete = row["CLE_PCTCMPL"].ToString(),
                Forecast = forecast,
                Due = due,
                Actual = actual,
                Registered = registered,
                Parent = parent
            });
        }
        #endregion

        #region Event Notes
        private Dictionary<string, string> EventNotes(string contractNumber)
        {
            var notes = new Dictionary<string, string>();
            
            var fields = "CLK_CONTRACT,CLK_CONST,CLK_DESC";
            var where = "CLK_CONTRACT ='" + contractNumber + "'";
            var query = "SELECT " + fields + " FROM CONLINK WHERE " + where;

            var Table = RecordUtils.Table(query);
            if (Table != null)
            {
                var spec = string.Empty;
                var key = string.Empty;
                int i = 0;
                foreach (DataRow row in Table.Rows)
                {
                    i++;
                    if (row["CLK_CONST"].ToString() != key && !string.IsNullOrEmpty(key))
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                        spec = string.Empty;
                    }
                    spec += row["CLK_DESC"].ToString();
                    key = row["CLK_CONST"].ToString();
                    if (i == Table.Rows.Count)
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                    }
                }
            }

            return notes;
        }
        #endregion

        #region GetEvent
        public Event GetEvent(string key)
        {
            string contractNumber = key.Substring(0, 6);
            return Events(contractNumber).Where(p => p.Key == key).FirstOrDefault();
        }

        public Event GetEvent(string contractNumber, string eventNumber)
        {
            return Events(contractNumber).Where(p => p.Code == eventNumber).FirstOrDefault();
        }
        #endregion

        #region UpdateEvent
        public void UpdateEvent(Event contractEvent)
        {
            BsnUtils.EventUpdate(contractEvent);
        }
        #endregion

        #region EventRegister
        public void EventRegister(string key)
        {
            string[] keyval = key.Split(':');
            string contractNumber = Utils.GetArray(keyval, 0);
            string eventNumber = Utils.GetArray(keyval, 1);

            var contractEvent = GetEvent(contractNumber, eventNumber);
            if (contractEvent == null)
            {
                Utils.SetError("Event not found.");
                return;
            }

            if (contractEvent.IsRegistered)
            {
                BsnUtils.EventUnregister(contractNumber, eventNumber);
            }
            else
            {
                BsnUtils.EventRegister(contractNumber, eventNumber);
            }
        }
        #endregion

        #region Variations
        public List<Variation> Variations(string contractNumber)
        {
            var variations = (List<Variation>)VarUtils.Get(WebVars.Variations);
            if (variations == null && !string.IsNullOrEmpty(contractNumber))
            {
                variations = new List<Variation>();
                string fields = "CLV_SEQA,CLV_SCRKEY,CLV_CONTRACTA,CLV_REF,CLV_DESCR,CLV_DATE,CLV_VTYPE,CLV_AMOUNT,CLV_GST_AMT,CLV_COST,CLV_ISSFLAG,CLV_INVOICED";
                string query = "SELECT " + fields + " FROM CONLINV WHERE CLV_CONTRACT ='" + contractNumber + "'";

                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    foreach (DataRow row in Table.Rows)
                    {
                        string date = (row["CLV_DATE"] is DateTime) ? 
                            ((DateTime)row["CLV_DATE"]).ToString("dd/MM/yyyy") : string.Empty;

                        decimal amount = (row["CLV_AMOUNT"] is Decimal) ? ((Decimal)row["CLV_AMOUNT"]) : 0;
                        decimal gstAmount = (row["CLV_GST_AMT"] is Decimal) ? ((Decimal)row["CLV_GST_AMT"]) : 0;
                        decimal costAmount = (row["CLV_COST"] is Decimal) ? ((Decimal)row["CLV_COST"]) : 0;
                        decimal incAmount = amount + gstAmount;
                        bool issued = row["CLV_ISSFLAG"].ToString().Equals("Y") ? true: false;
                        bool invoiced = row["CLV_INVOICED"].ToString().Equals("Y") ? true : false;                        
                        variations.Add(new Variation()
                        {
                            Key = row["CLV_SCRKEY"].ToString(),
                            ContractNumber = row["CLV_CONTRACTA"].ToString(),
                            Sequence = row["CLV_SEQA"].ToString(),
                            Date = date,
                            Reference = row["CLV_REF"].ToString(),
                            Description = row["CLV_DESCR"].ToString(),
                            Type = row["CLV_VTYPE"].ToString(),
                            Amount = amount,
                            GSTAmount = gstAmount,
                            IncAmount = incAmount,
                            CostAmount = costAmount,
                            IncludeGST = true,
                            Issued = issued,
                            Invoiced = invoiced
                        });
                    }
                }
                if (variations.Count > 0)
                {
                    var notes = VariationNotes(contractNumber);
                    var spec = string.Empty;
                    foreach (var variation in variations)
                    {
                        spec = string.Empty;
                        if (notes.ContainsKey(variation.Key))
                        {
                            spec = notes[variation.Key];
                        }
                        variation.LoadNotes(spec);
                    }
                    VarUtils.Set(WebVars.Variations, variations);
                }       
            }
            return variations;
        }
        #endregion

        #region Variation Notes
        private Dictionary<string, string> VariationNotes(string contractNumber)
        {
            var notes = new Dictionary<string, string>();

            var fields = "CLH_ALPHA,CLH_CONST,CLH_NOTES";
            var where = "CLH_ALPHA ='" + contractNumber + "'";
            var query = "SELECT " + fields + " FROM CONLINH WHERE " + where;

            var Table = RecordUtils.Table(query);
            if (Table != null)
            {
                var spec = string.Empty;
                var key = string.Empty;
                int i = 0;
                foreach (DataRow row in Table.Rows)
                {
                    i++;
                    if (row["CLH_CONST"].ToString() != key && !string.IsNullOrEmpty(key))
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                        spec = string.Empty;
                    }
                    spec += row["CLH_NOTES"].ToString();
                    key = row["CLH_CONST"].ToString();
                    if (i == Table.Rows.Count)
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                    }
                }
            }

            return notes;
        }
        #endregion       

        #region GetVariation
            public Variation GetVariation(string key)
        {
            string contractNumber = key.Substring(0, 6);
            return Variations(contractNumber).Where(p => p.Key == key).FirstOrDefault();
        }

        public Variation GetVariation(string contractNumber, string variationNumber)
        {
            return Variations(contractNumber).Where(p => p.Sequence == variationNumber).FirstOrDefault();
        }
        #endregion

        #region UpdateVariation
        public int UpdateVariation(Variation variation)
        {
            string bsnparams = string.Empty;
            bsnparams += "INVOICED=" + (variation.Invoiced ? "Y":"N") + ",";            
            return BsnUtils.UpdateVariation(variation, ref bsnparams);
        }
        #endregion
        
        #region VariationDocuments
        public List<Document> VariationDocuments(string contractNumber, string variationSeq)
        {
            /*
            var documents = Documents(contractNumber, Document.CLT_DOC_XREF_VAR, variationSeq);                       
            return documents;
            */
            return new List<Document>();
        }
        #endregion

        #region VariationEvents
        public List<VariationEvent> VariationEvents(string contractNumber, string VariationSeq)
        {
            var variationEvents = (List<VariationEvent>)VarUtils.Get(WebVars.VariationEvents);
            if (variationEvents == null && !string.IsNullOrEmpty(contractNumber) && !string.IsNullOrEmpty(VariationSeq))
            {
                variationEvents = new List<VariationEvent>();
                string fields = "CLG_CONTRACT,CLG_CONST,CLG_SEQNUM,CLG_EVENT,CLG_EVENTA,CLG_SCRKEY,";
                fields += "CLG_ACTDATE,CLG_REGDATE,CLG_REF,CLG_DESC,CLG_EMPLOYEE,CLG_FOREDATE,CLG_FOREFLAG,CLG_CHNGEVNT";
                string query = "SELECT " + fields + " FROM CONLING WHERE CLG_CONTRACT = '" + contractNumber;
                query += "' AND CLG_SEQNUM = '" + VariationSeq + "'";

                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    foreach (DataRow row in Table.Rows)
                    {
                        var Actual = (row["CLG_ACTDATE"] is DateTime) ? ((DateTime)row["CLG_ACTDATE"]) : DateTime.MinValue;
                        var Registered = (row["CLG_REGDATE"] is DateTime) ? ((DateTime)row["CLG_REGDATE"]) : DateTime.MinValue;
                        var Forecast = (row["CLG_FOREDATE"] is DateTime) ? ((DateTime)row["CLG_FOREDATE"]) : DateTime.MinValue;

                        variationEvents.Add(new VariationEvent()
                        {
                            Key = row["CLG_SCRKEY"].ToString(),
                            ContractNumber = contractNumber,
                            VariationSequence = VariationSeq,
                            EventNumber = row["CLG_EVENT"].ToString(),
                            EventNumberA = row["CLG_EVENTA"].ToString(),
                            ActualDate = Actual,
                            RegisterDate = Registered,
                            ForecastDate = Forecast,
                            Reference = row["CLG_REF"].ToString(),
                            Description = row["CLG_DESC"].ToString(),
                            Employee = row["CLG_EMPLOYEE"].ToString(),
                            ForecastFlag = row["CLG_FOREFLAG"].ToString(),
                            ChangeEvent = row["CLG_CHNGEVNT"].ToString()
                        });
                    }
                }
                if (variationEvents.Count > 0)
                {                    
                    VarUtils.Set(WebVars.VariationEvents, variationEvents);
                }
            }
            return variationEvents;
        }
        #endregion

        #region VariationEventRegister
        public void VariationEventRegister(string key)
        {
            string[] keyval = key.Split(':');
            string contractNumber = Utils.GetArray(keyval, 0);
            string variationSeq = Utils.GetArray(keyval, 1);
            string eventNumber = Utils.GetArray(keyval, 2);
            var variation = GetVariation(contractNumber, variationSeq);
            var variationEvent = GetVariationEvent(contractNumber, variationSeq, eventNumber);
            if (variationEvent == null)
            {
                Utils.SetError("Variation Event not found.");
                return;
            }

            if (variationEvent.IsRegistered)
            {
               BsnUtils.VariationEventUnregister(contractNumber, variationSeq, eventNumber);
            }
            else
            {
                BsnUtils.VariationEventRegister(contractNumber, variationSeq, eventNumber, variation.Amount,variation.CostAmount,variation.GSTAmount);
            }
        }
        #endregion

        #region UpdateVariationEvent
        public void UpdateVariationEvent(VariationEvent variationEvent)
        {
            var variation = GetVariation(variationEvent.ContractNumber, variationEvent.VariationSequence);
            if (variationEvent.IsRegistered)
            {
                BsnUtils.VariationEventRegister(variationEvent.ContractNumber, variationEvent.VariationSequence,
                variationEvent.EventNumberA, variationEvent.ActualDate.ToString("dd/MM/yyyy"), variationEvent.Employee, 
                variationEvent.Reference,variation.Amount,variation.CostAmount,variation.GSTAmount);                
            }
            else
            {
                BsnUtils.VariationEventUnregister(variationEvent.ContractNumber, variationEvent.VariationSequence, variationEvent.EventNumberA);
            }
        }
        #endregion

        #region GetVariationEvent
        public VariationEvent GetVariationEvent(string key)
        {
            string contractNumber = key.Substring(0, 6);
            string variationSeq = key.Substring(6, 3);
            return VariationEvents(contractNumber, variationSeq).Where(p => p.Key == key).FirstOrDefault();
        }

        public VariationEvent GetVariationEvent(string contractNumber,string variationSeq, string eventNumber)
        {
            return VariationEvents(contractNumber, variationSeq).Where(p => p.EventNumberA == eventNumber).FirstOrDefault();
        }
        #endregion

        #region ChecklistHeaders
        public List<ChecklistHeader> ChecklistHeaders
        {
            get
            {
                var headers = (List<ChecklistHeader>)VarUtils.Get(WebVars.ChecklistHeaders);
                if (headers == null || headers.Count == 0)
                {
                    headers = new List<ChecklistHeader>();

                    var query = "SELECT * FROM CONTBLG";
                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            headers.Add(new ChecklistHeader() {
                                Template = row["CTG_TEMPLATE"].ToString(),
                                Description = row["CTG_DESC"].ToString(),
                                Trigger = row["CTG_TRIG_EVENT"].ToString(),
                                Complete = row["CTG_CMPL_EVENT"].ToString()
                            });
                        }
                        VarUtils.Set(WebVars.ChecklistHeaders, headers);
                    }
                }
                return headers;
            }
        }
        #endregion

        #region ChecklistLines
        public List<ComboItem> ChecklistLines(string template)
        {
            var lines = (List<ComboItem>)VarUtils.Get(WebVars.ChecklistLines);
            if (VarUtils.Str("ChecklistTemplate") != template)
            {
                lines = null;
                VarUtils.Set("ChecklistTemplate", template);
            }

            if (lines == null || lines.Count == 0)
            {
                lines = GetDropDownList("CTH_GROUP", "CTH_DESC", "CONTBLH", "CTH_TEMPLATE = '" + template + "'");
                if (lines != null && lines.Count > 0)
                {
                    VarUtils.Set(WebVars.ChecklistLines, lines);
                }
                else
                {
                    lines = new List<ComboItem>();
                }
            }
            return lines;
        }
        #endregion

        #region CheckLists
        public List<Checklist> Checklists(string contractNumber)
        {
            var checklists = (List<Checklist>)VarUtils.Get(WebVars.Checklists);
            if (checklists == null && !string.IsNullOrEmpty(contractNumber))
            {
                checklists = new List<Checklist>();
                var fields = "CL_C_KEY,CL_C_SEQNUMA,CL_C_SRC_TMPL,CL_C_DESC";
                var query = "SELECT " + fields + " FROM CONLIN_C WHERE CL_C_CONSTANT = '" + contractNumber + "'";

                var table = RecordUtils.Table(query);
                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        checklists.Add(new Checklist()
                        {
                            Key = row["CL_C_KEY"].ToString(),
                            ContractNumber = contractNumber,
                            Sequence = row["CL_C_SEQNUMA"].ToString(),
                            Template = row["CL_C_SRC_TMPL"].ToString(),
                            Description = row["CL_C_DESC"].ToString()
                        });
                    }
                }
                if (checklists.Count > 0)
                {
                    VarUtils.Set(WebVars.Checklists, checklists);
                }
            }
            return checklists;
        }
        #endregion

        #region GetChecklist
        public Checklist GetChecklist(string key)
        {
            string contractNumber = key.Substring(0, 6);
            return Checklists(contractNumber).Where(p => p.Key == key).FirstOrDefault();
        }

        public Checklist Checklist(string contractNumber, string checklistSequence)
        {
            return Checklists(contractNumber).Where(p => p.Sequence == checklistSequence).FirstOrDefault();
        }
        #endregion

        #region OpCentres
        public List<ComboItem> OpCentres
        {
            get
            {
                List<ComboItem> opCentres = VarUtils.Get(WebVars.OpCentres) as List<ComboItem>;
                if (opCentres == null || opCentres.Count == 0)
                {
                    string filter = "COTY_ACTIVE<>'N'";
                    filter += Utils.GetFilterString("COTY_CODE", Utils.Settings.Comp.OpCentreFilter);

                    opCentres = GetDropDownList("COTY_CODE", "COTY_DESC", "COTABLY", filter);
                    if (opCentres != null && opCentres.Count > 0)
                    {
                        VarUtils.Set(WebVars.OpCentres, opCentres);
                    }
                    else
                    {
                        opCentres = new List<ComboItem>();
                    }
                }
                return opCentres;
            }
        }
        #endregion

        #region Districts
        public List<ComboItem> Districts
        {
            get
            {
                List<ComboItem> districts = VarUtils.Get(WebVars.Districts) as List<ComboItem>;
                if (districts == null || districts.Count == 0)
                {
                    string filter = "COTZ_ACTIVE<>'N'";
                    filter += Utils.GetFilterString("COTZ_CODE", Utils.Settings.Comp.DistrictFilter);
                    districts = GetDropDownList("COTZ_CODE", "COTZ_DESC", "COTABLZ", filter);                    
                    if (districts != null && districts.Count > 0)
                    {
                        VarUtils.Set(WebVars.Districts, districts);
                    }
                    else
                    {
                        districts = new List<ComboItem>();
                    }
                }
                return districts;
            }
        }
        #endregion

        #region Councils
        public List<ComboItem> Councils
        {
            get
            {
                List<ComboItem> councils = VarUtils.Get(WebVars.Councils) as List<ComboItem>;
                if (councils == null || councils.Count == 0)
                {
                    string filter = "COT_C_ACTIVE<>'N'";
                    filter += Utils.GetFilterString("COT_C_CODE", Utils.Settings.Comp.CouncilFilter);
                    councils = GetDropDownList("COT_C_CODE", "COT_C_DESC", "COTABL_C", filter);
                    if (councils != null && councils.Count > 0)
                    {
                        VarUtils.Set(WebVars.Councils, councils);
                    }
                    else
                    {
                        councils = new List<ComboItem>();
                    }                    
                }
                return councils;
            }
        }
        #endregion

        #region Employees
        public List<ComboItem> Employees
        {
            get
            {
                var employees = (List<ComboItem>)VarUtils.Get(WebVars.Employees);
                if (employees == null || employees.Count == 0)
                {
                    string filter = "COTG_ACTIVE <> 'N'";
                    employees = GetDropDownList("COTG_CODE", "COTG_NAME", "COTABLG", filter, "COTG_CATCODE");
                    if (employees != null && employees.Count > 0)
                    {
                        VarUtils.Set(WebVars.Employees, employees);
                    }
                    else
                    {
                        employees = new List<ComboItem>();
                    }
                }
                return employees;
            }
        }
        #endregion

        #region SalesConsultants
        public List<ComboItem> SalesConsultants
        {
            get
            {
                var salesConsultants = (List<ComboItem>)VarUtils.Get(WebVars.SalesConsultants);
                if (salesConsultants == null || salesConsultants.Count == 0)
                {
                    string filter = "COTG_ACTIVE <> 'N'";
                    filter += Utils.GetFilterString("COTG_CATCODE", Utils.Settings.Comp.SalesConsultantCategories, 1);
                    salesConsultants = GetDropDownList("COTG_CODE", "COTG_NAME", "COTABLG", filter);

                    if (salesConsultants != null && salesConsultants.Count > 0)
                    {
                        VarUtils.Set(WebVars.SalesConsultants, salesConsultants);
                    }
                    else
                    {
                        salesConsultants = new List<ComboItem>();
                    }
                }
                return salesConsultants;
            }
        }
        #endregion

        #region States
        public List<ComboItem> States
        {
            get
            {
                List<ComboItem> states = VarUtils.Get(WebVars.States) as List<ComboItem>;
                if (states == null || states.Count == 0)
                {
                    states = GetDropDownList("COT_J_CODE", "COT_J_DESC", "COTABL_J");
                    if (states != null && states.Count > 0)
                    {
                        VarUtils.Set(WebVars.States, states);
                    }
                    else
                    {
                        states = new List<ComboItem>
                        {
                            new ComboItem("NSW", "New South Wales"),
                            new ComboItem("VIC", "Victoria"),
                            new ComboItem("QLD", "Queensland"),
                            new ComboItem("SA", "South Australia"),
                            new ComboItem("ACT", "Australian Capital Territory"),
                            new ComboItem("WA", "Western Australia"),
                            new ComboItem("TAS", "Tasmania"),
                            new ComboItem("NT", "Northern Territory")
                        };
                    }
                }
                return states;
            }
        }
        #endregion

        #region SalesPromotions
        public List<ComboItem> SalesPromotions
        {
            get
            {
                List<ComboItem> salesPromotions = VarUtils.Get(WebVars.SalesPromotions) as List<ComboItem>;
                if (salesPromotions == null || salesPromotions.Count == 0)
                {
                    string filter = "COT_B_ACTIVE <> 'N'";
                    filter += Utils.GetFilterString("COT_B_CODE", Utils.Settings.Comp.PromotionFilter);
                    salesPromotions = GetDropDownList("COT_B_CODE", "COT_B_DESC", "COTABL_B", filter);
                    if (salesPromotions != null && salesPromotions.Count > 0)
                    {
                        VarUtils.Set(WebVars.SalesPromotions, salesPromotions);
                    }
                    else
                    {
                        salesPromotions = new List<ComboItem>();
                    }
                }
                return salesPromotions;
            }
        }
        #endregion

        #region SalesCentres
        public List<ComboItem> SalesCentres
        {
            get
            {
                List<ComboItem> salesCentres = VarUtils.Get(WebVars.SalesCentres) as List<ComboItem>;
                if (salesCentres == null || salesCentres.Count == 0)
                {
                    string filter = "COT_P_ACTIVE <> 'N'";
                    filter += Utils.GetFilterString("COT_P_CODE", Utils.Settings.Comp.SaleCentreFilter);
                    salesCentres = GetDropDownList("COT_P_CODE", "COT_P_DESC", "COTABL_P", filter);
                    if (salesCentres != null && salesCentres.Count > 0)
                    {
                        VarUtils.Set(WebVars.SalesCentres, salesCentres);
                    }
                    else
                    {
                        salesCentres = new List<ComboItem>();
                    }
                }
                return salesCentres;
            }
        }
        #endregion

        #region StatusCodes
        public List<ComboItem> StatusCodes
        {
            get
            {
                List<ComboItem> statusCodes = VarUtils.Get(WebVars.StatusCodes) as List<ComboItem>;
                if (statusCodes == null || statusCodes.Count == 0)
                {
                    string filter = "COT_M_ACTIVE<>'N'";
                    statusCodes = GetDropDownList("COT_M_CODE", "COT_M_DESC", "COTABL_M", filter);
                    if (statusCodes != null && statusCodes.Count > 0)
                    {
                        VarUtils.Set(WebVars.StatusCodes, statusCodes);
                    }
                    else
                    {
                        statusCodes = new List<ComboItem>();
                    }
                }
                return statusCodes;
            }
        }

        List<ComboItem> contractStatusCodes;
        public List<ComboItem> ContractStatusCodes
        {
            get
            {
                if (contractStatusCodes == null)
                {
                    contractStatusCodes = new List<ComboItem>();
                    foreach (var status in Utils.Settings.Comp.ActiveContractStatusList)
                    {
                        var code = Utils.RemoveDefaultFormat(status);
                        if (StatusCodes.Any(p => p.Code.Equals(code)))
                        {
                            var statusCode = StatusCodes.Where(p => p.Code.Equals(code)).FirstOrDefault();
                            contractStatusCodes.Add(statusCode);
                        }
                    }
                }
                return contractStatusCodes;
            }
        }

        List<ComboItem> leadStatusCodes;
        public List<ComboItem> LeadStatusCodes
        {
            get
            {
                if (leadStatusCodes == null)
                {
                    leadStatusCodes = new List<ComboItem>();
                    foreach (var status in Utils.Settings.Comp.ActiveLeadStatusList)
                    {
                        var code = Utils.RemoveDefaultFormat(status);
                        if (StatusCodes.Any(p => p.Code.Equals(code)))
                        {
                            var statusCode = StatusCodes.Where(p => p.Code.Equals(code)).FirstOrDefault();
                            leadStatusCodes.Add(statusCode);
                        }
                    }
                }
                return leadStatusCodes;
            }
        }
        #endregion

        #region SaleTypes
        public List<ComboItem> SaleTypes
        {
            get
            {
                var saleTypes = (List<ComboItem>)VarUtils.Get(WebVars.SaleTypes);
                if (saleTypes == null || saleTypes.Count == 0)
                {
                    string filter = "COTU_ACTIVE <> 'N'";
                    filter += Utils.GetFilterString("COTU_CODE", Utils.Settings.Comp.SaleTypeFilter);
                    saleTypes = GetDropDownList("COTU_CODE", "COTU_DESC", "COTABLU", filter);
                    if (saleTypes != null && saleTypes.Count > 0)
                    {
                        VarUtils.Set(WebVars.SaleTypes, saleTypes);
                    }
                    else
                    {
                        saleTypes = new List<ComboItem>();
                    }
                }
                return saleTypes;
            }
        }
        #endregion

        #region SaleGroups
        public List<ComboItem> SaleGroups
        {
            get
            {
                var saleGroups = VarUtils.Get(WebVars.SaleGroups) as List<ComboItem>;
                if (saleGroups == null || saleGroups.Count == 0)
                {
                    string filter = "COTV_ACTIVE <> 'N' AND COTV_CODE <> '" + Settings.VariationSalesGroup + "'";
                    saleGroups = GetDropDownList("COTV_CODE", "COTV_DESC", "COTABLV", filter);
                    if (saleGroups != null && saleGroups.Count > 0)
                    {
                        VarUtils.Set(WebVars.SaleGroups, saleGroups);
                    }
                    else
                    {
                        saleGroups = new List<ComboItem>();
                    }
                }
                return saleGroups;
            }
        }
        #endregion
                
        #region ContractsByOperatingCentre
        private List<ChartData> contractsByOperatingCentre;
        public List<ChartData> ContractsByOperatingCentre
        {
            get
            {
                if (contractsByOperatingCentre == null)
                {
                    contractsByOperatingCentre = new List<ChartData>();
                    foreach (var contract in Contracts)
                    {
                        string operatingCentre = string.Empty;
                        if (string.IsNullOrEmpty(contract.OpCentre))
                        {
                            operatingCentre = "[ Blank ]";
                        }
                        else
                        {
                            try
                            {
                                operatingCentre = OpCentres.Find(p => p.Code == contract.OpCentre).Desc;
                            }
                            catch
                            {
                                operatingCentre = "Unknown: " + contract.OpCentre;
                            }
                        }
                        contractsByOperatingCentre.Increment(operatingCentre);
                    }
                }
                return contractsByOperatingCentre;
            }
        }
        #endregion

        #region EventCodes
        public List<ComboItem> EventCodes
        {
            get
            {
                var eventCodes = (List<ComboItem>)VarUtils.Get(WebVars.EventCodes);
                if (eventCodes == null || eventCodes.Count == 0)
                {
                    string filter = "COTW_ACTIVE <> 'N'";
                    eventCodes = GetDropDownList("COTW_CODE", "COTW_DESC", "COTABLW", filter);
                    if (eventCodes != null && eventCodes.Count > 0)
                    {
                        VarUtils.Set(WebVars.EventCodes, eventCodes);
                    }
                    else
                    {
                        eventCodes = new List<ComboItem>();
                    }
                }
                return eventCodes;
            }
        }

        public ComboItem GetEventCode(string eventNumber)
        {
            return EventCodes.Where(p => p.Code == eventNumber).FirstOrDefault();
        }
        #endregion
    }
}