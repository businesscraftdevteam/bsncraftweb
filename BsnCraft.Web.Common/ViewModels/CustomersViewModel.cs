﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BsnCraft.Web.Common.Classes.Grid;

namespace BsnCraft.Web.Common.ViewModels
{
    public class CustomersViewModel : ViewModelBase
    {
        const string BookmarksKey = "Customers.Bookmarks";
        const string ContactBmKey = "Contacts.Bookmarks";
                
        #region Customers
        public List<dynamic> Customers
        {
            get
            {
                var customers = (List<dynamic>)VarUtils.Get(WebVars.Customers);
                if (customers == null || customers.Count == 0)
                {
                    customers = new List<dynamic>();
                    
                    var fields = "CUSNO,CM_NAME,ADD1,ADD2,ADD3,ADD4,CITY,STATE,ZIP,PHONE,EMLA_ADDRESS";
                    var join = " LEFT OUTER JOIN IVCTBLU ON CUSNO = IVTU_CUSTNO";
                    join += " LEFT OUTER JOIN EMLADDB ON IVTU_SCRKEY = EMLA_REFCODE";
                    var query = "SELECT " + fields + " FROM CUSMASA " + join + " WHERE {fn LENGTH(CUSNO)} > 0";
                    query += " AND IVTU_SEQNUM = (SELECT MIN(IVTU_SEQNUM) FROM IVCTBLU WHERE IVTU_CUSTNO = CUSNO)";

                    if (!string.IsNullOrEmpty(Utils.Settings.Comp.CustomerCode))
                    {
                        query += " AND CUSCD = '" + Utils.Settings.Comp.CustomerCode + "'";
                    }

                    if (!string.IsNullOrEmpty(Utils.Settings.Comp.CustomerQuery))
                    {
                        query += " " + Utils.Settings.Comp.CustomerQuery;
                    }

                    DataTable Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            string email = Utils.CleanSpec(row["EMLA_ADDRESS"].ToString());
                            customers.Add(new Customer()
                            {
                                CustomerNumber = row["CUSNO"].ToString(),
                                Name = row["CM_NAME"].ToString(),
                                Address1 = row["ADD1"].ToString(),
                                Address2 = row["ADD2"].ToString(),
                                Address3 = row["ADD3"].ToString(),
                                Address4 = row["ADD4"].ToString(),
                                Suburb = row["CITY"].ToString(),
                                State = row["STATE"].ToString(),
                                PostCode = row["ZIP"].ToString(),
                                Phone = row["PHONE"].ToString(),
                                Email = email,
                                IsBookmarked = Bookmarks.Contains(row["CUSNO"].ToString())
                            });
                        }
                    }

                    VarUtils.Set(WebVars.Customers, customers);
                }
                return customers;
            }
        }
        #endregion

        #region CustomersGridView
        public BsnGridViewModel CustomersGridView(bool showContacts)
        {
            var grid = (BsnGridViewModel)VarUtils.Get(WebVars.CustomersGrid);

            if (grid == null)
            {
                var key = showContacts ? "CustomerContacts" : "Customers";
                var keyfield = showContacts ? "Key" : "CustomerNumber";
                var title = showContacts ? "Contacts" : "Customers";
                grid = new BsnGridViewModel(key, keyfield, title)
                {
                    DataSource = showContacts ? CustomerContacts : Customers,
                    ShowPager = true,
                    ShowSearch = true            
                };

                grid.OptionsColumn.FieldName = "CustomerNumber";
                var button = BookmarkButton(showContacts);
                grid.OptionsColumn.Buttons.Add(button);

                grid.KeyColumn.FieldName = "CustomerNumber";
                grid.KeyColumn.Caption = "Customer";

                var buttonType = Utils.Settings.Comp.TouchMode ? ButtonTypes.Primary : ButtonTypes.Link;
                button = new BsnGridButton(new BsnCallback()
                {
                    Type = CallbackType.WindowLocation,
                    Key = Menus.Customers_Details.ToString(),
                    Value = "?key=[cellvalue]"
                }
                , "fal fa-external-link-square", "[cellvalue]", true, buttonType);
                grid.KeyColumn.Buttons.Add(button);

                int sort = 0;
                grid.Columns.Add(new BsnGridColumn() { Name = "Name", FieldName = "FriendlyName", Caption = "Name", Sort = sort++ });
                grid.Columns.Add(new BsnGridColumn() { Name = "Address", FieldName = "FullAddress", Caption = "Address", Sort = sort++ });
                grid.Columns.Add(new BsnGridColumn() { Name = "Phone", FieldName = "Phone", Caption = "Phone", Sort = sort++ });
                grid.Columns.Add(new BsnGridColumn() { Name = "Email", FieldName = "Email", Caption = "Email", Sort = sort++ });
                
                VarUtils.Set(WebVars.CustomersGrid, grid);
            }

            return grid;
        }

        private BsnGridButton BookmarkButton(bool showContacts)
        {
            var callback = new BsnCallback() { Mode = CallbackMode.Bookmark };
            callback.Id = showContacts ? BsnControls.Grid_Contacts : BsnControls.Grid_Customers;
            var button = new BsnGridButton(callback, "fal fa-bookmark fa-fw", string.Empty, true, ButtonTypes.Link);
            if (showContacts)
            {
                button.CellPrepared += Contacts_Bookmark;
            }
            else
            {
                button.CellPrepared += Customers_Bookmark;
            }
            return button;
        }

        private void Contacts_Bookmark(object sender, EventArgs e)
        {
            var key = ((BsnEventArgs)e).KeyValue;
            var button = (BsnGridButton)sender;
            button.Callback.Key = key;
            var customer = GetContact(key);
            button.Icon = customer.IsBookmarked ? "fas fa-bookmark fa-fw" : "fal fa-bookmark fa-fw";
        }

        private void Customers_Bookmark(object sender, EventArgs e)
        {
            var key = ((BsnEventArgs)e).KeyValue;
            var button = (BsnGridButton)sender;
            button.Callback.Key = key;
            var customer = GetCustomer(key);
            button.Icon = customer.IsBookmarked ? "fas fa-bookmark fa-fw" : "fal fa-bookmark fa-fw";
        }
        #endregion

        #region LookupCustomers
        public List<SearchData> LookupCustomers(string query)
        {
            var match = Customers.Where(p => p.CustomerNumber.Contains(query) ||
                p.FriendlyName.ToLower().Contains(query) ||
                p.Email.ToLower().Contains(query) ||
                p.Phone.Contains(query) ||
                p.FullAddress.ToLower().Contains(query)).Take(10);

            var results = new List<SearchData>();
            foreach (var item in match)
            {
                var value = item.CustomerNumber + " (" + item.FriendlyName + " ";
                value += item.Email + " " + item.Phone + " " + item.FullAddress + ")";
                results.Add(new SearchData() { Value = value, Data = JsonConvert.SerializeObject(item) });
            }
            return results;
        }
        #endregion

        #region CustomerContacts
        public List<dynamic> CustomerContacts
        {
            get
            {
                var contacts = (List<dynamic>)VarUtils.Get(WebVars.CustomerContacts);
                if (contacts == null)
                {
                    contacts = new List<dynamic>();                    
                    var fields = "CUSNO,CM_NAME,IVTU_CUSTNO,IVTU_SEQNUM,IVTU_SCRKEY,IVTU_TITLE,IVTU_SURNAME,IVTU_FIRSTNAME";
                    fields += ",IVTU_ADDRESS,IVTU_ADDRESS2,IVTU_ADDRESS3,IVTU_ADDRESS4,IVTU_SUBURB,IVTU_STATE,IVTU_ZIP";
                    fields += ",IVTU_PHHOME,IVTU_PHWORK,IVTU_MOBILE,IVTU_FAX,EMLA_ADDRESS";
                    var join = "JOIN CUSMASA ON CUSNO = IVTU_CUSTNO";
                    join += " LEFT OUTER JOIN EMLADDB ON IVTU_SCRKEY = EMLA_REFCODE";
                    var query = "SELECT " + fields + " FROM IVCTBLU " + join + " WHERE {fn LENGTH(CUSNO)} > 0";

                    if (!string.IsNullOrEmpty(Utils.Settings.Comp.CustomerCode))
                    {
                        query += " AND CUSCD = '" + Utils.Settings.Comp.CustomerCode + "'";
                    }

                    if (!string.IsNullOrEmpty(Utils.Settings.Comp.ContactsQuery))
                    {
                        query += " " + Utils.Settings.Comp.ContactsQuery;
                    }

                    DataTable Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            string email = Utils.CleanSpec(row["EMLA_ADDRESS"].ToString());

                            contacts.Add(new CustomerContact()
                            {
                                Key = row["IVTU_SCRKEY"].ToString(),
                                CustomerNumber = row["CUSNO"].ToString(),
                                Name = row["CM_NAME"].ToString(),
                                Sequence = int.Parse(row["IVTU_SEQNUM"].ToString()).ToString("000"),
                                Title = row["IVTU_TITLE"].ToString(),
                                LastName = row["IVTU_SURNAME"].ToString(),
                                FirstName = row["IVTU_FIRSTNAME"].ToString(),
                                Address1 = row["IVTU_ADDRESS"].ToString(),
                                Address2 = row["IVTU_ADDRESS2"].ToString(),
                                Address3 = row["IVTU_ADDRESS3"].ToString(),
                                Address4 = row["IVTU_ADDRESS4"].ToString(),
                                Suburb = row["IVTU_SUBURB"].ToString(),
                                State = row["IVTU_STATE"].ToString(),
                                PostCode = row["IVTU_ZIP"].ToString(),
                                HomePhone = row["IVTU_PHHOME"].ToString(),
                                WorkPhone = row["IVTU_PHWORK"].ToString(),
                                MobilePhone = row["IVTU_MOBILE"].ToString(),
                                FaxNumber = row["IVTU_FAX"].ToString(),
                                Email = email,
                                IsBookmarked = ContactBookmarks.Contains(row["IVTU_SCRKEY"].ToString())
                            });
                        }                        
                    }
                    VarUtils.Set(WebVars.CustomerContacts, contacts);
                }
                return contacts;
            }
        }
        #endregion       

        #region GetCustomer - Customers Collection
        public List<dynamic> GetCustomers(string customerNumber)
        {
            var customer = Customers.Where(p => p.CustomerNumber == customerNumber);            
            return customer.ToList();
        }

        public Customer GetCustomer(string customerNumber)
        {
            var customer = Customers.Where(p => p.CustomerNumber == customerNumber);
            return customer.FirstOrDefault();
        }
        #endregion

        #region GetContacts - Contacts Collection
        public List<dynamic> GetContacts(string customerNumber)
        {
            var customer = CustomerContacts.Where(p => p.CustomerNumber == customerNumber);
            return customer.ToList();
        }
        #endregion

        #region GetContact - Contacts Collection
        public CustomerContact GetContact(string customerNumber, string contactSequence, bool getfirst)
        {
            var contacts = CustomerContacts.Where(p => p.CustomerNumber == customerNumber).OrderBy(p => p.Sequence);

            if (contacts == null)
                return null;
            
            if (string.IsNullOrEmpty(contactSequence) && getfirst)
                return contacts.FirstOrDefault();

            var contact = contacts.Where(p => p.Sequence == contactSequence);

            if (contact == null)
                return null;

            return contact.FirstOrDefault();
        }

        public CustomerContact GetContact(string key)
        {
            var contact = CustomerContacts.Where(p => p.Key == key).FirstOrDefault();
            return contact;
        }
        #endregion

        #region UpdateCustomer
        public void UpdateCustomer(CustomerContact contact)
        {
            bool isNew = (string.IsNullOrEmpty(contact.CustomerNumber));
            string key = BsnUtils.UpdateCustomer(contact);
            if (isNew && !string.IsNullOrEmpty(key))
            {
                var newCustomer = GetCustomer(key);
                if (newCustomer == null)
                {
                    Utils.SetError("Error finding new key: " + key);
                }
                else
                {
                    string urlextras = "?key=" + newCustomer.CustomerNumber;
                    var location = Utils.GetMenu(Menus.Customers_Details).ResolveUrl + urlextras;
                    VarUtils.Set(WebVars.JSLocation, location);
                }
            }
        }
        #endregion

        #region DeleteContact
        public void DeleteContact(string customerNumber, int sequence)
        {
            BsnUtils.DeleteContact(customerNumber, sequence);
        }
        #endregion

        #region CustomerTitles
        public List<ComboItem> CustomerTitles
        {
            get
            {
                List<ComboItem> customerTitles = VarUtils.Get(WebVars.CustomerTitles) as List<ComboItem>;
                if (customerTitles == null || customerTitles.Count == 0)
                {
                    customerTitles = GetDropDownList("COTC_CODE", "COTC_DESC", "COTABLC");
                    if (customerTitles != null && customerTitles.Count > 0)
                    {
                        VarUtils.Set(WebVars.CustomerTitles, customerTitles);
                    }
                    else
                    {
                        customerTitles = new List<ComboItem>();
                    }
                }
                return customerTitles;
            }
        }
        #endregion

        #region States
        public List<ComboItem> States
        {
            get
            {
                List<ComboItem> states = VarUtils.Get(WebVars.States) as List<ComboItem>;
                if (states == null || states.Count == 0)
                {
                    states = GetDropDownList("COT_J_CODE", "COT_J_DESC", "COTABL_J");
                    if (states != null && states.Count > 0)
                    {
                        VarUtils.Set(WebVars.States, states);
                    }
                    else
                    {
                        states = new List<ComboItem>
                        {
                            new ComboItem("NSW", "New South Wales"),
                            new ComboItem("VIC", "Victoria"),
                            new ComboItem("QLD", "Queensland"),
                            new ComboItem("SA", "South Australia"),
                            new ComboItem("ACT", "Australian Capital Territory"),
                            new ComboItem("WA", "Western Australia"),
                            new ComboItem("TAS", "Tasmania"),
                            new ComboItem("NT", "Northern Territory")
                        };
                    }
                }
                return states;
            }
        }
        #endregion

        #region Bookmarks
        public List<string> Bookmarks
        {
            get
            {
                var bookmarks = (List<string>)VarUtils.Get(WebVars.BookmarkedCustomers);
                if (bookmarks == null)
                {
                    string keyvals = string.Empty;
                    try
                    {
                        keyvals = BsnUtils.GetSettings(SettingsLevel.User, BookmarksKey);
                        bookmarks = (List<string>)JsonConvert.DeserializeObject(keyvals, typeof(List<string>));
                        if (bookmarks == null)
                        {
                            bookmarks = new List<string>();
                        }                        
                    }
                    catch (Exception e)
                    {
                        Utils.LogExc(e);
                        bookmarks = new List<string>();
                    }
                    VarUtils.Set(WebVars.BookmarkedCustomers, bookmarks);
                }
                return bookmarks;
            }
        }
        #endregion

        #region Bookmark
        public void Bookmark(string customerNumber)
        {
            var Customer = GetCustomer(customerNumber);
            if (Bookmarks.Contains(customerNumber))
            {
                Bookmarks.Remove(customerNumber);
                Customer.IsBookmarked = false;
            }
            else
            {
                Bookmarks.Add(customerNumber);
                Customer.IsBookmarked = true;
            }

            string keyvals = JsonConvert.SerializeObject(Bookmarks);
            BsnUtils.SaveSettings(SettingsLevel.User, BookmarksKey, keyvals);

            string message = (Customer.IsBookmarked) ? "Added" : "Removed";
            message += " customer bookmark: " + customerNumber;
            Utils.SetSuccess(message);
                
        }
        #endregion

        #region ContactBookmarks
        public List<string> ContactBookmarks
        {
            get
            {
                var bookmarks = (List<string>)VarUtils.Get(WebVars.BookmarkedContacts);
                if (bookmarks == null)
                {
                    string keyvals = string.Empty;
                    try
                    {
                        keyvals = BsnUtils.GetSettings(SettingsLevel.User, ContactBmKey);
                        bookmarks = (List<string>)JsonConvert.DeserializeObject(keyvals, typeof(List<string>));
                        if (bookmarks == null)
                        {
                            bookmarks = new List<string>();
                        }                        
                    }
                    catch (Exception e)
                    {
                        Utils.LogExc(e);
                        bookmarks = new List<string>();
                    }
                    VarUtils.Set(WebVars.BookmarkedContacts, bookmarks);
                }
                return bookmarks;
            }
        }
        #endregion

        #region BookmarkContact
        public void BookmarkContact(string key)
        {
            var Contact = GetContact(key);
            if (ContactBookmarks.Contains(key))
            {
                ContactBookmarks.Remove(key);
                Contact.IsBookmarked = false;
            }
            else
            {
                ContactBookmarks.Add(key);
                Contact.IsBookmarked = true;
            }
            
            string keyvals = JsonConvert.SerializeObject(ContactBookmarks);
            BsnUtils.SaveSettings(SettingsLevel.User, ContactBmKey, keyvals);

            string message = (Contact.IsBookmarked) ? "Added" : "Removed";
            message += " contact bookmark: " + key;
            Utils.SetSuccess(message);
        }
        #endregion

    }
}