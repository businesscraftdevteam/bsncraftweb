﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BsnCraft.Web.Common.ViewModels
{
    public class DesktopViewModel : ViewModelBase
    {
        const string PanelsKey = "Desktop.Panels";
        public string BookmarkedCustomers = "BookmarkedCustomers";
        public string BookmarkedContacts = "BookmarkedContacts";
        public string BookmarkedLeads = "BookmarkedLeads";
        public string BookmarkedContracts = "BookmarkedContracts";
        public string Activities = "Activities";

        #region ChartsVM
        private ChartsViewModel chartsVM;
        private ChartsViewModel ChartsVM
        {
            get
            {
                if (chartsVM == null)
                {
                    chartsVM = new ChartsViewModel();
                }
                return chartsVM;
            }
        }
        #endregion
        
        #region AllPanels
        private List<DockPanel> allPanels;
        public List<DockPanel> AllPanels
        {
            get
            {
                allPanels = (List<DockPanel>)VarUtils.Get(WebVars.DesktopPanels);
                if (allPanels == null)
                {                    
                    GetAllPanels();
                    CheckDefaults();
                    BookmarkedCharts();
                    VarUtils.Set(WebVars.DesktopPanels, allPanels);
                }
                return allPanels.OrderBy(p => p.VisibleIndex).ToList();
            }
        }
        #endregion
        
        #region GetAllPanels
        public void GetAllPanels()
        {
            if (!BsnUtils.Connected)
                return;

            try
            {
                string JsonPanels = BsnUtils.GetSettings(SettingsLevel.User, PanelsKey);                
                if (!string.IsNullOrEmpty(JsonPanels))
                {
                    allPanels = (List<DockPanel>)JsonConvert.DeserializeObject(JsonPanels, typeof(List<DockPanel>));
                    return;
                }
            }
            catch (Exception e)
            {
                Utils.LogExc(e, true);
            }
            allPanels = new List<DockPanel>();
        }
        #endregion

        #region CheckDefaults
        int visibleIndex = -1;
        protected void CheckDefaults()
        {
            if (allPanels.FindIndex(p => p.Name == BookmarkedCustomers) < 0)
            {
                allPanels.Add(new DockPanel()
                {
                    Name = BookmarkedCustomers,
                    Caption = "My Customers",
                    Type = PanelTypes.Bookmarks.ToString(),
                    VisibleIndex = visibleIndex++
                });
            };
            
            if (allPanels.FindIndex(p => p.Name == BookmarkedContacts) < 0)
            {                
                allPanels.Add(new DockPanel()
                {
                    Name = BookmarkedContacts,
                    Caption = "My Contacts",
                    Type = PanelTypes.Bookmarks.ToString(),
                    VisibleIndex = visibleIndex++
                });
            }
            
            if (allPanels.FindIndex(p => p.Name == BookmarkedLeads) < 0)
            {
                if (Utils.ShowLeads)
                {
                    allPanels.Add(new DockPanel()
                    {
                        Name = BookmarkedLeads,
                        Caption = "My Leads",
                        Type = PanelTypes.Bookmarks.ToString(),
                        VisibleIndex = visibleIndex++
                    });
                }
            }

            if (allPanels.FindIndex(p => p.Name == BookmarkedContracts) < 0)
            {
                if (Utils.ShowContracts)
                {
                    allPanels.Add(new DockPanel()
                    {
                        Name = BookmarkedContracts,
                        Caption = "My Contracts",
                        Type = PanelTypes.Bookmarks.ToString(),
                        VisibleIndex = visibleIndex++
                    });
                }
            }

            if (allPanels.FindIndex(p => p.Name == Activities) < 0)
            {
                allPanels.Add(new DockPanel()
                {
                    Name = Activities,
                    Caption = "My Activities",
                    Type = PanelTypes.Activities.ToString(),
                    VisibleIndex = visibleIndex++
                });
            }
        }
        #endregion

        #region BookmarkedCharts
        private void BookmarkedCharts()
        {
            foreach (var chart in ChartsVM.Charts)
            {
                string panelName = "Chart" + chart.Name;
                int index = allPanels.FindIndex(p => p.Name == panelName);
                if (chart.IsBookmarked)
                {
                    if (index < 0)
                    {
                        allPanels.Add(new DockPanel()
                        {
                            Name = panelName,
                            Caption = "Chart: " + chart.Name,
                            Type = PanelTypes.Charts.ToString(),
                            VisibleIndex = visibleIndex++
                        });
                    }
                }
                else
                {
                    if (index >= 0)
                    {
                        DeletePanel(allPanels[index]);
                    }
                }
            }
        }
        #endregion
        
        #region SavePanels
        public void SavePanels()
        {
            var JsonPanels = JsonConvert.SerializeObject(AllPanels);
            BsnUtils.SaveSettings(SettingsLevel.User, PanelsKey, JsonPanels);
            VarUtils.Set(WebVars.DesktopPanels, null);
        }
        #endregion

        #region DeletePanel
        public void DeletePanel(DockPanel panel)
        {
            try
            {
                allPanels.Remove(panel);
            }
            catch (Exception e)
            {
                Utils.LogExc(e);
            }
        }
        #endregion

        #region SaveLayout
        public void SaveLayout(BsnCallback Callback)
        {
            var panels = (List<DockPanel>)JsonConvert.DeserializeObject(Callback.Value, typeof(List<DockPanel>));
            foreach (var panelData in panels)
            {
                var panel = AllPanels.Where(p => p.Name == panelData.Name).FirstOrDefault();
                if (panel != null)
                {
                    panel.ZoneId = panelData.ZoneId;
                    panel.Visible = panelData.Visible;
                    panel.VisibleIndex = panelData.VisibleIndex;
                }
            }
            SavePanels();

        }
        #endregion

        #region ResetLayout
        public void ResetLayout()
        {
            BsnUtils.SaveSettings(SettingsLevel.User, PanelsKey, string.Empty);
            VarUtils.Set(WebVars.DesktopPanels, null);
        }
        #endregion

        #region UpdatePanel
        public void UpdatePanel(BsnCallback Callback)
        {
            var panel = AllPanels.Where(p => p.Name == Callback.Value).FirstOrDefault();
            if (panel != null)
            {
                panel.Visible = bool.Parse(Callback.GetAttr("visible"));
                SavePanels();
            }
        }
        #endregion
        
    }
}
