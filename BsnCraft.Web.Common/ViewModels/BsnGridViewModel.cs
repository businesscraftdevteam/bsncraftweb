﻿using BsnCraft.Web.Common.Classes.Grid;
using DevExpress.Web;
using System.Collections.Generic;

namespace BsnCraft.Web.Common.ViewModels
{

    public class BsnGridViewModel
    {
        public string Key { get; private set; }
        public string Title { get; private set; }
        public string KeyField { get; private set; }

        public BsnGridViewModel(string key, string keyfield, string title = "")
        {
            Key = key;
            if (string.IsNullOrEmpty(title))
            {
                Title = key;
            }
            else
            {
                Title = title;
            }
            KeyField = keyfield;
            Columns = new List<BsnGridColumn>();
            Filters = new List<BsnGridFilter>();
            FormatConditions = new List<GridViewFormatConditionHighlight>();
        }
        

        public List<dynamic> DataSource { get; set; }

        //TODO: Move below to settings
        public bool ShowSearch { get; set; }
        public bool ShowPager { get; set; }
        public bool ShowStripes { get; set; }
        public bool ShowLines { get; set; }

        public List<BsnGridColumn> Columns { get; private set; }
        public List<BsnGridFilter> Filters { get; private set; } 
        public List<GridViewFormatConditionHighlight> FormatConditions { get; private set; }

        #region Settings
        private List<BsnGridSetting> settings;
        public List<BsnGridSetting> Settings {
            get
            {
                if (settings == null)
                {
                    //Defaults
                    settings = new List<BsnGridSetting>();
                    settings.Add(new BsnGridSetting() { Name = "ShowFilters", Caption = "Show Filters", Sort = 1 });                    
                }
                return settings;
            }
            set
            {
                if (value != settings)
                {
                    //Allow custom
                    settings = value;
                }
            }
        }
        #endregion

        #region Options
        public bool ShowOptions { get { return (!string.IsNullOrEmpty(OptionsColumn.FieldName)); } }

        private BsnGridColumn optionsColumn;
        public BsnGridColumn OptionsColumn
        {
            get
            {
                if (optionsColumn == null)
                {
                    optionsColumn = new BsnGridColumn() { Name = "Options" };
                }
                return optionsColumn;
            }
        }

        private BsnGridTemplate optionsTemplate;
        public BsnGridTemplate OptionsTemplate
        {
            get
            {
                if (optionsTemplate == null)
                {
                    optionsTemplate = new BsnGridTemplate(OptionsColumn);
                }
                return optionsTemplate;
            }
        }
        #endregion

        #region Key
        //Key field will not be able to be hidden in column selection

        public bool ShowKey { get { return (!string.IsNullOrEmpty(KeyColumn.FieldName)); } }

        private BsnGridColumn keyColumn;
        public BsnGridColumn KeyColumn
        {
            get
            {
                if (keyColumn == null)
                {
                    keyColumn = new BsnGridColumn() { Name = "Key" };
                }
                return keyColumn;
            }
        }

        private BsnGridTemplate keyTemplate;
        public BsnGridTemplate KeyTemplate
        {
            get
            {
                if (keyTemplate == null)
                {
                    keyTemplate = new BsnGridTemplate(KeyColumn);
                }
                return keyTemplate;
            }
        }
        #endregion

        public PageModes Mode { get; set; } = PageModes.Grid;
    }   

}
