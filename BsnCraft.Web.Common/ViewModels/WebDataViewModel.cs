﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BsnCraft.Shared.DTO.Classes;
using BsnCraft.Shared.DTO.View;
using Newtonsoft.Json;

namespace BsnCraft.Web.Common.ViewModels
{
    public class WebDataViewModel : ViewModelBase
    {
        public void FinanceData()
        {
            var request = new DtoRequest();
            request.Command = "financeView";
            request.AddKey(new KeyInfo("001015"));
            var strRequest = JsonConvert.SerializeObject(request);
            var strResponse = string.Empty;
            if (BsnUtils.WebData(ref strRequest, ref strResponse) == 0)
            {
                Finance data = (Finance)JsonConvert.DeserializeObject(strResponse, typeof(Finance));
                var contractValue = data.Contracts.Where(p => p.ContractNumber.Equals("001015")).FirstOrDefault().Value;
            }
        }
    }
}
