﻿using BsnCraft.Web.Common.Classes;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace BsnCraft.Web.Common.ViewModels
{
    public class DataViewViewModel : ViewModelBase
    {
        private string DataViewsKey = "DataViews";

        #region DataViews
        public List<BsnDataView> DataViews
        {
            get
            {
                var dataViews = (List<BsnDataView>)VarUtils.Get(WebVars.DataViews);
                if (dataViews == null)
                {
                    var settings = BsnUtils.GetSettings(SettingsLevel.Company, DataViewsKey);
                    dataViews = (List<BsnDataView>)JsonConvert.DeserializeObject(settings, typeof(List<BsnDataView>));
                    if (dataViews != null && dataViews.Count > 0)
                    {                            
                        VarUtils.Set(WebVars.DataViews, dataViews);
                    }
                    if (dataViews == null)
                    {
                        dataViews = new List<BsnDataView>();
                    }
                }
                return dataViews;
            }
        }
        #endregion

        #region Save
        public void Save(BsnDataView dataView)
        {
            var dataViews = DataViews;

            if (!dataViews.Contains(dataView))
            {
                dataViews.Add(dataView);
                VarUtils.Set(WebVars.DataViews, dataViews);
            }

            var settings = JsonConvert.SerializeObject(dataViews);
            BsnUtils.SaveSettings(SettingsLevel.Company, DataViewsKey, settings);
        }
        #endregion

        #region Delete
        public void Delete(BsnDataView dataView)
        {
            DataViews.Remove(dataView);
            var settings = JsonConvert.SerializeObject(DataViews);
            BsnUtils.SaveSettings(SettingsLevel.Company, DataViewsKey, settings);
        }
        #endregion
    }
}
