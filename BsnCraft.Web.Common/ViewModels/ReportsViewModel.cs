﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using DevExpress.XtraReports.Native;
using DevExpress.XtraReports.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BsnCraft.Web.Common.ViewModels
{
    public class ReportsViewModel : DataViewViewModel
    {
                
        #region Reports
        public List<BsnDataView> Reports
        {
            get
            {
                var dataViews = DataViews.Where(p => p.Type == "Report");
                if (dataViews == null)
                {
                    return new List<BsnDataView>();
                }
                return dataViews.ToList();
            }
        }
        #endregion
        
        #region GetReport
        public BsnDataView GetReport(string name)
        {
            return Reports.Where(p => p.Name == name).FirstOrDefault();
        }
        #endregion

        #region GenerateReport
        public XtraReport GenerateReport(string name)
        {
            return GenerateReport(GetReport(name));
        }

        public XtraReport GenerateReport(BsnDataView report)
        {
            XtraReport devxReport = new XtraReport();
            try
            {
                devxReport.Name = report.Name;
                SerializationService.UnregisterSerializer(report.Name);
                SerializationService.RegisterSerializer(report.Name, new DevxDataSerializer(report.DataSource));
                devxReport.Extensions[SerializationService.Guid] = report.Name;
                devxReport.DataSource = DevxDataSerializer.GenerateTables(RecordUtils.BsnDSN, report.DataSource);
                devxReport.DataMember = "Table";
                if (!string.IsNullOrEmpty(report.Display))
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(report.Display);
                    MemoryStream stream = new MemoryStream(byteArray);
                    devxReport.LoadLayoutFromXml(stream);
                }
            }
            catch (Exception e)
            {
                Utils.LogExc(e);
            }
            return devxReport;
        }
        #endregion
        
    }
}
