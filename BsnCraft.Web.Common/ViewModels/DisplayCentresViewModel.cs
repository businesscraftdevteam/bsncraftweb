﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;

namespace BsnCraft.Web.Common.ViewModels
{
    public class DisplayCentresViewModel : ViewModelBase
    {

        #region DisplayCentres
        public List<ComboItem> DisplayCentres
        {
            get
            {
                var displayCentres = (List<ComboItem>)VarUtils.Get(WebVars.DisplayCentres);
                if (displayCentres == null || displayCentres.Count == 0)
                {
                    string filter = "COT_P_ACTIVE <> 'N'";

                    if (!string.IsNullOrEmpty(Utils.Settings.Comp.DisplayCentresQuery))
                    {
                        filter += " " + Utils.Settings.Comp.DisplayCentresQuery;
                    }

                    displayCentres = GetDropDownList("COT_P_CODE", "COT_P_DESC", "COTABL_P", filter);
                    if (displayCentres != null && displayCentres.Count > 0)
                    {
                        VarUtils.Set(WebVars.DisplayCentres, displayCentres);
                    }
                    else
                    {
                        displayCentres = new List<ComboItem>();
                    }
                }
                return displayCentres;
            }
        }
        #endregion

        #region FindDisplayInfo
        public DisplayInfo FindDisplayInfo(string key)
        {
            var displayInfo = (List<DisplayInfo>)VarUtils.Get(WebVars.DisplayInfo);

            if (displayInfo.Count < 1)
                return null;

            int index = displayInfo.FindIndex(p => p.Key == key);

            if (index < 0)
                return null;

            return displayInfo[index];
        }
        #endregion
        
        #region GetDisplayInfo
        public List<DisplayInfo> GetDisplayInfo(List<DateTime> dates, string salesCentre)
        {
            var questions = DisplayInfoQuestions;
            if (questions == null)
            {
                return questions;
            }

            var displayInfo = new List<DisplayInfo>();

            int key = 0;
            string julian = string.Empty;
            string updateKey = string.Empty;
            string answer = string.Empty;
            string ansCode = string.Empty;


            foreach (DateTime day in dates)
            {
                julian = day.Year.ToString() + day.DayOfYear.ToString("000");
                var answers = DisplayInfoAnswers(julian + salesCentre);

                foreach (DisplayInfo question in questions)
                {
                    updateKey = string.Empty;
                    answer = string.Empty;
                    ansCode = string.Empty;

                    if (answers.ContainsKey(question.Code))
                    {
                        var data = answers[question.Code];
                        updateKey = Utils.GetArray(data, 0);
                        answer = Utils.GetArray(data, 1);
                        ansCode = Utils.GetArray(data, 2);
                    }

                    //Format Numeric Values
                    if (question.Type == "N" || question.Type == "P")
                    {
                        if (!string.IsNullOrEmpty(answer) && !string.IsNullOrEmpty(question.Dec))
                        {
                            answer = answer.Replace(",", "").Replace(".", "");
                            var dp = int.Parse(question.Dec);
                            var numeric = int.Parse(answer);
                            if (dp == 0)
                            {
                                answer = numeric.ToString();
                            }
                            else
                            {
                                var dpstring = "1000000000";
                                var denominator = int.Parse(dpstring.Substring(0, dp + 1));
                                var dec = Convert.ToDouble(numeric) / denominator;
                                answer = dec.ToString("F" + dp);
                            }
                        }
                    }

                    //TODO: Consider passing Sequence or Saving all Questions up front
                    //The sequence number will currently be whichever order questions are answered


                    displayInfo.Add(new DisplayInfo()
                    {
                        Key = (key += 10).ToString(),
                        Day = day,
                        SalesCentre = salesCentre,
                        Code = question.Code,
                        Prompt = question.Prompt,
                        Type = question.Type,
                        Min = question.Min,
                        Max = question.Max,
                        Dec = question.Dec,
                        Default = question.Default,
                        Answers = question.Answers,
                        Answer = answer,
                        AnsCode = ansCode,
                        UpdateKey = updateKey
                    });
                }
            }
            VarUtils.Set(WebVars.DisplayDates, dates);
            VarUtils.Set(WebVars.DisplayInfo, displayInfo);
            return displayInfo;
        }
        #endregion

        #region GetDisplayData
        public List<DisplayInfo> GetDisplayData(List<DateTime> dates, string salesCentre)
        {
            int key = 0;
            string fields = "XIL_SCRKEY,XIL_CODE,XIL_PROMPT,XIL_REFCODE,XIL_ANSWER,XIL_ANSCODE,XIL_ANSTYPE";
            string query = string.Empty;
            string julian = string.Empty;
            
            var displayInfo = new List<DisplayInfo>();
            foreach (DateTime day in dates)
            {
                julian = day.Year.ToString() + day.DayOfYear.ToString("000");
                var where = "XIL_MODULE = 'CSC' AND XIL_REFCODE='" + julian + salesCentre + "'";
                query = "SELECT " + fields + " FROM INFLINA WHERE " + where;
                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    foreach (DataRow row in Table.Rows)
                    {
                        displayInfo.Add(new DisplayInfo()
                        {
                            Key = key++.ToString(),
                            Day = day,
                            SalesCentre = salesCentre,
                            Code = row["XIL_CODE"].ToString(),
                            Prompt = row["XIL_PROMPT"].ToString(),
                            AnsCode = row["XIL_ANSCODE"].ToString(),
                            Answer = row["XIL_ANSWER"].ToString(),
                            Type = row["XIL_ANSTYPE"].ToString()
                        });
                    }
                }
            }
            VarUtils.Set(WebVars.DisplayDates, dates);
            VarUtils.Set(WebVars.DisplayData, displayInfo);
            return displayInfo;
        }
        #endregion

        #region DisplayInfo Combo
        private List<ComboItem> DisplayInfoCombo(string category)
        {
            var answers = (Dictionary<String, List<ComboItem>>)VarUtils.Get(WebVars.DisplayInfoAnswers);
            if (answers == null)
            {
                answers = new Dictionary<string, List<ComboItem>>();
                var fields = "XIB_CAT,XIB_CATCODE,XIB_ANSWER";
                var where = "WHERE XIB_MODULE = 'CSC'";
                var query = "SELECT " + fields + " FROM INFTBLB " + where + " ORDER BY XIB_CAT ASC";
                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    var combo = new List<ComboItem>();
                    var cat = string.Empty;
                    int i = 0;
                    foreach (DataRow row in Table.Rows)
                    {
                        i++;
                        if (row["XIB_CAT"].ToString() == "**")
                            continue;

                        if (row["XIB_CAT"].ToString() != cat && !string.IsNullOrEmpty(cat))
                        {
                            answers.Add(cat, combo);
                            combo = new List<ComboItem>();
                        }
                        cat = row["XIB_CAT"].ToString();                        
                        combo.Add(new ComboItem(row["XIB_CATCODE"].ToString(), row["XIB_ANSWER"].ToString()));                        
                        if (i == Table.Rows.Count)
                        {
                            answers.Add(cat, combo);
                        }
                    }
                }
                VarUtils.Set(WebVars.DisplayInfoAnswers, answers);
            }
            if (answers.ContainsKey(category))
            {
                return answers[category];
            }
            return new List<ComboItem>();
        }
        #endregion

        #region DisplayInfo Questions
        public List<DisplayInfo> DisplayInfoQuestions
        {
            get
            {
                var questions = (List<DisplayInfo>)VarUtils.Get(WebVars.DisplayInfoQuestions);
                if (questions == null)
                {
                    questions = new List<DisplayInfo>();
                    string fields = "XIA_MODULE,XIA_CODE,XIA_PROMPT,XIA_ANSTYPE,XIA_MIN,XIA_MAX,XIA_DEC,XIA_DEF,XIA_LNK_PRF";
                    string where = "WHERE XIA_MODULE = 'CSC'";
                    string query = "SELECT " + fields + " FROM INFTBLA " + where + " ORDER BY XIA_SORT_ORDER ASC, XIA_CODE ASC";
                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            var cat = row["XIA_LNK_PRF"].ToString();
                            var answers = (!string.IsNullOrEmpty(cat)) ? 
                                DisplayInfoCombo(cat) : new List<ComboItem>();
                            
                            questions.Add(new DisplayInfo()
                            {
                                Code = row["XIA_CODE"].ToString(),
                                Prompt = row["XIA_PROMPT"].ToString(),
                                Type = row["XIA_ANSTYPE"].ToString(),
                                Min = row["XIA_MIN"].ToString(),
                                Max = row["XIA_MAX"].ToString(),
                                Dec = row["XIA_DEC"].ToString(),
                                Default = row["XIA_DEF"].ToString(),
                                Answers = answers
                            });
                        }
                    }
                    if (questions.Count == 0)
                    {
                        Utils.SetError("No Display Information Questions have been configured.");
                        return null;
                    }
                    VarUtils.Set(WebVars.DisplayInfoQuestions, questions);
                }
                return questions;
            }
        }
        #endregion

        #region DisplayInfo Answers
        private Dictionary<string, string[]> DisplayInfoAnswers(string RefCode)
        {
            var answers = new Dictionary<string, string[]>();

            var fields = "XIL_SCRKEY,XIL_CODE,XIL_ANSWER,XIL_ANSCODE";
            var where = "XIL_MODULE = 'CSC' AND XIL_REFCODE = '" + RefCode + "'";
            var query = "SELECT " + fields + " FROM INFLINA WHERE " + where;
            
            var Table = RecordUtils.Table(query);
            if (Table != null)
            {
                foreach (DataRow row in Table.Rows)
                {
                    var data = new string[3];
                    data[0] = row["XIL_SCRKEY"].ToString();
                    data[1] = row["XIL_ANSWER"].ToString();
                    data[2] = row["XIL_ANSCODE"].ToString();
                    answers.Add(row["XIL_CODE"].ToString(), data);
                }
            }

            return answers;
        }
        #endregion

        #region UpdateDisplayInfo
        public void UpdateDisplayInfo(string salesCentre)
        {
            var displayInfo = (List<DisplayInfo>)VarUtils.Get(WebVars.DisplayInfo);
            if (displayInfo == null)
                return;

            bool updated = false;
            foreach (DisplayInfo info in displayInfo)
            {
                if (info.Modified)
                {
                    updated = true;
                    if (!string.IsNullOrEmpty(info.UpdateKey))
                    {
                        BsnUtils.ExtraInfoUpdate(info.UpdateKey, info.AnsCode, info.Answer);
                    }
                    else
                    {
                        BsnUtils.ExtraInfoAdd(info.JulianDay + info.SalesCentre, info.Code, info.AnsCode, info.Answer);
                    }
                    info.Modified = false;
                }
            }

            if (updated)
            {
                Utils.SetSuccess();
                var dates = (List<DateTime>)VarUtils.Get(WebVars.DisplayDates);
                GetDisplayInfo(dates, salesCentre);
                GetDisplayData(dates, salesCentre);
            }
        }
        #endregion

    }
}
