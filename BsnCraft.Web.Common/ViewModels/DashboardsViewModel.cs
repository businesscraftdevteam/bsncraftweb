﻿using BsnCraft.Web.Common.Classes;
using System.Collections.Generic;
using System.Linq;

namespace BsnCraft.Web.Common.ViewModels
{
    public class DashboardsViewModel : DataViewViewModel
    {

        #region Dashboards
        public List<BsnDataView> Dashboards
        {
            get
            {
                var dataViews = DataViews.Where(p => p.Type == "Dashboard");
                if (dataViews == null)
                {
                    return new List<BsnDataView>();
                }
                return dataViews.ToList();
            }
        }
        #endregion

        #region GetDashboard
        public BsnDataView GetDashboard(string name)
        {
            return Dashboards.Where(p => p.Name == name).FirstOrDefault();
        }
        #endregion
    }
}
