﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BsnCraft.Web.Common.ViewModels
{
    public class ItemsViewModel : ViewModelBase
    {
        #region ItemList
        public List<ComboItem> ItemList
        {
            get
            {
                List<ComboItem> itemList = VarUtils.Get(WebVars.ItemList) as List<ComboItem>;
                if (itemList == null || itemList.Count == 0)
                {
                    string filter = "OBSFLG='A'";
                    itemList = GetDropDownList("ITEMNO", "DESCR", "ITMMASA", filter);
                    if (itemList != null && itemList.Count > 0)
                    {
                        VarUtils.Set(WebVars.ItemList, itemList);
                    }
                    else
                    {
                        itemList = new List<ComboItem>();
                    }
                }
                return itemList;
            }
        }
        #endregion

        #region AllItems
        public List<Item> AllItems
        {
            get
            {
                var items = (List<Item>)VarUtils.Get(WebVars.AllItems);
                if (items == null)
                {
                    items = new List<Item>();
                    string filter = "WHERE OBSFLG='A'";
                    var fields = "ITEMNO,PRDCAT,SUPLCD,DESCR,EDESC,USRDEF,SUOFM";
                    var query = "SELECT " + fields + " FROM ITMMASA " + filter;
                    if (!string.IsNullOrEmpty(Utils.Settings.Comp.ItemsQuery))
                    {
                        query += " " + Utils.Settings.Comp.ItemsQuery;
                    }
                    var table = RecordUtils.Table(query);
                    if (table != null && table.Rows.Count > 0)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            items.Add(new Item()
                            {
                                ItemNumber = row["ITEMNO"].ToString(),
                                Description = row["DESCR"].ToString(),
                                ExtraDescription = row["EDESC"].ToString()
                            });
                        }
                    }
                    if (items != null && items.Count > 0)
                    {
                        VarUtils.Set(WebVars.AllItems, items);
                    }
                }
                return items;
            }            
        }
        #endregion

        #region Items
        public List<Item> Items(string selection)
        {
            var items = (List<Item>)VarUtils.Get(WebVars.Items);
            var currentSelection = VarUtils.Str(WebVars.ItemSelection);

            if (items == null || items.Count == 0 || selection != currentSelection)
            {
                VarUtils.Set(WebVars.ItemSelection, selection);
                items = new List<Item>();
                string filter = "WHERE OBSFLG='A'";
                var fields = "ITEMNO,PRDCAT,SUPLCD,DESCR,EDESC,USRDEF,SUOFM";
                var query = "SELECT " + fields + " FROM ITMMASA " + filter;
                if (!string.IsNullOrEmpty(selection))
                {
                    var itemSelection = ItemSelections.Where(p => p.Code.ToUpper() == selection.ToUpper()).FirstOrDefault();
                    if (itemSelection != null)
                    {
                        if (itemSelection.Query.Contains(","))
                        {
                            var list = itemSelection.Query;
                            if (!itemSelection.Query.Contains("'"))
                            {
                                var itemlist = list.Split(',');
                                list = string.Empty;
                                foreach (var itemno in itemlist)
                                {
                                    list += "'" + itemno + "',";
                                }                                
                            }
                            list = list.TrimEnd(',');
                            query += " AND ITEMNO IN (" + list + ")";
                        }
                        else
                        {
                            query += " AND " + itemSelection.Query;
                        }
                    }
                    else
                    {
                        query += " AND ITEMNO LIKE '" + selection.ToUpper() + "%'";
                    }
                }
                if (!string.IsNullOrEmpty(Utils.Settings.Comp.ItemsQuery))
                {
                    query += " " + Utils.Settings.Comp.ItemsQuery;
                }
                var table = RecordUtils.Table(query);
                if (table != null && table.Rows.Count > 0)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        items.Add(new Item()
                        {
                            ItemNumber = row["ITEMNO"].ToString(),
                            Description = row["DESCR"].ToString(),
                            ExtraDescription = row["EDESC"].ToString()
                        });
                    }
                }
                if (items != null && items.Count > 0)
                {
                    VarUtils.Set(WebVars.ItemList, items);
                }
                else
                {
                    items = AllItems;
                }
            }
            return items;
        }
        #endregion

        #region Lookup Items
        public List<SearchData> LookupItems(string query)
        {
            var selection = VarUtils.Str(WebVars.ItemSelection);
            var items = (List<Item>)VarUtils.Get(WebVars.Items);
            if (items == null)
            {
                items = Items(selection);
                if (items != null && items.Count > 0)
                {
                    VarUtils.Set(WebVars.Items, items);
                }
            }

            query = query.ToUpper();

            var match = items.Where(p => p.ItemNumber.ToUpper().Contains(query) ||
                p.Description.ToUpper().Contains(query) ||
                p.ExtraDescription.ToUpper().Contains(query)).Take(10);

            var results = new List<SearchData>();
            foreach (var item in match)
            {
                var value = item.ItemNumber + " | " + item.Description + " | " + item.ExtraDescription;
                results.Add(new SearchData() { Value = value, Data = JsonConvert.SerializeObject(item) });
            }
            return results;
        }
        #endregion
        
        #region Item Selections
        public List<ItemSelection> ItemSelections
        {
            get
            {
                var table = (List<ItemSelection>)VarUtils.Get(WebVars.ItemSelections);
                if (table == null)
                {
                    table = Utils.GetRecords<ItemSelection>("BMSPEC_U", "");
                    VarUtils.Set(WebVars.ItemSelections, table);
                }
                return table;
            }
        }
        #endregion

        #region Get Selection
        public ItemSelection GetSelection(string code)
        {
            return ItemSelections.Where(p => p.Code == code).FirstOrDefault();
        }
        #endregion

        #region Selection Options
        public List<ComboItem> SelectionOptions
        {
            get
            {
                var options = new List<ComboItem>
                {
                    new ComboItem("Q", "Query"),
                    new ComboItem("L", "List")
                };
                return options;
            }
        }
        #endregion

        #region Update Selection
        public void UpdateSelection(ItemSelection selection, bool delete)
        {
            string extras = (delete) ? "DELETE=YES" : "OPTION=" + selection.Option + ",DESC=" + selection.Description;
            BsnUtils.UpdateItemSelection(selection.Code, selection.Query, ref extras);                
        }
        #endregion

        #region Houses
        public List<Item> Houses
        {
            get
            {
                var items = (List<Item>)VarUtils.Get(WebVars.Houses);
                if (items == null)
                {
                    var cvm = new ContractsViewModel();

                    items = new List<Item>();
                    string filter = "WHERE OBSFLG='A'";
                    var fields = "ITEMNO,PRDCAT,SUPLCD,DESCR,EDESC,USRDEF,SUOFM,PC_GROUP";
                    var join = "JOIN INVTBLB ON PC_CATEGORY = PRDCAT";
                    var query = "SELECT " + fields + " FROM ITMMASA " + join + " " + filter;
                    
                    if (!string.IsNullOrEmpty(cvm.Settings.HouseFilterCode))
                    {
                        if (cvm.Settings.HouseFilterType == "G")
                        {
                            query += " AND PC_GROUP = '" + cvm.Settings.HouseFilterCode + "'";
                        }
                        else
                        {
                            query += " AND PRDCAT = '" + cvm.Settings.HouseFilterCode + "'";
                        }
                    }
                    

                    var table = RecordUtils.Table(query);
                    if (table != null && table.Rows.Count > 0)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            items.Add(new Item()
                            {
                                ItemNumber = row["ITEMNO"].ToString(),
                                Description = row["DESCR"].ToString(),
                                ExtraDescription = row["EDESC"].ToString()
                            });
                        }
                    }
                    if (items != null && items.Count > 0)
                    {
                        VarUtils.Set(WebVars.Houses, items);
                    }
                }
                return items;
            }
        }
        #endregion

        #region Lookup Houses
        public List<SearchData> LookupHouses(string query)
        {
            var items = (List<Item>)VarUtils.Get(WebVars.Houses);
            if (items == null)
            {
                items = Houses;
                if (items != null && items.Count > 0)
                {
                    VarUtils.Set(WebVars.Houses, items);
                }
            }

            query = query.ToUpper();

            var match = items.Where(p => p.ItemNumber.ToUpper().Contains(query) ||
                p.Description.ToUpper().Contains(query) ||
                p.ExtraDescription.ToUpper().Contains(query)).Take(10);

            var results = new List<SearchData>();
            foreach (var item in match)
            {
                var value = item.ItemNumber + " | " + item.Description + " | " + item.ExtraDescription;
                results.Add(new SearchData() { Value = value, Data = JsonConvert.SerializeObject(item) });
            }
            return results;
        }
        #endregion

        #region Facades
        public List<Item> Facades
        {
            get
            {
                var items = (List<Item>)VarUtils.Get(WebVars.Facades);
                if (items == null)
                {
                    var cvm = new ContractsViewModel();

                    items = new List<Item>();
                    string filter = "WHERE OBSFLG='A'";
                    var fields = "ITEMNO,PRDCAT,SUPLCD,DESCR,EDESC,USRDEF,SUOFM,PC_GROUP";
                    var join = "JOIN INVTBLB ON PC_CATEGORY = PRDCAT";
                    var query = "SELECT " + fields + " FROM ITMMASA " + join + " " + filter;

                    if (!string.IsNullOrEmpty(cvm.Settings.FacadeFilterCode))
                    {
                        if (cvm.Settings.FacadeFilterType == "G")
                        {
                            query += " AND PC_GROUP = '" + cvm.Settings.FacadeFilterCode + "'";
                        }
                        else
                        {
                            query += " AND PRDCAT = '" + cvm.Settings.FacadeFilterCode + "'";
                        }
                    }


                    var table = RecordUtils.Table(query);
                    if (table != null && table.Rows.Count > 0)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            items.Add(new Item()
                            {
                                ItemNumber = row["ITEMNO"].ToString(),
                                Description = row["DESCR"].ToString(),
                                ExtraDescription = row["EDESC"].ToString()
                            });
                        }
                    }
                    if (items != null && items.Count > 0)
                    {
                        VarUtils.Set(WebVars.Facades, items);
                    }
                }
                return items;
            }
        }
        #endregion

        #region Lookup Facades
        public List<SearchData> LookupFacades(string query)
        {
            var items = (List<Item>)VarUtils.Get(WebVars.Facades);
            if (items == null)
            {
                items = Facades;
                if (items != null && items.Count > 0)
                {
                    VarUtils.Set(WebVars.Facades, items);
                }
            }

            query = query.ToUpper();

            var match = items.Where(p => p.ItemNumber.ToUpper().Contains(query) ||
                p.Description.ToUpper().Contains(query) ||
                p.ExtraDescription.ToUpper().Contains(query)).Take(10);

            var results = new List<SearchData>();
            foreach (var item in match)
            {
                var value = item.ItemNumber + " | " + item.Description + " | " + item.ExtraDescription;
                results.Add(new SearchData() { Value = value, Data = JsonConvert.SerializeObject(item) });
            }
            return results;
        }
        #endregion
    }
}
