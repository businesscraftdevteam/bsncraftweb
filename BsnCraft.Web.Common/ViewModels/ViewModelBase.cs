﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using DevExpress.Web.Bootstrap;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BsnCraft.Web.Common.ViewModels
{
    public class ViewModelBase
    {
        protected Logger Log { get; private set; }

        protected ViewModelBase()
        {
            Log = LogManager.GetLogger(GetType().FullName);
        }

        #region GetDropDownList        
        public List<ComboItem> GetDropDownList(string code, string desc, string table, string filter = "", string extraField = "")
        {
            var list = new List<ComboItem>();
            string fields = code + "," + desc;

            if (!string.IsNullOrEmpty(extraField))
                fields += "," + extraField;

            string query = "SELECT " + fields + " FROM " + table + " WHERE {fn LENGTH(" + code + ")} > 0";
            if (!string.IsNullOrEmpty(filter))
            {
                query += " AND " + filter;
            }
            var List = RecordUtils.Table(query);
            if (List != null)
            {
                foreach (DataRow item in List.Rows)
                {
                    if (!string.IsNullOrEmpty(extraField))
                    {
                        list.Add(new ComboItem(item[code].ToString(), item[desc].ToString(), item[extraField].ToString()));
                    }
                    else
                    {
                        list.Add(new ComboItem(item[code].ToString(), item[desc].ToString()));
                    }
                }
            }
            return list;
        }
        #endregion       
        
    }
}