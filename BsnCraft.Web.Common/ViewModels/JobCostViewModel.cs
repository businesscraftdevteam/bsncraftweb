﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using BsnCraft.Web.Common.Classes.Grid;

namespace BsnCraft.Web.Common.ViewModels
{
    public class JobCostViewModel : ViewModelBase
    {
        #region ClearCache
        public void ClearCache()
        {
            VarUtils.Set(WebVars.Headings, null);
            VarUtils.Set(WebVars.Documents, null);
            VarUtils.Set(WebVars.JobDocuments, null);
            VarUtils.Set(WebVars.EstimateDetails, null);
            VarUtils.Set(WebVars.EstimateFilter, null);
            VarUtils.Set(WebVars.Paragraphs, null);
            VarUtils.Set(WebVars.DocumentTypes, null);
            VarUtils.Set(WebVars.HeadingDocTypes, null);
            VarUtils.Set(WebVars.PurchaseOrders, null);
        }
        #endregion

        #region GetContract
        public Contract GetContract(string key, bool isContractNo = true)
        {
            Contract contract;
            if (isContractNo)
            {
                contract = new ContractsViewModel().Contracts.Where(p => p.ContractNumber == key).FirstOrDefault();
                if (contract == null)
                {
                    //Since a Lead is a contract object, should be safe. Saves too much duplication in Leads module
                    contract = new LeadsViewModel().Leads.Where(p => p.ContractNumber == key).FirstOrDefault();
                }
                return contract;
            }

            contract = new ContractsViewModel().Contracts.Where(p => p.JobNumber == key).FirstOrDefault();
            if (contract == null)
            {
                //Since a Lead is a contract object, should be safe. Saves too much duplication in Leads module
                contract = new LeadsViewModel().Leads.Where(p => p.JobNumber == key).FirstOrDefault();
            }

            return contract;
        }
        #endregion

        #region Headings
        public List<Heading> Headings(string key, EstimateTypes type)
        {
            string[] keyval = key.Split(':');
            string contractNumber = Utils.GetArray(keyval, 0);
            string variationSeq = Utils.GetArray(keyval, 1);

            var headings = (List<Heading>)VarUtils.Get(WebVars.Headings);
            if (headings == null && !string.IsNullOrEmpty(contractNumber))
            {
                headings = new List<Heading>();
                string fields = "CO_KEY,CO_JOBNUM,JSEA_KEY,JSEA_SECT,JSEA_JOBNUM,JSEA_HEADING,JSEA_DESC,JSEA_CATALOG,";
                fields += "JSEA_PROT,JSEA_LOCKED,JSEA_STATUS,JSEA_TEMPLATE,JSEA_SALE,JSEA_GST_AMOUNT";
                string join = "JOIN CONHDRA ON CO_JOBNUM = JSEA_JOBNUM";
                string query = "SELECT " + fields + " FROM JCSPEC_A " + join + " WHERE CO_KEY ='" + contractNumber + "'";

                string[] sections = Utils.Settings.Comp.SalesEstimateSections;
                if (type == EstimateTypes.Variation)
                {
                    query += " AND JSEA_HEADING = '" + variationSeq + "'";
                    sections = Utils.Settings.Comp.VariationEstimateSections;
                }

                string sectionlist = string.Empty;
                foreach (string section in sections)
                {
                    sectionlist += "'" + section + "',";
                }

                if (!string.IsNullOrEmpty(sectionlist))
                {
                    query += " AND JSEA_SECT IN (" + sectionlist.TrimEnd(',') + ")";
                }

                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    var headingCheck = HeadingCheck(contractNumber, sections);
                    foreach (DataRow row in Table.Rows)
                    {
                        Decimal sellAmtEx = (row["JSEA_SALE"] is Decimal) ? ((Decimal)row["JSEA_SALE"]) : 0;
                        Decimal sellAmtGST = (row["JSEA_GST_AMOUNT"] is Decimal) ? ((Decimal)row["JSEA_GST_AMOUNT"]) : 0;
                        var hasEstimate = headingCheck.Contains(row["JSEA_HEADING"].ToString());
                        if (!hasEstimate)
                        {
                            hasEstimate = (row["JSEA_CATALOG"].ToString() == "EMPTY");
                        }
                        headings.Add(new Heading()
                        {
                            Key = row["JSEA_KEY"].ToString(),
                            ContractNumber = row["CO_KEY"].ToString(),
                            Code = row["JSEA_HEADING"].ToString(),
                            Description = row["JSEA_DESC"].ToString(),
                            Catalog = row["JSEA_CATALOG"].ToString(),
                            Status = row["JSEA_STATUS"].ToString(),
                            SellAmtEx = sellAmtEx,
                            SellAmtGST = sellAmtGST,
                            HasEstimate = hasEstimate
                        });
                    }
                }

                VarUtils.Set(WebVars.Headings, headings);
            }
            return headings;
        }
        #endregion

        #region Heading Check
        private List<string> HeadingCheck(string contractNumber, string[] sections)
        {
            var headingCheck = new List<string>();

            string fields = "CO_KEY,CO_JOBNUM,JSEB_SECT,JSEB_JOBNUM,JSEB_HEADING";
            string join = "JOIN CONHDRA ON CO_JOBNUM = JSEB_JOBNUM";
            string query = "SELECT " + fields + " FROM JCSPEC_B " + join + " WHERE CO_KEY ='" + contractNumber + "'";

            string sectionlist = string.Empty;
            foreach (string section in sections)            //BJP 19-Feb-2018
            {
                sectionlist += "'" + section + "',";
            }
            if (!string.IsNullOrEmpty(sectionlist))
            {
                query += " AND JSEB_SECT IN (" + sectionlist.TrimEnd(',') + ")";
            }

            var Table = RecordUtils.Table(query);
            if (Table != null)
            {
                foreach (DataRow row in Table.Rows)
                {
                    headingCheck.Add(row["JSEB_HEADING"].ToString());
                }
            }

            return headingCheck;
        }
        #endregion

        #region GetHeading
        public Heading GetHeading(string key, string estimateHeading, EstimateTypes type)
        {
            if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(estimateHeading))
                return null;

            var headings = Headings(key, type).Where(p => p.Code == estimateHeading);
            if (headings == null)
                return null;

            return headings.FirstOrDefault();
        }
        #endregion

        #region HeadingDocTypes
        public List<DocumentType> HeadingDocTypes
        {
            get
            {
                var documentTypes = (List<DocumentType>)VarUtils.Get(WebVars.HeadingDocTypes);
                if (documentTypes == null || documentTypes.Count == 0)
                {
                    documentTypes = new List<DocumentType>();
                    string fields = "IVWP_CODE,IVWP_MODULE,IVWP_FILE,IVWP_MERGEFMT,IVWP_DESCR";
                    string query = "SELECT " + fields + " FROM IVCTBLP WHERE IVWP_MODULE IN ('', 'CO', 'JC')";

                    string typelist = string.Empty;
                    foreach (string doctype in Utils.Settings.Comp.HeadingDocumentTypes)
                    {
                        typelist += "'" + doctype + "',";
                    }
                    if (!string.IsNullOrEmpty(typelist))
                    {
                        query += " AND IVWP_CODE IN (" + typelist.TrimEnd(',') + ")";
                    }

                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            var code = row["IVWP_CODE"].ToString();
                            var descr = row["IVWP_DESCR"].ToString();
                            var module = row["IVWP_MODULE"].ToString();

                            var docType = new DocumentType(code, descr)
                            {
                                ExtraData = module,
                                MasterDocument = row["IVWP_FILE"].ToString()
                            };
                            documentTypes.Add(docType);
                        }
                    }
                    if (documentTypes != null && documentTypes.Count > 0)
                    {
                        VarUtils.Set(WebVars.HeadingDocTypes, documentTypes);
                    }
                }
                return documentTypes;
            }
        }

        public string HeadingDocDesc(string code, string module)
        {
            var docTypes = HeadingDocTypes.Where(p => p.Code == code && p.ExtraData == module);
            if (docTypes == null || docTypes.Count() == 0)
            {
                return string.Empty;
            }
            return docTypes.FirstOrDefault().Desc;
        }
        #endregion
        
        #region Documents
        public List<Document> Documents(string contractNumber)
        {
            var documents = (List<Document>)VarUtils.Get(WebVars.JobDocuments);
            if (documents != null)
                return documents;

            var contract = new ContractsViewModel().GetContract(contractNumber);
            if (!string.IsNullOrEmpty(contract.JobNumber))
            {
                string[] sections = Utils.Settings.Comp.SalesEstimateSections;
                string section = sections[0];

                documents = new List<Document>();
                string fields = "JWP_DOCTYP,JWP_JOBSEQ,JWP_JOBNUM,JWP_SEQNUM,JWP_DOCREF,JWP_DESCR,JWP_CRE_DATE,JWP_ALT_DOCS,IVWP_NOCHNG";
                var join = "JOIN IVCTBLP ON IVWP_CODE = JWP_DOCTYP AND IVWP_MODULE IN ('', 'JC')";
                string query = "SELECT " + fields + " FROM JCSPECX " + join;
                query += " WHERE JWP_JOBNUM = '" + contract.JobNumber + "'";
                //query += " AND JWP_SECT = '" + section + "'";
                
                string typelist = string.Empty;
                foreach (string doctype in Utils.Settings.Comp.HeadingDocumentTypes)
                {
                    typelist += "'" + doctype + "',";
                }
                if (!string.IsNullOrEmpty(typelist))
                {
                    query += " AND JWP_DOCTYP IN (" + typelist.TrimEnd(',') + ")";
                }
                query += " ORDER BY JWP_JOBSEQ ASC";

                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    foreach (DataRow row in Table.Rows)
                    {
                        string date = (row["JWP_CRE_DATE"] is DateTime) ? ((DateTime)row["JWP_CRE_DATE"]).ToString("dd/MM/yyyy") : string.Empty;
                        int.TryParse(row["JWP_SEQNUM"].ToString(), out int seq);
                        var sequence = (seq > 0) ? seq.ToString("000") : string.Empty;

                        documents.Add(new Document()
                        {
                            Key = row["JWP_JOBSEQ"].ToString(),
                            ContractNumber = contractNumber,
                            Module = "JC",
                            Type = row["JWP_DOCTYP"].ToString(),
                            Sequence = sequence,
                            Reference = row["JWP_DOCREF"].ToString(),
                            Description = row["JWP_DESCR"].ToString(),
                            Filename = row["JWP_ALT_DOCS"].ToString(),
                            Locked = (row["IVWP_NOCHNG"].ToString() == "Y"),
                            Date = date
                        });
                    }

                }
                if (documents.Count > 0)
                {
                    var notes = DocumentNotes(contract.JobNumber);
                    var spec = string.Empty;
                    foreach (var document in documents)
                    {
                        spec = string.Empty;
                        if (notes.ContainsKey(document.Key))
                        {
                            spec = notes[document.Key];
                        }
                        document.LoadNotes(spec);
                    }
                    VarUtils.Set(WebVars.JobDocuments, documents);
                }
            }
            return documents;
        }
        #endregion

        #region Document Notes
        private Dictionary<string, string> DocumentNotes(string jobNumber)
        {
            var notes = new Dictionary<string, string>();

            var fields = "JS5_JOBNUM,JS5_CONST,JS5_DESC";
            var where = "JS5_JOBNUM = '" + jobNumber + "'";
            var query = "SELECT " + fields + " FROM JCSPEC5 WHERE " + where;

            var Table = RecordUtils.Table(query);
            if (Table != null)
            {
                var spec = string.Empty;
                var key = string.Empty;
                int i = 0;
                foreach (DataRow row in Table.Rows)
                {
                    i++;
                    //TODO: remove substring when Genesis fixed
                    var curr = row["JS5_CONST"].ToString().Substring(0, 11);
                    if (curr != key && !string.IsNullOrEmpty(key))
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                        spec = string.Empty;
                    }
                    spec += row["JS5_DESC"].ToString();
                    key = curr;
                    if (i == Table.Rows.Count)
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                    }
                }
            }
            return notes;
        }
        #endregion

        #region GetDocument        
        public Document GetDocument(string key)
        {
            if (string.IsNullOrEmpty(key))
                return null;

            var jobNumber = key.Substring(0, 8);
            var contract = GetContract(jobNumber, false);
            if (contract == null)
                return null;

            return Documents(contract.ContractNumber).Where(p => p.Key == key).FirstOrDefault();
        }
        #endregion

        #region UpdateHeading
        public bool UpdateHeading(Heading estimate, string option, EstimateTypes type)
        {
            var contract = GetContract(estimate.ContractNumber);
            if (contract == null)
            {
                Utils.SetError("Could not find key: " + estimate.ContractNumber);
                return false;
            }

            string[] sections = (type == EstimateTypes.Variation) ?
                        Utils.Settings.Comp.VariationEstimateSections :
                        Utils.Settings.Comp.SalesEstimateSections;

            string section = sections[0];
            int mode = 1;

            if (estimate.IsNew)
            {
                if (string.IsNullOrEmpty(estimate.Code))
                {
                    int heading = 0;
                    var query = "SELECT JSEA_HEADING FROM JCSPEC_A WHERE JSEA_JOBNUM = '";
                    query += contract.JobNumber + "' AND JSEA_SECT='";
                    query += section + "' AND JSEA_HEADING LIKE '0%' ORDER BY JSEA_HEADING DESC";
                    var table = RecordUtils.Table(query);
                    if (table != null && table.Rows.Count > 0)
                    {
                        var previous = table.Rows[0]["JSEA_HEADING"].ToString();
                        int.TryParse(previous, out heading);
                    }
                    heading++;
                    estimate.Code = heading.ToString("0000");
                    estimate.Description = "Estimate " + heading.ToString();
                }
            }
            else
            {
                mode = 2;   //Update
            }

            if (!BsnUtils.UpdateHeading(mode, contract.JobNumber, section, estimate, option, type))
            {
                return false;
            }

            if (mode == 1)
            {
                Utils.SetSuccess();
            }
            else
            {
                string message = "Successfully ";
                switch (option)
                {
                    case "R":
                        message += "re-opened";
                        break;
                    case "X":
                        message += "cancelled";
                        break;
                    default:
                        message += "updated";
                        break;
                }
                message += " estimate " + estimate.Code;
                Utils.SetSuccess(message);
            }
            return true;
        }
        #endregion

        #region CopyHeading
        public bool CopyHeading(Heading source, Heading destination, EstimateTypes type)
        {
            //Todo: support cross-Job copy?

            var contract = GetContract(source.ContractNumber);
            if (contract == null)
            {
                Utils.SetError("Could not find key: " + source.ContractNumber);
                return false;
            }

            string[] sections = (type == EstimateTypes.Variation) ?
                        Utils.Settings.Comp.VariationEstimateSections :
                        Utils.Settings.Comp.SalesEstimateSections;

            string section = sections[0];

            if (!BsnUtils.CopyHeading(contract.JobNumber, section, source, destination))
            {
                return false;
            }

            string message = "Successfully copied estimate";
            Utils.SetSuccess(message);
            return true;
        }
        #endregion

        #region UpdateParagraph
        public void UpdateParagraph(string contractNumber, Paragraph paragraph, EstimateTypes type, int mode = 2)
        {
            try
            {
                string[] sections = (type == EstimateTypes.Variation) ?
                        Utils.Settings.Comp.VariationEstimateSections :
                        Utils.Settings.Comp.SalesEstimateSections;

                string section = sections[0];
                if (string.IsNullOrEmpty(section))
                    return;

                var contract = GetContract(contractNumber);
                if (contract == null)
                {
                    Utils.SetError("Could not find key: " + contractNumber);
                    return;
                }
                
                //Update Margin
                if (mode == 4)
                {
                    if (paragraph.MarginPct != paragraph.OriginalMargin)
                    {
                        paragraph.MarginCode = string.Empty;
                    }
                    BsnUtils.UpdateMargin(contract.JobNumber, section, paragraph.Heading, paragraph.Code,
                        paragraph.MarginCode, paragraph.MarginPct, paragraph.MarginUpdAll);
                }
                else
                {
                    if (paragraph.IsNew)
                    {
                        mode = 1;
                    }
                    //mode 3 = delete
                    BsnUtils.UpdateParagraph(mode, contract.JobNumber, section, paragraph);
                }                

                ClearCache();
            }
            catch (Exception e)
            {
                Utils.LogExc(e);
            }
        }
        #endregion

        #region EstimateDetails
        public List<EstimateDetail> EstimateDetails(string contractNumber, string estimateHeading, EstimateTypes type)
        {
            var estimateDetails = (List<EstimateDetail>)VarUtils.Get(WebVars.EstimateDetails);
            if (estimateDetails == null && !string.IsNullOrEmpty(contractNumber))
            {
                try
                {
                    string[] sections = (type == EstimateTypes.Variation) ?
                        Utils.Settings.Comp.VariationEstimateSections :
                        Utils.Settings.Comp.SalesEstimateSections;

                    string section = sections[0];

                    if (string.IsNullOrEmpty(section))
                        return null;

                    var paragraphs = Paragraphs(contractNumber, estimateHeading, type);

                    estimateDetails = new List<EstimateDetail>();

                    string fields = "CO_KEY,JSEC_KEY,JSEC_SCRKEY,JSEC_JOBNUM,JSEC_SECT,JSEC_HEADING,JSEC_PARAGRAPH,JSEC_DETSEQ,";
                    fields += "JSEC_ITMNUM,JSEC_DESC,JSEC_SALE,JSEC_GST_AMOUNT,JSEC_ESTQTY,JSEC_DFLT_CHK,JSEC_DFLT_QTY,";
                    fields += "JSEC_UOM,JSEC_PRCFLG,JSEC_DISP_CHKBOX,JSEC_DISP_QTYFLD,JSEC_GST_INC,JSEC_COST,JSEC_MGNCDE,JSEC_MGNPCT";
                    if (!Utils.Settings.App.UppercasePassword && string.IsNullOrEmpty(Utils.Settings.Comp.AlternateDSN))
                    {
                        fields += ",JSEC_DISP_SELECT";
                    }
                    string join = "JOIN CONHDRA ON CO_JOBNUM = JSEC_JOBNUM";
                    string where = "CO_KEY ='" + contractNumber + "'";
                    where += " AND JSEC_SECT = '" + section + "'";
                    where += " AND JSEC_HEADING = '" + estimateHeading + "'";
                    string query = "SELECT " + fields + " FROM JCSPEC_C " + join + " WHERE " + where;
                    var table = RecordUtils.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            decimal quantity = (row["JSEC_ESTQTY"] is decimal) ? ((decimal)row["JSEC_ESTQTY"]) : 0;
                            decimal unitCost = (row["JSEC_COST"] is decimal) ? ((decimal)row["JSEC_COST"]) : 0;
                            decimal marginPct = (row["JSEC_MGNPCT"] is decimal) ? ((decimal)row["JSEC_MGNPCT"]) : 0;
                            decimal unitAmountEx = (row["JSEC_SALE"] is decimal) ? ((decimal)row["JSEC_SALE"]) : 0;
                            decimal unitAmountGst = (row["JSEC_GST_AMOUNT"] is decimal) ? ((decimal)row["JSEC_GST_AMOUNT"]) : 0;                            
                            int.TryParse(row["JSEC_DETSEQ"].ToString(), out int sequence);

                            var est = new EstimateDetail()
                            {
                                Key = row["JSEC_KEY"].ToString(),
                                ScrKey = row["JSEC_SCRKEY"].ToString(),
                                ContractNumber = row["CO_KEY"].ToString(),
                                Heading = row["JSEC_HEADING"].ToString(),
                                Parent = FindParagraph(row["JSEC_PARAGRAPH"].ToString()),
                                Sequence = sequence.ToString("00000"),
                                Item = row["JSEC_ITMNUM"].ToString(),
                                Description = row["JSEC_DESC"].ToString(),
                                UnitOfMeasure = row["JSEC_UOM"].ToString(),
                                PriceDisplay = row["JSEC_PRCFLG"].ToString(),
                                MarginCode = row["JSEC_MGNCDE"].ToString(),
                                IncludeGST = (row["JSEC_GST_INC"].ToString().ToUpper() == "Y"),
                                ShowCheckbox = (row["JSEC_DISP_CHKBOX"].ToString().ToUpper() != "N"),
                                ShowQuantity = (row["JSEC_DISP_QTYFLD"].ToString().ToUpper() != "N"),                                
                                Included = (row["JSEC_DFLT_CHK"].ToString().ToUpper() != "N"),
                                Quantity = quantity,
                                UnitAmountGst = unitAmountGst
                            };

                            est.SetOriginalCost(unitCost);
                            est.SetOriginalMargin(marginPct);
                            est.SetOriginalAmountEx(unitAmountEx);
                            est.SetOriginalAmountInc(unitAmountEx + unitAmountGst);

                            if (!Utils.Settings.App.UppercasePassword && string.IsNullOrEmpty(Utils.Settings.Comp.AlternateDSN))
                            {
                                est.ShowSelect = (row["JSEC_DISP_SELECT"].ToString().ToUpper() != "N");
                            }

                            estimateDetails.Add(est);
                        }
                    }

                    //Create dummy line for paragraphs without items        
                    /*
                    foreach (var paragraph in paragraphs)
                    {
                        if (estimateDetails.Where(p => p.Parent == paragraph).Count() == 0)
                        {
                            string key = contractNumber + ":" + estimateHeading + ":" + paragraph.Code + ":00001";
                            estimateDetails.Add(new EstimateDetail() {
                                Key = key,
                                ContractNumber = contractNumber,
                                Heading = estimateHeading,
                                Parent = FindParagraph(paragraph.Code),
                                IsNew = true
                            });
                        }
                    }
                    */
                }
                catch (Exception e)
                {
                    Utils.LogExc(e);
                }
                if (estimateDetails != null && estimateDetails.Count > 0)
                {
                    LoadEstimateExtras(contractNumber, estimateHeading, estimateDetails, type);
                    VarUtils.Set(WebVars.EstimateDetails, estimateDetails);
                }
                else
                {
                    return new List<EstimateDetail>();
                }
            }
            return estimateDetails;
        }
        #endregion

        #region LoadEstimateExtras
        public void LoadEstimateExtras(string contractNumber, string estimateHeading, List<EstimateDetail> details, EstimateTypes type)
        {
            var contract = GetContract(contractNumber);
            string[] sections = (type == EstimateTypes.Variation) ?
                Utils.Settings.Comp.VariationEstimateSections :
                Utils.Settings.Comp.SalesEstimateSections;
            string section = sections[0];
            if (string.IsNullOrEmpty(section))
                return;

            var fields = "JS0_CONST,JS0_DESC";
            var where = "JS0_JOBNUM = '" + contract.JobNumber + "'";
            where += " AND JS0_SECT = '" + section + "'";
            where += " AND JS0_HEADING = '" + estimateHeading + "'";
            var query = "SELECT " + fields + " FROM JCSPEC0 WHERE " + where;
            query += " ORDER BY JS0_KEY ASC";
            var Table = RecordUtils.Table(query);
            if (Table != null && Table.Rows.Count > 0)
            {
                var spec = string.Empty;
                var key = string.Empty;
                var notes = new Dictionary<string, string>();
                int i = 0;
                foreach (DataRow row in Table.Rows)
                {
                    i++;
                    if (row["JS0_CONST"].ToString() != key && !string.IsNullOrEmpty(key))
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                        spec = string.Empty;
                    }
                    spec += row["JS0_DESC"].ToString();
                    key = row["JS0_CONST"].ToString();
                    if (i == Table.Rows.Count)
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                    }
                }

                foreach (var detail in details.Where(p => !p.IsNew))
                {
                    spec = string.Empty;
                    if (notes.ContainsKey(detail.ScrKey))
                    {
                        spec = notes[detail.ScrKey];
                    }
                    detail.LoadNotes(spec);
                }
            }


            fields = "JSEO_CONST,JSEO_SPEC";
            where = "JSEO_JOBNUM = '" + contract.JobNumber + "'";
            where += " AND JSEO_SECT = '" + section + "'";
            where += " AND JSEO_HEADING = '" + estimateHeading + "'";
            query = "SELECT " + fields + " FROM JCSPEC_I WHERE " + where;
            query += " ORDER BY JSEO_KEY ASC";
            Table = RecordUtils.Table(query);
            if (Table != null)
            {
                var spec = string.Empty;
                var key = string.Empty;
                var options = new Dictionary<string, string>();
                int i = 0;
                foreach (DataRow row in Table.Rows)
                {
                    i++;
                    if (row["JSEO_CONST"].ToString() != key && !string.IsNullOrEmpty(key))
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        options.Add(key, spec);
                        spec = string.Empty;
                    }
                    spec += row["JSEO_SPEC"].ToString();
                    key = row["JSEO_CONST"].ToString();
                    if (i == Table.Rows.Count)
                    {
                        spec = Utils.CleanSpec(spec, "\r\n");
                        options.Add(key, spec);
                    }
                }

                foreach (var detail in details.Where(p => !p.IsNew))
                {
                    spec = string.Empty;
                    if (options.ContainsKey(detail.ScrKey))
                    {
                        spec = options[detail.ScrKey];
                    }
                    detail.LoadOptions(spec);
                }
            }
        }
        #endregion

        #region EstimateDetails
        public List<EstimateDetail> EstimateDetails(string contractNumber, string estimateHeading, string paragraphs, EstimateTypes type)
        {
            var selectedParagraphs = paragraphs.Split(',');
            var estimateDetails = EstimateDetails(contractNumber, estimateHeading, type)
                .Where(p => selectedParagraphs.Contains(p.Paragraph)).ToList();

            if (!Utils.Settings.Comp.EstimateSubHeadings)
            {
                return estimateDetails;
            }

            var subHeadings = Paragraphs(contractNumber, estimateHeading, type).Where(p => p.SubHeading);
            if (subHeadings == null || subHeadings.Count() == 0)
            {
                return estimateDetails;
            }

            bool addSubHeading = false;
            foreach (var paragraph in Paragraphs(contractNumber, estimateHeading, type).OrderBy(p => p.Code))
            {
                if (paragraph.SubHeading && addSubHeading)
                {
                    estimateDetails.AddRange(EstimateDetails(contractNumber, estimateHeading, type)
                        .Where(p => p.Paragraph == paragraph.Code));
                }
                else
                {
                    addSubHeading = selectedParagraphs.Contains(paragraph.Code);
                }
            }
            return estimateDetails;
        }
        #endregion

        #region CopyTemplate
        public bool CopyTemplate(string contractNumber, string estimateHeading, Template template, string Extras)
        {
            try
            {
                string section = Utils.Settings.Comp.SalesEstimateSections[0];
                if (string.IsNullOrEmpty(section))
                    return false;

                var contract = GetContract(contractNumber);
                if (contract == null)
                {
                    Utils.SetError("Could not find key: " + contractNumber);
                    return false;
                }

                return BsnUtils.TemplateCopy(template.TemplateCode, template.HeadingCode,
                    contract.JobNumber, section, estimateHeading, Extras);
            }
            catch (Exception e)
            {
                Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region FindEstimateDetail
        public EstimateDetail FindEstimateDetail(string key)
        {
            var estimateDetail = (List<EstimateDetail>)VarUtils.Get(WebVars.EstimateDetails);

            if (estimateDetail == null)
                return null;

            return estimateDetail.Where(p => p.Key == key).FirstOrDefault();
        }

        /*
        public EstimateDetail FindEstimateDetail(string contractNumber, string estimateHeading, string paragraph, string sequence)
        {
            List<EstimateDetail> estimateDetail = VarUtils.Get(WebVars.EstimateDetails) as List<EstimateDetail>;

            if (estimateDetail == null)
                return null;

            int index = estimateDetail.FindIndex(
                p => p.ContractNumber == contractNumber && 
                p.Heading == estimateHeading && 
                p.Paragraph == paragraph && 
                p.Sequence == sequence);

            if (index < 0)
                return null;

            return estimateDetail[index];
        }
        */
        #endregion

        #region Paragraphs
        public List<Paragraph> Paragraphs(string contractNumber, string estimateHeading, EstimateTypes type)
        {
            var paragraphs = VarUtils.Get(WebVars.Paragraphs) as List<Paragraph>;
            if (paragraphs == null && !string.IsNullOrEmpty(contractNumber))
            {
                try
                {
                    string[] sections = (type == EstimateTypes.Variation) ?
                        Utils.Settings.Comp.VariationEstimateSections :
                        Utils.Settings.Comp.SalesEstimateSections;

                    string section = sections[0];
                    if (string.IsNullOrEmpty(section))
                        return null;

                    paragraphs = new List<Paragraph>();

                    string fields = "CO_KEY,JSEB_SCRKEY,JSEB_JOBNUM,JSEB_SECT,JSEB_HEADING,JSEB_PARAGRAPH,JSEB_DESC,JSEB_SALE,";
                    fields += "JSEB_GST_AMOUNT,JSEB_FMTOPT,JSEB_LOCKED,JSEB_MGNCDE,JSEB_MGNPCT,JSEA_LOCKED";
                    string join = "JOIN CONHDRA ON CO_JOBNUM = JSEB_JOBNUM ";
                    join += "JOIN JCSPEC_A ON JSEA_JOBNUM = JSEB_JOBNUM AND JSEB_SECT = JSEA_SECT AND JSEB_HEADING = JSEA_HEADING";
                    string where = "CO_KEY ='" + contractNumber + "'";
                    where += " AND JSEB_SECT = '" + section + "'";
                    where += " AND JSEB_HEADING = '" + estimateHeading + "'";
                    string query = "SELECT " + fields + " FROM JCSPEC_B " + join + " WHERE " + where;
                    query += " ORDER BY JSEB_PARAGRAPH ASC";
                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            decimal amountEx = (row["JSEB_SALE"] is decimal) ? ((decimal)row["JSEB_SALE"]) : 0;
                            decimal amountGst = (row["JSEB_GST_AMOUNT"] is decimal) ? ((decimal)row["JSEB_GST_AMOUNT"]) : 0;
                            decimal marginPct = (row["JSEB_MGNPCT"] is decimal) ? ((decimal)row["JSEB_MGNPCT"]) : 0;
                            var locked = (row["JSEB_LOCKED"].ToString() == "Y");
                            if (!locked)
                            {
                                locked = (row["JSEA_LOCKED"].ToString() == "Y");
                            }
                            paragraphs.Add(new Paragraph()
                            {
                                Key = row["JSEB_SCRKEY"].ToString(),
                                ContractNumber = row["CO_KEY"].ToString(),
                                Heading = row["JSEB_HEADING"].ToString(),
                                Code = row["JSEB_PARAGRAPH"].ToString(),
                                Description = row["JSEB_DESC"].ToString(),
                                FormatOption = row["JSEB_FMTOPT"].ToString(),
                                Locked = locked,
                                AmountEx = amountEx,
                                AmountGst = amountGst,
                                MarginCode = row["JSEB_MGNCDE"].ToString(),
                                MarginPct = marginPct,
                                OriginalMargin = marginPct
                            });
                        }
                    }
                }
                catch (Exception e)
                {
                    Utils.LogExc(e);
                }
                if (paragraphs != null && paragraphs.Count > 0)
                {
                    VarUtils.Set(WebVars.Paragraphs, paragraphs);
                }
                else
                {
                    return new List<Paragraph>();
                }
            }
            return paragraphs;
        }
        #endregion

        #region ParagraphCombo
        public List<ComboItem> ParagraphCombo(string contractNumber, string estimateHeading, EstimateTypes type)
        {
            var comboParagraphs = new List<ComboItem>();
            foreach (var paragraph in Paragraphs(contractNumber, estimateHeading, type))
            {
                comboParagraphs.Add(new ComboItem(paragraph.Code, paragraph.Description));
            }
            return comboParagraphs;
        }
        #endregion

        #region FindParagraph
        public Paragraph FindParagraph(string code)
        {
            var paragraphs = VarUtils.Get(WebVars.Paragraphs) as List<Paragraph>;

            if (paragraphs == null)
                return null;

            int index = paragraphs.FindIndex(p => p.Code == code);

            if (index < 0)
                return null;

            return paragraphs[index];
        }
        #endregion

        #region EstimateAmount
        public string EstimateAmount(string contractNumber, string estimateHeading, EstimateTypes type)
        {
            var heading = Headings(contractNumber, type).Where(p => p.Code == estimateHeading).FirstOrDefault();
            if (heading == null)
                return "N/A";

            return heading.Amount;
        }
        #endregion

        #region EstimateImage
        public string EstimateImage(EstimateDetail detail, int thumbSize)
        {
            string ImageBase = Utils.ImagesFolder + "products/";
            string[] exts = { ".jpg", ".jpeg", ".png", ".gif" };
            string imageFile = ImageBase + detail.Item;
            bool exists = false;

            foreach (string ext in exts)
            {
                string img = imageFile + ext;
                exists = File.Exists(HttpContext.Current.Server.MapPath(img));
                if (exists)
                {
                    imageFile = img;
                    break;
                }
            }

            //TODO: support jcspec_c image file
            if (!exists)
            {
                return string.Empty;
            }

            if (thumbSize > 0)
            {
                imageFile = "~/Shared/Thumbnail.aspx?image=" + imageFile;
                imageFile += "&width=" + thumbSize.ToString();
                imageFile += "&height=" + thumbSize.ToString();
            }
            return Utils.ResolveUrl(imageFile);
        }
        #endregion

        #region UpdateEstimate
        public bool UpdateEstimate(EstimateDetail detail, EstimateTypes type)
        {
            return UpdateEstimate(detail, false, type);
        }

        public bool UpdateEstimate(EstimateDetail detail, bool delete, EstimateTypes type)
        {
            int mode = 1;

            var contract = GetContract(detail.ContractNumber);
            if (contract == null)
            {
                Utils.SetError("Could not find key: " + detail.ContractNumber);
                return false;
            }

            string[] sections = (type == EstimateTypes.Variation) ?
                Utils.Settings.Comp.VariationEstimateSections :
                Utils.Settings.Comp.SalesEstimateSections;

            string section = sections[0];
            string keyval = string.Empty;

            if (!detail.IsNew)
            {
                //Update
                mode = (delete) ? 3 : 2;
                keyval = detail.Key;
            }

            var contractNumber = detail.ContractNumber;
            var estimateHeading = detail.Heading;

            var amount = string.Empty; //Don't update price
            var gstInc = string.Empty;
            var costAmt = string.Empty;

            if (detail.UnitCost != detail.OriginalCost)
            {
                amount = detail.UnitCost.ToString("F2");
                costAmt = "Y";
            }
            else
            {
                if (detail.UnitAmountEx != detail.OriginalAmountEx)
                {
                    gstInc = "N";
                    amount = detail.UnitAmountEx.ToString("F2");
                }
                else
                {
                    if (detail.UnitAmountInc != detail.OriginalAmountInc)
                    {
                        gstInc = "Y";
                        amount = detail.UnitAmountInc.ToString("F2");
                    }
                }
            }

            var success = BsnUtils.EstimateDetail(ref keyval, ref mode, contract.JobNumber, section, detail, amount, gstInc, costAmt);

            if (delete)
            {
                var heading = GetHeading(contractNumber, estimateHeading, type);
                if (heading != null)
                {
                    BsnUtils.RecalcHeading(heading);
                }
            }

            var refresh = EstimateDetails(contractNumber, estimateHeading, type);

            return success;
        }
        #endregion

        #region TemplateCouncils
        public List<ComboItem> TemplateCouncils
        {
            get
            {
                var list = (List<ComboItem>)VarUtils.Get(WebVars.TemplateCouncils);
                if (list == null)
                {
                    list = GetDropDownList("BMSB_HEADING", "BMSB_DESC", "BMSPEC_B", "BMSB_CODE='" + Utils.Settings.Comp.CouncilCode + "'");
                    if (list != null && list.Count > 0)
                    {
                        VarUtils.Set(WebVars.TemplateCouncils, list);
                    }
                    else
                    {
                        list = new List<ComboItem>();
                    }
                }
                return list;
            }
        }
        #endregion

        #region TemplateEstates
        public List<ComboItem> TemplateEstates
        {
            get
            {
                var list = (List<ComboItem>)VarUtils.Get(WebVars.TemplateEstates);
                if (list == null)
                {
                    list = GetDropDownList("BMSB_HEADING", "BMSB_DESC", "BMSPEC_B", "BMSB_CODE='" + Utils.Settings.Comp.EstateCode + "'");
                    if (list != null && list.Count > 0)
                    {
                        VarUtils.Set(WebVars.TemplateEstates, list);
                    }
                    else
                    {
                        list = new List<ComboItem>();
                    }
                }
                return list;
            }
        }
        #endregion
        
        #region UOMCodes
        public List<ComboItem> UOMCodes
        {
            get
            {
                List<ComboItem> uomCodes = VarUtils.Get(WebVars.UOMCodes) as List<ComboItem>;
                if (uomCodes == null || uomCodes.Count == 0)
                {
                    uomCodes = GetDropDownList("UM_CODE", "UM_DESC", "INVTBLG");
                    if (uomCodes != null && uomCodes.Count > 0)
                    {
                        VarUtils.Set(WebVars.UOMCodes, uomCodes);
                    }
                    else
                    {
                        uomCodes = new List<ComboItem>();
                    }
                }
                return uomCodes;
            }
        }
        #endregion

        #region PriceDisplay
        public List<ComboItem> PriceDisplay
        {
            get
            {
                List<ComboItem> priceDisplay = VarUtils.Get(WebVars.PriceDisplay) as List<ComboItem>;
                if (priceDisplay == null || priceDisplay.Count == 0)
                {
                    priceDisplay = GetDropDownList("BMSE_CODE", "BMSE_DESC", "BMSPEC_E");
                    if (priceDisplay != null && priceDisplay.Count > 0)
                    {
                        VarUtils.Set(WebVars.PriceDisplay, priceDisplay);
                    }
                    else
                    {
                        priceDisplay = new List<ComboItem>();
                    }
                }
                return priceDisplay;
            }
        }
        #endregion

        #region MarginCodes
        public List<ComboItem> MarginCodes
        {
            get
            {
                var marginCodes = (List<ComboItem>)VarUtils.Get(WebVars.MarginCodes);
                if (marginCodes == null || marginCodes.Count == 0)
                {
                    marginCodes = GetDropDownList("BMSD_CODE", "BMSD_DESC", "BMSPEC_D", "", "BMSD_MARGIN");
                    if (marginCodes != null && marginCodes.Count > 0)
                    {
                        VarUtils.Set(WebVars.MarginCodes, marginCodes);
                    }
                    else
                    {
                        marginCodes = new List<ComboItem>();
                    }
                }
                return marginCodes;
            }
        }
        #endregion

        #region AllTemplates
        public List<Template> AllTemplates(string contractNumber)
        {
            List<Template> templates = VarUtils.Get(WebVars.Templates) as List<Template>;
            if (templates == null)
            {
                templates = GetTemplates(contractNumber);
                if (templates != null && templates.Count > 0)
                {
                    VarUtils.Set(WebVars.Templates, templates);
                }
                else
                {
                    return new List<Template>();
                }
            }
            return templates;
        }
        #endregion

        #region TemplateControl
        public TemplateSettings TemplateControl
        {
            get
            {
                var templateControl = (TemplateSettings)VarUtils.Get(WebVars.TemplateControl);
                if (templateControl == null)
                {
                    var list = Utils.GetRecords<TemplateSettings>("JOBTBL2", "CONSTANT=JP02_CODE\t02\t2");
                    if (list != null)
                    {
                        templateControl = list.FirstOrDefault();
                        if (templateControl != null)
                        {
                            VarUtils.Set(WebVars.TemplateControl, templateControl);
                        }
                    }
                    if (templateControl == null)
                    {
                        templateControl = new TemplateSettings();
                    }
                }
                return templateControl;
            }
        }
        #endregion

        #region GetTemplates        
        public List<Template> GetTemplates(string contractNumber)
        {
            string query = "SELECT * FROM JOBTBL_A WHERE JSETA_ACTIVE <> 'N'";
            var Table = RecordUtils.Table(query);
            if (Table == null)
                return null;

            Dictionary<string, string> Group1 = new Dictionary<string, string>();
            foreach (DataRow row in Table.Rows)
            {
                Group1.Add(row["JSETA_CODE"].ToString(), row["JSETA_DESC"].ToString());
            }

            query = "SELECT * FROM JOBTBL_B WHERE JSETB_ACTIVE <> 'N'";
            Table = RecordUtils.Table(query);
            if (Table == null)
                return null;

            Dictionary<string, string> Group2 = new Dictionary<string, string>();
            foreach (DataRow row in Table.Rows)
            {
                Group2.Add(row["JSETB_CODE"].ToString(), row["JSETB_DESC"].ToString());
            }

            decimal.TryParse(TemplateControl.Group3Length, out decimal Group3Length);
            if (Group3Length > 0)
            {
                query = "SELECT * FROM JOBTBL_C WHERE JSETC_ACTIVE <> 'N'";
                Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    Dictionary<string, string> Group3 = new Dictionary<string, string>();
                    foreach (DataRow row in Table.Rows)
                    {
                        Group3.Add(row["JSETC_CODE"].ToString(), row["JSETC_DESC"].ToString());
                    }
                    return GetHeadings(contractNumber, Group1, Group2, Group3);
                }
            }
            return GetHeadings(contractNumber, Group1, Group2, new Dictionary<string, string>());
        }

        private List<Template> GetHeadings(string contractNumber, Dictionary<string, string> Group1,
            Dictionary<string, string> Group2, Dictionary<string, string> Group3)
        {
            //Contract will be used for filter by state

            string query = "SELECT BMSA_CODE,BMSA_DESC FROM BMSPEC_A";
            var Table = RecordUtils.Table(query);
            Dictionary<string, string> templates = new Dictionary<string, string>();
            if (Table != null)
            {
                foreach (DataRow row in Table.Rows)
                {
                    templates.Add(row["BMSA_CODE"].ToString(), row["BMSA_DESC"].ToString());
                }
            }

            query = "SELECT BMSB_SCRKEY,BMSB_DESC FROM BMSPEC_B";
            Table = RecordUtils.Table(query);
            Dictionary<string, string> headings = new Dictionary<string, string>();
            if (Table != null)
            {
                foreach (DataRow row in Table.Rows)
                {
                    headings.Add(row["BMSB_SCRKEY"].ToString(), row["BMSB_DESC"].ToString());
                }
            }

            List<Template> Templates = new List<Template>();
            foreach (var item1 in Group1)
            {
                foreach (var item2 in Group2)
                {
                    if (Group3.Count > 0)
                    {
                        foreach (var item3 in Group3)
                        {
                            string[] template = new string[2] { "", "" };
                            string[] group1 = new string[2] { "", "" };
                            string[] group2 = new string[2] { "", "" };
                            string[] group3 = new string[2] { "", "" };
                            string[] heading = new string[2] { "", "" };
                            string keyval = item1.Key + item2.Key + item3.Key;
                            //Log.Debug("Looking for template: " + keyval);
                            foreach (var item in headings.Where(p => p.Key.StartsWith(keyval)))
                            {
                                template[0] = item.Key;
                                if (templates.ContainsKey(keyval))
                                {
                                    template[1] = templates[keyval];
                                }
                                group1[0] = item1.Key;
                                group1[1] = item1.Value;
                                group2[0] = item2.Key;
                                group2[1] = item2.Value;
                                group3[0] = item3.Key;
                                group3[1] = item3.Value;
                                heading[0] = item.Key.Substring(6);
                                heading[1] = item.Value;
                                //Log.Debug("Found template: " + item.Key + " " + item1.Value + " " + item2.Value + " " + item3.Value);
                                Templates.Add(AddTemplate(template, group1, group2, group3, heading));
                            }
                        }
                    }
                    else
                    {
                        string[] template = new string[2] { "", "" };
                        string[] group1 = new string[2] { "", "" };
                        string[] group2 = new string[2] { "", "" };
                        string[] group3 = new string[2] { "", "" };
                        string[] heading = new string[2] { "", "" };
                        string keyval = item1.Key + item2.Key;
                        foreach (var item in headings.Where(p => p.Key.StartsWith(keyval)))
                        {
                            template[0] = item.Key;
                            if (templates.ContainsKey(keyval))
                            {
                                template[1] = templates[keyval];
                            }
                            group1[0] = item1.Key;
                            group1[1] = item1.Value;
                            group2[0] = item2.Key;
                            group2[1] = item2.Value;
                            heading[0] = item.Key.Substring(6);
                            heading[1] = item.Value;
                            Templates.Add(AddTemplate(template, group1, group2, group3, heading));
                        }
                    }
                }
            }

            return Templates;
        }

        private Template AddTemplate(string[] template, string[] group1, string[] group2, string[] group3, string[] heading)
        {
            return new Template()
            {
                TemplateCode = template[0],
                Description = template[1],
                Group1Code = group1[0],
                Group1Desc = group1[1],
                Group2Code = group2[0],
                Group2Desc = group2[1],
                Group3Code = group3[0],
                Group3Desc = group3[1],
                HeadingCode = heading[0],
                HeadingDesc = heading[1]
            };
        }
        #endregion

        #region Jobs
        public List<dynamic> Jobs
        {
            get
            {
                var jobs = (List<dynamic>)VarUtils.Get(WebVars.Jobs);
                if (jobs == null)
                {
                    jobs = new List<dynamic>();

                    var fields = "JC_JOBNUM,JC_CUST,JC_CUSNAM,JC_AADDR1,JC_AADDR2,JC_AADDR3,JC_STATUS,JC_OPCENTRE,JC_DISTRICT";

                    var query = "SELECT " + fields + " FROM JCHEADA";
                    query += " WHERE { fn LENGTH(JC_JOBNUM)} > 0";

                    //query += Utils.GetFilterString("JC_STATUS", Utils.Settings.Comp.ActiveContractStatusList, 1);

                    //if (!string.IsNullOrEmpty(Utils.Settings.Comp.ContractsQuery))
                        //query += " " + Utils.Settings.Comp.ContractsQuery;

                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            var address = string.Concat(string.Concat(row["JC_AADDR1"].ToString(), row["JC_AADDR2"].ToString()), row["JC_AADDR3"].ToString());
                            jobs.Add(new Job()
                            {
                                JobNumber = row["JC_JOBNUM"].ToString(),
                                CustomerNumber = row["JC_CUST"].ToString(),
                                Name = row["JC_CUSNAM"].ToString(),
                                OpCentre = row["JC_OPCENTRE"].ToString(),
                                District = row["JC_DISTRICT"].ToString(),
                                Address = address,
                                Status = row["JC_STATUS"].ToString()
                            });
                        }
                    }
                    VarUtils.Set(WebVars.Jobs, jobs);
                }
                return jobs;
            }
        }
        #endregion

        #region GetJob
        public Job GetJob(string JobNumber)
        {
            return Jobs.Where(p => p.JobNumber == JobNumber).FirstOrDefault();
        }
        #endregion

        #region GridView
        public BsnGridViewModel GridView
        {
            get
            {
                var grid = new BsnGridViewModel("Jobs", "JobNumber")
                {
                    DataSource = Jobs,
                    ShowSearch = true,
                    ShowPager = true
                };

                int sort = 0;
                grid.Columns.Add(new BsnGridColumn() { Name = "CustomerNumber", FieldName = "CustomerNumber", Caption = "Customer", Sort = sort++ });
                grid.Columns.Add(new BsnGridColumn() { Name = "Name", FieldName = "Name", Caption = "Name", Sort = sort++ });
                grid.Columns.Add(new BsnGridColumn() { Name = "OpCentre", FieldName = "OpCentre", Caption = "Opc", Sort = sort++ });
                grid.Columns.Add(new BsnGridColumn() { Name = "District", FieldName = "District", Caption = "Dist", Sort = sort++ });
                grid.Columns.Add(new BsnGridColumn() { Name = "Address", FieldName = "Address", Caption = "Address", Sort = sort++ });
                grid.Columns.Add(new BsnGridColumn() { Name = "Status", FieldName = "Status", Caption = "Sts", Sort = sort++ });

                return grid;
            }
        }
        #endregion
        
        #region KeyButtons
        List<BsnGridButton> KeyButtons
        {
            get
            {
                var buttonType = Utils.Settings.Comp.TouchMode ? ButtonTypes.Primary : ButtonTypes.Link;
                var edit = new BsnGridButton(new BsnCallback()
                {
                    Type = CallbackType.WindowLocation,
                    Key = Menus.Jobs_Details.ToString(),
                    Value = "?key=[cellvalue]"
                }
                , "fal fa-external-link-square", "cellvalue", true, buttonType);
                return new List<BsnGridButton>() { edit };
            }
        }
        #endregion

    }
}
