﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BsnCraft.Web.Common.Classes.Grid;

namespace BsnCraft.Web.Common.ViewModels
{
    public class ActivitiesViewModel : ViewModelBase
    {
        #region ViewModel
        private ContractsViewModel viewModel;
        private ContractsViewModel ViewModel
        {
            get
            {
                if (viewModel == null)
                {
                    viewModel = new ContractsViewModel();
                }
                return viewModel;
            }
        }
        #endregion

        #region ClearCache
        public void ClearCache()
        {              
            VarUtils.Set(WebVars.Activity, null);
            VarUtils.Set(WebVars.ActivityEventCodes, null);
            VarUtils.Set(WebVars.ActivityContracts, null);            
        }
        #endregion

        #region GridView
        public BsnGridViewModel GridView
        {
            get
            {
                var grid = new BsnGridViewModel("Activities", "ContractNumber")
                {
                    DataSource = Contracts,
                    ShowSearch = true,
                    ShowPager = true
                };

                grid.OptionsColumn.FieldName = "ContractNumber";
                grid.OptionsColumn.Buttons.Add(new BsnGridButton(new BsnCallback() { Id = BsnControls.Edit_Contract, Mode = CallbackMode.Edit }, "fal fa-pencil"));

                grid.KeyColumn.FieldName = "ContractNumber";
                grid.KeyColumn.Caption = Utils.ShowLeads ? "Lead" : "Contract";

                var buttonType = Utils.Settings.Comp.TouchMode ? ButtonTypes.Primary : ButtonTypes.Link;
                var menu = (!Utils.ShowContracts) ? Menus.Leads_Details.ToString() : Menus.Contracts_Details.ToString();
                var button = new BsnGridButton(new BsnCallback()
                {
                    Type = CallbackType.WindowLocation,
                    Key = menu,
                    Value = "?key=[cellvalue]"
                }
                , "fal fa-external-link-square", "[cellvalue]", true, buttonType);
                grid.KeyColumn.Buttons.Add(button);

                int sort = 0;
                grid.Columns.Add(new BsnGridColumn() { Name = "CustomerNumber", FieldName = "CustomerNumber", Caption = "Customer", Sort = sort++ });
                grid.Columns.Add(new BsnGridColumn() { Name = "Name", FieldName = "FriendlyName", Caption = "Name", Sort = sort++ });
                grid.Columns.Add(new BsnGridColumn() { Name = "Address", FieldName = "FullAddress", Caption = "Address", Sort = sort++ });

                if (GetActivity != null)
                {
                    foreach (var evt in GetActivity.Events)
                    {
                        var column = new BsnGridColumn()
                        {
                            Name = evt.Code,
                            FieldName = "Events." + evt.Code + ".Actual",
                            Caption = evt.CodeDesc,
                            Sort = sort++
                        };
                        var register = new BsnGridButton(new BsnCallback()
                        {
                            Id = BsnControls.Edit_Event,
                            Mode = CallbackMode.QuickRegister
                        }
                        , "fal fa-check", "cellvalue", true, ButtonTypes.EventRegister);
                        column.Buttons.Add(register);
                        grid.Columns.Add(column);
                    }
                }

                var ActivityFilter = new BsnGridFilter()
                {
                    Name = "CSA_ACTIVITY",
                    Caption = "Activity",
                    DataSource = Activities,
                    DataKey = "Code",
                    DataCaption = "Desc",
                    DataCount = "Count",
                    MultiSelect = false,
                    ShowCount = true,
                    Type = CallbackType.Normal
                };
                var UsersFilter = new BsnGridFilter()
                {
                    Name = "CSA_USERS",
                    Caption = "Users",
                    Parent = "CSA_ACTIVITY",
                    RequiresParent = true,
                    DataSource = ActivityUsers,
                    DataKey = "Code",
                    DataCaption = "Desc",
                    DataCount = "Count",
                    MultiSelect = true,
                    ShowCount = true,
                    Type = CallbackType.Normal
                };
                grid.Filters.Add(ActivityFilter);
                grid.Filters.Add(UsersFilter);

                return grid;
            }
        }
        #endregion
        
        #region PageFilters
        List<PageFilters> PageFilters
        {
            get
            {
                var activityFilters = (List<PageFilters>)VarUtils.Get(WebVars.ActivityFilter);
                if (activityFilters == null)
                {
                    activityFilters = new List<PageFilters>();
                    VarUtils.Set(WebVars.ActivityFilter, activityFilters);
                }
                return activityFilters;
            }
        }
        #endregion

        #region SetFilters
        public void SetFilters()
        {
            foreach (var filter in Utils.ActiveFilters)
            {
                PageFilters.Add(filter);
            }
        }
        #endregion

        #region ApplyFilters
        public void ApplyFilters()
        {
            foreach (var filter in PageFilters)
            {
                Utils.ActiveFilters.Add(filter);
            }
        }
        #endregion

        #region GetActivity
        public Activity GetActivity
        {
            get
            {
                var activity = (Activity)VarUtils.Get(WebVars.Activity);
                if (activity == null)
                {
                    if (PageFilters.Any(p => p.Key == "CSA_ACTIVITY"))
                    {
                        var selectedActivity = PageFilters.Where(p => p.Key == "CSA_ACTIVITY").FirstOrDefault().Values.FirstOrDefault();
                        activity = Activities.Where(p => p.Code == selectedActivity).FirstOrDefault();
                    }
                    if (activity != null)
                    {
                        VarUtils.Set(WebVars.Activity, activity);
                    } else
                    {
                        activity = new Activity();
                    }
                }
                return activity;
            }
        }
        #endregion

        #region Contracts
        public List<dynamic> Contracts
        {
            get
            {
                var contracts = (List<dynamic>)VarUtils.Get(WebVars.ActivityContracts);
                if (contracts == null || contracts.Count == 0)
                {
                    contracts = new List<dynamic>();
                    if (string.IsNullOrEmpty(GetActivity.Code))
                    {
                        foreach (var contract in ViewModel.Contracts)
                        {
                            contract.Events.Clear();
                            contracts.Add(contract);
                        }

                        var LeadsVM = new LeadsViewModel();
                        foreach (var contract in LeadsVM.Leads)
                        {
                            contract.Events.Clear();
                            contracts.Add(contract);
                        }
                    }
                    else
                    {
                        var fields = "CO_KEY,CSA_CONTRACT";
                        var join = "JOIN CONHDRA ON CSA_CONTRACT = CO_CONTRACT ";
                        var query = "SELECT " + fields + " FROM CONSUMA " + join + " WHERE CSA_ACTIVITY = '" + GetActivity.Code + "'";

                        var combined = Utils.Settings.Comp.ActiveContractStatusList.Concat(Utils.Settings.Comp.ActiveLeadStatusList).ToArray();
                        query += Utils.GetFilterString("CO_STATUS", combined, 1);

                        if (Utils.Settings.Comp.RestrictContracts && !BsnUtils.AllContracts)
                        {
                            query += " AND CO_SALESCONS = '" + BsnUtils.BsnInitials + "'";
                        }

                        var table = RecordUtils.Table(query);
                        if (table != null)
                        {
                            foreach (DataRow row in table.Rows)
                            {
                                var contract = ViewModel.GetContract(row["CO_KEY"].ToString());
                                if (contract != null)
                                {
                                    contract.Events.Clear();
                                    contracts.Add(contract);
                                }
                            }
                        }
                    }

                    LoadContractEvents(contracts);

                    if (contracts.Count > 0)
                    {
                        VarUtils.Set(WebVars.ActivityContracts, contracts);
                    }
                }                
                return contracts;
            }
        }
        #endregion
        
        #region Activities
        public List<Activity> Activities
        {
            get
            {
                var activities = (List<Activity>)VarUtils.Get(WebVars.Activities);
                if (activities == null || activities.Count == 0)
                {
                    activities = new List<Activity>();

                    var fields = "COTM_CODE,COTM_DESC,COTM_USER_SOURCE,COTM_EVENT_USER,CSB_CO_COUNT";
                    var join = "LEFT OUTER JOIN CONSUMB ON CSB_ACTIVITY = COTM_CODE";
                    var query = "SELECT " + fields + " FROM COTABLM " + join;

                    string actList = string.Empty;
                    foreach (var act in Utils.Settings.Comp.ActivitiesFilter)
                    {
                        if (!string.IsNullOrEmpty(act))
                        {
                            actList += "'" + act + "',";
                        }
                    }
                    if (!string.IsNullOrEmpty(actList))
                    {
                        query += " WHERE COTM_CODE IN (" + actList.TrimEnd(',') + ")";
                    }

                    var table = RecordUtils.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            var activity = row["COTM_CODE"].ToString();
                            int.TryParse(row["CSB_CO_COUNT"].ToString(), out int count);
                            activities.Add(new Activity()
                            {
                                Code = activity,
                                Desc = row["COTM_DESC"].ToString(),
                                UserSource = row["COTM_USER_SOURCE"].ToString(),
                                UserEvent = row["COTM_EVENT_USER"].ToString(),
                                Count = count,
                                Events = LoadEventCodes(activity)
                            });
                        }
                    }

                    if (activities.Count > 0)
                    {
                        VarUtils.Set(WebVars.Activities, activities);
                    }
                }
                return activities;
            }
        }
        #endregion

        #region ActivityUsers
        public class ActivityUser
        {
            public string Code { get; set; }
            public string Desc { get; set; }
            public int Count { get; set; }

            public ActivityUser(string code, string desc)
            {
                Code = code;
                Desc = desc;
                Count = 1;
            }
        }
        #endregion

        #region ActivityUsers
        public List<ActivityUser> ActivityUsers
        {
            get
            {
                var activityUsers = new List<ActivityUser>();
                if (string.IsNullOrEmpty(GetActivity.Code))
                    return activityUsers;

                var allUsers = ViewModel.Employees;
                
                var userSource = GetActivity.UserSource;
                var userEvent = GetActivity.UserEvent;
                foreach (Contract contract in Contracts)
                {

                    switch (userSource)
                    {
                        case "C":
                            var custServ= activityUsers.Where(p => p.Code == contract.CustomerService).FirstOrDefault();
                            if (custServ != null)
                            {
                                custServ.Count++;
                            }
                            else
                            {
                                var user = allUsers.Where(p => p.Code == contract.SalesConsultant).FirstOrDefault();
                                if (user != null)
                                {
                                    activityUsers.Add(new ActivityUser(user.Code, user.Desc));
                                }
                            }
                            break;
                        case "B":
                            // Building Supervisor
                            var buildSupv = activityUsers.Where(p => p.Code == contract.BuildingSupervisor).FirstOrDefault();
                            if (buildSupv != null)
                            {
                                buildSupv.Count++;
                            }
                            else
                            {
                                var user = allUsers.Where(p => p.Code == contract.SalesConsultant).FirstOrDefault();
                                if (user != null)
                                {
                                    activityUsers.Add(new ActivityUser(user.Code, user.Desc));
                                }
                            }
                            break;
                        case "S":
                            // Sales Consultant
                            var salesCons = activityUsers.Where(p => p.Code == contract.SalesConsultant).FirstOrDefault();
                            if (salesCons != null)
                            {
                                salesCons.Count++;
                            }
                            else
                            {
                                var user = allUsers.Where(p => p.Code == contract.SalesConsultant).FirstOrDefault();
                                if (user != null)
                                {
                                    activityUsers.Add(new ActivityUser(user.Code, user.Desc));
                                }
                            }
                            break;
                        case "E":
                            // Event Employee
                            if (!contract.Events.ContainsKey(userEvent))
                            {
                                //Bad user setup
                                break;
                            }
                            var contractEvent = (Event)contract.Events[userEvent];
                            var eventUser = activityUsers.Where(p => p.Code == contractEvent.EmpCode).FirstOrDefault();
                            if (eventUser != null)
                            {
                                eventUser.Count++;
                            }
                            else
                            {
                                var user = allUsers.Where(p => p.Code == contractEvent.EmpCode).FirstOrDefault();
                                if (user != null)
                                {
                                    activityUsers.Add(new ActivityUser(user.Code, user.Desc));
                                }
                            }
                            break;
                    }
                }

                return activityUsers;
            }
        }
        #endregion

        #region LoadContractEvents
        void LoadContractEvents(List<dynamic> contracts)
        {
            if (string.IsNullOrEmpty(GetActivity.Code))
                return;
            
            string fields = "CLE_KEY,CLE_SCRKEY,CLE_CONTRACT,CLE_EVENT,CLE_DESC,CLE_REGDATE,CLE_FOREDATE,CLE_DUEDATE,CLE_ACTDATE,";
            fields += "CLE_PCTCMPL,CLE_EMPLOYEE,CLE_AMOUNT,CLE_REF,CO_CONTRACT,CO_KEY,CO_SALEGROUP,COTG_CODE,COTG_NAME,";
            fields += "COTW_CATG,COTW_USERCAT,COTX_EVENT,COTX_GROUP,COTX_SORT_ORDER,CSA_CONTRACT,CSA_ACTIVITY";
            string join = "JOIN CONSUMA ON CSA_CONTRACT = CO_CONTRACT ";
            join += "JOIN CONLINE ON CLE_CONTRACT = CO_CONTRACT ";
            join += "LEFT OUTER JOIN COTABLG ON CLE_EMPLOYEE = COTG_CODE ";
            join += "LEFT OUTER JOIN COTABLX ON CLE_EVENT = COTX_EVENT AND CO_SALEGROUP = COTX_GROUP ";
            join += "LEFT OUTER JOIN COTABLW ON CLE_EVENT = COTW_CODE";
            string where = " CSA_ACTIVITY = '" + GetActivity.Code + "'";

            string evtList = string.Empty;
            foreach (var evt in GetActivity.Events)
            {
                if (!string.IsNullOrEmpty(evt.Code))
                {
                    evtList += "'" + evt.Code + "',";
                }
            }

            var usrEvt = GetActivity.UserEvent;
            if (!string.IsNullOrEmpty(usrEvt) && !GetActivity.Events.Any(p => p.Code == usrEvt))
            {
                evtList += "'" + usrEvt + "',";
            }

            if (!string.IsNullOrEmpty(evtList))
            {
                where += " AND CLE_EVENT IN (" + evtList.TrimEnd(',') + ")";
            }

            string query = "SELECT " + fields + " FROM CONHDRA " + join + " WHERE " + where + " ORDER BY CO_CONTRACT ASC";

            var contract = new Contract();
            var events = new List<Event>();            
            var contractNumber = string.Empty;

            var table = RecordUtils.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    if (row["CO_CONTRACT"].ToString() != contract.ContractNumber)
                    {
                        foreach (var contractEvent in events)
                        {
                            contract.Events.Add(contractEvent.Code, contractEvent);
                        };
                        events.Clear();

                        contractNumber = row["CO_KEY"].ToString();
                        contract = ViewModel.GetContract(contractNumber);
                        if (contract == null)
                        {
                            //Utils.SetError("Contract " + contractNumber + " not found.");
                            contract = new Contract();
                        }
                    }
                    ViewModel.AddEvent(row, events);
                }
                foreach (var contractEvent in events)
                {
                    contract.Events.Add(contractEvent.Code, contractEvent);
                };
            }
        }
        #endregion

        #region LoadEventCodes
        public List<ComboItem> LoadEventCodes(string Activity)
        {
            var activityEvents = (List<ComboItem>)VarUtils.Get(WebVars.ActivityEventCodes);
            if (activityEvents == null || activityEvents.Count == 0)
            {
                activityEvents = new List<ComboItem>();
                var query = "SELECT COTO_EVENT FROM COTABLO WHERE COTO_ACTIVITY = '" + Activity + "'";

                var table = RecordUtils.Table(query);
                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        var contractEvent = ViewModel.GetEventCode(row["COTO_EVENT"].ToString());
                        if (contractEvent != null)
                        {
                            activityEvents.Add(contractEvent);
                        }
                    }
                }

                if (activityEvents.Count > 0)
                {
                    VarUtils.Set(WebVars.ActivityEventCodes, activityEvents);
                }
            }
            return activityEvents;
        }
        #endregion
    }
}
