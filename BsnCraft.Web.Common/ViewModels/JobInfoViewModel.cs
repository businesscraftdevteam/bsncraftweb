﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsnCraft.Web.Common.ViewModels
{
    public class JobInfoViewModel : ViewModelBase
    {
        #region ViewModel
        private ContractsViewModel viewModel;
        private ContractsViewModel ViewModel
        {
            get
            {
                if (viewModel == null)
                {
                    viewModel = new ContractsViewModel();
                }
                return viewModel;
            }
        }
        #endregion

        #region JobInfo
        public List<JobInfo> JobInfo(string contractNumber)
        {
            var jobInfo = (List<JobInfo>)VarUtils.Get(WebVars.JobInfo);
            if (jobInfo == null && !string.IsNullOrEmpty(contractNumber))
            {
                jobInfo = new List<JobInfo>();
                string template = Utils.Settings.Comp.JobInfoTemplate;
                if (string.IsNullOrEmpty(template))
                    return jobInfo;

                var contract = ViewModel.GetContract(contractNumber);

                BsnUtils.JobInfoCopy(contract.JobNumber, template);

                var questions = JobInfoQuestions();
                if (questions.Count == 0)
                    return questions;

                var answers = JobInfoAnswers(contract.JobNumber);

                int key = 0;
                string updateKey = string.Empty;
                string answer = string.Empty;
                string ansDesc = string.Empty;
                foreach (JobInfo question in questions)
                {
                    updateKey = string.Empty;
                    answer = string.Empty;
                    ansDesc = string.Empty;

                    if (answers.ContainsKey(question.Code))
                    {
                        var data = answers[question.Code];
                        updateKey = Utils.GetArray(data, 0);
                        answer = Utils.GetArray(data, 1);
                        ansDesc = Utils.GetArray(data, 2);
                    }

                    //Format Numeric Values
                    if (question.Type == "N" || question.Type == "P")
                    {
                        if (!string.IsNullOrEmpty(answer) && !string.IsNullOrEmpty(question.Dec))
                        {
                            answer = answer.Replace(",", "").Replace(".", "");
                            var dp = int.Parse(question.Dec);
                            var numeric = int.Parse(answer);
                            if (dp == 0)
                            {
                                answer = numeric.ToString();
                            }
                            else
                            {
                                var dpstring = "1000000000";
                                var denominator = int.Parse(dpstring.Substring(0, dp + 1));
                                var dec = Convert.ToDouble(numeric) / denominator;
                                answer = dec.ToString("F" + dp);
                            }
                        }
                    }

                    jobInfo.Add(new JobInfo()
                    {
                        Key = key++.ToString(),
                        Code = question.Code,
                        Category = question.Category,
                        CategoryDesc = question.CategoryDesc,
                        CategorySort = question.CategorySort,
                        Group = question.Group,
                        Prompt = question.Prompt,
                        Type = question.Type,
                        Min = question.Min,
                        Max = question.Max,
                        Dec = question.Dec,
                        Default = question.Default,
                        Answers = question.Answers,
                        UpdateKey = updateKey,
                        Answer = answer,
                        AnsDesc = ansDesc
                    });
                }
                if (jobInfo.Count > 0)
                {
                    var bsnError = string.Empty;
                    foreach (var info in jobInfo)
                    {
                        if (info.Type == "M")
                        {
                            var filename = "JCSPEC_M";
                            var keyval = contract.JobNumber + info.Code;

                            var spec = BsnUtils.GetSpec(filename, keyval);

                            info.Answer = spec;
                        }
                    }
                }
                VarUtils.Set(WebVars.JobInfo, jobInfo);
            }
            return jobInfo;
        }
        #endregion

        #region JobInfo Combo1
        private List<ComboItem> JobInfoCombo1(string category)
        {
            var answers1 = (Dictionary<String, List<ComboItem>>)VarUtils.Get(WebVars.JobInfoAnswers1);
            if (answers1 == null)
            {
                answers1 = new Dictionary<string, List<ComboItem>>();
                var query = "SELECT JUA_CAT,JUA_CATCODE,JUA_DESCR FROM JOBTBLV ORDER BY JUA_CAT ASC";
                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    var answers = new List<ComboItem>();
                    var cat = string.Empty;
                    int i = 0;
                    foreach (DataRow row in Table.Rows)
                    {
                        i++;
                        if (row["JUA_CAT"].ToString() == "**")
                            continue;

                        if (row["JUA_CAT"].ToString() != cat && !string.IsNullOrEmpty(cat))
                        {
                            answers1.Add(cat, answers);
                            answers = new List<ComboItem>();
                        }
                        cat = row["JUA_CAT"].ToString();
                        answers.Add(new ComboItem(row["JUA_CATCODE"].ToString(), row["JUA_DESCR"].ToString()));
                        if (i == Table.Rows.Count)
                        {
                            answers1.Add(cat, answers);
                        }
                    }
                }
                VarUtils.Set(WebVars.JobInfoAnswers1, answers1);
            }
            if (answers1.ContainsKey(category))
            {
                return answers1[category];
            }
            return new List<ComboItem>();
        }
        #endregion

        #region JobInfo Combo2
        private List<ComboItem> JobInfoCombo2(string category)
        {
            var answers2 = (Dictionary<String, List<ComboItem>>)VarUtils.Get(WebVars.JobInfoAnswers2);
            if (answers2 == null)
            {
                answers2 = new Dictionary<string, List<ComboItem>>();
                var query = "SELECT JUB_CAT,JUB_CATCODE,JUB_DESCR FROM JOBTBLW ORDER BY JUB_CAT ASC";
                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    var answers = new List<ComboItem>();
                    var cat = string.Empty;
                    int i = 0;
                    foreach (DataRow row in Table.Rows)
                    {
                        i++;
                        if (row["JUB_CAT"].ToString() == "**")
                            continue;

                        if (row["JUB_CAT"].ToString() != cat && !string.IsNullOrEmpty(cat))
                        {
                            answers2.Add(cat, answers);
                            answers = new List<ComboItem>();
                        }
                        cat = row["JUB_CAT"].ToString();
                        answers.Add(new ComboItem(row["JUB_CATCODE"].ToString(), row["JUB_DESCR"].ToString()));
                        if (i == Table.Rows.Count)
                        {
                            answers2.Add(cat, answers);
                        }
                    }
                }
                VarUtils.Set(WebVars.JobInfoAnswers2, answers2);
            }
            if (answers2.ContainsKey(category))
            {
                return answers2[category];
            }
            return new List<ComboItem>();
        }
        #endregion

        #region JobInfo Combo3
        private List<ComboItem> JobInfoCombo3(string category)
        {
            var answers3 = (Dictionary<String, List<ComboItem>>)VarUtils.Get(WebVars.JobInfoAnswers3);
            if (answers3 == null)
            {
                answers3 = new Dictionary<string, List<ComboItem>>();
                var Table = RecordUtils.Table("SELECT JUC_CAT,JUC_CATCODE,JUC_DESCR FROM JOBTBLX ORDER BY JUC_CAT ASC");
                if (Table != null)
                {
                    var answers = new List<ComboItem>();
                    var cat = string.Empty;
                    int i = 0;
                    foreach (DataRow row in Table.Rows)
                    {
                        i++;
                        if (row["JUC_CAT"].ToString() == "**")
                            continue;


                        if (row["JUC_CAT"].ToString() != cat && !string.IsNullOrEmpty(cat))
                        {
                            answers3.Add(cat, answers);
                            answers = new List<ComboItem>();
                        }
                        cat = row["JUC_CAT"].ToString();
                        answers.Add(new ComboItem(row["JUC_CATCODE"].ToString(), row["JUC_DESCR"].ToString()));
                        if (i == Table.Rows.Count)
                        {
                            answers3.Add(cat, answers);
                        }
                    }
                }
                VarUtils.Set(WebVars.JobInfoAnswers3, answers3);
            }
            if (answers3.ContainsKey(category))
            {
                return answers3[category];
            }
            return new List<ComboItem>();
        }
        #endregion

        #region JobInfo Questions
        private List<JobInfo> JobInfoQuestions()
        {
            var questions = (List<JobInfo>)VarUtils.Get(WebVars.JobInfoQuestions);
            if (questions == null)
            {
                questions = new List<JobInfo>();

                string template = Utils.Settings.Comp.JobInfoTemplate;
                if (string.IsNullOrEmpty(template))
                    return questions;

                string fields = "IDGL_SYMCODE,IDGL_GROUP,IDGL_CAT,IDGL_SORT,IDGC_SORT,JI_CODE,JI_PROMPT,JI_ANSTYPE,";
                fields += "JI_MIN,JI_MAX,JI_DEC,JI_DEF,JI_LNK_TBL,JI_LNK_PRF";
                string join = "JOIN BMSPEC_T ON IDGL_SYMCODE = JI_CODE";
                
                if (Utils.Settings.App.UppercasePassword && string.IsNullOrEmpty(Utils.Settings.Comp.AlternateDSN))
                {
                    join += " JOIN BMSPEC_S ON {fn LEFT(IDGL_CONST,16)} = IDGC_SCRKEY";
                }
                else
                {
                    join += " JOIN BMSPEC_S ON IDGC_SCRKEY = IDGL_CONST";
                }

                string where = "IDGL_SELCODE = '" + template + "'";
                string query = "SELECT " + fields + " FROM JOBTBLU " + join + " WHERE " + where;

                string grouplist = string.Empty;
                foreach (string group in Utils.Settings.Comp.JobInfoGroups)
                {
                    if (!string.IsNullOrEmpty(group))
                    {
                        grouplist += "'" + group + "',";
                    }
                }
                if (!string.IsNullOrEmpty(grouplist))
                {
                    query += " AND IDGL_GROUP IN (" + grouplist.TrimEnd(',') + ")";
                }
                query += " ORDER BY IDGL_SORT";

                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    List<ComboItem> answers;
                    string tbl = string.Empty;
                    string cat = string.Empty;
                    foreach (DataRow row in Table.Rows)
                    {
                        answers = new List<ComboItem>();
                        tbl = row["JI_LNK_TBL"].ToString();
                        cat = row["JI_LNK_PRF"].ToString();
                        if (!string.IsNullOrEmpty(cat))
                        {

                            switch (tbl)
                            {
                                case "1":
                                    answers = JobInfoCombo1(cat);
                                    break;
                                case "2":
                                    answers = JobInfoCombo2(cat);
                                    break;
                                case "3":
                                    answers = JobInfoCombo3(cat);
                                    break;
                            }
                        }

                        var category = row["IDGL_CAT"].ToString();
                        var categoryDesc = string.Empty;
                        var categorySort = row["IDGC_SORT"].ToString();
                        if (!string.IsNullOrEmpty(category) &&
                            JobInfoCategories.ContainsKey(category))
                        {
                            var data = JobInfoCategories[category];
                            categoryDesc = Utils.GetArray(data, 0);
                            if (string.IsNullOrEmpty(categorySort))
                            {
                                categorySort = Utils.GetArray(data, 1);
                            }
                        }

                        questions.Add(new JobInfo()
                        {
                            Code = row["JI_CODE"].ToString(),
                            Group = row["IDGL_GROUP"].ToString(),
                            Category = category,
                            CategoryDesc = categoryDesc,
                            CategorySort = (string.IsNullOrEmpty(categorySort)) ? category : categorySort,
                            QuestionSort = row["IDGL_SORT"].ToString(),
                            Prompt = row["JI_PROMPT"].ToString(),
                            Type = row["JI_ANSTYPE"].ToString(),
                            Min = row["JI_MIN"].ToString(),
                            Max = row["JI_MAX"].ToString(),
                            Dec = row["JI_DEC"].ToString(),
                            Default = row["JI_DEF"].ToString(),
                            Answers = answers
                        });
                    }
                }
                VarUtils.Set(WebVars.JobInfoQuestions, questions);
            }
            return questions;
        }
        #endregion

        #region JobInfo Groups
        public Dictionary<string, string[]> JobInfoGroups(string jobNumber)
        {
            var groups = (Dictionary<string, string[]>)VarUtils.Get(WebVars.JobInfoGroups);
            if (groups == null)
            {
                groups = new Dictionary<string, string[]>();

                var fields = "JSLP_GROUP,JSLP_DESC,JSLP_SORT";
                var where = "JSLP_JOBNUM = '" + jobNumber + "'";
                var query = "SELECT " + fields + " FROM JCSLINP WHERE " + where;

                string grouplist = string.Empty;
                foreach (string group in Utils.Settings.Comp.JobInfoGroups)
                {
                    if (!string.IsNullOrEmpty(group))
                    {
                        grouplist += "'" + group + "',";
                    }
                }
                if (!string.IsNullOrEmpty(grouplist))
                {
                    query += " AND JSLP_GROUP IN (" + grouplist.TrimEnd(',') + ")";
                }

                var Table = RecordUtils.Table(query);
                if (Table != null)
                {
                    foreach (DataRow row in Table.Rows)
                    {
                        var data = new string[2];
                        data[0] = row["JSLP_GROUP"].ToString();
                        data[1] = row["JSLP_DESC"].ToString();
                        var key = row["JSLP_SORT"].ToString();
                        if (string.IsNullOrEmpty(key))
                        {
                            key = data[0];
                        }
                        groups.Add(key, data);
                    }
                }
                VarUtils.Set(WebVars.JobInfoGroups, groups);
            }
            return groups;
        }
        #endregion

        #region JobInfo Categories
        public Dictionary<string, string[]> JobInfoCategories
        {
            get
            {
                var categories = (Dictionary<string, string[]>)VarUtils.Get(WebVars.JobInfoCategories);
                if (categories == null)
                {
                    categories = new Dictionary<string, string[]>();

                    var fields = "JBT_H_CAT,JBT_H_GROUP,JBT_H_DESC,JBT_H_SORT";
                    var query = "SELECT " + fields + " FROM JOBTBL_H";

                    string grouplist = string.Empty;
                    foreach (string group in Utils.Settings.Comp.JobInfoGroups)
                    {
                        if (!string.IsNullOrEmpty(group))
                        {
                            grouplist += "'" + group + "',";
                        }
                    }
                    if (!string.IsNullOrEmpty(grouplist))
                    {
                        query += " WHERE JBT_H_GROUP IN (" + grouplist.TrimEnd(',') + ")";
                    }

                    var Table = RecordUtils.Table(query);
                    if (Table != null)
                    {
                        foreach (DataRow row in Table.Rows)
                        {
                            var data = new string[3];
                            data[0] = row["JBT_H_DESC"].ToString();
                            data[1] = row["JBT_H_SORT"].ToString();
                            data[2] = row["JBT_H_GROUP"].ToString();
                            categories.Add(row["JBT_H_CAT"].ToString(), data);
                        }
                    }
                    VarUtils.Set(WebVars.JobInfoCategories, categories);
                }
                return categories;
            }
        }
        #endregion

        #region JobInfo Answers
        private Dictionary<string, string[]> JobInfoAnswers(string jobNumber)
        {
            var answers = new Dictionary<string, string[]>();

            var fields = "JSI_KEY,JSI_JOBNUM,JSI_CODE,JSI_ANSWER,JSI_ANS_DESC";
            var where = "JSI_JOBNUM ='" + jobNumber + "'";
            var query = "SELECT " + fields + " FROM JCSPECW WHERE " + where;

            var Table = RecordUtils.Table(query);
            if (Table != null)
            {
                foreach (DataRow row in Table.Rows)
                {
                    var data = new string[3];
                    data[0] = row["JSI_KEY"].ToString();
                    data[1] = row["JSI_ANSWER"].ToString();
                    data[2] = row["JSI_ANS_DESC"].ToString();
                    answers.Add(row["JSI_CODE"].ToString(), data);
                }
            }

            return answers;
        }
        #endregion

        #region FindJobInfo
        public JobInfo FindJobInfo(string key)
        {
            var jobInfo = (List<JobInfo>)VarUtils.Get(WebVars.JobInfo);
            if (jobInfo == null)
                return null;

            return jobInfo.Where(p => p.Key == key).FirstOrDefault();
        }
        #endregion

        #region UpdateJobInfo
        public void UpdateJobInfo(string contractNumber)
        {
            try
            {
                var jobInfo = (List<JobInfo>)VarUtils.Get(WebVars.JobInfo);
                if (jobInfo == null)
                {
                    Utils.SetError("Error saving Job Information. Questions not found.");
                }
                var contract = ViewModel.GetContract(contractNumber);
                if (contract == null)
                {
                    Utils.SetError("Could not find key: " + contractNumber);
                    return;
                }

                bool updated = false;
                foreach (JobInfo info in jobInfo)
                {
                    if (info.Modified)
                    {
                        updated = true;
                        if (info.Type == "M")
                        {
                            var filename = "JCSPEC_M";
                            var keyval = contract.JobNumber + info.Code;
                            BsnUtils.SaveSpec(filename, keyval, info.Answer);

                            //JAC 25-Jan-2018 Leave answer as Job Info collection is not re-read every save
                            //info.Answer = string.Empty;

                        }

                        if (!string.IsNullOrEmpty(info.UpdateKey))
                        {
                            BsnUtils.JobInfoUpdate(info.UpdateKey, info.Answer);
                        }
                        else
                        {
                            BsnUtils.JobInfoAdd(contract.JobNumber, info.Group, info.Category, info.Code, info.Answer);
                        }
                        info.Modified = false;
                    }
                }
                if (updated)
                {
                    Utils.SetSuccess();
                    //VarUtils.Set(WebVars.JobInfo, null);
                }
            }
            catch (Exception e)
            {
                Utils.LogExc(e);
            }
        }
        #endregion
    }
}
