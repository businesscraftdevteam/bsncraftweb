﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BsnCraft.Web.Common.Controllers
{
    public class ItemsViewModel : BsnViewModel
    {
        #region Constructor
        public ItemsViewModel(BsnApplication app) : base(app, Base.Controllers.Items)
        {

        }
        #endregion

        #region ItemList
        private List<ComboItem> itemList;
        public List<ComboItem> ItemList
        {
            get
            {
                if (itemList == null || itemList.Count == 0)
                {
                    itemList = new List<ComboItem>();
                    string filter = "WHERE OBSFLG='A'";
                    var fields = "ITEMNO,DESCR,EDESC";
                    var query = "SELECT " + fields + " FROM ITMMASA " + filter;
                    query += " " + App.Settings.Comp(CompanySettingsType.ItemsQuery).ToString();
                    query = query.TrimEnd();
                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            itemList.Add(new ComboItem(row["ITEMNO"].ToString(), row["DESCR"].ToString() + row["EDESC"].ToString()));
                        }
                    }
                }
                return itemList;
            }
        }
        #endregion
                
        #region Items
        private List<Item> items;
        public List<Item> Items
        {
            get
            {
                if (items == null || items.Count == 0)
                {
                    items = new List<Item>();
                    string filter = "WHERE OBSFLG='A'";
                    var fields = "ITEMNO,PRDCAT,SUPLCD,DESCR,EDESC,USRDEF,SUOFM";
                    var query = "SELECT " + fields + " FROM ITMMASA " + filter;
                    query += " " + App.Settings.Comp(CompanySettingsType.ItemsQuery).ToString();
                    query = query.TrimEnd();
                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            items.Add(new Item()
                            {
                                ItemNumber = row["ITEMNO"].ToString(),
                                Description = row["DESCR"].ToString(),
                                ExtraDescription = row["EDESC"].ToString()
                            });
                        }
                    }
                }
                return items;
            }            
        }
        #endregion
        
        #region GetSelectionItemList
        public List<ComboItem> GetSelectionItemList(string selectionList)
        {
            var list = new List<ComboItem>();
            if (string.IsNullOrEmpty(selectionList))
            {
                list = ItemList;
            }
            else
            {
                string[] selections = selectionList.Split(',');
                foreach (var sel in selections)
                {
                    List<Item> items = GetSelection(sel);
                    if (items != null)
                    {
                        foreach (var item in items)
                        {
                            list.Add(new ComboItem(item.ItemNumber, item.Description + item.ExtraDescription));
                        }
                    }
                }
            }
            return list;
        }
        #endregion

        #region GetSelection
        private Dictionary<string, List<Item>> selectionItems;
        public List<Item> GetSelection(string selection)
        {
            if (selectionItems == null)
            {
                selectionItems = new Dictionary<string, List<Item>>();
            }

            if (string.IsNullOrEmpty(selection))
            {
                return items;
            }

            if (selectionItems.ContainsKey(selection))
            {
                return selectionItems[selection];
            }

            var filteredItems = new List<Item>();
            string filter = "WHERE OBSFLG='A'";
            var fields = "ITEMNO,PRDCAT,SUPLCD,DESCR,EDESC,USRDEF,SUOFM";
            var query = "SELECT " + fields + " FROM ITMMASA " + filter;
            if (!string.IsNullOrEmpty(selection))
            {
                var itemSelection = ItemSelections.Where(p => p.Code.ToUpper().Equals(selection.ToUpper())).FirstOrDefault();
                if (itemSelection != null)
                {
                    if (itemSelection.Option == "L")
                    {
                        var list = itemSelection.Query;
                        if (!itemSelection.Query.Contains("'"))
                        {
                            //Add quotes
                            var itemlist = list.Split(',').Select(x => string.Format("'{0}'", x)).ToList();
                            list = string.Join(",", itemlist);
                        }
                        list = list.TrimEnd(',');
                        query += " AND ITEMNO IN (" + list + ")";
                    }
                    else
                    {
                        query += " AND " + itemSelection.Query;
                    }
                }
                else
                {
                    if (selection.ToUpper().Contains(" LIKE ") || selection.Contains("="))
                    {
                        query += " AND " + selection.Replace("\r\n", "");
                    }
                    else
                    {
                        query += " AND ITEMNO LIKE '" + selection.Replace("\r\n", "").ToUpper() + "%'";
                    }
                }
            }
            query += " " + App.Settings.Comp(CompanySettingsType.ItemsQuery).ToString();
            query = query.TrimEnd();
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    filteredItems.Add(new Item()
                    {
                        ItemNumber = row["ITEMNO"].ToString(),
                        Description = row["DESCR"].ToString(),
                        ExtraDescription = row["EDESC"].ToString()
                    });
                }
                selectionItems.Add(selection, filteredItems);
            }
            return filteredItems;
        }
        #endregion

        #region Lookup Items
        public List<FieldData> LookupItems(string query)
        {
            query = query.ToUpper();
            var match = Items.Where(p => p.ItemNumber.ToUpper().Contains(query) ||
                p.Description.ToUpper().Contains(query) ||
                p.ExtraDescription.ToUpper().Contains(query)).Take(10);

            var results = new List<FieldData>();
            foreach (var item in match)
            {
                var value = item.ItemNumber + " | " + item.Description + " | " + item.ExtraDescription;
                results.Add(new FieldData(value, JsonConvert.SerializeObject(item)));
            }
            return results;
        }
        #endregion

        #region Item Selections
        private List<ItemSelection> itemSelections;
        public List<ItemSelection> ItemSelections
        {
            get
            {
                if (itemSelections == null || itemSelections.Count == 0)
                {
                    itemSelections = App.Utils.GetRecords<ItemSelection>("BMSPEC_U", "");
                }
                return itemSelections;
            }
        }
        #endregion

        #region Find Selection
        public ItemSelection FindSelection(string code)
        {
            return ItemSelections.Where(p => p.Code == code).FirstOrDefault();
        }
        #endregion

        #region Selection Options
        public List<ComboItem> SelectionOptions
        {
            get
            {
                var options = new List<ComboItem>
                {
                    new ComboItem("Q", "Query"),
                    new ComboItem("L", "List")
                };
                return options;
            }
        }
        #endregion

        #region Update Selection
        public void UpdateSelection(ItemSelection selection, bool delete = false)
        {
            string extras = (delete) ? "DELETE=YES" : "OPTION=" + selection.Option + ",DESC=" + selection.Description;
            App.Api.UpdateItemSelection(selection.Code, selection.Query, ref extras);
            itemSelections.Clear();
        }
        #endregion

        #region Houses
        private List<Item> houses;
        public List<Item> Houses
        {
            get
            {
                if (houses == null || houses.Count == 0)
                {
                    houses = new List<Item>();
                    string filter = "WHERE OBSFLG='A'";
                    var fields = "ITEMNO,PRDCAT,SUPLCD,DESCR,EDESC,USRDEF,SUOFM,PC_GROUP";
                    var join = "JOIN INVTBLB ON PC_CATEGORY = PRDCAT";
                    var query = "SELECT " + fields + " FROM ITMMASA " + join + " " + filter;
                    
                    if (!string.IsNullOrEmpty(App.VM.CO.Settings.HouseFilterCode))
                    {
                        if (App.VM.CO.Settings.HouseFilterType == "G")
                        {
                            query += " AND PC_GROUP = '" + App.VM.CO.Settings.HouseFilterCode + "'";
                        }
                        else
                        {
                            query += " AND PRDCAT = '" + App.VM.CO.Settings.HouseFilterCode + "'";
                        }
                    }
                    

                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            houses.Add(new Item()
                            {
                                ItemNumber = row["ITEMNO"].ToString(),
                                Description = row["DESCR"].ToString(),
                                ExtraDescription = row["EDESC"].ToString()
                            });
                        }
                    }
                }
                return houses;
            }
        }
        #endregion

        #region Lookup Houses
        public List<FieldData> LookupHouses(string query)
        {
            query = query.ToUpper();
            var match = Houses.Where(p => p.ItemNumber.ToUpper().Contains(query) ||
                p.Description.ToUpper().Contains(query) ||
                p.ExtraDescription.ToUpper().Contains(query)).Take(10);

            var results = new List<FieldData>();
            foreach (var item in match)
            {
                var value = item.ItemNumber + " | " + item.Description + " | " + item.ExtraDescription;
                results.Add(new FieldData(value, JsonConvert.SerializeObject(item)));
            }
            return results;
        }
        #endregion

        #region Facades
        private List<Item> facades;
        public List<Item> Facades
        {
            get
            {
                if (facades == null || facades.Count == 0)
                {
                    facades = new List<Item>();
                    string filter = "WHERE OBSFLG='A'";
                    var fields = "ITEMNO,PRDCAT,SUPLCD,DESCR,EDESC,USRDEF,SUOFM,PC_GROUP";
                    var join = "JOIN INVTBLB ON PC_CATEGORY = PRDCAT";
                    var query = "SELECT " + fields + " FROM ITMMASA " + join + " " + filter;

                    if (!string.IsNullOrEmpty(App.VM.CO.Settings.FacadeFilterCode))
                    {
                        if (App.VM.CO.Settings.FacadeFilterType == "G")
                        {
                            query += " AND PC_GROUP = '" + App.VM.CO.Settings.FacadeFilterCode + "'";
                        }
                        else
                        {
                            query += " AND PRDCAT = '" + App.VM.CO.Settings.FacadeFilterCode + "'";
                        }
                    }

                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            facades.Add(new Item()
                            {
                                ItemNumber = row["ITEMNO"].ToString(),
                                Description = row["DESCR"].ToString(),
                                ExtraDescription = row["EDESC"].ToString()
                            });
                        }
                    }
                }
                return facades;
            }
        }
        #endregion

        #region Lookup Facades
        public List<FieldData> LookupFacades(string query)
        {
            query = query.ToUpper();
            var match = Facades.Where(p => p.ItemNumber.ToUpper().Contains(query) ||
                p.Description.ToUpper().Contains(query) ||
                p.ExtraDescription.ToUpper().Contains(query)).Take(10);

            var results = new List<FieldData>();
            foreach (var item in match)
            {
                var value = item.ItemNumber + " | " + item.Description + " | " + item.ExtraDescription;
                results.Add(new FieldData(value, JsonConvert.SerializeObject(item)));
            }
            return results;
        }
        #endregion
    }
}
