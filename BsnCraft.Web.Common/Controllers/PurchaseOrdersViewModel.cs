﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BsnCraft.Web.Common.Controllers
{
    public class PurchaseOrdersViewModel : BsnViewModel
    {
        public PurchaseOrdersViewModel(BsnApplication app) : base(app, Base.Controllers.PurchaseOrders)
        {

        }

        #region PurchaseOrders
        public List<PurchaseOrder> GetPurchaseOrders(Contract contract)
        {            
            var purchaseOrders = new List<PurchaseOrder>();
            var fields = "PO_PONUM,PO_JOBNUM,PO_VENNO,PO_VENNAME,PO_ADD1,PO_ADD2,PO_CITY,PO_STATE,PO_PCODE,PO_CSTCTR,PO_COMPLETE,PO_DATE,PO_ISSU_VAL";
            var query = "SELECT " + fields + " FROM POHEADA";
            query += " WHERE PO_JOBNUM = '" + contract.JobNumber + "'";                
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var date = (row["PO_DATE"] is DateTime) ? ((DateTime)row["PO_DATE"]) : DateTime.MinValue;
                    string address = string.Concat(row["PO_ADD1"].ToString(), row["PO_ADD2"].ToString());
                    address = string.Concat(address, row["PO_ADD1"].ToString());
                    address = string.Concat(address, row["PO_STATE"].ToString());
                    address = string.Concat(address, row["PO_PCODE"].ToString());
                    string issued = (row["PO_ISSU_VAL"] is Decimal) ?
                        ((((Decimal)row["PO_ISSU_VAL"]) != 0) ?
                        ((Decimal)row["PO_ISSU_VAL"]).ToString("C") :
                        string.Empty) :
                        string.Empty;
                    purchaseOrders.Add(new PurchaseOrder()
                    {
                        PONumber = row["PO_PONUM"].ToString(),
                        Date = date,
                        Status = row["PO_COMPLETE"].ToString(),
                        JobNumber = row["PO_JOBNUM"].ToString(),
                        Vendor = row["PO_VENNO"].ToString(),
                        VendorName = row["PO_VENNAME"].ToString(),
                        Address = address,
                        CostCentre = row["PO_CSTCTR"].ToString(),
                        Issued = issued
                    });
                }
            }             
            return purchaseOrders;
        }
        #endregion        
    }
}
