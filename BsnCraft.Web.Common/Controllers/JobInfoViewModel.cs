﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BsnCraft.Web.Common.Controllers
{
    public class JobInfoViewModel : BsnViewModel
    {
        #region Constructor
        public JobInfoViewModel(BsnApplication app) : base(app, Base.Controllers.JobInfo)
        {

        }
        #endregion

        #region JobInfo
        public List<JobInfo> GetJobInfo(Contract contract)
        {
            var jobInfo = new List<JobInfo>();
            string template = App.Settings.Comp(CompanySettingsType.JobInfoTemplate).ToString();
            if (string.IsNullOrEmpty(template))
            {
                App.Utils.SetError("Please specify a Job Information Template in Settings");
                return jobInfo;
            }
            App.Api.JobInfoCopy(contract.JobNumber, template);
            var questions = JobInfoQuestions;
            if (questions.Count == 0)
            {
                return questions;
            }
            var answers = GetJobInfoAnswers(contract);
            int key = 0;
            string updateKey = string.Empty;
            string answer = string.Empty;
            string ansDesc = string.Empty;
            foreach (JobInfo question in questions)
            {
                updateKey = string.Empty;
                answer = string.Empty;
                ansDesc = string.Empty;
                if (answers.ContainsKey(question.Code))
                {
                    var data = answers[question.Code];
                    updateKey = Helpers.GetArray(data, 0);
                    answer = Helpers.GetArray(data, 1);
                    ansDesc = Helpers.GetArray(data, 2);
                }
                //Format Numeric Values
                if (question.Type == "N" || question.Type == "P")
                {
                    if (!string.IsNullOrEmpty(answer) && !string.IsNullOrEmpty(question.Dec))
                    {
                        answer = answer.Replace(",", "").Replace(".", "");
                        var dp = int.Parse(question.Dec);
                        var numeric = int.Parse(answer);
                        if (dp == 0)
                        {
                            answer = numeric.ToString();
                        }
                        else
                        {
                            var dpstring = "1000000000";
                            var denominator = int.Parse(dpstring.Substring(0, dp + 1));
                            var dec = Convert.ToDouble(numeric) / denominator;
                            answer = dec.ToString("F" + dp);
                        }
                    }
                }
                jobInfo.Add(new JobInfo(contract)
                {
                    Key = key++.ToString(),
                    Code = question.Code,
                    Category = question.Category,
                    CategoryDesc = question.CategoryDesc,
                    CategorySort = question.CategorySort,
                    Group = question.Group,
                    Prompt = question.Prompt,
                    Type = question.Type,
                    Min = question.Min,
                    Max = question.Max,
                    Dec = question.Dec,
                    Default = question.Default,
                    LinkPrefix = question.LinkPrefix,
                    LinkTable = question.LinkTable,
                    UpdateKey = updateKey,
                    Answer = answer,
                    AnsDesc = ansDesc
                });
            }
            if (jobInfo.Count > 0)
            {
                var bsnError = string.Empty;
                foreach (var info in jobInfo)
                {
                    if (info.Type == "M")
                    {
                        var filename = "JCSPEC_M";
                        var keyval = contract.JobNumber + info.Code;
                        var spec = App.Api.GetSpec(filename, keyval);
                        info.Answer = spec;
                    }
                }
            }                
            return jobInfo;
        }
        #endregion

        #region JobInfo Combo1
        Dictionary<string, List<ComboItem>> jobInfoAnswers1;
        private List<ComboItem> JobInfoCombo1(string category, List<string> filters)
        {
            if (jobInfoAnswers1 == null)
            {
                jobInfoAnswers1 = new Dictionary<string, List<ComboItem>>();
            }

            filters = filters.Where(p => !jobInfoAnswers1.ContainsKey(p)).ToList();
            foreach (var cat in filters)
            {
                jobInfoAnswers1.Add(cat, new List<ComboItem>());
            }
            if (filters.Count > 0)
            {
                filters = filters.ToQuotedList();
                var fields = "JUA_CAT,JUA_CATCODE,JUA_DESCR";
                var where = " WHERE JUA_CAT IN (" + string.Join(",", filters) + ")";
                var order = " ORDER BY JUA_CAT ASC";
                var query = "SELECT " + fields + " FROM JOBTBLV" + where + order;
                var table = App.Odbc.Table(query);
                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        var answers = jobInfoAnswers1[row["JUA_CAT"].ToString()];
                        answers.Add(new ComboItem(row["JUA_CATCODE"].ToString(), row["JUA_DESCR"].ToString()));
                    }
                }
            }
            if (jobInfoAnswers1.ContainsKey(category))
            {
                return jobInfoAnswers1[category];
            }
            return new List<ComboItem>();
        }
        #endregion

        #region JobInfo Combo2
        Dictionary<string, List<ComboItem>> jobInfoAnswers2;
        private List<ComboItem> JobInfoCombo2(string category, List<string> filters)
        {
            if (jobInfoAnswers2 == null)
            {
                jobInfoAnswers2 = new Dictionary<string, List<ComboItem>>();
            }

            filters = filters.Where(p => !jobInfoAnswers2.ContainsKey(p)).ToList();
            foreach (var cat in filters)
            {
                jobInfoAnswers2.Add(cat, new List<ComboItem>());
            }
            if (filters.Count > 0)
            {
                filters = filters.ToQuotedList();
                var fields = "JUB_CAT,JUB_CATCODE,JUB_DESCR";
                var where = " WHERE JUB_CAT IN (" + string.Join(",", filters) + ")";
                var order = " ORDER BY JUB_CAT ASC";
                var query = "SELECT " + fields + " FROM JOBTBLW" + where + order;
                var table = App.Odbc.Table(query);
                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        var answers = jobInfoAnswers1[row["JUB_CAT"].ToString()];
                        answers.Add(new ComboItem(row["JUB_CATCODE"].ToString(), row["JUB_DESCR"].ToString()));
                    }
                }
            }
            if (jobInfoAnswers2.ContainsKey(category))
            {
                return jobInfoAnswers2[category];
            }
            return new List<ComboItem>();
        }
        #endregion

        #region JobInfo Combo3
        Dictionary<string, List<ComboItem>> jobInfoAnswers3;
        private List<ComboItem> JobInfoCombo3(string category, List<string> filters)
        {
            if (jobInfoAnswers3 == null)
            {
                jobInfoAnswers3 = new Dictionary<string, List<ComboItem>>();
            }

            filters = filters.Where(p => !jobInfoAnswers3.ContainsKey(p)).ToList();
            foreach (var cat in filters)
            {
                jobInfoAnswers3.Add(cat, new List<ComboItem>());
            }
            if (filters.Count > 0)
            {
                filters = filters.ToQuotedList();
                var fields = "JUC_CAT,JUC_CATCODE,JUC_DESCR";
                var where = " WHERE JUC_CAT IN (" + string.Join(",", filters) + ")";
                var order = " ORDER BY JUC_CAT ASC";
                var query = "SELECT " + fields + " FROM JOBTBLX" + where + order;
                var table = App.Odbc.Table(query);
                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        var answers = jobInfoAnswers1[row["JUC_CAT"].ToString()];
                        answers.Add(new ComboItem(row["JUC_CATCODE"].ToString(), row["JUC_DESCR"].ToString()));
                    }
                }
            }
            if (jobInfoAnswers3.ContainsKey(category))
            {
                return jobInfoAnswers3[category];
            }
            return new List<ComboItem>();
        }
        #endregion

        #region JobInfo Categories
        private Dictionary<string, string[]> jobInfoCategories;
        public Dictionary<string, string[]> JobInfoCategories
        {
            get
            {
                if (jobInfoCategories == null)
                {
                    jobInfoCategories = new Dictionary<string, string[]>();

                    var fields = "JBT_H_CAT,JBT_H_GROUP,JBT_H_DESC,JBT_H_SORT";
                    var query = "SELECT " + fields + " FROM JOBTBL_H";

                    var list = App.Settings.Comp(CompanySettingsType.JobInfoGroups).ToQuotedList();
                    if (list.Count > 0)
                    {
                        query += " WHERE JBT_H_GROUP IN (" + string.Join(",", list) + ")";
                    }

                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            var data = new string[3];
                            data[0] = row["JBT_H_DESC"].ToString();
                            data[1] = row["JBT_H_SORT"].ToString();
                            data[2] = row["JBT_H_GROUP"].ToString();
                            jobInfoCategories.Add(row["JBT_H_CAT"].ToString(), data);
                        }
                    }
                }
                return jobInfoCategories;
            }
        }
        #endregion

        #region JobInfo Questions
        private List<JobInfo> jobInfoQuestions;
        private List<JobInfo> JobInfoQuestions
        {
            get
            {
                if (jobInfoQuestions == null)
                {
                    jobInfoQuestions = new List<JobInfo>();
                    var fields = "IDGL_SYMCODE,IDGL_GROUP,IDGL_CAT,IDGL_SORT,IDGC_SORT,JI_CODE,JI_PROMPT,JI_ANSTYPE,";
                    fields += "JI_MIN,JI_MAX,JI_DEC,JI_DEF,JI_LNK_TBL,JI_LNK_PRF";
                    var join = "JOIN BMSPEC_T ON IDGL_SYMCODE = JI_CODE";
                    //TODO: check this genesis change still working
                    //join += " JOIN BMSPEC_S ON {fn LEFT(IDGL_CONST,16)} = IDGC_SCRKEY";
                    join += " JOIN BMSPEC_S ON IDGC_SCRKEY = IDGL_CONST";
                    var query = "SELECT " + fields + " FROM JOBTBLU " + join;
                    var template = App.Settings.Comp(CompanySettingsType.JobInfoTemplate).ToString();
                    if (!string.IsNullOrEmpty(template))
                    {
                        query += " WHERE IDGL_SELCODE = '" + template + "'";
                    }
                    var list = App.Settings.Comp(CompanySettingsType.JobInfoGroups).ToQuotedList();
                    if (list.Count > 0)
                    {
                        query += " AND IDGL_GROUP IN (" + string.Join(",", list) + ")";
                    }
                    query += " ORDER BY IDGL_SORT";
                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            var category = row["IDGL_CAT"].ToString();
                            var categoryDesc = string.Empty;
                            var categorySort = row["IDGC_SORT"].ToString();
                            if (!string.IsNullOrEmpty(category) &&
                                JobInfoCategories.ContainsKey(category))
                            {
                                var data = JobInfoCategories[category];
                                categoryDesc = Helpers.GetArray(data, 0);
                                if (string.IsNullOrEmpty(categorySort))
                                {
                                    categorySort = Helpers.GetArray(data, 1);
                                }
                            }
                            jobInfoQuestions.Add(new JobInfo(null)
                            {
                                Code = row["JI_CODE"].ToString(),
                                Group = row["IDGL_GROUP"].ToString(),
                                Category = category,
                                CategoryDesc = categoryDesc,
                                CategorySort = (string.IsNullOrEmpty(categorySort)) ? category : categorySort,
                                QuestionSort = row["IDGL_SORT"].ToString(),
                                Prompt = row["JI_PROMPT"].ToString(),
                                Type = row["JI_ANSTYPE"].ToString(),
                                Min = row["JI_MIN"].ToString(),
                                Max = row["JI_MAX"].ToString(),
                                Dec = row["JI_DEC"].ToString(),
                                Default = row["JI_DEF"].ToString(),
                                LinkTable = row["JI_LNK_TBL"].ToString(),
                                LinkPrefix = row["JI_LNK_PRF"].ToString()
                            });
                        }
                    }
                }
                return jobInfoQuestions;
            }            
        }
        #endregion

        #region Load Answers
        public void LoadAnswers(List<JobInfo> jobInfo)
        {
            if (jobInfo.Any(p => !p.Answers.IsNull()))
            {
                return;
            }            
            var filter1 = jobInfo.Where(p => p.LinkTable.Equals("1")).Select(x => x.LinkPrefix).Distinct().ToList();
            var filter2 = jobInfo.Where(p => p.LinkTable.Equals("2")).Select(x => x.LinkPrefix).Distinct().ToList();
            var filter3 = jobInfo.Where(p => p.LinkTable.Equals("3")).Select(x => x.LinkPrefix).Distinct().ToList();
            foreach (var info in jobInfo)
            {
                if (info.Answers != null)
                {
                    continue;
                }
                switch (info.LinkTable)
                {
                    case "1":
                        info.Answers = JobInfoCombo1(info.LinkPrefix, filter1);
                        break;
                    case "2":
                        info.Answers = JobInfoCombo2(info.LinkPrefix, filter2);
                        break;
                    case "3":
                        info.Answers = JobInfoCombo3(info.LinkPrefix, filter3);
                        break;
                    default:
                        info.Answers = new List<ComboItem>();
                        break;
                }
            }            
        }
        #endregion

        #region JobInfo Groups
        public Dictionary<string, string[]> GetJobInfoGroups(Contract contract)
        {
            var groups = new Dictionary<string, string[]>();
            var fields = "JSLP_GROUP,JSLP_DESC,JSLP_SORT";
            var where = " WHERE JSLP_JOBNUM = '" + contract.JobNumber + "'";
            var query = "SELECT " + fields + " FROM JCSLINP" + where;
            var list = App.Settings.Comp(CompanySettingsType.JobInfoGroups).ToQuotedList();
            if (list.Count > 0)
            {
                query += " AND JSLP_GROUP IN (" + string.Join(",", list) + ")";
            }
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var data = new string[2];
                    data[0] = row["JSLP_GROUP"].ToString();
                    data[1] = row["JSLP_DESC"].ToString();
                    var key = row["JSLP_SORT"].ToString();
                    if (string.IsNullOrEmpty(key))
                    {
                        key = data[0];
                    }
                    groups.Add(key, data);
                }
            }                
            return groups;
        }
        #endregion

        #region JobInfo Answers
        private Dictionary<string, string[]> GetJobInfoAnswers(Contract contract)
        {
            var answers = new Dictionary<string, string[]>();

            var fields = "JSI_KEY,JSI_JOBNUM,JSI_CODE,JSI_ANSWER,JSI_ANS_DESC";
            var where = "JSI_JOBNUM ='" + contract.JobNumber + "'";
            var query = "SELECT " + fields + " FROM JCSPECW WHERE " + where;

            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var data = new string[3];
                    data[0] = row["JSI_KEY"].ToString();
                    data[1] = row["JSI_ANSWER"].ToString();
                    data[2] = row["JSI_ANS_DESC"].ToString();
                    answers.Add(row["JSI_CODE"].ToString(), data);
                }
            }

            return answers;
        }
        #endregion

        #region UpdateJobInfo
        public void UpdateJobInfo(Contract contract)
        {
            try
            {
                bool updated = false;
                foreach (var question in contract.JobInformation.Where(p => p.Modified))
                {
                    updated = true;
                    if (question.Type == "M")
                    {
                        var filename = "JCSPEC_M";
                        var keyval = contract.JobNumber + question.Code;
                        App.Api.SaveSpec(filename, keyval, question.Answer);
                    }
                    if (!string.IsNullOrEmpty(question.UpdateKey))
                    {
                        App.Api.JobInfoUpdate(question.UpdateKey, question.Answer);
                    }
                    else
                    {
                        App.Api.JobInfoAdd(contract.JobNumber, question.Group, question.Category, question.Code, question.Answer);
                    }
                    question.Modified = false;
                }
                if (updated)
                {
                    App.Utils.SetSuccess("Saved Job Info");
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion
    }
}
