﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Menu.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsnCraft.Web.Common.Controllers
{
    public class QuotesViewModel : BsnViewModel
    {
        #region Contract
        public Contract Contract { get; private set; }
        #endregion

        #region Heading
        public Heading Heading { get; private set; }
        #endregion

        #region Constructor
        public QuotesViewModel(BsnApplication app) : base(app, Base.Controllers.Quotes)
        {
        }
        #endregion

        #region SetContract
        public void SetContract(Contract contract)
        {
            Contract = contract;
        }
        #endregion

        #region SetHeading
        public void SetHeading(Heading heading)
        {
            Heading = heading;
        }
        #endregion

        #region PerformUpdate
        public bool PerformUpdate(CallbackData data, bool InQuote, bool IsEstimate)
        {
            bool updated = false;

            switch (data.Type)
            {
                case BsnDataTypes.EstimateHeadings:
                    updated = PerformUpdateHeading(data);
                    break;
                case BsnDataTypes.EstimateParagraphs:
                    updated = PerformUpdateParagraph(data);
                    break;
                case BsnDataTypes.EstimateDetails:
                    updated = PerformUpdateDetail(data);
                    break;
            }

            if (InQuote && IsEstimate && !updated)
            {
                updated = UpdateEstimate(data); 
            }            
            return updated;
        }
        #endregion

        #region Update Estimate
        protected virtual bool UpdateEstimate(CallbackData data)
        {
            if (data.Mode != BsnDataModes.Include && data.Mode != BsnDataModes.Delete)
            {
                return false;
            }
            var key = App.DataCallback.Key;
            if (string.IsNullOrEmpty(key))
            {
                return false;
            }
            var detail = Contract.FindEstimateDetail(Heading, data.Key);
            if (detail == null)
            {
                return false;
            }
            detail.Included = data.Value.DefaultBool(false);
            var success = UpdateEstimateDetail(detail, data.Mode == BsnDataModes.Delete);
            if (success)
            {
                App.DataLocation = App.Menus.Current.GetLocation();
                App.DataLocation.Type = CallbackType.Normal;
            }
            return success;
        }
        #endregion

        #region UpdateEstimate
        public bool UpdateEstimateDetail(EstimateDetail detail, bool delete = false, bool dataOnly = false)
        {
            //mode: 1 = Add, 2 = Update, 3 = Delete
            int mode = 1;
            var keyval = string.Empty;
            if (!detail.IsNew)
            {
                mode = (delete) ? 3 : 2;
                keyval = detail.Key;
            }
            var amount = string.Empty; //Don't update price
            var gstInc = string.Empty;
            var costAmt = string.Empty;
            if (detail.UnitCost != detail.OriginalCost)
            {
                amount = detail.UnitCost.ToString("F2");
                costAmt = "Y";
            }
            else
            {
                if (detail.UnitAmountEx != detail.OriginalAmountEx)
                {
                    gstInc = "N";
                    amount = detail.UnitAmountEx.ToString("F2");
                }
                else
                {
                    if (detail.UnitAmountInc != detail.OriginalAmountInc)
                    {
                        gstInc = "Y";
                        amount = detail.UnitAmountInc.ToString("F2");
                    }
                }
            }
            var success = App.Api.EstimateDetail(ref keyval, ref mode, detail, amount, gstInc, costAmt, dataOnly);
            if (dataOnly)
            {
                return success;
            }
            if (detail.IsNew && success)
            {
                detail.Key = keyval;
                detail.IsNew = false;
            }
            if (delete)
            {
                detail.Contract.RemoveDetail(detail);
            }
            if (mode == 1 || mode == 3)         //add or delete
            {
                App.Api.RecalcHeading(detail.Heading);
            }
            else
            {
                ReloadEstimateDetail(detail);
            }
            return success;
        }
        #endregion

        #region ReloadEstimateDetail       
        public void ReloadEstimateDetail(EstimateDetail detail)
        {
            App.VM.JC.GetEstimateDetails(detail.Paragraph.Heading, detail.Paragraph, detail);
        }
        #endregion 

        #region PerformUpdateHeading
        protected virtual bool PerformUpdateHeading(CallbackData data)
        {
            bool updated = false;
            switch (data.Mode)
            {
                case BsnDataModes.UpdateHeading:
                    UpdateHeading(data);
                    break;
            }
            return updated;
        }
        #endregion
        
        #region Update Heading
        protected void UpdateHeading(CallbackData data)
        {
            if (data.Mode != BsnDataModes.UpdateHeading)
            {
                return;
            }
            var key = App.DataCallback.Key;
            var heading = Contract.FindHeading(key);
            if (heading == null)
            {
                if (!key.StartsWith("#"))
                {
                    App.Utils.SetError("Heading not found: " + key);
                    return;
                }
                var type = (key.ToLower() == "#empty#") ? EstimateTypes.EmptySales : EstimateTypes.Sales;
                heading = new Heading(Contract, type) { IsNew = true };
                var sections = App.Settings.Comp(CompanySettingsType.SalesEstimateSections).ToStrArray();
                var section = Helpers.GetArray(sections, 0);
                heading.Section = section;
            }
            App.VM.JC.UpdateHeading(heading, data.Value);
            App.DataLocation = App.Menus.Current.GetLocation();
        }
        #endregion

        
        #region PerformUpdateParagraph
        protected virtual bool PerformUpdateParagraph(CallbackData data)
        {
            bool updated = false;
            switch (data.Mode)
            {
                case BsnDataModes.Duplicate:
                    CopyParagraph(data);
                    App.DataLocation = App.Menus.Current.GetLocation();
                    App.DataLocation.Type = CallbackType.Normal;
                    updated = true;
                    break;
                case BsnDataModes.Delete:
                    DeleteParagraph(data);
                    App.DataLocation = App.Menus.Current.GetLocation();
                    App.DataLocation.Type = CallbackType.Normal;
                    updated = true;
                    break;
                case BsnDataModes.Copy:
                    updated = true;
                    break;
                case BsnDataModes.Paste:
                    EstimateDetailPaste(data);
                    break;
                case BsnDataModes.GetNextSortOrder:
                    updated = true;
                    break;
                case BsnDataModes.MoveDown:
                    updated = true;
                    break;
                case BsnDataModes.MoveUp:
                    updated = true;
                    break;
                case BsnDataModes.MoveTop:
                    updated = true;
                    break;
                case BsnDataModes.MoveBottom:
                    updated = true;
                    break;
            }
            return updated;
        }
        #endregion

        #region PerformUpdateDetail
        protected virtual bool PerformUpdateDetail(CallbackData data)
        {
            bool updated = false;
            switch (data.Mode)
            {
                case BsnDataModes.Delete:
                    EstimateDetailDelete(data);
                    App.DataLocation = App.Menus.Current.GetLocation();
                    App.DataLocation.Type = CallbackType.Normal;
                    updated = true;
                    break;
                case BsnDataModes.Copy:
                    EstimateDetailCopy(data);
                    updated = true;
                    break;
                case BsnDataModes.Paste:
                    EstimateDetailInsert(data);
                    updated = true;
                    break;
                case BsnDataModes.GetNextSortOrder:
                    var sortOrder = EstimateDetailGetNextSortOrder(data);
                    App.Utils.SetSuccess("Next Seq: " + sortOrder.ToString("000000"));
                    updated = true;
                    break;
                case BsnDataModes.MoveDown:
                    EstimateDetailMove(data, "D");
                    updated = true;
                    break;
                case BsnDataModes.MoveUp:
                    EstimateDetailMove(data, "U");
                    updated = true;
                    break;
                case BsnDataModes.MoveTop:
                    EstimateDetailMove(data, "T");
                    updated = true;
                    break;
                case BsnDataModes.MoveBottom:
                    EstimateDetailMove(data, "B");
                    updated = true;
                    break;
            }
            return updated;
        }
        #endregion
        
        #region DeleteParagraph
        protected void DeleteParagraph(CallbackData data)
        {
            var key = App.DataCallback.Data.Key;
            if (string.IsNullOrEmpty(key))
            {
                return;
            }
            var paragraph = Contract.FindParagraph(Heading, data.Key);
            if (paragraph == null)
            {
                return;
            }         
            if (!App.Api.UpdateParagraph(3, paragraph))
            {
                return;
            }
            string message = "Successfully deleted Paragraph";
            App.Utils.SetSuccess(message);
            var headingKey = paragraph.Heading.Key;
            var contract = paragraph.Heading.Contract;
            contract.RecalcHeadings(headingKey);
        }
        #endregion

        #region CopyParagraph
        protected void CopyParagraph(CallbackData data)
        {
            var key = App.DataCallback.Data.Key;
            if (string.IsNullOrEmpty(key))
            {
                return;
            }
            var paragraph = Contract.FindParagraph(Heading, data.Key);
            if (paragraph == null)
            {
                return;
            }

            var destination = new Paragraph(Heading);
            if (!App.Api.CopyParagraph(paragraph, destination))
            {
                return;
            }

            string message = "Successfully copied Paragraph";
            App.Utils.SetSuccess(message);
            var headingKey = destination.Heading.Key;
            var contract = destination.Heading.Contract;
            contract.RecalcHeadings(headingKey);
        }
        #endregion

        #region EstimateDetailDelete
        protected void EstimateDetailDelete(CallbackData data)
        {
            var list = new List<EstimateDetail>();
            if (Contract.SelectedEstimateDetailKeys(Heading).Count > 0)
            {
                foreach (var item in Contract.SelectedEstimateDetailKeys(Heading))
                {
                    var detail = Contract.FindEstimateDetail(Heading, item);
                    if (detail != null)
                    {
                        list.Add(detail);
                    }
                }
            }
            else
            {
                var key = App.DataCallback.Data.Key;
                if (string.IsNullOrEmpty(key))
                {
                    return;
                }
                var detail = Contract.FindEstimateDetail(Heading, data.Key);
                if (detail == null)
                {
                    return;
                }
                list.Add(detail);
            }
            foreach (var item in list)
            {
                var success = UpdateEstimateDetail(item, true);
                if (!success) return;
            }            
        }
        #endregion
        
        #region EstimateDetailGetNextSortOrder
        protected long EstimateDetailGetNextSortOrder(CallbackData data)
        {
            var key = App.DataCallback.Key;
            if (string.IsNullOrEmpty(key))
            {
                return -1;
            }
            var detail = Contract.FindEstimateDetail(Heading, data.Key);
            if (detail == null)
            {
                return -1;
            }            
            return App.Api.EstimateDetailGetNextSortOrder(new List<EstimateDetail> { detail });
        }
        #endregion

        #region EstimateDetailCopy
        protected void EstimateDetailCopy(CallbackData data)
        {
            var key = App.DataCallback.Key;
            if (string.IsNullOrEmpty(key))
            {
                return;
            }
            var list = new List<EstimateDetail>();
            if (Contract.SelectedEstimateDetailKeys(Heading).Count > 0)
            {
                foreach (var item in Contract.SelectedEstimateDetailKeys(Heading))
                {
                    var detail = Contract.FindEstimateDetail(Heading, item);
                    if (detail != null)
                    {
                        list.Add(detail);
                    }
                }
            }
            else
            {
                var detail = Contract.FindEstimateDetail(Heading, data.Key);
                if (detail == null)
                {
                    return;
                }

                list.Add(detail);
            }
            Contract.CopyEstimateDetails(list);
            App.DataLocation = App.Menus.Current.GetLocation();
            App.DataLocation.Type = CallbackType.Normal;
        }
        #endregion

        #region EstimateDetailPaste
        protected virtual void EstimateDetailPaste(CallbackData data)
        {
            if (string.IsNullOrEmpty(data.Key))
            {
                return;
            }
            var paragraph = Contract.FindParagraph(Heading, data.Key);
            if (paragraph == null)
            {
                return;
            }
            App.VM.JC.EstimateDetailLinesCopy(Contract.CopiedEstimateDetails, paragraph);
            Contract.SelectedEstimateDetailKeys(Heading).Clear();
            Contract.ReloadParagraphDetails(paragraph);
            App.DataLocation = App.Menus.Current.GetLocation();
            App.DataLocation.Type = CallbackType.Normal;
        }
        #endregion

        #region EstimateDetailInsert
        protected void EstimateDetailInsert(CallbackData data)
        {
            if (string.IsNullOrEmpty(data.Key))
            {
                return;
            }
            var detail = Contract.FindEstimateDetail(Heading, data.Key);
            if (detail == null)
            {
                return;
            }
            App.VM.JC.EstimateDetailLinesInsert(Contract.CopiedEstimateDetails, detail);
            Contract.SelectedEstimateDetailKeys(Heading).Clear();
            Contract.ReloadParagraphDetails(detail.Paragraph);
            App.DataLocation = App.Menus.Current.GetLocation();
            App.DataLocation.Type = CallbackType.Normal;
        }
        #endregion

        #region EstimateDetailMove
        protected void EstimateDetailMove(CallbackData data, string mode)
        {
            var key = App.DataCallback.Key;
            if (string.IsNullOrEmpty(key))
            {
                return;
            }
            var list = new List<EstimateDetail>();
            if (Contract.SelectedEstimateDetailKeys(Heading).Count > 0)
            {
                foreach (var item in Contract.SelectedEstimateDetailKeys(Heading))
                {
                    var detail = Contract.FindEstimateDetail(Heading, item);
                    if (detail != null)
                    {
                        if (list.Count == 0 || detail.ParagraphCode == list[0].ParagraphCode)
                        {
                            list.Add(detail);
                        }
                        else
                        {
                            App.Utils.SetError("Only lines for the same Paragraph can be moved");
                            return;
                        }
                    }
                }
            }
            else
            {
                var detail = Contract.FindEstimateDetail(Heading, data.Key);
                if (detail == null)
                {
                    return;
                }

                list.Add(detail);
            }
            var success = App.Api.EstimateDetailMove(list, mode, out bool resequencePerformed);
            if (success && !resequencePerformed)
            {
                foreach (var item in list)
                {
                    ReloadEstimateDetail(item);
                }
            }            
            if (resequencePerformed)
            {
                Contract.ReloadParagraphDetails(list[0].Paragraph);
            }
            App.DataLocation = App.Menus.Current.GetLocation();
            App.DataLocation.Type = CallbackType.Normal;
        }
        #endregion
                
    }
}
