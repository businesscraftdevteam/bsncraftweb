﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BsnCraft.Web.Common.Controllers
{
    public class CustomersViewModel : BsnViewModel
    {
        #region Constructor
        public CustomersViewModel(BsnApplication app) : base(app, Base.Controllers.Customers)
        {

        }
        #endregion

        #region Customers
        private List<Customer> customers;
        public List<Customer> Customers
        {
            get
            {
                if (customers == null || customers.Count == 0)
                {
                    customers = new List<Customer>();
                    
                    var fields = "CUSNO,CM_NAME,ADD1,ADD2,ADD3,ADD4,CITY,STATE,ZIP,PHONE,EMLA_ADDRESS";
                    var join = " LEFT OUTER JOIN IVCTBLU ON CUSNO = IVTU_CUSTNO";
                    join += " LEFT OUTER JOIN EMLADDB ON IVTU_SCRKEY = EMLA_REFCODE";
                    var query = "SELECT " + fields + " FROM CUSMASA " + join + " WHERE {fn LENGTH(CUSNO)} > 0";
                    query += " AND IVTU_SEQNUM = (SELECT MIN(IVTU_SEQNUM) FROM IVCTBLU WHERE IVTU_CUSTNO = CUSNO)";

                    var cuscd = App.Settings.Comp(CompanySettingsType.CustomerCode).ToString();
                    if (!string.IsNullOrEmpty(cuscd))
                    {
                        query += " AND CUSCD = '" + cuscd + "'";
                    }

                    query += " " + App.Settings.Comp(CompanySettingsType.CustomerQuery).ToString();
                    query = query.TrimEnd();

                    DataTable table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            string email =  Helpers.CleanSpec(row["EMLA_ADDRESS"].ToString());
                            customers.Add(new Customer()
                            {
                                CustomerNumber = row["CUSNO"].ToString(),
                                Name = row["CM_NAME"].ToString(),
                                Address1 = row["ADD1"].ToString(),
                                Address2 = row["ADD2"].ToString(),
                                Address3 = row["ADD3"].ToString(),
                                Address4 = row["ADD4"].ToString(),
                                Suburb = row["CITY"].ToString(),
                                State = row["STATE"].ToString(),
                                PostCode = row["ZIP"].ToString(),
                                Phone = row["PHONE"].ToString(),
                                Email = email
                            });
                        }
                    }
                }
                return customers;
            }
        }
        #endregion
        
        #region LookupCustomers
        public List<FieldData> LookupCustomers(string query)
        {
            var match = Customers.Where(p => p.CustomerNumber.Contains(query) ||
                p.FriendlyName.ToLower().Contains(query) ||
                p.Email.ToLower().Contains(query) ||
                p.Phone.Contains(query) ||
                p.FullAddress.ToLower().Contains(query)).Take(10);

            var results = new List<FieldData>();
            foreach (var item in match)
            {
                var value = item.CustomerNumber + " (" + item.FriendlyName + " ";
                value += item.Email + " " + item.Phone + " " + item.FullAddress + ")";
                results.Add(new FieldData(value, JsonConvert.SerializeObject(item)));
            }
            return results;
        }
        #endregion

        #region CustomerCombo
        public List<ComboItem> CustomerCombo
        {
            get
            {
                var customerCombo = new List<ComboItem>();
                foreach (var customer in Customers)
                {
                    customerCombo.Add(new ComboItem(customer.CustomerNumber, customer.FriendlyName));
                }
                return customerCombo;
            }
        }
        #endregion

        #region CustomerContacts
        private List<CustomerContact> customerContacts;
        public List<CustomerContact> CustomerContacts
        {
            get
            {
                if (customerContacts == null || customerContacts.Count == 0)
                {
                    customerContacts = new List<CustomerContact>();                    
                    var fields = "CUSNO,CM_NAME,IVTU_CUSTNO,IVTU_SEQNUM,IVTU_SCRKEY,IVTU_TITLE,IVTU_SURNAME,IVTU_FIRSTNAME";
                    fields += ",IVTU_ADDRESS,IVTU_ADDRESS2,IVTU_ADDRESS3,IVTU_ADDRESS4,IVTU_SUBURB,IVTU_STATE,IVTU_ZIP";
                    fields += ",IVTU_PHHOME,IVTU_PHWORK,IVTU_MOBILE,IVTU_FAX,EMLA_ADDRESS";
                    var join = "JOIN CUSMASA ON CUSNO = IVTU_CUSTNO";
                    join += " LEFT OUTER JOIN EMLADDB ON IVTU_SCRKEY = EMLA_REFCODE";
                    var query = "SELECT " + fields + " FROM IVCTBLU " + join + " WHERE {fn LENGTH(CUSNO)} > 0";

                    var cuscd = App.Settings.Comp(CompanySettingsType.CustomerCode).ToString();
                    if (!string.IsNullOrEmpty(cuscd))
                    {
                        query += " AND CUSCD = '" + cuscd + "'";
                    }
                    query += " " + App.Settings.Comp(CompanySettingsType.ContactsQuery).ToString();
                    query = query.TrimEnd();

                    DataTable table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            string email =  Helpers.CleanSpec(row["EMLA_ADDRESS"].ToString());

                            customerContacts.Add(new CustomerContact()
                            {
                                Key = row["IVTU_SCRKEY"].ToString(),
                                CustomerNumber = row["CUSNO"].ToString(),
                                Name = row["CM_NAME"].ToString(),
                                Sequence = int.Parse(row["IVTU_SEQNUM"].ToString()).ToString("000"),
                                Title = row["IVTU_TITLE"].ToString(),
                                LastName = row["IVTU_SURNAME"].ToString(),
                                FirstName = row["IVTU_FIRSTNAME"].ToString(),
                                Address1 = row["IVTU_ADDRESS"].ToString(),
                                Address2 = row["IVTU_ADDRESS2"].ToString(),
                                Address3 = row["IVTU_ADDRESS3"].ToString(),
                                Address4 = row["IVTU_ADDRESS4"].ToString(),
                                Suburb = row["IVTU_SUBURB"].ToString(),
                                State = row["IVTU_STATE"].ToString(),
                                PostCode = row["IVTU_ZIP"].ToString(),
                                HomePhone = row["IVTU_PHHOME"].ToString(),
                                WorkPhone = row["IVTU_PHWORK"].ToString(),
                                MobilePhone = row["IVTU_MOBILE"].ToString(),
                                FaxNumber = row["IVTU_FAX"].ToString(),
                                Email = email
                            });
                        }                        
                    }
                }
                return customerContacts;
            }
        }
        #endregion       

        #region GetCustomers
        public List<Customer> GetCustomers(string customerNumber)
        {
            return Customers.Where(p => p.CustomerNumber.Equals(customerNumber)).ToList();
        }
        #endregion

        #region FindCustomer
        public Customer FindCustomer(string customerNumber)
        {
            var customer = Customers.Where(p => p.CustomerNumber == customerNumber);
            return customer.FirstOrDefault();
        }
        #endregion

        #region GetContacts
        public List<CustomerContact> GetContacts(string customerNumber)
        {
            return CustomerContacts.Where(p => p.CustomerNumber.Equals(customerNumber)).ToList();
        }
        #endregion

        #region FindContact
        public CustomerContact FindContact(string customerNumber, string sequence)
        {
            var contact = CustomerContacts
                .Where(p => p.CustomerNumber.Equals(customerNumber) && p.Sequence.Equals(sequence))
                .FirstOrDefault();
            if (contact == null)
            {
                contact = CustomerContacts
                    .Where(p => p.CustomerNumber.Equals(customerNumber))
                    .FirstOrDefault();
            }
            return contact;
        }

        public CustomerContact FindContact(string key)
        {
            return CustomerContacts.Where(p => p.Key.Equals(key)).FirstOrDefault();
        }
        #endregion

        #region UpdateCustomer
        public void UpdateCustomer(CustomerContact contact)
        {
            bool isNew = (string.IsNullOrEmpty(contact.CustomerNumber));
            string key = App.Api.UpdateCustomer(contact);
            if (isNew && !string.IsNullOrEmpty(key))
            {
                var newCustomer = FindCustomer(key);
                if (newCustomer == null)
                {
                    App.Utils.SetError("Error finding new key: " + key);
                }
                else
                {
                    //TODO: Update
                    //string urlextras = "?key=" + newCustomer.CustomerNumber;
                    //var location = App.Menus.Get(BsnMenus.Customers, BsnActions.Details).ResolveUrl + urlextras;
                    //VarUtils.Set(WebVars.JSLocation, location);
                }
            }
        }
        #endregion

        #region DeleteContact
        public void DeleteContact(CustomerContact contact)
        {
            App.Api.DeleteContact(contact);
            CustomerContacts.Remove(contact);
        }
        #endregion

        #region CustomerTitles
        private List<ComboItem> customerTitles;
        public List<ComboItem> CustomerTitles
        {
            get
            {
                if (customerTitles == null || customerTitles.Count == 0)
                {
                    customerTitles = GetDropDownList("COTC_CODE", "COTC_DESC", "COTABLC");
                }
                return customerTitles;
            }
        }
        #endregion

        #region States
        public List<ComboItem> States
        {
            get
            {                
                return App.VM.CO.States;
            }
        }
        #endregion

    }
}