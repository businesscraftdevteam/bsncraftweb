﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BsnCraft.Web.Common.Controllers
{
    public class VariationsViewModel : BsnViewModel
    {
        public VariationsViewModel(BsnApplication app) : base(app, Base.Controllers.Variations)
        {

        }


        #region Variations
        public List<Variation> GetVariations(Contract contract)
        {
            var variations = new List<Variation>();
            string fields = "CLV_SEQA,CLV_SCRKEY,CLV_CONTRACTA,CLV_REF,CLV_DESCR,CLV_DATE,CLV_VTYPE,CLV_AMOUNT,CLV_GST_AMT,CLV_COST,CLV_ISSFLAG,CLV_INVOICED";
            string query = "SELECT " + fields + " FROM CONLINV WHERE CLV_CONTRACT = '" + contract.ContractNumber + "'";

            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    string date = (row["CLV_DATE"] is DateTime) ?
                        ((DateTime)row["CLV_DATE"]).ToString("dd/MM/yyyy") : string.Empty;

                    decimal amount = (row["CLV_AMOUNT"] is decimal) ? ((decimal)row["CLV_AMOUNT"]) : 0;
                    decimal gstAmount = (row["CLV_GST_AMT"] is decimal) ? ((decimal)row["CLV_GST_AMT"]) : 0;
                    decimal costAmount = (row["CLV_COST"] is decimal) ? ((decimal)row["CLV_COST"]) : 0;
                    decimal incAmount = amount + gstAmount;
                    bool issued = row["CLV_ISSFLAG"].ToString().Equals("Y") ? true : false;
                    bool invoiced = row["CLV_INVOICED"].ToString().Equals("Y") ? true : false;
                    variations.Add(new Variation()
                    {
                        Key = row["CLV_SCRKEY"].ToString(),
                        ContractNumber = row["CLV_CONTRACTA"].ToString(),
                        Sequence = row["CLV_SEQA"].ToString(),
                        Date = date,
                        Reference = row["CLV_REF"].ToString(),
                        Description = row["CLV_DESCR"].ToString(),
                        Type = row["CLV_VTYPE"].ToString(),
                        Amount = amount,
                        GSTAmount = gstAmount,
                        IncAmount = incAmount,
                        CostAmount = costAmount,
                        IncludeGST = true,
                        Issued = issued,
                        Invoiced = invoiced
                    });
                }
            }
            if (variations.Count > 0)
            {
                var notes = VariationNotes(contract.ContractNumber);
                var spec = string.Empty;
                foreach (var variation in variations)
                {
                    spec = string.Empty;
                    if (notes.ContainsKey(variation.Key))
                    {
                        spec = notes[variation.Key];
                    }
                    variation.Notes = spec;
                }
            }
            return variations;
        }
        #endregion

        #region Variation Notes
        private Dictionary<string, string> VariationNotes(string contractNumber)
        {
            var notes = new Dictionary<string, string>();

            var fields = "CLH_ALPHA,CLH_CONST,CLH_NOTES";
            var where = "CLH_ALPHA ='" + contractNumber + "'";
            var query = "SELECT " + fields + " FROM CONLINH WHERE " + where;

            var table = App.Odbc.Table(query);
            if (table != null)
            {
                var spec = string.Empty;
                var key = string.Empty;
                int i = 0;
                foreach (DataRow row in table.Rows)
                {
                    i++;
                    if (row["CLH_CONST"].ToString() != key && !string.IsNullOrEmpty(key))
                    {
                        spec =  Helpers.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                        spec = string.Empty;
                    }
                    spec += row["CLH_NOTES"].ToString();
                    key = row["CLH_CONST"].ToString();
                    if (i == table.Rows.Count)
                    {
                        spec =  Helpers.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                    }
                }
            }

            return notes;
        }
        #endregion
        
        #region UpdateVariation
        public int UpdateVariation(Variation variation)
        {
            string bsnparams = string.Empty;
            bsnparams += "INVOICED=" + (variation.Invoiced ? "Y" : "N") + ",";
            return App.Api.UpdateVariation(variation, ref bsnparams);
        }
        #endregion

        #region VariationDocuments
        public List<Document> VariationDocuments(string contractNumber, string variationSeq)
        {
            /*
            var documents = Documents(contractNumber, Document.CLT_DOC_XREF_VAR, variationSeq);                       
            return documents;
            */
            return new List<Document>();
        }
        #endregion

        #region VariationEvents
        public List<VariationEvent> GetVariationEvents(Contract contract, string seq)
        {
            var variationEvents = new List<VariationEvent>();
            string fields = "CLG_CONTRACT,CLG_CONST,CLG_SEQNUM,CLG_EVENT,CLG_EVENTA,CLG_SCRKEY,";
            fields += "CLG_ACTDATE,CLG_REGDATE,CLG_REF,CLG_DESC,CLG_EMPLOYEE,CLG_FOREDATE,CLG_FOREFLAG,CLG_CHNGEVNT";
            string query = "SELECT " + fields + " FROM CONLING WHERE CLG_CONTRACT = '" + contract.ContractNumber;
            query += "' AND CLG_SEQNUM = '" + seq + "'";
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var Actual = (row["CLG_ACTDATE"] is DateTime) ? ((DateTime)row["CLG_ACTDATE"]) : DateTime.MinValue;
                    var Registered = (row["CLG_REGDATE"] is DateTime) ? ((DateTime)row["CLG_REGDATE"]) : DateTime.MinValue;
                    var Forecast = (row["CLG_FOREDATE"] is DateTime) ? ((DateTime)row["CLG_FOREDATE"]) : DateTime.MinValue;

                    variationEvents.Add(new VariationEvent()
                    {
                        Key = row["CLG_SCRKEY"].ToString(),
                        ContractNumber = contract.ContractNumber,
                        VariationSequence = seq,
                        EventNumber = row["CLG_EVENT"].ToString(),
                        EventNumberA = row["CLG_EVENTA"].ToString(),
                        ActualDate = row["CLG_ACTDATE"].ToDate(),
                        RegisterDate = row["CLG_REGDATE"].ToDate(),
                        ForecastDate = row["CLG_FOREDATE"].ToDate(),
                        Reference = row["CLG_REF"].ToString(),
                        Description = row["CLG_DESC"].ToString(),
                        Employee = row["CLG_EMPLOYEE"].ToString(),
                        ForecastFlag = row["CLG_FOREFLAG"].ToString(),
                        ChangeEvent = row["CLG_CHNGEVNT"].ToString()
                    });
                }
            }             
            return variationEvents;
        }
        #endregion

        #region VariationEventRegister
        public void VariationEventRegister(string key)
        {
            //string[] keyval = key.Split(':');
            //string contractNumber = Helpers.GetArray(keyval, 0);
            //string variationSeq = Helpers.GetArray(keyval, 1);
            //string eventNumber = Helpers.GetArray(keyval, 2);
            //var variation = GetVariation(contractNumber, variationSeq);
            //var variationEvent = GetVariationEvent(contractNumber, variationSeq, eventNumber);
            //if (variationEvent == null)
            //{
            //    App.Utils.SetError("Variation Event not found.");
            //    return;
            //}

            //if (variationEvent.IsRegistered)
            //{
            //    App.Api.VariationEventUnregister(contractNumber, variationSeq, eventNumber);
            //}
            //else
            //{
            //    App.Api.VariationEventRegister(contractNumber, variationSeq, eventNumber, variation.Amount, variation.CostAmount, variation.GSTAmount);
            //}
        }
        #endregion

        #region UpdateVariationEvent
        public void UpdateVariationEvent(VariationEvent variationEvent)
        {
            //var variation = GetVariation(variationEvent.ContractNumber, variationEvent.VariationSequence);
            //if (variationEvent.IsRegistered)
            //{
            //    App.Api.VariationEventRegister(variationEvent.ContractNumber, variationEvent.VariationSequence,
            //    variationEvent.EventNumberA, variationEvent.ActualDate.ToString("dd/MM/yyyy"), variationEvent.Employee,
            //    variationEvent.Reference, variation.Amount, variation.CostAmount, variation.GSTAmount);
            //}
            //else
            //{
            //    App.Api.VariationEventUnregister(variationEvent.ContractNumber, variationEvent.VariationSequence, variationEvent.EventNumberA);
            //}
        }
        #endregion

        #region GetVariationEvent

        //TODO: Move these to contract model

        //public VariationEvent GetVariationEvent(string key)
        //{
        //    string contractNumber = key.Substring(0, 6);
        //    string variationSeq = key.Substring(6, 3);
        //    return VariationEvents(contractNumber, variationSeq).Where(p => p.Key == key).FirstOrDefault();
        //}

        //public VariationEvent GetVariationEvent(string contractNumber, string variationSeq, string eventNumber)
        //{
        //    return VariationEvents(contractNumber, variationSeq).Where(p => p.EventNumberA == eventNumber).FirstOrDefault();
        //}
        #endregion

        #region GetInvoicingSettings
        private InvoicingSettings invoicingSettings;
        public InvoicingSettings InvoicingSettings
        {
            get
            {
                if (invoicingSettings == null)
                {
                    var list = new List<InvoicingSettings>();
                    string fields = "IVC_INTCO";
                    string query = "SELECT " + fields + " FROM IVCMSTB WHERE IVC_KEY =' '";

                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            bool interfaceContracts = row["IVC_INTCO"].ToString().Equals("Y") ? true : false;
                            list.Add(new InvoicingSettings()
                            {
                                InterfaceContracts = interfaceContracts,
                            });
                        }
                    }
                    invoicingSettings = list.FirstOrDefault();
                }
                return invoicingSettings;
            }
        }
        #endregion
    }
}
