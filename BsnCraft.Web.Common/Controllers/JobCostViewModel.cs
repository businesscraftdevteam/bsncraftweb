using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace BsnCraft.Web.Common.Controllers
{
    public class JobCostViewModel : BsnViewModel
    {
        #region Constructor
        public JobCostViewModel(BsnApplication app) : base(app, Base.Controllers.Jobs)
        {
        }
        #endregion

        #region Constants
        public const string HeadingFields = "JSEA_SCRKEY,JSEA_SECT,JSEA_JOBNUM,JSEA_HEADING,JSEA_DESC,JSEA_CATALOG," + 
                                            "JSEA_PROT,JSEA_LOCKED,JSEA_STATUS,JSEA_TEMPLATE,JSEA_SALE,JSEA_GST_AMOUNT," +
                                            "JSEA_COST,JSEA_GST_INC,JSEA_MGNCDE,JSEA_MGNPCT";

        public const string ParagraphFields = "JSEB_SCRKEY,JSEB_JOBNUM,JSEB_SECT,JSEB_HEADING,JSEB_PARAGRAPH,JSEB_DESC," +
                                             "JSEB_SALE,JSEB_GST_AMOUNT,JSEB_FMTOPT,JSEB_LOCKED,JSEB_MGNCDE,JSEB_MGNPCT," +
                                             "JSEB_COST,JSEB_GST_INC";
        #endregion

        #region GetContract
        public Contract GetContract(string key, bool isContractNo = true)
        {
            Contract contract;
            if (isContractNo)
            {
                contract = App.VM.CO.Contracts.Where(p => p.ContractNumber == key).FirstOrDefault();
                if (contract == null)
                {
                    //Since a Lead is a contract object, should be safe. Saves too much duplication in Leads module
                    contract = App.VM.LE.Leads.Where(p => p.ContractNumber == key).FirstOrDefault();
                }
                return contract;
            }
            contract = App.VM.CO.Contracts.Where(p => p.JobNumber == key).FirstOrDefault();
            if (contract == null)
            {
                //Since a Lead is a contract object, should be safe. Saves too much duplication in Leads module
                contract = App.VM.LE.Leads.Where(p => p.JobNumber == key).FirstOrDefault();
            }
            return contract;
        }
        #endregion

        #region Headings
        public void ReloadHeading(Heading heading)
        {
            var key = "a" + heading.Key;
            var where = "WHERE JSEA_KEY LIKE '" + key + "%'";
            var query = "SELECT " + HeadingFields + " FROM JCSPEC_A " + where;            
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                LoadHeadingFromDataRow(heading, table.Rows[0]);
            }
        }

        public void LoadHeadingFromDataRow(Heading heading, DataRow row)
        {
            heading.Key = row["JSEA_SCRKEY"].ToString();
            heading.Section = row["JSEA_SECT"].ToString();
            heading.Code = row["JSEA_HEADING"].ToString();
            heading.Description = row["JSEA_DESC"].ToString();
            heading.Catalog = row["JSEA_CATALOG"].ToString();
            heading.Status = row["JSEA_STATUS"].ToString();
            heading.SellAmtEx = row["JSEA_SALE"].ToDec();
            heading.SellAmtGST = row["JSEA_GST_AMOUNT"].ToDec();
            heading.CostAmt = row["JSEA_COST"].ToDec();
            heading.GSTInc = row["JSEA_GST_INC"].ToString();
            heading.DefMarginPct = row["JSEA_MGNPCT"].ToDec();
            heading.OriginalMargin = row["JSEA_MGNPCT"].ToDec();
            heading.MarginCode = row["JSEA_MGNCDE"].ToString();
        }

        public List<Heading> GetHeadings(Contract contract, EstimateTypes type)
        {
            var headings = new List<Heading>();
            var fields = "CO_KEY,CO_JOBNUM," + HeadingFields;
            var join = " JOIN CONHDRA ON CO_JOBNUM = JSEA_JOBNUM";
            var query = "SELECT " + fields + " FROM JCSPEC_A" + join;
            var where = " WHERE CO_KEY ='" + contract.ContractNumber + "'";            
            var sections = App.Settings.Comp(CompanySettingsType.SalesEstimateSections).ToQuotedList();
            if (type == EstimateTypes.Variation)
            {
                sections = App.Settings.Comp(CompanySettingsType.VariationEstimateSections).ToQuotedList();
            }
            if (sections.Count > 0)
            {
                where += " AND JSEA_SECT IN (" + string.Join(",", sections) + ")";
            }            
            query += where;
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                var headingCheck = HeadingCheck(contract.JobNumber, sections);
                foreach (DataRow row in table.Rows)
                {
                    var hasEstimate = headingCheck.Contains(row["JSEA_HEADING"].ToString());
                    if (!hasEstimate)
                    {
                        hasEstimate = (row["JSEA_CATALOG"].ToString() == "EMPTY");
                    }
                    Heading heading = new Heading(contract, type);
                    LoadHeadingFromDataRow(heading, row);
                    heading.IsEstimate = hasEstimate;
                    headings.Add(heading);                    
                }
            }
            return headings;
        }
        #endregion

        #region Heading Check
        private List<string> HeadingCheck(string jobNumber, List<string> sections)
        {
            var headingCheck = new List<string>();
            var fields = "JSEB_SECT,JSEB_JOBNUM,JSEB_HEADING";
            var where = " WHERE JSEB_JOBNUM ='" + jobNumber + "'";
            var query = "SELECT DISTINCT " + fields + " FROM JCSPEC_B" + where;
            if (sections.Count > 0)
            {
                query += " AND JSEB_SECT IN (" + string.Join(",", sections) + ")";
            }
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var heading = row["JSEB_HEADING"].ToString();
                    headingCheck.Add(heading);
                }
            }
            return headingCheck;
        }
        #endregion

        #region HeadingDocTypes
        private List<DocumentType> headingDocTypes;
        public List<DocumentType> HeadingDocTypes
        {
            get
            {
                if (headingDocTypes == null)
                {
                    headingDocTypes = new List<DocumentType>();
                    string fields = "IVWP_CODE,IVWP_MODULE,IVWP_FILE,IVWP_MERGEFMT,IVWP_DESCR";
                    string query = "SELECT " + fields + " FROM IVCTBLP WHERE IVWP_MODULE IN ('', 'CO', 'JC')";
                    var list = App.Settings.Comp(CompanySettingsType.HeadingDocumentTypes).ToQuotedList();
                    if (list.Count > 0)
                    {
                        query += " AND IVWP_CODE IN (" + string.Join(",", list) + ")";
                    }
                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            var code = row["IVWP_CODE"].ToString();
                            var descr = row["IVWP_DESCR"].ToString();
                            var module = row["IVWP_MODULE"].ToString();

                            var docType = new DocumentType(code, descr)
                            {
                                ExtraData = module,
                                MasterDocument = row["IVWP_FILE"].ToString()
                            };
                            headingDocTypes.Add(docType);
                        }
                    }
                }
                return headingDocTypes;
            }
        }

        public string HeadingDocDesc(string code, string module)
        {
            var docTypes = HeadingDocTypes.Where(p => p.Code == code && p.ExtraData == module);
            if (docTypes == null || docTypes.Count() == 0)
            {
                return string.Empty;
            }
            return docTypes.FirstOrDefault().Desc;
        }
        #endregion

        #region UpdateHeading
        public bool UpdateHeading(Heading heading, string option, int mode = 1)
        {
            string section = heading.Section;
            //int mode = 1;

            if (heading.IsNew)
            {
                if (string.IsNullOrEmpty(heading.Code))
                {
                    int code = 0;
                    var query = "SELECT JSEA_HEADING FROM JCSPEC_A";
                    query += " WHERE JSEA_JOBNUM = '" + heading.Contract.JobNumber + "'";
                    query += " AND JSEA_SECT='" + section + "'";
                    query += " AND JSEA_HEADING LIKE '0%' ORDER BY JSEA_HEADING DESC";
                    var table = App.Odbc.Table(query);
                    if (table != null && table.Rows.Count > 0)
                    {
                        var previous = table.Rows[0]["JSEA_HEADING"].ToString();
                        int.TryParse(previous, out code);
                    }
                    code++;
                    heading.Code = code.ToString("0000");
                    heading.Description = "Estimate " + code.ToString();
                }
            }
            else
            {
                mode = 2;   //Update
            }

            if (mode == 4)   //set margin
            {
                var job = heading.Contract.JobNumber;
                if (heading.DefMarginPct != heading.OriginalMargin)
                {
                    heading.MarginCode = string.Empty;
                }

                App.Api.UpdateMargin(job, heading.Section, heading.Code, "", heading.MarginCode,
                    heading.DefMarginPct, heading.MarginUpdAll);
                return true;
            }

            if (!App.Api.UpdateHeading(mode, heading, option))
            {
                return false;
            }

            if (mode == 1)
            {
                App.Utils.SetSuccess();
            }
            else
            {
                string message = "Successfully ";
                switch (option)
                {
                    case "R":
                        message += "re-opened";
                        break;
                    case "X":
                        message += "cancelled";
                        break;
                    default:
                        message += "updated";
                        break;
                }
                message += " estimate " + heading.Code;
                App.Utils.SetSuccess(message);
            }
            var headingKey = heading.Key;
            var contract = heading.Contract;
            contract.RecalcHeadings(headingKey);
            return true;
        }
        #endregion

        #region Documents
        public List<Document> GetDocuments(Contract contract)
        {
            var documents = new List<Document>();
            if (!string.IsNullOrEmpty(contract.JobNumber))
            {
                
                var fields = "JWP_DOCTYP,JWP_JOBSEQ,JWP_JOBNUM,JWP_SEQNUM,JWP_DOCREF,JWP_DESCR,JWP_CRE_DATE,JWP_ALT_DOCS";
                var query = "SELECT " + fields + " FROM JCSPECX WHERE JWP_JOBNUM = '" + contract.JobNumber + "'";

                var list = App.Settings.Comp(CompanySettingsType.SalesEstimateSections).ToQuotedList();
                if (list.Count > 0)
                {
                    query += " AND JWP_SECT IN (" + string.Join(",", list) + ")";
                }

                list = App.Settings.Comp(CompanySettingsType.HeadingDocumentTypes).ToQuotedList();
                if (list.Count > 0)
                {
                    query += " AND JWP_DOCTYP IN (" + string.Join(",", list) + ")";
                }
                query += " ORDER BY JWP_JOBSEQ ASC";

                var table = App.Odbc.Table(query);
                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        var docType = row["JWP_DOCTYP"].ToString();
                        documents.Add(new Document(contract)
                        {
                            Key = row["JWP_JOBSEQ"].ToString(),
                            Module = "JC",
                            Type = docType,
                            Sequence = row["JWP_SEQNUM"].ToInt().ToString("000"),
                            Reference = row["JWP_DOCREF"].ToString(),
                            Description = row["JWP_DESCR"].ToString(),
                            Filename = row["JWP_ALT_DOCS"].ToString(),
                            Locked = App.VM.SY.FindDocumentType(docType, "JC").Locked,
                            Date = row["JWP_CRE_DATE"].ToDate().ToAus()
                        });
                    }

                }
                if (documents.Count > 0)
                {
                    var notes = DocumentNotes(contract.JobNumber);
                    foreach (var document in documents)
                    {
                        string spec = string.Empty;
                        if (notes.ContainsKey(document.Key))
                        {
                            spec = notes[document.Key];
                        }
                        document.Notes = spec;
                    }
                }
            }
            return documents;
        }
        #endregion

        #region Document Notes
        private Dictionary<string, string> DocumentNotes(string jobNumber)
        {
            var notes = new Dictionary<string, string>();

            var fields = "JS5_JOBNUM,JS5_CONST,JS5_DESC";
            var where = "JS5_JOBNUM = '" + jobNumber + "'";
            var query = "SELECT " + fields + " FROM JCSPEC5 WHERE " + where;

            var table = App.Odbc.Table(query);
            if (table != null)
            {
                var spec = string.Empty;
                var key = string.Empty;
                int i = 0;
                foreach (DataRow row in table.Rows)
                {
                    i++;
                    //TODO: remove substring when Genesis fixed
                    var curr = row["JS5_CONST"].ToString().Substring(0, 11);
                    if (curr != key && !string.IsNullOrEmpty(key))
                    {
                        spec =  Helpers.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                        spec = string.Empty;
                    }
                    spec += row["JS5_DESC"].ToString();
                    key = curr;
                    if (i == table.Rows.Count)
                    {
                        spec =  Helpers.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                    }
                }
            }
            return notes;
        }
        #endregion
        
        #region CopyHeading
        public bool CopyHeading(Heading source, Heading destination)
        {
            var job = source.Contract.JobNumber;
            var section = source.Section;

            if (!App.Api.CopyHeading(job, section, source, destination))
            {
                return false;
            }

            string message = "Successfully copied estimate";
            App.Utils.SetSuccess(message);
            return true;
        }
        #endregion
                
        #region UpdateParagraph
        public void UpdateParagraph(Paragraph paragraph, int mode = 2)
        {
            try
            {
                //Update Margin
                if (mode == 4)
                {
                    if (paragraph.DefMarginPct != paragraph.OriginalMargin)
                    {
                        paragraph.MarginCode = string.Empty;
                    }

                    var job = paragraph.Heading.Contract.JobNumber;
                    var section = paragraph.Heading.Section;
                    var heading = paragraph.Heading.Code;

                    App.Api.UpdateMargin(job, section, heading, paragraph.Code, paragraph.MarginCode, 
                        paragraph.DefMarginPct, paragraph.MarginUpdAll);
                }
                else
                {
                    if (paragraph.IsNew)
                    {
                        mode = 1;
                    }
                    //mode 3 = delete
                    App.Api.UpdateParagraph(mode, paragraph);
                }

                paragraph.Heading.Contract.RecalcHeadings(paragraph.Heading.Key);
                //ClearCache();
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
        }
        #endregion
               
        #region GetEstimateDetails       
        public List<EstimateDetail> GetEstimateDetails(Heading heading, Paragraph paragraph = null, EstimateDetail detail = null)
        {
            var estimateDetails = new List<EstimateDetail>();
            try
            {
                string fields = "CO_KEY,JSEC_KEY,JSEC_TKEY,JSEC_JOBNUM,JSEC_SECT,JSEC_HEADING,JSEC_PARAGRAPH,JSEC_DETSEQ,JSEC_ITMNUM,";
                fields += "JSEC_DESC,JSEC_EDESC,JSEC_SALE,JSEC_GST_AMOUNT,JSEC_ESTQTY,JSEC_DFLT_CHK,JSEC_DFLT_QTY,JSEC_UOM,JSEC_PRCFLG,";
                fields += "JSEC_DISP_CHKBOX,JSEC_DISP_QTYFLD,JSEC_GST_INC,JSEC_COST,JSEC_MGNCDE,JSEC_MGNPCT,JSEC_DISP_SELECT,JSEC_SORTORDER,";
                fields += "COSTCT";
                string join = "JOIN CONHDRA ON CO_JOBNUM = JSEC_JOBNUM ";
                join += "{OJ LEFT OUTER JOIN ITMMASA ON ITEMNO = JSEC_ITMNUM}";
                
                string where = "CO_KEY ='" + heading.Contract.ContractNumber + "'";
                
                if (detail != null)
                {
                    where  += " AND JSEC_KEY = '" + detail.Key+ "'";
                }
                else
                {
                    where += " AND JSEC_SECT = '" + heading.Section + "'";
                    where += " AND JSEC_HEADING = '" + heading.Code + "'";
                    if (paragraph != null)
                    {
                        where += " AND JSEC_PARAGRAPH = '" + paragraph.Code + "'";
                    }
                }

                string query = "SELECT " + fields + " FROM JCSPEC_C " + join + " WHERE " + where;
                var table = App.Odbc.Table(query);
                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        var para = paragraph ?? heading.Contract.FindParagraph(heading, row["JSEC_PARAGRAPH"].ToString());                    
                        var item = detail ?? new EstimateDetail(para);
                        if (para != null)
                        {
                            item.Key = row["JSEC_KEY"].ToString();
                            item.ScrKey = row["JSEC_TKEY"].ToString();
                            item.Sequence = row["JSEC_DETSEQ"].ToInt().ToString("00000");
                            item.Item = row["JSEC_ITMNUM"].ToString();
                            item.Description = row["JSEC_DESC"].ToString() + row["JSEC_EDESC"].ToString();
                            item.UnitOfMeasure = row["JSEC_UOM"].ToString();
                            item.PriceDisplay = row["JSEC_PRCFLG"].ToString();
                            item.MarginCode = row["JSEC_MGNCDE"].ToString();
                            item.IncludeGST = (row["JSEC_GST_INC"].ToString().ToUpper() == "Y");
                            item.ShowCheckbox = (row["JSEC_DISP_CHKBOX"].ToString().ToUpper() != "N");
                            item.ShowQuantity = (row["JSEC_DISP_QTYFLD"].ToString().ToUpper() != "N");
                            item.Included = (row["JSEC_DFLT_CHK"].ToString().ToUpper() != "N");
                            item.CostCentre = row["COSTCT"].ToString();
                            item.Quantity = row["JSEC_ESTQTY"].ToDec();
                            item.UnitAmountGst = row["JSEC_GST_AMOUNT"].ToDec();
                            int sortorder = row["JSEC_SORTORDER"].ToInt();
                            if (sortorder == 0) sortorder = row["JSEC_DETSEQ"].ToInt();
                            item.SortOrder = para.Code + sortorder.ToString("000000");
                            item.SetOriginalCost(row["JSEC_COST"].ToDec());
                            item.SetOriginalMargin(row["JSEC_MGNPCT"].ToDec());
                            item.SetOriginalAmountEx(row["JSEC_SALE"].ToDec());
                            item.SetOriginalAmountInc(row["JSEC_SALE"].ToDec() + row["JSEC_GST_AMOUNT"].ToDec());
                            item.ShowSelect = (row["JSEC_DISP_SELECT"].ValueOrDefault("Y").ToUpper() != "N");

                            if (!estimateDetails.Contains(item))
                            {
                                estimateDetails.Add(item);
                            }
                        }
                    }

                    if (detail != null)
                    {
                        LoadEstimateDetailExtras(detail);
                        ReloadParagraph(detail.Paragraph);
                        ReloadHeading(detail.Paragraph.Heading);
                    }
                    else
                    {
                        LoadEstimateExtras(heading, estimateDetails);
                    }
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return estimateDetails;
        }
        #endregion
                
        #region Load Paragraph AddItems
        public void LoadParagraphAddItems(Heading heading, List<EstimateDetail> estimateDetails)
        {
            foreach (var para in heading.Contract.Paragraphs(heading))
            {
                if (!estimateDetails.Any(p => p.Key == "#" + para.Code))
                {
                    //TODO: Load up more defaults
                    estimateDetails.Add(new EstimateDetail(para)
                    {
                        Key = "#" + para.Code,
                        SortOrder = para.Code + "999999",
                        IsNew = true,
                        Included = true,
                        Quantity = 1,
                        IncludeGST = para.IncludeGST,
                        ShowCheckbox = true,
                        ShowQuantity = true,
                        ShowSelect = true                        
                    });
                }
            }
        }
        #endregion

        #region LoadEstimateExtras
        public void LoadEstimateDetailExtras(EstimateDetail detail)
        {
            var spec = App.Odbc.SpecL("JCSPEC0", "JS0_KEY", detail.ScrKey, "JS0_DESC", "0");
            detail.LoadNotes(spec);

            spec = App.Odbc.SpecL("JCSPEC_I", "JSEO_KEY", detail.ScrKey, "JSEO_SPEC", "i");
            detail.LoadOptions(spec);            
        }

        public void LoadEstimateExtras(Heading heading, List<EstimateDetail> details)
        {
            var fields = "JS0_CONST,JS0_DESC";
            var where = "JS0_JOBNUM = '" + heading.Contract.JobNumber + "'";
            where += " AND JS0_SECT = '" + heading.Section + "'";
            where += " AND JS0_HEADING = '" + heading.Code + "'";
            var query = "SELECT " + fields + " FROM JCSPEC0 WHERE " + where;
            query += " ORDER BY JS0_KEY ASC";
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                var spec = string.Empty;
                var key = string.Empty;
                var notes = new Dictionary<string, string>();
                int i = 0;
                foreach (DataRow row in table.Rows)
                {
                    i++;
                    if (row["JS0_CONST"].ToString() != key && !string.IsNullOrEmpty(key))
                    {
                        notes.Add(key, Helpers.CleanSpec(spec));
                        spec = string.Empty;
                    }
                    spec += row["JS0_DESC"].ToString();
                    key = row["JS0_CONST"].ToString();
                    if (i == table.Rows.Count)
                    {
                        notes.Add(key, Helpers.CleanSpec(spec));
                    }
                }
                foreach (var detail in details.Where(p => !p.IsNew))
                {
                    spec = string.Empty;
                    if (notes.ContainsKey(detail.ScrKey))
                    {
                        spec = notes[detail.ScrKey];
                    }
                    detail.LoadNotes(spec);
                }
            }          

            fields = "JSEO_CONST,JSEO_SPEC";
            where = "JSEO_JOBNUM = '" + heading.Contract.JobNumber + "'";
            where += " AND JSEO_SECT = '" + heading.Section + "'";
            where += " AND JSEO_HEADING = '" + heading.Code + "'";
            query = "SELECT " + fields + " FROM JCSPEC_I WHERE " + where;
            query += " ORDER BY JSEO_KEY ASC";
            table = App.Odbc.Table(query);
            if (table != null)
            {
                var spec = string.Empty;
                var key = string.Empty;
                var options = new Dictionary<string, string>();
                int i = 0;
                foreach (DataRow row in table.Rows)
                {
                    i++;
                    if (row["JSEO_CONST"].ToString() != key && !string.IsNullOrEmpty(key))
                    {
                        options.Add(key, Helpers.CleanSpec(spec));
                        spec = string.Empty;
                    }
                    spec += row["JSEO_SPEC"].ToString();
                    key = row["JSEO_CONST"].ToString();
                    if (i == table.Rows.Count)
                    {
                        options.Add(key, Helpers.CleanSpec(spec));
                    }
                }

                foreach (var detail in details.Where(p => !p.IsNew))
                {
                    spec = string.Empty;
                    if (options.ContainsKey(detail.ScrKey))
                    {
                        spec = options[detail.ScrKey];
                    }
                    detail.LoadOptions(spec);
                }
            }
        }
        #endregion
        
        #region CopyTemplate
        public bool CopyTemplate(Contract contract, string estimateHeading, Template template, string Extras)
        {
            try
            {
                var success = App.Api.TemplateCopy(contract, estimateHeading, template, Extras);

                //TODO: consider the below once enhanced for estimating
                //var heading = contract.Headings.Where(p => p.Code.Equals(estimateHeading)).FirstOrDefault();
                //if (heading != null)
                //{
                //    contract.Headings.Remove(heading);
                //}                

                contract.RecalcHeadings(string.Empty);

                return success;
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return false;
        }
        #endregion

        #region Get Paragraphs
        public void ReloadParagraph(Paragraph paragraph)
        {
            var key = "b" + paragraph.Key;
            var where = "WHERE JSEB_KEY LIKE '" + key + "%'";
            var query = "SELECT " + ParagraphFields + " FROM JCSPEC_B " + where;
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                DataRow row = table.Rows[0];
                LoadParagraphFromDataRow(paragraph, row);
                if (!paragraph.Locked)
                {
                    paragraph.Locked = paragraph.Heading.Locked;
                }
            }
        }

        public void LoadParagraphFromDataRow(Paragraph paragraph, DataRow row)
        {
            paragraph.Key = row["JSEB_SCRKEY"].ToString();
            paragraph.Code = row["JSEB_PARAGRAPH"].ToString();
            paragraph.Description = row["JSEB_DESC"].ToString();
            paragraph.FormatOption = row["JSEB_FMTOPT"].ToString();
            paragraph.Locked = (row["JSEB_LOCKED"].ToString() == "Y");
            paragraph.CostAmt = row["JSEB_COST"].ToDec();
            paragraph.SellAmtEx = row["JSEB_SALE"].ToDec();
            paragraph.SellAmtGST = row["JSEB_GST_AMOUNT"].ToDec();
            paragraph.MarginCode = row["JSEB_MGNCDE"].ToString();
            paragraph.DefMarginPct = row["JSEB_MGNPCT"].ToDec();
            paragraph.OriginalMargin = row["JSEB_MGNPCT"].ToDec();
            paragraph.IncludeGST = row["JSEB_GST_INC"].ToBool();

            LoadParagraphExtras(paragraph);
        }

        public void LoadParagraphExtras(Paragraph paragraph)
        {
            var spec = App.Odbc.SpecL("JCSPEC_R", "JS_R_KEY", paragraph.Key, "JS_R_DESC", "r");
            paragraph.LoadOptions(spec);
        }

        public List<Paragraph> GetParagraphs(Heading heading)
        {
            var paragraphs = new List<Paragraph>();
            try
            {
                string fields = "CO_KEY,JSEA_LOCKED,";
                fields += ParagraphFields;
                string join = "JOIN CONHDRA ON CO_JOBNUM = JSEB_JOBNUM ";
                join += "JOIN JCSPEC_A ON JSEA_JOBNUM = JSEB_JOBNUM AND JSEB_SECT = JSEA_SECT AND JSEB_HEADING = JSEA_HEADING";
                string where = "CO_KEY ='" + heading.Contract.ContractNumber + "'";                
                where += " AND JSEB_SECT = '" + heading.Section + "'";
                where += " AND JSEB_HEADING = '" + heading.Code + "'";                
                string query = "SELECT " + fields + " FROM JCSPEC_B " + join + " WHERE " + where;
                query += " ORDER BY JSEB_PARAGRAPH ASC";
                var table = App.Odbc.Table(query);
                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        var locked = (row["JSEB_LOCKED"].ToString() == "Y");
                        Paragraph para = new Paragraph(heading);
                        LoadParagraphFromDataRow(para, row);
                        if (!para.Locked)
                        {
                            para.Locked = (row["JSEA_LOCKED"].ToString() == "Y");
                        }
                        paragraphs.Add(para);
                    }

                    if (paragraphs.Any(p => p.FormatOption.Equals("H")))
                    {
                        Paragraph parent = null;
                        int childCount = 0;
                        foreach (var para in paragraphs.OrderBy(p => p.Code))
                        {
                            if (!para.SubHeading)
                            {
                                if (childCount > 0) parent.IsParent = true;
                                childCount = 0;
                                parent = para;
                            }
                            else
                            {
                                if (parent != null)
                                {
                                    childCount++;
                                    parent.SumAmountEx += para.SellAmtEx;
                                    parent.SumAmountGst += para.SellAmtGST;
                                }
                            }
                        }

                    }
                    
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }             
            return paragraphs;
        }
        #endregion

        #region ParagraphCombo
        public List<ComboItem> ParagraphCombo(EstimateDetail estimateDetail)
        {
            var comboParagraphs = new List<ComboItem>();
            if (estimateDetail != null)
            {
                var heading = estimateDetail.Paragraph.Heading;
                foreach (var paragraph in heading.Contract.Paragraphs(heading))
                {
                    comboParagraphs.Add(new ComboItem(paragraph.Code, paragraph.Description));
                }
            }
            return comboParagraphs;
        }
        #endregion
               
        #region EstimateImage
        public string EstimateImage(EstimateDetail detail, int thumbSize)
        {
            return ItemImage(detail.Item, thumbSize);
        }

        public string ItemImage(String itemCode, int thumbSize)
        {
            string ImageBase = App.Utils.ImagesFolder + "products/";
            string[] exts = { ".jpg", ".jpeg", ".png", ".gif" };
            string imageFile = ImageBase + itemCode;
            bool exists = false;

            foreach (string ext in exts)
            {
                string img = imageFile + ext;
                exists = File.Exists(HttpContext.Current.Server.MapPath(img));
                if (exists)
                {
                    imageFile = img;
                    break;
                }
            }

            //TODO: support jcspec_c image file
            if (!exists)
            {
                return string.Empty;
            }

            if (thumbSize > 0)
            {
                imageFile = "~/Shared/Thumbnail.aspx?image=" + imageFile;
                imageFile += "&width=" + thumbSize.ToString();
                imageFile += "&height=" + thumbSize.ToString();
            }
            return Helpers.ResolveUrl(imageFile);
        }
        #endregion
        
        #region EstimateDetailLinesCopy
        public bool EstimateDetailLinesCopy(List<EstimateDetail> detailLines, Paragraph paragraph)
        {

            bool success = App.Api.CopyEstimateDetail(detailLines, paragraph);
            return success;
        }
        #endregion
        
        #region EstimateDetailLinesInsert
        public bool EstimateDetailLinesInsert(List<EstimateDetail> detailLines, EstimateDetail detail)
        {

            bool success = App.Api.InsertEstimateDetail(detailLines, detail);
            return success;
        }
        #endregion
                
        #region TemplateCouncils
        private List<ComboItem> templateCouncils;
        public List<ComboItem> TemplateCouncils
        {
            get
            {
                if (templateCouncils == null || templateCouncils.Count == 0)
                {
                    var filter = "BMSB_CODE='" + App.Settings.Comp(CompanySettingsType.CouncilCode).ToString() + "'";
                    templateCouncils = GetDropDownList("BMSB_HEADING", "BMSB_DESC", "BMSPEC_B", filter);                    
                }
                return templateCouncils;
            }
        }
        #endregion

        #region TemplateEstates
        private List<ComboItem> templateEstates;
        public List<ComboItem> TemplateEstates
        {
            get
            {
                if (templateEstates == null || templateEstates.Count == 0)
                {
                    var filter = "BMSB_CODE='" + App.Settings.Comp(CompanySettingsType.EstateCode).ToString() + "'";
                    templateEstates = GetDropDownList("BMSB_HEADING", "BMSB_DESC", "BMSPEC_B", filter);
                }
                return templateEstates;
            }
        }
        #endregion

        #region UOM Codes
        private List<ComboItem> uomCodes;
        public List<ComboItem> UOMCodes
        {
            get
            {
                if (uomCodes == null || uomCodes.Count == 0)
                {
                    uomCodes = GetDropDownList("UM_CODE", "UM_DESC", "INVTBLG");
                }
                return uomCodes;
            }
        }
        #endregion

        #region PriceDisplay Codes
        private List<ComboItem> pdCodes;
        public List<ComboItem> PDCodes
        {
            get
            {
                if (pdCodes == null || pdCodes.Count == 0)
                {
                    pdCodes = GetDropDownList("BMSE_CODE", "BMSE_DESC", "BMSPEC_E");                    
                }
                return pdCodes;
            }
        }
        #endregion

        #region MarginCodes
        private List<ComboItem> marginCodes;
        public List<ComboItem> MarginCodes
        {
            get
            {
                if (marginCodes == null || marginCodes.Count == 0)
                {
                    marginCodes = GetDropDownList("BMSD_CODE", "BMSD_DESC", "BMSPEC_D", "", "BMSD_MARGIN");
                }
                return marginCodes;
            }
        }
        #endregion

        #region CostCentres
        private List<ComboItem> costCentres;
        public List<ComboItem> CostCentres
        {
            get
            {
                if (costCentres == null || costCentres.Count == 0)
                {
                    string filter = "CC_ACTIVE<>'N'";
                    costCentres = GetDropDownList("CC_CODE", "CC_DESC", "JOBTBLA", filter);
                }
                return costCentres;
            }
        }
        #endregion

        #region TemplateControl
        private TemplateSettings templateControl;
        public TemplateSettings TemplateControl
        {
            get
            {
                if (templateControl == null)
                {
                    var list = App.Utils.GetRecords<TemplateSettings>("JOBTBL2", "CONSTANT=JP02_CODE\t02\t2");
                    if (list != null)
                    {
                        templateControl = list.FirstOrDefault();
                    }
                    if (templateControl == null)
                    {
                        templateControl = new TemplateSettings();
                    }
                }
                return templateControl;
            }
        }
        #endregion

        #region Templates        
        private List<Template> templates;
        public List<Template> Templates
        {
            get
            {
                if (templates == null || templates.Count == 0)
                {
                    templates = new List<Template>();
                    var group1List = GetDropDownList("JSETA_CODE", "JSETA_DESC", "JOBTBL_A", "JSETA_ACTIVE <> 'N'");
                    var group2List = GetDropDownList("JSETB_CODE", "JSETB_DESC", "JOBTBL_B", "JSETB_ACTIVE <> 'N'");
                    var group3List = new List<ComboItem>();
                    decimal.TryParse(TemplateControl.Group3Length, out decimal Group3Length);
                    if (Group3Length > 0)
                    {
                        group3List = GetDropDownList("JSETC_CODE", "JSETC_DESC", "JOBTBL_C", "JSETC_ACTIVE <> 'N'");
                    }
                    var codes = GetDropDownList("BMSA_CODE", "BMSA_DESC", "BMSPEC_A");
                    var headings = GetDropDownList("BMSB_SCRKEY", "BMSB_DESC", "BMSPEC_B");
                    foreach (var group1 in group1List)
                    {
                        foreach (var group2 in group2List)
                        {
                            if (group3List.Count > 0)
                            {
                                foreach (var group3 in group3List)
                                {
                                    var keyval = group1.Code + group2.Code + group3.Code;
                                    foreach (var heading in headings.Where(p => p.Code.StartsWith(keyval)))
                                    {
                                        var template = codes.Find(p => p.Code.Equals(heading.Code.Substring(0, 6)));
                                        templates.Add(AddTemplate(template, group1, group2, group3, heading));
                                    }
                                }
                            }
                            else
                            {
                                var keyval = group1.Code + group2.Code;
                                foreach (var heading in headings.Where(p => p.Code.StartsWith(keyval)))
                                {
                                    var template = codes.Find(p => p.Code.Equals(heading.Code.Substring(0, 6)));
                                    templates.Add(AddTemplate(template, group1, group2, null, heading));
                                }
                            }
                        }
                    }
                }
                return templates;
            }            
        }

        private Template AddTemplate(ComboItem template, ComboItem group1, ComboItem group2, ComboItem group3, ComboItem heading)
        {
            template = template ?? new ComboItem("", "");
            group3 = group3 ?? new ComboItem("", "");
            return new Template()
            {
                Code = (template.Code, template.Desc),
                Group1 = (group1.Code, group1.Desc),
                Group2 = (group2.Code, group2.Desc),
                Group3 = (group3.Code, group3.Desc),
                Heading = (heading.Code, heading.Desc)
            };
        }
        #endregion

        #region Jobs
        private List<Job> jobs;
        public List<Job> Jobs
        {
            get
            {
                if (jobs == null)
                {
                    jobs = new List<Job>();

                    var fields = "JC_JOBNUM,JC_CUST,JC_CUSNAM,JC_AADDR1,JC_AADDR2,JC_AADDR3,JC_STATUS,JC_OPCENTRE,JC_DISTRICT";

                    var query = "SELECT " + fields + " FROM JCHEADA";
                    query += " WHERE { fn LENGTH(JC_JOBNUM)} > 0";

                    //query +=Helpers.GetFilterString("JC_STATUS", App.Settings.Comp.ActiveContractStatusList, 1);

                    //if (!string.IsNullOrEmpty(App.Settings.Comp.ContractsQuery))
                        //query += " " + App.Settings.Comp.ContractsQuery;

                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            var address = string.Concat(string.Concat(row["JC_AADDR1"].ToString(), row["JC_AADDR2"].ToString()), row["JC_AADDR3"].ToString());
                            jobs.Add(new Job()
                            {
                                JobNumber = row["JC_JOBNUM"].ToString(),
                                CustomerNumber = row["JC_CUST"].ToString(),
                                Name = row["JC_CUSNAM"].ToString(),
                                OpCentre = row["JC_OPCENTRE"].ToString(),
                                District = row["JC_DISTRICT"].ToString(),
                                Address = address,
                                Status = row["JC_STATUS"].ToString()
                            });
                        }
                    }
                }
                return jobs;
            }
        }
        #endregion

        #region GetJob
        public Job GetJob(string JobNumber)
        {
            return Jobs.Where(p => p.JobNumber == JobNumber).FirstOrDefault();
        }
        #endregion

    }
}
