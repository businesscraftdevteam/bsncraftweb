using DC = BsnCraft.Shared.DTO.Classes;
using BsnCraft.Shared.DTO.Model;
using BsnCraft.Shared.DTO.View;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.HtmlControls;
using BsnCraft.Shared.DTO.Classes;

namespace BsnCraft.Web.Common.Controllers
{
    public class AreaPricesViewModel : BsnViewModel
    {
        #region Public Properties
        public int SelectedGroupCount { get; set; }
        public int SelectedCatCount { get; set; }
        public int SelectedSortCount { get; set; }
        public ComboItem SelectedProductGroup { get; set; }
        public ComboItem SelectedProductCat { get; set; }
        public string CurrentArea { get; private set; } = "....";
        public DateTime CurrentEffective { get; private set; } = DateTime.Today;
        public Boolean IncludeMissingPrices { get; private set; }
        #endregion

        #region Constructor
        public AreaPricesViewModel(BsnApplication app) : base(app, Base.Controllers.AreaPrices)
        {

        }
        #endregion
        
        #region Update Selection
        public void UpdateSelection(AreaPriceSelection selection)
        {
            CurrentArea = selection.Area;
            IncludeMissingPrices = selection.IncludeMissing;
            CurrentEffective = selection.EffectiveDate;
            if (AreaPriceLists.ContainsKey((CurrentArea, CurrentEffective)))
            {
                AreaPriceLists.Remove((CurrentArea, CurrentEffective));
            }
        }
		#endregion
		
        #region SelectedAreaPrices
        public List<AreaPrice> SelectedAreaPrices
        {
            get
            {
                try
                {
                    var prices = CurrentPricing;
                    var filterProductGroups = new List<ComboItem>(App.VM.APR.ProductGroups.Where(p => p.IsSelected));
                    var filterProductCats = new List<ComboItem>(App.VM.APR.ProductCats.Where(p => p.IsSelected));
                    var filterProductSortCodes = new List<ComboItem>(App.VM.APR.SortCodes.Where(p => p.IsSelected));
                    if (filterProductGroups.Count > 0)
                    {
                        prices = prices.Where(p => filterProductGroups.Any(i => i.Code.Equals(p.ProductGroup.Code))).ToList();
                    }
                    if (filterProductCats.Count > 0)
                    {
                        prices = prices.Where(p => filterProductCats.Any(i => i.Code.Equals(p.ProductCategory.Code))).ToList();
                    }
                    if (filterProductSortCodes.Count > 0)
                    {
                        prices = prices.Where(p => filterProductSortCodes.Any(i => i.Code.Equals(p.SortCode.Code))).ToList();
                    }
                    return prices;
                }
                catch (Exception ex)
                {
                    App.Utils.LogExc(ex);
                }
                return new List<AreaPrice>();
            }
	   }
        #endregion

        #region CurrentPricing
        public List<AreaPrice> CurrentPricing
        {
            get
            {
                if (string.IsNullOrEmpty(CurrentArea) || CurrentEffective.Equals(DateTime.MinValue))
                {
                    return new List<AreaPrice>();
                }
                if (!AreaPriceLists.ContainsKey((CurrentArea, CurrentEffective)))
                {
                    LoadAreaPrices(CurrentArea, CurrentEffective);
                }
                if (AreaPriceLists.ContainsKey((CurrentArea, CurrentEffective)))
                {
                    return AreaPriceLists[(CurrentArea, CurrentEffective)];
                }
                return new List<AreaPrice>();
            }
        }
        #endregion

        #region AreaPrices
        private Dictionary<(string, DateTime), List<AreaPrice>> areaPriceLists;
        public Dictionary<(string, DateTime), List<AreaPrice>> AreaPriceLists
        {
            get
            {
                if (areaPriceLists == null)
                {
                    areaPriceLists = new Dictionary<(string, DateTime), List<AreaPrice>>();
                }
                return areaPriceLists;
            }
        }
        #endregion

        #region LoadAreaPrices
        public void LoadAreaPrices(string area, DateTime effectiveDate)
        {
            var strResponse = string.Empty;
            try
            {
                CurrentArea = area;
                CurrentEffective = effectiveDate;
                if (AreaPriceLists.ContainsKey((area, effectiveDate)))
                {
                    return;
                }
                AreaPriceLists.Add((area, effectiveDate), new List<AreaPrice>());
                var dto = new AreaPrices();
                var request = new DC.DtoRequest
                {
                    Command = DC.ApiKey.AreaPriceList.ToString()
                };
                request.AddKey(new DC.KeyInfo(area));
                request.AddParam("EffectiveDate", effectiveDate.ToAus());
                var Specifications = new List<string>
                {
                    " ",
                    "E",
                    "P"
                };
                request.AddParam("Specifications", Specifications);
                request.AddParam("IncludeMissingPrices", IncludeMissingPrices ? "Y": "N" );
                request.MaxRecs = 500;
                var strRequest = JsonConvert.SerializeObject(request);
                var success = App.Api.WebData(ref strRequest, ref strResponse);
                while (success)
                {
                    //Log.Trace("Area Prices response: " + strResponse);
                    if (!string.IsNullOrEmpty(strResponse))
                    {
                        dto = (AreaPrices)JsonConvert.DeserializeObject(strResponse, typeof(AreaPrices));
                    }

                    foreach (var item in dto.Prices)
                    {
                        var imagePath = App.VM.JC.ItemImage(item.ItemNumber, 0);
                        if (!string.IsNullOrEmpty(imagePath))
                        {
                            var thumb = Classes.Helpers.ResolveUrl("~/Viewers/Thumbnail.aspx?");
                            thumb += "image=" + imagePath;
                            thumb += "&width=" + App.Settings.Comp(CompanySettingsType.ThumbnailWidth).ToInt();
                            thumb += "&height=" + App.Settings.Comp(CompanySettingsType.ThumbnailHeight).ToInt();

                            var card = new HtmlGenericControl("div");
                            card.Attributes["class"] = "card m-1";

                            var img = new HtmlGenericControl("img");
                            img.Attributes["src"] = thumb;
                            img.Attributes["class"] = "card-img-top";
                            card.Controls.Add(img);

                            item.ImageHTML = card.ToHtml();
                        }    
                    }
                    AreaPriceLists[(area, effectiveDate)].AddRange(dto.Prices);
                    dto.Prices.Clear();
                    strResponse = string.Empty;
                    request = (DtoRequest)JsonConvert.DeserializeObject(strRequest, typeof(DtoRequest));
                    if (!string.IsNullOrWhiteSpace(request.RFA))
                    {
                        success = App.Api.WebData(ref strRequest, ref strResponse);
                    }
                    else
                    {
                        break;
                    }
                    
                }

                //if (!success)
                //{
                    //return;
                //}
                
                
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex, true, strResponse);
            }
        }
        #endregion

        #region ProductGroups
        private List<ComboItem> _ProductGroups;
        public List<ComboItem> ProductGroups
        {
            get
            {
                if (_ProductGroups == null || _ProductGroups.Count == 0)
                {
                    string filter = "PG_ACTIVE<>'N'";
                    _ProductGroups = GetDropDownList("PG_GROUP", "PG_DESC", "INVTBLA", filter);
                }
                return _ProductGroups;
            }
        }
        #endregion

        #region ProductCats
        private List<ComboItem> _ProductCats;
        public List<ComboItem> ProductCats
        {
            get
            {
                if (_ProductCats == null || _ProductCats.Count == 0)
                {
                    string filter = "PC_ACTIVE<>'N'";
                    _ProductCats = GetDropDownList("PC_CATEGORY", "PC_DESC", "INVTBLB", filter);
                }
                return _ProductCats;
            }
        }
        #endregion
        
        #region SortCodes
        private List<ComboItem> _SortCodes;
        public List<ComboItem> SortCodes
        {
            get
            {
                if (_SortCodes == null || _SortCodes.Count == 0)
                {
                    string filter = "BM_X_ACTIVE<>'N'";
                    _SortCodes = GetDropDownList("BM_X_CODE", "BM_X_DESC", "BMSPEC_X", filter);
                }
                return _SortCodes;
            }
        }
        #endregion

        #region CostCentres
        private List<ComboItem> _CostCentres;
        public List<ComboItem> CostCentres
        {
            get
            {
                if (_CostCentres == null || _CostCentres.Count == 0)
                {
                    string filter = "CC_ACTIVE<>'N'";
                    _CostCentres = GetDropDownList("CC_CODE", "CC_DESC", "JOBTBLA", filter);
                }
                return _CostCentres;
            }
        }
        #endregion
        
        #region Areas
        private List<ComboItem> _Areas;
        public List<ComboItem> Areas
        {
            get
            {
                if (_Areas == null || _Areas.Count == 0)
                {
                    string filter = "RAC_ACTIVE<>'N'";
                    _Areas = GetDropDownList("RAC_CODE", "RAC_DESC", "INVTBLI", filter);
                }
                return _Areas;
            }
        }
        #endregion
        
        #region ClearProductCatSelection
        public void ClearProductCatSelection()
        {
            foreach (var item in App.VM.APR.ProductCats)
            {
                item.IsSelected = false;
            }
            SelectedCatCount = 0;
        }
        #endregion

        #region ClearSortCodeSelection
        public void ClearSortCodeSelection()
        {
            foreach (var item in App.VM.APR.SortCodes)
            {
                item.IsSelected = false;
            }
            SelectedSortCount = 0;
        }
        #endregion

    }
}
