﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BsnCraft.Web.Common.Controllers
{
    public class SalesCentresViewModel : BsnViewModel
    {
        public SalesCentresViewModel(BsnApplication app) : base(app, Base.Controllers.SalesCentres)
        {

        }

        #region SalesCentres
        private List<ComboItem> salesCentres;
        public List<ComboItem> SalesCentres
        {
            get
            {
                if (salesCentres == null || salesCentres.Count == 0)
                {
                    salesCentres = new List<ComboItem>();
                    string filter = "COT_P_ACTIVE <> 'N'";
                    filter += " " + App.Settings.Comp(CompanySettingsType.DisplayCentresQuery).ToString();
                    filter = filter.TrimEnd();
                    salesCentres = GetDropDownList("COT_P_CODE", "COT_P_DESC", "COTABL_P", filter);                    
                }
                return salesCentres;
            }
        }
        #endregion

        #region FindDisplayInfo
        public DisplayInfo FindDisplayInfo(string salesCentre, string key)
        {
            if (displayInfo == null)
            {
                displayInfo = new Dictionary<string, List<DisplayInfo>>();
            }

            var salesCentreDisplayInfo = new List<DisplayInfo>();
            if (displayInfo.ContainsKey(salesCentre))
            {
                salesCentreDisplayInfo = displayInfo[salesCentre];
            }

            return salesCentreDisplayInfo.Where(p => p.Key.Equals(key)).FirstOrDefault();
        }
        #endregion
        
        #region GetDisplayInfo
        private Dictionary<string, List<DisplayInfo>> displayInfo;
        public List<DisplayInfo> GetDisplayInfo(List<DateTime> dates, string salesCentre)
        {         
            if (displayInfo == null)
            {
                displayInfo = new Dictionary<string, List<DisplayInfo>>();
            }

            var salesCentreDisplayInfo = new List<DisplayInfo>();
            if (displayInfo.ContainsKey(salesCentre))
            {
                bool containsDates = true;
                salesCentreDisplayInfo = displayInfo[salesCentre];
                foreach (var day in dates)
                {
                    if (!salesCentreDisplayInfo.Any(p => p.Day.Date.Equals(day.Date)))
                    {
                        containsDates = false;
                        break;
                    }
                }
                if (containsDates)
                {
                    return salesCentreDisplayInfo.Where(p => dates.Contains(p.Day)).OrderBy(p => p.Day).ToList();
                }
            }
            else
            {
                displayInfo.Add(salesCentre, salesCentreDisplayInfo);
            }

            var key = 0;
            var julian = string.Empty;
            var updateKey = string.Empty;
            var answer = string.Empty;
            var ansCode = string.Empty;
            
            foreach (var day in dates)
            {
                if (salesCentreDisplayInfo.Any(p => p.Day.Date.Equals(day.Date)))
                {
                    continue;
                }

                julian = day.Year.ToString() + day.DayOfYear.ToString("000");
                var answers = DisplayInfoAnswers(julian + salesCentre);

                foreach (var question in DisplayInfoQuestions)
                {
                    updateKey = string.Empty;
                    answer = string.Empty;
                    ansCode = string.Empty;

                    if (answers.ContainsKey(question.Code))
                    {
                        var data = answers[question.Code];
                        updateKey = Helpers.GetArray(data, 0);
                        answer = Helpers.GetArray(data, 1);
                        ansCode = Helpers.GetArray(data, 2);
                    }

                    //Format Numeric Values
                    if (question.Type == "N" || question.Type == "P")
                    {
                        if (!string.IsNullOrEmpty(answer) && !string.IsNullOrEmpty(question.Dec))
                        {
                            answer = answer.Replace(",", "").Replace(".", "");
                            var dp = int.Parse(question.Dec);
                            var numeric = int.Parse(answer);
                            if (dp == 0)
                            {
                                answer = numeric.ToString();
                            }
                            else
                            {
                                var dpstring = "1000000000";
                                var denominator = int.Parse(dpstring.Substring(0, dp + 1));
                                var dec = Convert.ToDouble(numeric) / denominator;
                                answer = dec.ToString("F" + dp);
                            }
                        }
                    }

                    //TODO: Consider passing Sequence or Saving all Questions up front
                    //The sequence number will currently be whichever order questions are answered


                    salesCentreDisplayInfo.Add(new DisplayInfo()
                    {
                        Key = (key += 10).ToString(),
                        Day = day,
                        SalesCentre = salesCentre,
                        Code = question.Code,
                        Prompt = question.Prompt,
                        Type = question.Type,
                        Min = question.Min,
                        Max = question.Max,
                        Dec = question.Dec,
                        Default = question.Default,
                        Answers = question.Answers,
                        Answer = answer,
                        AnsCode = ansCode,
                        UpdateKey = updateKey
                    });
                }
            }
            return salesCentreDisplayInfo.Where(p => dates.Contains(p.Day)).OrderBy(p => p.Day).ToList();
        }
        #endregion

        #region GetDisplayData
        public List<DisplayInfo> GetDisplayData(List<DateTime> dates, string salesCentre)
        {
            int key = 0;
            string fields = "XIL_SCRKEY,XIL_CODE,XIL_PROMPT,XIL_REFCODE,XIL_ANSWER,XIL_ANSCODE,XIL_ANSTYPE";
            string query = string.Empty;
            string julian = string.Empty;
            
            var displayInfo = new List<DisplayInfo>();
            foreach (DateTime day in dates)
            {
                julian = day.Year.ToString() + day.DayOfYear.ToString("000");
                var where = "XIL_MODULE = 'CSC' AND XIL_REFCODE='" + julian + salesCentre + "'";
                query = "SELECT " + fields + " FROM INFLINA WHERE " + where;
                var table = App.Odbc.Table(query);
                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        displayInfo.Add(new DisplayInfo()
                        {
                            Key = key++.ToString(),
                            Day = day,
                            SalesCentre = salesCentre,
                            Code = row["XIL_CODE"].ToString(),
                            Prompt = row["XIL_PROMPT"].ToString(),
                            AnsCode = row["XIL_ANSCODE"].ToString(),
                            Answer = row["XIL_ANSWER"].ToString(),
                            Type = row["XIL_ANSTYPE"].ToString()
                        });
                    }
                }
            }
            return displayInfo;
        }
        #endregion

        #region DisplayInfo Combo
        private Dictionary<string, List<ComboItem>> displayInfoCombo;
        private List<ComboItem> DisplayInfoCombo(string category)
        {
            if (displayInfoCombo == null)
            {
                displayInfoCombo = new Dictionary<string, List<ComboItem>>();
            }
            if (displayInfoCombo.ContainsKey(category))
            {
                return displayInfoCombo[category];
            }
            var comboAnswers = new List<ComboItem>();            
            var fields = "XIB_CAT,XIB_CATCODE,XIB_ANSWER";
            var where = "WHERE XIB_MODULE = 'CSC' AND XIB_CAT = '" + category + "'";
            var query = "SELECT " + fields + " FROM INFTBLB " + where + " ORDER BY XIB_CAT ASC";
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    comboAnswers.Add(new ComboItem(row["XIB_CATCODE"].ToString(), row["XIB_ANSWER"].ToString()));                                                
                }
                displayInfoCombo.Add(category, comboAnswers);
            }            
            return comboAnswers;
        }
        #endregion

        #region DisplayInfo Questions
        private List<DisplayInfo> displayInfoQuestions;
        public List<DisplayInfo> DisplayInfoQuestions
        {
            get
            {
                if (displayInfoQuestions == null || displayInfoQuestions.Count == 0)
                {
                    displayInfoQuestions = new List<DisplayInfo>();
                    string fields = "XIA_MODULE,XIA_CODE,XIA_PROMPT,XIA_ANSTYPE,XIA_MIN,XIA_MAX,XIA_DEC,XIA_DEF,XIA_LNK_PRF";
                    string where = "WHERE XIA_MODULE = 'CSC'";
                    string query = "SELECT " + fields + " FROM INFTBLA " + where + " ORDER BY XIA_SORT_ORDER ASC, XIA_CODE ASC";
                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            var cat = row["XIA_LNK_PRF"].ToString();
                            var answers = (!string.IsNullOrEmpty(cat)) ? 
                                DisplayInfoCombo(cat) : new List<ComboItem>();

                            displayInfoQuestions.Add(new DisplayInfo()
                            {
                                Code = row["XIA_CODE"].ToString(),
                                Prompt = row["XIA_PROMPT"].ToString(),
                                Type = row["XIA_ANSTYPE"].ToString(),
                                Min = row["XIA_MIN"].ToString(),
                                Max = row["XIA_MAX"].ToString(),
                                Dec = row["XIA_DEC"].ToString(),
                                Default = row["XIA_DEF"].ToString(),
                                Answers = answers
                            });
                        }
                    }
                }
                return displayInfoQuestions;
            }
        }
        #endregion

        #region DisplayInfo Answers
        private Dictionary<string, string[]> DisplayInfoAnswers(string RefCode)
        {
            var answers = new Dictionary<string, string[]>();

            var fields = "XIL_SCRKEY,XIL_CODE,XIL_ANSWER,XIL_ANSCODE";
            var where = "XIL_MODULE = 'CSC' AND XIL_REFCODE = '" + RefCode + "'";
            var query = "SELECT " + fields + " FROM INFLINA WHERE " + where;
            
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var data = new string[3];
                    data[0] = row["XIL_SCRKEY"].ToString();
                    data[1] = row["XIL_ANSWER"].ToString();
                    data[2] = row["XIL_ANSCODE"].ToString();
                    answers.Add(row["XIL_CODE"].ToString(), data);
                }
            }

            return answers;
        }
        #endregion

        #region UpdateDisplayInfo
        public void UpdateDisplayInfo(string salesCentre)
        {
            bool updated = false;
            foreach (var info in displayInfo[salesCentre])
            {
                if (info.Modified)
                {
                    updated = true;
                    if (!string.IsNullOrEmpty(info.UpdateKey))
                    {
                        App.Api.ExtraInfoUpdate(info.UpdateKey, info.AnsCode, info.Answer);
                    }
                    else
                    {
                        App.Api.ExtraInfoAdd(info.JulianDay + info.SalesCentre, info.Code, info.AnsCode, info.Answer);
                    }
                    info.Modified = false;
                }
            }

            if (updated)
            {
                App.Utils.SetSuccess();
            }
        }
        #endregion

    }
}
