﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Views.Grid.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BsnCraft.Web.Common.Controllers
{
    public class ActivitiesViewModel : BsnViewModel
    {
        public ActivitiesViewModel(BsnApplication app) : base(app, Base.Controllers.Activities)
        {

        }

        #region Activity
        public Activity Activity
        {
            get
            {
                return Activities.Where(p => p.IsSelected).FirstOrDefault();
            }
        }
        #endregion

        #region Activity Contracts
        private Dictionary<string, List<Contract>> activityContracts;
        public List<Contract> ActivityContracts
        {
            get
            {
                if (activityContracts == null)
                {
                    activityContracts = new Dictionary<string, List<Contract>>();
                }
                var activity = (Activity != null) ? Activity.Code : string.Empty;                
                if (string.IsNullOrEmpty(activity))
                {
                    return new List<Contract>();
                }
                if (activityContracts.ContainsKey(activity))
                {
                    return activityContracts[activity];
                }
                var contracts = new List<Contract>();
                var query = "SELECT CSA_CONTRACTA FROM CONSUMA WHERE CSA_ACTIVITY = '" + activity + "'";
                var table = App.Odbc.Table(query);
                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        var contract = App.VM.CO.GetContract(row["CSA_CONTRACTA"].ToString());
                        if (contract != null)
                        {
                            contracts.Add(contract);
                        }
                    }
                }
                activityContracts.Add(activity, contracts);
                return contracts;
            }
        }
        #endregion

        #region Activities
        private List<Activity> activities;
        public List<Activity> Activities
        {
            get
            {
                if (activities == null || activities.Count == 0)
                {
                    activities = new List<Activity>();

                    var fields = "COTM_CODE,COTM_DESC,COTM_USER_SOURCE,COTM_EVENT_USER,CSB_CO_COUNT";
                    var join = "LEFT OUTER JOIN CONSUMB ON CSB_ACTIVITY = COTM_CODE";
                    var query = "SELECT " + fields + " FROM COTABLM " + join;

                    var list = App.Settings.Comp(CompanySettingsType.ActivitiesFilter).ToQuotedList();
                    if (list.Count > 0)
                    {
                        query += " WHERE COTM_CODE IN (" + string.Join(",", list) + ")";
                    }

                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            var activity = row["COTM_CODE"].ToString();
                            int.TryParse(row["CSB_CO_COUNT"].ToString(), out int count);
                            activities.Add(new Activity()
                            {
                                Code = activity,
                                Desc = row["COTM_DESC"].ToString(),
                                UserSource = row["COTM_USER_SOURCE"].ToString(),
                                UserEvent = row["COTM_EVENT_USER"].ToString(),
                                Count = count                                
                            });
                        }
                    }
                }
                return activities;
            }
        }
        #endregion

        #region ActivityUser
        public class ActivityUser
        {
            public string Code { get; set; }
            public string Desc { get; set; }
            public int Count { get; set; }

            public ActivityUser(string code, string desc)
            {
                Code = code;
                Desc = desc;
                Count = 1;
            }
        }
        #endregion

        #region ActivityUsers
        private Dictionary<string, List<ActivityUser>> users;
        public List<ActivityUser> Users
        {
            get
            {
                if (users == null)
                {
                    users = new Dictionary<string, List<ActivityUser>>();
                }
                var activityUsers = new List<ActivityUser>();
                if (Activity == null)
                {
                    return activityUsers;
                }
                if (users.ContainsKey(Activity.Code))
                {
                    return users[Activity.Code];
                }
                var allUsers = App.VM.CO.Employees;
                var userSource = Activity.UserSource;
                var userEvent = Activity.UserEvent;
                foreach (Contract contract in ActivityContracts)
                {
                    switch (userSource)
                    {
                        case "C":
                            var custServ = activityUsers.Where(p => p.Code == contract.CustomerService).FirstOrDefault();
                            if (custServ != null)
                            {
                                custServ.Count++;
                            }
                            else
                            {
                                var user = allUsers.Where(p => p.Code == contract.SalesConsultant).FirstOrDefault();
                                if (user != null)
                                {
                                    activityUsers.Add(new ActivityUser(user.Code, user.Desc));
                                }
                            }
                            break;
                        case "B":
                            // Building Supervisor
                            var buildSupv = activityUsers.Where(p => p.Code == contract.BuildingSupervisor).FirstOrDefault();
                            if (buildSupv != null)
                            {
                                buildSupv.Count++;
                            }
                            else
                            {
                                var user = allUsers.Where(p => p.Code == contract.SalesConsultant).FirstOrDefault();
                                if (user != null)
                                {
                                    activityUsers.Add(new ActivityUser(user.Code, user.Desc));
                                }
                            }
                            break;
                        case "S":
                            // Sales Consultant
                            var salesCons = activityUsers.Where(p => p.Code == contract.SalesConsultant).FirstOrDefault();
                            if (salesCons != null)
                            {
                                salesCons.Count++;
                            }
                            else
                            {
                                var user = allUsers.Where(p => p.Code == contract.SalesConsultant).FirstOrDefault();
                                if (user != null)
                                {
                                    activityUsers.Add(new ActivityUser(user.Code, user.Desc));
                                }
                            }
                            break;
                        case "E":
                            // Event Employee
                            if (!contract.Events.Any(p => p.Code.Equals(userEvent)))
                            {
                                //Bad user setup
                                break;
                            }
                            var contractEvent = contract.Events.Where(p => p.Code.Equals(userEvent)).FirstOrDefault();
                            var eventUser = activityUsers.Where(p => p.Code.Equals(contractEvent.EmpCode)).FirstOrDefault();
                            if (eventUser != null)
                            {
                                eventUser.Count++;
                            }
                            else
                            {
                                var user = allUsers.Where(p => p.Code.Equals(contractEvent.EmpCode)).FirstOrDefault();
                                if (user != null)
                                {
                                    activityUsers.Add(new ActivityUser(user.Code, user.Desc));
                                }
                            }
                            break;
                    }
                }
                users.Add(Activity.Code, activityUsers);
                return activityUsers;
            }
        }
        #endregion
        
        #region Activity Events
        private Dictionary<string, List<ComboItem>> activityEventCodes;
        public List<ComboItem> ActivityEventCodes(string code)
        {
            if (activityEventCodes == null || activityEventCodes.Count == 0)
            {
                activityEventCodes = new Dictionary<string, List<ComboItem>>();
                foreach (var activity in Activities)
                {
                    activityEventCodes[activity.Code] = new List<ComboItem>();
                }
                var query = "SELECT DISTINCT COTO_ACTIVITY,COTO_EVENTA FROM COTABLO";
                var table = App.Odbc.Table(query);
                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        var list = activityEventCodes[row["COTO_ACTIVITY"].ToString()];
                        var contractEvent = App.VM.CO.GetEventCode(row["COTO_EVENTA"].ToString());
                        if (contractEvent != null)
                        {
                            list.Add(contractEvent);
                        }
                    }
                }
            }
            if (activityEventCodes.ContainsKey(code))
            {
                return activityEventCodes[code];
            }
            return new List<ComboItem>();
        }
        #endregion
    }
}
