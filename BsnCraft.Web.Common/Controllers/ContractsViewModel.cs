﻿using BsnCraft.Shared.DTO.View;
using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using DTO = BsnCraft.Shared.DTO.Classes;

namespace BsnCraft.Web.Common.Controllers
{
    public class ContractsViewModel : BsnViewModel
    {
        #region Public Properties
        public bool MultiStatusCodes
        {
            get
            {
                var sts = App.Settings.Comp(CompanySettingsType.ActiveContractStatusList).ToStrArray();
                return sts.Length > 1;
            }
        }
        #endregion

        #region Constructor
        public ContractsViewModel(BsnApplication app) : base(app, Base.Controllers.Contracts)
        {

        }
        #endregion
        
        #region Contracts
        private List<Contract> contracts;
        public List<Contract> Contracts
        {
            get
            {
                if (contracts == null || contracts.Count == 0)
                {
                    contracts = new List<Contract>();

                    var fields = "CO_KEY,CO_CUSNUM,CO_NAME,CO_LOTNO,CO_STREETNO,CO_ADDR,CO_CITY,CO_STATE,CO_ZIP,CO_STATUS,CO_SALETYPE";
                    fields += ",CO_DISTRICT,CO_COUNCIL,CO_SALEPROM,CO_SALECENT,CO_COMM1,CO_COMM2,CO_COMM3,CO_HOUSE,CO_ELEVATION";
                    fields += ",CO_SUBJSALE,CO_SAVEPLAN,CO_JOBNUM,CO_SALEGROUP,CO_OPCENTRE,CO_DP,CO_VOLUME,CO_FOLIO";
                    fields += ",CO_SALESCONS,CO_CSR,CO_BLDGSUPV,CO_SERVSUPV";

                    var sts = App.Settings.Comp(CompanySettingsType.ActiveContractStatusList).ToStrArray();                    
                    var query = "SELECT " + fields + " FROM CONHDRA";
                    query += " WHERE { fn LENGTH(CO_KEY)} > 0";
                    query +=Helpers.GetFilterString("CO_STATUS", sts, 1);                    

                    if (App.Settings.Comp(CompanySettingsType.RestrictContracts).ToBool() && !App.Api.AllContracts)
                    {
                        query += " AND CO_SALESCONS = '" + App.Api.Initials + "'";
                    }

                    query += " " + App.Settings.Comp(CompanySettingsType.ContractsQuery).ToString();
                    query = query.TrimEnd();

                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            contracts.Add(new Contract(App)
                            {
                                ContractNumber = row["CO_KEY"].ToString(),
                                CustomerNumber = row["CO_CUSNUM"].ToString(),
                                Name = row["CO_NAME"].ToString(),
                                LotNumber = row["CO_LOTNO"].ToString(),
                                StreetNumber = row["CO_STREETNO"].ToString(),
                                Address = row["CO_ADDR"].ToString(),
                                City = row["CO_CITY"].ToString(),
                                State = row["CO_STATE"].ToString(),
                                PostCode = row["CO_ZIP"].ToString(),
                                Status = row["CO_STATUS"].ToString(),
                                SaleType = row["CO_SALETYPE"].ToString(),
                                SalesConsultant = row["CO_SALESCONS"].ToString(),
                                CustomerService = row["CO_CSR"].ToString(),
                                BuildingSupervisor = row["CO_BLDGSUPV"].ToString(),
                                District = row["CO_DISTRICT"].ToString(),
                                Council = row["CO_COUNCIL"].ToString(),
                                SalesPromotion = row["CO_SALEPROM"].ToString(),
                                SalesCentre = row["CO_SALECENT"].ToString(),
                                Comment1 = row["CO_COMM1"].ToString(),
                                Comment2 = row["CO_COMM2"].ToString(),
                                Comment3 = row["CO_COMM3"].ToString(),
                                House = row["CO_HOUSE"].ToString(),
                                Facade = row["CO_ELEVATION"].ToString(),
                                SubjectSale = row["CO_SUBJSALE"].ToString(),
                                SavingsPlan = row["CO_SAVEPLAN"].ToString(),                                
                                SaleGroup = row["CO_SALEGROUP"].ToString(),
                                OpCentre = row["CO_OPCENTRE"].ToString(),
                                DpNumber = row["CO_DP"].ToString(),
                                Volume = row["CO_VOLUME"].ToString(),
                                Folio = row["CO_FOLIO"].ToString(),
                                JobNumber = row["CO_JOBNUM"].ToString()
                            });
                        }
                    }
                }
                return contracts;
            }
        }
        #endregion
        
        #region LookupContracts
        public List<FieldData> LookupContracts(string query)
        {  
            var match = Contracts.Where(p => p.ContractNumber.Contains(query) || 
                p.CustomerNumber.Contains(query) ||
                p.FriendlyName.ToLower().Contains(query) ||
                p.FullAddress.ToLower().Contains(query)).Take(10);

            var results = new List<FieldData>();
            foreach (var item in match)
            {
                var value = item.ContractNumber + " | " + item.FriendlyName + " | " + item.FullAddress;
                results.Add(new FieldData(value, JsonConvert.SerializeObject(item)));
            }
            return results;
        }
        #endregion
        
        #region CustomerContracts
        public List<Contract> CustomerContracts(string customerNumber)
        {
            return Contracts.Where(p => p.CustomerNumber == customerNumber).ToList();
        }
        #endregion

        #region New Contract
        public Contract NewContract(bool isLead = false)
        {
            var contract = new Contract(App)
            {
                IsLead = isLead,
                State = App.Settings.Comp(CompanySettingsType.DefaultState).ToString(),
                District = Helpers.GetDefault(App.Settings.Comp(CompanySettingsType.DistrictFilter).ToStrArray()),
                OpCentre = Helpers.GetDefault(App.Settings.Comp(CompanySettingsType.OpCentreFilter).ToStrArray()),
                Council = Helpers.GetDefault(App.Settings.Comp(CompanySettingsType.CouncilFilter).ToStrArray()),
                SalesPromotion = Helpers.GetDefault(App.Settings.Comp(CompanySettingsType.PromotionFilter).ToStrArray()),
                SalesCentre = Helpers.GetDefault(App.Settings.Comp(CompanySettingsType.SaleCentreFilter).ToStrArray()),
                SaleType = Helpers.GetDefault(App.Settings.Comp(CompanySettingsType.SaleTypeFilter).ToStrArray())
            };
            if (contract.IsLead && App.VM.LE.MultiStatusCodes)
            {
                contract.Status = Helpers.GetDefault(App.Settings.Comp(CompanySettingsType.ActiveLeadStatusList).ToStrArray());
            }
            if (!contract.IsLead && App.VM.CO.MultiStatusCodes)
            {
                contract.Status = Helpers.GetDefault(App.Settings.Comp(CompanySettingsType.ActiveContractStatusList).ToStrArray());
            }
            return contract;
        }
        #endregion

        #region GetContract
        public Contract GetContract(string contractNumber)
        {
            var contract = Contracts.Where(p => p.ContractNumber == contractNumber).FirstOrDefault();
            if (contract == null)
            {
                //Since a Lead is a contract object, should be safe. Saves too much duplication in Leads module
                contract = App.VM.LE.Leads.Where(p => p.ContractNumber == contractNumber).FirstOrDefault();
            }
            return contract;
        }
        #endregion

        #region CheckEffective
        public void CheckEffective(string contractNumber)
        {
            var effective = App.Settings.Comp(CompanySettingsType.EffectiveDate).ToString();
            if (string.IsNullOrEmpty(effective))
                return;

            var fields = "CO_KEY,CO_JOBNUM,JSM_EFFECTIVE";
            string join = " JOIN JCSPECM ON JSM_JOBNUM = CO_JOBNUM";
            var query = "SELECT " + fields + " FROM CONHDRA" + join;
            query += " WHERE CO_KEY = '" + contractNumber + "'";

            var table = App.Odbc.Table(query);
            if (table != null && table.Rows.Count > 0)
            {
                DataRow row = table.Rows[0];
                var effectiveDate = row["JSM_EFFECTIVE"].ToDate().ToJulian();
                if (effective != effectiveDate)
                {
                    var jobNumber = row["CO_JOBNUM"].ToString();
                    Log.Info("Updating " + jobNumber + " Effective: " + effectiveDate + " to " + effective);
                    App.Api.SetEffective(jobNumber);
                }
            }
        }
        #endregion

        #region SingleContract
        public Contract SingleContract(string contractNumber)
        {
            Contract contract = null;

            var fields = "CO_KEY,CO_CUSNUM,CO_NAME,CO_LOTNO,CO_STREETNO,CO_ADDR,CO_CITY,CO_STATE,CO_ZIP,CO_STATUS,CO_SALETYPE";
            fields += ",CO_DISTRICT,CO_COUNCIL,CO_SALEPROM,CO_SALECENT,CO_COMM1,CO_COMM2,CO_COMM3,CO_SUBJSALE,CO_SAVEPLAN";
            fields += ",CO_JOBNUM,CO_SALEGROUP,CO_OPCENTRE,CO_SALESCONS,CO_CSR,CO_BLDGSUPV,CO_SERVSUPV";
            var query = "SELECT " + fields + " FROM CONHDRA WHERE CO_KEY = '" + contractNumber + "'";
            
            var table = App.Odbc.Table(query);
            if (table != null && table.Rows.Count > 0)
            {
                DataRow row = table.Rows[0];
                contract = new Contract(App)
                {
                    ContractNumber = row["CO_KEY"].ToString(),
                    CustomerNumber = row["CO_CUSNUM"].ToString(),
                    Name = row["CO_NAME"].ToString(),
                    LotNumber = row["CO_LOTNO"].ToString(),
                    StreetNumber = row["CO_STREETNO"].ToString(),
                    Address = row["CO_ADDR"].ToString(),
                    City = row["CO_CITY"].ToString(),
                    State = row["CO_STATE"].ToString(),
                    PostCode = row["CO_ZIP"].ToString(),
                    Status = row["CO_STATUS"].ToString(),
                    SaleType = row["CO_SALETYPE"].ToString(),
                    SalesConsultant = row["CO_SALESCONS"].ToString(),
                    CustomerService = row["CO_CSR"].ToString(),
                    BuildingSupervisor = row["CO_BLDGSUPV"].ToString(),
                    District = row["CO_DISTRICT"].ToString(),
                    Council = row["CO_COUNCIL"].ToString(),
                    SalesPromotion = row["CO_SALEPROM"].ToString(),
                    SalesCentre = row["CO_SALECENT"].ToString(),
                    Comment1 = row["CO_COMM1"].ToString(),
                    Comment2 = row["CO_COMM2"].ToString(),
                    Comment3 = row["CO_COMM3"].ToString(),
                    SubjectSale = row["CO_SUBJSALE"].ToString(),
                    SavingsPlan = row["CO_SAVEPLAN"].ToString(),
                    JobNumber = row["CO_JOBNUM"].ToString(),
                    SaleGroup = row["CO_SALEGROUP"].ToString(),
                    OpCentre = row["CO_OPCENTRE"].ToString()
                };
            }

            return contract;
        }
        #endregion

        #region FinanceData
        public void FinanceData()
        {
            var request = new DTO.DtoRequest
            {
                Command = "financeView"
            };
            request.AddKey(new DTO.KeyInfo("001015"));
            var strRequest = JsonConvert.SerializeObject(request);
            var strResponse = string.Empty;
            if (App.Api.WebData(ref strRequest, ref strResponse))
            {
                Finance data = (Finance)JsonConvert.DeserializeObject(strResponse, typeof(Finance));
                var contractValue = data.Contracts.Where(p => p.ContractNumber.Equals("001015")).FirstOrDefault().Value;
            }
        }
        #endregion
      
        #region UpdateContract
        public void UpdateContract(Contract contract)
        {
            string key = App.Api.UpdateContract(contract);
            if (contract.IsLead)
            {
                App.VM.LE.Leads.Clear();
            }
            else
            {
                Contracts.Clear();
            }
        }
        #endregion

        #region ContractSettings
        private ContractSettings settings;
        public ContractSettings Settings
        {

            get
            {
                if (settings == null)
                {
                    var contractSettings = App.Utils.GetRecords<ContractSettings>("CONHDRB", "");
                    if (contractSettings == null)
                    {
                        App.Utils.SetError("Error getting Contract Settings.");
                        settings = new ContractSettings();                        
                    }
                    else
                    {
                        settings = contractSettings.FirstOrDefault();
                    }
                }                
                return settings;
            }
        }
        #endregion
        
        #region Documents        
        public List<Document> GetDocuments(Contract contract, string xRefSource = "", string xRefKey = "")
        {
            var documents = new List<Document>();
            string fields = "CLW_DOCTYP,CLW_SCRKEY,CLW_CONTRACT,CLW_SEQNUM,CLW_DOCREF,CLW_DESCR,CLW_CRE_DATE,CLW_ALT_DOCS,CLW_XREF_KEY,CLW_XREF_SRC,";
            fields += " CASE CLW_RATING WHEN 'H' THEN 1 WHEN 'M' THEN 2 WHEN 'L' THEN 3 END AS RATING";
            string query = "SELECT " + fields + " FROM CONLINW WHERE CLW_CONTRACT = '" + contract.ContractNumber + "'";
            if (!string.IsNullOrEmpty(xRefSource) && !string.IsNullOrEmpty(xRefKey))
            {
                query += " AND CLW_XREF_SRC='" + xRefSource + "' AND CLW_XREF_KEY='" + xRefKey + "'";
            }

            var list = App.Settings.Comp(CompanySettingsType.DocumentTypes).ToQuotedList();
            if (list.Count > 0)
            { 
                query += " AND CLW_DOCTYP IN (" + string.Join(",", list) + ")";
            }
            query += " ORDER BY CLW_SCRKEY ASC";

            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var docType = row["CLW_DOCTYP"].ToString();                    
                    documents.Add(new Document(contract)
                    {
                        Key = row["CLW_SCRKEY"].ToString(),
                        Type = docType,
                        Sequence = row["CLW_SEQNUM"].ToInt().ToString("000"),
                        Reference = row["CLW_DOCREF"].ToString(),
                        Description = row["CLW_DESCR"].ToString(),                            
                        Filename = row["CLW_ALT_DOCS"].ToString(),
                        Locked = App.VM.SY.FindDocumentType(docType, "CO").Locked,
                        Date = row["CLW_CRE_DATE"].ToDate().ToString("dd/MM/yyyy"),
                        Rating = row["RATING"].ToInt(),
                        XRefSource = row["CLW_XREF_SRC"].ToString().ValueOrEmpty(),
                        XRefKey = row["CLW_XREF_KEY"].ToString().ValueOrEmpty()
                    });
                }
            }
            if (documents.Count > 0)
            {
                var notes = DocumentNotes(contract.ContractNumber);
                var spec = string.Empty;
                foreach (var document in documents)
                {
                    spec = string.Empty;
                    if (notes.ContainsKey(document.Key))
                    {
                        spec = notes[document.Key];
                    }
                    document.Notes = spec;
                }
            }                
            return documents;
        }
        #endregion

        #region Document Notes
        private Dictionary<string, string> DocumentNotes(string contractNumber)
        {
            var notes = new Dictionary<string, string>();
            var fields = "CLX_CONTRACT,CLX_CONST,CLX_DESC";
            var where = "CLX_CONTRACT ='" + contractNumber + "'";
            var query = "SELECT " + fields + " FROM CONLINX WHERE " + where;
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                var spec = string.Empty;
                var key = string.Empty;
                int i = 0;
                foreach (DataRow row in table.Rows)
                {
                    i++;
                    if (row["CLX_CONST"].ToString() != key && !string.IsNullOrEmpty(key))
                    {
                        spec = Helpers.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                        spec = string.Empty;
                    }
                    spec += row["CLX_DESC"].ToString();
                    key = row["CLX_CONST"].ToString();
                    if (i == table.Rows.Count)
                    {
                        spec = Helpers.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                    }
                }
            }
            return notes;
        }
        #endregion

        #region GetImportant
        public List<Document> GetImportant(Contract contract)
        {
            var documents = new List<Document>();
            string fields = "CLW_DOCTYP,CLW_SCRKEY,CLW_CONTRACT,CLW_SEQNUM,CLW_DOCREF,CLW_DESCR,CLW_CRE_DATE,CLW_ALT_DOCS,CLW_RATING,";
            fields += "CASE CLW_RATING WHEN 'H' THEN 1 WHEN 'M' THEN 2 WHEN 'L' THEN 3 END AS RATING";
            string where = "WHERE CLW_CONTRACT = '" + contract.ContractNumber + "' AND CLW_RATING <> '' ORDER BY RATING";
            string query = "SELECT " + fields + " FROM CONLINW " + where;

            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    documents.Add(new Document(contract)
                    {
                        Key = row["CLW_SCRKEY"].ToString(),
                        Type = row["CLW_DOCTYP"].ToString(),
                        Sequence = row["CLW_SEQNUM"].ToInt().ToString("000"),
                        Reference = row["CLW_DOCREF"].ToString(),
                        Description = row["CLW_DESCR"].ToString(),
                        Filename = row["CLW_ALT_DOCS"].ToString(),
                        Date = row["CLW_CRE_DATE"].ToDate().ToString("dd/MM/yyyy"),
                        Rating = row["RATING"].ToInt()
                    });
                }
            }
            if (documents.Count > 0)
            {
                var notes = DocumentNotes(contract.ContractNumber);
                var spec = string.Empty;
                foreach (var document in documents)
                {
                    spec = string.Empty;
                    if (notes.ContainsKey(document.Key))
                    {
                        spec = notes[document.Key];
                    }
                    document.Notes = spec;
                }
            }
            return documents;
        }
        #endregion

        #region DealDocuments
        public List<Document> GetDealDocuments(Contract contract)
        {
            var dealDocuments = new List<Document>();
            foreach (var docType in App.Settings.Company.DealDocuments)
            {
                var doc = contract.Documents.Where(p => p.DealKey == docType.Key).FirstOrDefault();
                if (doc == null)
                {
                    doc = new Document(contract)
                    {
                        Key = docType.Key,
                        Type = docType.Type,
                        Reference = docType.Reference,
                        Description = docType.Description
                    };
                }
                bool uploaded = !(string.IsNullOrEmpty(doc.Filename));
                doc.SubmitStatus = uploaded ? 1 : 0;
                if (!uploaded && docType.Required)
                {
                    doc.SubmitStatus = -1;
                }
                dealDocuments.Add(doc);
            }
            return dealDocuments;
        }
        #endregion

        #region DocumentImage
        public string DocumentImage(Document document, int thumbSize)
        {
            string filename = App.Utils.DocsFolder + Path.GetFileName(document.Filename);
            
            if (!File.Exists(HttpContext.Current.Server.MapPath(filename)))
                return string.Empty;
                    
            if (thumbSize != 0)
            {
                filename = "~/Shared/Thumbnail.aspx?image=" + filename;
                if (thumbSize > 0)
                {
                    filename += "&width=" + thumbSize.ToString();
                    filename += "&height=" + thumbSize.ToString();
                }
            }
            return Helpers.ResolveUrl(filename);
        }
        #endregion

        #region DocumentTypes
        private List<DocumentType> documentTypes;
        public List<DocumentType> DocumentTypes
        {
            get
            {
                //TODO: consider building this from SystemViewModel document types
                if (documentTypes == null || documentTypes.Count == 0)
                {
                    documentTypes = new List<DocumentType>();
                    string fields = "IVWP_CODE,IVWP_MODULE,IVWP_MERGEFMT,IVWP_DESCR,IVWP_FILE";
                    string query = "SELECT " + fields + " FROM IVCTBLP WHERE IVWP_MODULE='CO'";
                    var list = App.Settings.Comp(CompanySettingsType.DocumentTypes).ToQuotedList();
                    if (list.Count > 0)
                    {
                        query += " AND IVWP_CODE IN (" + string.Join(",", list) + ")";
                    }

                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            var code = row["IVWP_CODE"].ToString();
                            var descr = row["IVWP_DESCR"].ToString();
                            var module = row["IVWP_MODULE"].ToString();

                            var docType = new DocumentType(code, descr)
                            {
                                ExtraData = module,
                                MasterDocument = row["IVWP_FILE"].ToString()
                            };
                            documentTypes.Add(docType);
                        }
                    }
                }
                return documentTypes;
            }
        }

        public string DocTypeDesc(string code)
        {
            var docTypes = DocumentTypes.Where(p => p.Code == code);
            if (docTypes == null || docTypes.Count() == 0)
            {
                return string.Empty;
            }
            return docTypes.FirstOrDefault().Desc;
        }
        #endregion
        
        #region JobInfoDocuments
        private List<ComboItem> jobInfoDocTypes;
        public List<ComboItem> JobInfoDocTypes
        {
            get
            {
                if (jobInfoDocTypes == null)
                {
                    jobInfoDocTypes = new List<ComboItem>();
                    string fields = "IVWP_CODE,IVWP_MODULE,IVWP_MERGEFMT,IVWP_DESCR";
                    string query = "SELECT " + fields + " FROM IVCTBLP WHERE IVCTBLP.IVWP_MODULE='CO'";
                    var list = App.Settings.Comp(CompanySettingsType.JobInfoDocumentTypes).ToQuotedList();
                    if (list.Count > 0)
                    {
                        query += " AND IVWP_CODE IN (" + string.Join(",", list) + ")";
                    }

                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            jobInfoDocTypes.Add(new ComboItem(row["IVWP_CODE"].ToString(), row["IVWP_DESCR"].ToString()));
                        }
                    }
                }
                return jobInfoDocTypes;
            }
        }
        #endregion

        #region UpdateDocument
        public void UpdateDocument(Document document)
        {
            var bsnparams = string.Empty;
            if (!string.IsNullOrEmpty(document.UploadFile))
            {
                string ext = Path.GetExtension(document.UploadFile).ToUpper().Replace(".","");
                bsnparams += "TYPE=UPLOAD,EXT=" + ext + ",";
            }

            bool success = false;
            bool isSitePhoto = false;            
            string newkey = string.Empty;
            
            if (document.Module == "CO")
            {
                if (!string.IsNullOrEmpty(document.ReportParameters))
                {
                    bsnparams += "RPTPARAMS=" + document.ReportParameters + ",";
                }

                if (!string.IsNullOrEmpty(document.XRefSource) && !string.IsNullOrEmpty(document.XRefKey))
                {
                    bsnparams += "XREF_SRC=" + document.XRefSource + ",";
                    bsnparams += "XREF_KEY=" + document.XRefKey + ",";
                }

                isSitePhoto = (document.Type == Settings.SitePhotoDocType);

                /*
                if (string.IsNullOrEmpty(document.Reference))
                {
                    document.Reference = (isSitePhoto) ? PageModes.Photos.ToString() : DocTypeDesc(document.Type);                
                }
                */

                if (string.IsNullOrEmpty(document.Description))
                {
                    document.Description = DocTypeDesc(document.Type);
                }
                success = App.Api.ContractDocument(document, ref bsnparams);
            }
            else
            {
                success = App.Api.JobDocument(document, ref bsnparams);
            }

            if (success && !string.IsNullOrEmpty(document.UploadFile))
            {
                byte[] bytes = File.ReadAllBytes(document.UploadFile);
                if (isSitePhoto)
                {
                    var maxHeight = App.Settings.Comp(CompanySettingsType.PhotoHeight).ToInt();
                    var maxWidth = App.Settings.Comp(CompanySettingsType.PhotoWidth).ToInt();
                    Log.Debug("MaxHeight = " + maxHeight.ToString() + ", MaxWidth = " + maxWidth.ToString());
                    if (maxHeight > 0 || maxWidth > 0)
                    {
                        bytes = App.Utils.CreateThumbnail(document.UploadFile, maxWidth, maxHeight);
                    }
                }
                success = App.Api.Upload(ref bytes, bsnparams);
            }

            var contract = document.Contract;
            contract.Documents.Clear();

            if (success)
            {
                App.Utils.SetSuccess();
            }
        }
        #endregion

        #region DownloadDocument
        public bool DownloadDocument(Document document)
        {
            return DownloadDocument(document, false);
        }

        public bool DownloadDocument(Document document, bool useExisting, int waitSeconds = 0)
        {
            if (document == null || string.IsNullOrEmpty(document.Filename))
            {
                return false;
            }

            string filename = Path.GetFileName(document.Filename);
            filename = HttpContext.Current.Server.MapPath(App.Utils.DocsFolder) + filename;
            if (File.Exists(filename) && useExisting)
            {
                return true;
            }

            byte[] bytes = new byte[0];
            if (!App.Api.Download(ref bytes, document.Filename, waitSeconds))
            {
                return false;
            }

            try
            {
                Log.Debug("Downloading document: " + filename);
                using (var fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
            catch (Exception ex)
            {
                App.Utils.LogExc(ex);
                return false;
            }

            return true;
        }
        #endregion       

        #region EventChecklist
        private Dictionary<string, string> eventChecklist;
        public Dictionary<string, string> EventChecklist
        {
            get
            {
                if (eventChecklist == null)
                {
                    eventChecklist = new Dictionary<string, string>();
                    string query = "SELECT COTW_CODEA,COTW_PARENT FROM COTABLW WHERE COTW_PARENT > 0";
                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            var parent = row["COTW_PARENT"].ToInt().ToString("00000");
                            eventChecklist.Add(row["COTW_CODEA"].ToString(), parent);
                        }
                    }
                }
                return eventChecklist;
            }
        }
        #endregion

        #region Events
        public List<Event> GetEvents(Contract contract)
        {
            var events = new List<Event>();
            var fields = "CLE_KEY,CLE_SCRKEY,CLE_CONTRACT,CLE_EVENTA,CLE_DESC,CLE_REGDATE,CLE_FOREDATE,CLE_DUEDATE,CLE_ACTDATE,";
            fields += "CLE_PCTCMPL,CLE_EMPLOYEE,CLE_AMOUNT,CLE_REF,CO_CONTRACT,CO_SALEGROUP,COTG_CODE,COTG_NAME,";
            fields += "COTW_CATG,COTW_USERCAT,COTX_EVENTA,COTX_GROUP,COTX_SORT_ORDER";
            var join = "JOIN CONHDRA ON CO_CONTRACT = CLE_CONTRACT ";
            join += "LEFT OUTER JOIN COTABLG ON CLE_EMPLOYEE = COTG_CODE ";
            join += "JOIN COTABLX ON CLE_EVENTA = COTX_EVENTA AND CO_SALEGROUP = COTX_GROUP ";
            join += "JOIN COTABLW ON CLE_EVENTA = COTW_CODEA";
            var mainQuery = "SELECT " + fields + " FROM CONLINE " + join + " WHERE CLE_CONTRACT='" + contract.ContractNumber + "'";
            var catgQuery = mainQuery;
            bool hasCheckList = (EventChecklist.Count > 0);
            var list = App.Settings.Comp(CompanySettingsType.EventCategories).ToQuotedList();
            if (list.Count > 0)
            {
                catgQuery += " AND COTW_CATG IN (" + string.Join(",", list) + ")";
            }
            catgQuery += " " + App.Settings.Comp(CompanySettingsType.EventsQuery).ToString();
            catgQuery = catgQuery.TrimEnd();            
            var order = " ORDER BY COTX_SORT_ORDER, CLE_EVENTA";
            catgQuery += order;

            var table = App.Odbc.Table(catgQuery);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    AddEvent(contract, row, events, hasCheckList);
                }
            }

            //Add any custom events outside of main categories below
            var dealSubmit = App.Settings.Comp(CompanySettingsType.DealSubmitEvent).ToInt();
            if (dealSubmit != 0)
            {
                var dealCode = dealSubmit.ToString("00000");
                if (!events.Any(p => p.Key.Equals(dealCode)))
                {
                    string dealEvent = mainQuery;
                    dealEvent += " AND CLE_EVENTA = '" + dealCode + "'";
                    dealEvent += order;

                    table = App.Odbc.Table(dealEvent);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            AddEvent(contract, row, events, hasCheckList);
                        }
                    }
                }
            }
            
            if (events.Count > 0)
            {
                var notes = EventNotes(contract.ContractNumber);
                var spec = string.Empty;
                foreach (var contractEvent in events)
                {
                    spec = string.Empty;
                    if (notes.ContainsKey(contractEvent.Key))
                    {
                        spec = notes[contractEvent.Key];
                    }
                    contractEvent.Notes = spec;
                }
            }
            return events;
        }

        public void AddEvent(Contract contract, DataRow row, List<Event> events, bool hasCheckList = false)
        {
            string eventCode = row["CLE_EVENTA"].ToString();
            string parent = string.Empty;
            if (hasCheckList)
            {
                if (EventChecklist.ContainsKey(eventCode))
                {
                    parent = EventChecklist[eventCode];
                }
            }
            var key = row["CLE_SCRKEY"].ToString();
            if (events.Any(p => p.Key.Equals(key)))
            {
                //Shouldn't happen but will break grid edit
                return;
            };
            events.Add(new Event(contract)
            {
                Key = row["CLE_SCRKEY"].ToString(),
                Code = row["CLE_EVENTA"].ToString(),
                Description = row["CLE_DESC"].ToString(),
                CatgCode = row["COTW_USERCAT"].ToString(),
                EmpCode = row["CLE_EMPLOYEE"].ToString(),
                Employee = row["COTG_NAME"].ToString().ValueOrEmpty(),
                Reference = row["CLE_REF"].ToString(),
                Amount = row["CLE_AMOUNT"].ToCurrency(),
                PctComplete = row["CLE_PCTCMPL"].ToString(),
                Forecast = row["CLE_FOREDATE"].ToDate(),
                Due = row["CLE_DUEDATE"].ToDate(),
                Actual = row["CLE_ACTDATE"].ToDate(),
                Registered = row["CLE_REGDATE"].ToDate(),
                Parent = parent
            });
        }
        #endregion

        #region Event Notes
        private Dictionary<string, string> EventNotes(string contractNumber)
        {
            var notes = new Dictionary<string, string>();
            
            var fields = "CLK_CONTRACT,CLK_CONST,CLK_DESC";
            var where = "CLK_CONTRACT ='" + contractNumber + "'";
            var query = "SELECT " + fields + " FROM CONLINK WHERE " + where;

            var table = App.Odbc.Table(query);
            if (table != null)
            {
                var spec = string.Empty;
                var key = string.Empty;
                int i = 0;
                foreach (DataRow row in table.Rows)
                {
                    i++;
                    if (row["CLK_CONST"].ToString() != key && !string.IsNullOrEmpty(key))
                    {
                        spec = Helpers.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                        spec = string.Empty;
                    }
                    spec += row["CLK_DESC"].ToString();
                    key = row["CLK_CONST"].ToString();
                    if (i == table.Rows.Count)
                    {
                        spec = Helpers.CleanSpec(spec, "\r\n");
                        notes.Add(key, spec);
                    }
                }
            }

            return notes;
        }
        #endregion

        #region UpdateEvent
        public void UpdateEvent(Event contractEvent)
        {
            App.Api.EventUpdate(contractEvent);
            var contract = contractEvent.Contract;
            contract.Events.Clear();
        }
        #endregion

        #region EventRegister
        public void EventRegister(Event contractEvent)
        {
            if (contractEvent == null)
            {
                App.Utils.SetError("Event not found.");
                return;
            }
            App.Api.EventRegister(contractEvent);
            var contract = contractEvent.Contract;
            contract.Events.Clear();
        }
        #endregion

        #region ChecklistHeaders
        private List<ChecklistHeader> checklistHeaders;
        public List<ChecklistHeader> ChecklistHeaders
        {
            get
            {
                if (checklistHeaders == null || checklistHeaders.Count == 0)
                {
                    checklistHeaders = new List<ChecklistHeader>();

                    var query = "SELECT * FROM CONTBLG";
                    var table = App.Odbc.Table(query);
                    if (table != null)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            checklistHeaders.Add(new ChecklistHeader() {
                                Template = row["CTG_TEMPLATE"].ToString(),
                                Description = row["CTG_DESC"].ToString(),
                                Trigger = row["CTG_TRIG_EVENT"].ToString(),
                                Complete = row["CTG_CMPL_EVENT"].ToString()
                            });
                        }
                    }
                }
                return checklistHeaders;
            }
        }
        #endregion

        #region ChecklistLines
        public List<ComboItem> ChecklistLines(string template)
        {
            return GetDropDownList("CTH_GROUP", "CTH_DESC", "CONTBLH", "CTH_TEMPLATE = '" + template + "'"); ;
        }
        #endregion

        #region CheckLists
        public List<Checklist> GetChecklists(Contract contract)
        {
            var checklists = new List<Checklist>();
            var fields = "CL_C_KEY,CL_C_SEQNUMA,CL_C_SRC_TMPL,CL_C_DESC";
            var query = "SELECT " + fields + " FROM CONLIN_C WHERE CL_C_CONSTANT = '" + contract.ContractNumber + "'";

            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    checklists.Add(new Checklist()
                    {
                        Key = row["CL_C_KEY"].ToString(),
                        ContractNumber = contract.ContractNumber,
                        Sequence = row["CL_C_SEQNUMA"].ToString(),
                        Template = row["CL_C_SRC_TMPL"].ToString(),
                        Description = row["CL_C_DESC"].ToString()
                    });
                }
            }
            return checklists;
        }
        #endregion

        #region OpCentres
        private List<ComboItem> opCentres;
        public List<ComboItem> OpCentres
        {
            get
            {
                if (opCentres == null || opCentres.Count == 0)
                {
                    var list = App.Settings.Comp(CompanySettingsType.OpCentreFilter).ToStrArray();
                    string filter = "COTY_ACTIVE<>'N'";
                    filter +=Helpers.GetFilterString("COTY_CODE", list);
                    opCentres = GetDropDownList("COTY_CODE", "COTY_DESC", "COTABLY", filter);
                }
                return opCentres;
            }
        }
        #endregion

        #region Districts
        private List<ComboItem> districts;
        public List<ComboItem> Districts
        {
            get
            {
                if (districts == null || districts.Count == 0)
                {
                    var list = App.Settings.Comp(CompanySettingsType.DistrictFilter).ToStrArray();
                    string filter = "COTZ_ACTIVE<>'N'";
                    filter +=Helpers.GetFilterString("COTZ_CODE", list);
                    districts = GetDropDownList("COTZ_CODE", "COTZ_DESC", "COTABLZ", filter);
                }
                return districts;
            }
        }
        #endregion

        #region Councils
        private List<ComboItem> councils;
        public List<ComboItem> Councils
        {
            get
            {
                if (councils == null || councils.Count == 0)
                {
                    var list = App.Settings.Comp(CompanySettingsType.CouncilFilter).ToStrArray();
                    string filter = "COT_C_ACTIVE<>'N'";
                    filter +=Helpers.GetFilterString("COT_C_CODE", list);
                    councils = GetDropDownList("COT_C_CODE", "COT_C_DESC", "COTABL_C", filter);
                }
                return councils;
            }
        }
        #endregion

        #region Employees
        private List<ComboItem> employees;
        public List<ComboItem> Employees
        {
            get
            {
                if (employees == null || employees.Count == 0)
                {
                    string filter = "COTG_ACTIVE <> 'N'";
                    employees = GetDropDownList("COTG_CODE", "COTG_NAME", "COTABLG", filter, "COTG_CATCODE");
                }
                return employees;
            }
        }
        #endregion

        #region SalesConsultants
        private List<ComboItem> salesConsultants;
        public List<ComboItem> SalesConsultants
        {
            get
            {
                if (salesConsultants == null || salesConsultants.Count == 0)
                {
                    var list = App.Settings.Comp(CompanySettingsType.SalesConsultantCategories).ToStrArray();
                    string filter = "COTG_ACTIVE <> 'N'";
                    filter +=Helpers.GetFilterString("COTG_CATCODE", list, 1);
                    salesConsultants = GetDropDownList("COTG_CODE", "COTG_NAME", "COTABLG", filter);
                }
                if (salesConsultants.Count == 0)
                {
                    return Employees;
                }
                return salesConsultants;
            }
        }
        #endregion

        #region States
        private List<ComboItem> states;
        public List<ComboItem> States
        {
            get
            {
                if (states == null || states.Count == 0)
                {
                    states = GetDropDownList("COT_J_CODE", "COT_J_DESC", "COTABL_J");
                    if (states == null || states.Count == 0)
                    { 
                        states = new List<ComboItem>
                        {
                            new ComboItem("NSW", "New South Wales"),
                            new ComboItem("VIC", "Victoria"),
                            new ComboItem("QLD", "Queensland"),
                            new ComboItem("SA", "South Australia"),
                            new ComboItem("ACT", "Australian Capital Territory"),
                            new ComboItem("WA", "Western Australia"),
                            new ComboItem("TAS", "Tasmania"),
                            new ComboItem("NT", "Northern Territory")
                        };
                    }
                }
                return states;
            }
        }
        #endregion

        #region SalesPromotions
        private List<ComboItem> salesPromotions;
        public List<ComboItem> SalesPromotions
        {
            get
            {
                if (salesPromotions == null || salesPromotions.Count == 0)
                {
                    var list = App.Settings.Comp(CompanySettingsType.PromotionFilter).ToStrArray();
                    string filter = "COT_B_ACTIVE <> 'N'";
                    filter +=Helpers.GetFilterString("COT_B_CODE", list);
                    salesPromotions = GetDropDownList("COT_B_CODE", "COT_B_DESC", "COTABL_B", filter);
                }
                return salesPromotions;
            }
        }
        #endregion

        #region SalesCentres
        private List<ComboItem> salesCentres;
        public List<ComboItem> SalesCentres
        {
            get
            {
                if (salesCentres == null || salesCentres.Count == 0)
                {
                    var list = App.Settings.Comp(CompanySettingsType.SaleCentreFilter).ToStrArray();
                    string filter = "COT_P_ACTIVE <> 'N'";
                    filter +=Helpers.GetFilterString("COT_P_CODE", list);
                    salesCentres = GetDropDownList("COT_P_CODE", "COT_P_DESC", "COTABL_P", filter);
                }
                return salesCentres;
            }
        }
        #endregion

        #region StatusCodes
        private List<ComboItem> statusCodes;
        public List<ComboItem> StatusCodes
        {
            get
            {                    
                if (statusCodes == null || statusCodes.Count == 0)
                {
                    string filter = "COT_M_ACTIVE<>'N'";
                    statusCodes = GetDropDownList("COT_M_CODE", "COT_M_DESC", "COTABL_M", filter);
                }
                return statusCodes;
            }
        }

        List<ComboItem> contractStatusCodes;
        public List<ComboItem> ContractStatusCodes
        {
            get
            {
                if (contractStatusCodes == null)
                {
                    contractStatusCodes = new List<ComboItem>();
                    var sts = App.Settings.Comp(CompanySettingsType.ActiveContractStatusList).ToStrArray();
                    foreach (var status in sts)
                    {
                        var code = Helpers.RemoveDefaultFormat(status);
                        if (StatusCodes.Any(p => p.Code.Equals(code)))
                        {
                            var statusCode = StatusCodes.Where(p => p.Code.Equals(code)).FirstOrDefault();
                            contractStatusCodes.Add(statusCode);
                        }
                    }
                }
                return contractStatusCodes;
            }
        }

        List<ComboItem> leadStatusCodes;
        public List<ComboItem> LeadStatusCodes
        {
            get
            {
                if (leadStatusCodes == null)
                {
                    leadStatusCodes = new List<ComboItem>();
                    var sts = App.Settings.Comp(CompanySettingsType.ActiveLeadStatusList).ToStrArray();
                    foreach (var status in sts)
                    {
                        var code = Helpers.RemoveDefaultFormat(status);
                        if (StatusCodes.Any(p => p.Code.Equals(code)))
                        {
                            var statusCode = StatusCodes.Where(p => p.Code.Equals(code)).FirstOrDefault();
                            leadStatusCodes.Add(statusCode);
                        }
                    }
                }
                return leadStatusCodes;
            }
        }
        #endregion

        #region SaleTypes
        private List<ComboItem> saleTypes;
        public List<ComboItem> SaleTypes
        {
            get
            {
                if (saleTypes == null || saleTypes.Count == 0)
                {
                    var list = App.Settings.Comp(CompanySettingsType.SaleTypeFilter).ToStrArray();
                    string filter = "COTU_ACTIVE <> 'N'";                    
                    filter +=Helpers.GetFilterString("COTU_CODE", list);
                    saleTypes = GetDropDownList("COTU_CODE", "COTU_DESC", "COTABLU", filter);
                }
                return saleTypes;
            }
        }
        #endregion

        #region SaleGroups
        private List<ComboItem> saleGroups;
        public List<ComboItem> SaleGroups
        {
            get
            {
                if (saleGroups == null || saleGroups.Count == 0)
                {
                    string filter = "COTV_ACTIVE <> 'N' AND COTV_CODE <> '" + Settings.VariationSalesGroup + "'";
                    saleGroups = GetDropDownList("COTV_CODE", "COTV_DESC", "COTABLV", filter);
                }
                return saleGroups;
            }
        }
        #endregion
                
        #region ContractsByOperatingCentre
        private List<ChartData> contractsByOperatingCentre;
        public List<ChartData> ContractsByOperatingCentre
        {
            get
            {
                if (contractsByOperatingCentre == null)
                {
                    contractsByOperatingCentre = new List<ChartData>();
                    foreach (var contract in Contracts)
                    {
                        string operatingCentre = string.Empty;
                        if (string.IsNullOrEmpty(contract.OpCentre))
                        {
                            operatingCentre = "[ Blank ]";
                        }
                        else
                        {
                            try
                            {
                                operatingCentre = OpCentres.Find(p => p.Code == contract.OpCentre).Desc;
                            }
                            catch
                            {
                                operatingCentre = "Unknown: " + contract.OpCentre;
                            }
                        }
                        contractsByOperatingCentre.Increment(operatingCentre);
                    }
                }
                return contractsByOperatingCentre;
            }
        }
        #endregion

        #region EventCodes
        public List<ComboItem> eventCodes;
        public List<ComboItem> EventCodes
        {
            get
            {
                if (eventCodes == null || eventCodes.Count == 0)
                {
                    string filter = "COTW_ACTIVE <> 'N'";
                    eventCodes = GetDropDownList("COTW_CODEA", "COTW_DESC", "COTABLW", filter);                    
                }
                return eventCodes;
            }
        }

        public ComboItem GetEventCode(string eventNumber)
        {
            return EventCodes.Where(p => p.Code == eventNumber).FirstOrDefault();
        }
        #endregion
    }
}