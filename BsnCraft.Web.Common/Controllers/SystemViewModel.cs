﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using BsnCraft.Web.Common.Settings;
using BsnCraft.Web.Common.Settings.Base;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BsnCraft.Web.Common.Controllers
{
    public class SystemViewModel : BsnViewModel
    {
        #region Constructor
        public SystemViewModel(BsnApplication app) : base(app, Base.Controllers.System)
        {

        }
        #endregion

        #region LoginUsers
        private List<LoginUser> loginUsers;
        public List<LoginUser> LoginUsers
        {
            get
            {
                if (loginUsers == null || loginUsers.Count == 0)
                {
                    loginUsers = App.Utils.GetRecords<LoginUser>("QSHSYSA", "");
                }
                return loginUsers;
            }
        }
        #endregion

        #region Find User
        public LoginUser FindUser(string username)
        {
            return LoginUsers.Where(p => p.Username.Equals(username)).FirstOrDefault();
        }
        #endregion

        #region DocumentTypes
        private List<DocumentType> documentTypes;
        public List<DocumentType> DocumentTypes
        {
            get
            {
                if (documentTypes == null || documentTypes.Count == 0)
                {
                    documentTypes = new List<DocumentType>();
                    var fields = "IVWP_CODE,IVWP_MODULE,IVWP_FILE,IVWP_MERGEFMT,IVWP_DESCR,IVWP_NOCHNG";
                    var query = "SELECT " + fields + " FROM IVCTBLP WHERE IVWP_MODULE IN ('', 'CO', 'JC')";
                    var list = App.Settings.Comp(CompanySettingsType.DocumentTypes).ToQuotedList();
                    if (list.Count == 0)
                    {
                        AddDocTypes(query, documentTypes);
                        return documentTypes;
                    }
                    query = "SELECT " + fields + " FROM IVCTBLP WHERE IVWP_MODULE IN ";
                    var main = query + "('', 'CO') AND IVWP_CODE IN (" + string.Join(",", list) + ")";
                    AddDocTypes(main, documentTypes);
                    list = App.Settings.Comp(CompanySettingsType.HeadingDocumentTypes).ToQuotedList();
                    if (list.Count > 0)
                    {
                        var estimate = query + "('', 'CO', 'JC') AND IVWP_CODE IN (" + string.Join(",", list) + ")";
                        AddDocTypes(estimate, documentTypes);
                    }
                    list = App.Settings.Comp(CompanySettingsType.JobInfoDocumentTypes).ToQuotedList();
                    if (list.Count > 0)
                    {
                        var estimate = query + "('', 'CO', 'JC') AND IVWP_CODE IN (" + string.Join(",", list) + ")";
                        AddDocTypes(estimate, documentTypes);
                    }
                }
                return documentTypes;
            }
        }

        void AddDocTypes(string query, List<DocumentType> docTypes)
        {
            var table = App.Odbc.Table(query);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var code = row["IVWP_CODE"].ToString();
                    var module = row["IVWP_MODULE"].ToString();
                    if (docTypes.Any(p => p.Code.Equals(code) && p.ExtraData.Equals(module)))
                    {
                        continue;
                    }
                    var docType = new DocumentType(code, row["IVWP_DESCR"].ToString())
                    {
                        ExtraData = module,
                        MasterDocument = row["IVWP_FILE"].ToString(),
                        Locked = (row["IVWP_NOCHNG"].ToString() == "Y")
                    };
                    docTypes.Add(docType);
                }
            }
        }

        #endregion

        #region Find DocumentType
        public DocumentType FindDocumentType(string code, string module)
        {
            var documentType = App.VM.SY.DocumentTypes.Find(p => p.Code.Equals(code) && p.ExtraData.Equals(module));
            if (!string.IsNullOrEmpty(module) && documentType == null)
            {
                documentType = App.VM.SY.DocumentTypes.Find(p => p.Code.Equals(code) && string.IsNullOrEmpty(p.ExtraData));
            }            
            if (documentType == null)
            {
                documentType = new DocumentType(string.Empty, string.Empty) { Locked = false };
            }
            return documentType;
        }
        #endregion

        #region SettingCats
        private List<ComboItem> _SettingCats;
        public List<ComboItem> SettingCats
        {
            get
            {
                if (_SettingCats == null) _SettingCats = new List<ComboItem>();
                if (_SettingCats.Count == 0)
                {
                    foreach (var item in App.Settings.CompanySettings)
                    {
                        if (_SettingCats.Find(p => p.Code == item.Category.ToString()) == null)
                        {
                            _SettingCats.Add(new ComboItem(item.Category.ToString(), Enum<CompanySettingsCategory>.GetEnumDescriptor(item.Category)));
                        }
                    }
                }
                return _SettingCats;
            }
        }
        #endregion

        #region SettingGroups
        private List<ComboItem> _SettingGroups;
        public List<ComboItem> SettingGroups
        {
            get
            {
                if (_SettingGroups == null) _SettingGroups = new List<ComboItem>();
                if (_SettingGroups.Count == 0)
                {
                    foreach (var item in App.Settings.CompanySettings)
                    {
                        if (_SettingGroups.Find(p => p.Code == item.Group.ToString()) == null)
                       {
                            _SettingGroups.Add(new ComboItem(item.Group.ToString(), Enum<CompanySettingsGroup>.GetEnumDescriptor(item.Group)));
                        }
                    }
                }
                return _SettingGroups;
            }
        }
        #endregion

        //TODO: cache suburbs?
        #region Lookup Suburbs
        public List<FieldData> LookupSuburbs(string query)
        {
            string length = query.Length.ToString();
            string search = "CONSTANT=PC_SUBURB\t" + query.ToUpper() + "\t" + length + ";KEYREF=1;";

            var postcodes = App.Utils.GetRecords<PostCode>("POSTCDA", search, 10);
            
            var results = new List<FieldData>();
            foreach (var item in postcodes)
            {
                var value = item.Suburb + " | " + item.Key.Substring(0,4);
                results.Add(new FieldData(value, JsonConvert.SerializeObject(item)));
            }
            return results;
        }
        #endregion


        public List<CompanySettings> GetSelectedSettings()
        {
            List<CompanySettings> list = App.Settings.CompanySettings;
            List<ComboItem> FilterSettingsGroups = new List<ComboItem>(SettingGroups.Where(p => p.IsSelected));
            List<ComboItem> FilterSettingCats = new List<ComboItem>(SettingCats.Where(p => p.IsSelected));

            if (FilterSettingsGroups.Count > 0)
            {
                list = list.Where(p => FilterSettingsGroups.Find(i => i.Code == p.Group.ToString()) != null).ToList();
            }
            if (FilterSettingCats.Count > 0)
            {
                list = list.Where(p => FilterSettingCats.Find(i => i.Code == p.Category.ToString()) != null).ToList();
            }
            return list;
        }
    }
}