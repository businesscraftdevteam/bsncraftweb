﻿using BsnCraft.Web.Common.Classes;
using BsnCraft.Web.Common.Controllers.Base;
using BsnCraft.Web.Common.Models;
using DevExpress.DashboardWeb;
using DevExpress.Web.Bootstrap;
using DevExpress.XtraReports.Native;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace BsnCraft.Web.Common.Controllers
{
    public class DataViewsViewModel : BsnViewModel
    {
        public DataViewsViewModel(BsnApplication app) : base (app, Base.Controllers.DataViews)
        {

        }

        #region DataViews
        public List<DataViewItem> DataViews
        {
            get
            {
                if (App.Settings.Company.DataViews == null)
                {
                    return new List<DataViewItem>();
                }
                return App.Settings.Company.DataViews;
            }
        }
        #endregion

        #region GetDataView
        public DataViewItem GetDataView(string key, bool isName = false)
        {
            if (!isName)
            {
                return DataViews.Where(p => p.Key.Equals(key)).FirstOrDefault();
            }
            return DataViews.Where(p => p.Name.Equals(key)).FirstOrDefault();
        }
        #endregion

        #region Save
        public void Save(DataViewItem dataView)
        {
            if (!DataViews.Contains(dataView))
            {
                DataViews.Add(dataView);
            }            
            App.Settings.SaveComp();
        }
        #endregion

        #region Delete
        public void Delete(DataViewItem dataView)
        {
            DataViews.Remove(dataView);
            App.Settings.SaveComp();
        }
        #endregion

        #region DataSources
        public List<ComboItem> DataSources
        {
            get
            {
                return Helpers.GetEnums<DataSourceTypes>();
            }
        }
        #endregion

        #region ChartDisplays
        public List<ComboItem> ChartDisplays
        {
            get
            {
                return Helpers.GetEnums<ChartDisplayTypes>();
            }
        }
        #endregion

        #region Charts
        public List<DataViewItem> Charts
        {
            get
            {
                var dataViews = DataViews.Where(p => p.Type.Equals(DataViewTypes.Chart));
                if (dataViews == null)
                {
                    return new List<DataViewItem>();
                }
                return dataViews.ToList();
            }
        }
        #endregion

        #region GetChart
        public DataViewItem GetChart(string name)
        {
            return Charts.Where(p => p.Name == name).FirstOrDefault();
        }
        #endregion

        #region GenerateChart
        public BootstrapChartBase GenerateChart(string chartName)
        {
            BootstrapChartBase cht = null;
            var chart = GetChart(chartName);
            if (chart == null)
            {
                App.Utils.SetError("Error loading chart: " + chartName);
                return cht;
            }

            var type = chart.Display.ToChartType();
            var dataSource = chart.DataSource.ToDataSource();

            switch (type)
            {
                case ChartDisplayTypes.Pie:
                case ChartDisplayTypes.Doughnut:
                    cht = new BootstrapPieChart();
                    if (type == ChartDisplayTypes.Doughnut)
                    {
                        ((BootstrapPieChart)cht).Type = PieChartType.Doughnut;
                    }
                    break;
                case ChartDisplayTypes.Bar:
                case ChartDisplayTypes.Area:
                case ChartDisplayTypes.Line:
                    cht = new BootstrapChart();
                    break;
            }
            //cht.TitleText = chart.Description;
            cht.DataSourceUrl = "~/Viewers/GetData.aspx?data=" + chart.DataSource + "&callback=?";
            var series = GetSeries(type, dataSource);
            cht.SeriesCollection.Add(series);
            return cht;
        }
        #endregion

        #region GetSeries
        public BootstrapChartCommonSeriesBase GetSeries(ChartDisplayTypes type, DataSourceTypes dataSource)
        {
            BootstrapChartCommonSeriesBase series = null;
            switch (type)
            {
                case ChartDisplayTypes.Pie:
                case ChartDisplayTypes.Doughnut:
                    series = new BootstrapPieChartSeries() { ArgumentField = "Argument", ValueField = "Value" };
                    break;
                case ChartDisplayTypes.Bar:
                    series = new BootstrapChartBarSeries() { ArgumentField = "Argument", ValueField = "Value" };
                    break;
                case ChartDisplayTypes.Area:
                    series = new BootstrapChartAreaSeries() { ArgumentField = "Argument", ValueField = "Value" };
                    break;
                case ChartDisplayTypes.Line:
                    series = new BootstrapChartLineSeries() { ArgumentField = "Argument", ValueField = "Value" };
                    break;
            }
            series.Name = GetSeriesName(dataSource);
            return series;
        }

        public string GetSeriesName(DataSourceTypes dataSource)
        {
            switch (dataSource)
            {
                case DataSourceTypes.LeadsBySalesCentre:
                case DataSourceTypes.LeadsByOperatingCentre:
                case DataSourceTypes.LeadsBySalesConsultant:
                    return "Leads";
            }
            return string.Empty;
        }
        #endregion

        #region Dashboards
        public List<DataViewItem> Dashboards
        {
            get
            {
                var dataViews = DataViews.Where(p => p.Type.Equals(DataViewTypes.Dashboard));
                if (dataViews == null)
                {
                    return new List<DataViewItem>();
                }
                return dataViews.ToList();
            }
        }
        #endregion

        #region GetDashboard
        public DataViewItem GetDashboard(string name)
        {
            return Dashboards.Where(p => p.Name == name).FirstOrDefault();
        }
        #endregion
        
        #region Reports
        public List<DataViewItem> Reports
        {
            get
            {
                var dataViews = DataViews.Where(p => p.Type.Equals(DataViewTypes.Report));
                if (dataViews == null)
                {
                    return new List<DataViewItem>();
                }
                return dataViews.ToList();
            }
        }
        #endregion
        
        #region GetReport
        public DataViewItem GetReport(string name)
        {
            return Reports.Where(p => p.Name == name).FirstOrDefault();
        }
        #endregion

        #region GenerateReport        
        public XtraReport GetDevxReport(string name)
        {
            var report = GetReport(name);
            var devxReport = new XtraReport();
            try
            {
                App.DataSerializer.QueryString = report.DataSource;
                devxReport.Name = report.Name;                
                devxReport.Extensions[SerializationService.Guid] = DevxDataSerializer.Name;
                devxReport.DataSource = App.Odbc.Data(report.DataSource);
                devxReport.DataMember = "Table";
                if (!string.IsNullOrEmpty(report.Display))
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(report.Display);
                    MemoryStream stream = new MemoryStream(byteArray);
                    devxReport.LoadLayoutFromXml(stream);
                }
            }
            catch (Exception e)
            {
                App.Utils.LogExc(e);
            }
            return devxReport;
        }
        #endregion

        

    }
}
