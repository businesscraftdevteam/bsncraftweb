﻿using BsnCraft.Web.Common.Classes;
using NLog;
using System.Collections.Generic;
using System.Linq;

namespace BsnCraft.Web.Common.Controllers.Base
{

    public enum Controllers
    {
        Activities,
        Customers,
        Contracts,
        SalesCentres,
        DataViews,
        Items,
        Jobs,
        JobInfo,
        Leads,
        PurchaseOrders,
        System,
        Variations,
        AreaPrices,
        Quotes
    }

    public class BsnController
    {
        #region Private Properties
        private BsnApplication App { get; set; }
        private List<BsnViewModel> Controllers { get; set; }
        #endregion

        #region Constructor
        public BsnController(BsnApplication app)
        {
            Controllers = new List<BsnViewModel>();
            App = app;
        }
        #endregion

        #region Init
        public void Init()
        {
            Controllers.Clear();
        }
        #endregion

        #region Get
        private BsnViewModel Get(Controllers key)
        {
            if (Controllers.Any(p => p.Key.Equals(key)))
            {
                return Controllers.Where(p => p.Key.Equals(key)).FirstOrDefault();
            }

            BsnViewModel controller = null;

            switch (key)
            {
                case Base.Controllers.Activities:
                    controller = new ActivitiesViewModel(App);
                    break;
                case Base.Controllers.Customers:
                    controller = new CustomersViewModel(App);
                    break;
                case Base.Controllers.Contracts:
                    controller = new ContractsViewModel(App);
                    break;
                case Base.Controllers.SalesCentres:
                    controller = new SalesCentresViewModel(App);
                    break;
                case Base.Controllers.DataViews:
                    controller = new DataViewsViewModel(App);
                    break;
                case Base.Controllers.Items:
                    controller = new ItemsViewModel(App);
                    break;
                case Base.Controllers.Jobs:
                    controller = new JobCostViewModel(App);
                    break;
                case Base.Controllers.JobInfo:
                    controller = new JobInfoViewModel(App);
                    break;
                case Base.Controllers.Leads:
                    controller = new LeadsViewModel(App);
                    break;
                case Base.Controllers.PurchaseOrders:
                    controller = new PurchaseOrdersViewModel(App);
                    break;
                case Base.Controllers.System:
                    controller = new SystemViewModel(App);
                    break;
                case Base.Controllers.Variations:
                    controller = new VariationsViewModel(App);
                    break;
                case Base.Controllers.AreaPrices:
                    controller = new AreaPricesViewModel(App);
                    break;
                case Base.Controllers.Quotes:
                    controller = new QuotesViewModel(App);
                    break;
            }

            if (controller != null)
            {
                Controllers.Add(controller);
            }

            return controller;

        }
        #endregion

        #region Activities
        public ActivitiesViewModel AC
        {
            get
            {
                return (ActivitiesViewModel)Get(Base.Controllers.Activities);
            }
        }
        #endregion

        #region Customers
        public CustomersViewModel AR
        {
            get
            {
                return (CustomersViewModel)Get(Base.Controllers.Customers);
            }
        }
        #endregion

        #region Contracts
        public ContractsViewModel CO
        {
            get
            {
                return (ContractsViewModel)Get(Base.Controllers.Contracts);
            }
        }
        #endregion

        #region SalesCentres
        public SalesCentresViewModel SC
        {
            get
            {
                return (SalesCentresViewModel)Get(Base.Controllers.SalesCentres);
            }
        }
        #endregion
        
        #region DataViews
        public DataViewsViewModel DV
        {
            get
            {
                return (DataViewsViewModel)Get(Base.Controllers.DataViews);
            }
        }
        #endregion

        #region Items
        public ItemsViewModel IM
        {
            get
            {
                return (ItemsViewModel)Get(Base.Controllers.Items);
            }
        }
        #endregion

        #region Job Cost
        public JobCostViewModel JC
        {
            get
            {
                return (JobCostViewModel)Get(Base.Controllers.Jobs);
            }
        }
        #endregion

        #region Quotes
        public QuotesViewModel QT
        {
            get
            {
                return (QuotesViewModel)Get(Base.Controllers.Quotes);
            }
        }
        #endregion

        #region JobInfo
        public JobInfoViewModel JI
        {
            get
            {
                return (JobInfoViewModel)Get(Base.Controllers.JobInfo);
            }
        }
        #endregion

        #region Leads
        public LeadsViewModel LE
        {
            get
            {
                return (LeadsViewModel)Get(Base.Controllers.Leads);
            }
        }
        #endregion

        #region AreaPrices
        public AreaPricesViewModel APR
        {
            get
            {
                return (AreaPricesViewModel)Get(Base.Controllers.AreaPrices);
            }
        }
        #endregion
               
        #region PurchaseOrders
        public PurchaseOrdersViewModel PO
        {
            get
            {
                return (PurchaseOrdersViewModel)Get(Base.Controllers.PurchaseOrders);
            }
        }
        #endregion

        #region System
        public SystemViewModel SY
        {
            get
            {
                return (SystemViewModel)Get(Base.Controllers.System);
            }
        }
        #endregion

        #region Variations
        public VariationsViewModel VA
        {
            get
            {
                return (VariationsViewModel)Get(Base.Controllers.Variations);
            }
        }
        #endregion

    }
}
