﻿using BsnCraft.Web.Common.Classes;
using NLog;
using System.Collections.Generic;
using System.Data;

namespace BsnCraft.Web.Common.Controllers.Base
{
    public abstract class BsnViewModel
    {
        #region Public Properties
        public Controllers Key { get; private set; }
        #endregion

        #region Protected Properties
        protected Logger Log { get; private set; }        
        protected BsnApplication App { get; private set; }        
        #endregion

        #region Constructor
        protected BsnViewModel(BsnApplication app, Controllers key)
        {
            Log = LogManager.GetLogger(GetType().FullName);
            App = app;
            Key = key;
        }
        #endregion

        #region GetDropDownList        
        public List<ComboItem> GetDropDownList(string code, string desc, string table, string filter = "", string extraField = "")
        {
            var list = new List<ComboItem>();
            string fields = code + "," + desc;

            if (!string.IsNullOrEmpty(extraField))
                fields += "," + extraField;

            string query = "SELECT " + fields + " FROM " + table + " WHERE {fn LENGTH(" + code + ")} > 0";
            if (!string.IsNullOrEmpty(filter))
            {
                query += " AND " + filter;
            }
            var List = App.Odbc.Table(query);
            if (List != null)
            {
                foreach (DataRow item in List.Rows)
                {
                    if (!string.IsNullOrEmpty(extraField))
                    {
                        list.Add(new ComboItem(item[code].ToString(), item[desc].ToString(), item[extraField].ToString()));
                    }
                    else
                    {
                        list.Add(new ComboItem(item[code].ToString(), item[desc].ToString()));
                    }
                }
            }
            return list;
        }
        #endregion       
        
    }
}