- Make sure Web Deploy 3.6 is installed (install from google search).

Go into add/remove programs or IIS role servies

- Make sure ASP.NET 4.6, etc. are all checked.

If you have the message:

The installer was interrupted before Application could be installed... 

- Make sure IIS 6 Management Compatibility is checked.


If you have the message:

This setup requires internet information server 5.1 or higher...

HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\W3SVC\Parameters

Change Major Version from decimal 10+ to decimal 9 (ms bug)
